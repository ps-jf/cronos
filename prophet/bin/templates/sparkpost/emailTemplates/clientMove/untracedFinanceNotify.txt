Hello Finance,


You are receiving this e-mail as client <?= $client ?> (<?= $client->id() ?>) has been moved from account <?= $current_partner ?> to <?= $new_partner ?>.


This client was moved by <?= $email ?> , If you require any further information please contact them directly.


Reason provided for move via Prophet:

<?= $reason ?>


Kind regards
Prophet | Policy Services