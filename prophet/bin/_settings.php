<?php
/**
 * Series of defined constants for use throughout application.
 *
 * This is a file full of constants that can be used throughout the application
 * All database tables are defined here, as well as other useful things
 * like filepaths
 * If you change something in here to get the application working on your local
 * install, then you are doing it wrong. Use my_settings instead
 *
 * @package prophet.variables
 * @author daniel.ness
 */

define("APP_NAME", "Prophet");
define("PROPHET_VERSION", "4.0");
define("APP_ENV", "production");

define("DEV_EMAIL", "jonathan.foley@policyservices.co.uk");

date_default_timezone_set('Europe/London');

# # Database settings # # 
define("SQL_HOST", "localhost");
define("SQL_USER", "prophet");
define("SQL_PASSWD", "eet5ap1zza3~14");

define("REMOTE_SQL_HOST", "134.19.164.22");
define("REMOTE_SQL_USER", "officeboy");
define("REMOTE_SQL_PASSWD", "KINd0vBLuu;");

define("IO_SQL_HOST", "35.178.41.208");
define("IO_SQL_USER", "ioadmin");
define("IO_SQL_PASSWD", "APsc7TQ7vJQ7E8k");

define("RPT_SQL_HOST", "192.168.16.250");
define("RPT_SQL_USER", "personal_access");
define("RPT_SQL_PASSWD", "that goes in there!");

define("RPT_REMOTE_SQL_USER", "reporter");
define("RPT_REMOTE_SQL_PASSWD", "1G2HRzdQTJrwk5P9iqeZ");

define("RPT_BM2_SQL_HOST", "192.168.16.22");
define("RPT_BM2_SQL_USER", "personal_access");
define("RPT_BM2_SQL_PASSWD", "that goes in there!");

define("PORTAL_HOST", "xxx-xxx-xxx");
define("PORTAL_USER", "xx");
define("PORTAL_PASSWD", "xxx");

## Jasper Server ##
#define("JASPER_HOST", "http://reports.internal.policyservices.com/jasperserver");
define("JASPER_HOST", "http://ps-rosie.internal.policyservices.com/jasperserver");
define("JASPER_USER", "reporter");
define("JASPER_PASSWD", "reporter");
define("JASPER_GDPR_REPORTS", "/reports/gdpr/");
define("JASPER_PROPHET_REPORTS", "/reports/prophet_reports/");
define("JASPER_STANDARD_LETTERS", "/reports/Standard_Letters/");

# # Databases # #
define("SYS_DB", "prophet");
define("DATA_DB", "feebase");
define("REMOTE_SYS_DB", "myps");
define("IO_DB", "io");
define("PORTAL_DB", "client_portal");

define("LOCAL", json_encode([SQL_HOST, SQL_USER, SQL_PASSWD, DATA_DB]));
define("REMOTE", json_encode([REMOTE_SQL_HOST, REMOTE_SQL_USER, REMOTE_SQL_PASSWD, REMOTE_SYS_DB]));
define("IO", json_encode([IO_SQL_HOST, IO_SQL_USER, IO_SQL_PASSWD, IO_DB]));
define("REPORTING", json_encode([RPT_SQL_HOST, RPT_SQL_USER, RPT_SQL_PASSWD, DATA_DB]));
define("REPORTING_REMOTE", json_encode([REMOTE_SQL_HOST, RPT_REMOTE_SQL_USER, RPT_REMOTE_SQL_PASSWD, REMOTE_SYS_DB]));
define("REPORTING_BM2", json_encode([RPT_BM2_SQL_HOST, RPT_BM2_SQL_USER, RPT_BM2_SQL_PASSWD, DATA_DB]));
define("PORTAL", json_encode([PORTAL_HOST, PORTAL_USER, PORTAL_PASSWD, PORTAL_DB]));

# # remote database tables # #
define("REMOTE_USR_TBL", REMOTE_SYS_DB . ".user_info");
define("REMOTE_VIEWER_TBL", REMOTE_SYS_DB . ".viewer_tbl");
define("REMOTE_CHANGES_TBL", REMOTE_SYS_DB . ".partner_changes");
define("REMOTE_CHANGES_AUDIT_TBL", REMOTE_SYS_DB . ".partner_changes_audit");
define("REMOTE_NOTIFY_TBL", REMOTE_SYS_DB . ".notify");
define("REMOTE_TRAFFIC_TBL", REMOTE_SYS_DB . ".traffic_audit");
define("REMOTE_LOGIN_TBL", REMOTE_SYS_DB . ".login_audit");

define("REMOTE_DATA_CHANGE_TBL", REMOTE_SYS_DB . ".data_change");

define("REMOTE_MANDATE_PREPOP_TBL", REMOTE_SYS_DB . ".mandate_prepop");

define("WEB_NOTIFICATION_TBL", REMOTE_SYS_DB . ".notifications");
define("WEB_NOTIFICATION_VIEWER_TBL", REMOTE_SYS_DB . ".notifications_viewers");
define("WEB_CORRESPONDENCE_AUDIT_TBL", REMOTE_SYS_DB . ".correspondence_audit");

# # System tables # #
define("TBL_MENU", SYS_DB . ".menu");
define("USR_TBL", SYS_DB . ".users");
define("GRP_TBL", SYS_DB . ".groups");
define("TBL_USERS_GROUPS", SYS_DB . ".users_groups");
define("TBL_GROUPS_MENUS", SYS_DB . ".groups_menus");
define("LOG_TBL", SYS_DB . ".log_logins");
define("ACTIONS_TBL", SYS_DB . ".log_actions_new");
//define("ACTIONS_TBL", SYS_DB . ".log_actions");
define("VIEWED_TBL", SYS_DB . ".recently_viewed");
define("ALERTS_TBL", SYS_DB . ".alerts");

# # Data tables # #
define("ADDITIONAL_EMAIL", DATA_DB . ".additional_email");
define("BRANCH_TBL", DATA_DB . ".tblbranch");
define("BULK_DETAILS_TBL", DATA_DB . ".bulk_details");
define("CLIENT_TBL", DATA_DB . ".tblclient");
define("CORR_TBL", DATA_DB . ".tblcorrespondencesql");
define("MANDATE_TBL", DATA_DB . ".mandate_tbl");
define("MANDATE_CLIENT_TBL", DATA_DB . ".mandate_client_tbl");
define("NOTES_TBL", DATA_DB . ".notes");
define("NOTES_AUDIT_TABLE", DATA_DB . ".notes_audit");
define("POLICY_TBL", DATA_DB . ".tblpolicy");
define("POLICY_STATUS_TBL", DATA_DB . ".tblpolicystatussql");
define("POLICY_TYPE_TBL", DATA_DB . ".tblpolicytype");
define("PARTNER_TBL", DATA_DB . ".tblpartner");
define("CHASE_TBL", DATA_DB . ".tbl_chase_holding");
define("PRACTICE_TBL", DATA_DB . ".practice");
define("PRACTICE_ALIAS_TBL", DATA_DB . ".practice_alias");
define("PRACTICE_STAFF_TBL", DATA_DB . ".practice_staff");
define("ONLINE_ACCESS_TBL", DATA_DB . ".provider_online_access");
define("PROSPECT_TBL", DATA_DB . ".pm_prospects");
define("STAFF_TBL", DATA_DB . ".staff");
define("COMPLAINTS_TBL", DATA_DB . ".complaints_register");

# adviser charging tables
define("AC_TBL", DATA_DB . ".adviser_charging");
define("AC_FILE_TBL", DATA_DB . ".adviser_charging_file");

# action required tables
define("AR_TABLE", REMOTE_SYS_DB . ".action_required");
define("AR_SUBJECT_TABLE", REMOTE_SYS_DB . ".action_required_subject");
define("AR_MESSAGE_TABLE", REMOTE_SYS_DB . ".action_required_message");
define("AR_ATTACHMENTS_TABLE", REMOTE_SYS_DB . ".action_required_attachments");

# commission tables
define("COMM_TBL", DATA_DB . ".tblcommission");
define("COMM_POST_TBL", DATA_DB . ".tblcommissionpost");
define("COMM_AUDIT_TBL", DATA_DB . ".tblcommissionauditsql");
define("COMM_TYPE_TBL", DATA_DB . ".tblcommissiontype");
define("COMM_IMPORT_FILE_TBL", SYS_DB . ".commission_import_files");
define("BATCH_TBL", DATA_DB . ".tblbatch");
define("BANK_TBL", DATA_DB . ".bank_account");
define("AGENCY_TBL", DATA_DB . ".tblagency_codes");
define("FCC_CATS", DATA_DB . ".accounts_categories");

# new business tables
define("NB_TBL", DATA_DB . ".tblnewbusiness");
define("NBTYPE_TBL", DATA_DB . ".tblnewbusinesstype");
define("NBSTATUS_TBL", DATA_DB . ".tblnewbusinessstatus");
define("NBCALL_TBL", DATA_DB . ".newbusiness_satisfaction_letter");

# workflow tables
define("WF_TBL", DATA_DB . ".workflow");
define("WFASS_TBL", DATA_DB . ".workflow_assigned");
define("WFPRO_TBL", DATA_DB . ".workflow_process");
define("WFS_TBL", DATA_DB . ".workflow_step");
define("WFA_TBL", DATA_DB . ".workflow_audit");
define("WFPROGRESS_TBL", DATA_DB . ".workflow_progress");

# issuer tables
define("ISSUER_TBL", DATA_DB . ".tblissuer");
define("ISSUER_CONTACT", DATA_DB . ".issuer_contact");
define("ISSUER_ALIAS_TBL", DATA_DB . ".tblissuer_alias");

# helper tables
define("OBJ_TBL", DATA_DB . ".objects");
define("PRI_TBL", DATA_DB . ".priority");

# valuation tables
define("VALUATION_TBL", DATA_DB . ".tblvalrequested");
define("VALUATION_STAGE_TBL", DATA_DB . ".info_prog");

# # Import tables # #
define("POLICY_IMPORT_TBL", DATA_DB . ".policy_import_holding");
define("CLIENT_IMPORT_TBL", DATA_DB . ".client_import_holding");

# # Merging tables # #
define("DATA_CHANGE_ERROR_TBL", DATA_DB . ".data_change_error");



# # filesystem paths # #
define("MANDATE_PENDING_SCAN_DIR", "/srv/mandates/scanned");
define("BULK_MANDATE_PENDING_SCAN_DIR", "/srv/mandates/scanned_bulk");
define("MANDATE_CLIENT_AGREEMENTS_DIR", "/srv/mandates/client_agreements");
define("MANDATE_PROCESSED_SCAN_DIR", "/srv/mandates/processed");
define("MANDATE_COPY_ISSUERS_DIR", "/mnt/terry_client_services/mandates/copies");
define("CORR_SCAN_DIR", "/mnt/terry2/psfiles/corrstore/");
define("COMM_SCAN_DIR", "/mnt/terry2/psfiles/commstore/");
define("COMM_UPLOAD_DIR", "/mnt/terry2/psfiles/commmypssync/");
define("SEARCH_LOG_DIR", "/mnt/terry2/psfiles/searchlog/");
define("PARTNER_PROFILE_DIR", "../lib/images/partners/");
define("RSS_FEED_LOCATION", "/mnt/terry2/psfiles/Slides/");
define("USER_PROFILE_DIR", "/lib/images/users/");
define("IMAGE_DIR", $_SERVER['DOCUMENT_ROOT'] . "/lib/images/");
define("CORR_MOTIVATION", "/lib/images/corr_motivation/");

define("RATES_DIR", "/mnt/terry2/rates/");

define("CORRESPONDENCE_PENDING_SCAN_DIR", "/srv/correspondence/pending/");
define("CORRESPONDENCE_PROCESSED_SCAN_DIR", "/mnt/corr_sync/");
#define( "CORRESPONDENCE_UPLOAD_DIR", "/mnt/terry2/psfiles/clientcorrespondence/" );

define("CORRESPONDENCE_DECLINED_DIR", "/srv/correspondence/spot_check/declined/");
define("CORRESPONDENCE_HOLDING_DIR", "/srv/correspondence/spot_check/");

define("PROPHET_FILE_UPLOAD_DIR", "/srv/file_storage/");
define("PROPHET_MANUALS_DIR", "/srv/file_storage/training_manuals/");
define("ADVISER_CHARGING_UPLOADS", "/srv/charging/");

define("PROREPORT_PENDING_SCAN_DIR", "/srv/portfolio_reports/pending/");
define("PROREPORT_PROCESSED_SCAN_DIR", "/srv/portfolio_reports/processed/");

define("COMMISSION_IMPORT_EDI", "/srv/commission/uploaded/EDI/");
define("COMMISSION_IMPORT_DATAFLOW", "/srv/commission/uploaded/Dataflow/");
define("COMMISSION_IMPORT_OTHER", "/srv/commission/uploaded/Other/");
define("COMMISSION_IMPORT_ERROR", "/srv/commission/errors/");
define("COMMISSION_IMPORT_FCC", "/srv/commission/uploaded/FCC/");

# # asset absolute paths # #
define("__APP_STATIC_ROOT__", "/lib/");
define("__CORE_STATIC_ROOT__", "/core/lib/");

define("_PUBLIC", $_SERVER["DOCUMENT_ROOT"]);
define("_BIN", _PUBLIC . "/../bin/");
define("TEMPLATE_DIR", _BIN . "templates/");
define("APPLET_DIR", "lib/applets/");
define("JASPER_DIR", __APP_BIN__ . "src/jasperclient");
define("AWS_DIR", __APP_BIN2__ . "src/aws-sdk");
define("GUZZLE_DIR", __APP_BIN2__ . "src/guzzlehttp");
define("PSR_DIR", __APP_BIN2__ . "src/psr");
define("JMES_DIR", __APP_BIN2__ . "src/mtdowling");

define("LOG_PATH", __APP_BIN__ . "logs");
define("RSS_FEED_LOCATION_BACKUP", TEMPLATE_DIR . "ps.xml");

# # helpers # #
//define( "TAB", str_repeat(" ",4) ); // a little lazy dontcha think?
define("MYSQLI_OBJECT", 'oop'); // a new friend for MYSQLI_ASSOC , MYSQLI_NUM , and MYSQL_BOTH
define("SEARCH_LIMIT", 15); // default limit for most suggests
define("PS_TELEPHONE_PREFIX", "0345 450 7806");    //change once,developer happy
define("PS_EMAIL_MAIN", "partnersupport@policyservices.co.uk");    //likewise

# # Jira API details # #
define("JIRA_USERNAME", "lewis.barbour@policyservices.co.uk");
define("JIRA_PASSWORD", "IjsdwmBh0BLq3Vlfm6NV716B");
define("JIRA_URL", "https://policyservices.atlassian.net/activity");
define("JIRA_RSS_FEED_LOCATION", "/mnt/slides/");

# # SPARKPOST API details # #
define("SPARKPOST_API_KEY", "06b2eb616803cd51d9b718ede66045fb9333e1d4");
define("EMAIL_TEMPLATES", "/templates/sparkpost/emailTemplates/");
define("DEDICATED_MAILER_IP", "ps_mailer");

# # constants for use throughout ps documentation/applications
define(
    "STANDARD_LETTER_FOOTER",
    serialize(
        [
            "COMPANY_EMAIL" => "partnersupport@policyservices.co.uk",
            "COMPANY_ADDR_1" => "POLICY SERVICES LTD",
            "COMPANY_ADDR_2" => "PO BOX 13594",
            "COMPANY_ADDR_3" => "LINLITHGOW",
            "COMPANY_POSTCODE" => "EH49 9BB",
            "COMPANY_TEL" => "0345 450 7806",
            "COMPANY_FAX" => "0870 420 5174",
            "COMPANY_WEB" => "www.policyservices.co.uk",
            "COMPANY_REG_OFFICE" => "Registered Office - Priorsford, 75 Grahamsdyke Road, Bo'ness, EH51 9DZ. Registered in Scotland No. 230167."
        ]
    )
);

# define array of subjects that should not update policy status on correspondence add
define("CORR_STATUS_OMISSION", serialize(
        [
            "Action Required",
            "Address notification from Client",
            "Address notification from Partner",
            "Advice Compliance File - SR to Client",
            "Adviser Charging Form sent to Provider",
            "Beneficiary Documents",
            "Birth Certificate - Sent to provider",
            "Birth Certificate - Returned from provider",
            "Birth Certificate - Returned to partner",
            "Client ID",
            "Client Signature Verification",
            "Copy original correspondence sent to Company",
            "Communication Report",
            "Data Subject Consent Form",
            "Death Certificate - Copy",
            "Death Certificate to Provider",
            "Death Certificate returned from Provider",
            "Death Certificate returned to Partner",
            "Death Claim forms",
            "GOP to Provider",
            "GOP returned from Provider",
            "GOP returned to Partner",
            "LOA",
            "Marriage Certificate Required",
            "Marriage Certificate - Sent to provider",
            "Marriage Certificate - Returned from provider",
            "Marriage Certificate - Returned to partner",
            "POA sent to provider",
            "POA returned from provider",
            "POA returned to Partner",
            "Risk Assessment",
            "Trustee Documents"
        ]
    )
);

define("PARTNER_CLIENT_ADD_BLOCK", [
    5130 => [ // UNTRACED (INCOME)
        "users" => [29,99,203],
        "depts" => [7],
        "team_leaders" => false,
    ],
    5431 => [ // *agencies only* (Chapel)
        "users" => [],
        "depts" => [7],
        "team_leaders" => false,
    ],
    394 => [ // UNTRACED
        "users" => [],
        "depts" => [7,6],
        "team_leaders" => true,
    ],
    3212 => [ // Ingram Graham
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3980  => [ // Atlantic Wealth
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3990 => [ // Mark Pentalow (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3991 => [ // Stephen Davis (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3992 => [ // Steve Baker (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3993 => [ // Ex Hugh May (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3994 => [ // Claire Connell (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3995 => [ // Steve Martin (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3996 => [ // Ian Wood (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3997 => [ // Katie Mundell (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3998 => [ // Peter Garrido (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3999 => [ // Matthew Newsome (Atlantic Wealth)
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    4252 => [ // Ruth ex Dolman
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    4725 => [// Gary Powling
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    4726 => [ // Gary Whiteside
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    4727 => [ // Rhodri Samuel
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    4728 => [ // Howard Rice
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    4729 => [ // Nigel Bilton
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    4730 => [ // Mark Wallis
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    3918 => [ // PS Virtue
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    3514 => [ // PS Virtue
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    3942 => [ // PS True Virtue
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    4015 => [ // PS Virtue Transferred Out
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    3896 => [ // Virtue (Ex Consilium Wealth Management LLP)
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    4059 => [ // Virtue (Ex Derek Wood Clients)
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    4112 => [ // Virtue Paul Yewdall
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    4160 => [ // Virtue Alan (Own Clients)
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    4161 => [ // Virtue Grant (Own Clients)
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    4166 => [ // Virtue (Ex Tim McTigue)
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    4167 => [ // Virtue (Ex Michael Gadsden)
        "users" => [109],
        "depts" => [7,10],
        "team_leaders" => false,
    ],
    5319 => [ // Tim Miller
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    5380 => [ // Grant Kingston
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    5338 => [ // Ian Cross
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    5320 => [ // Greg Kingston
        "users" => [],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    5479 => [ // *unmatched*
        "users" => [203],
        "depts" => [1,4,7],
        "team_leaders" => false,
    ],
    5687 => [
        "users" => [98],
        "depts" => [7],
        "team_leaders" => false,
    ],
    5688 => [
        "users" => [98],
        "depts" => [7],
        "team_leaders" => false,
    ],
    5527 => [
        "users" => [60, 44, 201, 248],
        "depts" => [4],
        "team_leaders" => false,
    ]
]);

define("CORR_JSON_PATH_POSITION", 6);

##AWS S3 CREDENTIALS
define("AWS_BUCKET", "mypsaccount-storage");
define("AWS_STORAGE_KEY", 'AKIAJS67X2GHFHJZGQDQ');
define("AWS_SECRET", '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');

define("MYPS_APP_KEY", "base64:ol1mSysxxSQ+F7bHKTOSUa4bnK8bW1Mo/OrMFES1bIY=");

define("INVOICE_NINJA_URL", "http://bigmummy2.internal.policyservices.com:8000");
//define("INVOICE_NINJA_TOKEN", "hcphliu88yreugxnspdpltsmgow2xmxc");
define("INVOICE_NINJA_TOKEN", "1awjton1di7rdfca4lkfsy1l2nzvvjcc");
define("NINJA_INVOICES", "ninja.invoices");

define("HRZN_URL", "https://iobridge.policyservices.co.uk");

define("DSAR_DIR", "\\\\terry\\compliance\\DSAR\\");

define("WIDGETS_DATA", "/mnt/slides/widgets/");
define("SQL_QUERIES", "/mnt/terry2/psfiles/queries/");

define("INCOME_RUN_INSTRUCTIONS", "/mnt/slides/incomerun/instructions.txt");

##AZURE CREDENTIALS
define("AZURE_ACCOUNT_NAME", "pscdnstore");
define("AZURE_ACCOUNT_KEY", "sTuQ6l5DKI7qp/kmdBeZwhM8nqXT0aTGSZBp5o8MRq0qtiPXdj4kd5L3+pBSHoe7T5iK1IhdEHKxMrnyBKhzUw==");

define("PROSPECT_PROFILE_DIRECTORY", "https://pscdnstore.blob.core.windows.net/cdn/recruit_images/");
define("PARTNER_PROFILE_DIRECTORY", "https://pscdnstore.blob.core.windows.net/cdn/partner_images/");
define("STAFF_PROFILE_DIRECTORY", "https://pscdnstore.blob.core.windows.net/cdn/staff_images/");

define("CDN_BASE", "https://pscdnstore.blob.core.windows.net/cdn/");
define("CDN_DIR", "cdn/");

define("EMAIL_WHITELIST", [
    '127.0.0.1',
    '127.0.0.1:8080',
    '127.0.0.1:8088',
    '::1',
    'localhost:8080',
    'localhost:8088',
    '10.0.2.2'
]);

define("IO_CLIENT_LINK", "https://www.intelligent-office.net/nio/Client/");

define("LIVE_POLICY_STATUS", [2,5,9,18]);

define("SCRIPTS_DIR", "/mnt/terry2/psfiles/scripts/");
define("SCRIPT_OUTPUT", "/mnt/terry2/psfiles/script_output/");

define("CFH_API_USR", "kevin.dorrian@policyservices.co.uk");
define("CFH_API_PWD", "t3h!C2t6h%%Pfkqm8w6k");

