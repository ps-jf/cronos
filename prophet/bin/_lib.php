<?php
/**
 * This file acts as an intermediary.
 *
 * This file acts as an intermediary between the development
 * environment and the shared core functionality of our
 * applications.
 *
 * @package prophet.variables
 * @author kevin.dorrian
 */

define("__CORE_DIR__", $_SERVER["DOCUMENT_ROOT"]."/../../core/");
define("__APP_DIR__", str_replace("/public/", "/", $_SERVER["DOCUMENT_ROOT"]));
define("__CORE_BIN__", __CORE_DIR__."/bin/");
define("__APP_BIN__", __APP_DIR__."/bin/");
define("__APP_BIN2__", __APP_DIR__."bin/"); // App bin (-1 forward slash)
define("__APP_CONTENT_DIR__", "/pages/");

define("USER_CLASSNAME", "User"); // Primary User Classname

include(__CORE_DIR__."bin/_lib.php");


class Log
{
  /**
   * A class for logging errors to the platform logs directory.
   * Each log entry is a json string, making it both human-friendly
   * and also compatible with NoSQL databases (lovely analysis data).
   */

    const LOG_DB = "prophetlogs";
    const ACCESS = "access";
    const ERROR = "error";
    const LOGIN = "login";
    private $type;
    private $usr;

    /**
     * @arg $type: one of the log constants above
     */
    public function __construct($type)
    {
        if (!defined("self::".strtoupper($type))) {
            trigger_error("Invalid log type '$type'");
        }
        $this->type = $type;
    }


    public function set_user($uid = null)
    {
        $this->usr = $uid;
    }


    /**
     * Writes a log entry to mongodb server
     * @arg $log: an assoc array to be json-ified
    */
    public function write(array $log = null)
    {

        $user = User::get_default_instance('id');

        // if we do not yet have a cookie set, check to see if a dev has told us which user it is
        if ($user == false) {
            $user = $this->usr;
        }

        // wrap the log entry with a base log of common data
        $base_log = array(
            'time' => time(),
            'user' => ($user) ? $user : null,
            'ip'   => $_SERVER["REMOTE_ADDR"],
            'request' => array( "url"=>$_SERVER["REQUEST_URI"], "method"=>$_SERVER["REQUEST_METHOD"] ),
        );

        if ($log != null) {
            $base_log[$this->type] = $log;
        }
    }
}
