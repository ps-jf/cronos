<?php

class PipelinePendingEntry extends DatabaseObject
{
    
    public function __construct()
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);
        $this->import_file = new Sub("PipelineImportFile", "file_id");
        
        $this->policy_text = new Field("policy_text");
        $this->policy = new Sub("Policy", "policy_id");
        
        $this->client_forename = new Field("client_forename");
        $this->client_surname = new Field("client_surname");
    }

    public function __toString()
    {
        return "$this->policy_text";
    }
}
