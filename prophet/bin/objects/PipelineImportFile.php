<?php

class PipelineImportFile extends DatabaseObject
{
    const EDI = "EDI";
    const DATAFLOW = "DataFlow";
    var $table = COMM_IMPORT_FILE_TBL;

    public function __construct()
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);
        $this->type = new Field("type");
        $this->uploaded = new Date("uploaded_datetime");
        $this->user = new Sub("User", "imported_by");
        $this->issuer = new Sub("Issuer", "issuer");
        $this->filename = new Field("filename");
        $this->safe_name = new Field("safe_name");
        $this->completed_time = new Date("completed_datetime");
        $this->hash = new Field("SHA1_hash");
    }

    public function import()
    {
        if ($this->type() == PipelineImportFile::EDI) {
        } else if ($this->type() == PipelineImportFile::DATAFLOW) {
        }
    }

    public function is_valid()
    {
    }

    public function __toString()
    {
        return "$this->type";
    }
}
