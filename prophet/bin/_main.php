<?php

/**
 * Ensure we are using the correct path for our autoloader
 */

session_start();

require_once("_lib.php");

ini_set("default_socket_timeout", 1);

if ($_SERVER['SERVER_PORT'] == "8080" || $_SERVER['SERVER_PORT'] == "80") {
    // development & new staging
    if (file_exists('/home/vagrant/dev/cronos/vendor/autoload.php')) {
        // include composer autoloader
        require '/home/vagrant/dev/cronos/vendor/autoload.php';
    } elseif (file_exists('/var/www/sites/prophet/current/vendor/autoload.php')) {
        // include composer autoloader
        require '/var/www/sites/prophet/current/vendor/autoload.php';
    }

    $_SESSION['terry'] = true;
    $_SESSION['io_bridge'] = true;
} elseif ($_SERVER['SERVER_PORT'] == "81") {
    // staging
    if (file_exists('/var/www/beta/prophet/current/vendor/autoload.php')) {
        // include composer autoloader
        require '/var/www/beta/prophet/current/vendor/autoload.php';
    }

    // Check Terry is online
    $terryIP = "192.168.16.104";
    $terry = fsockopen($terryIP, "80", $errno, $errstr);

    if ($terry){
        $_SESSION['terry'] = true;
    } else {
        $_SESSION['terry'] = false;
    }

    // Check IO Bridge is online
    $ioIP = "35.178.41.208";
    $io = fsockopen($ioIP, "80", $errno, $errstr);

    if ($io){
        $_SESSION['io_bridge'] = true;
    } else {
        $_SESSION['io_bridge'] = false;
    }
} else {
    // production
    if (file_exists('/var/www/sites/prophet/current/vendor/autoload.php')) {
        // include composer autoloader
        require '/var/www/sites/prophet/current/vendor/autoload.php';
    }

    // Check Terry is online
    $terryIP = "192.168.16.104";
    $terry = fsockopen($terryIP, "80", $errno, $errstr);

    if ($terry){
        $_SESSION['terry'] = true;
    } else {
        $_SESSION['terry'] = false;
    }

    // Check IO Bridge is online
    $ioIP = "35.178.41.208";
    $io = fsockopen($ioIP, "80", $errno, $errstr);

    if ($io){
        $_SESSION['io_bridge'] = true;
    } else {
        $_SESSION['io_bridge'] = false;
    }
}

$bugsnag = Bugsnag\Client::make("917eb844e1fd9a875bcd199a1601a3dc");
Bugsnag\Handler::register($bugsnag);
$bugsnag->registerCallback(function ($report) {
    $report->setUser([
        'id' => $_COOKIE["uid"]
    ]);
});
$bugsnag->setErrorReportingLevel(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
$bugsnag->setReleaseStage($_COOKIE["stage"] ?? 'unstaged');

$redisClient = RedisConnection::connect();

setcookie("stage", APP_ENV);

// A spot of security to make sure that user is signed on
// and that they are a valid user.
//
if (isset($_COOKIE["uid"], $_COOKIE[ "u_enc" ])) {
    $_USER = new User($_COOKIE["uid"]);

    if (!$_USER->get() || !$_USER->valid_cookies()) {
        // If user cannot be found on db or the security cookies
        // appear to have been tampered with, reset cookies and
        // throw back to the login screen.

        $_USER->logout();
        header("Location: /");
    } else {
        $_USER->auth(); // boost cookies
    }
} else {
    // User's security cookies have expired. Popup a re-login prompt
    // so as to try and retain the user's session.

    if (!isset($_GET["response-type"])) {
?>
    <script type="text/javascript">
        $( 
          function( ){ 
                var pop = new popup(); 
                $( pop.content ).load("/?timeout=1"); 
          } 
        );
    </script>
<?php
    }

    die();
}
?>
