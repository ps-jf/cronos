<?php
/**
 * This allows us to carry out tasks needed to deploy the application
 *
 *
 * @package prophet.helpers
 * @author kevin.dorrian
 */

require_once($_SERVER[ "DOCUMENT_ROOT" ] . "/../bin/_main.php");

$user = User::get_default_instance();
if ($user->department() !== '7') {
    exit('You are not allowed to run setup. Contact someone in IT');
}

/**
 * Here we build a directory of serialised objects that can be used by
 * other scripts etc. Providing some sort of interface into our ORM
 *
 * It's in JSON format, but be careful - the object definition is obviously not: it is PHP serialised
 *
*/

foreach ([ __CORE_BIN__, __APP_BIN__] as $base) {
    $fp = "{$base}objects/";

    $iterator = new DirectoryIterator($fp);

    foreach ($iterator as $fileinfo) {
        $json = [];
        $json['field_map'] = false;
        $json['obj_def'] = false;

        //don't deal with sub-directories
        if (!$fileinfo->isDot() && !$fileinfo->isDir()) {
            $obj_name = $fileinfo->getBasename('.php');

            // we don't want to invoke main either
            if ($obj_name !== '_main') {
                $x = new $obj_name;

                try {
                    $flattened = serialize($x->flat_array());
                    $json['obj_def'] = $flattened;

                    $map = $x->get_field_name_map();
                    $json['field_map'] = $map;

                    $file = "{$fp}serialised/{$obj_name}.json";

                    file_put_contents($file, json_encode($json));
                } catch (Exception $e) {
                    echo $e;
                }
            }
        }
    }
}

echo "Serialised object files created";
