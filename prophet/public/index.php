<?php

/* User is currently logged in */

if (isset($_COOKIE[ "uid" ], $_COOKIE[ "u_enc" ])) {
    require_once($_SERVER[ "DOCUMENT_ROOT" ] . "/../bin/_main.php");

    /* Logout user */
    if (isset($_POST["logout"])) {
        $logout_type = $_POST['logout_type'];
        $_USER->logout($logout_type);

        session_destroy();
    } else {
        $db = new mydb();

        /*
            ## Permissions-friendly menu builder ##
            
            Build a menu of tabs that the user is either 
            allowed to view due being a menu of a group
            who has access to said tab, or because the
            tab is globally available i.e. Home.
        */

        $user = User::get_default_instance("id");
        $dept = User::get_default_instance("department");

//        $query = "SELECT a.id, name, path, parent_id, icon, counter ".
//            "FROM ".TBL_MENU." as a ".
//            "LEFT JOIN ".TBL_GROUPS_MENUS." as b ON b.menu_id = a.id ".
//            "WHERE (b.group_id in ( SELECT group_id FROM ".TBL_USERS_GROUPS." where user_id = $user ) ".
//            "OR FIND_IN_SET($user, b.user_ids) ".
//            "OR b.is_global) and a.is_published = 1 ".
//            "GROUP BY a.id ".
//            "ORDER BY a.parent_id ASC, a.position, a.name";
//
//        if ($dept == 7) {
//            //let the IT department see every menu option regardless
//            $query = "SELECT a.id, name, path, parent_id, icon, counter ".
//                "FROM ".TBL_MENU." as a ".
//                "LEFT JOIN ".TBL_GROUPS_MENUS." as b ON b.menu_id = a.id ".
//                "WHERE a.is_published = 1 GROUP BY a.id ".
//                "ORDER BY a.parent_id ASC, a.position, a.name";
//        }

        // load all menu options and let permissions handle whether the user is allowed
        $query = "SELECT a.id, name, path, parent_id, icon, counter ".
            "FROM ".TBL_MENU." as a ".
            "WHERE a.is_published = 1 GROUP BY a.id ".
            "ORDER BY a.parent_id ASC, a.position, a.name";

        try {
            $db->query($query);
        } catch (Exception $e) {
            echo  "Error: cannot generate menu";
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        $navigation_tabs = [];
        $submenu = [];

        while ($d = $db->next(MYSQLI_OBJECT)) {
            // get the related permission
            $s = new Search(new Permission());
            $s->eq("category", 1); // menu category
            $s->eq("related_id", $d->id);

            $relatedPermission = $s->next(MYSQLI_ASSOC);

            if(!$relatedPermission || User::get_default_instance()->has_permission($relatedPermission->id())){
                // there is no permission needed or user has the permission
                /** Add a tab to the main menu **/
                if (!$d->parent_id) {
                    // some tabs may execute js
                    if (preg_match('/^javascript:/i', $d->path)) {
                        $href = $d->path;
                    } else {
                        // some tabs will load of page by default
                        if (empty($d->path)) {
                            $href = "#";
                        } else if (preg_match('/^\/common\//', $d->path)) {
                            $href = $d->path;
                        } else {
                            $href = "/pages".$d->path; // ... others require anchor to be picked up
                        }
                    }

                    // add tab to main menu
                    $navigation_tabs[] = "<li class='nav-item position-relative' data-submenu='$d->id' data-counter='$d->counter'><a class='nav-link' id=\"menu_$d->id\" name=\"$d->id\" href=\"$href\">$d->name <i title=\"$d->name\" class=\"$d->icon shortmenu animate\"></i></a><span class='badge badge-danger rounded-0' style='display: none;'>0</span></li>";

                    // ensure there is an array in submenu for this parent
                    if (!isset($submenu[ $d->id ])) {
                        $submenu[ $d->id ] = [];
                    }
                } else {
                    /** Or file submenu link under parent tab **/
                    $key = $d->parent_id;
                    $submenu[ $key ][ ] = ['id'=>$d->id,'name'=>$d->name,'path'=>(!empty($d->path) ? $d->path : ''),'counter'=>$d->counter];
                }
            }
        }

        // format submenu into javascript object
        $submenu_json = json_encode($submenu);
    }

    /* output shell + frontpage */
    echo new Template("shell.html", get_defined_vars());
} else {
    /* User hasn't logged in */

    // only require library
    require_once($_SERVER[ "DOCUMENT_ROOT" ] . "/../bin/_lib.php");

    if (isset($_GET["timeout"])) {
        /*
         * User's session cookies have expired and user
         * has is being shown a popup asking them to login
         * again.
         *
         */

        $t = new Template("timeout.html");
        $t -> tag(new Template("login-form.html"), "form");
        echo $t;
    } else if (isset($_POST["username"], $_POST["password"])) {
        // handle login requests
        $u = new User;
        echo json_encode($u->login($_POST["username"], $_POST["password"]));
    } else {
        // display login form
        echo new Template("login.html");
    }
}
