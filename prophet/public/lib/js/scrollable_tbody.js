function ScrollableTbody ( obj ) {
    /*
      I want to take a basic table structure, i.e.

      TABLE
      THEAD
	  TBODY
      TR
      TD - DATA
      ...
      
	  and turn it into this...
   
	  TABLE	
	  THEAD
	  TBODY
	  TR
	  TD
	  DIV
	  TABLE
	  TBODY
	  TR
	  TD - DATA
	  ....
							
	  To aid graceful degradation and also for developmental
	  ease this will be achieved purely through client-side DOM
	  manipulation rather than server-side templates etc.
   
	*/

	if( obj.tagName != "TABLE" ){ 
		return false; 
	}

    if ( obj.made_scrollable != null ) {
        return false;
    }
    
    obj.made_scrollable = true;

    $(obj).addClass("has_onshow");
	
	var content = $(obj).find("tbody")[0];
	var nested = $( "<div class='scroll y'><table></table></div>" );  // nested scrolling 

    $(">table",nested)
        .append( content );

    // object now contains a single-celled tbody, cell should span entire thead
	$(obj)
        .append( "<tbody><tr><td></td></tr></tbody>" ) // give outer-table a new single celled tbody
        .find("tbody td:first")
        .attr( "colSpan",999);

    $( "tbody td:first", obj )
        .html( nested );    // embed new div + table within original <table>

	obj.content = $("tbody:first",nested)[0];

    obj.resizeTimeout = null;

    $(obj.content).bind(
        "DOMNodeInserted",
        function( ){

            if( obj.resizeTimeout != null ){
                clearTimeout( obj.resizeTimeout );
            }

            obj.resizeTimeout = setTimeout(
                function(){ 

                    $(obj).trigger("calibrateContent");
                }, 100 
            );
        }
    );


    $( obj ).bind(
	"onshow",
	function( ){
		//for use with tabs, when the tab div is clicked and shown, calibrate the table
		$( obj ).trigger("calibrateContent");
	}
    );

    $( obj ).bind(
        "reset",
        function( ){
            // Reset styles in prep for an entirely new set of data
            $("thead th",obj).removeClass("sorted asc desc");
        }
    );

	$( obj ).bind( 
        "calibrateContent",
        function( ) {
            // Style rows
            $("tr",obj.content)
                .removeClass("even")
                .filter(":even")
                .addClass("even");

            var hCells = $(">thead>tr:last",obj).find("td,th");
            var i = 0;
            $(">tr:first",obj.content)
                .find("td,th")
                .each(
                    function(){
                        var hc = hCells.filter(":eq("+(i++)+")");
                        var w = parseInt( hc.css("width") );
                        $(this).css( "width", w );
                    }
                );
        }
    );

    $( nested ).bind(
        "scroll", 
        function( ){
            /*
              Implements an event which fires when the user scrolls to the bottom of a 
              scrollable tbody. This is handy for things such as staggered ajax lists etc.
            */
			
				if($(this).outerHeight() == (this.scrollHeight-$(this).scrollTop()) || ($(this).outerHeight() -1 ) == (this.scrollHeight-$(this).scrollTop()) && $(this).scrollTop() != 0 )
				{					
					$(obj).trigger( "hitScrollBottom" );
				}
        }
    );

	// Also, make all thead th cells sortable, ignore td cells
	$( "thead th:not(.no_sort)", obj ).bind( "click",
		function(){

			var me = this;
					
			var thead = $( me ).closest("thead");
			var table = $( thead ).closest( "table" );
			var div = $( table ).find( "div.scroll" );
			var data_tbody = $( div ).find( "tbody" );
			var rows = $( data_tbody ).children( "tr" );
            
            var me_index = $( thead ).children("tr").children("th").index( me );
			var th = $( thead ).find( "th" )[ me_index ];
			
			// Check if header cell has a predefined class determining
			// the type of sort that will be performed.
			
			var sorts = [
				"date", "percentage", "currency", 
				"number", "string"
			];
			
			var sort_class = sorts[ 4 ]; // default to string search
			for( var i = 0; i < sorts.length; i++ )
				if( $( th ).is( "." + sorts[ i ] ) )
					sort_class = sorts[ i ];							   
			
			// Sort ascending or descending?
			var sort_asc;
			
            if( $(th).is( ".sorted" ) ){

			    if( $(th).is( ".desc" ) )	{
				    sort_asc = true;
				    $( th )
                        .addClass("asc")
                        .removeClass( "desc" );									  
			    } else {
                    sort_asc = false;
				    $( th )
                        .removeClass( "asc" )
                        .addClass( "desc" );
                    
                }
            } else {
                sort_asc = true;
                $(this)
                    .addClass("sorted asc");
                
            }
			
			rows.sort(
				function(a,b) {

					if ($(th).hasClass("data-sort")){
						a = $( $( a ).children("td,th")[ me_index ] ).data("sort");
						b = $( $( b ).children("td,th")[ me_index ] ).data("sort");
					} else {
                        a = $( $( a ).children("td,th")[ me_index ] ).text( );
                        b = $( $( b ).children("td,th")[ me_index ] ).text( );
					}

					switch( sort_class ) {
                        
					case "date":

						//re-format dates so that Date.parse() has a readable format
						//Date.parse then converts them to unix timestamp which allows for correct ordering
						//output will remain in original format
						if(a != "" && b != ""){
							a = a.replace(/^(\d{2})\/(\d{2})\/(\d{4}).*$/, '$2/$1/$3');
							b = b.replace(/^(\d{2})\/(\d{2})\/(\d{4}).*$/, '$2/$1/$3');

							a = Date.parse( a );
							b = Date.parse( b );
						}
					break;
						
					case "percentage":
						a = parseFloat( a.replace( /[%\s]/g, "" ) );
						b = parseFloat( b.replace( /[%\s]/g, "" ) );	
						break;
						
					case "currency":
						if(a != "" && b != ""){
							a = parseFloat( a.replace( /[\$£\s,]/g, "" ) );
							b = parseFloat( b.replace( /[\$£\s,]/g, "" ) );
						}
						break;
						
					case "number":
						a = parseFloat( a );
						b = parseFloat( b );											
						break;
						
					}
					
					
					if( a == b )
						return 0;

					var x = ( a > b ) ? +1 : -1;
					return ( sort_asc ) ? x : ( x * -1 ) ;	 
					
				} 
			);
			
			$( data_tbody ).html( rows ); // Rearrange data rows
		}
	);

    $( obj )
        .trigger( "ready" )
        .trigger( "calibrateContent" );
}
