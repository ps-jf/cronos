/*
NB. You are implementing an object containing resultsets indexed by offset 
in order to save an unnecessary http request.
*/

function SuggestionBox(el, value) {

    /*
     * An AJAX-based form element which allows the user to search for
     * a particular DatabaseObject entry, displays matching results in
     * a simulated dropdown table below the textfield, and upon an item
     * being selected will take the entry's ID as it's own value.
     */

    el.addClass("init");

    var sb = $(":input:hidden", $(el))[0];

    sb.xhr;            // the most recent XmlHttpRequest
    sb.xhrTimeout;     // timeout to impose a delay before data requests
    sb.lastSearchHtml; // html content of textfield prior to last xhr
    sb.lastChoiceHtml; // html content of selected result prior to last xhr
    sb.offset;
    sb.lastResultReturned;
    sb.resultCache;
    sb.resultHover;
    sb.GETobj = {limit: 15};

    // internal components of sb
    sb.frame = $(el);
    sb.textfield = $(":input:text", sb.frame);
    sb.resultList = $("<ul/>");

    sb.img = $("<img/>")
        .attr("src", "/lib/images/suggest-ajax.gif")
        .css("position", "absolute")
        .css("right", "10px")
        .css("bottom", "10px");


    /***** Instance methods *****/

    sb.setValue = function (value, text) {

        $(sb).val(value);
        sb.textfield.val(text);
        sb.frame.removeClass("bad_news");

        if (parseInt($(sb).val())) {
            $(sb).trigger("selection");
        }
    };

    sb.resolveValue = function () {
        sb.textfield.val("-id " + sb.textfield.val());
        sb.requestData();
    };

    sb.requestData = function () {

        /* 
         * Perform an AJAX request for data using the config parameters and current text
         * of the nested textfield element.
         */

        if (sb.xhr != null) {
            sb.xhr.abort();
        }

        if (sb.offset in sb.resultCache) {

            sb.resultList.html(sb.resultCache[sb.offset]);
            sb.attachCtrls();

        } else {

            sb.GETobj.skip = sb.offset;
            sb.GETobj[sb.frame.attr("data-var")] = sb.textfield.val()
            ;
            sb.xhr = $.ajax({
                url: sb.frame.attr("data-url"),
                type: "GET",
                data: sb.GETobj,
                dataType: "json",
                beforeSend: function () {
                    sb.img
                        .appendTo(sb.frame)
                        .fadeIn("fast");
                },
                success: function (r) {

                    if (!("results" in r) || r.results.length < 1) {

                        sb.frame.addClass("bad_news");
                        sb.frame.trigger("no-suggestion");
                        sb.resultList.slideUp("fast");


                    } else {

                        if (r.remaining == 0) {
                            sb.lastResultReturned = true;
                        }

                        sb.lastSearchHtml = sb.textfield.val();
                        sb.displayResults(r.results);
                        sb.attachCtrls();
                        sb.resultCache[sb.offset] = sb.resultList.children("li[data-value]").clone();
                        sb.offset += r.results.length;
                    }
                },
                complete: function () {
                    sb.xhr = null;
                    sb.img.fadeOut("fast", function () {
                        $(this).remove()
                    });
                }
            });
        }
    };

    sb.attachCtrls = function () {

        /*
          1. Listen for clicks on list items when user makes a selection

          2. Prepend/append buttons to display next/previous set of
             results from the last search.
        */

        var execCtrl = function (e) {

            if (sb.xhr !== null) {
                return false;
            }

            sb.offset = parseInt($(this).attr("data-offset"));
            sb.requestData();
        };

        $("li[data-value]", sb.resultList).bind("mouseup", function (e) {
            // Set value based on selection and hide results
            e.stopPropagation();
            sb.setValue($(this).attr("data-value"), $(this).clone().children().remove().end().text());
            sb.resultList.slideUp("fast");
            sb.frame.css("z-index", 30);
        });


        if (sb.offset > 0) {
            $("<li/>")
                .text("Previous")
                .attr("data-offset", (sb.offset - sb.GETobj.limit))
                .prependTo(sb.resultList)
                .click(execCtrl);
        }

        if (!sb.lastResultReturned || (sb.offset + sb.GETobj.limit) in sb.resultCache) {

            $("<li/>")
                .text("Next")
                .attr("data-offset", sb.offset + sb.GETobj.limit)
                .appendTo(sb.resultList)
                .click(execCtrl);
        }
    };

    sb.displayResults = function (rArray) {

        /*
         * Populates this.resultList with all of the returned options
         * and displays the list to the user
         */

        sb.resultList.html(""); // empty last list

        // ensure param is Array
        if (!/^function Array/.test(rArray.constructor.toString())) {
            return;
        }

        for (var i = 0; i < rArray.length; i++) {
            var badge = "";

            if (rArray[i].type != null) {

                var text = rArray[i].text;

                if (rArray[i].type == "client") {
                    badge = "<span class='badge badge-pill badge-client'>" + rArray[i].type + "</span>";
                } else if (rArray[i].type == "scheme") {
                    badge = "<span class='badge badge-pill badge-scheme'>" + rArray[i].type + "</span>";
                } else if (rArray[i].type == "policy") {
                    badge = "<span class='badge badge-pill badge-policy'>" + rArray[i].type + "</span>";
                } else if (rArray[i].type == "issuer") {
                    badge = "<span class='badge badge-pill badge-issuer'>" + rArray[i].type + "</span>";
                } else if (rArray[i].type == "partner") {
                    badge = "<span class='badge badge-pill badge-partner'>" + rArray[i].type + "</span>";
                } else if (rArray[i].type == "practice") {
                    badge = "<span class='badge badge-pill badge-practice'>" + rArray[i].type + "</span>";
                }

                $("<li/>")
                    .html(text)
                    .attr("data-value", rArray[i].value) // 'value' attr is deprecated, this is HTML5-compliant
                    .attr("data-type", rArray[i].type)
                    .append(badge)
                    .appendTo(sb.resultList);
            } else {
                $("<li/>")
                    .text(rArray[i].text)
                    .attr("data-value", rArray[i].value) // 'value' attr is deprecated, this is HTML5-compliant
                    .appendTo(sb.resultList);
            }
        }

        sb.resultList.show();
    };


    //// Apply configurations
    sb.frame.append(sb.resultList);

    //// Bind event handlers
    sb.resultList.bind({
        mouseenter: function () {
            sb.resultHover = true;
        },
        mouseleave: function () {
            sb.resultHover = false;
        }
    });

    sb.textfield
        .bind("keyup", function () {

            // Listen for user editing search field
            // by any method e.g. pasting, typing etc.

            // Cancel current/previous search
            if (sb.xhr != null) {
                sb.xhr.abort();
            }
            sb.offset = 0;
            sb.resultCache = {};
            sb.lastChoiceHtml = null;
            sb.lastResultReturned = false;

            sb.frame.removeClass("bad_news");

            if (sb.xhrTimeout != null) {
                clearTimeout(sb.xhrTimeout);
            }

            // Set a timeout for a new search
            var txt = sb.textfield.val();
            var timeo;

            // take a little longer if search string is particularly short
            if (txt.length == 0) {
                return;
            } else if(el.hasClass("instant")) {
                timeo = 100;
            } else if (txt.length <= 3) {
                timeo = 1000;
            } else {
                timeo = 500;
            }

            sb.xhrTimeout = setTimeout(sb.requestData, timeo);
        })
        .bind("mouseup focus", function (e) {

            sb.frame.addClass("hasFocus").css("z-index", parseInt(sb.frame.css("z-index")) + 1);

            if (sb.resultHover) {
                return false;
            }

            if (sb.lastSearchHtml != null) {

                // If user has not selected text contents of textfield
                // return to the last search value. Otherwise, leave contents
                // as they are incase user wants to copy/paste etc.
                sb.lastChoiceHtml = sb.textfield.val();
                sb.textfield.val(sb.lastSearchHtml);
            }


            if ($(">li", sb.resultList).length) {
                sb.resultList.slideDown("fast");
            }
        })
        .blur(function () {

            if (sb.resultHover) {
                return;
            }

            sb.frame.removeClass("hasFocus");

            if (sb.lastChoiceHtml != null) {
                sb.textfield.text(sb.lastChoiceHtml);
            }

            sb.resultList.slideUp("fast");

            sb.frame.css("z-index", 30);
        });

    $(sb).trigger("ready");
}
