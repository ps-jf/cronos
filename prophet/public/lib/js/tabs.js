function tabs( )
{

    /*
     *
     *  Picks up all tabbed container on a page and applies
     *  the necessary events.
     *
     */

    $("div.tabs:not(.inited)").each(function( ){

        // tabbed container's component parts
        var tabs = $(this).children("ul")[0];
        var content = $(this).children("div")[0];

        $(this).addClass("inited");

        $( tabs ).children( "li" ).click( function( ){
        
            /*
             * # A tab is clicked, switch content and style container
             */
        
            // escape if already selected
            if( $(this).is( ".active" ) || $(this).is( ".inline_obj" ))
                return false;

            // switch selected tab
            $( tabs ).children( "li.active" ).removeClass("active");
            $( this ).addClass( "active" );

            // determine the index of clicked tab in ul
            var t   = null;
            var idx = null;
            for( i=0; i<$(tabs).children("li").length; i++ )
            {
                t = $(tabs).children("li")[i];
                if( t == this )
                {
                    idx = i;
                }
            }


            // switch to corresponding content div
            $( content ).children("div.active").removeClass("active");

            var c = $( content ).children("div")[idx];
            
            if( $(this).attr("href") ){
                $( c ).load( $(this).attr("href") );
            } // for loading content on click
            
            $(c).addClass( "active" );

            //$(c).find("table.scroll").map(function(){ $(this).trigger('resize'); });
            $( c )
                .find( ".has_onshow:visible" )
                .trigger( "onshow" );

        });

    });
}
