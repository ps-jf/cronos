function ProphetNotification ( configObj ){

    /*
     */

    var self = $("<div/>")
        .addClass("notification")
        .bind( "close", function(){
            self.fadeOut(
                "fast", function(){
                    self.remove();
                }
            )
        });

    self.apply = function( configObj ){

        if( 'type' in configObj ){
            self.addClass( configObj.type );
        }

        if( 'text' in configObj ){
            self.html( configObj.text );
        }

        if( 'onclick' in configObj ){
            self
                .css( "cursor", "pointer" )
                .click( function(){
                    configObj.onclick(self);
                });
        }
    };

    $("#ps-toaster").append( self );

    if( configObj == undefined || !/^function Object/.test( configObj.constructor.toString()) ){
        configObj = {};
    }

    self.apply(configObj);

    self.fadeIn( 
        "fast",
        function( ){

            if( 'timeout' in configObj ){

                var t = parseInt(configObj.timeout);

                if( t !== NaN ){
                    self
                        .delay( t )
                        .fadeOut( "fast", function(){ self.remove(); } )
                }
            }
        }
    );

    return self;
}

//Function used for copying strings to clipboard
function copyAddress(element){

    var temp = $("<textarea id='copyme'>");
    $("body").append(temp);
    var newel = element.replace(",", "\r\n");
    //Creates a temporary textarea adds the element to it
    temp.val(newel);
    temp.select();
    //Uses the selector
    document.execCommand("copy");
    temp.remove();
}


function mergeConfigObj( baseConfig, customConfig ){

    /*
     Merge 2 objects by overriding the values of the 
     first parameter with those of the second (if defined).
     Return the merged version of baseConfig
    */
    if( !/^function Object/.test(baseConfig.constructor.toString()) ){
        return false;
    }

    if( customConfig == undefined || !/^function Object/.test(customConfig.constructor.toString()) ){
        return baseConfig;
    }

    for( key in baseConfig ){
        if( key in customConfig ){
            baseConfig[key] = customConfig[key];
        }
    }  
    
    return baseConfig;
}

function saveProfileStatusSpan( form, settings ){

    form = $(form);

    var config = {
        before: function( ){ return true; },
        success: function( ){},
        successText: "Successfully Saved"        
    };

    config = mergeConfigObj( config, settings );

    var statusSpan = $("span.status:last",form);

    form.bind( 
        "submit",
        function( e, force ){

            if( form.xhr != null ){
                form.xhr.abort();
            }

            if( !force && !config.before() ){
                return false;
            }

            form.xhr = $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: form.mySerialize(),
                dataType: "json",
                beforeSend: function(){
                    $(":submit",form)
                        .attr("disabled",true);
                },
                success: function( r ){
                    try {
                        if( r.status ){
                            statusSpan
                                .text( config.successText )
                                .removeClass( "bad_news" )
                                .addClass( "good_news" );

                            // call custom callback function
                            config.success( r );
                            form.trigger("successfulSave");
                            
                        } else {
                            statusSpan
                                .removeClass( "good_news" )
                                .addClass( "bad_news" )
                                .text( r.error );

                            form.trigger("failedSave");
                        }

                    } catch( e ) {
                        handleSystemError( e );
                    }
                },
                complete: function( r ){

                    setTimeout( function(){
                        $(":submit",form).attr("disabled",false);
                    }, 2000 );
                }
            });

            return false;
        }
    );
}

function saveProfileNotification(form, settings) {

    /*
      Prepend a hidden notification bar to the form arg which will be
      displayed when input fields within the form are edited. Clicking
      on the bar will submit the form and change according to the response.
     */

    var config = { 
        orientation: "top",     //, "bottom"
        always_show: false,     //, true
        success: function( ){} //, callback
    };

    if (settings !== undefined && settings.constructor.toString().indexOf("function Object") !==- 1) {
        config = mergeConfigObj(config, settings);
    }

    var saveBar = $("<div/>")
        .addClass("notification save_profile static");

    (config.orientation === "bottom") ? saveBar.appendTo(form) : saveBar.prependTo(form);

    if (config.always_show) {
        saveBar.show();
    } else {
        $(form).bind("change keyup", function(){
            if(saveBar.is(":visible"))
                return;

            saveBar
                .text("Click here to save changes...")
                .slideDown("fast")
                .removeClass("good_news bad_news");

            $(form).trigger("save-span.open");
        });
    }

    saveBar.click(function() {

        if (saveBar.is(".good_news")) {
            return;
        }

        if (saveBar.xhr != null) {
            saveBar.xhr.abort();
        }

        saveBar.xhr = $.ajax({
            'url': $(form).attr("action"),
            'type': $(form).attr("method"),
            'data': $(form).mySerialize(),
            'dataType': "json",
            'success': function(r) {
                try {
                    if (r.status) {
                        // changes successfully saved
                        saveBar
                            .removeClass("bad_news")
                            .addClass("good_news")
                            .attr("saved_id", r.id)
                            .text("Changes saved");

                        if (!config.always_show) {
                            saveBar
                                .delay( 2000 )
                                .slideUp("fast");
                        }

                        setTimeout(config.success, 2000);
                        form.trigger("successfulSave");

                        if ('data' in r && r.data && 'trigger' in r.data && r.data.trigger){
                            if(r.data.trigger === "alert"){
                                alert(r.data.data);
                            } else {
                                form.trigger(r.data.trigger, [r.data.data]);
                            }
                        }

                    } else {
                        // an error occurred
                        saveBar
                            .removeClass("good_news")
                            .addClass("bad_news")
                            .html("<b>Error:</b> " + r.error + " &nbsp;&nbsp;<i>( Click to retry )</i>");

                        form.trigger("failedSave");
                    }

                } catch( e ) {
                    handleSystemError( e );
                }
            }
        });
    });
}

function handleSystemError(e) {

    var n = new ProphetNotification({
        type: "system_error",
        text: "<b>System Error:</b> " + e
    });

    n.apply({
        click: function( ){
            $(n).trigger("close");
        }
    });
}

function pluralize(count, noun, plural = noun + 's'){
    if (count > 1){
        return plural;
    } else {
        return noun;
    }
}

