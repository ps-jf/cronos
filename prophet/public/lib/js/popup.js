function getHighestZindex() {
    var index_highest = 0;
    $(".popup").each(function () {
        // always use a radix when using parseInt
        var index_current = parseInt($(this).css("zIndex"), 10);
        if (index_current > index_highest) {
            index_highest = index_current;
        }
    });

    if (index_highest <= 50) {
        index_highest = 50;
    }

    return index_highest;
}

function position_mini_popups() {
    var w = 2;

    $("table.popup tbody td.content:hidden").each(function () {

        var p = $(this).closest("body>table.popup");
        $(p)
            .css("top", $(window).height() + scrolled().y - $(p).height() - 5)
            .css("left", w)
            .css("z-index", getHighestZindex() + 1);
        w += ($(p).width() + 2)
    });
}

function popup(settings) {
    // A popup overlay
    this.width = this.height = 0;
    this.x = this.y = 0;

    var self = $("<table class='popup'></table>")[0];
    $(self).attr("cellspacing", "0");

    $(".popup").removeClass("foreground");
    $(self).addClass("foreground").css("z-index", getHighestZindex() + 1);

    // bring to the front of other popup overlays when clicked
    $(self).mousedown(function () {
        $(".popup").removeClass("foreground");
        $(this).addClass("foreground").css("z-index", getHighestZindex() + 1);

        if ($(document.body).find(".popup:last").get(0) != this) {
            $(document.body).append(this);
        }
    });

    $(self).append(
        "<thead class='tools'>" +
        "<tr>" +
        "<th class='title'></th>" +
        "<td>" +
        "<button class='help_guide' style='display: none;'></button>" +
        "<button class='toggle'></button>" +
        "<button class='maximize' style='display: none;'></button>" +
        "<button class='close'></button>" +
        "</td>" +
        "</tr>" +
        "</thead>"
    );

    self.setTitle = function (title) {
        /** Set the Popup's title **/
        $("thead th.title:first", self).html(title);
    };

    self.applySettings = function (obj) {
        for (name in obj) {
            switch (name) {
                case "center":
                    $(self).center();
                    setTimeout(function() {
                            $(self).center();
                        }, 250);
                    break;
                case "title":
                    self.setTitle(obj.title);
                    break;
                case "url":
                    $(self.content).load(obj.url);
                    break;
                case "content":
                    $(self.content).html(obj.content);
                    break;
                case "triggers":
                    for (event in obj.triggers) {
                        $(self).bind(event, obj.triggers[event]);
                    }
                    break;
            }
        }
    };

    self.buttons = {
        'mini': $("button.toggle", self),
        'maxi': $("button.maximize", self),
        'close': $("button.close", self)
    };

    $(self.buttons.mini).click(function () {
        $(self.content).toggle();

        if ($(self.content).is(":visible")) {
            $(window).unbind("scroll resize", position_mini_popups);
            $(self).removeClass("mini").center();
        } else {
            $(self).addClass("mini");
            $(window).bind("scroll resize", position_mini_popups);
        }

        position_mini_popups();
    });

    $(self.buttons.maxi).click(function () {
        $(".nav-bc").html("");
        $("#content_window").html(self.content);
        self.close();
    });

    $(self.buttons.close).click(function () {
        $(self).trigger("close");
        self.close();
    });

    // drag-n-drop popup when mousedown on toolbar
    $(self).mousedown(function (event) {
        if ($(self.content).is(":hidden")) {
            // can't drag-drop if minimized
            return false;
        }

        var mouse = mouseCoords(event);
        self.x = (scrolled().x + mouse.x) - $(self).offset().left;
        self.y = (scrolled().y + mouse.y) - $(self).offset().top;

        // respond to dragging by repositioning
        $(document).bind('mousemove', function (event) {

            var mouse = mouseCoords(event);

            var x = ((scrolled().x + mouse.x) - self.x);
            var y = ((scrolled().y + mouse.y) - self.y);

            // snap at sides
            if (x < 0) x = 0;
            if (y < 0) y = 0;

            $(self).css("left", x).css("top", y);
        });

        // respond to dropping by preventing further drag
        $(document).bind('mouseup', function () {
            $(self).removeClass("up");
            $(document).unbind('mousemove').unbind('mouseup');
        });
    });


    $(self).append(
        "<tbody>" +
        "<tr>" +
        "<td colspan='2' class='content'>&nbsp;</td>" +
        "</tr>" +
        "</tbody>"
    );
    self.content = $("td.content", self)[0];

    // append popup to page
    $(document.body).append(self);
    $(self).center();

    // Prevent drag-and-drop action when interacting with tbody content
    $(self).ajaxComplete(function (event) {
        $("*", self.content).mousedown(function (event) {
            event.stopPropagation();
        });
    });

    self.close = function () {
        /** Close Popup **/
        $(self).fadeOut('fast', function () {
            $(this).remove();
            setTimeout(position_mini_popups, 150);
        });
    };

    $(self).bind("close", self.close);

    if (settings !== undefined) {
        self.applySettings(settings);
    }

    return self;
}

function mouseCoords(ev) {
    if (!moz && (ev.pageX || ev.pageY)) {
        return {
            x: ev.pageX,
            y: ev.pageY
        };
    }

    return {
        x: ev.clientX + document.body.scrollLeft + document.body.clientLeft + window.pageXOffset,
        y: ev.clientY + document.body.scrollTop + document.body.clientTop + window.pageYOffset
    }
}
