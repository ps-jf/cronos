/*

    @ Build a context menu from JSON
    @ Daniel Ness - 2009-09-03

    Context( x )
    x = -- JSON --
        [

            --item 1-- {
                'label'    : item label
                'function' : custom action to perform on click
                'menu'     : submenu object
            },

            --item n-- {
                ...
            }

        ]
    
	
*/

function Context( menu, event )
{

    var self = this;


    /*** Parse context's functions ***/
    self.actions = {};
    if ( "actions" in menu ) {
        for ( x in menu.actions ) {
            eval( "self.actions."+ x +" = " + menu.actions[ x ] );
            
        }
    }
    
    
    function Menu( menu ) {
        
        var ul = $("<UL></UL>")[0];
	    var li;


        ul.ensure_visible = function ( ) {

            /* 
               Make sure that this Menu is entirely visible
               and doesn't flow offscreen 
            */

            var win_h = $(window).height( ) + window.pageYOffset;
            var win_w = $(window).width( ) + window.pageXOffset;

            if ( ( $(this).offset().top + $( this ).height( ) ) > win_h ) { // bottom is below window

                var menu_h = $(this).height( );             // height of Menu
                var overflo;                                             // amount of overflow
                var menu_y = $(this).offset().top;          // current y position of Menu
                var final_menu_y;                                  // final y position of Menu
                var BUFFER = 10;            

                overflo = menu_h - ( win_h - menu_y );     // calculate overflow 
                final_menu_y = ( overflo + BUFFER ) * -1;  // recalculate y relative to parent

                $( this ).css( "top", final_menu_y + "px" );
            }

            // *** Ensure Menu is not flowing off the right-side of the screen ***
            if ( ( $(this).offset().left + $(this).width( ) ) > win_w ) {
                $(this).css( "left", ( ( $(this).width( ) ) * -1 ) +"px" );  // ... and flip left
            }

        };

        for( var i=0; i < menu.items.length; i++ ) {
            
            var item = menu.items[ i ];

            li = $( "<li>" + item.label + "</li>" )[ 0 ];  // create a new item for each property in menu

            if( 'menu' in item ) {
                
                // a little flag to show this menu has a submenu
                $( li ).prepend( $( "<div class='arrow'>&rsaquo;</div>" ) );

                // initiate a nested menu within this menu item
                var submenu = new Menu( item.menu );

                // when cursor touches menu item, display / configure submenu
                li.submenu = submenu;               
                
                $( li ).mouseenter(
                    function( ) {                        
                        
                        //  hide all other submenus
                        $( this ).parent().find( "li ul" ).not( this.submenu ).hide();

                        // make submenu appear flush to the right-hand-side of this menu item
                        $( this.submenu ).css( "left", $( ul ).width() );
                        
                        $( this.submenu ).fadeIn( 'fast',
                            function( ) {

                                $( this )[0].ensure_visible( );
                                
                                var self = this;
                                
                                // if cursor leaves submenu, start fadeout countdown ...
                                $( this ).mouseout( 
                                    function(){

                                        this.to = setTimeout( 
                                            function( ) {

                                                $( self ).hide( );
                                                $( self ).find( "ul" ).hide( );
                                            }, 1750
                                        ); 
                                    } );
                                
                                // ... however, if cursor returns to submenu, cancel timeout
                                $( this ).mousemove( function(){ clearTimeout( this.to ); } );
                            }
                        );
                    }
                );

                $( li ).append( submenu );

                // reposition submenu to ensure that it doesn't overflow from the screen
                
            }
            else {
                
                $( li ).mouseenter(
                    function( ) {
                        $( this ).parent( ).find( "li ul" ).hide();
                    }
                );
            }


            if( 'disabled' in item ) {
                
                $( li ).addClass( "disabled" );
            }
            
            if( 'action' in item ) {
                
                li.action = item.action;

                $( li ).click(
                    function( event ) { 

                        event.stopPropagation( ); // no bubbling!
                        $( context ).mouseout( );
                        eval( "self.actions." + this.action + ";" );
                    }
                );
                $( li ).addClass( "has_action" );
            }
            
            if( 'href' in item ) {

                li.href = item.href;

                if ('external' in item){
                    li.external = item.external;
                }

                $( li ).click( 
                    function( event ) {
                                                
                        event.stopPropagation();
                        $( ul ).closest( ".context" ).mouseout( );

                        if (this.external){
                            window.location.replace(this.href);
                        } else {
                            new popup({ 'url': this.href });
                        }
                    }
                );
                
                $( li ).addClass( "has_action" );
                
            }
            else
                $( li ).click( function( event ){ event.stopPropagation( ); } ); // do nothing

            $( ul ).append( li );
        }

        return ul;                    
    }

    var context = $( "<div class='context'><div></div></div>" )[0];

    // determine position on-screen
    var mouse = mouseCoords( event );
    $( context ).css( 'left', ( mouse.x ) + "px" );
    $( context ).css( 'top', ( mouse.y ) + "px" );

    // don't kill the menu until user has touched it
    context.kill = function( ) {
        $( context ).remove( );
    };
    
    $( context ).mouseenter( function() {
        
        if( 'to' in context ) {
            clearTimeout( context.to );
        }
        else {
            
            // ... now you can kill it
            $( context ).mouseleave( 
                function() {
                    context.to = setTimeout( context.kill, 500 );
                }
            );
        }
    });
    
    var main_menu = new Menu(menu);
    $( context ).append( main_menu );
    $( document.body ).append( context );
    main_menu.ensure_visible( );
    
}
