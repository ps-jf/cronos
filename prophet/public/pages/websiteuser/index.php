<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_REQUEST["id"])) {
    $user = new WebsiteUser($_REQUEST["id"], true);

    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "save":
                // *** creating a new user / saving changes to an existing one ***
                $json = ["status"=>0];
                
                $new_user = ( !(bool)$_REQUEST["id"] );
                $json["new_user"] = $new_user;

                $user = new WebsiteUser;

                // get current user entry if existing
                if (!$new_user) {
                    $user->get($_REQUEST["id"]);
                }

                // load in the new data
                $user->load($_POST);

                $passwd = passwd(); // make an account password

                if ($user->save()) {
                    $json["user"] = $user->id();

                    if ($new_user) {
                        // generate a new password reset token for user
                        $pwd_reset = new DatabaseTokenRepository();
                        $token = $pwd_reset->create($this);

                        if ($token) {
                            // email a notification to the user with their login details
                            $body = Template("websiteuser/new_user.txt", ["user"=>$user,"password"=>$token]);
                            $emailed = mymail($user->email_address(), "Your Mypsaccount is ready", "$body");

                            if ($emailed) {
                                // success, bail!
                                $json["status"] = 1;
                                die(json_encode($json));
                            } else {
                                $json["error"] = "Account created but user could not be notified";
                            }
                        } else {
                            $json["error"] = "Could not set a password for new user";
                        }
                    } else {                         // existing user has been updated
                        $json["status"] = 1;
                    }
                } else {
                    $json["error"] = "Could not ".( ( $new_user ) ? "create" : "update" )." user";
                }

                die(json_encode($json));

            break;
            
            case "reset":
                if (!$user->get()) {
                    throw new Exception("User not found");
                } else {
                    // generate a new password reset token for user
                    $pwd_reset = new DatabaseTokenRepository();
                    $token = $pwd_reset->create($user);
                }

                $json = (object) []; // prepare array for json response

                try {
                    $recipient[]['address'] = ['email' => $user->email_address()];
                    $subject = "Mypsaccount password recovery";
                    $from_name = "Policy Services Ltd - Partner Support";
                    $sender_email = "partnersupport@policyservices.co.uk";
                    $sender = ['name' => $from_name, 'email' => $sender_email];

                    // build up main content of email body
                    $content_string = nl2br(strval(WebsiteUser::get_template("reset.txt", ["user" => $user, "password" => $token])));

                    (date('G') <= 11) ? $greeting = "Good Morning," : $greeting = "Good Afternoon,";

                    $body = json_encode([
                        "SUBJECT" => $subject,
                        "FROM_NAME" => $from_name,
                        "SENDER" => $sender_email,
                        "REPLY_TO" => $sender_email,
                        "ADDRESSEE" => $greeting,
                        "CONTENT_STRING" => $content_string
                    ]);

                    // attempt email with ol' sparky
                    if (!sendSparkEmail($recipient, $subject, 'generic-partner', $body, $sender)) {
                        throw new Exception("Could not email password to user via sparkpost");
                    }

                    $json->status = true;
                } catch (Exception $e) {
                    $json->status = false;
                    $json->error = $e->getMessage();
                }

                die(json_encode($json));

                break;

            case "view_permissions":
                $permission_desc = [
                    "ACC_EDIT" => "Account Edit",
                    "ACC_EDIT_BANK_DETAILS" => "Account Edit, Bank Details",
                    "ACC_USER_ADMIN" => "Account User Admin",
                    "INBOX" => "Inbox",
                    "CLI_MERGE" => "Client Merge",
                    "CLI_CASHFLOW" => "Client Cashflow",
                    "CLI_VIEW" => "Client View",
                    "CLI_EDIT" => "Client Edit",
                    "COMM_PIPELINE" => "Income Pipeline",
                    "COMM_PAID" => "Income Paid",
                    "COMM_EXPORT" => "Income Export",
                    "POL_MERGE" => "Policy Merge",
                    "POL_CASHFLOW" => "Policy Cashflow",
                    "REQ_VAL" => "Request Valuations",
                    "NEW_BUS" => "New Business",
                    "TAG_ADMIN" => "Tag Admin",
                    "UPLOAD_SHOW" => "Show File Storage",
                    "UPLOAD_STORE" => "Save to File Storage",
                    "UPLOAD_DESTROY" => "Delete From File Storage"
                ];

                if (isset($_GET['template'])) {
                    $user_id = $_REQUEST["id"];

                    $s = new Search(new ViewerEntry());
                    $s->eq("id", $user_id);

                    if($ve = $s->next(MYSQLI_ASSOC)){
                        $oldPerms = [];

                        $s = new Search(new PermissionsAudit());
                        $s->eq("viewer_entry", $user_id);
                        $s->add_order("updated_when", "DESC");

                        while($pa = $s->next(MYSQLI_ASSOC)){
                            $oldPerms[] = $pa;
                        }
                    } else {
                        trigger_error("No viewer entry found for id " . $user_id);
                    }

                    echo Template(WebsiteUser::get_template_path("user_permissions.html"), ["partner"=>$_GET['partnerID'], "websiteuser"=>$user_id, "viewerentry"=>$ve, "oldperms"=>$oldPerms, "permission_desc"=>$permission_desc]);
                } else {
                    $json = [
                        "allowed_permissions" => null,
                        "denied_permissions" => null,
                        "data" => null
                    ];

                    $user_id = $_REQUEST["id"];

                    //get viewer_tbl id
                    $db = new mydb(REMOTE, E_USER_WARNING);
                    if (mysqli_connect_errno()) {
                        $json['data'] = "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                        echo json_encode($json);
                        exit();
                    }
                    $q = "SELECT CAST(permissions_array AS CHAR(10000) CHARACTER SET utf8) as 'permissions', access_level FROM ".REMOTE_SYS_DB.".viewer_tbl where id = ".$user_id;
                    $db->query($q);

                    while ($d = $db->next(MYSQLI_ASSOC)) {
                        $permissions = $d['permissions'];
                        $access_level = $d['access_level'];
                    }

                    // if permission array is null, get default permissions based on access level
                    if ($permissions == null) {
                        if ($access_level == "USER") {
                            $permissions = '["ACC_EDIT","INBOX","CLI_VIEW","CLI_EDIT","COMM_PIPELINE","COMM_PAID","NEW_BUS", "REQ_VAL", "UPLOAD_SHOW", "UPLOAD_STORE", "UPLOAD_DESTROY"]';
                        } elseif ($access_level == "ADMIN") {
                            $permissions = '["ACC_EDIT","ACC_EDIT_BANK_DETAILS","ACC_USER_ADMIN","INBOX","CLI_MERGE","CLI_CASHFLOW","CLI_VIEW","CLI_EDIT","COMM_PIPELINE","COMM_PAID","COMM_EXPORT","POL_MERGE","REQ_VAL","NEW_BUS","TAG_ADMIN", "UPLOAD_SHOW", "UPLOAD_STORE", "UPLOAD_DESTROY"]';
                        }
                    }

                    //remove square brackets
                    $permissions = str_replace(['[', ']'], '', $permissions);
                    //remove double quotation marks
                    $permissions = str_replace('"', "", $permissions);
                    //remove single quotation marks
                    $permissions = str_replace("'", "", $permissions);
                    //explode to array
                    $permissions_array = explode(',', $permissions);

                    //set up arrays
                    $allow_desc = [];
                    $deny_desc = [];

                    //for each user permission, get description
                    foreach ($permissions_array as $item) {
                        if (array_key_exists($item, $permission_desc)) {
                            $allow_desc[] = $permission_desc[$item];
                        }
                    }

                    //get permissions that are not permitted
                    $deny_desc = array_diff($permission_desc, $allow_desc);

                    foreach ($allow_desc as $item) {
                        $json['data'] .= "<tr><td>".$item."</td><td><img class='permission_image' src='/lib/images/tick.png'><input type='checkbox' checked='checked' id='".$item."'/></td></tr>";
                    }
                    foreach ($deny_desc as $item) {
                        $json['data'] .= "<tr><td>".$item."</td><td><img class='permission_image' src='/lib/images/cross.png'><input type='checkbox' id='".$item."'/></td></tr>";
                    }

                    echo json_encode($json);
                }

                break;

            case "edit_permissions":
                $json = [
                    "status" => null,
                    "error" => false
                ];

                $permission_desc = [
                    "ACC_EDIT" => "Account Edit",
                    "ACC_EDIT_BANK_DETAILS" => "Account Edit, Bank Details",
                    "ACC_USER_ADMIN" => "Account User Admin",
                    "INBOX" => "Inbox",
                    "CLI_MERGE" => "Client Merge",
                    "CLI_CASHFLOW" => "Client Cashflow",
                    "CLI_VIEW" => "Client View",
                    "CLI_EDIT" => "Client Edit",
                    "COMM_PIPELINE" => "Income Pipeline",
                    "COMM_PAID" => "Income Paid",
                    "COMM_EXPORT" => "Income Export",
                    "POL_MERGE" => "Policy Merge",
                    "POL_CASHFLOW" => "Policy Cashflow",
                    "REQ_VAL" => "Request Valuations",
                    "NEW_BUS" => "New Business",
                    "TAG_ADMIN" => "Tag Admin",
                    "UPLOAD_SHOW" => "Show File Storage",
                    "UPLOAD_STORE" => "Save to File Storage",
                    "UPLOAD_DESTROY" => "Delete From File Storage"
                ];

                $valid_permissions = [];

                # get key for each of the requested permissions
                foreach ($permission_desc as $key => $value) {
                    if (in_array($value, $_GET['data'])) {
                        $valid_permissions[] = $key;
                    }
                }

                # create comma separated string from array
                $permissions = '"'.implode('","', $valid_permissions).'"';
                # add square brackets to permissions list
                $permissions = '['.$permissions.']';

                // update viewer_tbl with specified permission level
                $s = new Search(new ViewerEntry);
                $s->eq("id", $_GET['id']);
                $v = $s->next();

                $pa = new PermissionsAudit();
                $pa->viewer_entry($v->id());
                $pa->user($v->user());
                $pa->partner($v->partner());
                $pa->permissions($v->permissions());
                $pa->access_level($v->access_level());
                $pa->email_notification($v->email_notification());
                $pa->access_revoked($v->access_revoked());

                $v->access_level("Custom");
                $v->permissions->set([]);
                $v->permissions($permissions);
                if ($v->save()) {
                    if ($pa->save()){
                        $json['status'] = "Permissions successfully updated";
                    } else {
                        $json['status'] = "Permissions successfully updated but failed to audit changes";
                    }
                } else {
                    $json['error'] = true;
                    $json['status'] = "Permissions failed to update, please try again or contact IT.";
                }
                
                echo json_encode($json);

                break;

            case "remove_2fa":
                $response = [
                    "success" => true,
                    "data" => null
                ];

                try{
                    $user->google2fa_secret->set(null);

                    $user->save();

                    $response['data'] = strval(Boolean::represent($user->google2fa_secret));
                } catch (Exception $e) {
                    $response['success'] = false;
                    $response['data'] = $e->getMessage();
                }

                echo json_encode($response);

                break;
        }
    }
} else if (isset($_GET['partnerID'])) {
    $permission_desc = [
        "ACC_EDIT" => "Account Edit",
        "ACC_EDIT_BANK_DETAILS" => "Account Edit, Bank Details",
        "ACC_USER_ADMIN" => "Account User Admin",
        "INBOX" => "Inbox",
        "CLI_MERGE" => "Client Merge",
        "CLI_CASHFLOW" => "Client Cashflow",
        "CLI_VIEW" => "Client View",
        "CLI_EDIT" => "Client Edit",
        "COMM_PIPELINE" => "Income Pipeline",
        "COMM_PAID" => "Income Paid",
        "COMM_EXPORT" => "Income Export",
        "POL_MERGE" => "Policy Merge",
        "POL_CASHFLOW" => "Policy Cashflow",
        "REQ_VAL" => "Request Valuations",
        "NEW_BUS" => "New Business",
        "TAG_ADMIN" => "Tag Admin",
        "UPLOAD_SHOW" => "Show File Storage",
        "UPLOAD_STORE" => "Save to File Storage",
        "UPLOAD_DESTROY" => "Delete From File Storage"
    ];

    if ($_GET['new_user']) {
        $json = [
            "status" => null,
            "error" => null,
            "data" => null
        ];

        // minus 1.. don't ask, horrible! but works (purely used for layout)
        $i = -1;
        $json['data'] .= "<tr id='permissions_".$_GET['partnerID']."'>";
        foreach ($permission_desc as $key => $value) {
            if (($i+1)%4 == 0) {
                $json['data'] .= "</tr><tr id='permissions_".$_GET['partnerID']."'>";
            }

            $json['data'] .= "<td><input type='checkbox' id='[".$_GET['partnerID']."][".$key."]' class='".$_GET['partnerID']."' /> &nbsp; ".$value."</td>";

            $i++;
        }

        $json['data'] .= "</tr>";


        echo json_encode($json);
    } elseif ($_GET['submit_user']) {
        $json = [
            "status" => null,
            "error" => false
        ];

        $partner_permissions = [];

        if (isset($_REQUEST['checkbox_array']) && count($_REQUEST['checkbox_array'])){
            foreach ($_REQUEST['checkbox_array'] as $key => $val) {
                // split each item
                $theArray = explode("]", $val);
                // remove all square brackets
                $string = str_replace(['[', ']'], '', $theArray);
                // remove empty items from array
                $string = (array_filter($string));

                // create multi-dimensional array for each partner account with all their permissions
                $partner_permissions[$string[0]][] = $string[1];
            }
        }

        $form_values = [];

        $params = [];
        parse_str($_REQUEST['form_data'], $params);

        (intval($_GET['main_partner'])) ? $partner_id = $_GET['partnerID'] : $partner_id = 0;

        // ensure we can connect to myps server
        $db = new mydb(REMOTE, E_USER_WARNING);
        if (mysqli_connect_errno()) {
            $json['status'] = "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
            $json['error'] = true;
            echo json_encode($json);
            exit();
        }

        $q = "SELECT email FROM " . REMOTE_SYS_DB . ".user_info where email = '" . $params['websiteuser']['email_address'] . "'";
        $db->query($q);

        //if this returns anything, the email address is not unique
        if ($u = $db->next(MYSQLI_ASSOC)) {
            $json['status'] = "Email address '" . $u['email'] . "' already on system, must be unique.";
            $json['error'] = true;
            echo json_encode($json);
            exit();
        }

        $websiteuser = new WebsiteUser();

        $websiteuser->first_name($params['websiteuser']['first_name']);
        $websiteuser->last_name($params['websiteuser']['last_name']);
        $websiteuser->user_name($params['websiteuser']['user_name']);
        $websiteuser->email_address($params['websiteuser']['email_address']);
        $websiteuser->partner($partner_id);
        $websiteuser->allowed_access($_REQUEST['allowed_access']);
        $websiteuser->defunct_email($_REQUEST['defunct_email']);

        $websiteuser->save();

        if ($websiteuser->id()) {
            $passwd = passwd(); // make an account password

            // generate a new password reset token for user
            $pwd_reset = new DatabaseTokenRepository();
            $token = $pwd_reset->create($websiteuser);

            if ($token) {
                // email a notification to the user with their login details
                $body = json_encode(['PASSWORD' => $token]);

                $receiver = ['name' => $websiteuser->first_name() . " " . $websiteuser->last_name(), 'email' => $websiteuser->email_address()];
                $sender = ['name' => "agencysupport@policyservices.co.uk", 'email' => "agencysupport@policyservices.co.uk"];

                if (sendSparkEmail($receiver, "Your Mypsaccount is ready", "new-myps-user", $body, $sender)) {
                    // success, this message is for testing purposes, alternative return will be decided in below code
                    $json["status"] = "Email successfully sent, user notified of new account";
                } else {
                    $json["error"] = "Account created but user could not be notified";
                    die(json_encode($json));
                }
            }

            if ($partner_id != 0) {
                $json["status"] = "Partner now set up for mypsaccount, account email sent.";
            } else {
                // initiate counters
                $i = 0;
                $j = 0;

                foreach ($params as $key => $value) {
                    if (is_numeric($key)) {
                        if ($value == "User") {
                            $permissions = '["ACC_EDIT","INBOX","CLI_VIEW","CLI_EDIT","COMM_PIPELINE","COMM_PAID","NEW_BUS", "REQ_VAL", "UPLOAD_SHOW", "UPLOAD_STORE", "UPLOAD_DESTROY"]';
                        } elseif ($value == "Admin") {
                            $permissions = '["ACC_EDIT","ACC_EDIT_BANK_DETAILS","ACC_USER_ADMIN","INBOX","CLI_MERGE","CLI_CASHFLOW","CLI_VIEW","CLI_EDIT","COMM_PIPELINE","COMM_PAID","COMM_EXPORT","POL_MERGE","REQ_VAL","NEW_BUS","TAG_ADMIN", "UPLOAD_SHOW", "UPLOAD_STORE", "UPLOAD_DESTROY"]';
                        } elseif ($value == "Custom") {
                            foreach ($partner_permissions as $k => $v) {
                                $permissions = '"' . implode('","', $v) . '"';
                            }

                            $permissions = '[' . $permissions . ']';
                        }

                        // add entry into viewer_tbl for each partner account with specified permission level
                        $v = new ViewerEntry();
                        $v->user($websiteuser->id());
                        $v->partner($key);
                        $v->access_level($value);
                        $v->permissions->set([]);
                        $v->permissions($permissions);
                        $v->email_notification($_REQUEST['email_notifications']);
                        if ($v->save()) {
                            $j++;
                        }
                        $i++;
                    }
                }

                // check that number of accounts submitted matches number of entries added to viewer_tbl
                if ($i == $j) {
                    $json["status"] = "Entry added to viewer_tbl for each specified partner account.";
                } else {
                    $json["status"] = "Entry added to User_info, not all entries added to viewer_tbl. Contact IT.";
                    $json["error"] = true;
                }
            }
        } else {
            $json["status"] = "Entry not added to user_info table, Contact IT.";
            $json["error"] = true;
        }

        echo json_encode($json);
        exit();
    }
} else {
    $search = Search(new WebsiteUser);
    $search
        ->flag("id", Search::DEFAULT_INT)
        ->flag("e", "email_address", Search::DEFAULT_STRING);

    if (isset($search)) {
        $x = $search->get_response_array(
            $_GET['search'],
            function ($object) {
                return $object->email_address();
            }
        );

        echo json_encode($x);
    }
}
