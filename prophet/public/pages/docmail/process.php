<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

$dm = new DocMail();

/*
 * NOTES :
 * call ProcessMailing  with Submit=0 PartialProcess=1 to approve the proof and submit the mailing
 * Poll GetStatus to check that proof is ready (loop)  'Mailing submitted', 'Mailing processed' or 'Partial processing complete' mean the proof is ready
 * call GetProofFile to return the proof file data
 * call ProcessMailing with Submit=1 PartialProcess=0 to approve the proof and submit the mailing
 */

$return = [
    'error' => false,
    'feedback' => '',
];

// "Your reference" for this mailing (information)
$sMailingName = date('Y-m-d H:i:s')." ".User::get_default_instance('myps_user_id');
$sMailingDescription = "Mailing via Prophet";

if (isset($_GET['client_id'])) {
    $c = new Client($_GET['client_id'], true);
    if (isset($_GET['c_index'])) {
        $index = $_GET['c_index'];
        $index_int = ($_GET['c_index'] === 'one') ? 1 : 2;
        $addressee_array = [
            'fullname' => $c->$index,
            'title' => $c->$index->title(),
            'fname' => $c->$index->forename(),
            'sname' => $c->$index->surname(),
        ];
    } else {
        $addressee_array = [
            'fullname' => $c,
        ];
    }

    $address_array = [
        'line1' => $c->address->line1,
        'line2' => $c->address->line2,
        'line3' => $c->address->line3,
        'line4' => $c->address->postcode,
    ];

    if ($_GET['process'] === 'portal_add') {

        // check if user exists
        $s = new Search(new ClientPortalUser());
        $s->eq('client_id', $_GET['client_id']);
        $s->eq('index', $index_int);
        if ($s->next(MYSQLI_ASSOC)) {
            $return['error'] = true;
            $return['feedback'] = 'User already exists';
            echo json_encode($return);
            exit();
        }

        // ensure ambiguous characters are not included
        $permitted_chars = '2346789ABCDEFGHJKMNPQRTWXYZ';
        $activation_code = substr(str_shuffle($permitted_chars), 0, 8);
        $db_version = password_hash($activation_code,PASSWORD_DEFAULT);

        $portal = new ClientPortalUser();
        $portal->activation_code($db_version);
        $portal->client_id($_GET['client_id']);
        $portal->index($index_int);
        if (!$portal->save()) {
            $return['error'] = true;
            $return['feedback'] = 'Error adding user to database';
            echo json_encode($return);
            exit();
        }

        // add dash in middle to make it easier to read
        $custom_array = [
            'custom1' => implode("-", str_split($activation_code, 4)),
        ];
    }

    $address = array_merge($addressee_array, $address_array, $custom_array ?? []);
}

if (isset($_GET['hosted_template'])) {

    $mailing = json_decode($dm->CreateMailing($sMailingName, $sMailingDescription));

    if ($mailing->MailingGUID) {

        if (isset($address_array)) {
            // mailing created, lets add an address
            $address = json_decode($dm->AddAddress($mailing->MailingGUID, $address));
        }

        $template = json_decode($dm->AddTemplateFromLibrary($mailing->MailingGUID, $_GET['hosted_template']));

        if ($template->TemplateGUID) {
            if ($_GET['proof']) {
                $pm = json_decode($dm->ProcessMailing($mailing->MailingGUID, true));
                $proof_location = $dm->GetProofFile($mailing->MailingGUID);
                // present proof to user for approval
                $return['feedback'] = $proof_location;
                echo json_encode($return);
                exit();
            }

            // no proof required, submit mailing
            $sm = json_decode($dm->ProcessMailing($mailing->MailingGUID));
            $return['feedback'] = 'Mailing has been submitted';
            echo json_encode($return);
            exit();
        }
    }

} elseif(!empty($_FILES)) {
    // friendly name in docmail for your template file (information)
    $sTemplateFileName = $sTemplateName = "test2.docx";

    // filename (in this case the file is on the root of the webserver!)
    $TemplateFile = "test2.docx";

    // false = Automatically approve the order without returning a proof
    $ProofApprovalRequired = true;

    # todo
    $dm->AddTemplateFile();

} elseif (isset($_GET['display_proof'])) {

    var_dump($_GET['location']);
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Description: File Transfer');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Content-Type: application/pdf');
    header('Accept-Ranges: bytes');
    header('Content-Length: ' . filesize($_GET['location']));
    header('Content-Disposition: inline; filename="' . $_GET['location'] . '"');
    @readfile($_GET['location']);

    echo $_GET['location'];
}