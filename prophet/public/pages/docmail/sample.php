<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

date_default_timezone_set('Europe/London');//assumes  UK Locale - modify to match your location if required

print_object_linebreak(date('r'));
print_object_linebreak("Initialising docmail API script...");

// Flag for outputting debug print messages
$GLOBALS["debug"] = false;
$GLOBALS["fileContentsDebug"] = false;
$GLOBALS["callParamArrayDebug"] = false;
$GLOBALS["resultArrayDebug"] = false;

//construct standard error handler for backwards compatibility
if (!function_exists('error_get_last')) {
    set_error_handler(
        create_function(
            '$errno,$errstr,$errfile,$errline,$errcontext',
            '
					global $__error_get_last_retval__;
					$__error_get_last_retval__ = array(
						\'type\'        => $errno,
						\'message\'        => $errstr,
						\'file\'        => $errfile,
						\'line\'        => $errline
					);
					return false;
				'
        )
    );

    function error_get_last()
    {
        global $__error_get_last_retval__;
        if (!isset($__error_get_last_retval__)) {
            return null;
        }
        return $__error_get_last_retval__;
    }
}


try {

    //Live URL
    $wsdl = "https://www.cfhdocmail.com/testAPI2/DMWS.asmx?WSDL";
    //$wsdl = "https://www.cfhdocmail.com/Test_SimpleAPI/DocMail.SimpleAPI.asmx?WSDL";
    // Setup nusoap client
    $GLOBALS["client"] = new nusoap_client($wsdl, true); //PHP5 now has its own soap client class, so use nusoap_client to avoid clash
    // Increase soap client timeout
    $GLOBALS["client"]->timeout = 240;
    // Increase php script server timeout
    set_time_limit(240);
    error_reporting(E_ALL);

    $sUsr = "kevin.dorrian@policyservices.co.uk";  // docmail username
    $sPwd = "t3h!C2t6h%%Pfkqm8w6k"; // docmail password
    $sMailingName = "test-".time();   // "Your reference" for this mailing (information)
    $sCallingApplicationID = "PHPCodeTemplate";   // could be useful to show your application name in docmail (information)
    $sTemplateName = "test2.docx"; // friendly name in docmail for your template file (information)
    $sTemplateFileName = "test2.docx"; // file name to be passed to docmail for your template file (information)
    $TemplateFile = "test2.docx";           // filename (in this case the file is on the root of the webserver!)
    $bColour = true;                // Print in colour?
    $bDuplex = false;                // Print on both sides of paper?
    $eDeliveryType = "Standard";// <eDeliveryType>Undefined or FirstClass or StandardClass</eDeliveryType> - to get the BEST benefit use StandardClass
    $eAddressNameFormat = "Full Name"; //How the name appears in the envelope address  “Full Name”, “Firstname Surname”, “Title Initial Surname”,“Title Surname”, or “Title Firstname Surname”
    $ProductType = "A4Letter";    //ProductType (on Mailing): “A4Letter”, “BusinessCard”, “GreetingCard”, or “Postcard”
    $DocumentType = "A4Letter";    //DocumentType (on Templates - selects the sub-type for a given template): “A4Letter”, “BusinessCard”,“GreetingCardA5”, “PostcardA5”, “PostcardA6”, “PostcardA5Right” or “PostcardA6Right”

    //used in adding an address list file
    $AddressFile = "test.csv";           // address CSV file filename (in this case the file is on the root of the webserver!)

    //used in adding a single address
    $NameTitle = "Mr.";           //recipient title/salutation
    $FirstName = "Jonny";       //recipient 1st name
    $LastName = "Foley";           //recipient surname
    $sAddress1 = "18 The Cormorant";         // Address line 1
    $sAddress2 = "Alloa";         // Address line 2
    $sAddress3 = "Clackmannanshire";       // Address line 3
    $sAddress4 = "";          // Address line 4
    $sAddress5 = "";
    $sPostCode = "FK101RL";       // PostCode


    $sMailingDescription = "Test mail";
    $bIsMono = !$bColour;
    $sDespatchASAP = true;
    $sDespatchDate = "";
    $ProofApprovalRequired = true; //false = Automatically approve the order without returning a proof

    $EmailOnProcessMailingError = "jonathan.foley@policyservices.co.uk";        //developer email used as an example
    $EmailOnProcessMailingSuccess = "jonathan.foley@policyservices.co.uk";

    $HttpPostOnProcessMailingError = "";     //URL on your server set up to handle callbacks
    $HttpPostOnProcessMailingSuccess = "";   //URL on your server set up to handle callbacks

    $callName = "";

    print_object_linebreak("...done");
    //true  = proof approval required.
    //	call ProcessMailing  with Submit=0 PartialProcess=1 to approve the proof and submit the mailing
    //	Poll GetStatus to check that proof is ready (loop)  'Mailing submitted', 'Mailing processed' or 'Partial processing complete' mean the proof is ready
    //	call GetProofFile to return the proof file data
    //	call ProcessMailing  with Submit=1 PartialProcess=0 to approve the proof and submit the mailing

    print_object_linebreak("<br/><br/><h1>Running Docmail API script</h1>");

    ///////////////////////
    // CreateMailing  - Setup array to pass into webservice call
    ///////////////////////

    $callName = "CreateMailing";
    // Setup array to pass into webservice call
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "CustomerApplication" => $sCallingApplicationID,
        "ProductType" => $ProductType,
        "MailingName" => $sMailingName,
        "MailingDescription" => $sMailingDescription,
        "IsMono" => $bIsMono,
        "IsDuplex" => $bDuplex,
        "DeliveryType" => $eDeliveryType,
        "DespatchASAP" => $sDespatchASAP,
        //"DespatchDate" => $sDespatchDate,   //only include if delayed despatch is required
        "AddressNameFormat" => $eAddressNameFormat,
        "ReturnFormat" => "Text"
    );
    // other available params listed here:  https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=CreateMailing
    $result = MakeCall($callName, $params, "");
    $MailingGUID = GetFld($result, "MailingGUID");//parse the value  for key 'MailingGUID' from $result
    print_object_linebreak("Order Ref: " . GetFld($result, "OrderRef"));

    ///////////////////////
    //Add Single Address   - use this to add a single address by setting up array to pass into webservice call
    ///////////////////////

    $callName = "AddAddress";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "Title" => $NameTitle,
        "FirstName" => $FirstName,
        "Surname" => $LastName,
        "Address1" => $sAddress1,
        "Address2" => $sAddress2,
        "Address3" => $sAddress3,
        "Address4" => $sAddress4,
        "Address5" => $sAddress5,
        "Address6" => $sPostCode,
        "Custom1" => 'AAA-222-CCC',
        "ReturnFormat" => "Text"
    );

    $result = MakeCall($callName, $params, "for MailingGUID " . $MailingGUID);

    if (file_exists($AddressFile)) {
        ///////////////////////
        //Add Address File  - use this to add a file of addresses (in CSV format with a header row) - Read in $AddressFile file data and Setup array to pass into webservice call
        ///////////////////////

        // Load contents of the Address CSV file into base-64 array to pass across SOAP
        // for example the Address CSV file is at the root of the webserver

        $AddressFileHandle = fopen($AddressFile, "rb");
        $AddressFileContents = base64_encode(fread($AddressFileHandle, filesize($AddressFile)));
        fclose($AddressFileHandle);
        if ($GLOBALS["debug"]) {
            print_object_linebreak("address file is " . filesize($AddressFile) . " bytes");
            if ($GLOBALS["fileContentsDebug"]) {
                print_object_linebreak("Contents of file:");
                print_object_linebreak($AddressFileContents);
            }
        }

        $callName = "AddMailingListFile";
        $params = array(
            "Username" => $sUsr,
            "Password" => $sPwd,
            "MailingGUID" => $MailingGUID,
            "FileName" => $AddressFile,
            "FileData" => $AddressFileContents,
            "HasHeaders" => True,
            "ReturnFormat" => "Text"
        );

        $result = MakeCall($callName, $params, "File " . $AddressFile . " (" . filesize($AddressFile) . ")");
    }


    //
    ///////////////////////
    // AddTemplateFile  - Read in $TemplateFile file data and Setup array to pass into webservice call
    ///////////////////////

    // Load contents of word file into base-64 array to pass across SOAP
    // for example the word file is at the root of the webserver

    $TemplateHandle = fopen($TemplateFile, "r");
    $TempeContents_doc = base64_encode(fread($TemplateHandle, filesize($TemplateFile)));
    fclose($TemplateHandle);

    print_object_linebreak("<br><b>Preparing file for AddTemplateFile </b>");

    if ($GLOBALS["debug"]) {
        print_object_linebreak("Template file size is " . filesize($TemplateFile) . " bytes");
        if ($GLOBALS["fileContentsDebug"]) {
            print_object_linebreak("Contents of file:");
            print_object_linebreak($TempeContents_doc);
        }
    }

/*    $callName = "AddTemplateFile";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "DocumentType" => $DocumentType,
        "TemplateName" => $sTemplateName,
        "FileName" => $sTemplateFileName,
        "FileData" => $TempeContents_doc,
        "AddressedDocument" => true,
        "Copies" => 1,
        "ReturnFormat" => "Text"
    );
    // other available params listed here:  https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=AddTemplateFile

    $result = MakeCall($callName, $params, "File " . $TemplateFile . " (" . filesize($TemplateFile) . ")");*/

    $callName = "AddTemplateFromLibrary";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "TemplateName" => 'Sample1',
        "Copies" => 1,
        "ReturnFormat" => "Text"
    );
    // other available params listed here:  https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=AddTemplateFile

    $result = MakeCall($callName, $params, false);

    /*$callName = "AddTemplateFile";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "TemplateName" => 'Sample1',
        "FileName" => 'Sample1.pdf',
        "FileData" => [],
        "Copies" => 1,
        "ReturnFormat" => "Text",
        "DocumentType" => 'A44PageBooklet'
    );
    // other available params listed here:  https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=AddTemplateFile

    $result = MakeCall($callName, $params, false);*/


    ///////////////////////
    // ProcessMailing - Setup array to pass into webservice call
    ///////////////////////
    $callName = "ProcessMailing";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "CustomerApplication" => $sCallingApplicationID,
        "SkipPreviewImageGeneration" => false,
        "Submit" => !$ProofApprovalRequired, //auto submit when approval is not requried
        "PartialProcess" => $ProofApprovalRequired, //fully process when approval is not requried
        "Copies" => 1,
        "ReturnFormat" => "Text",
        "EmailSuccessList" => $EmailOnProcessMailingSuccess,
        "EmailErrorList" => $EmailOnProcessMailingError,
        "HttpPostOnSuccess" => $HttpPostOnProcessMailingSuccess,
        "HttpPostOnError" => $HttpPostOnProcessMailingError
    );
    // there are useful parameters that you may wish to include on this call which enable asynchronous notifications of successes and fails of automated orders to be sent to you via email or HTTP Post:
    //		EmailSuccessList,EmailErrorList
    //		HttpPostOnSuccess,HttpPostOnError
    // other available params listed here:  https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=ProcessMailing

    $result = MakeCall($callName, $params, "");

    //optional wait to confirm the partial processing from ProcessMailing has completed
    sleep(1);
    WaitForProcessMailingStatus($sUsr, $sPwd, $MailingGUID, "Partial processing complete", true);

    //additional calls that may be useful:

    ///////////////////////
    // GetStatus - Setup array to pass into webservice call
    ///////////////////////
    // other available params listed here:  (https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetStatus) returns the status of a mailing from the mailing guid
    $result = GetStatus($sUsr, $sPwd, $MailingGUID);


    ///////////////////////
    // GetProofFile - Setup array to pass into webservice call
    ///////////////////////
    //NOTE:  Status must that the show last "ProcessMailing"	call is complete before a proof can be returned.

    $callName = "GetProofFile";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "ReturnFormat" => "Text"
    );
    //  other available params listed here:  (GetProofFile (https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetProofFile) returns the file data of the PDF proof if it has been generated.
    $result = MakeCall($callName, $params, "");


    // download a copy of proof
    //echo base64_decode($result);
    file_put_contents('testing.pdf', base64_decode($result));


    /*$callName = "GetProofImage";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "ReturnFormat" => "Text",
        "PageNumber" => 0
    );
    //  other available params listed here:  (GetProofFile (https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetProofFile) returns the file data of the PDF proof if it has been generated.
    $result = MakeCall($callName, $params, "");

    // download a copy of proof
    //echo base64_decode($result);
    file_put_contents('testing.png', base64_decode($result));*/

    ///////////////////////
    // ProcessMailing - With "Submit" and "PartialProcess" flags set to APPROVE the mailing - Setup array to pass into webservice call
    ///////////////////////
    $callName = "ProcessMailing";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "CustomerApplication" => $sCallingApplicationID,
        "SkipPreviewImageGeneration" => true,
        "Submit" => true, //auto submit
        "PartialProcess" => false, //fully process
        "MaxPriceExVAT" => 0,
        "Copies" => 1,
        "ReturnFormat" => "Text",
        "EmailSuccessList" => $EmailOnProcessMailingSuccess,
        "EmailErrorList" => $EmailOnProcessMailingError,
        "HttpPostOnSuccess" => $HttpPostOnProcessMailingSuccess,
        "HttpPostOnError" => $HttpPostOnProcessMailingError

    );
    // there are useful parameters that you may wish to include on this call which enable asynchronous notifications of successes and fails of automated orders to be sent to you via email or HTTP Post:
    //		EmailSuccessList,EmailErrorList
    //		HttpPostOnSuccess,HttpPostOnError
    // other available params listed here:  https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=ProcessMailing
    $result = MakeCall($callName, $params, "");

    //optional wait to confirm the processing from ProcessMailing has completed
    WaitForProcessMailingStatus($sUsr, $sPwd, $MailingGUID, "Mailing submitted", true);

    print_object_linebreak(date('r'));
    print_object_linebreak("<br/><br/><h1>FINISHED!</h1>");
} catch (Exception $e) {
    print_object_linebreak(date('r'));
    print_object_linebreak("<br/><br/><h1>PROBLEM:</h1>");
    print_object_linebreak($e->getMessage());
}

//display any system errors
print_object_linebreak(error_get_last());


function MakeCall($callName, $params, $extraInfo)
{

    $callResult = "";
    print_object_linebreak("<br/><b>About to call " . $callName . " " . $extraInfo . "</b>");
    if ($GLOBALS["callParamArrayDebug"]) {
        print_object_linebreak($params);
    }
    $callResult = $GLOBALS["client"]->call($callName, $params);

    if ($GLOBALS["resultArrayDebug"]) {
        print $callName . " RESULT WAS: ";
        print_object_linebreak($callResult);
    }
    if ($GLOBALS["debug"]) {
        print_object_linebreak("<b>Result " . $callName . ": </b>");
        print_object_linebreak($callResult[$callName . "Result"]);
    }
    
    CheckError($callResult[$callName . "Result"]);   //parse & check error fields from result as described above
    flush();

    return $callResult[$callName . "Result"];
}

function GetStatus($sUsr, $sPwd, $MailingGUID)
{

    ///////////////////////
    // GetStatus - Setup array to pass into webservice call
    ///////////////////////
    // other available params listed here:  (https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetStatus) returns the status of a mailing from the mailing guid
    $callResult = "";
    $callName = "GetStatus";
    $params = array(
        "Username" => $sUsr,
        "Password" => $sPwd,
        "MailingGUID" => $MailingGUID,
        "ReturnFormat" => "Text"
    );
    print_object_linebreak($MailingGUID);
    // other available params listed here:  (https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetStatus) returns the status of a mailing from the mailing guid
    $callResult = MakeCall($callName, $params, "for MailingGUID " . $MailingGUID);

    $Status = GetFld($callResult, "Status");
    print_object_linebreak("Status = " . $Status);

    return $callResult;
}

function WaitForProcessMailingStatus($sUsr, $sPwd, $MailingGUID, $ExpectedStatus, $ExceptionOnFail)
{

    //poll GetStatus in a loop until the processing has completed
    //loop a maximum of 10 times, with a 10 second delay between iterations.
    //	alternatively; handle callbacks from the HttpPostOnSuccess & HttpPostOnError parameters on ProcessMailing to identify when the processing has completed
    $i = 0;
    do {
        // other available params listed here:  (https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetStatus) returns the status of a mailing from the mailing guid
        $result = GetStatus($sUsr, $sPwd, $MailingGUID);

        $Status = GetFld($result, "Status");
        $Error = GetFld($result, "Error code");
        //end loop once processing is complete
        if ($Status == $ExpectedStatus) {
            break;
        }    //success
        if ($Status == "Error in processing") {
            break;
        }    //error in processing
        if ($Error) {
            break;
        }            //error

        sleep(10);//wait 10 seconds before repeating
        ++$i;
    } while ($i < 10);

    //
    if ($Status == "Error in processing") {
        //get description of error in processing
        $params = array(
            "Username" => $sUsr,
            "Password" => $sPwd,
            "MethodName" => "GetProcessingError",
            "ReturnFormat" => "Text",
            "Properties" => array(
                "PropertyName" => "GetProcessingError",
                "PropertyValue" => $MailingGUID
            )
        );
        $result = MakeCall("ExtendedCall", $params, ": GetProcessingError");
        print_object_linebreak($result);
    }


    //TODO:	handle the status not being reached appropriately for your system
    if ($Status != $ExpectedStatus) {
        if ($ExceptionOnFail) {
            throw new Exception("<h2>There was an error:</h2> expected status '" . $ExpectedStatus . "' not reached.  Current status: '" . $Status . "'<br/>");
        } else {
            print_object_linebreak("WARNING: expected status '" . $ExpectedStatus . "' not reached.  Current status: '" . $Status . "'");
        }
    }

    flush();
}

function CheckError($Res)
{
    if ($Res == null) return;
    //make sure the array is read fromt he beginning
    if (is_array($Res)) reset($Res);
    //check for  the keys 'Error code', 'Error code string' and 'Error message' to test/report errors
    $errCode = GetFld($Res, "Error code");
    if ($errCode) {
        $errName = GetFld($Res, "Error code string");
        $errMsg = GetFld($Res, "Error message");
        print_object_linebreak('ErrCode ' . $errCode);
        print_object_linebreak('ErrName ' . $errName);
        print_object_linebreak('ErrMsg ' . $errMsg);
        throw new Exception("<h2>There was an error:</h2> " . $errCode . " " . $errName . " - " . $errMsg);
    } else {
        print_object_linebreak("Success!");
    }
    if (is_array($Res)) reset($Res);
    flush();
}


//uses print_r to output the object contents, AND resets the object pointer back to the beginning
function print_object($obj)
{
    if (is_array($obj)) reset($obj);
    print_r($obj);
    if (is_array($obj)) reset($obj);
    flush();
}

function print_object_linebreak($obj)
{
    print_object($obj);
    print_object("<br/>");
}

function GetFld($FldList, $FldName)
{
    // calls return a multi-line string structured as :
    // [KEY]: [VALUE][carriage return][line feed][KEY]: [VALUE][carriage return][line feed][KEY]: [VALUE][carriage return][line feed][KEY]: [VALUE]
    //explode lines
    //print "Looking for Field '".$FldName."'<br>";
    $lines = explode("\n", $FldList);
    for ($lineCounter = 0; $lineCounter < count($lines); $lineCounter += 1) {
        //explode field/value
        $fields = explode(":", $lines[$lineCounter]);
        //find matching field name
        if ($fields[0] == $FldName) {
            //print "'".$FldName."' Value: ".ltrim($fields[1], " ")."<br>";
            return ltrim($fields[1], " "); //return value
        }
    }
    //print_object_linebreak( "'".$FldName."' NOT found");
}


