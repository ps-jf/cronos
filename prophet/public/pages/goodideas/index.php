<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 08/11/2019
 * Time: 14:29
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

$charLimit = 70;

$base64auth = base64_encode(JIRA_USERNAME . ":" . JIRA_PASSWORD);

if (isset($_GET['id'])){
    $gi = new GoodIdea($_GET['id'], true);

    if (isset($_GET['do'])){
        switch($_GET['do']){
            case "add_jira_form":
                echo new Template("goodidea/jira.html", compact("gi"));

                break;

            case "add_jira":
                $json = [
                    'success' => true,
                    'data' => null
                ];

                $issue = [
                    "fields" => [
                        "project" => [
                            "key" => $_REQUEST['jira']['board']
                        ],
                        "summary" => $_REQUEST['jira']['summary'],
                        "description" => $_REQUEST['jira']['description'] . "\r\n\r\nIGAGI - " . $gi->addedby->staff_name(),
                        "issuetype" => [
                            "name" => $_REQUEST['jira']['issue_type']
                        ]
                    ]
                ];

                if ($_REQUEST['jira']['assignee'] !== "auto"){
                    $issue['fields']['assignee'] = ["name" => $_REQUEST['jira']['assignee']];
                }
                $curl = curl_init();

                curl_setopt($curl, CURLOPT_URL, "https://policyservices.atlassian.net/rest/api/2/issue/");
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_HTTPHEADER, [
                    "Content-Type: application/json",
                    "Authorization: Basic " . $base64auth
                ]);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($issue));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    $json['success'] = false;
                    $json['data'] = "cURL Error #:" . $err;
                } else {
                    $response = json_decode($response, true);

                    if ($response['errors']){
                        $json['success'] = false;

                        $errors = [];

                        foreach($response['errors'] as $key => $value){
                            $errors[] = $key . ": " . $value;
                        }

                        $json['data'] = implode(" ", $errors);
                    } else {
                        $gi->jira_id($response['key']);

                        if ($gi->save()){
                            $json['data'] = [
                                'jira' => $response['key'],
                                'approve' => true,
                            ];
                        } else {
                            $json['success'] = false;
                            $json['data'] = "Successfully added to JIRA but failed to link JIRA ID in database.";
                        }
                    }
                }

                echo json_encode($json);

                break;

            case "approve":
            case "reject":
                $implement = $_GET['do'];

                echo new Template("goodidea/feedback.html", compact(["implement", "gi"]));

                break;

            case "implement":
                $response = [
                    'success' => true,
                    'data' => null
                ];

                $implement = $_GET['implement'];
                $imp_past_tense = "approved";

                switch($implement){
                    case "approve":
                        $gi->implement(time());

                        $imp_past_tense = "approved";

                        break;

                    case "reject":
                        $gi->implement(-1);

                        $imp_past_tense = "rejected";

                        break;
                }

                $gi->feedback($_GET['feedback']);

                if ($gi->save()){
                    $recipients[]['address']['email'] = $gi->addedby->email();

                    // team leader/heads of mapping
                    $map = [
                        29 => "kevin.dorrian@policyservices.co.uk",             // avril
                        44 => "kayleigh.dorrian@policyservices.co.uk",          // blair
                        45 => "kevin.dorrian@policyservices.co.uk",             // diane
                        87 => "kevin.dorrian@policyservices.co.uk",             // lewis
                        98 => "kayleigh.dorrian@policyservices.co.uk",          // louise gibson
                        99 => "avril.braes@policyservices.co.uk",               // lisa
                        116 => "louise.gibson@policyservices.co.uk",            // michelle
                        141 => "michelle.fraser@policyservices.co.uk",          // catherine
                        185 => "louise.gibson@policyservices.co.uk",            // louise mcgillivray
                        192 => "louise.gibson@policyservices.co.uk",            // sarah ireland
                        203 => "avril.braes@policyservices.co.uk",              // charmaine
                    ];

                    if (array_key_exists($gi->addedby->id(), $map)){
                        $recipients[]['address']['email'] = $map[$gi->addedby->id()];
                    } elseif (in_array($gi->addedby->department(), [14, 15])) {
                        $recipients[]['address']['email'] = "michelle.fraser@policyservices.co.uk";
                    } elseif ($gi->addedby->department() == 1){
                        $recipients[]['address']['email'] = "charmaine.quinn@policyservices.co.uk";
                    } else {
                        $s = new Search(new User());

                        $s->eq("department", $gi->addedby->department());
                        $s->eq("team_leader", 1);
                        $s->eq("active", 1);

                        if ($u = $s->next(MYSQLI_ASSOC)){
                            $recipients[]['address']['email'] = $u->email();
                        }
                    }

                    $from = [
                        'name' => 'Policy Services - IT',
                        'email' => 'it@policyservices.co.uk',
                    ];

                    $local_template['html'] = new Template("sparkpost/emailTemplates/goodidea/implement_feedback.html", compact(["gi", "implement"]));

                    if (sendSparkEmail($recipients, "Good Idea " . ucwords($imp_past_tense), '', false, $from, [], $local_template)) {
                        $response['data'] = "Good idea marked as " . $imp_past_tense . " and user notified.";
                    } else {
                        $response['success'] = false;
                        $response['data'] = "Updated status but failed to send good idea feedback email through sparkpost";
                    }
                } else {
                    $response['success'] = false;
                    $response['data'] = "Failed to update 'implement' status";
                }

                echo json_encode($response);

                break;

            case "complete":
                $response = [
                    'success' => true,
                    'data' => null
                ];

                $gi->implemented(time());

                if ($gi->save()){
                    $recipients[]['address']['email'] = $gi->addedby->email();

                    // team leader/heads of mapping
                    $map = [
                        29 => "kevin.dorrian@policyservices.co.uk",             // avril
                        44 => "kayleigh.dorrian@policyservices.co.uk",          // blair
                        45 => "kevin.dorrian@policyservices.co.uk",             // diane
                        87 => "kevin.dorrian@policyservices.co.uk",             // lewis
                        98 => "kayleigh.dorrian@policyservices.co.uk",          // louise gibson
                        99 => "avril.braes@policyservices.co.uk",               // lisa
                        116 => "louise.gibson@policyservices.co.uk",            // michelle
                        141 => "michelle.fraser@policyservices.co.uk",          // catherine
                        185 => "louise.gibson@policyservices.co.uk",            // louise mcgillivray
                        192 => "louise.gibson@policyservices.co.uk",            // sarah ireland
                        203 => "avril.braes@policyservices.co.uk",              // charmaine
                    ];

                    if (array_key_exists($gi->addedby->id(), $map)){
                        $recipients[]['address']['email'] = $map[$gi->addedby->id()];
                    } elseif (in_array($gi->addedby->department(), [14, 15])) {
                        $recipients[]['address']['email'] = "michelle.fraser@policyservices.co.uk";
                    } elseif ($gi->addedby->department() == 1){
                        $recipients[]['address']['email'] = "charmaine.quinn@policyservices.co.uk";
                    } else {
                        $s = new Search(new User());

                        $s->eq("department", $gi->addedby->department());
                        $s->eq("team_leader", 1);
                        $s->eq("active", 1);

                        if ($u = $s->next(MYSQLI_ASSOC)){
                            $recipients[]['address']['email'] = $u->email();
                        }
                    }

                    $from = [
                        'name' => 'Policy Services - IT',
                        'email' => 'it@policyservices.co.uk',
                    ];

                    $local_template['html'] = new Template("sparkpost/emailTemplates/goodidea/implemented.html", compact(["gi"]));

                    if (sendSparkEmail($recipients, "Good Idea Implemented", '', false, $from, [], $local_template)) {
                        $response['data'] = "Good idea marked as complete and user notified.";
                    } else {
                        $response['success'] = false;
                        $response['data'] = "Updated status but failed to send good idea implemented email through sparkpost";
                    }
                } else {
                    $response['success'] = false;
                    $response['data'] = "Failed to update 'implemented' status";
                }

                echo json_encode($response);

                break;

            case "follow_up_form":
                echo new Template("goodidea/follow_up.html", compact("gi"));

                break;

            case "send_follow_up":
                $response = [
                    'success' => true,
                    'data' => null
                ];

                $recipients[]['address']['email'] = $gi->addedby->email();

                // team leader/heads of mapping
                $map = [
                    29 => "kevin.dorrian@policyservices.co.uk",             // avril
                    44 => "kayleigh.dorrian@policyservices.co.uk",          // blair
                    45 => "kevin.dorrian@policyservices.co.uk",             // diane
                    87 => "kevin.dorrian@policyservices.co.uk",             // lewis
                    98 => "kayleigh.dorrian@policyservices.co.uk",          // louise gibson
                    99 => "avril.braes@policyservices.co.uk",               // lisa
                    116 => "louise.gibson@policyservices.co.uk",            // michelle
                    141 => "michelle.fraser@policyservices.co.uk",          // catherine
                    185 => "louise.gibson@policyservices.co.uk",            // louise mcgillivray
                    192 => "louise.gibson@policyservices.co.uk",            // sarah ireland
                    203 => "avril.braes@policyservices.co.uk",              // charmaine
                ];

                if (array_key_exists($gi->addedby->id(), $map)){
                    $recipients[]['address']['email'] = $map[$gi->addedby->id()];
                } elseif (in_array($gi->addedby->department(), [14, 15])) {
                    $recipients[]['address']['email'] = "michelle.fraser@policyservices.co.uk";
                } elseif ($gi->addedby->department() == 1){
                    $recipients[]['address']['email'] = "charmaine.quinn@policyservices.co.uk";
                } else {
                    $s = new Search(new User());

                    $s->eq("department", $gi->addedby->department());
                    $s->eq("team_leader", 1);

                    if ($u = $s->next(MYSQLI_ASSOC)){
                        $recipients[]['address']['email'] = $u->email();
                    }
                }

                $from = [
                    'name' => 'Policy Services - IT',
                    'email' => 'it@policyservices.co.uk',
                ];

                $message = $_GET['message'];

                $local_template['html'] = new Template("sparkpost/emailTemplates/goodidea/follow_up.html", compact(["gi", "message"]));

                if (sendSparkEmail($recipients, "Good Idea Follow Up", '', false, $from, [], $local_template)) {
                    $response['data'] = "Follow up email sent.";
                } else {
                    $response['success'] = false;
                    $response['data'] = "Failed to send follow up email";
                }

                echo json_encode($response);

                break;

            case "pay":
                $response = [
                    'success' => true,
                    'data' => null
                ];

                $gi->paid(time());

                if ($gi->save()){
                    $recipients[]['address']['email'] = $gi->addedby->email();

                    // team leader/heads of mapping
                    $map = [
                        29 => "kevin.dorrian@policyservices.co.uk",             // avril
                        44 => "kayleigh.dorrian@policyservices.co.uk",          // blair
                        45 => "kevin.dorrian@policyservices.co.uk",             // diane
                        87 => "kevin.dorrian@policyservices.co.uk",             // lewis
                        98 => "kayleigh.dorrian@policyservices.co.uk",          // louise gibson
                        99 => "avril.braes@policyservices.co.uk",               // lisa
                        116 => "louise.gibson@policyservices.co.uk",            // michelle
                        141 => "michelle.fraser@policyservices.co.uk",          // catherine
                        185 => "louise.gibson@policyservices.co.uk",            // louise mcgillivray
                        192 => "louise.gibson@policyservices.co.uk",            // sarah ireland
                        203 => "avril.braes@policyservices.co.uk",              // charmaine
                    ];

                    if (array_key_exists($gi->addedby->id(), $map)){
                        $recipients[]['address']['email'] = $map[$gi->addedby->id()];
                    } elseif (in_array($gi->addedby->department(), [14, 15])) {
                        $recipients[]['address']['email'] = "michelle.fraser@policyservices.co.uk";
                    } elseif ($gi->addedby->department() == 1){
                        $recipients[]['address']['email'] = "charmaine.quinn@policyservices.co.uk";
                    } else {
                        $s = new Search(new User());

                        $s->eq("department", $gi->addedby->department());
                        $s->eq("team_leader", 1);
                        $s->eq("active", 1);

                        if ($u = $s->next(MYSQLI_ASSOC)){
                            $recipients[]['address']['email'] = $u->email();
                        }
                    }

                    $from = [
                        'name' => 'Policy Services - IT',
                        'email' => 'it@policyservices.co.uk',
                    ];

                    $local_template['html'] = new Template("sparkpost/emailTemplates/goodidea/paid.html", compact(["gi"]));

                    if (sendSparkEmail($recipients, "Good Idea Paid", '', false, $from, [], $local_template)) {
                        $response['data'] = "Good idea marked as paid and user notified.";
                    } else {
                        $response['success'] = false;
                        $response['data'] = "Updated status but failed to send good idea paid email through sparkpost";
                    }
                } else {
                    $response['success'] = false;
                    $response['data'] = "Failed to update 'paid' status";
                }

                echo json_encode($response);

                break;
        }
    }
} else {
    $response = [
        'success' => true,
        'data' => null,
    ];

    if (isset($_GET['do'])){
        switch($_GET['do']){
            case "main_counter":
            case "assigned_counter":
                $response['count'] = GoodIdea::assigned_counter();

                echo json_encode($response);

                break;

            case "load_assigned":
                $s = new Search(new GoodIdea());

                $s->eq("implemented", null);
                $s->eq("allocated_to", User::get_default_instance("id"));
                $s->add_or([
                    $s->eq("implement", null, false, 0),
                    $s->nt("implement", -1, false, 0),
                ]);
                $s->add_order("implement");

                while($gi = $s->next(MYSQLI_ASSOC)){
                    $description = $gi->description();
                    $notes = $gi->notes();

                    if (!empty($description) && strlen($description) > $charLimit){
                        $description = substr($description, 0, $charLimit) . "...";
                    }

                    if (!empty($notes) && strlen($notes) > $charLimit){
                        $notes = substr($notes, 0, $charLimit) . "...";
                    }

                    $subject = $gi->link();

                    if($gi->allocated_to()){
                        $subject .= "<br><i class='fas fa-user'></i> " . $gi->allocated_to;
                    }

                    $response['data'] .= "<tr>
                        <td>" . $subject . "</td>
                        <td style='max-width: 200px'>" . $description . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->addedwhen())->toDayDateTimeString() . "</td>
                        <td>" . ($gi->date_due() ? \Carbon\Carbon::parse($gi->date_due())->toDayDateTimeString() : 'None') . "</td>
                        <td>" . $gi->addedby->staff_name() . "</td>
                        <td style='max-width: 200px'>" . $notes . "</td>
                        </tr>";
                }

                echo json_encode($response);

                break;

            case "load_pending":
                $s = new Search(new GoodIdea());

                $s->eq("implement", null);
                $s->add_order("addedwhen");

                while($gi = $s->next(MYSQLI_ASSOC)){
                    $description = $gi->description();
                    $notes = $gi->notes();

                    if (!empty($description) && strlen($description) > $charLimit){
                        $description = substr($description, 0, $charLimit) . "...";
                    }

                    if (!empty($notes) && strlen($notes) > $charLimit){
                        $notes = substr($notes, 0, $charLimit) . "...";
                    }

                    $subject = $gi->link();

                    if($gi->allocated_to()){
                        $subject .= "<br><i class='fas fa-user'></i> " . $gi->allocated_to;
                    }

                    $response['data'] .= "<tr>
                        <td>" . $subject . "</td>
                        <td style='max-width: 200px'>" . $description . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->addedwhen())->toDayDateTimeString() . "</td>
                        <td>" . ($gi->date_due() ? \Carbon\Carbon::parse($gi->date_due())->toDayDateTimeString() : 'None') . "</td>
                        <td>" . $gi->addedby->staff_name() . "</td>
                        <td style='max-width: 200px'>" . $notes . "</td>
                        </tr>";
                }

                echo json_encode($response);

                break;

            case "load_development":
                $s = new Search(new GoodIdea());

                $s->nt("implement", null);
                $s->nt("implement", -1);
                $s->eq("implemented", null);
                $s->add_order("implement");

                while($gi = $s->next(MYSQLI_ASSOC)){
                    $description = $gi->description();
                    $notes = $gi->notes();

                    if (!empty($description) && strlen($description) > $charLimit){
                        $description = substr($description, 0, $charLimit) . "...";
                    }

                    if (!empty($notes) && strlen($notes) > $charLimit){
                        $notes = substr($notes, 0, $charLimit) . "...";
                    }

                    $subject = $gi->link();

                    if($gi->allocated_to()){
                        $subject .= "<br><i class='fas fa-user'></i> " . $gi->allocated_to;
                    }

                    $response['data'] .= "<tr>
                        <td>" . $subject . "</td>
                        <td style='max-width: 200px'>" . $description . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->addedwhen())->toDayDateTimeString() . "</td>
                        <td>" . ($gi->date_due() ? \Carbon\Carbon::parse($gi->date_due())->toDayDateTimeString() : 'None') . "</td>
                        <td>" . $gi->addedby->staff_name() . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->implement())->toDayDateTimeString() . "</td>
                        <td style='max-width: 200px'>" . $notes . "</td>
                        </tr>";
                }

                echo json_encode($response);

                break;

            case "load_unpaid":
                $s = new Search(new GoodIdea());

                $s->nt("implemented", null);
                $s->eq("paid", null);
                $s->add_order("implemented", "DESC");

                while($gi = $s->next(MYSQLI_ASSOC)){
                    $description = $gi->description();
                    $notes = $gi->notes();

                    if (!empty($description) && strlen($description) > $charLimit){
                        $description = substr($description, 0, $charLimit) . "...";
                    }

                    if (!empty($notes) && strlen($notes) > $charLimit){
                        $notes = substr($notes, 0, $charLimit) . "...";
                    }

                    $subject = $gi->link();

                    if($gi->allocated_to()){
                        $subject .= "<br><i class='fas fa-user'></i> " . $gi->allocated_to;
                    }

                    $response['data'] .= "<tr>
                        <td><input type='checkbox' class='unpaid-checkbox' value='" . $gi->id() . "'></td>
                        <td>" . $subject . "</td>
                        <td style='max-width: 200px'>" . $description . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->addedwhen())->toDayDateTimeString() . "</td>
                        <td>" . ($gi->date_due() ? \Carbon\Carbon::parse($gi->date_due())->toDayDateTimeString() : 'None') . "</td>
                        <td>" . $gi->addedby->staff_name() . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->implement())->toDayDateTimeString() . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->implemented())->toDayDateTimeString() . "</td>
                        <td style='max-width: 200px'>" . $notes . "</td>
                        </tr>";
                }

                echo json_encode($response);

                break;

            case "load_paid":
                $s = new Search(new GoodIdea());

                $s->nt("paid", null);
                $s->add_order("paid", "DESC");

                while($gi = $s->next(MYSQLI_ASSOC)){
                    $description = $gi->description();
                    $notes = $gi->notes();

                    if (!empty($description) && strlen($description) > $charLimit){
                        $description = substr($description, 0, $charLimit) . "...";
                    }

                    if (!empty($notes) && strlen($notes) > $charLimit){
                        $notes = substr($notes, 0, $charLimit) . "...";
                    }

                    $subject = $gi->link();

                    if($gi->allocated_to()){
                        $subject .= "<br><i class='fas fa-user'></i> " . $gi->allocated_to;
                    }

                    $response['data'] .= "<tr>
                        <td>" . $subject . "</td>
                        <td style='max-width: 200px'>" . $description . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->addedwhen())->toDayDateTimeString() . "</td>
                        <td>" . ($gi->date_due() ? \Carbon\Carbon::parse($gi->date_due())->toDayDateTimeString() : 'None') . "</td>
                        <td>" . $gi->addedby->staff_name() . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->implement())->toDayDateTimeString() . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->implemented())->toDayDateTimeString() . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->paid())->toDayDateTimeString() . "</td>
                        <td style='max-width: 200px'>" . $notes . "</td>
                        </tr>";
                }

                echo json_encode($response);

                break;

            case "load_rejected":
                $s = new Search(new GoodIdea());

                $s->eq("implement", -1);
                $s->add_order("addedwhen", "DESC");

                while($gi = $s->next(MYSQLI_ASSOC)){
                    $description = $gi->description();
                    $notes = $gi->notes();

                    if (!empty($description) && strlen($description) > $charLimit){
                        $description = substr($description, 0, $charLimit) . "...";
                    }

                    if (!empty($notes) && strlen($notes) > $charLimit){
                        $notes = substr($notes, 0, $charLimit) . "...";
                    }

                    $subject = $gi->link();

                    if($gi->allocated_to()){
                        $subject .= "<br><i class='fas fa-user'></i> " . $gi->allocated_to;
                    }

                    $response['data'] .= "<tr>
                        <td>" . $subject . "</td>
                        <td style='max-width: 200px'>" . $description . "</td>
                        <td>" . \Carbon\Carbon::createFromTimestamp($gi->addedwhen())->toDayDateTimeString() . "</td>
                        <td>" . ($gi->date_due() ? \Carbon\Carbon::parse($gi->date_due())->toDayDateTimeString() : 'None') . "</td>
                        <td>" . $gi->addedby->staff_name() . "</td>
                        <td style='max-width: 200px'>" . $notes . "</td>
                        </tr>";
                }

                echo json_encode($response);

                break;

            case "load_user-breakdown":
                $db = new mydb();

                $query = "SELECT added_by, count(implemented) - count(paid_date) as unpaid, count(paid_date) as paid 
                          FROM prophet.good_ideas 
                          group by added_by 
                          order by paid desc, unpaid desc";

                $db->query($query);

                while($r = $db->next(MYSQLI_ASSOC)){
                    $user = new User($r['added_by'], true);

                    $response['data'] .= "<tr>
                        <td>" . $user->link($user->staff_name()) . "</td>
                        <td>&pound;" . ($r['unpaid'] * 50) . " (" . $r['unpaid'] . " good ideas)</td>
                        <td>&pound;" . ($r['paid'] * 50) . " (" . $r['paid'] . " good ideas)</td>
                    </tr>";
                }

                echo json_encode($response);

                break;

            case "add_new":
                $goodidea = new GoodIdea();

                echo new Template("goodidea/new.html", compact(["goodidea"]));

                break;

            case "pay-range":
                $response = [
                    'success' => true,
                    'data' => null,
                ];

                $emailsSent = 0;

                foreach($_GET['ids'] as $id){
                    $gi = new GoodIdea($id, true);

                    $gi->paid(time());

                    $recipients = [];

                    if ($gi->save()){
                        $recipients[]['address']['email'] = $gi->addedby->email();

                        // team leader/heads of mapping
                        $map = [
                            29 => "kevin.dorrian@policyservices.co.uk",             // avril
                            44 => "kayleigh.dorrian@policyservices.co.uk",          // blair
                            45 => "kevin.dorrian@policyservices.co.uk",             // diane
                            87 => "kevin.dorrian@policyservices.co.uk",             // lewis
                            98 => "kayleigh.dorrian@policyservices.co.uk",          // louise gibson
                            99 => "avril.braes@policyservices.co.uk",               // lisa
                            116 => "louise.gibson@policyservices.co.uk",            // michelle
                            141 => "michelle.fraser@policyservices.co.uk",          // catherine
                            185 => "louise.gibson@policyservices.co.uk",            // louise mcgillivray
                            192 => "louise.gibson@policyservices.co.uk",            // sarah ireland
                            203 => "avril.braes@policyservices.co.uk",              // charmaine
                        ];

                        if (array_key_exists($gi->addedby->id(), $map)){
                            $recipients[]['address']['email'] = $map[$gi->addedby->id()];
                        } elseif (in_array($gi->addedby->department(), [14, 15])) {
                            $recipients[]['address']['email'] = "michelle.fraser@policyservices.co.uk";
                        } elseif ($gi->addedby->department() == 1){
                            $recipients[]['address']['email'] = "charmaine.quinn@policyservices.co.uk";
                        } else {
                            $s = new Search(new User());

                            $s->eq("department", $gi->addedby->department());
                            $s->eq("team_leader", 1);

                            if ($u = $s->next(MYSQLI_ASSOC)){
                                $recipients[]['address']['email'] = $u->email();
                            }
                        }

                        $from = [
                            'name' => 'Policy Services - IT',
                            'email' => 'it@policyservices.co.uk',
                        ];

                        $local_template['html'] = new Template("sparkpost/emailTemplates/goodidea/paid.html", compact(["gi"]));

                        if (sendSparkEmail($recipients, "Good Idea Paid", '', false, $from, [], $local_template)) {
                            $emailsSent++;
                        } else {
                            $response['success'] = false;
                            $response['data'] = "Updated status but failed to send good idea paid email through sparkpost";
                        }
                    } else {
                        $response['success'] = false;
                        $response['data'] = "Failed to update 'paid' status";
                    }

                    if ($emailsSent == count($_GET['ids'])){
                        $response['data'] = "All tickets marked as paid and users notified";
                    }
                }

                echo json_encode($response);

                break;

            case "load_tbp":
                $pendingCount = 0;
                $developmentCount = 0;

                $totalTBP = 0;
                $totalUnpaid = 0;

                $totalPaid = 0;
                $totalPaidCount = 0;

                $sp = new Search(new GoodIdea());

                $sp->eq("implement", null);

                $pendingCount = $sp->count();

                $sd = new Search(new GoodIdea());

                $sd->nt("implement", null);
                $sd->nt("implement", -1);
                $sd->eq("implemented", null);

                $developmentCount = $sd->count();

                $stbp = new Search(new GoodIdea());

                $stbp->nt("implemented", null);
                $stbp->eq("paid", null);

                $totalTBP = $stbp->count();

                $totalUnpaid = $totalTBP * 50;

                $tp = new Search(new GoodIdea());

                $sixMonthsAgo = \Carbon\Carbon::now()->subMonths(6)->format("U");

                $tp->nt("paid", null);
                $tp->gt("paid", $sixMonthsAgo);

                $totalPaidCount = $tp->count();

                $totalPaid = $totalPaidCount * 50;

                $response['data']['pending'] = $pendingCount;
                $response['data']['development'] = $developmentCount;
                $response['data']['total'] = $totalUnpaid;
                $response['data']['count'] = $totalTBP;
                $response['data']['paid_total'] = $totalPaid;
                $response['data']['paid_count'] = $totalPaidCount;

                echo json_encode($response);

                break;

            case "export_unpaid":
                $db = new mydb();

                $db->query("SELECT added_by, count(*) as unpaid from prophet.good_ideas where paid_date is null and implemented is not null group by added_by order by unpaid desc");

                $file = fopen("php://memory", "w");

                $printKeys = true;

                while($row = $db->next(MYSQLI_ASSOC)){
                    if ($printKeys){
                        fputcsv($file, ["User", "Unpaid Ideas", "Total"]);
                        $printKeys = false;
                    }
                    $user = new User($row['added_by'], true);

                    fputcsv($file, [$user->staff_name(), $row['unpaid'], utf8_decode("£") . ($row['unpaid'] * 50)]);
                }

                fseek($file, 0);

                header('Content-Type: application/csv');
                header('Content-Disposition: attachment; filename="'. date("Y-m-d") . '"_unpaid_good_ideas.csv"');

                fpassthru($file);

                break;

            case "assigned":
                echo new Template("goodidea/assigned.html");

                break;
        }
    } else {
        $pendingCount = 0;
        $developmentCount = 0;

        $totalTBP = 0;
        $totalUnpaid = 0;

        $totalPaid = 0;
        $totalPaidCount = 0;

        $sp = new Search(new GoodIdea());

        $sp->eq("implement", null);

        $pendingCount = $sp->count();

        $sd = new Search(new GoodIdea());

        $sd->nt("implement", null);
        $sd->nt("implement", -1);
        $sd->eq("implemented", null);

        $developmentCount = $sd->count();

        $stbp = new Search(new GoodIdea());

        $stbp->nt("implemented", null);
        $stbp->eq("paid", null);

        $totalTBP = $stbp->count();

        $totalUnpaid = $totalTBP * 50;

        $tp = new Search(new GoodIdea());

        $tp->nt("paid", null);

        $totalPaidCount = $tp->count();

        $totalPaid = $totalPaidCount * 50;

        echo new Template("goodidea/list.html", compact(["pendingCount", "developmentCount", "totalUnpaid", "totalTBP", "totalPaid", "totalPaidCount"]));
    }
}