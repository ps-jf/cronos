<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 19/07/2019
 * Time: 13:32
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

$userGroups = User::groups(User::get_default_instance("id"));

if (isset($_GET['do'])){
    switch($_GET['do']){
        case "read_file":
            if(isset($_GET['dir'])){
                $file = urldecode($_GET['dir']);
            } elseif(isset($_GET['help'])){
                $file = PROPHET_MANUALS_DIR . urldecode($_GET['help']);
            }

            if(empty($file)){
                throw new Exception("dir or help variable not set for filepath");
            }

            $info = pathinfo($file);

            $redisClient = RedisConnection::connect();

            $file = str_replace("//", "/", $file);

            $title = last(explode("/", $file));

            if (User::get_default_instance("department") != 7){
                if (!$redisClient->exists("Documentation.Viewers." . $file . "." . User::get_default_instance("id"))){
                    $redisClient->set("Documentation.Viewers." . $file . "." . User::get_default_instance("id"), time());
                }
            } else {
                if ($redisClient->exists("Documentation.Viewers." . $file . "." . User::get_default_instance("id"))){
                    // If IT, delete the key, we dont want to see what IT have viewed
                    $redisClient->del("Documentation.Viewers." . $file . "." . User::get_default_instance("id"));
                }
            }

            switch (strtolower($info['extension'])){
                case "txt":
                case "html":
                    //read the file and output its plain text
                    $output = file_get_contents($file);

                    break;

                case "pdf":
                    $output = "<object class=\"w-100 h-100\" id=\"pdf_container\" name=\"pdf_container\" data=\"/pages/documentation/?do=load_pdf&file=" . $file . "\" type=\"application/pdf\" />";

                    break;

                case "png":
                case "jpg":
                    $type = mime_content_type($file);
                    $img = file_get_contents($file);
                    $output = sprintf("<img class='mw-100' src='data:" . $type . ";base64,%s'>", base64_encode($img));

                    break;

                default:
                    $output = "<div class='w-100'>
                        <div class='p-5 text-center'>
                            <a class='btn btn-primary px-5 py-2' id='download-file' href='/pages/documentation/?do=download_file&file=" . $file . "' target='_blank'><i class=\"fal fa-download\"></i> Download</a>
                        </div>                           
                        </div>";

                    break;
            }

            echo new Template("documentation/viewer.html", compact([
                "output",
                "title"
            ]));

            break;

        case "load_pdf":
            if (file_exists($_GET['file'])) {
                if ($f = fopen($_GET['file'], "r")) {
                    header("Cache-control: private");
                    header("Content-Type: application/pdf");
                    header("Pragma: public");
                    $contents = fread($f, filesize($_GET['file']));
                    print($contents);
                    fclose($f);
                    exit();
                }
            } else {
                print "File not found";
                exit();
            }

            break;

        case "download_file":
            if (file_exists($_GET['file'])){
                header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
                header("Cache-Control: public"); // needed for internet explorer
                header("Content-Type: " . mime_content_type($file));
                header("Content-Transfer-Encoding: Binary");
                header("Content-Length:".filesize($_GET['file']));
                header("Content-Disposition: attachment; filename=" . basename($_GET['file']));
                readfile($_GET['file']);
                die();
            } else {
                echo "Failed to load file: " . $file;
            }

            break;

        case "make_dir":
            $response = [
                'success' => true,
                'error' => null
            ];

            try{
                //create the directory
                mkdir(urldecode($_GET['dir']) . "/" . urldecode($_GET['name']));

                if (isset($_GET['owner'])){
                    // owner can only be set if IT created a directory in the home path
                    // create a prophet.json file
                    $perm = [
                        'owner' => $_GET['owner'],
                        'visible' => $_GET['show'],
                        'groups' => $_GET['group_filter'] ?? [],
                        'users' => $_GET['user_filter'] ?? [],
                    ];

                    $file = urldecode($_GET['dir']) . "/" . urldecode($_GET['name']) . "/prophet.json";
                    file_put_contents($file, json_encode($perm));
                }
            } catch (Exception $e){
                $response['success'] = false;
                $response['error'] = $e->getMessage();
            }

            echo json_encode($response);

            break;

        case "upload_file":
            $response = [
                'success' => true,
                'error' => null,
            ];

            if (isset($_FILES['file'])){
                try{
                    if ($_FILES['file']['size'] < 41943040 /* 40mb */){
                        if ($_FILES['file']['error'] < 1){
                            if (file_exists($_FILES['file']['tmp_name'])){
                                if (!move_uploaded_file($_FILES['file']['tmp_name'], urldecode($_GET['dir']) . "/" . $_FILES['file']['name'])){
                                    throw new Exception("Failed to upload file");
                                }
                            } else {
                                throw new Exception("File not found");
                            }
                        } else {
                            throw new Exception("File error: " . $_FILES['file']['error']);
                        }
                    } else {
                        throw new Exception("File Size is too big");
                    }
                } catch (Exception $e) {
                    $response['success'] = false;
                    $response['error'] = $e->getMessage();
                }
            } else {
                $response['success'] = false;
                $response['error'] = "No file was passed through";
            }

            echo json_encode($response);

            break;

        case "delete":
            $response = [
                'success' => true,
                'error' => null,
            ];

            try{
                $file = urldecode($_GET['dir']) . "/" . $_GET['name'];

                if (file_exists($file)){
                    if (is_dir($file)){
                        $it = new RecursiveDirectoryIterator($file, RecursiveDirectoryIterator::SKIP_DOTS);
                        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

                        foreach($files as $f){
                            if (is_dir($f)){
                                rmdir($f->getRealPath());
                            } else {
                                unlink($f->getRealPath());
                            }
                        }

                        rmdir($file);
                    } else {
                        unlink($file);
                    }
                } else {
                    throw new Exception("File does not exist");
                }

                if (file_exists($file)){
                    throw new Exception("Failed to delete file");
                }
            } catch (Exception $e) {
                $response['success'] = false;
                $response['error'] = $e->getMessage();
            }

            echo json_encode($response);

            break;

        case "save_settings":
            $response = [
                'success' => true,
                'error' => null,
                'renamed' => false,
            ];

            try{
                $permsFile = get_perms(urldecode($_GET['dir']));

                if ($_GET['name'] != basename(urldecode($_GET['dir']))){
                    //the file was renamed
                    $oldname = urldecode($_GET['dir']);
                    $newname = dirname($oldname) . "/" . $_GET['name'];

                    if(!rename($oldname, $newname)){
                        throw new Exception("Failed to rename directory");
                    }

                    $response['renamed'] = urlencode($newname);
                    $permsFile = get_perms($newname);
                }

                $perms = json_decode(file_get_contents($permsFile));

                $perms->owner = $_GET['owner'];
                $perms->visible = $_GET['show'];
                $perms->groups = $_GET['group_filter'] ?? [];
                $perms->users = $_GET['user_filter'] ?? [];

                if(!file_put_contents($permsFile, json_encode($perms))){
                    throw new Exception("Failed to save settings file");
                }
            } catch (Exception $e) {
                $response['success'] = false;
                $response['error'] = $e->getMessage();
            }

            echo json_encode($response);

            break;

        case "get_viewers":
            $response = [
                'success' => true,
                'data' => null,
            ];

            $redisClient = RedisConnection::connect();

            $file = urldecode($_GET['file']);

            $file = str_replace("//", "/", $file);

            $i = 0;
            $runi = true;

            $keys = [];

            while ($runi){
                $viewerCursor = $redisClient->scan($i, "MATCH", "Documentation.Viewers." . $file . ".*");

                foreach($viewerCursor[1] as $vc){
                    $keys[] = $vc;
                }

                if ($viewerCursor[0] == 0){
                    $runi = false;
                } else {
                    $i = $viewerCursor[0];
                }
            }

            $viewers = [];

            foreach($keys as $key){
                $keyParts = explode(".", $key);

                $uid = end($keyParts);

                $u = new User($uid, true);

                $viewers[] = [
                    'name' => $u->staff_name(),
                    'time' => \Carbon\Carbon::createFromTimestamp($redisClient->get($key))->toDayDateTimeString()
                ];
            }

            $response['data'] = $viewers;

            echo json_encode($response);

            break;
    }
} else {
    if (isset($_GET['dir'])){
        $dir = urldecode($_GET['dir']);
    } else {
        $dir = PROPHET_MANUALS_DIR;
    }

    $perms = get_perms($dir);

    $perms ? $perms = json_decode(file_get_contents($perms)) : $perms;

    $files = scandir($dir . "/");

    $excludes = [".", "..", "prophet.json"];

    $canCreate = false;

    if (User::get_default_instance("department") == 7){
        $canCreate = true;
    }

    if ($perms){
        if ($perms->visible == "all") {
            $canCreate = true;
        } elseif (User::get_default_instance("department") == intval($perms->owner) && User::get_default_instance("team_leader")){
            $canCreate = true;
        }
    }

    foreach ($files as $key => $value){
        if (!in_array($value, $excludes)){
            $files[$key] = $dir . "/" . $value;
        } else {
            unset($files[$key]);
        }
    }

    echo new Template("documentation/index.html", ["baseDir" => $dir, "files" => $files, "perms" => $perms, "groups" => $userGroups, "canCreate" => $canCreate]);
}

function get_perms($dir){
    $file = false;

    while ($file == false && $dir != PROPHET_MANUALS_DIR && $dir . "/" != PROPHET_MANUALS_DIR){
        if (file_exists($dir . "/prophet.json")){
            $file = $dir . "/prophet.json";
        } else {
            $dir = explode("/", $dir);
            array_pop($dir);
            $dir = implode("/", $dir);
        }
    }

    return $file;
}
