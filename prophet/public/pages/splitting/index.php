<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$split = new Split();

if (isset($_GET["id"])) {
    if (isset($_GET["list"])) {
        $db = new mydb(REMOTE, E_USER_WARNING);
        if (mysqli_connect_errno()) {
            echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
            exit();
        }

        // Query now limited by user id to allow more people to split at the same time 'allocated = .......'
        $db->query("SELECT " . REMOTE_DATA_CHANGE_TBL . ".id, object_type, object_id, 
        " . REMOTE_USR_TBL . " .email as req_by, 
        " . REMOTE_USR_TBL . ".id as req_id, proposed_time, 
        from_unixtime(proposed_time, '%d-%m-%Y %h:%i:%s') as p_time 
                    FROM " . REMOTE_DATA_CHANGE_TBL . "
                    INNER JOIN " . REMOTE_USR_TBL . " 
                    ON " . REMOTE_DATA_CHANGE_TBL . ".proposed_by = " . REMOTE_USR_TBL . ".id 
            INNER JOIN prophet.users
            ON " . REMOTE_DATA_CHANGE_TBL . ".allocated = prophet.users.id
            WHERE committed_by IS NULL AND committed_time IS NULL
            AND data_change.allocated = " . $_COOKIE['uid'] . "
            AND " . REMOTE_DATA_CHANGE_TBL . ".comments = 'Client split'
            AND proposed_time NOT IN (select proposed_time from " . DATA_CHANGE_ERROR_TBL . "
            INNER JOIN " . REMOTE_DATA_CHANGE_TBL . "
            ON " . DATA_CHANGE_ERROR_TBL . ".data_change_id = " . REMOTE_DATA_CHANGE_TBL . ".id
            WHERE " . REMOTE_DATA_CHANGE_TBL . ".comments = 'Client split')
            ORDER BY proposed_time
            LIMIT 100");


        while ($d = $db->next()) {

            echo "<tr id='$d[id]' name='$d[id]'>
            <td>" . $d['id'] . "</td>
            <td>" . $d['object_type'] . "</td>
            <td>" . $d['req_by'] . "</td>
            <td>" . $d['p_time'] . "</td>
            <td>
                <button href=\"/pages/splitting/action_split.php?id=" .
                    $d['id'] . "\">Action</button>
                <button class='delete-split' data-id=" . $d['id'] . ">
                    <i class='fas fa-trash-alt text-danger'></i>
                </button>
            </td>
            </tr>";
        }
    } elseif (isset($_GET["report"])) {

        // Add split request to error table to lookup later
        $json = [
            "success" => false,
            "error" => false
        ];

        $dc = new DataChange($_GET['id'], true);
        $wu = new WebsiteUser($dc->proposed_by(), true);

        $client = new Client($dc->object_id(), true);

        $db = new mydb();
        $q = "INSERT INTO feebase.data_change_error(data_change_id, addedby, addedwhen, comments, object) VALUES(" . $_GET['id'] . ", " . User::get_default_instance("id") . ", " . time() . ",'" . addslashes($_GET['split-problem-reason']) . "','Client')";

        if ($db->query($q)) {
            $whitelist = [
                '127.0.0.1',
                '127.0.0.1:8080',
                '127.0.0.1:8088',
                '::1',
                'localhost:8080',
                'localhost:8088',
                '10.0.2.2'
            ];

            if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                $recipient = ['name' => 'Jonathan', 'email' => 'jonathan.foley@policyservices.co.uk'];
            } else {
                $recipient = ['name' => '', 'email' => $wu->email_address()];
            }

            $sender = ['name' => 'Policy Services', 'email' => 'it@policyservices.co.uk'];
            $user = User::get_default_instance();

            $data = json_encode([
                "CLIENT" => strval($client),
                "REASON" => $_GET['split-problem-reason'],
                "SENDER" => $user->staff_name(),
                "DEPARTMENT" => "IT Department"
            ]);

            if (sendSparkEmail($recipient, 'Policy Services Split Issue', 'split-issue', $data, $sender)) {
                $json['status'] = " An email has been sent to " . $recipient['email'];
            } else {
                $json['status'] = "Email failed to send";
                $json['error'] = true;
            }
        } else {
            $json['status'] = "Failed to mark as problem.";
            $json['error'] = true;
        }

        echo json_encode($json);

    } elseif (isset($_GET['do'])){
        switch ($_GET['do']){
            case "delete":
                $json = [
                    "success" => true,
                    "data" => "",
                ];

                $s = new Split($_GET['id'], true);

                if (!$s->delete()){
                    $json['success'] = false;
                    $json['data'] = "Failed to delete client split request! Contact IT!";
                }

                echo json_encode($json);

                break;
        }
    }
} else {
    echo new Template("splitting/client_home.html", ["split" => $split]);
}
