<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$redisClient = RedisConnection::connect();

if (isset($_GET["id"])) {

    $return = [
        "id"     => false,
        "status" => false,
        "description" => "",
        "error"  => false
    ];

    if ($redisClient->get("incomerun")){
        $return['error'] = true;
        $return['description'] = "Splits cannot be actioned whilst the income run is being processed.";
    } else {
        $split = new Split($_GET['id'], true);

        $client = new Client($split->object_id(), true);

        // Set paths to the python script to run
        $python = '/usr/bin/python3.5';
        $py_script = '/usr/local/bin/split_client.py';

        $client1address = [];
        $client2address = [];

        if (!empty($_GET['client-1-keep-address'])){
            $client1address['line1'] = str_replace("'", "", strval($client->address->line1));
            $client1address['line2'] = str_replace("'", "", strval($client->address->line2));
            $client1address['line3'] = str_replace("'", "", strval($client->address->line3));
            $client1address['postcode'] = str_replace("'", "", strval($client->address->postcode));
        } else {
            $client1address['line1'] = str_replace("'", "", $_GET['client-1-address-1']);
            $client1address['line2'] = str_replace("'", "", $_GET['client-1-address-2']);
            $client1address['line3'] = str_replace("'", "", $_GET['client-1-address-3']);
            $client1address['postcode'] = str_replace("'", "", $_GET['client-1-address-4']);
        }

        if (!empty($_GET['client-2-keep-address'])){
            $client2address['line1'] = str_replace("'", "", strval($client->address->line1));
            $client2address['line2'] = str_replace("'", "", strval($client->address->line2));
            $client2address['line3'] = str_replace("'", "", strval($client->address->line3));
            $client2address['postcode'] = str_replace("'", "", strval($client->address->postcode));
        } else {
            $client2address['line1'] = str_replace("'", "", $_GET['client-2-address-1']);
            $client2address['line2'] = str_replace("'", "", $_GET['client-2-address-2']);
            $client2address['line3'] = str_replace("'", "", $_GET['client-2-address-3']);
            $client2address['postcode'] = str_replace("'", "", $_GET['client-2-address-4']);
        }

        $policies = [];

        if (!empty($_GET['split_policy'])){
            foreach($_GET['split_policy'] as $id => $p){
                if ($p == 2){
                    $policies[] = $id;
                }
            }
        }

        if (!empty($policies)) {
            $policyString = implode(",", $policies);

            if (!file_exists($python)) {
                #print("The python executable '$python' does not exist!");
                $return["error"] = true;
                $return["description"] .= "The python executable '$python' does not exist!";
            }
            if (!is_executable($python)) {
                #print("The python executable '$python' is not executable!");
                $return["error"] = true;
                $return["description"] .= "The python executable '$python' is not executable!";
            }
            if (!file_exists($py_script)) {
                #print("The python script file '$py_script' does not exist!");
                $return["error"] = true;
                $return["description"] .= "The python script file '$py_script' does not exist!";
            }
            if (!is_executable($py_script)) {
                #print("The python script '$python' is not executable!");
                $return["error"] = true;
                $return["description"] .= "The python script '$py_script' is not executable!";
            }

            if ($return["error"]) {
                echo json_encode($return);
                exit();
            }

            // Build up the command for the cmd line
            $cmd = "$python $py_script";
//            var_dump($cmd . " " . $split->object_id() . " " . $policyString . " '" . json_encode($client1address) . "' '" . json_encode($client2address) . "' 2>&1");
//            die();
            exec($cmd . " " . $split->object_id() . " " . $policyString . " '" . json_encode($client1address) . "' '" . json_encode($client2address) . "' 2>&1", $output, $return_code);

            //	If return code returns 0, no problems have arisen
            if ($return_code == 0) {
                $return["description"] = "Split successful";
                $return["status"] = true;
                $return["id"] = $split->object_id();

                /* If previously a 'problem merge' - Remove data change error after problem has been rectified
                and clients have been merged */
                $s = new Search(new SplitError());
                $s->eq("data_change_id", $split->id());

                if ($se = $s->next(MYSQLI_ASSOC)){
                    $se->delete();
                }
            } else {
                $errormessage = "";
                // Loop through the return array and create a string of errors.
                foreach ($output as $r) {
                    $errormessage .= $r . " ";
                }

                // If array returned with data in it, then json return the data
                $return["error"] = true;
                $return["description"] = $errormessage;
            }
        } else {
            // no policies to go to client 2, just remove their details and create a new client under the partner
            $client = new Client($split->object_id());
            $newClient = new Client();

            $newClient->load($client->flat_array());

            if (empty($_GET['client-1-keep-address'])){
                $client->address->line1($_GET['client-1-address-1']);
                $client->address->line2($_GET['client-1-address-2']);
                $client->address->line3($_GET['client-1-address-3']);
                $client->address->postcode($_GET['client-1-address-4']);
            }

            if (!empty($_GET['client-2-keep-address'])){
                $newClient->address->line1(strval($client->address->line1));
                $newClient->address->line2(strval($client->address->line2));
                $newClient->address->line3(strval($client->address->line3));
                $newClient->address->postcode(strval($client->address->postcode));
            } else {
                $newClient->address->line1($_GET['client-2-address-1']);
                $newClient->address->line2($_GET['client-2-address-2']);
                $newClient->address->line3($_GET['client-2-address-3']);
                $newClient->address->postcode($_GET['client-2-address-4']);
            }

            foreach(get_object_vars($client->two)["field_name_map"] as $key => $value){
                $newClient->one->{$key}->set($client->two->{$key}());

                $client->two->{$key}->set(null);
                $newClient->two->{$key}->set(null);
            }

            if ($newClient->save()){
                $s = new Search(new Policy());
                $s->eq("client", $client->id());
                $s->eq("issuer", 11764);
                $s->eq("owner", [2,3]);
                $s->add_order("policyID", "DESC");
                $s->limit(1);

                if($ca = $s->next(MYSQLI_ASSOC)){
                    // client has a new client agreement signed which belongs to client 2 as well, copy it over
                    $newCA = new Policy();
                    $newCA->load($ca->flat_array());

                    $newCA->client($newClient->id());
                    $newCA->owner(1);

                    if($newCA->save()){
                        $s2 = new Search(new ServicingPropositionLevel());
                        $s2->eq("client_id", $client->id());

                        if($spl = $s2->next(MYSQLI_ASSOC)){
                            // new servicing level, copy this too
                            $newSPL = new ServicingPropositionLevel();

                            $newSPL->load($spl->flat_array());
                            $newSPL->client_id($newClient->id());

                            if(!$newSPL->save()){
                                $return['error'] = true;
                                $return['description'] = "Failed to copy over client servicing level";
                            }
                        }
                    } else {
                        $return['error'] = true;
                        $return['description'] = "Failed to copy over client agreement policy";
                    }
                }

                if ($client->save()){
                    $return['description'] = "Successfully split clients through Prophet!";

                    /* If previously a 'problem merge' - Remove data change error after problem has been rectified
                    and clients have been merged */
                    $s = new Search(new SplitError());
                    $s->eq("data_change_id", $split->id());

                    if ($se = $s->next(MYSQLI_ASSOC)){
                        $se->delete();
                    }

                    $split->committed_time(time());
                    $split->committed_by(User::get_default_instance("id"));

                    if (!$split->save()){
                        $return['description'] .= " Failed to mark split request as complete!";
                    }
                } else {
                    $return['error'] = true;
                    $return['description'] = "Failed to save current client details";
                }
            } else {
                $return['error'] = true;
                $return['description'] = "Failed to save new client details";
            }
        }
    }

    echo json_encode($return);
}
