<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");


if (isset($_GET["id"])) {
    if (isset($_GET["list"])) {
        switch ($_GET["list"]) {
            case "client_probs":
                $db = new mydb(REMOTE);

                $db -> query("SELECT ".DATA_CHANGE_ERROR_TBL.".id, data_change_id, from_unixtime(addedwhen,'%d-%m-%Y %h:%i:%s') as rep_time, ".USR_TBL.".user_name,
									 ".DATA_CHANGE_ERROR_TBL.".comments, proposed_time, from_unixtime(proposed_time,'%d-%m-%Y %h:%i:%s') as proposed_t, ".REMOTE_USR_TBL.".id as req_id, ".REMOTE_USR_TBL.".user_name as req_by
							FROM ".DATA_CHANGE_ERROR_TBL."
							INNER JOIN ".USR_TBL."
							ON ".DATA_CHANGE_ERROR_TBL.".addedby = ".USR_TBL.".id
							INNER JOIN ".REMOTE_DATA_CHANGE_TBL."
							on ".DATA_CHANGE_ERROR_TBL.".data_change_id = ".REMOTE_DATA_CHANGE_TBL.".id
							INNER JOIN ".REMOTE_USR_TBL."
							ON ".REMOTE_DATA_CHANGE_TBL.".proposed_by = ".REMOTE_USR_TBL.".id
							WHERE ".REMOTE_DATA_CHANGE_TBL.".comments = 'Client split'
							GROUP BY req_by");


                while ($d=$db->next()) {
                    $spliterror = new SplitError($d['id'], true);

                    echo "<tr id='$d[id]' name='$d[id]'>
					<td>".$d['id']."</td>
					<td>".$d['data_change_id']."</td>
					<td>".$d['req_by']."</td>
					<td>".$d['comments']."</td>
					<td>".$d['user_name']."</td>
					<td>".$d['rep_time']."</td>
					<td>".el($spliterror -> contacted)."</td>
					<td>
					    <button href=\"/pages/splitting/action_split.php?id=".$d['data_change_id']."&object=client&time=".$d['proposed_time']."&proposed_by=".$d['req_id']."&problem=true\">Action</button>
					    <button class='delete-split' data-id=" . $d['data_change_id'] . "><i class='fas fa-trash-alt text-danger'></i></button>
                    </td>
					</tr>";
                }

                break;


            case "contactpartner":
                $cp = [];
                $cbtnid = $_GET['cbtnid'];

                try {
                    $db = new mydb();
                    $db -> query("UPDATE ".DATA_CHANGE_ERROR_TBL." SET contacted = IF (contacted = 0, 1, 0) WHERE id = ".$cbtnid);
                } catch (Exception $e) {
                    $cp["error"] = $e -> getMessage();
                }

                echo json_encode($cp);

                break;
        }
    } elseif (isset($_GET['do'])){
        switch($_GET['do']){
            case "delete":
                $json = [
                    "success" => true,
                    "data" => "",
                ];

                $m = new Split($_GET['id'], true);

                $splits = $deleted = 0;

                $s = new Search(new Split());
                $s->eq("proposed_time", $m->proposed_time());
                $s->eq("proposed_by", $m->proposed_by());
                $s->eq("comments", $m->comments());

                while($split = $s->next(MYSQLI_ASSOC)){
                    $splits++;

                    if($split->delete()){
                        $deleted++;
                    }
                }

                if ($splits === $deleted) {
                    // Delete the data change error entry
                    $db = new mydb();
                    $db->query("DELETE FROM " . DATA_CHANGE_ERROR_TBL . " WHERE data_change_id = " . $_GET['id']);
                } else {
                    $json['success'] = false;
                    $json['data'] = "Number of records deleted does not match! Contact IT!";
                }

                echo json_encode($json);

                break;
        }
    }
} else {
    echo new Template("splitting/client_problems.html");
}
