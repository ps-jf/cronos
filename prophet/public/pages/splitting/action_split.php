<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $ids = [];

    $db = new mydb(REMOTE, E_USER_WARNING);
    if (mysqli_connect_errno()) {
        echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
        exit();
    }

    $split = new Split($_GET["id"], true);
    $client = new Client($split->object_id(), true);

    $data = json_decode($split->data(), true);

    if (array_key_exists("policies", $data)){
        $policies = $data['policies'];
    } elseif (array_key_exists("policy", $data)) {
        $policies = $data['policy'];
    } else {
        $policies = $data;
    }

    if (array_key_exists("address", $data)){
        $address = $data['address'];
    } else {
        $address = null;
    }

    echo new Template("splitting/client_action_form.html", [
        "split" => $split,
        "client" => $client,
        "policies" => $policies,
        "address" => $address,
        "dcid" => $_GET['id'],
    ]);
} else {
    die("Profile not found");
}
