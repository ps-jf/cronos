<?php
require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    if (isset($_GET["do"])) {
        $practice = $_GET["id"];

        switch ($_GET["do"]) {
            case "list":
                $db = new mydb();

                $s = new Search(new ProviderOnlineAccess);
                $s -> eq('practice_id', $practice);

                while ($p = $s->next()) {
                    echo Template("practice/poa_row.html", ["poa"=>$p]);
                }

                break;

            case "addprovider":
                $poa = new ProviderOnlineAccess($_GET['id'], true);
                echo Template("practice/add_online_access.html", ["poa"=>$poa, "practice_id"=>$_GET['id']]);

                break;

            case "edit":
                $poa = new ProviderOnlineAccess(($_GET['id']), true);
                echo new Template("practice/edit_online_access.html", ["poa"=>$poa, "practice_id"=>$poa->practice_id()]);

                break;
        }
    }
}
