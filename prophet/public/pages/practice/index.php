<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    if (isset($_GET['get'])) {
        switch ($_GET['get']) {
            case "statistics":
                /*
					Gets ID's of all partners within practice
					Put into array for getting combined graph data
				*/
                $s = new Search(new PracticeStaffEntry);
                $s->eq("practice", $_GET["id"]);
                $s->eq("active", 1);

                $partnerIDs = [];

                while ($p = $s->next()) {
                    if ($p->partner() != null) {
                        $partnerIDs[] = $p->partner();
                    }
                }

                if (!empty($partnerIDs)) {
                    /* Graph monthly commission since last year */
                    $json = [
                        "cGraph" => []
                    ];

                    // Compile a list of the last 12 months
                    $months = [];
                    for ($i = 12; $i >= 0; $i--) {
                        $months[] = date('M Y', mktime(0, 0, 0, date('n') - $i));
                    }

                    $json["cGraph"]["months"] = $months;

                    $db = new mydb();
                    $c = new Commission;

                    $db->query(
                        "SELECT DATE_FORMAT(" . $c->date->get_field_name() . ", '%b %Y'), " .
                        "ROUND(SUM(" . $c->amount->get_field_name() . "),2) " .
                        "FROM " . $c->get_table() . " " .
                        "WHERE " . $c->partner->to_sql($partnerIDs) . " " .
                        "AND " . $c->date->get_field_name() . " BETWEEN '" . (date("Y") - 1) . "-" . date("m") . "-01 00:00:00' AND '" . date("Y") . "-" . date("m") . "-" . date("d") . " " . date('G:i:s') . "' " .
                        "GROUP BY DATE_FORMAT(" . $c->date->get_field_name() . ", '%b %Y')"
                    );


                    $a = [];
                    while ($row = $db->next(MYSQLI_NUM)) {
                        $a[$row[0]] = $row[1];
                    }

                    $amounts = [];
                    foreach ($months as $m) {
                        if (!array_key_exists($m, $a)) {
                            $amounts[$m] = 0;
                            continue;
                        }

                        $amounts[$m] = $a[$m];
                    }

                    $json["cGraph"]["data"] = array_values($amounts);

                    /* Various figures */
                    $figures = [];

                    // number of partners within practice
                    $figures["Partners"] = count($partnerIDs);

                    //number of clients within practice
                    $s = new Search(new Client);
                    $s->eq("partner", $partnerIDs);
                    $figures["Clients"] = $s->count();

                    // number of policies
                    $s = new Search(new Policy);
                    $s->eq("client->partner", $partnerIDs);
                    $figures["Policies"] = $s->count();

                    // number of outstanding policies
                    $s->eq("status", 1);
                    $s->eq("addedhow", 2);
                    $figures["Outstanding Policies"] = $s->count();

                    // number of new business cases
                    $s = new Search(new NewBusiness);
                    $s->eq("policy->client->partner", $partnerIDs);
                    $figures["New Business"] = $s->count();

                    // number of unique paying policies
                    $s = new Search(new Commission);
                    $s->eq("partner", $partnerIDs);
                    $figures["Paying Policies"] = $s->count("policy");

                    // total commission for partners
                    $ps = new Search(new Commission);
                    $ps->eq("partner", $partnerIDs);
                    $figures["Total Income Paid To Partners"] = "&pound; " . number_format($ps->sum("paid"), 2);

                    /* GRI calculated as (renewal(0) + clawback(2) + adviser_charging_ongoing(8) + client_direct_ongoing(10) + dfm_fees(12) +
                    OAS Income(14) + Trail(15) + PPB Income(16)) - VAT */
                    $figures["12 Month Rolling GRI"] = Practice::practice_gri($partnerIDs);
                    $figures["Lifetime GRI"] = Practice::practice_gri($partnerIDs, true);

                    // number of correspondence items
                    $s = new Search(new Correspondence);
                    $s->eq("partner", $partnerIDs);
                    $figures["Correspondence Items"] = $s->count();

                    foreach ($figures as $key => $value) {
                        $figures[$key] = "<tr><th>$key</th><td>$value</td></tr>";
                    }

                    $json["figures"] = implode("\n", $figures);
                } else {
                    $json["cGraph"]["data"] = "";
                    $json["figures"] = "There are no statistics available for this practice";
                }


                echo json_encode($json);

                break;

            case "list_alias":
                $json = [
                    "status" => false,
                    "error" => false,
                    "data" => false
                ];

                $db = new mydb();
                $q = "SELECT * FROM " . PRACTICE_ALIAS_TBL . " WHERE practice_id = " . $_GET['id'];

                $db->query($q);

                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $json["data"] .= "<tr><td>" . $row['alias'] . "</td></tr>";
                }

                if ($json['data'] != false) {
                    $json['status'] = true;
                }

                echo json_encode($json);

                break;

            case "add_alias":
                if (isset($_GET['alias'])) {
                    $pa = new PracticeAlias();

                    $pa->alias->set($_GET['alias']);
                    $pa->practice_id->set($_GET['id']);

                    $json->status = (bool)$pa->save();

                    echo json_encode($json);
                } else {
                    echo Practice::get_template("add_alias.html", ["practice" => $_GET['id']]);
                }

                break;

            case "notes":
                $json = [
                    "status" => false,
                    "error" => null,
                    "page_no" => null,
                    "pages" => null,
                    "row_count" => null,
                    "offset" => null,
                    "data" => null
                ];

                try {
                    $db = new mydb();

                    //set offset
                    if (!isset($_GET['offset'])) {
                        $offset = 0;
                    } else {
                        $offset = (int)$_GET['offset'] * 20;
                        $json['offset'] = (int)$_GET['offset'] * 20;
                    }

                    //set limit
                    $limit = 20;

                    //use specific query dependant upon which object is selected
                    if ($_GET['object'] == "Partner") {
                        $q = "SELECT  n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                INNER JOIN " . PARTNER_TBL . " pa
                                    ON n.object_id = pa.partnerid
                                INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                    ON pa.partnerid = ps.partner_id
                                WHERE n.object = 'Partner'
                                    AND ps.practice_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1
                                GROUP BY n.id
                                ORDER BY n.id DESC
                                LIMIT " . $limit . " OFFSET " . $offset;

                        $q_count = "SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    INNER JOIN " . PARTNER_TBL . " pa
                                        ON n.object_id = pa.partnerid
                                    INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                        ON pa.partnerid = ps.partner_id
                                    WHERE n.object = 'Partner'
                                        AND ps.practice_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1";
                    } else if ($_GET['object'] == "Client") {
                        $q = "SELECT n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                INNER JOIN " . CLIENT_TBL . " cl
                                    ON n.object_id = cl.clientid
                                INNER JOIN " . PARTNER_TBL . " pa
                                    ON cl.partnerid = pa.partnerid
                                INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                    ON pa.partnerid = ps.partner_id
                                WHERE n.object = 'Client'
                                    AND ps.practice_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1
                                ORDER BY n.id DESC
                                LIMIT " . $limit . " OFFSET " . $offset;

                        $q_count = "SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    INNER JOIN " . CLIENT_TBL . " cl
                                        ON n.object_id = cl.clientid
                                    INNER JOIN " . PARTNER_TBL . " pa
                                        ON cl.partnerid = pa.partnerid
                                    INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                        ON pa.partnerid = ps.partner_id
                                    WHERE n.object = 'Client'
                                        AND ps.practice_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1";
                    } else if ($_GET['object'] == "Policy") {
                        $q = "SELECT n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                INNER JOIN " . POLICY_TBL . " po
                                    ON n.object_id = po.policyid
                                INNER JOIN " . CLIENT_TBL . " cl
                                    ON po.clientid = cl.clientid
                                INNER JOIN " . PARTNER_TBL . " pa
                                    ON cl.partnerid = pa.partnerid
                                INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                    ON pa.partnerid = ps.partner_id
                                WHERE n.object = 'Policy'
                                    AND ps.practice_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1
                                ORDER BY n.id DESC
                                    LIMIT " . $limit . " OFFSET " . $offset;

                        $q_count = "SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    INNER JOIN " . POLICY_TBL . " po
                                        ON n.object_id = po.policyid
                                    INNER JOIN " . CLIENT_TBL . " cl
                                        ON po.clientid = cl.clientid
                                    INNER JOIN " . PARTNER_TBL . " pa
                                        ON cl.partnerid = pa.partnerid
                                    INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                        ON pa.partnerid = ps.partner_id
                                    WHERE n.object = 'Policy'
                                        AND ps.practice_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1";
                    } else if ($_GET['object'] == "Practice") {
                        $q = "SELECT n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                WHERE n.object = 'Practice'
                                    AND object_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1
                                GROUP BY n.id ORDER BY n.id DESC LIMIT " . $limit . " OFFSET " . $offset;

                        $q_count = "SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    WHERE n.object = 'Practice'
                                        AND object_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1";
                    } else if ($_GET['object'] == "All") {
                        $q = "SELECT  n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                INNER JOIN " . PARTNER_TBL . " pa
                                    ON n.object_id = pa.partnerid
                                INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                    ON pa.partnerid = ps.partner_id
                                WHERE n.object = 'Partner'
                                    AND ps.practice_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1
                                GROUP BY n.id

                                UNION

                                SELECT n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                INNER JOIN " . CLIENT_TBL . " cl
                                    ON n.object_id = cl.clientid
                                INNER JOIN " . PARTNER_TBL . " pa
                                    ON cl.partnerid = pa.partnerid
                                INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                    ON pa.partnerid = ps.partner_id
                                WHERE n.object = 'Client'
                                    AND ps.practice_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1

                                UNION

                                SELECT n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                INNER JOIN " . POLICY_TBL . " po
                                    ON n.object_id = po.policyid
                                INNER JOIN " . CLIENT_TBL . " cl
                                    ON po.clientid = cl.clientid
                                INNER JOIN " . PARTNER_TBL . " pa
                                    ON cl.partnerid = pa.partnerid
                                INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                    ON pa.partnerid = ps.partner_id
                                WHERE n.object = 'Policy'
                                    AND ps.practice_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1

                                UNION

                                SELECT n.id, n.object, n.object_id, n.text, n.added_when, n.added_by, n.note_type FROM " . NOTES_TBL . " n
                                WHERE n.object = 'Practice'
                                    AND object_id = " . $_GET['id'] . "
                                    AND n.is_visible = 1
                                GROUP BY n.id

                                ORDER BY id DESC LIMIT " . $limit . " OFFSET " . $offset;

                        $q_count = "SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    WHERE n.object = 'Practice'
                                        AND object_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1

                                    UNION

                                    SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    INNER JOIN " . POLICY_TBL . " po
                                        ON n.object_id = po.policyid
                                    INNER JOIN " . CLIENT_TBL . " cl
                                        ON po.clientid = cl.clientid
                                    INNER JOIN " . PARTNER_TBL . " pa
                                        ON cl.partnerid = pa.partnerid
                                    INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                        ON pa.partnerid = ps.partner_id
                                    WHERE n.object = 'Policy'
                                        AND ps.practice_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1

                                    UNION

                                    SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    INNER JOIN " . CLIENT_TBL . " cl
                                        ON n.object_id = cl.clientid
                                    INNER JOIN " . PARTNER_TBL . " pa
                                        ON cl.partnerid = pa.partnerid
                                    INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                        ON pa.partnerid = ps.partner_id
                                    WHERE n.object = 'Client'
                                        AND ps.practice_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1

                                    UNION

                                    SELECT SQL_NO_CACHE COUNT(*) FROM " . NOTES_TBL . " n
                                    INNER JOIN " . PARTNER_TBL . " pa
                                        ON n.object_id = pa.partnerid
                                    INNER JOIN " . PRACTICE_STAFF_TBL . " ps
                                        ON pa.partnerid = ps.partner_id
                                    WHERE n.object = 'Partner'
                                        AND ps.practice_id = " . $_GET['id'] . "
                                        AND n.is_visible = 1";
                    } else {
                        throw new Exception("Unrecognised object given");
                    }

                    //$q_count used for pagination
                    $db->query($q_count);

                    //result($x) & page count
                    if ($_GET['object'] == "All") {
                        while ($row = $db->next(MYSQLI_ASSOC)) {
                            $total_practic_notes += $row["COUNT(*)"];
                        }
                        $x = (int)$total_practic_notes;
                    } else {
                        $x = $db->next(MYSQLI_NUM);

                        if (is_array($x)){
                            $x = (int)array_shift($x);
                        } else {
                            $x = 0;
                        }
                    }
                    $pages = $x / 20;

                    //create array used to populate select#page_number
                    $i = 0;
                    $page_no = [];
                    while ($i <= $pages) {
                        $i++;
                        $page_no[] = $i;
                    }

                    $db->query($q);

                    $type = Notes::get_types();
                    $note_types = implode(',', $type);

                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        if (in_array($row['object'], $type)) {
                            $objtype = $row['object'];
                            $obj = new $objtype($row['object_id'], true);
                        }

                        if (isset($note['id'])) {
                            $parent = Notes::get_parent($note['id']);
                        }

                        //format date
                        if ($row['added_when'] != 0) {
                            $addedwhen = date('H:i  d/m/Y', $row['added_when']);
                        } else {
                            $addedwhen = "n/a";
                        }

                        //get user objected for displaying note author
                        $user = new User($row['added_by'], true);

                        //display note type with image
                        if ($row['note_type'] == 'phone') {
                            $notetype = "<img id='phone' src='/lib/images/phone_16.png'/>&nbsp; Phonecall";
                        } else if ($row['note_type'] == 'email') {
                            $notetype = "<img id='email' src='/lib/images/email_16.png'/>&nbsp; Email";
                        } else if ($row['note_type'] == 'meet') {
                            $notetype = "<img id='meet' src='/lib/images/meet_16.png'/>&nbsp; Meeting";
                        } else if ($row['note_type'] == 'vunerable') {
                            $notetype = "<img id='vunerable' src='/lib/images/vunerable_16.png'/>&nbsp; Vunerable Client";
                        } else if ($row['note_type'] == 'other') {
                            $notetype = "<img id='other' src='/lib/images/other_16.png'/>&nbsp; Other";
                        } else {
                            $notetype = "<img id='note' src='/lib/images/note_16.png'/>&nbsp; Note";
                        }

                        //create some rows!
                        $json['data'] .= "<tr>
                                            <td>" . $row['id'] . "</td>
                                            <td>" . $row['object'] . "</td>
                                            <td>" . $obj->link($obj->id->__toString(), $row['object_id']) . "</td>
                                            <td>" . $addedwhen . "</td>
                                            <td>" . ucwords(str_replace(".", " ", $user->username)) . "</td>
                                            <td>" . $notetype . "</td>
                                        </tr>
                                        <tr><td colspan='6'><b><i>" . $row['text'] . "</i></b></td></tr>";
                    }

                    if ($json['data'] != null) {
                        $json['status'] = "Query Successfully Executed";

                        //fix for encoding issue with certain characters
                        $json['data'] = str_replace(utf8_decode("£"), "&pound;", $json['data']);
                        $json['data'] = str_replace(utf8_decode("Â"), "&nbsp;", $json['data']);
                        $json['data'] = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $json['data']);
                    } else {
                        $json['status'] = "No Notes found";
                    }

                    $json['page_no'] = $page_no;
                    $json['row_count'] = $x;
                    $json['pages'] = ceil($pages);
                } catch (Exception $e) {
                    $json["error"] = $e->getMessage();
                }

                echo json_encode($json);

                break;

            case "clients":
                function get_initials()
                {
                    global $client;

                    $numeric = $special = false;
                    $letters = [];
                    $db = new mydb();
                    $c = new Client;
                    $partners = [];

                    if ($_GET['id']) {
                        //search for all partner ids within our $_GET['id'] practice value
                        $db->query(
                            "select partner_id from practice_staff where partner_id is not null and active = 1 and practice_id = " . $_GET['id']
                        );

                        while ($p = $db->next(MYSQLI_NUM)) {
                            $partners[] = $p[0];
                        }

                        if (!empty($partners)) {
                            $db->query(
                                "select distinct(initial) from (" .
                                "( select distinct(ucase(substr(" . $c->one->surname->get_field_name() . ",1,1))) as initial " .
                                "  from " . $c->get_table() . " where " . $c->partner->get_field_name() . " in (" . implode(",", $partners) . " )) " .
                                "union all " .
                                "( select distinct(ucase(substr(" . $c->two->surname->get_field_name() . ",1,1))) as initial " .
                                "  from " . $c->get_table() . " where " . $c->partner->get_field_name() . " in (" . implode(",", $partners) . " )) " .
                                ") as clients"
                            );

                            while (list($i) = $db->next(MYSQLI_NUM)) {
                                if (ctype_alpha($i)) {
                                    $letters[] = $i;
                                } else if (ctype_digit($i)) {
                                    $numeric = true;
                                } else if (ctype_graph($i)) {
                                    $special = true;
                                }
                            }
                            sort($letters);
                        }
                    }

                    return ["letters" => $letters, "numeric" => $numeric, "special" => $special];
                }

                function get_list($initial = false, $limit, $offset = 0, $search = false)
                {
                    global $practice;
                    $db = new mydb();

                    $partners = [];

                    //search for all partner ids within our $_GET['id'] practice value
                    $db->query(
                        "select partner_id from practice_staff where partner_id is not null and practice_id = " . $_GET['id']
                    );

                    while ($p = $db->next(MYSQLI_NUM)) {
                        $partners[] = $p[0];
                    }

                    $src = "";

                    if (sizeof($partners)){
                        $s = new Search(new Client);

                        $s->eq("partner", $partners);

                        if ($initial !== false) {
                            if (ctype_alpha($initial)) {
                                $s->add_or(
                                    $s->eq("one->surname", "$initial%", true, false),
                                    $s->eq("two->surname", "$initial%", true, false)
                                );
                            } else if ($initial == "0-9") {
                                $s->add_or(
                                    $s->object->one->surname->get_field_name() . " REGEXP'^[0-9]'",
                                    $s->object->two->surname->get_field_name() . " REGEXP'^[0-9]'"
                                );
                            } else if ($initial == "*") {
                                $s->add_or(
                                    $s->object->one->surname->get_field_name() . " REGEXP'^\\\*'",
                                    $s->object->two->surname->get_field_name() . " REGEXP'^\\\*'"
                                );
                            }
                        }

                        if ($search !== false) {
                            // if we're searching postcodes, we should input properly with spaces.
                            // Let this line take care of whether or not we have the postcode stored with a space
                            $wSearch = str_replace(" ", "%", $search);

                            $s->add_or(
                                $s->eq("one->forename", "%$search%", true, false),
                                $s->eq("two->forename", "%$search%", true, false),
                                $s->eq("one->surname", "%$search%", true, false),
                                $s->eq("two->surname", "%$search%", true, false),
                                $s->eq("address->postcode", "%$search%", true, false),
                                $s->eq("address->postcode", "%$wSearch%", true, false)
                            );
                        }

                        $s->order(["one->surname", "one->forename", "two->surname", "two->forename"]);


                        $s->set_limit($limit);
                        $s->set_offset($offset);

                        $t = Template(Client::get_template_path("list_row.html"));
                        while ($c = $s->next()) {
                            $t->tag($c, "client");
                            $src .= $t;
                        }
                    }

                    return $src;
                }

                if (isset($_GET["initial"])) {
                    $initial = ($_GET["initial"] != "all") ? $_GET["initial"] : false;
                    $list = get_list($initial, $_GET["limit"], $_GET["offset"]);
                    echo json_encode(["clients" => $list]);
                } elseif (isset($_GET["search"])) {
                    $list = get_list(false, $_GET["limit"], $_GET["offset"], $_GET["search"]);
                    echo json_encode(["clients" => $list]);
                } else {
                    $initials = get_initials();

                    $i = (!empty($initials["letters"][0])) ? $initials["letters"][0] : false;

                    foreach (["*" => $initials["special"], "0-9" => $initials["numeric"]] as $sym => $x) {
                        if ($x) {
                            $initials["letters"][] = $sym;
                        }
                    }

                    $r = [];

                    $r["initials"] = $initials["letters"];
                    $r["clients"] = get_list($i, $_GET["limit"], $_GET["offset"]);
                    echo json_encode($r);
                }
                break;

            case "schemes":
                function get_initials()
                {
                    global $client;

                    $numeric = $special = false;
                    $letters = [];
                    $db = new mydb();
                    $c = new Client;
                    $partners = [];

                    if ($_GET['id']) {
                        //search for all partner ids within our $_GET['id'] practice value
                        $db->query(
                            "select partner_id from practice_staff where partner_id is not null and active = 1 and practice_id = " . $_GET['id']
                        );

                        while ($p = $db->next(MYSQLI_NUM)) {
                            $partners[] = $p[0];
                        }

                        if (!empty($partners)) {
                            $db->query(
                                "select distinct(initial) from (" .
                                "( select distinct(ucase(substr(" . $c->one->surname->get_field_name() . ",1,1))) as initial " .
                                "  from " . $c->get_table() . " where " . $c->partner->get_field_name() . " in (" . implode(",", $partners) . " ) and group_scheme = 1)" .
                                "union all " .
                                "( select distinct(ucase(substr(" . $c->two->surname->get_field_name() . ",1,1))) as initial " .
                                "  from " . $c->get_table() . " where " . $c->partner->get_field_name() . " in (" . implode(",", $partners) . " ) and group_scheme = 1)" .
                                ") as clients"
                            );

                            while (list($i) = $db->next(MYSQLI_NUM)) {
                                if (ctype_alpha($i)) {
                                    $letters[] = $i;
                                } else if (ctype_digit($i)) {
                                    $numeric = true;
                                } else if (ctype_graph($i)) {
                                    $special = true;
                                }
                            }
                            sort($letters);
                        }
                    }

                    return ["letters" => $letters, "numeric" => $numeric, "special" => $special];
                }

                function get_list($initial = false, $limit, $offset = 0, $search = false)
                {
                    global $practice;
                    $db = new mydb();

                    $partners = [];

                    //search for all partner ids within our $_GET['id'] practice value
                    $db->query(
                        "select partner_id from practice_staff where partner_id is not null and practice_id = " . $_GET['id']
                    );

                    while ($p = $db->next(MYSQLI_NUM)) {
                        $partners[] = $p[0];
                    }

                    $src = "";

                    if (sizeof($partners)){
                        $s = new Search(new Client);

                        $s->eq("partner", $partners);
                        $s->eq("group_scheme", 1);

                        if ($initial !== false) {
                            if (ctype_alpha($initial)) {
                                $s->add_or(
                                    $s->eq("one->surname", "$initial%", true, false),
                                    $s->eq("two->surname", "$initial%", true, false)
                                );
                            } else if ($initial == "0-9") {
                                $s->add_or(
                                    $s->object->one->surname->get_field_name() . " REGEXP'^[0-9]'",
                                    $s->object->two->surname->get_field_name() . " REGEXP'^[0-9]'"
                                );
                            } else if ($initial == "*") {
                                $s->add_or(
                                    $s->object->one->surname->get_field_name() . " REGEXP'^\\\*'",
                                    $s->object->two->surname->get_field_name() . " REGEXP'^\\\*'"
                                );
                            }
                        }

                        if ($search !== false) {
                            $s->add_or(
                                $s->eq("one->surname", "$search%", true, false),
                                $s->eq("two->surname", "$search%", true, false)
                            );
                        }

                        $s->order(["one->surname", "one->forename", "two->surname", "two->forename"]);


                        $s->set_limit($limit);
                        $s->set_offset($offset);

                        $t = Template(Client::get_template_path("list_row.html"));
                        while ($c = $s->next()) {
                            $t->tag($c, "client");
                            $src .= $t;
                        }
                    }

                    return $src;
                }

                if (isset($_GET["initial"])) {
                    $initial = ($_GET["initial"] != "all") ? $_GET["initial"] : false;
                    $list = get_list($initial, $_GET["limit"], $_GET["offset"]);
                    echo json_encode(["clients" => $list]);
                } elseif (isset($_GET["search"])) {
                    $list = get_list(false, $_GET["limit"], $_GET["offset"], $_GET["search"]);
                    echo json_encode(["clients" => $list]);
                } else {
                    $initials = get_initials();

                    $i = (!empty($initials["letters"][0])) ? $initials["letters"][0] : false;

                    foreach (["*" => $initials["special"], "0-9" => $initials["numeric"]] as $sym => $x) {
                        if ($x) {
                            $initials["letters"][] = $sym;
                        }
                    }

                    $r = [];

                    $r["initials"] = $initials["letters"];
                    $r["clients"] = get_list($i, $_GET["limit"], $_GET["offset"]);
                    echo json_encode($r);
                }
                break;

            case "clients_by_issuer":
                $out = [];

                //get a list of all partner ID's within the practice
                $db = new mydb();

                $db->query(
                    "SELECT partner_id FROM " . DATA_DB . ".practice_staff where practice_id = " . $_GET['id'] . " and partner_id is not null;"
                );

                $partners = [];

                while($partner = $db->next(MYSQLI_ASSOC))
                {
                    $partners[] = $partner['partner_id'];
                }

                if (!isset($_GET["issuer"])) {

                    /** need to prepare a list of issuers **/

                    // necessary objects
                    $i = new Issuer;
                    $c = new Client;
                    $p = new Policy;

                    $db->query(
                        "SELECT " . $i->id->get_field_name() . ", " . $i->name->get_field_name() . " " .
                        "FROM " . $c->get_table() . " " .
                        "LEFT JOIN " . $p->get_table() . " ON " . $c->id->get_field_name() . " = " . $p->client->get_field_name() . " " .
                        "INNER JOIN " . $i->get_table() . " ON " . $p->issuer->get_field_name() . " = " . $i->id->get_field_name() . " " .
                        "WHERE " . $c->partner->get_field_name() . " in (" . implode(",", $partners) . ") " .
                        "GROUP BY " . $i->id->get_field_name() . " " .
                        "ORDER BY " . $i->name->get_field_name()
                    );

                    $out["issuers"] = el::dropdown(false, $db, false, false, true);
                } else {
                    $out["clients"] = "";
                    $t = Template(Practice::get_template_path("client_by_issuer_row.html"));

                    foreach($partners as $partner){
                        $s = new Search(new Policy);
                        $s->eq("client->partner", $partner);
                        if ($_GET["issuer"] != "all") {
                            $s->eq("issuer", (int)$_GET["issuer"]);
                        }

                        while ($policy = $s->next()) {
                            $t->tag($policy, "policy");
                            $out["clients"] .= $t;
                        }
                    }
                }

                echo json_encode($out);

                break;
        }
    }
} else {
    function practice_name_search(&$search, $str)
    {
        $search->eq($search->object->name, "%$str[text]%", true);
    }

    $practice = new Practice;
    $search = new Search($practice);

    $search
        ->flag("id", Search::DEFAULT_INT)
        ->flag("name", Search::CUSTOM_FUNC, "practice_name_search", Search::DEFAULT_STRING);

    if (isset($_GET["search"])) {
        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);

        $matches = [];

        while ($match = $search->next($_GET["search"])) {
            $matches["results"][] = ["text" => "$match", "value" => "$match->id"];
        }

        $db = new mydb();
        $q = "SELECT * FROM " . PRACTICE_TBL . " INNER JOIN " . PRACTICE_ALIAS_TBL . " ON " . PRACTICE_TBL . ".id = " . PRACTICE_ALIAS_TBL . " .practice_ID
					WHERE " . PRACTICE_ALIAS_TBL . ".alias LIKE '%" . addslashes($_GET['search']) . "%'";

        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            $matches["results"][] = ['value' => $row['practice_id'], 'text' => $row['alias'] . " (" . $row['name'] . ")"];
        }

        $matches["remaining"] = $search->remaining;
        echo json_encode($matches);
    } else {
        echo new Template("generic/search.html", ["help" => $search->help_html(), "object" => $practice]);
    }
}
