<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";


class CommissionImportFile extends DatabaseObject
{
    const EDI = "EDI";
    const DATAFLOW = "DataFlow";
    const OTHER = "Other";
    var $table = "prophet.commission_import_files";

    public function __construct()
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);
        $this->type = new Field("type");
        $this->uploaded_datetime = new Field("uploaded_datetime");
        $this->imported_by = new Sub("User", "imported_by");
        $this->filename = new Field("filename");
        $this->safe_name = new Field("safe_name");
        $this->completed_datetime = new Field("completed_datetime");
        $this->hash = new Field("SHA1_hash");

        $this->CAN_DELETE = true;
        call_user_func_array(["parent", "__construct"], func_get_args());
    }
}


if (isset($_FILES["file"]) && sizeof($_FILES["file"])) {
    //// Batch files are being uploaded by the User

    // this page is being loaded into an iframe due to the ajax form file upload
    // problem. Therefore prepare a JSON-object containing information about each
    // uploaded file and it's result to be returned to the parent frame.

    header("Content-type: text/plain");

    $json = [];

    $file = $_FILES["file"];

    $json = (object)[
        "id" => null,
        "status" => false, // file upload status
        "error" => null,
        "issuer" => null,  // determine issuer from first line of file
        "preview" => null,  // first ten data lines from import file
        "safename" => null,  // randomly generated filename for import file
        "paid" => null,  // paiddate from imported file
        "type" => null,  // import type
        "zeros" => null,  // 0 amounts in file
        "header" => null,  // Electronic file import column headings
        "commtypes" => null // Commission types from import file for conversion
    ];

    // only accept csv files
    if (in_array($file["type"], ["text/csv", "text/plain", "application/vnd.ms-excel"])) {
        // give file a randomly generated file name so that it doesn't overwrite exisiting file
        $safe_name = random_string(10);

        if (isset($_POST["type"]) && $_POST["type"] == "EDI") {
            file_exists(Batch::get_pending_import_dir_edi() . $safe_name);
        } else if (isset($_POST["type"]) && $_POST["type"] == "Dataflow") {
            file_exists(Batch::get_pending_import_dir_dataflow() . $safe_name);
        } else if (isset($_POST["type"]) && $_POST["type"] == "Other") {
            file_exists(Batch::get_pending_import_dir_other() . $safe_name);
        } else {
            exit("Not a valid upload type");
        }

        // to prevent batch files being imported multiple times, prepare a hash of
        // the file to store and compare against previously imported files
        $content = file_get_contents($file["tmp_name"]);
        $hash = sha1($content);

        // check for potential duplicates
        $s = new Search(new CommissionImportFile);
        $s->eq("hash", $hash);

        $json->matches = [];
        while ($m = $s->next()) {
            $json->matches[] = "$m->filename imported by $m->imported_by at " . date('d/m/Y h:i:s', $m->uploaded_datetime());
        }

        $batchf = new CommissionImportFile;
        $batchf->load([
            "type" => $_POST["type"],
            "imported_by" => $_USER->id(),
            "uploaded_datetime" => time(),
            "safe_name" => $safe_name,
            "filename" => $file["name"],
            "hash" => $hash
        ]);

        if ($batchf->save()) {
            if (isset($_POST["type"])) {
                if ($_POST["type"] == "EDI") {
                    move_uploaded_file($file["tmp_name"], Batch::get_pending_import_dir_edi() . $safe_name);
                } else if ($_POST["type"] == "Dataflow") {
                    move_uploaded_file($file["tmp_name"], Batch::get_pending_import_dir_dataflow() . $safe_name);
                } else if ($_POST["type"] == "Other") {
                    move_uploaded_file($file["tmp_name"], Batch::get_pending_import_dir_other() . $safe_name);
                } else {
                    exit("Not valid import, no files moved");
                }

                $json->status = true;
                $json->id = $batchf->id();

                /*
                Here we need check what kind of file is being imported.
                By definition, EDI files should all be the same format however Dataflow can change by issuer.
                Check the type of file and read in the contents to multidensional array for manipulation.
                */

                // If EDI file, read first 10 lines of file to pre-determine issuer and display data to user
                if ($_POST["type"] == "EDI") {
                    // Open file in read mode
                    $f = fopen(Batch::get_pending_import_dir_edi() . $safe_name, 'r');
                    flock($f, LOCK_SH);

                    // Pre-determine issuer by reading first line of file and regex out the issuer name
                    if ($f) {
                        $line = fgets($f);
                        if (preg_match_all('/\:(.*?)\,/', $line, $match)) {
                            $json->issuer = trim(strtr($match[0][0], [':' => '', ',' => '']));
                            $json->paid = trim(strtr($match[0][3], [':' => '', ',' => '']));
                        }
                    }

                    // Close file
                    flock($f, LOCK_UN);
                    fclose($f);

                    // Open and read file then assign data to multidimensional array -> ten line preview for user
                    $edifile = [];
                    if (($handle = fopen(Batch::get_pending_import_dir_edi() . $safe_name, "r")) !== false) {
                        flock($handle, LOCK_SH);

                        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                            $edifile[] = $data; //add the row to the main array.
                        }
                    }

                    // Close File
                    flock($handle, LOCK_UN);
                    fclose($handle);

                    // Create counter and use to remove '£0.00' amounts after import.
                    $zeros = 0;
                    $size = count($edifile);
                    for ($x = 1; $x < $size; $x++) {
                        if ($edifile[$x][16] == "0") {
                            $zeros++;
                        }
                    }

                    // Ensure that 'needed' columns all have data present
                    $errors = [];
                    $errors_csv = [];
                    $types = [];

                    for ($i = 1; $i < count($edifile); $i++) {
                        if (empty($edifile[$i][4])) {           /* Agency Number */
                            array_push($errors, $i);
                            array_push($errors_csv, $edifile[$i]);
                        } else if (empty($edifile[$i][8])) {     /* Policy Number */
                            array_push($errors, $i);
                            array_push($errors_csv, $edifile[$i]);
                        } else if ($edifile[$i][15] === '') {    /* Commission Types */
                            array_push($errors, $i);
                            array_push($errors_csv, $edifile[$i]);
                        } else if ($edifile[$i][16] === '') {    /* Amount */
                            array_push($errors, $i);
                            array_push($errors_csv, $edifile[$i]);
                        } else if (empty($edifile[$i][12])) {    /* Client Surname */
                            array_push($errors, $i);
                            array_push($errors_csv, $edifile[$i]);
                        } else {
                            // Add commission types from file to array
                            $types[] = trim(strtoupper($edifile[$i][15]));
                            continue;
                        }
                    }

                    // Obtain unique commission types
                    $typeslist = array_unique($types);
                    $json->commtypes = $typeslist;

                    // Output errors/blanks to csv file, if any
                    if (count($errors_csv) > 0) {
                        $fp = fopen(Batch::get_pending_import_dir_errors() . $safe_name . "_errors.csv", 'w');
                        fputcsv($fp, $edifile[0]);
                        foreach ($errors_csv as $fields) {
                            fputcsv($fp, $fields);
                        }
                        fclose($fp);
                    }

                    $json->error = $errors;
                    $json->preview = $edifile;
                    $json->safename = Batch::get_pending_import_dir_edi() . $safe_name;
                    $json->type = "EDI";
                    $json->zeros = $zeros;
                } else if ($_POST["type"] == "Dataflow") {
                    // Open and read file then assign data to multidimensional array -> ten line preview for user
                    $dataflowfile = [];

                    if (($handle = fopen(Batch::get_pending_import_dir_dataflow() . $safe_name, "r")) !== false) {
                        flock($handle, LOCK_SH);

                        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                            $dataflowfile[] = $data; //add the row to the main array.
                        }
                    }

                    // Close File
                    flock($handle, LOCK_UN);
                    fclose($handle);

                    // Create counter and use to remove '£0.00' amounts after import.
                    $zeros = 0;
                    $size = count($dataflowfile);
                    for ($x = 1; $x < $size; $x++) {
                        if ($dataflowfile[$x][7] == "0") {
                            $zeros++;
                        }
                    }

                    // Ensure that 'needed' columns all have data present
                    $errors = [];
                    $errors_csv = [];
                    $types = [];

                    for ($i = 1; $i < count($dataflowfile); $i++) {
                        if (empty($dataflowfile[$i][0])) {           /* Agency Number */
                            array_push($errors, $i);
                            array_push($errors_csv, $dataflowfile[$i]);
                        } else if (empty($dataflowfile[$i][5])) {     /* Policy Number */
                            array_push($errors, $i);
                            array_push($errors_csv, $dataflowfile[$i]);
                        } else if (empty($dataflowfile[$i][6])) {     /* Commission Type */
                            array_push($errors, $i);
                            array_push($errors_csv, $dataflowfile[$i]);
                        } else if ($dataflowfile[$i][7] === '') {    /* Amount */
                            array_push($errors, $i);
                            array_push($errors_csv, $dataflowfile[$i]);
                        } else if (trim($dataflowfile[$i][11]) == false) {    /* Client Surname */
                            array_push($errors, $i);
                            array_push($errors_csv, $dataflowfile[$i]);
                        } else {
                            // Add commission types from file to array
                            $types[] = trim(strtoupper($dataflowfile[$i][6]));
                            continue;
                        }
                    }

                    // Obtain unique commission types
                    $typeslist = array_unique($types);
                    $json->commtypes = $typeslist;

                    // Output errors/blanks to csv file, if any
                    if (count($errors_csv) > 0) {
                        $fp = fopen(Batch::get_pending_import_dir_errors() . $safe_name . "_errors.csv", 'w');
                        fputcsv($fp, $dataflowfile[0]);
                        foreach ($errors_csv as $fields) {
                            fputcsv($fp, $fields);
                        }
                        fclose($fp);
                    }

                    $json->paid = $dataflowfile[1][4];
                    $json->error = $errors;
                    $json->preview = $dataflowfile;
                    $json->safename = Batch::get_pending_import_dir_dataflow() . $safe_name;
                    $json->type = "Dataflow";
                    $json->zeros = $zeros;
                } else if ($_POST["type"] == "Other") {
                    // Open and read file then assign data to multidimensional array -> ten line preview for user
                    $otherfile = [];

                    if (($handle = fopen(Batch::get_pending_import_dir_other() . $safe_name, "r")) !== false) {
                        flock($handle, LOCK_SH);

                        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                            $otherfile[] = $data; //add the row to the main array.
                        }
                    }

                    // Close File
                    flock($handle, LOCK_UN);
                    fclose($handle);

                    $json->error = '';
                    $json->preview = $otherfile;
                    $json->safename = Batch::get_pending_import_dir_other() . $safe_name;
                    $json->type = "Other";
                    $json->header = $otherfile[0];
                } else {
                    exit("Not a valid import");
                }
            }
        }

        if (!$json->status) {
            $json->error = "System error, please try again";
        }
    } else {
        $json->error = $file["type"] . " not acceptable";
    }

    print json_encode($json);
} else if (isset($_GET["id"], $_GET["do"]) && intval($_GET["id"])) {
    $importid = (int)$_GET["id"];

    switch ($_GET["do"]) {
        case "delete":
            $json = (object)[
                "id" => null,
                "status" => false,
                "error" => false
            ];

            try {
                $db = new mydb();

                // Delete uploaded file from server
                $db->query("SELECT type, safe_name FROM prophet.commission_import_files WHERE id = " . $importid);

                if ($d = $db->next(MYSQLI_ASSOC)) {
                    if ($d['type'] == "EDI") {
                        unlink(Batch::get_pending_import_dir_edi() . $d['safe_name']);
                    } else if ($d['type'] == "Dataflow") {
                        unlink(Batch::get_pending_import_dir_dataflow() . $d['safe_name']);
                    } else if ($d['type'] == "Other") {
                        unlink(Batch::get_pending_import_dir_other() . $d['safe_name']);
                    } else {
                        exit("Can't determine import type to delete import/file");
                    }
                } else {
                    die("Cant find import file to be deleted");
                }

                // Delete database entry
                $db->query("DELETE FROM prophet.commission_import_files WHERE id = " . $importid);

                $json->status = true;
            } catch (Exception $e) {
                $json->status = false;
                $json->error = $e->getMessage();
            }

            print json_encode($json);

            break;
    }
} else {
    //new pipeline entry object for currency selection
    $pipeline = new PipelineEntry();

    echo Template("commission/import_home.html", ["pipeline" => $pipeline]);
}
