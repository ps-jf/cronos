<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "list":
            set_time_limit(60);

            /*
              Provide a list of Commission/PipelineEntry matching the supplied parameters. This
              is essentially the common.php list method but adapted to allow dynamic property
              filters and both PipelineEntry + Commission entries to be returned in the same response.
             */

            if (isset($_GET["filter"])) {
                if (array_key_exists("date", $_GET["filter"])) {
                    if ($_GET["filter"]["date"] == "pipeline") {
                        $object = [new PipelineEntry];
                        unset($_GET["filter"]["date"]);
                    } else {
                        $object = [new Commission];
                    }
                } else {
                    $object = [new PipelineEntry, new Commission];
                }

                $t = Commission::get_template("list_row.html", ['excludes'=>$_GET["excludes"]]);

                if (isset($_GET["excludes"]) && is_array($_GET["excludes"])) {
                    foreach ($_GET["excludes"] as $x) {
                        $t->tag(true, "exclude_$x");
                    }
                }

                foreach ($object as $o) {
                    $s = new Search($o);
                    $filters = $_GET["filter"];

                    if (get_class($o) == "PipelineEntry") {
                        if (array_key_exists("partner", $filters)) {
                            $s->eq("policy->client->partner", $filters["partner"]);
                            unset($filters["partner"]);
                        }

                        if (array_key_exists("client", $filters)) {
                            $s->eq("policy->client", $filters["client"]);
                            unset($filters["client"]);
                        }

                        $s->cols(
                            ["policy->bulk", "is_bulk"],
                            ["policy->client->partner->single_rate","single_rate"],
                            ["policy->client->partner->bulk_rate","bulk_rate"],
                            ["policy->client->partner->id","partner_id"]
                        );
                    }

                    $s->cols(
                        ["policy->number","policy_num"],
                        ["policy->client->one->surname","client_sname"],
                        ["policy->issuer->name","issuer_name"],
                        ["policy->issuer","issuer_id"],
                        ["type->name","entry_type"],
                        ["partner->id","partner_id"]
                    );

                    $s->add_order("id", Search::ORDER_DESCEND);

                    if (sizeof($filters)) {
                        $s->apply_filters($filters);
                    }

                    if (isset($_GET["limit"])) {
                        $s->set_limit((int) $_GET["limit"]);
                    }

                    if (isset($_GET["offset"])) {
                        $s->set_offset((int) $_GET["offset"]);
                    }

                    if (isset($_GET["order"]) && is_array($_GET["order"])) {
                        $s->order($_GET["order"]);
                    } else {
                        $s->add(["date","DESC"]);
                    }

                    while (list($r, $extra) = $s->next()) {
                        $r->issuer = new Issuer($extra->issuer_id);

                        if (get_class($o) == "PipelineEntry") {
                            //// Emulate a Commission entry by resolving additional properties
                            $r->client = $r->policy->get_object('id')->client;
                            $r->partner = $r->policy->client->get_object()->partner;
                            $r->rate = $r->partner_percent;

                            $r->date = "<i>Pending</i>";

                            // Calculate renewal commission share on the fly
                            if ($r->type() == 0 || $r->type() == 8 || $r->type() == 10 || $r->type() == 12) {
                                $r->rate(( $extra->is_bulk ) ? $extra->bulk_rate : $extra->single_rate);
                            }

                            $r->paid = Field::factory(false)
                                ->set_var(Field::TYPE, Field::CURRENCY);

                            //calculate and transform the vat share on the fly
                            // I use round here to workaround phps inconsistent number_format behaviour with 0.005 rounding
                            $r->vat = Field::factory(false)
                                ->set_var(Field::TYPE, Field::CURRENCY);


                            if ($r->vat_rate() == '4') {
                                $r->vat($r->amount() - round(($r->amount()/(PipelineEntry::VAT_RATE + 1)), 2));
                            } elseif ($r->vat_rate() == '3') {
                                $r->vat($r->amount() - round(($r->amount()/(PipelineEntry::VAT_REDUCED_RATE + 1)), 2));
                            } else {
                                $r->vat(0);
                            }

                            $r->paid->set(( ($r->amount()-$r->vat()) / 100 ) * $r->rate());
                        }

                        $t->tag($r, "commission");
                        $t->tag($extra, "extra_info");
                        echo $t;
                    }
                }
            }
            break;
        
        case "amount_search":

            if (!isset($_GET['amount']) || $_GET['amount'] == "") {
                $y = "<tr><td colspan='10'><span class='bad_news'>Amount cannot be null</span></td></tr>";
                $z = "";
            } else {
                $db = new mydb();

                $q = "SELECT * FROM " . COMM_TBL . " comm
					INNER JOIN " . POLICY_TBL . " p
						ON comm.policyID = p.policyID
					INNER JOIN " . CLIENT_TBL . " cl
						ON p.clientID = cl.clientID
					INNER JOIN " . ISSUER_TBL . " i
						ON  p.issuerID = i.issuerID
					INNER JOIN " . PARTNER_TBL . " ptnr
						ON cl.partnerID = ptnr.partnerID
					INNER JOIN " . COMM_TYPE_TBL . " ct
						ON comm.commissiontype = ct.Commntypeid
				WHERE comm.batchid = " . $_GET['batch'] . " and
				 cast(comm.amount as decimal(10,2)) = CAST(" . $_GET['amount'] . " AS decimal(10,2))";

                $db->query($q);

                $user = new User($_COOKIE['uid'], true);

                while ($x = $db->next(MYSQLI_ASSOC)) {
                    $share = $x['amount'] / 100 * $x['partnerBulkRate'];
                    $y .= "<tr id='" . $x['commissionID'] . "'>
						<td><i>Pending</i></td>
						<td><a href='common/Client/?id=" . $x['clientID'] . "' class='Client profile'>" . $x['clientFname1'] . " " . $x['clientSname1'] . "</a></td>
						<td><a href='common/Policy/?id=" . $x['policyID'] . "' class='Policy profile'>" . $x['policyNum'] . "</a></td>
						<td><a href='common/Issuer/?id=" . $x['issuerID'] . "' class='Issuer profile'>" . $x['issuerName'] . "</a></td>
						<td>" . $x['Description'] . "</td>
						<td><a href='common/Partner/?id=" . $x['partnerID'] . "' class='Partner profile'>" . $x['partnerFname'] . " " . $x['partnerSname'] . "</a></td>
						<td>&pound;" . number_format($x['amount'], 2, '.', ',') . "</td>
						<td>" . $x['partnerBulkRate'] . "%</td>
						<td>&pound;" . number_format($share, 2, '.', ',') . "</td>
						<td><button id='" . $x['commissionID'] . "' name='" . $x['amount'] . "' class='update'>Update</button></td>";

                    #check that user is either IT or finance team leader
                    if ((($user->department() == 7 || $user->department() == 1))) {
                        $y .= "<td><button id='" . $x['commissionID'] . "' name='" . $x['amount'] . "' class='delete'></button></td>";
                    }

                    $y .= "</tr>";
                }

                $db1 = new mydb();
                $q1 = "SELECT * FROM " . COMM_AUDIT_TBL . " comm
						INNER JOIN " . POLICY_TBL . " p
							ON comm.policyID = p.policyID
						INNER JOIN " . CLIENT_TBL . " cl
							ON p.clientID = cl.clientID
						INNER JOIN " . ISSUER_TBL . " i
							ON  p.issuerID = i.issuerID
						INNER JOIN " . PARTNER_TBL . " ptnr
							ON cl.partnerID = ptnr.partnerID
						INNER JOIN " . COMM_TYPE_TBL . " ct
							on comm.Commntype = ct.Commntypeid
						WHERE comm.batchid = " . $_GET['batch'] . "
					  AND 
					  (comm.CommToPartner  = CAST(" . $_GET['amount'] . " AS decimal(8,2)) OR
                      cast(comm.amount as decimal(8,2)) = CAST(" . $_GET['amount'] . "AS decimal(8,2)))";

                $db1->query($q1);

                while ($x = $db1->next(MYSQLI_ASSOC)) {
                    $share = $x['amount'] / 100 * $x['Partnershare'];
                    $z .= "<tr>
							<td>" . date('d/m/Y', strtotime($x['paidDate'])) . "</td>
							<td><a href='common/Client/?id=" . $x['clientID'] . "' class='Client profile'>" . $x['clientFname1'] . " " . $x['clientSname1'] . "</a></td>
							<td><a href='common/Policy/?id=" . $x['policyID'] . "' class='Policy profile'>" . $x['policyNum'] . "</a></td>
							<td><a href='common/Issuer/?id=" . $x['issuerID'] . "' class='Issuer profile'>" . $x['issuerName'] . "</a></td>
							<td>" . $x['Description'] . "</td>
							<td><a href='common/Partner/?id=" . $x['partnerID'] . "' class='Partner profile'>" . $x['partnerFname'] . " " . $x['partnerSname'] . "</a></td>
							<td>&pound;" . number_format($x['amount'], 2, '.', ',') . "</td>
							<td>" . $x['Partnershare'] . "%</td>
							<td>&pound;" . number_format($share, 2, '.', ',') . "</td>
							<td>--</td>";

                    $z .= "</tr>";
                }
            }

            echo json_encode($y . $z);
        
            break;

        case "amounts":
            $matches = [];
            $batch = new Batch($_GET['batchid'], true);
            $partnerProfile = false;

            //lets do this manually, my guessing is it'll be much!! faster
            // get search to do the query building legwork and then nick the query at the end

            if (array_key_exists("date", $_GET["filter"])) {
                if ($_GET["filter"]["date"] == "pipeline") {
                    $object = [new PipelineEntry];
                    unset($_GET["filter"]["date"]);
                } else {
                    $object = [new Commission];
                }
            } else {
                $object = [new PipelineEntry, new Commission];
            }

            $i = $vat_on_hold = $vat_total = $paid_on_hold = $paid_total = $amount_on_hold = $amount_total = 0;

            $matches['held_income'] = false;

            $db = new mydb();

            foreach ($object as $o) {
                $s = new Search($o);
                $filters = $_GET["filter"];
                $filters_oh = $_GET["filter"];

                if (get_class($o) == "PipelineEntry") {
                    if (array_key_exists("partner", $filters)) {
                        $s->eq("policy->client->partner", $filters["partner"]);
                        $partnerProfile = true;
                        unset($filters["partner"]);
                    }

                    if (array_key_exists("client", $filters)) {
                        $s->eq("policy->client", $filters["client"]);
                        unset($filters["client"]);
                    }

                    $s->cols(
                        ["policy->bulk", "is_bulk"],
                        ["policy->client->partner->single_rate","single_rate"],
                        ["policy->client->partner->bulk_rate","bulk_rate"]
                    );

                    $s->eq("on_hold", 0);
                }

                if (sizeof($filters)) {
                    $s->apply_filters($filters);
                }

                if (get_class($o) == "PipelineEntry") {

                    $q = preg_replace('/^SELECT.*FROM/', "SELECT count(commissionid), sum(amount), sum(round(if(`vat`=4,(round((amount / ". (PipelineEntry::VAT_RATE + 1) ."),2)),if(`vat`=3,(round((amount / ". (PipelineEntry::VAT_REDUCED_RATE + 1) ."),2)),amount))*(if(`%agetopartner` != 0,`%agetopartner`,if(`%agetops` = 100,0,if(policyBatched = 1,partnerbulkrate,partnercurrentrate)))/100),2)), sum(if(`vat`=4,amount - round((amount / ". (PipelineEntry::VAT_RATE + 1) ."),2),if(`vat`=3,amount - round((amount / ". (PipelineEntry::VAT_REDUCED_RATE + 1) ."),2),0))) FROM", $s->execute(false, true));

                    //get income on hold amount too
                    $soh = new Search(new PipelineEntry());

                    if (get_class($o) == "PipelineEntry") {
                        if (array_key_exists("partner", $filters_oh)) {
                            $soh->eq("policy->client->partner", $filters_oh["partner"]);
                            unset($filters_oh["partner"]);
                        }

                        if (array_key_exists("client", $filters_oh)) {
                            $soh->eq("policy->client", $filters_oh["client"]);
                            unset($filters_oh["client"]);
                        }

                        $soh->cols(
                            ["policy->bulk", "is_bulk"],
                            ["policy->client->partner->single_rate","single_rate"],
                            ["policy->client->partner->bulk_rate","bulk_rate"]
                        );

                        $soh->eq("on_hold", 1);
                    }

                    if (sizeof($filters_oh)) {
                        $soh->apply_filters($filters_oh);
                    }

                    $q2 = preg_replace('/^SELECT.*FROM/', "SELECT sum(amount), sum(round(if(`vat`=4,(round((amount / ". (PipelineEntry::VAT_RATE + 1) ."),2)),if(`vat`=3,(round((amount / ". (PipelineEntry::VAT_REDUCED_RATE + 1) ."),2)),amount))*(if(`%agetopartner` != 0,`%agetopartner`,if(`%agetops` = 100,0,if(policyBatched = 1,partnerbulkrate,partnercurrentrate)))/100),2)), sum(if(`vat`=4,amount - round((amount / ". (PipelineEntry::VAT_RATE + 1) ."),2),if(`vat`=3,amount - round((amount / ". (PipelineEntry::VAT_REDUCED_RATE + 1) ."),2),0))) FROM", $soh->execute(false, true));
                    $db->query($q2);

                    if ($ioh = $db->next(MYSQLI_NUM)) {

                        $q3 = $soh->execute(false, true);
                        $db->query($q3);
                        $rowcount = $db->affected_rows;

                        $matches['held_income'] = true;
                        $matches['held_count'] = $rowcount;
                        $amount_on_hold += $ioh[0];
                        $paid_on_hold += $ioh[1];
                        $vat_on_hold += $ioh[2];
                    }
                } else {
                    $q = preg_replace('/^SELECT.*FROM/', "SELECT count(auditrefno),sum(amount) , sum(CommtoPartner), sum(vat) FROM", $s->execute(false, true));
                }

                $db->query($q);

                if (!$d = $db->next(MYSQLI_NUM)) {
                    $d = [0,0,0];
                }

                $i = $i + $d[0];
                $amount_total += $d[1];
                $paid_total += $d[2];
                $vat_total += $d[3];
            }

            $difference = (float)$amount_total + $amount_on_hold - (float)$batch->target();

            if ($partnerProfile) {
                $matches["total"] = "<span>&pound; " . number_format((float) $amount_total, 2) . ($amount_on_hold > 0 ? ' to be paid' : '') . "</span>";

                if ($amount_on_hold > 0){
                    $matches["total"] .= "<span class='amount_hide'><br>&pound; " . number_format((float) $amount_on_hold, 2) . " on hold</span>";
                    $matches["total"] .= "<span class='amount_hide'><br>&pound; " . number_format((float) $amount_total + $amount_on_hold, 2) . " TOTAL</span>";
                }

                $matches["paid_total"] = "<span>&pound; " . number_format((float) $paid_total, 2) . ($paid_on_hold > 0 ? ' to be paid' : '') . "</span>";

                if ($paid_on_hold > 0){
                    $matches["paid_total"] .= "<span class='amount_hide'><br>&pound; " . number_format((float) $paid_on_hold, 2) . " on hold</span>";
                    $matches["paid_total"] .= "<span class='amount_hide'><br>&pound; ".number_format((float) $paid_total + $paid_on_hold, 2) . " TOTAL</span>";
                }

                $matches["vat_total"] = "<span>&pound; " . number_format((float) $vat_total, 2) . ($vat_on_hold > 0 ? ' to be paid' : '') . "</span>";

                if ($vat_on_hold > 0){
                    $matches["vat_total"] .= "<span class='amount_hide'><br>&pound; " . number_format((float) $vat_on_hold, 2) . " on hold</span>";
                    $matches["vat_total"] .= "<span class='amount_hide'><br>&pound; ".number_format((float) $vat_total + $vat_on_hold, 2) . " TOTAL</span>";
                }
            } else {
                $matches["total"] = "&pound; " . number_format((float) $amount_total + $amount_on_hold, 2);
                $matches["paid_total"] = "&pound; ".number_format((float) $paid_total + $paid_on_hold, 2);
                $matches["vat_total"] = "&pound; ".number_format((float) $vat_total + $vat_on_hold, 2);
            }

            if ($_GET['batch_only'] == "batchOnly") {
                $matches["difference"] = "&pound; ".number_format((float) $difference, 2);
            }

            $matches["entry_count"] = $i;

            if (!$i && $matches['held_income']) {
                $matches["entry_count"] = $matches['held_count'];
            } else {
                $matches["entry_count"] += $matches['held_count'];
            }

            echo json_encode($matches);

            break;
        
        case "matcher":
            echo new Template("commission/matcher.html");
            break;
        
        case "unmatched":
            $db = new mydb();
            $q ="SELECT count(batch_id) FROM tbl_source WHERE batch_id = ".$_GET['batchID']." AND policynum_matched != -1";
            $db->query($q);
            
            if ($policyCount=$db->next(MYSQLI_ASSOC)) {
                echo json_encode(array_shift($policyCount));
            }
            break;
        
        case "policies":
            $db = new mydb();

            if ($_GET['position2'] == "beginning") {
                $q = "SELECT * FROM tbl_source WHERE batch_id = ".$_GET['batchID']." AND policynum_matched != -1 AND  policyNum LIKE '".$_GET['value2']."%'"; //beginning with
            } else if ($_GET['position2'] == "ending") {
                $q = "SELECT * FROM tbl_source WHERE batch_id = ".$_GET['batchID']." AND policynum_matched != -1 AND  policyNum LIKE '%".$_GET['value2']."'"; //ending in
            } else if ($_GET['position2'] == "length") {
                $q = "SELECT * FROM tbl_source WHERE batch_id = ".$_GET['batchID']." AND policynum_matched != -1 AND LENGTH(policyNum) = ".$_GET['value2']; //length of
            }
            $db->query($q);

            $arrayCount = 0;
            while ($policyCount1=$db->next(MYSQLI_ASSOC)) {
                $arrayCount = $arrayCount +1;
                if ($_GET['position'] == "append") {
                    $policyArray[] = $policyCount1[policyNum].$_GET['value1']; //append
                } else if ($_GET['position'] == "prepend") {
                    $policyArray[] = $_GET['value1'].$policyCount1[policyNum]; //prepend
                }
            }


            /* Throw error when limit of 15000+ reached so to not impact on performance.
             * Also, never saw a file previously with more than 15000 entries, a larger number
             * than this here may indicate that something has went wrong!
             */
            /*
			if ($arrayCount >= 15000){
				echo json_encode("error");
				$pol_match = "void" ;
			}else{
            */
            if (!empty($policyArray)) {
                if (is_array($policyArray)) {
                    $policyArray = implode("','", $policyArray);
                }

                $q = "	SELECT count(DISTINCT tblpolicy.policyNum) FROM tblpolicy
							INNER JOIN tblissuer
							  ON tblpolicy.issuerID = tblissuer.issuerID
							INNER JOIN tbl_source
							  ON tblissuer.issuerName = tbl_source.issuer
							WHERE tbl_source.batch_id = ".$_GET['batchID']."
							  AND tblpolicy.policyNum
							IN ('".$policyArray."')";

                $db->query($q);

                if ($policyCount2=$db->next(MYSQLI_ASSOC)) {
                    $pol_match = array_shift(array_values($policyCount2));
                    echo json_encode(array_shift($policyCount2));
                }

                if (empty($pol_match)) {
                    $pol_match = 0;
                }
            } else {
                if (empty($pol_match)) {
                    $pol_match = 0;
                }
                echo json_encode(0);
            }
            //}
            
            // insert all values into audit table
            $db -> query("INSERT INTO policy_matching_audit (batch_id, unmatched_policies, app_or_pre, app_pre_value, pos_or_len, pos_len_value, policies_matched, added_when, added_by)
							VALUES ('".$_GET['batchID']."',".$_GET['unmatched'].",'".$_GET['position']."','".$_GET['value1']."','".$_GET['position2']."','".$_GET['value2']."','".$pol_match."',".time().",'".User::get_default_instance('id')."')");
            
            break;


        case "mapping":

            $json = [
                "row" => "",
                "error" => false
            ];
            try {
                if (isset($_GET['type'])) {
                    $user = User::get_default_instance();
                    $db = new mydb();
                    $q = "SELECT cct.ID, cct.Issuer, cct.Issuer_ID, cct.Comm_desc, ct.name 
                    FROM commission_conversion_table cct 
                    INNER JOIN commission_type ct ON cct.comm_ps = ct.id 
                    ORDER BY cct.Issuer ASC";
                    $db->query($q);

                    while ($results = $db->next(MYSQLI_ASSOC)) {
                        $i = new Issuer($results['Issuer_ID'], true);
                        $json['row'] .= "<tr>
                            <td class='issuer'>".$i->link()."</td>
                            <td>".$results['Comm_desc']."</td>
                            <td>".$results['name']."</td>";

                        if (($user->department() == 7 || $user->id() == 99 || $user->id() == 203)) {
                            $json['row'] .= "<td><button id = '".$results['ID']."' class='delete' >&nbsp;</button ></td >";
                        }
                        $json['row'] .= "</tr>";
                    }

                    echo json_encode($json);
                } else {
                    echo new Template("commission/mapping.html");
                }
            } catch (Exception $e) {
                $json["error"] = $e -> getMessage();
            }

            break;

        case "add_mapping":
            try {
                if (isset($_GET['issuer'])) {
                    $json = [
                        "status" => false,
                        "message" => null,
                    ];

                    $db = new mydb();
                    $q = "SELECT *
                            FROM commission_conversion_table cct
                            INNER JOIN commission_type ct
                            ON cct.comm_ps = ct.id
                            where issuer_ID =".$_GET['issuer']."
                            and Comm_desc = '".trim($_GET['desc'])."'
                            ORDER BY cct.Issuer ASC";
                    $db->query($q);

                    if ($db->next(MYSQLI_ASSOC)) {
                        $json["status"] = false;
                        $json["message"] = "Entry not added due to duplicate Income Statement Description for this issuer";
                        echo json_encode($json);
                        exit();
                    }

                    $i = new Issuer($_GET['issuer'], true);

                    $db = new mydb();
                    $q = "INSERT INTO commission_conversion_table (Issuer, Comm_desc, comm_ps, Issuer_ID)
							VALUES ('".$i->name()."','".trim($_GET['desc'])."','".$_GET['type']."','".$_GET['issuer']."')";
                    $db->query($q);

                    if ($db) {
                        $json["status"] = true;
                        $json["message"] = "Entry successfully added";
                    } else {
                        $json["message"] = "Error occurred, Please inform IT";
                    }

                    echo json_encode($json);
                } else {
                    $db = new mydb();
                    $q = "SELECT ct.id, ct.name
                      FROM commission_type ct
                      ORDER BY ct.name ASC";
                    $db->query($q);

                    $ct = "<select id='commission_type'>";
                    while ($results = $db->next(MYSQLI_ASSOC)) {
                        $ct .= "<option id='".$results['id']."'>".$results['name']."</option>";
                    }
                    $ct .= "</select>";

                    echo new Template("commission/add_mapping.html", ["comm_type" => $ct]);
                }
            } catch (Exception $e) {
                $json["error"] = $e -> getMessage();
            }

            break;

        case "delete_mapping":
            $json = [
                "status" => false,
                "error" => null
            ];

            try {
                $db = new mydb();
                $q = "DELETE FROM commission_conversion_table WHERE ID = ".$_GET['id'];
                $db->query($q);

                if ($db) {
                    $json["status"] = "Entry successfully Deleted";
                } else {
                    $json['error'] = "An error has occurred please try again or contact IT";
                }

                echo json_encode($json);
            } catch (Exception $e) {
                $json["error"] = $e -> getMessage();
            }

            break;

        case "fe_record":
            if (isset($_GET['action'])) {
                if ($_GET['action'] == "list") {
                    $json = [
                        "status" => false,
                        "error" => null,
                        "data" => null
                    ];

                    $s = new Search(new FinanceElectronicRecord);
                    if ($_GET['search'] == true) {
                        $s -> btw("bank_entry_date", $_GET['from_date'], $_GET['to_date']);
                    }

                    while ($fe = $s->next()) {
                        //define variables
                        $edit = "";
                        $delete = "";

                        if (User::get_default_instance('department')  == 1 || User::get_default_instance('department')  == 7) {
                            $edit = "<button id='".$fe->id()."' class='edit'>Edit</button>";
                            $delete = "<button id='".$fe->id()."' class='delete'>&nbsp;</button>";
                        }

                        $json['data'] .= "<tr>
                                            <td>".$fe->issuer->link()."</td>
                                            <td>".$fe->statement_date."</td>
                                            <td>".$fe->reference."</td>
                                            <td>".$fe->entries."</td>
                                            <td>".$fe->total_value."</td>
                                            <td>".$fe->bank_entry_date."</td>
                                            <td>".$fe->process_date."</td>
                                            <td>".$fe->batch->link()."</td>
                                            <td>".$fe->entry_type."</td>
                                            <td>".$fe->balanced."</td>
                                            <td>".$fe->processed_by."</td>
                                            <td>".$edit.$delete."</td>
                                        </tr>";
                    }

                    // fall back for when no entries are available
                    if ($json['data'] == null) {
                        $json['data'] = "<tr><td>There are no entries to show at this time</td></tr>";
                    } else {
                        $json['status'] = true;
                    }

                    echo json_encode($json);
                } else if ($_GET['action'] == "delete") {
                    $json = [
                        "status" => false,
                        "error" => null
                    ];

                    $db = new mydb();
                    $q = "DELETE FROM finance_electronic_records WHERE ID = ".$_GET['entry_id'];
                    $db->query($q);

                    if ($db) {
                        $json["status"] = "Entry successfully Deleted";
                    } else {
                        $json['error'] = "An error has occurred please try again or contact IT";
                    }

                    echo json_encode($json);
                } else if ($_GET['action'] ==  "edit_fe_record") {
                    $fe = new FinanceElectronicRecord($_GET['entry_id'], true);
                    echo new Template("commission/edit_fe_record.html", ["fe"=>$fe]);
                }
            } else {
                echo new Template("commission/fe_record.html");
            }

            break;
        
        case "invoices":
            if (isset($_GET['template'])) {
                echo new Template("commission/invoices.html");
            } else {
                $json = (object)[
                    "search" => null,
                    "status" => false,
                    "error" => null,
                    "page_no" => null,
                    "pages" => null,
                    "record_count" => null,
                    "data" => null
                ];

                $limit = 15;
                if (!isset($_GET['offset'])) {
                    $offset = 0;
                } else {
                    $offset = (int)$_GET['offset'] * $limit;
                }

                $db = new mydb();
                $q = "SELECT * FROM feebase.invoices";

                $q .= " LEFT JOIN invoice_reminder on invoices.id = invoice_reminder.invoiceID ";

                // build up query depending on search
                // if both search and filter used..
                if ((isset($_GET['filter']) && $_GET['filter'] != "--") && (isset($_GET['search_flag']))  && $_GET['search_flag'] != "--") {
                    if ($_GET['search_flag'] == "policy") {
                        $q .= " INNER JOIN feebase.tblpolicy on invoices.policyID = tblpolicy.policyID 
                        WHERE policyNum like '%" . $_GET['search_criteria'] . "%'";
                    } elseif ($_GET['search_flag'] == "total") {
                        $q .= " WHERE total = '" . $_GET['search_criteria'] . "'";
                    }

                    if ($_GET['filter'] == "paid") {
                        $q .= " AND date_received IS NOT NULL";
                    } elseif ($_GET['filter'] == "unpaid") {
                        $q .= " AND date_received IS NULL";
                    }


                    $q .= " AND reminder_only = 0 ";
                } elseif (isset($_GET['filter']) && $_GET['filter'] != "--") {
                    // if only paid/unpaid dropdown is used..
                    if ($_GET['filter'] == "paid") {
                        $q .= " WHERE date_received IS NOT NULL";
                    } elseif ($_GET['filter'] == "unpaid") {
                        $q .= " WHERE date_received IS NULL";
                    }

                    $q .= " AND reminder_only = 0 ";
                } elseif (isset($_GET['search_flag']) && $_GET['search_flag'] != "--") {
                    // if only policy number/total search function is used..
                    if ($_GET['search_flag'] == "policy") {
                        $q .= " INNER JOIN feebase.tblpolicy on invoices.policyID = tblpolicy.policyID 
                        WHERE policyNum like '%" . $_GET['search_criteria'] . "%'";
                    } elseif ($_GET['search_flag'] == "total") {
                        $q .= " WHERE total = '" . $_GET['search_criteria'] . "'";
                    }

                    $q .= " AND reminder_only = 0 ";
                } else {
                    $q .= " WHERE reminder_only = 0 ";
                }

                // get number of rows
                $q1 = "SELECT FOUND_ROWS();";

                $db -> query($q);
                $db->query($q1);

                //result($x) & page count
                $x = $db->next(MYSQLI_NUM);

                $x = (int)array_shift($x);
                $pages = $x / $limit;

                //create array used to populate select#page_number
                $i = 0;
                $page_no = [];
                while ($i <= $pages) {
                    $i++;
                    $page_no[] = $i;
                }

                if (count($page_no) > ceil($pages)) {
                    array_pop($page_no);
                }


                if (isset($_GET['offset']) && $_GET['offset'] != "all") {
                    $q .= " LIMIT ".$limit." OFFSET ".$offset;
                }

                $data = "";

                $db->query($q);


                while ($invoice = $db->next(MYSQLI_NUM)) {
                    $iv = new Invoices($invoice[0], true);

                    $actioned = "<img src='/lib/images/tick.png' /><br /><button id='".$iv->id."' class='reprint'>Reprint</button>";
                    $actions = "<button id='" . $iv->id . "' class='edit'>Edit</button>
                            <button id='" . $iv->id . "' class='delete'>&nbsp;</button>";

                    $data .= "<tr id='".$iv->id."'>
                                <td>#" . $iv->id . "</td>
                                <td>" . $iv->partner->link() . "</td>
                                <td>" . $iv->policy->client->link() . "</td>
                                <td>" . $iv->policy->link() . "</td>
                                <td>" . $iv->policy->issuer->link() . "</td>
                                <td>" . $iv->reason . "</td>
                                <td>" . $iv->total . "</td>
                                <td>" . $iv->vat . "</td>
                                 <td>" . $iv->object_type . "</td>
                                <td>" . $iv->frequency . "</td>
                                <td>" . $iv->date_invoiced . "</td>
                                <td>" . $iv->date_received . "</td>
                                <td>" . $actioned . "</td>
                                <td>".$actions."</td>
                              </tr>";

                    if (strlen($iv->notes) > 0) {
                        $data .= "<tr>
                                    <td colspan='13'>" . $iv->notes . "</td>
                                  </tr>";
                    }
                }

                if ($data == "") {
                    $data = "<tr><td colspan='8'>There are no invoices to show</td></tr>";
                    $json->error = "No entries found";
                } else {
                    $json->status = true;
                }



                $json -> page_no = $page_no;
                $json -> record_count = $x;
                $json -> pages = ceil($pages);
                $json -> data = $data;


                echo json_encode($json);
            }

            break;

        case "add_invoice":
            $invoice = new Invoices();
            
            if (isset($_GET['partner'])) {
                $json = [
                    "status" => false,
                    "message" => null,
                    "id" => null,
                ];

                // ensure policy belongs to partner
                $policy =  new Policy($_GET['policy'], true);
                $client = new Client($policy->client(), true);

                if ($client->partner() == $_GET['partner']) {
                    $invoice->partner($_GET['partner']);
                    $invoice->issuer($_GET['issuer']);
                    $invoice->policy($_GET['policy']);
                    $invoice->reason($_GET['reason']);
                    $invoice->total($_GET['total']);
                    $invoice->vat($_GET['vat']);
                    $invoice->object_type($_GET['object_type']);
                    $invoice->frequency($_GET['frequency']);
                    $invoice->date_invoiced($_GET['date_invoiced']);
                    $invoice->date_received($_GET['date_received']);
                    $invoice->notes($_GET['notes']);
                    if ($_GET['reminder_only'] == true) {
                        $invoice->reminder_only(1);
                    }
                    if ($invoice->save()) {
                        // if invoice is not a one time invoice, add reminder
                        if ($_GET['frequency'] != 0 || $_GET['reminder_only'] == true) {
                            // get due date based on date invoiced and frequency
                            $due_date = date('Y-m-d', strtotime("+".$_GET['frequency']." months", strtotime($_GET['date_invoiced'])));

                            //  convert date to timestamp
                            $due_date_timestamp = strtotime($due_date);

                            // get date 14 days before invoice due
                            $due = strtotime('-14 day', $due_date_timestamp);
                            

                            // save reminder
                            $invoice_reminder = new InvoiceReminder();
                            $invoice_reminder->invoiceID($invoice->id());
                            $invoice_reminder->date_due($due_date_timestamp);
                            $invoice_reminder->date_resend($due);
                            if ($invoice_reminder->save()) {
                                $json['status'] = true;

                                if ($_GET['reminder_only'] == true) {
                                    $message = "Invoice reminder successfully added";
                                } else {
                                    $message = "Invoice entry successfully added";
                                }

                                $json['message'] = $message;
                                $json['id'] = $invoice->id();
                            } else {
                                $json['status'] = true;
                                $json['message'] = "Invoice entry successfully added";
                            }
                        } else {
                            $json['status'] = true;
                            $json['message'] = "Invoice entry successfully added";
                            $json['id'] = $invoice->id();
                        }
                    } else {
                        $json['status'] = false;
                        $json['message'] = "An error occurred, please contact IT";
                    }
                } else {
                    $json['status'] = false;
                    $json['message'] = "Policy does not belong to Partner";
                }

                echo json_encode($json);
            } else {
                if (isset($_GET['reminder'])) {
                    $reminder = true;
                } else {
                    $reminder = false;
                }

                echo new Template("commission/add_invoice.html", ["invoice"=>$invoice, "reminder"=>$reminder]);
            }

            break;

        case "delete_invoice":
            if (isset($_GET['id'])) {
                $json = [
                    "status" => false,
                    "message" => null,
                ];

                $db = new mydb();
                
                if ($db->query("DELETE FROM feebase.invoices WHERE id = ".$_GET['id'])) {
                    if ($db->query("DELETE FROM feebase.invoice_reminder WHERE invoiceID = ".$_GET['id'])) {
                        $json['status'] = true;
                        $json['message'] = "Invoice entry and relevant reminders successfully deleted";
                    } else {
                        $json['status'] = true;
                        $json['message'] = "Invoice entry successfully deleted, reminder not";
                    }
                } else {
                    $json['status'] = false;
                    $json['message'] = "An error occurred, please contact IT";
                }

                echo json_encode($json);
            }

            break;

        case "edit_invoice":
            if (isset($_GET['invoice_id'])) {
                $invoice = new Invoices($_GET['invoice_id'], true);
                echo new Template("commission/edit_invoice.html", ["invoice" => $invoice]);
            } elseif ($_GET['update'] == true) {
                $json = [
                    "status" => false,
                    "message" => null
                ];

                $invoice = new Invoices($_GET['invoice'], true);
                $invoice->reason($_GET['reason']);
                $invoice->total($_GET['total']);
                $invoice->vat($_GET['vat']);
                $invoice->frequency($_GET['frequency']);
                $invoice->date_invoiced($_GET['date_invoiced']);
                $invoice->date_received($_GET['date_received']);
                $invoice->notes($_GET['notes']);
                if ($invoice->save()) {
                    // if invoice is not a one time invoice, search for reminder and update
                    if ($_GET['frequency'] != 0) {
                        // get due date based on date invoiced and frequency
                        $due_date = date('Y-m-d', strtotime("+".$_GET['frequency']." months", strtotime($_GET['date_invoiced'])));

                        //  convert date to timestamp
                        $due_date_timestamp = strtotime($due_date);

                        // get date 14 days before invoice due
                        $due = strtotime('-14 day', $due_date_timestamp);

                        // get linked reminder and update
                        $s = new Search(new InvoiceReminder);
                        $s->eq("invoiceID", $invoice->id());
                        $s->nt("archived", 1);


                        if ($v = $s->next()) {
                            $invoice_reminder_update = new InvoiceReminder($v->id(), true);
                            $invoice_reminder_update->date_due($due_date_timestamp);
                            $invoice_reminder_update->date_resend($due);
                            if ($invoice_reminder_update->save()) {
                                $json['status'] = true;
                                $json['message'] = "Invoice entry successfully updated";
                            } else {
                                $json['status'] = false;
                                $json['message'] = "Invoice entry successfully updated, reminder not";
                            }
                        } else {
                            $json['status'] = true;
                            $json['message'] = "Invoice entry successfully updated, No reminder to update";
                        }
                    } else {
                        $json['status'] = true;
                        $json['message'] = "Invoice entry successfully updated";
                    }
                } else {
                    $json['status'] = false;
                    $json['message'] = "An error occurred, please contact IT";
                }

                echo json_encode($json);
            }

            break;

        case "invoices_due":
            $json = (object) [
                "search" => null,
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $limit = 15;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            $db = new mydb();
            $q = "SELECT * FROM feebase.invoice_reminder";


            // build up query depending on search
            if (isset($_GET['search_flag']) && $_GET['search_flag'] != "--") {
                // if only policy number/total search function is used..
                if ($_GET['search_flag'] == "policy") {
                    $q .= " INNER JOIN invoices ON invoice_reminder.invoiceID = invoices.id 
                    INNER JOIN feebase.tblpolicy on invoices.policyID = tblpolicy.policyID 
                    WHERE policyNum like '%" . $_GET['search_criteria'] . "%'  AND invoice_reminder.archived = 0";
                } elseif ($_GET['search_flag'] == "total") {
                    $q .= " INNER JOIN invoices ON invoice_reminder.invoiceID = invoices.id 
                    WHERE total = '" . $_GET['search_criteria'] . "'  AND invoice_reminder.archived = 0";
                }
            } else {
                $q .= " WHERE archived = 0";
            }

            $q .= " ORDER BY date_resend";

            // get number of rows
            $q1 = "SELECT FOUND_ROWS();";

            $db -> query($q);
            $db->query($q1);

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);

            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages) {
                $i++;
                $page_no[] = $i;
            }

            if (count($page_no) > ceil($pages)) {
                array_pop($page_no);
            }


            if (isset($_GET['offset']) && $_GET['offset'] != "all") {
                $q .= " LIMIT ".$limit." OFFSET ".$offset;
            }

            $data = "";

            $db->query($q);

            while ($reminder = $db->next(MYSQLI_ASSOC)) {
                $r = new InvoiceReminder($reminder['id'], true);
                $iv = new Invoices($reminder['invoiceID'], true);

                // indicate to user than a invoice is over due to be sent
                if ($r->date_resend() < time()) {
                    $overdue = "style='color:red'";
                } else {
                    $overdue = "";
                }


                $data .= "<tr id='".$r->id."'>
                            <td>#" . $iv->id . "</td>
                            <td>" . $iv->partner->link() . "</td>
                            <td>" . $iv->policy->client->link() . "</td>
                            <td>" . $iv->policy->link() . "</td>
                            <td>" . $iv->policy->issuer->link() . "</td>
                            <td>" . $iv->reason . "</td>
                            <td>" . $iv->total . "</td>
                            <td>" . $iv->vat . "</td>
                            <td>" . $iv->object_type . "</td>
                            <td>" . $iv->frequency . "</td>
                            <td ".$overdue.">" . $r->date_resend . "</td>
                            <td><button id='" . $iv->id . "' class='action'>Action</button>
                            <button id='" . $r->id . "' class='delete'>&nbsp;</button></td>
                          </tr>";

                if (strlen($iv->notes) > 0) {
                    $data .= "<tr>
                            <td colspan='13'>" . $iv->notes . "</td>
                          </tr>";
                }
            }

            if ($data == "") {
                $data = "<tr><td colspan='8'>There are no invoice reminders to show</td></tr>";
                $json->error = "No entries found";
            } else {
                $json->status = true;
            }



            $json -> page_no = $page_no;
            $json -> record_count = $x;
            $json -> pages = ceil($pages);
            $json -> data = $data;


            echo json_encode($json);

            break;

        case "action_invoice":
            if (isset($_GET['invoice_id'])) {
                $invoice = new Invoices($_GET['invoice_id'], true);
                $invoice_reminder = new InvoiceReminder($_GET['invoice_reminder_id'], true);

                echo new Template("commission/action_invoice.html", ["invoice" => $invoice, "invoice_reminder" => $invoice_reminder]);
            } elseif ($_GET['action'] == true) {
                $json = [
                    "status" => false,
                    "message" => null,
                    "id" => null
                ];

                $invoice = new Invoices($_GET['invoice'], true);
                $invoice_new = new Invoices();

                $invoice_new->partner($invoice->partner());
                $invoice_new->issuer($invoice->issuer());
                $invoice_new->policy($invoice->policy());
                $invoice_new->reason($_GET['reason']);
                $invoice_new->total($_GET['total']);
                $invoice_new->vat($_GET['vat']);
                $invoice_new->frequency($_GET['frequency']);
                $invoice_new->object_type($invoice->object_type());
                $invoice_new->date_invoiced($_GET['date_invoiced']);
                $invoice_new->date_received($_GET['date_received']);
                $invoice_new->notes($_GET['notes']);
                if ($invoice_new->save()) {
                    // if invoice is not a one time invoice, add reminder
                    if ($_GET['frequency'] != 0) {
                        // get due date based on date invoiced and frequency
                        $due_date = date(
                            'Y-m-d',
                            strtotime("+" . $_GET['frequency'] . " months", strtotime($_GET['date_invoiced']))
                        );

                        //  convert date to timestamp
                        $due_date_timestamp = strtotime($due_date);

                        // get date 14 days before invoice due
                        $due = strtotime('-14 day', $due_date_timestamp);

                        // save new reminder
                        $invoice_reminder_new = new InvoiceReminder();
                        $invoice_reminder_new->invoiceID($invoice_new->id());
                        $invoice_reminder_new->date_due($due_date_timestamp);
                        $invoice_reminder_new->date_resend($due);
                        if ($invoice_reminder_new->save()) {
                            // archive old reminder
                            $invoice_reminder = new InvoiceReminder($_GET['invoice_reminder'], true);
                            $invoice_reminder->archived(1);
                            if ($invoice_reminder->save()) {
                                $json['status'] = true;
                                $json['message'] = "Invoice entry successfully updated";
                                $json['id'] = $invoice_new->id();
                            } else {
                                $json['status'] = false;
                                $json['message'] = "Invoice entry successfully added, New Reminder not set";
                            }
                        } else {
                            $json['status'] = false;
                            $json['message'] = "An error occurred saving a new reminder, please contact IT";
                        }
                    } else {
                        $json['id'] = $invoice_new->id();
                        // archive old reminder
                        $invoice_reminder = new InvoiceReminder($_GET['invoice_reminder'], true);
                        $invoice_reminder->archived(1);
                        if ($invoice_reminder->save()) {
                            $json['status'] = true;
                            $json['message'] = "Invoice saved";
                        }
                    }
                } else {
                    $json['status'] = false;
                    $json['message'] = "An error occurred saving a new invoice, please contact IT";
                }

                echo json_encode($json);
            }


            break;

        case "archive_invoice":
            if (isset($_GET['id'])) {
                $json = [
                    "status" => false,
                    "message" => null,
                ];

                $db = new mydb();

                if ($db->query("UPDATE feebase.invoice_reminder SET archived = 1 WHERE id = ".$_GET['id'])) {
                        $json['status'] = true;
                        $json['message'] = "Invoice reminders successfully deleted";
                } else {
                    $json['status'] = false;
                    $json['message'] = "An error occurred, please contact IT";
                }

                echo json_encode($json);
            }

            break;

        case "upload_invoice":
            $db = new mydb();
            $q = "SELECT * FROM invoices WHERE id = " . $_GET['invoiceID'];
            $db->query($q);

            while ($invoice = $db->next(MYSQLI_ASSOC)) {
                $invoice_obj = $invoice['object_type'];
                $policy = new Policy($invoice['policyID'], true);
            }

            // set up correspondence entry details
            $date = date("Y-m-d H:i:s");
            $subject = "Invoice";

            if ($invoice_obj == "client") {
                $invoice_object = $policy->client->__toString();
            } elseif ($invoice_obj == "partner") {
                $invoice_object = $policy->client->partner->__toString();
            } elseif ($invoice_obj == "issuer") {
                $invoice_object = $policy->issuer->__toString();
            }

            $message = "Invoice enclosed for ".$invoice_object;
            $letter_type = 28;

            $db->query("INSERT INTO " . CORR_TBL . " (Policy, Client, Partner, dateStamp, Message, Sender, Subject, letter_type )
							VALUES ('" . $policy->id() . "','" . $policy->client->id() . "','" . $policy->client->partner->id() . "','" . $date . "','" . $message . "','" . User::get_default_instance('id') . "','" . $subject . "'," . $letter_type . ")");

            $q = "SELECT LAST_INSERT_ID()";
            $db->query($q);
            if ($x = $db->next(MYSQLI_ASSOC)) {
                $corr_id = array_values($x);
            } else {
                $return['error'] = true;
                $return['status'] = "Correspondence item failed to insert to database";
                echo json_encode($return);
                exit();
            }

            // here we want to run jasper report and move the file to client correspondence folder for uploading on the hour
            if (isset($corr_id[0])) {
                try {
                    // get letter object based on ID
                    $letter_obj = new Letter($letter_type, true);

                    $c = new \Jaspersoft\Client\Client(
                        JASPER_HOST,
                        JASPER_USER,
                        JASPER_PASSWD,
                        ""
                    );

                    $form_values = [];
                    $form_values["id"] = $_GET['invoiceID'];
                    $form_values["correspondence[letter]"][] = $letter_type;
                    
                    // point to prophet_reports dir on rosie
                    $report_url = JASPER_STANDARD_LETTERS . $letter_obj->rosie_file();

                    $report_url = Letter::url_path_corrections($report_url);

                    // check if we have any form data to pass through
                    if (!empty($form_values)) {
                        try {
                            $report = $c->reportService()->runReport($report_url, 'pdf', null, null, $form_values);
                        } catch (Exception $e) {
                            exit('Caught exception: ' . $e->getMessage() . "\n");
                        }
                    } else {
                        $return['error'] = true;
                        $return['status'] = "Report parameters missing, please re-run report.";
                        echo json_encode($return);
                        exit();
                    }


                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Description: File Transfer');
                    header('Content-Disposition: attachment; filename=' . $letter_obj->rosie_file() . '.pdf');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Content-Length: ' . strlen($report));
                    header('Content-Type: application/pdf');

                    // create pdf
                    $file_output = CORRESPONDENCE_PROCESSED_SCAN_DIR . $corr_id[0] .".pdf";

                    file_put_contents($file_output, $report);

                    // check that temporary coversheet has been created, if so continue..
                    if (!file_exists($file_output)) {
                        $json['error'] = true;
                        $json['status'] = "An error has occurred creating coversheet, please contact IT.";
                    } else {
                        $json['error'] = false;
                        $json['status'] = "Standard Letter successfully uploaded.";
                    }
                } catch (Exception $e) {
                    die($e->getMessage());
                }
            } else {
                $return['error'] = true;
                $return['status'] = "Correspondence failed to add to database, contact IT.";
            }

            echo json_encode($json);
            
            break;
    }
}
