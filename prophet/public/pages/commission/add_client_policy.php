<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if ($_GET["do"] == "add") {
    try {
        $json = [];
        $db = new mydb();

        // User now adds a client before hand and then we match policies on policynum form tbl_source to tblpolicy.
        // Lets not let EDI or DATAFLOW add anymore clients/policies
        $db -> query("UPDATE tbl_source " .
                        "INNER JOIN tblpolicy " .
                        "ON (tbl_source.policyNum = tblpolicy.policyNum) AND (tbl_source.issuer_ID = tblpolicy.issuerid) " .
                        "SET tbl_source.clientid = tblpolicy.clientid, tbl_source.policyid = tblpolicy.policyid, tbl_source.policynum_matched = '-1'
                        where tblpolicy.archived is null");

        /* NEW QUERIES FOR COMMISSION ENTRIES AND TIDY UP */

        // Insert new entries into tblcommission for newly created clients / policies
        $db -> query("INSERT INTO tblcommission ( editedWhen, batchID, amount, description, commissiontype, policyID, editedBy, currency_type) " .
                        "SELECT Now() AS date, tbl_source.batch_id, tbl_source.comm, tbl_source.Added_by, tbl_source.commtype_ps, tbl_source.policyID, tbl_source.Added_by, tbl_source.currency_type " .
                        "FROM tbl_source " .
                        "WHERE (((tbl_source.comm)>=0) AND ((tbl_source.commtype_ps)=0) AND ((tbl_source.policynum_matched)='-1') AND (tblpolicy.archived is null) " .
                        "ORDER BY tbl_source.id");


        // Remove entries from tbl_source after clients / policies / commission entries have been added / processed
        $db -> query("DELETE tbl_source.* " .
                        "FROM tbl_source " .
                        "INNER JOIN tblpolicy " .
                        "ON tbl_source.policyID = tblpolicy.policyID " .
                        "WHERE (((tbl_source.comm)>=0) AND ((tbl_source.commtype_ps)=0) AND ((tbl_source.policynum_matched)='-1') AND (tblpolicy.archived is null))");

        $json["status"] = true;
    } catch (Exception $e) {
        $json["status"] = false;
        $json["error"] = $e -> getMessage();
    }

    echo json_encode($json);
}
