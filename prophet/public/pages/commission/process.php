<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["get"])) {
    switch ($_GET["get"]) {
        case "balances":
            $row_t = Template("pipelineentry/balances_row.html");

            $db = new mydb();
            $db->query("SELECT tbl_source.issuer, tbl_source.issuer_ID, tbl_source.batch_id, sum(comm) as pending,
                             (select sum(amount) as processed from tblcommission where batchid = a.batchid) as processed, target
                             FROM tbl_source
                             INNER JOIN tblbatch as a
                             ON tbl_source.batch_id = a.batchid
                             GROUP BY a.batchid
                             ORDER BY issuer");

            while ($source = $db->next(MYSQLI_ASSOC)) {
                $row_t->tag($source, "source");
                $balances .= $row_t;
            }

            echo Template("pipelineentry/balances.html", ["balance_list" => $balances]);

            break;

        case "all_clawback":
            $json = (object)[
                "rows" => null,
                "page_no" => 0,
                "pages" => null,
                "offset" => null,
                "data" => null
            ];

            $data = "";
            $limit = 500;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            $db = new mydb();

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
							tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype, tbl_source.paiddate,
							tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps, tbl_source.issuer_ID,
							tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
							concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
							concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner,
							tblpartner.partnerID, tblpartner.partnerSname, tblpartner.bulkPartner, tblpartner.partnerCurrentRate,
							tblpartner.partnerBulkRate,tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID, tbl_source.currency_type,
							concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
							FROM (tblpartner INNER JOIN (tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) ON tblpartner.partnerID = tblclient.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
							WHERE (((tbl_source.comm)<0)) AND tbl_source.issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "'
							ORDER BY tbl_source.issuer, tblpartner.partnerSname");

            if ($db->affected_rows <= 0) {
                $json->data = "<tr><td>No clawback entries for this issuer.</td></tr>";
                echo json_encode($json);
                exit(); // no clawback entries
            }

            $rows = $db->affected_rows;
            $page_count = ceil($rows / $limit);

            //create array used to populate page number select
            $i = 0;
            $page_no = [];
            while ($i < $page_count) {
                $i++;
                $page_no[] = $i;
            }

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
							tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype, tbl_source.paiddate,
							tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps, tbl_source.issuer_ID,
							tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
							concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
							concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner,
							tblpartner.partnerID, tblpartner.partnerSname, tblpartner.bulkPartner, tblpartner.partnerCurrentRate,
							tblpartner.partnerBulkRate,tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID, tbl_source.currency_type,
							concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
							FROM (tblpartner INNER JOIN (tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) ON tblpartner.partnerID = tblclient.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
							WHERE (((tbl_source.comm)<0) ) AND tbl_source.issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "'
							ORDER BY tbl_source.issuer, tblpartner.partnerSname LIMIT " . $limit . " OFFSET " . $offset);

            while ($claw = $db->next(MYSQLI_ASSOC)) {
                $source = new Source($claw["ID"]);
                $batch = new Batch($claw["batch_id"]);

                if (isset($claw["policyid"])) {
                    $s = new Search(new NewBusiness());
                    $s->eq('policy', $claw["policyid"]);
                    $s->add_or([
                        $s->eq("status", 1, true, 0),
                        $s->eq("status", 8, true, 0)
                    ]);

                    if ($nb = $s->next(MYSQLI_ASSOC)) {
                        $nb_id = $nb->id;
                        $nb_class = "has_nb";
                    } else {
                        $nb_class = "no_nb";
                    }
                } else {
                    $nb_class = "no_nb";
                }

                // only let team leader from finance delete from tbl_source
                $delete = "";
                if (User::get_default_instance('department') == 1 && User::get_default_instance("team_leader") == 1 ||
                    User::get_default_instance('department') == 7) {
                    $delete = "<button id='$claw[ID]' name='$claw[ID]' class='delete_tbl_source'>Delete</button>";
                }

                $data .= "<tr id='$claw[ID]' name='$claw[ID]'>" .
                    "<td>" . $source->issuer($claw["issuerID"])->link($claw["issuer"]) . "</td>" .
                    "<td>" . $source->client($claw["clientid"])->link($claw["clientFname1"] . " " . $claw["clientSname1"]) . "</td>" .
                    "<td>" . $source->policy($claw["policyid"])->link($claw["policyNum"]) . "</td>" .
                    "<td>" . $batch->link($claw["batch_id"]) . "</td>" .
                    "<td>" . $claw["agency_no"] . "</td>" .
                    "<td>" . $source->partner($claw["partnerID"])->link($claw["partner"]) . "</td>" .
                    "<td>" . number_format(floatval($claw["comm"]), 2, '.', ',') . "</td>" .
                    "<td>" . $claw["rate"] . "</td>" .
                    "<td>" . $claw["currency_type"] . "</td>" .
                    "<td>" .
                    "<button id='$claw[ID]_$claw[batch_id]' name='$claw[ID]_$claw[batch_id]' class='clawback $nb_class'>Process</button>" . $delete .
                    "</td>
                          </tr>";
            }

            $json->offset = $offset;
            $json->pages = $page_count;
            $json->rows = $rows;
            $json->page_no = $page_no;
            $json->data = $data;

            echo json_encode($json);

            break;

        case "all_initial":
            $json = (object)[
                "rows" => null,
                "page_no" => 0,
                "pages" => null,
                "offset" => null,
                "data" => null
            ];

            $data = "";
            $limit = 500;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * 500;
            }

            $db = new mydb();

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
							tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype,
							tbl_source.paiddate, tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps,
							tbl_source.issuer_ID, tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
							tblpolicy.policyID, concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
							concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner, tbl_source.agency_no,
							tblpartner.partnerSname, tblpartner.partnerCurrentRate, tblpartner.partnerBulkRate, tblpartner.bulkPartner,
							tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID,tblclient.partnerID, tbl_source.currency_type,
							concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
							FROM ((tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
							WHERE (((tbl_source.commtype_ps)=1) AND (tbl_source.comm > 0) and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "')
							ORDER BY tbl_source.issuer, tblpartner.partnerSname");

            if ($db->affected_rows <= 0) {
                $json->data = "<tr><td>No initial entries for this issuer.</td></tr>";
                echo json_encode($json);
                exit(); // no initial entries
            }

            $rows = $db->affected_rows;
            $page_count = ceil($rows / $limit);

            //create array used to populate page number select
            $i = 0;
            $page_no = [];
            while ($i < $page_count) {
                $i++;
                $page_no[] = $i;
            }

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
							tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype,
							tbl_source.paiddate, tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps,
							tbl_source.issuer_ID, tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
							tblpolicy.policyID, concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
							concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner, tbl_source.agency_no,
							tblpartner.partnerSname, tblpartner.partnerCurrentRate, tblpartner.partnerBulkRate, tblpartner.bulkPartner,
							tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID,tblclient.partnerID, tbl_source.currency_type,
							concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
							FROM ((tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
							WHERE (((tbl_source.commtype_ps)=1) AND (tbl_source.comm > 0) and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "')
							ORDER BY tbl_source.issuer, tblpartner.partnerSname LIMIT " . $limit . " OFFSET " . $offset);

            while ($init = $db->next(MYSQLI_ASSOC)) {
                $source = new Source($init["ID"]);
                $batch = new Batch($init["batch_id"]);

                if (isset($init["policyid"])) {
                    $s = new Search(new NewBusiness());
                    $s->eq('policy', $init["policyid"]);
                    $s->add_or([
                        $s->eq("status", 1, true, 0),
                        $s->eq("status", 8, true, 0)
                    ]);
                    if ($nb = $s->next(MYSQLI_ASSOC)) {
                        $nb_id = $nb->id;
                        $nb_class = "has_nb";
                    } else {
                        $nb_class = "no_nb";
                    }
                } else {
                    $nb_class = "no_nb";
                }

                // Let the team leader of finance be able to delete from tbl_source
                $delete = "";
                if (User::get_default_instance('department') == 1 && User::get_default_instance("team_leader") == 1 ||
                    User::get_default_instance('department') == 7) {
                    $delete = "<button id='$init[ID]' name='$init[ID]' class='delete_tbl_source'>Delete</button>";
                }

                $data .= "<tr id='$init[ID]' name='$init[ID]'>" .
                    "<td>" . $source->issuer($init["issuerID"])->link($init["issuer"]) . "</td>" .
                    "<td>" . $source->client($init["clientid"])->link($init["clientFname1"] . " " . $init["clientSname1"]) . "</td>" .
                    "<td>" . $source->policy($init["policyid"])->link($init["policyNum"]) . "</td>" .
                    "<td>" . $batch->link($init["batch_id"]) . "</td>" .
                    "<td>" . $init["agency_no"] . "</td>" .
                    "<td>" . $source->partner($init["partnerID"])->link($init["partner"]) . "</td>" .
                    "<td>" . number_format(floatval($init["comm"]), 2, '.', ',') . "</td>" .
                    "<td>" . $init["commtype"] . "</td>" .
                    "<td>" . $init["rate"] . "</td>" .
                    "<td>" . $init["currency_type"] . "</td>" .
                    "<td>" .
                    "<button id='$init[ID]_$init[batch_id]' name='$init[ID]_$init[batch_id]' class='initial $nb_class'>Process</button>" . $delete .
                    "</td>
						 </tr>";
            }

            $json->offset = $offset;
            $json->pages = $page_count;
            $json->rows = $rows;
            $json->page_no = $page_no;
            $json->data = $data;

            echo json_encode($json);

            break;

        case "all_aco":
            $json = (object)[
                "rows" => null,
                "page_no" => 0,
                "pages" => null,
                "offset" => null,
                "data" => null
            ];

            $data = "";
            $limit = 500;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * 500;
            }

            $db = new mydb();

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
                                    tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype,
                                    tbl_source.paiddate, tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps,
                                    tbl_source.issuer_ID, tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
                                    tblpolicy.policyID, concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
                                    concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner, tbl_source.agency_no,
                                    tblpartner.partnerSname, tblpartner.partnerCurrentRate, tblpartner.partnerBulkRate, tblpartner.bulkPartner,
                                    tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID,tblclient.partnerID, tbl_source.currency_type,
                                    concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
                                    FROM ((tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
                                    WHERE (((tbl_source.commtype_ps) = 8 ) AND (tbl_source.comm > 0) and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "')
                                    ORDER BY tbl_source.issuer, tblpartner.partnerSname");

            if ($db->affected_rows <= 0) {
                $json->data = "<tr><td>No adviser charging ongoing entries for this issuer.</td></tr>";
                echo json_encode($json);
                exit(); // no aco entries
            }

            $rows = $db->affected_rows;
            $page_count = ceil($rows / $limit);

            //create array used to populate page number select
            $i = 0;
            $page_no = [];
            while ($i < $page_count) {
                $i++;
                $page_no[] = $i;
            }

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
                                    tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype,
                                    tbl_source.paiddate, tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps,
                                    tbl_source.issuer_ID, tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
                                    tblpolicy.policyID, concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
                                    concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner, tbl_source.agency_no,
                                    tblpartner.partnerSname, tblpartner.partnerCurrentRate, tblpartner.partnerBulkRate, tblpartner.bulkPartner,
                                    tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID,tblclient.partnerID, tbl_source.currency_type,
                                    concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
                                    FROM ((tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
                                    WHERE (((tbl_source.commtype_ps) = 8 ) AND (tbl_source.comm > 0) and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "')
                                    ORDER BY tbl_source.issuer, tblpartner.partnerSname LIMIT " . $limit . " OFFSET " . $offset);

            while ($aco = $db->next(MYSQLI_ASSOC)) {
                $source = new Source($aco["ID"]);
                $batch = new Batch($aco["batch_id"]);

                if (isset($aco["policyid"])) {
                    $s = new Search(new NewBusiness());
                    $s->eq('policy', $aco["policyid"]);
                    $s->add_or([
                        $s->eq("status", 1, true, 0),
                        $s->eq("status", 8, true, 0)
                    ]);
                    if ($nb = $s->next(MYSQLI_ASSOC)) {
                        $nb_id = $nb->id;
                        $nb_class = "has_nb";
                    } else {
                        $nb_class = "no_nb";
                    }
                } else {
                    $nb_class = "no_nb";
                }

                // Let the team leader of finance be able to delete from tbl_source
                $delete = "";
                if (User::get_default_instance('department') == 1 && User::get_default_instance("team_leader") == 1 ||
                    User::get_default_instance('department') == 7) {
                    $delete = "<button id='$aco[ID]' name='$aco[ID]' class='delete_tbl_source'>Delete</button>";
                }

                $data .= "<tr id='$aco[ID]' name='$aco[ID]'>" .
                    "<td>" . $source->issuer($aco["issuerID"])->link($aco["issuer"]) . "</td>" .
                    "<td>" . $source->client($aco["clientid"])->link($aco["clientFname1"] . " " . $aco["clientSname1"]) . "</td>" .
                    "<td>" . $source->policy($aco["policyid"])->link($aco["policyNum"]) . "</td>" .
                    "<td>" . $batch->link($aco["batch_id"]) . "</td>" .
                    "<td>" . $aco["agency_no"] . "</td>" .
                    "<td>" . $source->partner($aco["partnerID"])->link($aco["partner"]) . "</td>" .
                    "<td>" . number_format(floatval($aco["comm"]), 2, '.', ',') . "</td>" .
                    "<td>" . $aco["commtype"] . "</td>" .
                    "<td>" . $aco["rate"] . "</td>" .
                    "<td>" . $aco["currency_type"] . "</td>" .
                    "<td>" .
                    "<button id='$aco[ID]_$aco[batch_id]' name='$aco[ID]_$aco[batch_id]' class='aco $nb_class'>Process</button>" . $delete .
                    "</td>
                    </tr>";
            }

            $json->offset = $offset;
            $json->pages = $page_count;
            $json->rows = $rows;
            $json->page_no = $page_no;
            $json->data = $data;

            echo json_encode($json);

            break;

        case "all_other":
            $json = (object)[
                "rows" => null,
                "page_no" => 0,
                "pages" => null,
                "offset" => null,
                "data" => null
            ];

            $data = "";
            $limit = 500;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * 500;
            }

            $db = new mydb();

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
							tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype,
							tbl_source.paiddate, tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps,
							tbl_source.issuer_ID, tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
							tblpolicy.policyID, concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
							concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner, tbl_source.agency_no,
							tblpartner.partnerSname, tblpartner.partnerCurrentRate, tblpartner.partnerBulkRate, tblpartner.bulkPartner,
							tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID,tblclient.partnerID, tbl_source.currency_type,
							concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
							FROM ((tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
							WHERE (((tbl_source.commtype_ps)>5) AND ((tbl_source.commtype_ps)!=8) AND (tbl_source.comm > 0) and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "')
							ORDER BY tbl_source.issuer, tblpartner.partnerSname");

            if ($db->affected_rows <= 0) {
                $json->data = "<tr><td>No other entries for this issuer.</td></tr>";
                echo json_encode($json);
                exit(); // no other entries
            }

            $rows = $db->affected_rows;
            $page_count = ceil($rows / $limit);

            //create array used to populate page number select
            $i = 0;
            $page_no = [];
            while ($i < $page_count) {
                $i++;
                $page_no[] = $i;
            }

            $db->query("SELECT tbl_source.ID, tbl_source.issuer, tbl_source.ref, tbl_source.comm, tbl_source.policyNum,
							tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.comm_total, tbl_source.commtype,
							tbl_source.paiddate, tbl_source.agency_no, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps,
							tbl_source.issuer_ID, tbl_source.Added_by, tbl_source.partnerID, tbl_source.policynum_matched,
							tblpolicy.policyID, concat(tbl_source.clientFname1,' ',tbl_source.clientSname1) AS clientName,
							concat(tblpartner.partnerFname,' ',tblpartner.partnerSname) AS partner, tbl_source.agency_no,
							tblpartner.partnerSname, tblpartner.partnerCurrentRate, tblpartner.partnerBulkRate, tblpartner.bulkPartner,
							tblclient.clientid,tblpolicy.policyid,tblpolicy.issuerID,tblclient.partnerID, tbl_source.currency_type,
							concat(if(tblpolicy.policyBatched=1,partnerBulkRate,partnerCurrentRate),'%') AS rate, tblbranch.branch, tblclient.clientID
							FROM ((tblclient INNER JOIN (tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID) INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID) INNER JOIN tblbranch ON tblpartner.branchID = tblbranch.branchID
							WHERE (((tbl_source.commtype_ps)>5) AND ((tbl_source.commtype_ps)!=8) AND (tbl_source.comm > 0) and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "')
							ORDER BY tbl_source.issuer, tblpartner.partnerSname LIMIT " . $limit . " OFFSET " . $offset);

            while ($other = $db->next(MYSQLI_ASSOC)) {
                $source = new Source($other["ID"]);
                $batch = new Batch($other["batch_id"]);

                if (isset($other["policyid"])) {
                    $s = new Search(new NewBusiness());
                    $s->eq('policy', $other["policyid"]);
                    $s->add_or([
                        $s->eq("status", 1, true, 0),
                        $s->eq("status", 8, true, 0)
                    ]);

                    if ($nb = $s->next(MYSQLI_ASSOC)) {
                        $nb_id = $nb->id;
                        $nb_class = "has_nb";
                    } else {
                        $nb_class = "no_nb";
                    }
                } else {
                    $nb_class = "no_nb";
                }

                // Let the team leader of finance be able to delete from tbl_source
                $delete = "";
                if (User::get_default_instance('department') == 1 && User::get_default_instance("team_leader") == 1 ||
                    User::get_default_instance('department') == 7) {
                    $delete = "<button id='$other[ID]' name='$other[ID]' class='delete_tbl_source'>Delete</button>";
                }

                $data .= "<tr id='$other[ID]' name='$other[ID]'>" .
                    "<td>" . $source->issuer($other["issuerID"])->link($other["issuer"]) . "</td>" .
                    "<td>" . $source->client($other["clientid"])->link($other["clientFname1"] . " " . $other["clientSname1"]) . "</td>" .
                    "<td>" . $source->policy($other["policyid"])->link($other["policyNum"]) . "</td>" .
                    "<td>" . $batch->link($other["batch_id"]) . "</td>" .
                    "<td>" . $other["agency_no"] . "</td>" .
                    "<td>" . $source->partner($other["partnerID"])->link($other["partner"]) . "</td>" .
                    "<td>" . number_format(floatval($other["comm"]), 2, '.', ',') . "</td>" .
                    "<td>" . $other["commtype"] . "</td>" .
                    "<td>" . $other["rate"] . "</td>" .
                    "<td>" . $other["currency_type"] . "</td>" .
                    "<td>" .
                    "<button id='$other[ID]_$other[batch_id]' name='$other[ID]_$other[batch_id]' class='other $nb_class'>Process</button>" . $delete .
                    "</td>
                    </tr>";
            }

            $json->offset = $offset;
            $json->pages = $page_count;
            $json->rows = $rows;
            $json->page_no = $page_no;
            $json->data = $data;

            echo json_encode($json);

            break;

        case "main_unmatched":
            $json = (object)[
                "rows" => null,
                "page_no" => 0,
                "pages" => null,
                "data" => null
            ];

            $db = new mydb();

            $data = "";
            $limit = 500;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * 500;
            }

            //TODO tidy up the horrid str_replace below to acutally do some decoding instead!

            $db->query("select tbl_source.id, tbl_source.policyNum, tbl_source.issuer, tbl_source.agency_no,
                                tbl_source.clientFname1, tbl_source.clientSname1,
                                tbl_source.batch_id, tbl_source.bank_id, tbl_source.comm, issuerid, tbl_source.currency_type
                                from tbl_source
                                inner join tblbatch
                                on tbl_source.batch_id = tblbatch.batchid
                                where policynum_matched = 0 and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "'
                                order by issuer");

            if ($db->affected_rows <= 0) {
                $json->data = "<tr><td>No unmatched entries for this issuer.</td></tr>";
                echo json_encode($json);
                exit(); // no unmatched entries
            }

            $rows = $db->affected_rows;
            $page_count = ceil($rows / $limit);

            //create array used to populate page number select
            $i = 0;
            $page_no = [];
            while ($i < $page_count) {
                $i++;
                $page_no[] = $i;
            }

            $db->query("select tbl_source.id, tbl_source.policyNum, tbl_source.issuer, tbl_source.agency_no,
                                tbl_source.clientFname1, tbl_source.clientSname1,
                                tbl_source.batch_id, tbl_source.bank_id, tbl_source.comm, issuerid, tbl_source.currency_type
                                from tbl_source
                                inner join tblbatch
                                on tbl_source.batch_id = tblbatch.batchid
                                where policynum_matched = 0 and issuer = '" . str_replace("&amp;", "&", $_GET['issuer']) . "'
                                order by issuer LIMIT " . $limit . " OFFSET " . $offset);

            while ($mu = $db->next(MYSQLI_ASSOC)) {
                $issuer = new Issuer($mu["issuerid"]);
                $batch = new Batch($mu["batch_id"]);

                if (isset($mu["policyid"])) {
                    $s = new Search(new NewBusiness());
                    $s->eq('policy', $mu["policyid"]);
                    $s->add_or([
                        $s->eq("status", 1, true, 0),
                        $s->eq("status", 8, true, 0)
                    ]);

                    if ($nb = $s->next(MYSQLI_ASSOC)) {
                        $nb_id = $nb->id;
                        $nb_class = "has_nb";
                    } else {
                        $nb_class = "no_nb";
                    }
                } else {
                    $nb_class = "no_nb";
                }


                // Let the team leader of finance be able to delete from tbl_source
                $delete = "";
                if (User::get_default_instance('department') == 1 && User::get_default_instance("team_leader") == 1 ||
                    User::get_default_instance('department') == 7) {
                    $delete = "<button id='" . $mu["id"] . "' name='" . $mu["id"] . "' class='delete_tbl_source'>Delete</button>";
                }

                $data .= "<tr id='" . $mu["id"] . "'><td id='polnum'>" . $mu["policyNum"] . "</td>" .
                    "<td>" . $mu["clientFname1"] . " " . $mu["clientSname1"] . "</td>" .
                    "<td>" . $issuer->link($mu["issuer"]) . "</td>" .
                    "<td>" . number_format(floatval($mu["comm"]), 2, '.', ',') . "</td>" .
                    "<td>" . $batch->link($mu["batch_id"]) . "</td>" .
                    "<td>" . $mu["agency_no"] . "</td>" .
                    "<td>" . $mu["currency_type"] . "</td>" .
                    "<td><button id='" . $mu["id"] . "' name='" . $mu["issuerid"] . "' class='update_code $nb_class'>Update</button>" . $delete . "</td>" .
                    "</tr>";
            }

            $json->pages = $page_count;
            $json->rows = $rows;
            $json->page_no = $page_no;
            $json->data = $data;

            echo json_encode($json);

            break;

        case "tbl_source_preview":
            try {
                $date = date('d-m-Y_H-i-s');

                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename=source_export_$date.csv");
                header("Pragma: no-cache");
                header("Expires: 0");

                $outstream = fopen("php://output", 'w');

                fputcsv($outstream, ["SourceID", "PolicyNum", "IssuerID", "IssuerName", "AgencyNo", "ClientFname1", "ClientSname1", "BatchID", "BankID", "Commtype_PS", "Comm"]);

                $db = new mydb();

                $db->query("SELECT tbl_source.id, tbl_source.policyNum, tblissuer.issuerID, tblissuer.issuerName, tbl_source.agency_no,
                                    tbl_source.clientFname1, tbl_source.clientSname1, tbl_source.batch_id, tbl_source.bank_id, tbl_source.commtype_ps, tbl_source.comm
                                    FROM tbl_source
                                    INNER JOIN tblissuer
                                    ON tbl_source.issuer_ID = tblissuer.issuerid
                                    LEFT JOIN tblagency_codes
                                    ON tbl_source.agency_no = tblagency_codes.agency
                                    WHERE policynum_matched = 0 AND comm > 0 AND commtype_ps != 1
                                    AND (tblagency_codes.main != '-1' OR tblagency_codes.agency IS NULL)
                                    ORDER BY tbl_source.issuer");

                $list = [];

                while ($ts = $db->next(MYSQLI_ASSOC)) {
                    array_push($list, $ts);
                }

                foreach ($list as $row) {
                    fputcsv($outstream, $row, ',', '"');
                }
            } catch (Exception $e) {
                die("Error creating file, contact IT");
            }

            break;

        case "exportAll":
            //get the issuer
            //get the comm type
            $commType = $_GET['commType'];
            $issuer = html_entity_decode($_GET['issuer']);

            try {
                //Determines which query will be used in the exporting of the data
                if (isset($_GET['commType'])) {
                    if ($commType == " exportall-unmatched") {
                        $heading = ["Issuer", "Client", "Policy", "Income", "Batch", "Agency", "Currency Type"];
                        $querySelection = "SELECT 
                                                    feebase.tbl_source.policyNum,
                                                    CONCAT(feebase.tbl_source.clientFname1,
                                                            ' ',
                                                            feebase.tbl_source.clientSname1) AS clientName,
                                                    feebase.tbl_source.issuer,
                                                    feebase.tbl_source.comm,
                                                    feebase.tbl_source.batch_id,
                                                    feebase.tbl_source.agency_no,
                                                    feebase.tbl_source.currency_type
                                                FROM
                                                    feebase.tbl_source
                                                WHERE
                                                    issuer = '" . $issuer . "'
                                                        AND policynum_matched = '0'";
                    } else if ($commType == " exportall-clawback") {
                        $heading = ["Issuer", "Client", "Policy", "Batch", "Agency", "Partner", "Amount", "Rate", "Currency Type"];
                        $querySelection = "SELECT 
                                             feebase.tbl_source.issuer,
                                             CONCAT( feebase.tbl_source.clientFname1,
                                                    ' ',
                                                     feebase.tbl_source.clientSname1) AS clientName,
                                                     feebase.tblpolicy.policyNum,
                                                      feebase.tbl_source.batch_id,
                                                      feebase.tbl_source.agency_no,
                                                    CONCAT( feebase.tblpartner.partnerFname,
                                                    ' ', 
                                                    feebase.tblpartner.partnerSname) AS partner,
                                                       feebase.tbl_source.comm,
                                                          CONCAT(IF(tblpolicy.policyBatched = 1,
                                                            partnerBulkRate,
                                                            partnerCurrentRate),
                                                        '%') AS rate,
                                             feebase.tbl_source.currency_type
                                         FROM
                                        (feebase.tblpartner
                                        INNER JOIN (feebase.tblclient
                                        INNER JOIN (feebase.tbl_source
                                        INNER JOIN feebase.tblpolicy ON feebase.tbl_source.policyID = feebase.tblpolicy.policyID) ON feebase.tblclient.clientID = feebase.tblpolicy.clientID) ON feebase.tblpartner.partnerID = feebase.tblclient.partnerID)
                                            INNER JOIN
                                        feebase.tblbranch ON feebase.tblpartner.branchID = feebase.tblbranch.branchID
                                    WHERE
                                        (((feebase.tbl_source.comm) < 0))
                                            AND feebase.tbl_source.issuer =  '" . $issuer . "'
                                    ORDER BY feebase.tbl_source.issuer , feebase.tblpartner.partnerSname";
                    } else if ($commType == " exportall-initial") {
                        $heading = ["Issuer", "Client", "Policy", "Batch", "Agency", "Partner", "Amount", "Type", "Rate", "Currency Type"];
                        $querySelection = "SELECT 
                                                     feebase.tbl_source.issuer,
                                                     CONCAT( feebase.tbl_source.clientFname1,
                                                            ' ',
                                                             feebase.tbl_source.clientSname1) AS clientName,
                                                             feebase.tblpolicy.policyNum,
                                                              feebase.tbl_source.batch_id,
                                                              feebase.tbl_source.agency_no,
                                                            CONCAT( feebase.tblpartner.partnerFname,
                                                            ' ', 
                                                            feebase.tblpartner.partnerSname) AS partner,
                                                               feebase.tbl_source.comm,
                                                                 feebase.tbl_source.commtype,
                                                                CONCAT(IF(tblpolicy.policyBatched = 1,
                                                                partnerBulkRate,
                                                                partnerCurrentRate),
                                                            '%') AS rate,
                                                     feebase.tbl_source.currency_type
                                                    FROM
                                                    ((feebase.tblclient
                                                        INNER JOIN (feebase.tbl_source
                                                        INNER JOIN feebase.tblpolicy ON feebase.tbl_source.policyID = feebase.tblpolicy.policyID) ON feebase.tblclient.clientID = feebase.tblpolicy.clientID)
                                                        INNER JOIN feebase.tblpartner ON feebase.tblclient.partnerID = feebase.tblpartner.partnerID)
                                                            INNER JOIN
                                                        feebase.tblbranch ON feebase.tblpartner.branchID = feebase.tblbranch.branchID
                                                    WHERE
                                                    (((feebase.tbl_source.commtype_ps) = 1)
                                                        AND (feebase.tbl_source.comm > 0)
                                                        AND issuer = '" . $issuer . "')
                                                    ORDER BY feebase.tbl_source.issuer , feebase.tblpartner.partnerSname";
                    } else if ($commType == " exportall-ACongoing") {
                        $heading = ["ID", "Issuer", "Client", "Partner", "Policy Number", "Batch", "Agency", "Partner", "Amount", "Type", "Rate", "Currency Type"];
                        $querySelection = "SELECT 
                                                        tbl_source.ID,
                                                        tbl_source.issuer,
                                                        CONCAT(tbl_source.clientFname1,
                                                                ' ',
                                                                tbl_source.clientSname1) AS clientName,
                                                        CONCAT(tblpartner.partnerFname,
                                                                ' ',
                                                                tblpartner.partnerSname) AS partner,
                                                        tblpolicy.policyNum,
                                                        tbl_source.batch_id,
                                                        tbl_source.agency_no,
                                                        tblclient.partnerID,
                                                        tbl_source.comm,
                                                        tbl_source.commtype,
                                                        CONCAT(IF(tblpolicy.policyBatched = 1,
                                                                    partnerBulkRate,
                                                                    partnerCurrentRate),
                                                                '%') AS rate,
                                                        tbl_source.currency_type
                                                    FROM
                                                        ((tblclient
                                                        INNER JOIN (tbl_source
                                                        INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) ON tblclient.clientID = tblpolicy.clientID)
                                                        INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID)
                                                            INNER JOIN
                                                        tblbranch ON tblpartner.branchID = tblbranch.branchID
                                                    WHERE
                                                        (((tbl_source.commtype_ps) = 8)
                                                            AND (tbl_source.comm > 0)
                                                            AND issuer = '" . $issuer . "')
                                                    ORDER BY tbl_source.issuer , tblpartner.partnerSname";
                    } else if ($commType == " exportall-other") {
                        $heading = ["Policy", "Client", "Issuer", "Comm", "Batch", "Agency", "Currency Type"];
                        $querySelection = "SELECT 
                                                    feebase.tbl_source.policyNum,
                                                    CONCAT(feebase.tbl_source.clientFname1,
                                                            ' ',
                                                            feebase.tbl_source.clientSname1) AS clientName,
                                                    feebase.tbl_source.issuer,
                                                    feebase.tbl_source.comm,
                                                    feebase.tbl_source.batch_id,
                                                    feebase.tbl_source.agency_no,
                                                    feebase.tbl_source.currency_type
                                                    FROM
                                                        ((feebase.tblclient
                                                        INNER JOIN (feebase.tbl_source
                                                        INNER JOIN feebase.tblpolicy ON feebase.tbl_source.policyID = feebase.tblpolicy.policyID) ON feebase.tblclient.clientID = tblpolicy.clientID)
                                                        INNER JOIN feebase.tblpartner ON feebase.tblclient.partnerID = feebase.tblpartner.partnerID)
                                                            INNER JOIN
                                                        feebase.tblbranch ON feebase.tblpartner.branchID = feebase.tblbranch.branchID
                                                    WHERE
                                                        (((feebase.tbl_source.commtype_ps) > 5)
                                                            AND ((feebase.tbl_source.commtype_ps) != 8)
                                                            AND (feebase.tbl_source.comm > 0)
                                                            AND issuer = '" . $issuer . "')
                                                    ORDER BY feebase.tbl_source.issuer , tblpartner.partnerSname";
                    } else {
                        die("Error creating file, no Income type found. Please contact IT");
                    }
                }


                $date = date('d-m-Y_H-i-s');

                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename=source_export_$date.csv");
                header("Pragma: no-cache");
                header("Expires: 0");

                $outstream = fopen("php://output", 'w');

                fputcsv($outstream, $heading);

                $db = new mydb();

                $db->query($querySelection);

                $list = [];

                while ($ts = $db->next(MYSQLI_ASSOC)) {
                    array_push($list, $ts);
                }

                foreach ($list as $row) {
                    fputcsv($outstream, $row, ',', '"');
                }
            } catch (Exception $e) {
                die("Error creating file, contact IT");
            }

            break;

        case "delete_from_tblsource":
            $json = [
                "status" => null,
                "error" => null
            ];

            if (isset($_GET['id'])) {
                $db = new mydb();
                if ($db->query("DELETE FROM tbl_source WHERE id = " . $_GET['id'])) {
                    $json["error"] = false;
                    $json["status"] = "Entry successfully deleted.";
                } else {
                    $json["error"] = true;
                    $json["status"] = "Failed to delete entry, please contact IT.";
                }
            }

            echo json_encode($json);

            break;
    }
} else if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "process_initial_auto":
            $json = [];

            try {
                $db = new mydb();
                $db->query("INSERT INTO tblcommission ( editedWhen, batchID, amount, description, commissiontype, policyID, editedBy, currency_type )
                                    SELECT Now(), tbl_source.batch_id, tbl_source.comm, tbl_source.Added_by, '0', tbl_source.policyID, tbl_source.Added_by, tbl_source.currency_type
                                    FROM tbl_source
                                    INNER JOIN tblpolicy
                                    ON tbl_source.policyid = tblpolicy.policyID
                                    WHERE (((tbl_source.comm)>=0 And (tbl_source.comm)<=50) AND ((tbl_source.policynum_matched)=-1) AND (tbl_source.commtype_ps = 1))
                                    AND ( tbl_source.policyID NOT IN (
                                        SELECT tblpolicy.policyID FROM tblpolicy
                                        INNER JOIN tblnewbusiness
                                        ON tblpolicy.policyID = tblnewbusiness.policyID
                                        WHERE tblnewbusiness.status = 1 AND tblnewbusiness.status = 8)
                                    )
                                    ORDER BY tbl_source.id
                                    ");

                $db->query("DELETE tbl_source.*
                                    FROM tbl_source
                                    INNER JOIN tblpolicy
                                    ON tbl_source.policyid = tblpolicy.policyID
                                    WHERE (tbl_source.comm >= 0 AND tbl_source.comm <= 50) AND (tbl_source.policynum_matched = -1) AND (tbl_source.commtype_ps = 1)
                                    AND ( tbl_source.policyID NOT IN (
                                        SELECT tblpolicy.policyID FROM tblpolicy
                                        INNER JOIN tblnewbusiness
                                        ON tblpolicy.policyID = tblnewbusiness.policyID
                                        WHERE tblnewbusiness.status = 1 AND tblnewbusiness.status = 8)
                                    )");

                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "process_clawback_auto":
            $json = [];

            try {
                $db = new mydb();
                $db->query("INSERT INTO tblcommission ( editedWhen, batchID, amount, description, commissiontype, policyID, editedBy, `%agetopartner`, `%agetops`, currency_type )
                                    SELECT Now(), tbl_source.batch_id, tbl_source.comm, tbl_source.Added_by, '2', tbl_source.policyID, tbl_source.Added_by,
                                    If(tblpolicy.policyBatched=1,tblpartner.partnerBulkrate,tblpartner.partnerCurrentRate) AS ptopartner,
                                    If(tblpolicy.policyBatched=1,(100-tblpartner.partnerBulkrate),(100-tblpartner.partnerCurrentRate)) AS ptops,
                                    tbl_source.currency_type
                                    FROM ((tbl_source INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID) INNER JOIN tblclient ON tblpolicy.clientID = tblclient.clientID) INNER JOIN tblpartner ON tblclient.partnerID = tblpartner.partnerID
                                    WHERE (((tbl_source.comm)>=-5 And (tbl_source.comm)<=0) AND ((tbl_source.policynum_matched)=-1))
                                    ORDER BY tbl_source.id");

                $db->query("DELETE tbl_source.*
                                    FROM tbl_source
                                    WHERE (tbl_source.comm >= -5 AND tbl_source.comm <= 0) AND tbl_source.policynum_matched = -1");

                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "process_matched":
            $json = (object)[
                "id" => null,
                "status" => false,
                "error" => null
            ];

            try {
                // Append any matched entries to tblcommission and remove from tbl_source
                $trans = (Source::source_to_commission());

                $json->status = $trans->status;
                $json->error = $trans->error;
            } catch (Exception $e) {
                $json->status = false;
                $json->error = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "process_aco":
            $json = [];
            try {
                $tbl_source_ids = [];
                $hold_policy_ids = [];

                $db = new mydb();

                $db->query("SELECT
                NOW(), tbl_source.batch_id, tbl_source.comm, tbl_source.Added_by, '8', tbl_source.policyID,
                tbl_source.Added_by, '1', tbl_source.currency_type, tbl_source.ID, tblpolicy.clientID,
                (SELECT id FROM income_on_hold WHERE object_type = 'Client' AND object_index = tblpolicy.clientID 
                                                 AND release_when IS NULL) AS `hold`
                FROM tbl_source
                INNER JOIN tblpolicy ON tbl_source.policyID = tblpolicy.policyID
                WHERE (
                    (tbl_source.comm >= 0) AND
                    (tbl_source.policynum_matched = -1) AND
                    (tbl_source.commtype_ps = 8) AND
                    (tblpolicy.vatable = 0)
                )
                ORDER BY tbl_source.id");

                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $tbl_source_ids[] = $row['ID'];
                    if (! is_null($row['hold'])) {
                        $hold_policy_ids[] = $row['policyID'];
                    }
                }

                $id_string = implode(', ', $tbl_source_ids);

                if(empty($id_string)){
                    throw new Exception("No commission entries to update");
                }

                $db->query("INSERT INTO tblcommission (editedWhen, batchID, amount, description, commissiontype, policyID, editedBy, vat, currency_type)
                SELECT NOW(), tbl_source.batch_id, tbl_source.comm, tbl_source.Added_by, '8', tbl_source.policyID, tbl_source.Added_by, '1', tbl_source.currency_type
                FROM tbl_source
                WHERE ID in (".$id_string.")");

                $db->query("DELETE FROM tbl_source WHERE ID in (".$id_string.")");

                // check if any of the policies have pending AC ongoing/Client Fees
                foreach ($hold_policy_ids as $policy_id) {
                    $search_p = new Search(new PipelineEntry());
                    $search_p->eq('policy', $policy_id);
                    $search_p->add_or([
                        $search_p->eq("type", 8, true, 0),
                        $search_p->eq("type", 10, true, 0)
                    ]);

                    // if so, place income on hld
                    while ($y = $search_p->next(MYSQLI_OBJECT)) {
                        $y->on_hold(1);
                        $y->save();
                    }
                }

                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;
    }
} else {
    echo Template("commission/process.html");
}
