<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    if (isset($_GET["do"]) && $_GET["do"] == "update") {
        $json = [];

        $id = $_GET["id"];
        $orgpol = $_GET["orgpol"];
        $issuer = $_GET["iss"];
        $newpolid = $_POST["new_code_id"];
        $newcode = $_POST["new_code"];

        $p = new Policy;
        $s = new Search($p);
        $s->eq("issuerID", $issuer);
        $s->eq("policyID", $newpolid);

        if ($x = $s->next(MYSQLI_ASSOC)) {
            $polarr = (array)$x;

            $polid = $polarr['id'];
            $polnum = $polarr['number'];

            try {
                $db = new mydb();

                $db->query("UPDATE feebase.tbl_source " .
                    "SET policyNum = '" . addslashes($polnum) . "', policynum_matched = '-1', policyid = " . $polid . " " .
                    "WHERE feebase.tbl_source.policynum = '" . addslashes($orgpol) . "' AND feebase.tbl_source.issuer_ID = " . $issuer);

                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e->getMessage();
            }
        } // If policy doesnt exist - leave policyid as 0 in tbl_source
        else {
            try {
                $db = new mydb();

                $db->query("UPDATE feebase.tbl_source " .
                    "SET policyNum = '" . addslashes($newcode) . "', policynum_matched = 0, policyid = 0 " .
                    "WHERE feebase.tbl_source.policynum = '" . addslashes($orgpol) . "' AND feebase.tbl_source.issuer_ID = " . $issuer);

                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e->getMessage();
            }
        }

        echo json_encode($json);
    } else {
        echo Template("pipelineentry/update_agency.html", ["id" => $_GET["id"]]);
    }
} else {
    die("No policy number supplied to update");
}
