<?php

require $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";

if (!empty($_GET["issuerid"]) && !empty($_GET["bankid"]) && isset($_GET["file"])) {
    $json = (object)[
        "id"      => null,
        "status"  => false,
        "error"   => null,
        "unmatched" => null
    ];

    // Gather requried data to import file to table source and create batch
    $issuerid = $_GET['issuerid'];
    $issuer = $_GET['issuer'];
    $bankid = $_GET['bankid'];
    $commtotal = $_GET['commtotal'];

    // Assign matched columns to variables for import
    $colmatch = $_GET['colmatch'];
    $policyno = $colmatch[0];
    $surname = $colmatch[1];
    $agencyno = $colmatch[2];
    $commtype = $colmatch[3];
    $amount = $colmatch[4];

    $currency_type = $_GET['currency_type'];

    // Implode commission types to string to check all types are present in database for issuer
    $commtypes = $_GET['commtypes'];
    $ctypes = implode("','", $commtypes);

    // Check to see if all commission types from file appear in commission_conversion_table
    // If not die() as we do not want entries being added without a commission type
    try {
        // Connection to DB
        $db = new mydb();

        $db -> query("SELECT * FROM feebase.commission_conversion_table ".
        "WHERE issuer_id = " . $issuerid . " ".
        "AND comm_desc IN ('" . $ctypes . "')");

        $count = $db -> affected_rows;

        if (sizeof($commtypes) > $count) {
            $json -> status = false;
            $json -> error = "not all Income types in file are present in database, contact IT";
            echo json_encode($json);
            die();
        }
    } catch (Exception $e) {
        $json -> status = false;
        $json -> error = $e -> getMessage();
    }

    try {
        // Connection to DB
        $db = new mydb();

        // Create batch to assign file to
        $b = new Batch;
        $b->issuer($issuerid);
        $b->bank($bankid);
        $b->target($commtotal);
        $b->date_period_from($_GET['date_period_from']);
        $b->date_period_to($_GET['date_period_to']);
        $b->pre_30_april($_GET['pre_30_april']);
        $b->post_30_april($_GET['post_30_april']);


        // Obtain batch id of newly created batch - used in 'data infile' below
        $batchid = '';
        if ($b -> save()) {
            $batchid = $b -> id();
        } else {
            exit("No batch ID found");
        }

        // Modify file path based on OS
        $filename = (PHP_OS == 'WINNT') ? str_replace("\\", "\\\\", $_GET['file']) : str_replace("\\", "/", $_GET['file']);

        // Import spreadsheet to table source
        $db -> query("LOAD DATA LOCAL INFILE '". $filename ."' " .
        "INTO TABLE feebase.tbl_source ".
        "FIELDS TERMINATED BY ',' ".
        "ENCLOSED BY '\"' ".
        "LINES TERMINATED BY '\r\n' ".
        "IGNORE 1 LINES ".
        "(@c1,@c2,@c3,@c4,@c5,@c6,@c7,@c8,@c9,@c10,@c11,@c12,@c13,@c14,@c15,@c16,@c17,@c18,@c19,@c20,@c21,@c22,@c23,@c24,@c25,@c26,@c27,@c28,@c29,@c30,@c31,@c32,@c33,@c34,@c35,@c36,@c37,@c38,@c39,@c40,@c41,@c42,@c43,@c44,@c45,@c46,@c47,@c48,@c49,@c50) ".
        "SET ".
        "ref = concat('general_electronic_', now()), ".
        "issuer = '". $issuer ."', ".
        "comm = CAST(@c" . $amount . " AS DECIMAL(10,2)), " .
        "policyNum = TRIM(@c" . $policyno . "), " .
        "clientSname1 = TRIM(@c" . $surname . "), " .
        "comm_total = '". $commtotal ."', ".
        "commtype = TRIM(@c" . $commtype . "), " .
        "agency_no = TRIM(@c" . $agencyno . "), " .
        "batch_id = ". $batchid .", ".
        "bank_id = ". $bankid .", ".
        "commtype_ps = (SELECT comm_ps FROM feebase.commission_conversion_table WHERE Issuer_ID = " . $issuerid . " and Comm_desc = TRIM(@c" . $commtype . ")),".  // use commission_conversion_table here - eg adviser charging new types need to be added
        "issuer_ID = ". $issuerid .", ".
        "Added_by = 'Electronic Other', ".
        "partnerID = 0, ".
        "clientID = 0, ".
        "policynum_matched = 0, ".
        "policyID = 0, ".
        "filename = '" . basename($filename) ."', ".
        "currency_type = '" . $currency_type ."', ".
        "added_when = " . time());

        $total_entries = $db->affected_rows;

        // Remove any entries that have been added with commission amount £0.00 - save adding unwanted clients/policies
        $db -> query("DELETE FROM tbl_source WHERE comm = 0 AND batch_id = " . $batchid . " AND bank_id = " . $bankid . " AND issuer_ID = " . $issuerid);

        // Append any matched entries to tblcommission and remove from tbl_source
        $trans = ( Source::source_to_commission() );

        // Obtain any unmatched entries and return to user
        $json -> unmatched = ( Source::get_unmatched($batchid) );

        $json -> status = $trans -> status;
        $json -> error = $trans -> error;

        if ($trans == true) {
            // get objects
            $bank = new BankAccount($bankid, true);
            $batch = new Batch($batchid, true);

            $batch->date_period_from($_GET['date_period_from']);
            $batch->date_period_to($_GET['date_period_to']);

            $batch->pre_30_april($_GET['pre_30_april']);
            $batch->post_30_april($_GET['post_30_april']);
            $batch->save();

            // get commission total
            $db -> query("SELECT comm_total FROM tbl_source WHERE batch_id = " . $batchid . " AND bank_id = " . $bankid . " AND issuer_ID = " . $issuerid." ORDER BY id DESC LIMIT 1");
            while ($c = $db -> next(MYSQLI_ASSOC)) {
                $total_value = $c['comm_total'];
            }

            // format date for inserting to database
            //$date_formatter = DateTime::createFromFormat('d/m/y', $paid);
            //$statement_date = $date_formatter->format('Y-m-d');

            // insert new record
            $fe = new FinanceElectronicRecord();
            $fe->issuer($issuerid);
            //$fe->statement_date($statement_date);
            $fe->entries($total_entries);
            $fe->total_value($total_value);
            $fe->bank_entry_date($bank->date());
            $fe->process_date(date("Y-m-d"));
            $fe->batch($batchid);
            $fe->entry_type("Other");
            $fe->balanced($batch->balanced());
            $fe->save();
        }
    } catch (Exception $e) {
        $json -> status = false;
        $json -> error = $e -> getMessage();
    }

    echo json_encode($json);
} else {
    $json = (object) [
        "id"      => null,
        "status"  => false,
        "error"   => null
    ];

    $json -> status = false;
    $json -> error = "please assign an Issuer and Bank Entry before proceeding";
    echo json_encode($json);
    die();
}
