<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

use Aws\S3\S3Client;
use Carbon\Carbon;


// Define our AWS credentials
$bucket = AWS_BUCKET;
$s3StorageKey = AWS_STORAGE_KEY;
$s3Secret = AWS_SECRET;

// Instantiate the S3 client with AWS credentials
$s3Client = new S3Client([
    'credentials' => [
        'key' => $s3StorageKey,
        'secret' => $s3Secret
    ],
    'region' => 'eu-west-2',
    'version' => '2006-03-01'
]);

if (isset($_GET['id'])) {
    $mandate = new Mandate($_GET['id'], true);

    switch ($_GET['do']) {
        case "process":
            $custom_message = [
                '3283' => "Please ensure you check the BSJ main account for this client",
                '3360' => "Please ensure you check the BSJ main account for this client",
                '3361' => "Please ensure you check the BSJ main account for this client",
                '2206' => "Please check that client doesnt appear in account for Colin Beevers (3290)"
            ];

            // search for 'new' servicing level
            $s = new Search(new ServicingPropositionLevel());
            $s->eq('client_id', $mandate->client->id());
            $s->partner_id('partner_id', $mandate->client->partner->id());

            if ($sl = $s->next(MYSQLI_ASSOC)) {
                $serv = $sl;
            } else {
                $serv = new ServicingPropositionLevel();
            }

            // Open overlay for processing this mandate
            echo Mandate::get_template("process.html", [
                "mandate" => $mandate,
                "custom_message" => json_encode($custom_message),
                "sl" => $serv
            ]);

            break;

        case "view":
            if (isset($_GET["filename"])) {
                // old scan reference, must be in archive
                $filename = $_GET["filename"];
                $filepath = MANDATE_PROCESSED_SCAN_DIR . "/$filename";
            } else {
                // modern post-2.1 scan
                $filename = $mandate->file();
                $filepath = ((!$mandate->sent_when()) ? MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] : MANDATE_PROCESSED_SCAN_DIR) . "/$filename";
            }

            if (isset($_GET['view'])) {
                if (in_array($_GET["view"], ["file", "download"])) {
                    // Determine Content-Type based on file extension
                    stristr($filepath, '.tif') ? header("Content-type: image/tiff") : header("Content-type: application/pdf");

                    //header( "Content-type: image/tiff" );

                    // Streams as attachment if download requested
                    if ($_GET["view"] == "download") {
                        header("Content-Disposition: attachment; filename=$filename");
                    }

                    if (file_exists($filepath)){
                        if ($file = file_get_contents($filepath)) {
                            echo $file;
                        } else {
                            echo $filepath;
                        }
                    } else {
                        echo "File does not exist";
                    }

                }
            } else {
                echo Mandate::get_template("view.html", ["id" => $mandate->id, "filename" => $filename]);
            }

            break;

        case "update":
            $response = [
                'success' => true,
                'link' => null,
            ];

            $mandate->load([
                $_GET['object'] => $_GET['object_id']
            ]);

            $object = $_GET['object'];

            //check if were updating the client, that they belong to the partner
            if ($object == "client") {
                $client = new Client($_GET['object_id'], true);
            }

            if ($mandate->save()) {
                if ($_GET['object_id']) {
                    $response['link'] = $mandate->$object->link();
                }
            } else {
                $response['success'] = false;
            }

            echo json_encode($response);

            break;

        case "send":
            try {
                $response = [
                    'success' => true,
                    'error' => null,
                    'policyRow' => null,
                    'adviserCharging' => false,
                ];

                $trusteeMandate = false;
                $pols = [];

                // we need to compare the post variables to a blacklist
                // syntax is partnerid => array of issuers blacklisted
                $blacklist = [
                    "3073" => ["11612", "233", "11116"],
                    "3054" => ["11612", "233", "11116"],
                    "3433" => "ALL",
                    "3803" => "ALL",
                    "3832" => "ALL",
                    "3833" => "ALL",
                    "3941" => "ALL"
                ];

                //there are certain issuers who will not transfer anything to us at all.
                $iss_not_transfer = ["89"];

                // list of issuers who should have workflow moved straight to AC to be Keyed, rather than AC with mandate
                $acKeyIssuers = [
                    11817, // Zurich Platform
                    11602, // Elevate
                ];

                //these issuers require adviser charging forms
                $rem_db = new mydb(REMOTE, E_USER_WARNING);
                $rem_db->query("SELECT issuerID FROM myps.ac_forms WHERE archived_when is null and prophet_ac_issuer = 1 GROUP BY issuerID");

                $ac_issuers = [];
                while ($iss = $rem_db->next()) {
                    $ac_issuers[] = $iss['issuerID'];
                }

                // check for policy types not being exempt, must have a policy not in the list for AR to work
                $ARactive = false;

                //list of partners who we dont want to send action required tickets to on the back of mandates
                $no_ac_partners = [
                    5211 // NOW financial
                ];

                $client = new Client($_POST['mandate']['client'], true);
                $partner = new Partner($_POST['mandate']['partner'], true);

                //check client belongs to this partner
                if ($client->partner() != $partner->id()) {
                    throw new Exception("Client (" . $client->id() . ") does not belong to Partner (" . $partner->id() . ")");
                }

                function partner_blacklist($partnerID, $issuerID, $blacklist){
                    if (array_key_exists($partnerID, $blacklist)) {
                        if (!is_array($blacklist[$partnerID]) && $blacklist[$partnerID] == "ALL") {
                            throw new Exception("This partner is NOT allowed to submit mandates, please check partner notes");
                        } elseif (in_array($issuerID, $blacklist[$partnerID])) {
                            throw new Exception("This partner is NOT allowed to transfer this provider.");
                        }
                    }
                }

                if ($partner->id() == 5527 && !in_array(User::get_default_instance('id'), [60, 44, 201, 248])) {
                    throw new Exception("Please pass all mandates/client agreements under John Scarratt's account to Kayleigh for approval.");
                }

                function move_mandate($queue, $file, $issuer = false){
                    if (file_exists(MANDATE_PENDING_SCAN_DIR . "/" . $queue . "/" . $file)){
                        if (rename(MANDATE_PENDING_SCAN_DIR . "/" . $queue . "/" . $file, MANDATE_PROCESSED_SCAN_DIR . "/" . $file)){
                            if($issuer){
                                $date = Carbon::now()->format("Y-m-d");
                                // copy the mandate to an issuer specific directory on terry
                                $copiesDir = MANDATE_COPY_ISSUERS_DIR . "/" . $date . "/" . $issuer . "/";
                                if(!file_exists($copiesDir)){
                                    mkdir($copiesDir, 0777, true);
                                }

                                if(!copy(MANDATE_PROCESSED_SCAN_DIR . "/" . $file, $copiesDir . $file)){
                                    throw new Exception("Failed to copy mandate " . $file . " to issuer specific copies directory");
                                }
                            }
                        } else {
                            throw new Exception("Failed to move mandate " . $file ." to processed directory");
                        }
                    } else {
                        throw new Exception("Mandate " . $file . " does not exist");
                    }
                }

                switch($_POST['mandate_type']){
                    case "client_agreement":
                        $redisClient = RedisConnection::connect();

                        // check for income run and block processing if client has pending income
                        if ($redisClient->get("incomerun")){
                            $s = new Search(new PipelineEntry());
                            $s->eq("policy->client", $_POST['mandate']['client']);

                            if($s->next(MYSQLI_ASSOC)){
                                throw new Exception("This client agreement cannot be processed while the income run is going as the client has pipeline income. Please try again later.");
                            }
                        }

                        partner_blacklist($_POST['mandate']['partner'], 11764, $blacklist);

                        if (isset($_POST['servicingpropositionlevel']['level']) && strlen($_POST['servicingpropositionlevel']['level']) != 0) {
                            $percent = [
                                1 => 1,
                                2 => '0.75',
                                3 => '0.5',
                                'T' => 0,
                                'P' => 0,
                                'B' => $_POST['servicingpropositionlevel']['percent']
                            ];

                            //check if we already have a new servicing level, if so update, else add new entry, Audit in log_actions
                            $s = new Search(new ServicingPropositionLevel());
                            $s->eq("client_id", $_POST['mandate']['client']);
                            $s->eq("partner_id", $_POST['mandate']['partner']);

                            $changeSL = false;

                            if ($sl = $s->next(MYSQLI_ASSOC)) {
                                if ($_POST['servicingpropositionlevel']['level'] != $sl->level()
                                    || $percent[$_POST['servicingpropositionlevel']['level']] != $sl->percent()
                                    || isset($_POST['update_review_date'])) {
                                    $changeSL = true;
                                    //only update the servicing prop table if if the level has changed
                                    $sl->updated_by(User::get_default_instance('id'));
                                    $sl->updated_when(date('Y-m-d'));

                                    if (in_array($sl->level(), [1, 2, 3, "B"]) && in_array($_POST['servicingpropositionlevel']['level'], ["T", "P"])){
                                        // if old level was 1,2,3 or Bespoke and we are moving to Transactional or Pure Protection, cancel any portfolio report workflows
                                        $db = new mydb(REMOTE);

                                        $db->query("select w.id from myps.valuation_requests vr
                                                    inner join feebase.workflow w
                                                    on vr.workflow_id = w.id
                                                    where servicing_report = 1
                                                    and clientID = " . $_POST['mandate']['client']);

                                        while($w = $db->next(MYSQLI_ASSOC)){
                                            $workflow = new Workflow($w['id'], true);

                                            // end workflow
                                            try{
                                                $workflow->end_ticket("Client Servicing Level Changed to " . $_POST['servicingpropositionlevel']['level']);
                                            } catch (Exception $e) {
                                                $response['success'] = false;
                                                $response['error'] = "Failed to end valuation workflow tickets: " . $e->getMessage();
                                            }
                                        }
                                    }
                                }
                            } else {
                                $changeSL = true;

                                $sl = new ServicingPropositionLevel();
                                $sl->partner_id($_POST['mandate']['partner']);
                                $sl->client_id($_POST['mandate']['client']);
                            }

                            if ($changeSL) {
                                $sl->level($_POST['servicingpropositionlevel']['level']);

                                if ($_POST['servicingpropositionlevel']['level'] == 'B'){
                                    if (!empty($_POST['servicingpropositionlevel']['percent'])){
                                        $sl->percent($_POST['servicingpropositionlevel']['percent']);
                                        $sl->amount("");
                                    } else {
                                        $sl->amount($_POST['servicingpropositionlevel']['amount']);
                                        $sl->percent("");
                                    }

                                    $sl->frequency($_POST['servicingpropositionlevel']['frequency']);
                                } else {
                                    $sl->percent($percent[$_POST['servicingpropositionlevel']['level']]);
                                }

                                $sl->method('mandate_system');

                                if (!$sl->save()) {
                                    throw new Exception("Servicing level failed to update, please try again or contact IT.");
                                } else {
                                    if ($ih = $client->income_on_hold($client)){
                                        // we do not want to release adhoc holds here
                                        if ($ih->reason() != 3) {
                                            // we now have a client agreement, lets release the hold
                                            if ($ih->reason() == 2) {
                                                // hold was put on due to Transactional serv level, we can now release as if not T or P
                                                if ($_POST['servicingpropositionlevel']['level'] != "T") {
                                                    // release income
                                                    IncomeHold::releaseIncome($client->id());
                                                }
                                            } elseif ($ih->reason() == 1) {
                                                // release income
                                                IncomeHold::releaseIncome($client->id());
                                            }
                                        }
                                    }

                                    // close overdue review or CA needed AR tickets and add final message
                                    $sar = new Search(new ActionRequired);
                                    $sar->eq("object", "Client");
                                    $sar->eq("object_id", $client->id());

                                    $subjects = [
                                        26, // Overdue Review
                                        29, // Client Agreement Needed
                                    ];

                                    $sar->eq("subject_id", $subjects);
                                    $sar->eq("completed_when", null);
                                    $sar->eq("archived", 0);

                                    $greeting = (date('G') <= 11) ? "Good Morning" : "Good Afternoon";
                                    $usr = User::get_default_instance();
                                    while ($t = $sar->next(MYSQLI_ASSOC)) {
                                        // add a message to the ticket before closing
                                        $tm = new ActionRequiredMessage();

                                        $tm->action_required_id($t->id());
                                        $tm->message($greeting.",
 
I hope you are well!

We appreciate you taking the time to arrange for this client to complete our new Client Agreement, I can confirm I have processed this for you today and the client has now been added to their selected Service Level within the Servicing section of your MyPSAccount where you can now schedule the client's next review meeting and order their Portfolio Valuation report.  
 
As a result of your client signing our new Proposition, if we haven't already, we will shortly be sending your client a pack which will include a short letter signed by one of our Directors and the PSL Terms and Conditions brochure. 
 
Attached to this action is our Servicing guide which will help you understand how to demonstrate service for your clients who hold legacy products with PSL. 
 
If you have any questions, please do not hesitate to contact me. 
 
Kind regards,
".$usr->staff_name());
                                        $tm->save();

                                        // close the ticket
                                        $t->completed_by(6615);
                                        $t->completed_when(time());

                                        if(!$t->save()){
                                            throw new Exception("Failed to close action required ticket");
                                        }
                                    }

                                    if ($_POST['servicingpropositionlevel']['level'] == 'P') {
                                        $client->pure_protection(1);
                                    }
                                    $client->save();
                                }
                            }

                            // add the client agreement policy
                            $p = new Policy();

                            $p->status(17); // Client Agreement Status is No Business
                            $p->addedhow(2); // Mandate System
                            $p->next_chase(''); //null the next chase date, client agreement, we dont need it
                            $p->mandate($mandate->id()); // set the mandate id to this mandate
                            $p->client($_POST['mandate']['client']); // use the client we set in the form
                            $p->issuer(11764); // ***Client Agreement*** issuer
                            $p->number(date("dmY")); // Policy Number is auto-generated
                            $p->type(32); // Client Agreement
                            $p->owner($_POST['policy'][0]['owner']);

                            if ($p->save()){
                                $pols[] = $p->id();

                                $mandate->partner($p->client->partner->id());
                                $mandate->client($p->client());
                                $mandate->issuer($p->issuer());
                                $mandate->sent_by(User::get_default_instance("id"));
                                $mandate->sent_when(time());
                                $mandate->policy_count(1);
                                $mandate->jasper_include(0);

                                if ($mandate->save()){
                                    move_mandate($_GET['queue'], $mandate->file());
                                }
                            } else {
                                throw new Exception("Failed to save client agreement policy");
                            }
                        } else {
                            throw new Exception("No servicing level provided for client agreement");
                        }
                        break;

                    case "mandate":
                        partner_blacklist($_POST['mandate']['partner'], $_POST['mandate']['issuer'], $blacklist);

                        if (in_array($_POST['mandate']['issuer'], $iss_not_transfer)) {
                            throw new Exception("This issuer will NOT transfer any servicing to us.");
                        }

                        // check for trustee mandate
                        if (isset($_POST['trustee_switch'])){
                            // trustee mandates should be assigned to the ***Unassigned*** issuer
                            $mandate->issuer(1045);

                            if (isset($_POST['trustee-select'])){
                                foreach($_POST['trustee_policies'] as $k => $pol){
                                    $id = $pol['id'];

                                    if ($id) {
                                        // check we have a policy id and create a new trustee mandate
                                        $mt = new MandateTrustee();
                                        $mt->trustee($_POST['trustee-select']);
                                        $mt->client($_POST['mandate']['client']); // TODO we should really create a trustee object here and check the trustee matches the client
                                        $mt->policy($id);
                                        $mt->mandate($mandate->id());

                                        // update status of policies with a trustee to awaiting confirmation
                                        $p = new Policy($id, true);
                                        $p->status(1);

                                        # update the chase date for 3 weeks time
                                        $threeWeeks = Carbon::now()->addWeeks(3)->format("Y-m-d");

                                        $p->next_chase($threeWeeks);

                                        $pols[] = $p->id();

                                        if (!$p->save()){
                                            throw new Exception("Failed to update policy status on trustee mandate");
                                        }

                                        if (!$mt->save()){
                                            throw new Exception("Failed to map trustee mandate");
                                        }

                                        $trusteeMandate = true;
                                    }
                                }
                            } else {
                                throw new Exception("Trustee mandate but no trustee selected");
                            }
                        } else {
                            $policy_n = intval($mandate->policy_count());

                            foreach($_REQUEST['policy'] as $i => $data){
                                $newPolicy = false;

                                // make sure we have a policy number and type
                                if (!empty($data['number']) && !empty($data['type'])){
                                    // check if we are updating an existing policy
                                    if (!empty($_POST['update_policy'][$i])){
                                        // get the existing policy object
                                        $p = new Policy($_POST['update_policy'][$i], true);

                                        if ($p->client() != $_POST['mandate']['client']){
                                            // check if policy owner is this client
                                            throw new Exception("This client does not own policy " . $p->number());
                                        }

                                        // if policy already has a mandate_id attached to it
                                        if ($p->mandate() != null){
                                            // create a mandate archive entry
                                            $am = new MandateArchive();
                                            $am->mandate_id($p->mandate());
                                            $am->policy($p->id());
                                            $am->client($_REQUEST['mandate']['client']);
                                            $am->issuer($_REQUEST['mandate']['issuer']);

                                            if (!$am->save()){
                                                throw new Exception("Failed to archive old mandate, contact IT");
                                            }
                                        }
                                    } else {
                                        $p = new Policy();
                                        $p->addedhow(2); // Mandate System
                                        $newPolicy = true;
                                    }

                                    $p->status(1); // Awaiting Confirmation
                                    $p->next_chase(''); // Null the next chase date
                                    $p->mandate($mandate->id()); // this mandates ID
                                    $p->client($_POST['mandate']['client']); // the mandates client ID
                                    $p->issuer($_POST['mandate']['issuer']); // the mandates issuer ID

                                    $p->load($data); // load the number, type and owner here

                                    if ($p->save()){
                                        if ($newPolicy){
                                            $p->on_create();
                                        }

                                        $pols[] = $p->id();
                                    } else {
                                        $response['policyRow'] = $i;
                                        throw new Exception("Failed to save policy " . $data['number']);
                                    }

                                    $mam = new MandateAddressMapping();

                                    $mam->issuer($_POST['mandate']['issuer']);
                                    $mam->issuer_contact($_POST['issuer_contact_select'][$i]);
                                    $mam->partner($_POST['mandate']['partner']);
                                    $mam->policy($p->id());
                                    $mam->owner($p->owner());
                                    $mam->mandate($mandate->id());
                                    $mam->client($_POST['mandate']['client']);

                                    if (!$mam->save()) {
                                        $response['policyRow'] = $i;
                                        throw new Exception("Failed to add policy mandate address mapping for policy " . $data['number'] . ", contact IT");
                                    }

                                    $policy_n++;
                                } elseif (!empty($data['number']) && empty($data['type'])){
                                    $response['policyRow'] = $i;
                                    throw new Exception("Policy type is empty");
                                }
                            }

                            $mandate->policy_count($policy_n); //update policy count here as trustee mandates do not have a count
                        }

                        $mandate->sent_by(User::get_default_instance("id"));
                        $mandate->sent_when(time());

                        $mandate->load($_POST['mandate']); //load all other details from the form

                        if ($mandate->policy_count() < 1 && !$trusteeMandate){
                            throw new Exception("You must provide at least 1 policy");
                        }

                        if (!$mandate->save()){
                            throw new Exception("Failed to save final mandate details, contact IT");
                        }

                        move_mandate($_GET['queue'], $mandate->file(), strval($mandate->issuer));

                        // check if issuer was Brewin Dolphin and email new business
                        $watchIssuers = [
                            1155, 1167, 11132, 11596, //brewin dolphin
                            1084, // Investec Wealth & Investment
                        ]; 

                        if (in_array($mandate->issuer(), $watchIssuers)){
                            //build up a list of recipients, basically just the NB advice team
                            $recipients = [];

                            $s = new Search(new User());
                            $s->eq("active", 1); // only get active users
                            $s->eq("department", 16); // New Business Advice

                            while($u = $s->next(MYSQLI_ASSOC)){
                                $recipients[] = [
                                    "address" => [
                                        "email" => $u->email()
                                    ],
                                ];
                            }

                            // use a different email when testing with watched issuer mandates
//                            $recipients[0]['address']['email'] = "sean.ross@policyservices.co.uk";

                            $from = [
                                'name' => 'Policy Services - IT',
                                'email' => 'it@policyservices.co.uk',
                            ];

                            $policies = [];

                            // get a list of policies which use this mandate
                            foreach ($pols as $pid){
                                $p = new Policy($pid, true);
                                $policies[] = $p;
                            }

                            $local_template['html'] = new Template("sparkpost/emailTemplates/issuerMandate/watch.html", [
                                "mandate" => $mandate,
                                "policies" => $policies
                            ]);

                            if (sendSparkEmail($recipients, strval($mandate->issuer) . " mandate processed", '', false, $from, [], $local_template)) {
                                $response['message'] = "Mandates from " . strval($mandate->issuer) . " are being watched and the appropriate emails have been sent";
                            } else {
                                throw new Exception("Failed to send Issuer Watch Alert email through sparkpost");
                            }
                        }

                        $ARexemptTypes = [
                            7, // Term Protection
                        ];

                        // get a list of policies which use this mandate
                        foreach ($pols as $pid){
                            $p = new Policy($pid, true);

                            if (!in_array($p->type(), $ARexemptTypes)){
                                $ARactive = true;
                            }
                        }

                        // partner specific issuer list
                        // These partners should only show the action required prompt for certain issuers
                        $ac_partners = [
                            "5180" => [11789] // Keith Barr (Aviva Wrap)
                        ];

                        // These partners dont want to receive AC for certain issuers
                        $ac_partner_exclude_issuers = [
                            "5527" => [11292] // John Scarlett (Hornbuckle)
                        ];

                        if (array_key_exists(strval($mandate->partner()), $ac_partners)){
                            // this partner only receives AR tickets for AC forms for certain providers
                            if (!in_array($mandate->issuer(), $ac_partners[strval($mandate->partner())])){
                                // the issuer is not in the array, dont send AC action required
                                $ARactive = false;
                            }
                        }

                        if (array_key_exists(strval($mandate->partner()), $ac_partner_exclude_issuers)){
                            // this partner do not receive AR tickets for AC forms for certain providers
                            if (in_array($mandate->issuer(), $ac_partner_exclude_issuers[strval($mandate->partner())])){
                                // the issuer is not in the array, dont send AC action required
                                $ARactive = false;
                            }
                        }

                        //everything is done, lets check if the issuer requires an adviser charging form
                        if ($ARactive
                            && !in_array($client->servicingPropositionLevel(), ["T", "P"])
                            && !in_array($mandate->partner(), $no_ac_partners)
                            && in_array(intval($mandate->issuer()), $ac_issuers)) {
                            //this issuer requires an adviser charging form
                            $response['adviserCharging'] = [
                                'partner' => $mandate->client->partner(),
                                'issuer' => $mandate->issuer(),
                                'client' => $mandate->client(),
                                'mandate' => $mandate->id(),
                                'policies' => $pols,
                            ];
                        }

                        break;

                    case "adviser_charging":
                        // add correspondence
                        // just use the first policy for adding correspondence to
                        $pol = new Policy($_POST['ac_policies'][0]['id'], true);

                        if (!$pol->id()){
                            throw new Exception("Failed to find policy");
                        }

                        if ($pol->client->id() !== $_POST['mandate']['client']){
                            throw new Exception("Policy holder does not match the client provided");
                        }

                        if ($pol->client->partner->id() !== $_POST['mandate']['partner']){
                            throw new Exception("Policy holder's adviser does not match the partner provided");
                        }

                        $date = date("Y-m-d H:i:s");

                        $corr = new Correspondence();
                        $corr->policy($pol->id());
                        $corr->client($pol->client->id());
                        $corr->partner($pol->client->partner->id());
                        $corr->date($date);
                        $corr->message("We are pleased to enclose correspondence that we have received for the above client for your attention.");
                        $corr->sender(User::get_default_instance("id"));
                        $corr->subject("Adviser Charging Form sent to Provider");
                        $corr->letter_type(6);

                        if ($corr->save()){
                            // get a coversheet instance
                            $letter = new Letter(6, true);

                            $c = new \Jaspersoft\Client\Client(
                                JASPER_HOST,
                                JASPER_USER,
                                JASPER_PASSWD,
                                ""
                            );

                            $form_values = [];
                            $form_values["ITEM_ID"] = $corr->id();
                            $form_values["correspondence[letter]"][] = 6;

                            // get constant values for standard letter footer
                            $std_letter_foot = unserialize(STANDARD_LETTER_FOOTER);
                            foreach ($std_letter_foot as $key => $value) {
                                $form_values[$key][] = $value;
                            }

                            // point to prophet_reports dir on rosie
                            $report_url = JASPER_STANDARD_LETTERS . $letter->rosie_file();

                            $report_url = Letter::url_path_corrections($report_url);

                            $report = $c->reportService()
                                ->runReport($report_url, 'pdf', null, null, $form_values);

                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Description: File Transfer');
                            header('Content-Disposition: attachment; filename=' . $letter->rosie_file() . '.pdf');
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Content-Length: ' . strlen($report));
                            header('Content-Type: application/pdf');

                            // temporary create coversheet for merging with correspondence
                            $temp_coversheet = CORRESPONDENCE_PENDING_SCAN_DIR . $corr->id() . "_temp_coversheet.pdf";
                            file_put_contents($temp_coversheet, $report);

                            if (file_exists($temp_coversheet)){
                                // now that we have the cover sheet we want to use ghost script to join this with our
                                // current item of correspondence
                                $correspondence = str_replace('//', '/', MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/" . $mandate->file());

                                // merge files contained in this array
                                $fileArray = [$temp_coversheet, $correspondence];

                                // specify directory to place new file in
                                $corr_file = str_replace('//', '/', CORRESPONDENCE_PROCESSED_SCAN_DIR . "/" . $corr->id() . ".pdf");

                                // let ghost script works its magic
                                $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH  -sOutputFile=$corr_file ";

                                //Add each pdf file to the end of the command
                                $cmd .= implode(" ", $fileArray);

                                //merge and create final pdf
                                $result = exec($cmd);

                                // check final pdf has ben created
                                if (file_exists($corr_file)) {
                                    // delete temp coversheet now that merged pdf has been created
                                    if (file_exists($temp_coversheet)) {
                                        unlink($temp_coversheet);
                                    }

                                    // dont delete the 'mandate' file yet, we'll do this later once everything else has gone smoothly
                                } else {
                                    throw new Exception("Failed to move the new item of correspondence to the right place");
                                }
                            } else {
                                throw new Exception("Failed to find coversheet after it was created");
                            }
                        } else {
                            throw new Exception("Failed to add correspondence");
                        }

                        foreach($_POST['ac_policies'] as $acPol){
                            if ($acPol['id'] != 0){
                                $pol = new Policy($acPol['id'], true);
                                $pol->rate($_POST['advisercharging']['ac_requested']);
                                $pol->save();
                                if (!$pol->save()){
                                    throw new Exception("Failed to update AC rate in tblpolicy");
                                }

                                // add workflow
                                $wf = new Workflow();

                                $dateDue = Carbon::now()->addWeeks(2)->startOfDay()->format("U");

                                $wf->due($dateDue);

                                if (in_array($pol->issuer(), $acKeyIssuers)){
                                    $wf->step(159);
                                } else {
                                    $wf->step(112);
                                }

                                $wf->priority(2);
                                $wf->_object("Policy");
                                $wf->index($pol->id());

                                if ($wf->save()){
                                    $wfa = new WorkflowAssigned();
                                    $wfa->workflow_id($wf->id());
                                    $wfa->is_global(1);

                                    if (!$wfa->save()){
                                        throw new Exception("Failed to assign workflow ticket");
                                    }
                                } else {
                                    throw new Exception("Failed to add workflow ticket");
                                }

                                // add adviser charging
                                $ac = new AdviserCharging();
                                $ac->policy($pol->id());
                                $ac->load($_POST['advisercharging']);

                                if (!$ac->save()){
                                    throw new Exception("Failed to add adviser charging entry");
                                }
                            }

                        }

                        /*
                         * Everything is done!
                         * This isn't technically a mandate and shouldn't really be here,
                         * let's delete it to cover our tracks.
                         */
                        if (file_exists(MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/" . $mandate->file())){
                            unlink(MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/" . $mandate->file());
                        }

                        break;

                    default:
                        throw new Exception("No mandate type selected");

                        break;
                }

                /*
                 * save the policy ID's to a session variable and we can
                 * process the next item against the same policies
                 */
                // TODO stick this baby in redis and remember for a few minutes
                $_SESSION['last_policies'] = $pols;

            } catch (Exception $e) {
//                trigger_error($e->getMessage());
                $response['success'] = false;
                $response['error'] = $e->getMessage();
            }

            echo json_encode($response);

            break;

        case "delete":
            $response = [
                'success' => true,
                'error' => null,
            ];

            if (is_file(MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/" . $mandate->file())) {
                //check that the file still exists and delete it
                unlink(MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/" . $mandate->file());
            }

            if (!$mandate->delete()) {
                $response['success'] = false;
                $response['error'] = "Failed to delete entry from the mandate table, contact IT";
            }

            echo json_encode($response);

            break;

        case "ac_action_required":
            $json = [
                'success' => true,
                'error' => null,
                'ar_id' => false,
                'ar_message_id' => false,
            ];

            try {
                $ar = new ActionRequired();
                //adviser charging subject
                $ar->subject_id(2);
                $ar->object("Client");
                $ar->object_id($_GET['client']);
                //$ar->priority(2); //TODO delete priority from ar object
                $ar->partner($_GET['partner']);

                if (isset($_GET['date_due']) && $_GET['date_due'] != "--") {
                    $dateDue = date("U", strtotime($_GET['date_due']));

                    $ar->date_due($dateDue);
                } else {
                    throw new Exception("Invalid chase date given");
                }

                if ($ar->save()) {
                    $json['ar_id'] = $ar->id();
                    $arm = new ActionRequiredMessage();
                    $arm->action_required_id($ar->id());

                    $message = $_GET['message'];

                    $rem_db = new mydb(REMOTE, E_USER_WARNING);
                    $rem_db->query("SELECT * FROM myps.ac_forms WHERE issuerID = " . $_GET['issuer']);

                    while ($d = $rem_db->next()) {
                        $message .= "<br><a href='https://mypsaccount.com/mandate' target='_blank'>Download Adviser Charging Forms</a>";
                    }

                    $arm->message($message);

                    if ($arm->save()) {
                        $json['ar_message_id'] = $arm->id();

                        //need partner instance to get practice
                        $partner = new Partner($_GET['partner'], true);

                        //file has been sent so will be in the processed directory
                        $filename = MANDATE_PROCESSED_SCAN_DIR . "/" . $mandate->file();

                        //attach the mandate
                        $f = pathinfo($filename);
                        $fileExtension = $f['extension'];
                        $hash = md5(file_get_contents($filename));
                        $unique_name = $hash . "." . $fileExtension;

                        $directory = $partner->get_practice()->id() . "/" . $partner->id() . "/" . $unique_name;

                        $result = $s3Client->putObject([
                            'Bucket' => $bucket,
                            'Key' => $directory,
                            'Body' => file_get_contents($filename),
                            'ServerSideEncryption' => 'AES256',
                        ]);

                        if ($result['@metadata']['statusCode'] == '200') {
                            //file uploaded, add entry to database
                            $ds = new DocumentStorage();
                            $ds->partnerID($_GET['partner']);
                            $ds->object_id($arm->id());
                            $ds->object("ActionRequiredMessage");
                            $ds->filepath($directory);
                            $ds->original_name($f['basename']);
                            $ds->friendly_name("mandate." . $fileExtension);
                            $ds->hash($unique_name);
                            $ds->mime_type($fileExtension);
                            $ds->file_size(filesize($filename));

                            if (!$ds->save()) {
                                throw new Exception("Failed to save mandate in document storage table");
                            }
                        } else {
                            throw new Exception("Mandate failed to upload to S3, contact IT");
                        }
                    } else {
                        throw new Exception("Couldn't save action required message");
                    }
                } else {
                    throw new Exception("Couldn't create action required ticket");
                }
            } catch (Exception $e) {
                $json['success'] = false;
                $json['error'] = $e->getMessage();
            }

            echo json_encode($json);

            break;
    }
} elseif (isset($_GET['do'])) {
    switch ($_GET['do']) {
        case "list":
            if ($_GET['queue']) {
                $response = [
                    'success' => true,
                    'rows' => '',
                ];

                $listRow = Mandate::get_template("list-row.html");
                $rem_db = new mydb(REMOTE, E_USER_WARNING);
                $db = new mydb();

                $filesToRead = glob(MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/*.pdf");
                $files = [];

                //sort the files first
                foreach ($filesToRead as $file) {
                    //rename any files which used a separator sheet, namely, adviser charging forms
                    if (strpos($file, "PS-SEPARATOR") !== false){
                        //rename the file without this string
                        $newFileName = str_replace("PS-SEPARATOR", "", $file);

                        //strip the first page off as it will be the separator sheet
                        exec("gs -sDEVICE=pdfwrite -o ". $newFileName ."  -dFirstPage=2 ". $file);

                        //delete the original file
                        unlink($file);
                        $file = $newFileName;
                    }
                    /*
                     * If scans have been loaded previously, timestamp manipulation fails and throws out order of table.
                     * Instead, match any files that have already been named, add to array and sort from here.
                     * REGEX - e.g M123456.pdf
                     */
                    $filename = str_replace(MANDATE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/", "", $file);
                    if ((!in_array($filename, [".", ".."])) && (preg_match('/M[0-9]{6}\.pdf/', $filename))) {
                        $noM = str_replace('M', '', $filename);
                        $ts = str_replace('.pdf', '', $noM);

                        while (array_key_exists($ts, $files)) {
                            ++$ts;
                        }

                        $files[$ts] = $filename;
                    } elseif ((!in_array($filename, [".", ".."])) && (preg_match('/\d*\+?\d*\s*-\d+.pdf/', $filename))) {
                        /*
                        * Initial load of mandates after pages have been put through scanner......
                        *
                        * Sneaky suspicion Scan2PDF isn't splitting files from batch in order and causing issues when we
                        * come to order the mandates to be processed using mtime.  Changed the order of play a bit here and now
                        * parse out the timestamp from file name and order the files based on this.  Tested a few times locally
                        * with different orders of test scans and seems to be OK.  Time will tell....
                        * REGEX - e.g -123456789.pdf, 79-123456789.pdf, 79+12345-123456789.pdf
                        */
                        $pieces = explode('-', $filename);

                        $ts = str_replace('.pdf', '', $pieces[1]);

                        while (array_key_exists($ts, $files)) {
                            ++$ts;
                        }

                        $files[$ts] = $filename;
                    } else {
                        //we dont know what this file is, so just add it to the next available slot
                        # todo will this break as $ts is undefined
                        while (array_key_exists($ts, $files)) {
                            ++$ts;
                        }

                        $files[$ts] = $filename;
                    }
                }

                ksort($files);

                foreach ($files as $filename) {
                    $mandate = new Mandate();

                    //check if file has already been renamed and added to DB
                    if (preg_match('/M[0-9]{6}\.pdf/', $filename)) {
                        // REGEX e.g. M123456.pdf
                        //this is an existing mandate, strip the extra parts and check DB for ID
                        $id = str_replace(["M", "U"], "", $filename);
                        $id = str_replace(".pdf", "", $id);

                        $mandate->get($id);
                    } else {
                        // mandate hasnt been added to the DB
                        // hopefully filename matches one of the below
                        //REGEX e.g. -123456789.pdf, 79-123456789.pdf, 79+12345-123456789.pdf
                        if (preg_match('/^(\d+)\+(\d+)(\-\d+)?\.pdf$/', $filename, $capture)) {
                            // Prepopulated Mandate e.g. 79+12345-20110314.pdf
                            list(, , $id,) = $capture;

                            //get remote record
                            $rem_db->query("SELECT * FROM " . REMOTE_MANDATE_PREPOP_TBL . " WHERE id = " . $id);

                            if ($rem_row = $rem_db->next(MYSQLI_OBJECT)) {
                                if (empty($rem_row->client_id)) {
                                    //client doesnt exist, lets add a new one using the prepop fields
                                    $keys = [
                                        "title1" => "clientTitle1",
                                        "fname1" => "clientFname1",
                                        "sname1" => "clientSname1",
                                        "dob1" => "clientDOB1",
                                        "nino1" => "clientNINO1",
                                        "title2" => "clientTitle2",
                                        "fname2" => "clientFname2",
                                        "sname2" => "clientSname2",
                                        "dob2" => "clientDOB2",
                                        "nino2" => "clientNINO2",
                                        "lei1" => "clientLEI1",
                                        "lei2" => "clientLEI2",
                                        "line1" => "clientaddress1",
                                        "line2" => "clientaddress2",
                                        "line3" => "clientaddress3",
                                        "postcode" => "clientaddresspostcode",
                                        "partner_id" => "partner_id"
                                    ];

                                    $vals = [];
                                    foreach ($keys as $rem_key => $local_key) {
                                        $value = "NULL";
                                        if (!empty($rem_row->{$rem_key})) {
                                            $value = "'" . $db->real_escape_string($rem_row->{$rem_key}) . "'";
                                        }

                                        $vals[] = "$local_key = $value";
                                    }

                                    // hash the client data, to look for other mandates with this prepop data
                                    $data_hash = md5(implode($vals));
                                    $vals[] = "data_hash = '" . $db->real_escape_string($data_hash) . "'";

                                    $pending_client = $live_id = false;

                                    $db->query("select clientID, live_id from " . MANDATE_CLIENT_TBL . " where data_hash='" . $db->real_escape_string($data_hash) . "'");

                                    if ($match = $db->next(MYSQLI_NUM)) {
                                        ////  A match has been found
                                        list($pending_client, $live_id) = $match;
                                        if (!empty($live_id)) {
                                            // has already been added to the live tblclient, so update mandate with real client id
                                            $rem_row->client_id = $live_id;
                                        }
                                    } else {
                                        // create a new pending client entry
                                        $pending_client = $db->query("insert into " . MANDATE_CLIENT_TBL . " set " . implode(", ", $vals));
                                    }
                                }

                                $mandate->load([
                                    "added_when" => time(),
                                    "partner" => $rem_row->partner_id,
                                    "client" => intval($rem_row->client_id),
                                    "pending_client" => $pending_client,
                                    "track_id" => $rem_row->id,
                                    "prepop" => true
                                ]);

                                if ($mandate->save()) {
                                    $mandate->file("M" . $mandate->id() . ".pdf");
                                }
                            }
                        } elseif (preg_match('/^(\d+)(\-?\d*)\.pdf$/', $filename, $capture)) {
                            //// Unpopulated mandate eg. 79-20110314.tif
                            list(, $partner_id,) = $capture;

                            $mandate->load([
                                "partner" => $partner_id,
                                "added_when" => time()
                            ]);

                            if ($mandate->save()) {
                                $mandate->file("M" . $mandate->id() . ".pdf");
                            }
                        }
                    }

                    if (!$mandate->id()) {
                        // the mandate doesnt have an id, we must have failed to get
                        // something from the database in one of the above queries
                        $mandate->added_when(time());

                        if ($mandate->save()) {
                            $mandate->file("M" . $mandate->id() . ".pdf");
                        }
                    }

                    if ($mandate->file->edited) {
                        if ($mandate->rename_scan($filename, $_GET['queue'])) {
                            $mandate->save();
                        }
                    }

                    $response['rows'] .= $listRow(["mandate" => $mandate]);
                }

                echo json_encode($response);
            } else {
                //list all queues
                $response = [
                    'success' => true,
                    'queues' => []
                ];

                $queueDirs = glob(MANDATE_PENDING_SCAN_DIR . "/*", GLOB_ONLYDIR);

                foreach ($queueDirs as $queue) {
                    if (basename($queue) != "exceptions"){
                        $mandates = glob($queue . "/*.pdf");

                        $stat = stat($queue);

                        $response['queues'][] = [
                            "name" => basename($queue),
                            "file_count" => count($mandates),
                            "modified" => Carbon::createFromTimestamp($stat['mtime'])->toDayDateTimeString()
                        ];
                    }
                }

                //order the directories alphabetically
                sort($response['queues']);

                echo json_encode($response);
            }

            break;

        case "upload_form":
            echo new Template("mandate/upload_form.html", ["object" => $_GET['object'], "object_index" => $_GET['object_index']]);

            break;

        case "upload_mandate":
            $json = [
                'mandate_id' => false,
                'error' => false
            ];

            $time = time();

            if ($_GET['object'] === "Client") {
                $client = new Client($_GET['object_id'], true);
            } elseif ($_GET['object'] === "Partner") {
                $partner = new Partner($_GET['object_id'], true);
            }

            if (is_file($_FILES['file']['tmp_name'])) {

                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

                if (move_uploaded_file($_FILES['file']['tmp_name'], MANDATE_PENDING_SCAN_DIR . "/" . User::get_default_instance('id') . "-" . $time . "-" . $_FILES["file"]['name'])) {

                    try {
                        $mandate = new Mandate;

                        if (isset($client)) {
                            $mandate->load([
                                "added_when" => $time,
                                "partner" => intval($client->partner->id()),
                                "client" => intval($client->id()),
                                //"prepop" => true,
                                "dropped" => true,
                                "original_copy" => 1
                            ]);
                        } elseif (isset($partner)) {
                            $mandate->load([
                                "added_when" => $time,
                                "partner" => intval($partner->id()),
                                //"prepop" => true,
                                "dropped" => true,
                                "original_copy" => 1
                            ]);
                        }

                        if ($mandate->save()) {
                            // rename mandate file, give prefix UM (UploadMandate) so we can keep these separate from mandate system
                            $mandate->file("UM" . $mandate->id() . ".$ext");
                            if ($mandate->save()) {
                                if (rename(MANDATE_PENDING_SCAN_DIR . "/" . User::get_default_instance('id') . "-" . $time . "-" . $_FILES["file"]['name'], MANDATE_PENDING_SCAN_DIR . "/" . $mandate->file())) {
                                    $json['mandate_id'] = $mandate->id();
                                } else {
                                    $json['error'] = "Mandate could not be renamed, contact IT";
                                }
                            }
                        } else {
                            $json['error'] = "Mandate could not be saved into database, contact IT";
                        }
                    } catch (Exception $e) {
                        $json['error'] = "Mandate failed to add correctly, contact IT";
                    }
                } else {
                    $json['error'] = "File was not uploaded, try again or contact IT";
                }
            } else {
                $json['error'] = "File could not be uploaded, try again or contact IT";
            }
            echo json_encode($json);

            break;

        case "adviser_charging_forms":
            $partner = new Partner($_GET['partner'], true);
            $issuer = new Issuer($_GET['issuer'], true);
            $client = new Client($_GET['client'], true);
            $mandate = new Mandate($_GET['mandate'], true);

            $policies = json_decode($_GET['policies']);

            echo new Template("mandate/adviser_charging_forms.html", ["partner" => $partner, "issuer" => $issuer, "client" => $client, "mandate" => $mandate, "policies" => $policies]);

            break;

        case "policy_num_mapping":
            $response = [
                "success" => true,
                "data" => []
            ];

            $pnim = new PolicyNumberIssuerMap();

            if ($issuerID = $pnim->mapIssuer($_GET['policy_num'], $_GET['currentIssuer'])){
                $current = new Issuer($_GET['currentIssuer'], true);
                $guessed = new Issuer($issuerID, true);

                $response['data'] = [
                    "current" => strval($current),
                    "guessed" => strval($guessed),
                    "guessed_id" => $guessed->id(),
                ];
            } else {
                $response['success'] = false;
            }

            echo json_encode($response);

            break;

        case "load_number_issuer_mapping":
            $response = [
                "success" => true,
                "data" => null,
            ];

            $s = new Search(new PolicyNumberIssuerMap());

            $s->add_order("policy_num");

            while($pnim = $s->next(MYSQLI_ASSOC)){
                $response['data'] .= "<tr><td>" . $pnim->policy_number . "</td><td>" . strval($pnim->issuer) . "</td><td><button class='btn btn-danger delete-num-issuer-map' title='delete' data-id='" . $pnim->id() . "'><i class='fas fa-trash-alt'></i></button></td></tr>";
            }

            echo json_encode($response);

            break;

        case "save_number_issuer_mapping":
            $response = [
                "success" => true,
                "data" => null,
            ];

            try{
                $pnim = new PolicyNumberIssuerMap();

                $pnim->policy_number($_GET['number']);
                $pnim->issuer($_GET['issuer']);

                $pnim->save();
            } catch(Exception $e) {
                $response['success'] = false;
                $response['data'] = $e->getMessage();
            }

            echo json_encode($response);

            break;

        case "delete_number_issuer_mapping":
            $response = [
                "success" => true,
                "data" => null,
            ];

            $pnim = new PolicyNumberIssuerMap($_GET['map_id'], true);

            $response['success'] = $pnim->delete();

            if (!$response['success']){
                $response['data'] = "Failed to delete mapping. Try again or contact IT";
            }

            echo json_encode($response);

            break;
    }
}