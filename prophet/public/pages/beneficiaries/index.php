<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "list":
            $data = "";
            $s = new Search(new Beneficiaries());
            $s->eq("policy_id", $_GET['policy']);

            while ($x = $s->next(MYSQLI_ASSOC)) {
                $data .= "<tr id='" . $x->id() . "'>
						        <td>" . $x->id() . "</td>
						        <td>" . $x->firstname() . "</td>
						        <td>" . $x->surname() . "</td>
						        <td>" . $x->dob() . "</td>
						        <td>
                         <button id='" . $x->id() . "' class='edit' >Edit</button>
                         <button id='" . $x->id() . "' class='delete' ></button>
						        </td>
					           </tr>";
            }
            echo $data;
            break;

        case "create":
            echo new Template("beneficiaries/new.html", ["policy" => $_GET['policy']]);
            break;

        case "edit":
            //$b  = new Beneficiaries($_GET['beneficiary'],true);
            echo new Template("beneficiaries/edit.html", ["bene" => $_GET['id']]);
            break;

        case "delete":
            $json = [
                "status" => null,
                "error" => false
            ];

            if (isset($_GET['id'])) {
                $b = new Beneficiaries($_GET['id'], true);

                if ($b->id()) {
                    if ($b->delete()) {
                        $json['status'] = "Beneficiary deleted";
                    } else {
                        $json['status'] = "Beneficiary not deleted";
                        $json['error'] = true;
                    }
                } else {
                    $json['status'] = "Beneficiary not found";
                    $json['error'] = true;
                }
            } else {
                $json['status'] = "Beneficiary ID not passed through";
                $json['error'] = true;
            }

            echo json_encode($json);
            break;
    }
}
