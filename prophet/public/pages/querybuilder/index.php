<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if(isset($_GET['id'])){
    $query = new Query($_GET['id'], true);

    switch($_GET['do']){
        case "load_query":
            $response = [
                "success" => true,
                "data" => [
                    "id" => $query->id(),
                    "name" => $query->name(),
                    "selects" => $query->getSelects(),
                    "rules" => $query->getFilters(),
                    "grouping" => $query->grouping(),
                    "group_access" => $query->getGroupAccess(),
                    "user_access" => $query->getUserAccess(),
                ]
            ];

            echo json_encode($response);

            break;

        case "save_query":
            $response = [
                "success" => true,
                "data" => null,
                "id" => null,
            ];

            if($_POST['query_name'] != $query->name()){
                // query name was changed, make this a new query
                $query = new Query();
            }

            try{
                $query->name($_POST['query_name']);
                $query->select($_POST['selects_json']);
                $query->filters($_POST['filters_json']);
                $query->grouping($_POST['grouping_json']);

                if(array_key_exists("all", $_POST['query_groups'])){
                    $query->group_access(json_encode(["all"]));
                } else {
                    $query->group_access(json_encode($_POST['query_groups']));
                    $query->user_access(json_encode($_POST['query_users']));
                }

                $query->save();

                $response['id'] = $query->id();
                $response['data'] = "Saved Query";
            } catch(Exception $e) {
                $response['success'] = false;
                $response['data'] = $e->getMessage();
            }

            echo json_encode($response);

            break;

        case "run_raw_sql":
            $response = [
                "success" => true,
                "data" => null,
            ];

            $hasTitleRow = false;

            $sqlFile = SQL_QUERIES . $query->sql_file();

            if(file_exists($sqlFile)){
                $q = file_get_contents($sqlFile);

                if(!empty($_GET['raw_filters'])){
                    $q = vsprintf($q, $_GET['raw_filters']);
                }

                $db = new mydb(constant($query->connection()));

                $db->query($q);

                while($row = $db->next(MYSQLI_ASSOC)){
                    if(!$hasTitleRow){
                        $response['data'] .= implode(",", array_keys($row)) . urlencode("\r\n");
                        $hasTitleRow = true;
                    }

                    foreach($row as $key => $value){
                        $row[$key] = str_replace(",", ";", $value);
                    }
                    $response['data'] .= implode(urlencode(","), $row) . urlencode("\r\n");
                }
            } else {
                $response['success'] = false;
                $response['data'] = "Failed to find query file.";
            }

            echo json_encode($response);

            break;
    }
} else {
    if(isset($_GET['do'])){
        switch($_GET['do']){
            case "run_query":
                $json = [
                    "success" => true,
                    "data" => ""
                ];

                $keys = [];

                foreach($_POST['select'] as $column => $value){
                    foreach(explode(",", $column) as $c){
                        $keys[] = $c;
                    }
                }

                $json['data'] = implode(urlencode(","), $keys) . urlencode("\r\n");

                $select = implode(", ", $_POST['select']);

                $db = new mydb(REPORTING);

                $query = "SELECT " . $select . " FROM feebase.tblpartner
                            LEFT JOIN feebase.practice_staff ON tblpartner.partnerID = practice_staff.partner_id
                            LEFT JOIN feebase.practice ON practice_staff.practice_id = practice.id
                            LEFT JOIN feebase.tblbranch ON tblpartner.branchID = tblbranch.branchID
                            LEFT JOIN feebase.tblclient ON tblpartner.partnerID = tblclient.partnerID
                            LEFT JOIN feebase.tblpolicy ON tblclient.clientID = tblpolicy.clientID
                            LEFT JOIN feebase.tblissuer ON tblpolicy.issuerID = tblissuer.issuerID
                            LEFT JOIN feebase.tblpolicytype ON tblpolicy.policyTypeID = tblpolicytype.policyTypeID
                            LEFT JOIN feebase.tblpolicystatussql ON tblpolicy.Status = tblpolicystatussql.ID
                            LEFT JOIN prophet.countries AS client1_residency ON client1_residency.alpha_3_code = tblclient.residency1
                            LEFT JOIN prophet.countries AS client2_residency ON client2_residency.alpha_3_code = tblclient.residency2
                            LEFT JOIN feebase.servicing_proposition_levels ON tblclient.clientID = servicing_proposition_levels.client_id
                            WHERE " . $_POST['filters'] . "
                            GROUP BY " . $_POST['grouping'];

                $db->query($query);

                // TODO find a way to change delimiter to pipe "|" so we can have commas in the output

                while($row = $db->next(MYSQLI_ASSOC)){
                    foreach($row as $key => $value){
                        $row[$key] = str_replace(",", ";", $value);
                    }
                    $json['data'] .= implode(urlencode(","), $row) . urlencode("\r\n");
                }

                echo json_encode($json);

                break;

            case "save_query":
                $response = [
                    "success" => true,
                    "data" => null,
                    "id" => null,
                ];

                try{
                    $query = new Query();

                    $query->name($_POST['query_name']);
                    $query->select($_POST['selects_json']);
                    $query->filters($_POST['filters_json']);
                    $query->grouping($_POST['grouping_json']);

                    if(array_key_exists("all", $_POST['query_groups'])){
                        $query->group_access(json_encode(["all"]));
                    } else {
                        $query->group_access(json_encode($_POST['query_groups']));
                        $query->user_access(json_encode($_POST['query_users']));
                    }

                    $query->save();

                    $response['id'] = $query->id();
                    $response['data'] = "Saved Query";
                } catch(Exception $e) {
                    $response['success'] = false;
                    $response['data'] = $e->getMessage();
                }

                echo json_encode($response);

                break;
        }
    } else {
        $policyTypes = [];
        $policyStatus = [];
        $branches = [];
        $currentPartner = [];

        $db = new mydb();

        // policy types
        $s = new Search(new PolicyType());

        while($type = $s->next(MYSQLI_ASSOC)){
            $policyTypes[] = [
                $type->id() => $type->name()
            ];
        }

        // policy status
        $db->query("SELECT ID, Field1 FROM " . POLICY_STATUS_TBL);

        while($status = $db->next(MYSQLI_ASSOC)){
            $policyStatus[] = [
                $status['ID'] => $status['Field1']
            ];
        }

        $s = new Search(new Branch());

        while($branch = $s->next(MYSQLI_ASSOC)){
            $branches[] = [
                $branch->id() => $branch->name()
            ];
        }

        $db->query("SELECT currentPartnerID, description FROM feebase.tblcurrentpartner");

        while($cp = $db->next(MYSQLI_ASSOC)){
            $currentPartner[] = [
                $cp['currentPartnerID'] => $cp['description']
            ];
        }

        $policyTypes = addslashes(json_encode($policyTypes));
        $policyStatus = addslashes(json_encode($policyStatus));
        $branches = addslashes(json_encode($branches));
        $currentPartner = addslashes(json_encode($currentPartner));

        echo new Template("querybuilder/index.html", compact([
            "policyTypes",
            "policyStatus",
            "branches",
            "currentPartner"
        ]));
    }
}
