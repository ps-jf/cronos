<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "load_ac_outstanding":
            $json = (object)[
                "search" => null,
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $limit = 50;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            $db = new mydb();
            $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL .
                " WHERE sent_to_provider is null " .
                " OR sent_to_provider = '0000-00-00'" .
                " OR pack_produced = 0" .
                " OR illustration_provided = 0" .
                " ORDER BY " . AC_TBL . ".id DESC LIMIT " . $limit . " OFFSET " . $offset;
            $q1 = "SELECT FOUND_ROWS();";

            $db->query($q);
            $db->query($q1);

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages) {
                $i++;
                $page_no[] = $i;
            }

            if (count($page_no) > ceil($pages)) {
                array_pop($page_no);
            }

            $db->query($q);
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $ac = new AdviserCharging($row['id'], true);
                $po = new Policy($row['policyID'], true);
                $c = new Client($po->client(), true);
                $pa = new Partner($c->partner(), true);
                $i = new Issuer($po->issuer(), true);


                if ($ac->sent() != 0 || $ac->sent() != "") {
                    $sent = strtotime($ac->sent());
                    $sent = date('d/m/Y', $sent);
                } else {
                    $sent = "";
                }

                if ($ac->value_of_plan()) {
                    $title = "Value of Plan: £" . $ac->value_of_plan();
                } else {
                    $title = "";
                }

                $comment = str_replace(utf8_decode("£"), "&pound;", $row['comments']);
                $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);

                $results .= "<tr id='" . $row['id'] . "' title='" . $title . "'>
									<td>" . $c->link() . "</td>
									<td>" . $pa->link() . "</td>
									<td>" . $po->link() . "</td>
									<td>" . $i->link() . "</td>
									<td>" . $ac->ac_requested . "%</td>
									<td>&pound;" . $ac->fee_requested . "</td>
									<td>" . $ac->vat . "</td>
									<td class='sent'>" . $sent . "</td>
									<td>" . $ac->pack_produced . "</td>
									<td>" . $ac->illustration_provided . "</td>
									<td><button id='" . $row['id'] . "' class='edit'>Update</button>
									<button id='" . $row['id'] . "' class='delete'></button></td>
								</tr>";

                if ($row['comments']) {
                    $results .= "<tr><td colspan='11'>" . $comment . "</td></tr>";
                }
            }

            if ($q && $q1) {
                $json->status = true;
            } else {
                $json->status = false;
                $json->error = "An error has occurred while trying to query the database. Please try again, if this persists contact IT.";
            }

            if ($results == null) {
                $results = "<tr><td>There are no more Adviser Charging entries to show</td></tr>";
            }

            $json->page_no = $page_no;
            $json->record_count = $x;
            $json->pages = ceil($pages);
            $json->data = $results;

            echo json_encode($json);

            break;

        case "load_ac_complete":
            $json = (object)[
                "search" => null,
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $limit = 50;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            $db = new mydb();
            $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL .
                " WHERE sent_to_provider is not null " .
                " AND sent_to_provider != '0000-00-00'" .
                " AND pack_produced = 1" .
                " AND illustration_provided = 1" .
                " ORDER BY " . AC_TBL . ".id DESC LIMIT " . $limit . " OFFSET " . $offset;
            $q1 = "SELECT FOUND_ROWS();";

            $db->query($q);
            $db->query($q1);

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages) {
                $i++;
                $page_no[] = $i;
            }

            if (count($page_no) > ceil($pages)) {
                array_pop($page_no);
            }

            $db->query($q);
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $ac = new AdviserCharging($row['id'], true);
                $po = new Policy($row['policyID'], true);
                $c = new Client($po->client(), true);
                $pa = new Partner($c->partner(), true);
                $i = new Issuer($po->issuer(), true);


                if ($ac->sent() != 0 || $ac->sent() != "") {
                    $sent = strtotime($ac->sent());
                    $sent = date('d/m/Y', $sent);
                } else {
                    $sent = "";
                }

                if ($ac->value_of_plan()) {
                    $title = "Value of Plan: £" . $ac->value_of_plan();
                } else {
                    $title = "";
                }

                $comment = str_replace(utf8_decode("£"), "&pound;", $row['comments']);
                $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);

                $results .= "<tr id='" . $row['id'] . "' title='" . $title . "'>
									<td>" . $c->link() . "</td>
									<td>" . $pa->link() . "</td>
									<td>" . $po->link() . "</td>
									<td>" . $i->link() . "</td>
									<td>" . $ac->ac_requested . "%</td>
									<td>&pound;" . $ac->fee_requested . "</td>
									<td>" . $ac->vat . "</td>
									<td class='sent'>" . $sent . "</td>
									<td>" . $ac->pack_produced . "</td>
									<td>" . $ac->illustration_provided . "</td>
									<td><button id='" . $row['id'] . "' class='edit'>Update</button></td>
								</tr>";

                if ($row['comments']) {
                    $results .= "<tr><td colspan='11'>" . $comment . "</td></tr>";
                }
            }

            if ($q && $q1) {
                $json->status = true;
            } else {
                $json->status = false;
                $json->error = "An error has occurred while trying to query the database. Please try again, if this persists contact IT.";
            }

            if ($results == null) {
                $results = "<tr><td>There are no more Adviser Charging entries to show</td></tr>";
            }

            $json->page_no = $page_no;
            $json->record_count = $x;
            $json->pages = ceil($pages);
            $json->data = $results;

            echo json_encode($json);

            break;

        case "create":
            $object = $_GET['object'];
            $object_id = $_GET['object_id'];

            $client = null;

            if ((isset($object_id)) && (isset($object))) {
                //Can be used at client level for selecting single policies, currently '$multiple'shows rather than this
                if ($object == "client") {
                    $client = new Client($object_id, true);

                    $db = new mydb();
                    $q = "SELECT * FROM " . POLICY_TBL . " WHERE clientID = " . $object_id;
                    $db->query($q);

                    $policy_select = "<select id='policy_select'><option value=''>-- Select Policy --</option>";
                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        $p_id = $row['policyID'];
                        $p_num = $row['policyNum'];

                        $policy_select .= "<option value='" . $p_id . "'>" . $p_num . "</option>";
                    }

                    $policy_select .= "</select>";

                    //allow AC to be added to multiple policies
                    $db->query($q);

                    $multiple .= "<table id='multipol_choice' class='scrollable'><thead><tr><th>Policy Number</th><th>Select</th></tr></thead><tbody>";
                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        $p_id = $row['policyID'];
                        $p = new Policy($p_id, true);

                        $multiple .= "<tr><td>" . $p->link() . "</td><td> <input type='checkbox' id='" . $p->id . "' class='pols' /></td></tr>";
                    }

                    $multiple .= "</tbody></table>";
                } else if ($object == "policy") {
                    //pass through policy details

                    $policy = new Policy($object_id, true);
                }
            }

            $ac = new AdviserCharging();
            echo new Template("advisercharging/new.html", ["ac" => $ac, "policies" => $policy_select, "policy" => $policy, "multiple" => $multiple, "client" => $client]);

            break;

        case "save":
            $json = [
                "status" => false,
                "error" => false,
                "prompt" => false,
                "id" => false
            ];

            try {
                if (is_array($_GET['policy'])) {
                    $comment = str_replace(utf8_decode("£"), "&pound;", $_GET['comments']);
                    $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);
                    $comment = str_replace(utf8_decode("%"), "&percnt;", $comment);

                    $i = 0;
                    foreach ($_GET['policy'] as $p) {
                        if ($_GET['policy'] != "") {
                            $p = new Policy($_GET['policy'][$i], true);
                            $p->rate($_GET['ac_requested']);

                            if ($p->save()) {
                                $c = new Client($p->client(), true);

                                $ac = new AdviserCharging();

                                $ac->ac_requested($_GET['ac_requested']);
                                $ac->fee_requested($_GET['fee_requested']);
                                $ac->policy($p->id);
                                $ac->vat($_GET['vat']);
                                $ac->sent($_GET['sent_provider']);
                                $ac->pack_produced($_GET['pack_produced']);
                                $ac->comments(addslashes($comment));
                                $ac->illustration_provided($_GET['illustration_provided']);
                                //$ac -> value_of_plan($_GET['value_of_plan']);
                                $ac->payment_frequency($_GET['payment_frequency']);
                                $ac->save();

                                $json['status'] = "Entry has been Successfully added.";

                                $db = new mydb();
                                $q = "SELECT id FROM " . AC_TBL . " ORDER BY id DESC LIMIT 1";
                                $db->query($q);

                                while ($row = $db->next(MYSQLI_ASSOC)) {
                                    $id = $row['id'];
                                }

                                if ($_GET['pack_produced'] == true || $_GET['pack_produced'] == 1) {
                                    $json['prompt'] = true;
                                    $json['id'] = $id;
                                }
                            }

                        } else {
                            $json['error'] = true;
                            $json['status'] = "Please select a policy using the search feature above";
                        }
                        $i++;
                    }
                } else {
                    if ($_GET['policy'] != "") {
                        $p = new Policy($_GET['policy'], true);
                        $p->rate($_GET['ac_requested']);
                        if ($p->save()) {
                            $c = new Client($p->client(), true);

                            $ac = new AdviserCharging();
                            $ac->ac_requested($_GET['ac_requested']);
                            $ac->fee_requested($_GET['fee_requested']);
                            $ac->policy($p->id);
                            $ac->vat($_GET['vat']);
                            $ac->sent($_GET['sent_provider']);
                            $ac->pack_produced($_GET['pack_produced']);
                            $ac->comments(addslashes($_GET['comments']));
                            $ac->illustration_provided($_GET['illustration_provided']);
                            //$ac -> value_of_plan($_GET['value_of_plan']);
                            $ac->payment_frequency($_GET['payment_frequency']);
                            $ac->save();

                            $json['status'] = "Entry has been Successfully added.";

                            $db = new mydb();
                            $q = "SELECT id FROM " . AC_TBL . " ORDER BY id DESC LIMIT 1";
                            $db->query($q);

                            while ($row = $db->next(MYSQLI_ASSOC)) {
                                $id = $row['id'];
                            }

                            if ($_GET['pack_produced'] == true || $_GET['pack_produced'] == 1) {
                                $json['prompt'] = true;
                                $json['id'] = $id;
                            }
                        }

                    } else {
                        $json['error'] = true;
                        $json['status'] = "Please select a policy using the search feature above";
                    }
                }
            } catch (Exception $e) {
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "delete":
            if ($_GET['id']) {
                $db = new mydb();
                $db->query("DELETE FROM " . AC_TBL . " WHERE id = " . $_GET['id']);
            }

            break;

        case "edit":
            $ac = new AdviserCharging($_GET['id'], true);
            $po = new Policy($ac->policy, true);
            $c = new Client($po->client(), true);
            $pa = new Partner($c->partner(), true);
            $i = new Issuer($po->issuer(), true);

            echo new Template("advisercharging/edit.html", ["ac" => $ac, "c" => $c, "pa" => $pa, "po" => $po, "i" => $i]);

            break;

        case "update":
            $json = [
                "status" => false,
                "error" => false,
                "prompt" => false,
                "id" => false
            ];

            try {
                $comment = str_replace(utf8_decode("£"), "&pound;", $_GET['comments']);
                $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);

                $ac = new AdviserCharging($_GET['id'], true);

                $db = new mydb();
                $q = "UPDATE " . AC_TBL . " SET
							AC_requested = " . $_POST['advisercharging']['ac_requested'] . ",
							fee_requested = " . $_POST['advisercharging']['fee_requested'] . ",
							vat_included = " . $_GET['vat'] . ",
							sent_to_provider = '" . $_POST['advisercharging']['sent']['Y'] . "-" . $_POST['advisercharging']['sent']['M'] . "-" . $_POST['advisercharging']['sent']['D'] . "',
							pack_produced = " . $_GET['pack_produced'] . ",
							illustration_provided = " . $_GET['illustration_provided'] . ",
							comments = '" . addslashes($comment) . "',
							expected_first_pay = '" . $_POST['advisercharging']['expected_first_pay']['Y'] . "-" . $_POST['advisercharging']['expected_first_pay']['M'] . "-" . $_POST['advisercharging']['expected_first_pay']['D'] . "',
							payment_frequency = '" . $_POST['advisercharging']['payment_frequency'] . "'
							WHERE id = " . $_GET['id'];
                $db->query($q);

                if ($q) {
                    $p = new Policy($ac->policy(), true);
                    $p->rate($_POST['advisercharging']['ac_requested']);
                    if ($p->save()) {
                        $json['status'] = "Entry has been Successfully updated.";

                        if ($_GET['pack_produced'] == true || $_GET['pack_produced'] == 1) {
                            $json['prompt'] = true;
                            $json['id'] = $_GET['id'];
                        }
                    } else {
                        $json['error'] = true;
                        $json['status'] = "Charging entry updated, policy not updated.";
                    }

                }
            } catch (Exception $e) {
                $json['error'] = true;
                $json['status'] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "prompt_upload":
            $ac = new AdviserCharging($_GET['id'], true);
            $c = new Correspondence();
            echo new Template("advisercharging/upload.html", ["ac" => $ac, "c" => $c]);

            break;

        case "upload":
            $json = [
                "status" => false,
                "error" => false,
                "dir" => false,
                "counter" => false
            ];

            $timestamp = User::get_default_instance('id') . $_GET['timestamp'];

            try {
                $output_dir = ADVISER_CHARGING_UPLOADS . $timestamp . "/";

                if (!file_exists($output_dir)) {
                    if (!mkdir($output_dir, 0777, true)) {
                        $json['error'] = "Failed to create directory, Please contact IT.";
                        echo json_encode($json);
                        exit();
                    }
                }

                if (isset($_FILES["file"])) {
                    //Filter the file types , if you want.
                    if ($_FILES["file"]["error"] > 0) {
                        $json['error'] = true;
                        $json['status'] = "Error: " . $_FILES["file"]["error"] . " ";
                        echo json_encode($json);
                        exit();
                    } else {
                        $file_order = explode(',', $_GET['file_order']);

                        foreach ($file_order as $key => $item) {
                            if (htmlspecialchars($_FILES["file"]["name"]) == $item) {
                                if (move_uploaded_file($_FILES["file"]["tmp_name"], $output_dir . $key . ".pdf")) {
                                    $json['counter'] = true;
                                } else {
                                    $json['counter'] = false;
                                    $json['error'] = "Failed to upload file, Please contact IT.";
                                    echo json_encode($json);
                                    exit();
                                }
                            }
                        }

                        $json['status'] = "Uploaded File :" . $_FILES["file"]["name"];
                        $json['dir'] = $timestamp;
                    }
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "merge":
            $json = [
                "status" => false,
                "error" => false,
                "filename" => false,
                "filepath" => false
            ];

            try {
                $output_dir = ADVISER_CHARGING_UPLOADS . $_GET['dir'] . "/";

                $files = scandir($output_dir);
                $files = glob($output_dir . '/*.{pdf}', GLOB_BRACE);

                $file_order = [];
                $i = 0;

                foreach ($files as $file) {
                    $file_order[$i] = $file;
                    $i++;
                }

                $input = implode(' ', $file_order);

                //Add new correspondence item
                $ac = new AdviserCharging($_GET['ac_id'], true);
                $po = new Policy($ac->policy, true);
                $cl = new Client($po->client(), true);
                $pa = new Partner($cl->partner(), true);
                $c = new Correspondence();

                $c->client($cl->id);
                $c->partner($pa->id);
                $c->policy($po->id);

                if ($_GET["subject"]) {
                    $c->subject($_GET["subject"]);
                } else {
                    $c->subject("Adviser Pack Sent To Partner");
                }

                $c->message($_GET["message"]);
                $c->dragdropflag(1);

                $c->save();


                //get ID of correspondence item
                $db = new mydb();
                $q = "SELECT * FROM " . CORR_TBL . " WHERE sender = " . User::get_default_instance('id') . " ORDER BY CorrID DESC LIMIT 1";
                $db->query($q);
                if ($x = $db->next(MYSQLI_ASSOC)) {
                    $corr = array_values($x);
                }

                $output = $output_dir . $corr[0] . ".pdf";

                // Merge all uploaded PDFs into one combined PDF.
                // Check to see if Live or Development server. exec("gs") for Linux (Live), exec("gswin64") for Windows (Development)

                $whitelist = [
                    '127.0.0.1',
                    '127.0.0.1:8080',
                    '127.0.0.1:8088',
                    '::1',
                    'localhost:8080',
                    'localhost:8088',
                    '10.0.2.2'
                ];

                if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                    echo exec("gswin64 -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$output $input", $out, $rv);
                } else {
                    echo exec("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$output $input", $out, $rv);
                }

                if ($rv != 0) {
                    $json["status"] = false;
                    $json['error'] = "Error creating pack, contact IT.";
                } else {
                    $ac = new AdviserChargingFile();
                    $ac->ac_id($_GET['ac_id']);
                    $ac->filepath($output);
                    $ac->save();
                    $current = $output;
                    $new = CORRESPONDENCE_PROCESSED_SCAN_DIR . $corr[0] . ".pdf";

                    if (!copy($current, $new)) {
                        $json['error'] = "Error moving files from " . $current . " to " . $new . ". Please contact IT";
                    } else {
                        if (file_exists($new)) {
                            // delete all files within temp_dir
                            array_map('unlink', glob("$current/*.*"));
                            // remove temp_dir
                            rmdir($current);

                            $json['status'] = "File was successfully uploaded";
                        } else {
                            $json['status'] = "New file does not exist";
                        }
                    }
                }
            } catch (Exception $e) {
                $json['error'] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "policy_search":
            $search = new Search(new Policy);

            $search
                ->flag("number", Search::DEFAULT_STRING, Search::DEFAULT_INT)
                ->flag("id")
                ->flag("partner", "client->partner")
                ->flag("issuer", "issuer")
                ->flag("has_mandate", Search::CUSTOM_FUNC, "policy_has_mandate")
                ->flag("type")
                ->flag("mandate");

            if (isset($_GET["search"])) {
                if (isset($_GET["filter"]) && is_array($_GET["filter"])) {
                    $search->apply_filters($_GET["filter"]);
                }

                $show_client = $show_partner = $show_type = true;

                // apply profile filters
                if (isset($_GET["client"])) {
                    $show_client = false;
                    $show_partner = false;

                    $search->eq("client", (int)$_GET["client"]);
                    $search->add_order("issuer->name", "ASC");
                }

                //if we want to restrict search to one specific issuer
                if (isset($_GET["issuer"])) {
                    $search->eq("issuer", (int)$_GET["issuer"]);
                }

                //exclude archived policies from search results
                $search->eq("archived", null);

                if (strlen($_GET["search"]) < 3) {
                    echo json_encode("Do not search for less than 3 characters");
                    exit();
                }

                if (!isset($excludes)) {
                    $excludes = [];
                }

                $t = AdviserCharging::get_template('list_row.html');
                $i = 0;
                while ($policy = $search->next($_GET["search"])) {
                    $t->tags([
                        "excludes" => $excludes,
                        "policy" => $policy,
                        "client" => $policy->client,
                        "issuer" => $policy->issuer,
                        "partner" => $policy->client->get_object()->partner
                    ]);

                    echo "$t";
                }
            } else {
                $ac = new AdviserCharging($_GET['id'], true);
                $search_policy = new Search(new Policy);

                $search_policy
                    ->flag("number", Search::DEFAULT_STRING, Search::DEFAULT_INT)
                    ->flag("id")
                    ->flag("partner", "client->partner")
                    ->flag("issuer", "issuer")
                    ->flag("has_mandate", Search::CUSTOM_FUNC, "policy_has_mandate")
                    ->flag("type")
                    ->flag("mandate");

                echo new Template("advisercharging/policy_search.html", ["ac" => $ac, "policy_help" => $search_policy->help_html()]);
            }

            break;

        case "search_outstanding":
            $json = (object)[
                "search" => $_GET['search_data'],
                "search_on" => $_GET['search_on'],
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $db = new mydb();

            $limit = 50;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            if ($_GET['search_on'] == "policy") {
                if ((TRIM($_GET['search_data'] == "")) || (TRIM($_GET['search_data']) == "Search on Policy Number..")) {
                    $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL .
                        " WHERE sent_to_provider is null " .
                        " OR sent_to_provider = '0000-00-00'" .
                        " OR pack_produced = 0" .
                        " OR illustration_provided = 0" .
                        " LIMIT " . $limit . " OFFSET " . $offset;
                    $q1 = "SELECT FOUND_ROWS();";
                } else {
                    $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                        " INNER JOIN " . POLICY_TBL . " p" .
                        " ON a.policyID = p.policyID" .
                        " WHERE p.policyNum LIKE '%" . TRIM($_GET['search_data']) . "%'" .
                        " AND (sent_to_provider is null OR sent_to_provider = '0000-00-00' OR pack_produced = 0 OR illustration_provided = 0)" .
                        " LIMIT " . $limit . " OFFSET " . $offset;
                    $q1 = "SELECT FOUND_ROWS();";
                }
            } else if ($_GET['search_on'] == "issuer") {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                    " INNER JOIN " . POLICY_TBL . " p" .
                    " ON a.policyID = p.policyID" .
                    " WHERE p.issuerID LIKE '%" . TRIM($_GET['search_data_issuer']) . "%'" .
                    " AND (sent_to_provider is null OR sent_to_provider = '0000-00-00' OR pack_produced = 0 OR illustration_provided = 0)" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            } else if ($_GET['search_on'] == "partner") {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                    " INNER JOIN " . POLICY_TBL . " p" .
                    " ON a.policyID = p.policyID" .
                    " INNER JOIN " . CLIENT_TBL . " c" .
                    " ON p.clientID = c.clientID" .
                    " INNER JOIN " . PARTNER_TBL . " pa" .
                    " ON c.partnerID = pa.partnerID" .
                    " WHERE pa.partnerID LIKE '%" . TRIM($_GET['search_data_partner']) . "%'" .
                    " AND (sent_to_provider is null OR sent_to_provider = '0000-00-00' OR pack_produced = 0 OR illustration_provided = 0)" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            } else if ($_GET['search_on'] == "both") {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                    " INNER JOIN " . POLICY_TBL . " p" .
                    " ON a.policyID = p.policyID" .
                    " INNER JOIN " . CLIENT_TBL . " c" .
                    " ON p.clientID = c.clientID" .
                    " INNER JOIN " . PARTNER_TBL . " pa" .
                    " ON c.partnerID = pa.partnerID" .
                    " WHERE pa.partnerID LIKE '%" . TRIM($_GET['search_data_partner']) . "%'" .
                    " AND p.issuerID LIKE '%" . TRIM($_GET['search_data_issuer']) . "%'" .
                    " AND (sent_to_provider is null OR sent_to_provider = '0000-00-00' OR pack_produced = 0 OR illustration_provided = 0)" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            } else {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                    " INNER JOIN " . POLICY_TBL . " p" .
                    " ON a.policyID = p.policyID" .
                    " WHERE p.policyNum LIKE '%" . TRIM($_GET['search_data']) . "%'" .
                    " AND (sent_to_provider is null OR sent_to_provider = '0000-00-00' OR pack_produced = 0 OR illustration_provided = 0)" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            }


            $db->query($q);
            $db->query($q1);

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i < $pages) {
                $i++;
                $page_no[] = $i;
            }

            $db->query($q);

            try {
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $ac = new AdviserCharging($row['id'], true);
                    $po = new Policy($row['policyID'], true);
                    $c = new Client($po->client(), true);
                    $pa = new Partner($c->partner(), true);
                    $i = new Issuer($po->issuer(), true);


                    if ($ac->sent() != 0 || $ac->sent() != "") {
                        $sent = strtotime($ac->sent());
                        $sent = date('d/m/Y', $sent);
                    } else {
                        $sent = "";
                    }

                    if ($ac->value_of_plan()) {
                        $title = "Value of Plan: £" . $ac->value_of_plan();
                    } else {
                        $title = "";
                    }

                    $comment = str_replace(utf8_decode("£"), "&pound;", $row['comments']);
                    $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);

                    $results .= "<tr id='" . $row['id'] . "' title='" . $title . "'>
										<td>" . $c->link() . "</td>
										<td>" . $pa->link() . "</td>
										<td>" . $po->link() . "</td>
										<td>" . $i->link() . "</td>
										<td>" . $ac->ac_requested . "%</td>
										<td>&pound;" . $ac->fee_requested . "</td>
										<td>" . $ac->vat . "</td>
										<td class='sent'>" . $sent . "</td>
										<td>" . $ac->pack_produced . "</td>
										<td>" . $ac->illustration_provided . "</td>
										<td><button id='" . $row['id'] . "' class='edit'>Update</button>
										<button id='" . $row['id'] . "' class='delete'></button></td>
									</tr>";

                    if ($row['comments']) {
                        $results .= "<tr><td colspan='11'>" . $comment . "</td></tr>";
                    }
                }
            } catch (Exception $e) {
                $json["error"] = $e->getMessage();
            }

            if ($results != "") {
                $json->data = $results;
                $json->status = true;
            } else {
                $json->data = "<tr><td>There are no matches for the current search criteria</td></tr>";
            }

            $json->page_no = $page_no;
            $json->record_count = $x;
            $json->pages = ceil($pages);

            echo json_encode($json);

            break;

        case "search_complete":
            $json = (object)[
                "search" => $_GET['search_data'],
                "search_on" => $_GET['search_on'],
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $db = new mydb();

            $limit = 50;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            if ($_GET['search_on'] == "policy") {
                if ((TRIM($_GET['search_data'] == "")) || (TRIM($_GET['search_data']) == "Search on Policy Number..")) {
                    $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL .
                        " WHERE sent_to_provider is not null " .
                        " AND sent_to_provider != '0000-00-00'" .
                        " AND pack_produced = 1" .
                        " AND illustration_provided = 1" .
                        " LIMIT " . $limit . " OFFSET " . $offset;
                    $q1 = "SELECT FOUND_ROWS();";
                } else {
                    $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                        " INNER JOIN " . POLICY_TBL . " p" .
                        " ON a.policyID = p.policyID" .
                        " WHERE p.policyNum LIKE '%" . TRIM($_GET['search_data']) . "%'" .
                        " AND sent_to_provider is not null" .
                        " AND sent_to_provider != '0000-00-00'" .
                        " AND pack_produced = 1" .
                        " AND illustration_provided = 1" .
                        " LIMIT " . $limit . " OFFSET " . $offset;
                    $q1 = "SELECT FOUND_ROWS();";
                }
            } else if ($_GET['search_on'] == "issuer") {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                    " INNER JOIN " . POLICY_TBL . " p" .
                    " ON a.policyID = p.policyID" .
                    " WHERE p.issuerID LIKE '%" . TRIM($_GET['search_data_issuer']) . "%'" .
                    " AND sent_to_provider is not null" .
                    " AND sent_to_provider != '0000-00-00'" .
                    " AND pack_produced = 1" .
                    " AND illustration_provided = 1" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            } else if ($_GET['search_on'] == "partner") {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                    " INNER JOIN " . POLICY_TBL . " p" .
                    " ON a.policyID = p.policyID" .
                    " INNER JOIN " . CLIENT_TBL . " c" .
                    " ON p.clientID = c.clientID" .
                    " INNER JOIN " . PARTNER_TBL . " pa" .
                    " ON c.partnerID = pa.partnerID" .
                    " WHERE pa.partnerID LIKE '%" . TRIM($_GET['search_data_partner']) . "%'" .
                    " AND sent_to_provider is not null" .
                    " AND sent_to_provider != '0000-00-00'" .
                    " AND pack_produced = 1" .
                    " AND illustration_provided = 1" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            } else if ($_GET['search_on'] == "both") {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL . " a" .
                    " INNER JOIN " . POLICY_TBL . " p" .
                    " ON a.policyID = p.policyID" .
                    " INNER JOIN " . CLIENT_TBL . " c" .
                    " ON p.clientID = c.clientID" .
                    " INNER JOIN " . PARTNER_TBL . " pa" .
                    " ON c.partnerID = pa.partnerID" .
                    " WHERE pa.partnerID LIKE '%" . TRIM($_GET['search_data_partner']) . "%'" .
                    " AND p.issuerID LIKE '%" . TRIM($_GET['search_data_issuer']) . "%'" .
                    " AND sent_to_provider is not null" .
                    " AND sent_to_provider != '0000-00-00'" .
                    " AND pack_produced = 1" .
                    " AND illustration_provided = 1" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            } else {
                $q = " SELECT SQL_CALC_FOUND_ROWS * FROM " . AC_TBL .
                    " WHERE sent_to_provider is not null " .
                    " AND sent_to_provider != '0000-00-00'" .
                    " AND pack_produced = 1" .
                    " AND illustration_provided = 1" .
                    " LIMIT " . $limit . " OFFSET " . $offset;
                $q1 = "SELECT FOUND_ROWS();";
            }
            
            $db->query($q);
            $db->query($q1);

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i < $pages) {
                $i++;
                $page_no[] = $i;
            }

            $db->query($q);

            try {
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $ac = new AdviserCharging($row['id'], true);
                    $po = new Policy($row['policyID'], true);
                    $c = new Client($po->client(), true);
                    $pa = new Partner($c->partner(), true);
                    $i = new Issuer($po->issuer(), true);


                    if ($ac->sent() != 0 || $ac->sent() != "") {
                        $sent = strtotime($ac->sent());
                        $sent = date('d/m/Y', $sent);
                    } else {
                        $sent = "";
                    }

                    if ($ac->value_of_plan()) {
                        $title = "Value of Plan: £" . $ac->value_of_plan();
                    } else {
                        $title = "";
                    }

                    $comment = str_replace(utf8_decode("£"), "&pound;", $row['comments']);
                    $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);

                    $results .= "<tr id='" . $row['id'] . "' title='" . $title . "'>
										<td>" . $c->link() . "</td>
										<td>" . $pa->link() . "</td>
										<td>" . $po->link() . "</td>
										<td>" . $i->link() . "</td>
										<td>" . $ac->ac_requested . "%</td>
										<td>&pound;" . $ac->fee_requested . "</td>
										<td>" . $ac->vat . "</td>
										<td class='sent'>" . $sent . "</td>
										<td>" . $ac->pack_produced . "</td>
										<td>" . $ac->illustration_provided . "</td>
										<td><button id='" . $row['id'] . "' class='edit'>Update</button></td>
									</tr>";

                    if ($row['comments']) {
                        $results .= "<tr><td colspan='11'>" . $comment . "</td></tr>";
                    }
                }
            } catch (Exception $e) {
                $json["error"] = $e->getMessage();
            }

            if ($results != "") {
                $json->data = $results;
                $json->status = true;
            } else {
                $json->data = "<tr><td>There are no matches for the current search criteria</td></tr>";
            }

            $json->page_no = $page_no;
            $json->record_count = $x;
            $json->pages = ceil($pages);

            echo json_encode($json);

            break;

        case "plugin":
            $json = (object)[
                "status" => false,
                "error" => false,
                "data" => null
            ];

            $data = "";

            $object = $_GET['object'];
            $object_id = $_GET['object_id'];

            $c = new Client($_GET['object_id'], true);

            $db = new mydb();

            if ($object == "client") {
                $q = "SELECT * FROM " . AC_TBL . " a
                            INNER JOIN " . POLICY_TBL . " p
                            ON a.policyID = p.policyID
                            INNER JOIN " . CLIENT_TBL . " c
                            ON p.clientID = c.clientID
                            WHERE c.clientID = " . $object_id . "
                            ORDER BY id DESC";
            } else if ($object == "policy") {
                $q = "SELECT * FROM " . AC_TBL . " WHERE policyID = " . $object_id . " ORDER BY id DESC";
            }

            $db->query($q);

            try {
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $ac = new AdviserCharging($row['id'], true);
                    $po = new Policy($row['policyID'], true);
                    $c = new Client($po->client(), true);
                    $pa = new Partner($c->partner(), true);
                    $i = new Issuer($po->issuer(), true);

                    if ($ac->sent() != 0 || $ac->sent() != "") {
                        $sent = strtotime($ac->sent());
                        $sent = date('d/m/Y', $sent);
                    } else {
                        $sent = "";
                    }

                    if ($ac->value_of_plan()) {
                        $title = "Value of Plan: £" . $ac->value_of_plan();
                    } else {
                        $title = "";
                    }

                    $comment = str_replace(utf8_decode("£"), "&pound;", $row['comments']);
                    $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);

                    $data .= "<tr id='" . $row['id'] . "' title='" . $title . "'>
                                    <td>" . $po->link() . "</td>
                                    <td>" . $i->link() . "</td>
                                    <td>" . $ac->ac_requested . "%</td>
                                    <td>&pound;" . $ac->fee_requested . "</td>
                                    <td>" . $ac->vat . "</td>
                                    <td class='sent'>" . $sent . "</td>
                                    <td>" . $ac->pack_produced . "</td>
                                    <td>" . $ac->illustration_provided . "</td>
                                    <td>" . $ac->payment_frequency . "</td>
                                    <td><button id='" . $row['id'] . "' class='edit'>Update</button>
                                    <button id='" . $row['id'] . "' class='delete'></button>";

                    $data .= "</td></tr>";

                    if ($row['comments']) {
                        $data .= "<tr><td colspan='11'>" . $comment . "</td></tr>";
                    }
                }
            } catch (Exception $e) {
                $json["error"] = $e->getMessage();
            }

            if (isset($data)) {
                if ($data != "") {
                    $json->data = $data;
                    $json->status = true;
                } else {
                    $json->data = "<tr><td>There are no matches for the current search criteria</td></tr>";
                }
            } else {
                $json->data = "<tr><td>There are no matches for the current search criteria</td></tr>";
            }

            echo json_encode($json);

            break;

        case "suggested_rate":
            $json = [
                "status" => false,
                "error" => false,
                "rate" => null
            ];

            try {
                $p = new Policy($_GET['id'], true);
                $rate = $p->ac_percent();

                if ($rate) {
                    $json['rate'] = $rate;
                    $json['status'] = true;
                }
            } catch (Exception $e) {
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);


            break;

        case "check_ac_applied":
            $json = [
                "status" => false,
                "error" => false,
                "ac_applied" => null
            ];

            try {
                $s = new Search(new Commission);
                $s->eq('policy', $_GET['policyid']);
                $s->eq('type', 8);


                if ($c = $s->next(MYSQLI_ASSOC)) {
                    $json['ac_applied'] = true;
                    $json['status'] = true;
                } else {
                    $json['ac_applied'] = false;
                    $json['status'] = true;
                }
            } catch (Exception $e) {
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;
    }
} else {
    echo new Template("advisercharging/index.html", []);
}
