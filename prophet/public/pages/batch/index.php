<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "../bin/_main.php");


if (isset($_GET["id"])) {
    $batch_id = (int) $_GET["id"];

    $batch = new Batch($batch_id, true);

    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "view":
                $path = $batch->get_path();

                if (file_exists($path)) {

                    $allowedFileSize = 40000000; //40mb

                    if(filesize($path) > $allowedFileSize){
                        $path = str_replace(COMM_SCAN_DIR, "terry2/share/psfiles/commstore", $path);
                        echo "This file exceeds Prophet's max file size (" . number_format($allowedFileSize/1000000, 2) . "mb). To view the file, copy and paste the link below into a new tab.<br>file://" . $path;
                    } else {
                        if ($f=fopen($path, "r")) {
                            header("Cache-control: private");
                            header("Content-Type: application/pdf");
                            header("Pragma: public");
                            $contents=fread($f, filesize($path));
                            print $contents;
                            fclose($f);
                            exit();
                        }
                    }
                }

                break;
            
            case "scan_check":
                $batch = new Batch($_GET['id'], true);
                
                //Bug 453 requests: "The check should be implemented from May 2014 onwards."
                //Assumed this refered to batches added after this date, not that the actual feature be implemented anytme from May
                $date1 = $batch->addedwhen();
                $date2 = "2014-05-01 07:00";

                if (strtotime($date1) > strtotime($date2)) {
                    //check directory and remove ".", ".." from array
                    $main_dir = scandir(COMM_SCAN_DIR);
                    $main_dir = array_slice($main_dir, 2);
                
                    foreach ($main_dir as $key => $value) {
                        //explode dir name so that we have a batchID range allowing us to see if the current batchID falls within it
                        $dirs = explode("-", $value);
                        
                        if (($dirs[0] <= $_GET['id']) && ($_GET['id'] <= $dirs[1])) {
                            $sub_dir = $dirs[0]."-".$dirs[1];
                        }
                    }
                    
                    $dir = COMM_SCAN_DIR.$sub_dir;
                    
                    if (is_dir($dir)) {
                        $file = file_exists($dir."/".$_GET['id'].".pdf");
                        
                        if (!$file) {
                            // Scan does not exist. show user notification
                            //echo "<div class='notification info static' style='display:block; width:77%'>There is no scanned file matching Batch #".$_GET['id']."</div>";
                        }
                    }
                }
                
                break;

            case "coversheet_form":
                $s = new Search(new User());
                $s->eq("department", 1);
                $s->eq("active", 1);
                $s->eq("weekend_staff", 0);
                $s->nt("id", 62);
                $s->nt("id", 63);

                while ($u = $s->next(MYSQLI_ASSOC)) {
                    $staff .= "<option id='".$u->id()."' name='".$u->id()."'>".ucwords(str_replace(".", " ", $u->username()))."</option>";
                }

                $upload = !empty($_GET['upload']);

                echo new Template("batch/coversheet_form.html", ["batch"=>$batch, "staff"=>$staff, "upload"=>$upload]);

                break;

            case "print_coversheet":
                try{
                    $statement = $_FILES["statement"]["tmp_name"];

                    $data = [
                        "batches" => "(" . $batch->id() . ")",
                        "ac_comm" => $_REQUEST['ac_comm'],
                        "process_method" => $_REQUEST['process_method'],
                        "vat" => $_REQUEST['vat'],
                        "processed_by" => $_REQUEST['processed_by'],
                        "notes" => $_REQUEST['notes'],
                    ];

                    $report_url = JASPER_PROPHET_REPORTS . "batch";

                    $c = new Jaspersoft\Client\Client(
                        JASPER_HOST,
                        JASPER_USER,
                        JASPER_PASSWD,
                        ""
                    );

                    $report = $c->reportService()->runReport($report_url, 'pdf', null, null, $data);

                    $coversheet = COMM_UPLOAD_DIR . $batch->id() . "_temp_coversheet.pdf";
                    $filename = $batch->id() . ".pdf";

                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Description: File Transfer');
                    header('Content-Disposition: inline; filename='.$filename);
                    header('Content-Type: application/pdf');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');

                    file_put_contents($coversheet, $report);

                    $output_name = COMM_UPLOAD_DIR . $filename;

                    $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH  -sOutputFile=$output_name $coversheet $statement";

                    exec($cmd);

                    unlink($coversheet);

                    header('Content-Length: ' . strlen(readfile($output_name)));

                    readfile($output_name);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }

                break;
        }
    } else {
        /* Display profile */
        echo new Template("batch/profile.html", ["batch"=>$batch]);
    }
} else {
    $batch = new Batch;
    
    $s = new Search($batch);
    $s
        ->flag("id", Search::DEFAULT_INT);

    if (isset($_GET["search"])) {
        // Perform search and return Suggest-compatible JSON
 
        if (strlen($_GET["search"]) < 3) {
            die("0");
        }

        // limit batch by issuer, used when adding commission to archived policies
        if (isset($_GET['issuer'])) {
            $s->eq("issuer", $_GET['issuer']);
        }
        $s->set_limit(SEARCH_LIMIT);
        $s->set_offset($_GET["skip"]);

        // Generate a standard response array
        $response_array = $s->get_response_array(
            $_GET["search"]
        );

        // JSONify and return to client
        echo json_encode($response_array);
    } else {
        echo Template("generic/search.html", ["object"=>$batch, "help"=>$s->help_html()]);
    }
}
