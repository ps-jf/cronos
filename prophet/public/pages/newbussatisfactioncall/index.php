<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

switch ($_GET["do"]) {
    case "save":
        if (($_GET['id'])!= null) {
            $db = new mydb();  // create a database connection
            $json = [];
            //Build update "query" using error and "status" JSON variables
            $q = "UPDATE ".NBCALL_TBL."
                  SET user_id=".User::get_default_instance('id').", sd1=".$_GET['newbussatisfactioncall']['sd1'].",
                      sd2=".$_GET['newbussatisfactioncall']['sd2'].", sd3=".$_GET['newbussatisfactioncall']['sd3'].",
                      sd4=".$_GET['newbussatisfactioncall']['sd4'].", sd5=".$_GET['newbussatisfactioncall']['sd5'].",
                      sd6=".$_GET['newbussatisfactioncall']['sd6'].", sd7=".$_GET['newbussatisfactioncall']['sd7'].",
                      ad1=".$_GET['newbussatisfactioncall']['ad1'].",  ad2=".$_GET['newbussatisfactioncall']['ad2'].",
                      ad3=".$_GET['newbussatisfactioncall']['ad3'].", ad4=".$_GET['newbussatisfactioncall']['ad4'].",
                      ad5=".$_GET['newbussatisfactioncall']['ad5'].", ad6=".$_GET['newbussatisfactioncall']['ad6'].",
                      ad7=".$_GET['newbussatisfactioncall']['ad7'].", com1=".$_GET['newbussatisfactioncall']['com1'].",
                      com2=".$_GET['newbussatisfactioncall']['com2'].",com3=".$_GET['newbussatisfactioncall']['com3'].",
                      com4=".$_GET['newbussatisfactioncall']['com4'].", com5=".$_GET['newbussatisfactioncall']['com5'].",
                      com6=".$_GET['newbussatisfactioncall']['com6'].", updated_when=".time().",
                      updatedby=".User::get_default_instance('id').", nbid=".$_GET['nbid'].", comments= '".$_GET['newbussatisfactioncall']['comments']."'
                  WHERE id =".$_GET['id'];

            $db->query($q); // RUN QUERY
            $json["newID"] = $_GET['id'];
            echo json_encode($json);
        } else {
            $db = new mydb();
            $json = [];

            $nbsc = new NewBusSatisfactionCall();

            $nbsc->user_id(User::get_default_instance('id'));
            $nbsc->sd1($_GET['newbussatisfactioncall']['sd1']);
            $nbsc->sd2($_GET['newbussatisfactioncall']['sd2']);
            $nbsc->sd3($_GET['newbussatisfactioncall']['sd3']);
            $nbsc->sd4($_GET['newbussatisfactioncall']['sd4']);
            $nbsc->sd5($_GET['newbussatisfactioncall']['sd5']);
            $nbsc->sd6($_GET['newbussatisfactioncall']['sd6']);
            $nbsc->sd7($_GET['newbussatisfactioncall']['sd7']);
            $nbsc->ad1($_GET['newbussatisfactioncall']['ad1']);
            $nbsc->ad2($_GET['newbussatisfactioncall']['ad2']);
            $nbsc->ad3($_GET['newbussatisfactioncall']['ad3']);
            $nbsc->ad4($_GET['newbussatisfactioncall']['ad4']);
            $nbsc->ad5($_GET['newbussatisfactioncall']['ad5']);
            $nbsc->ad6($_GET['newbussatisfactioncall']['ad6']);
            $nbsc->ad7($_GET['newbussatisfactioncall']['ad7']);
            $nbsc->com1($_GET['newbussatisfactioncall']['com1']);
            $nbsc->com2($_GET['newbussatisfactioncall']['com2']);
            $nbsc->com3($_GET['newbussatisfactioncall']['com3']);
            $nbsc->com4($_GET['newbussatisfactioncall']['com4']);
            $nbsc->com5($_GET['newbussatisfactioncall']['com5']);
            $nbsc->com6($_GET['newbussatisfactioncall']['com6']);
            $nbsc->addedby(User::get_default_instance('id'));
            $nbsc->addedwhen(time());
            $nbsc->nbid($_GET['nbid']);
            $nbsc->comments($_GET['newbussatisfactioncall']['comments']);

            if ($nbsc->save()) {
                $s = new Search(new NewBusSatisfactionCall);
                $s->eq("nbid", $_GET['nbid']);
                $s->add_order("id", "desc");
                $s->set_limit("1");
                if ($d = $s -> next(MYSQLI_ASSOC)) {
                    $newID = $d->id();
                }
            }
            $json["newID"] = $newID;

            echo json_encode($json);
        }
        break;
}
