<?php

require "$_SERVER[DOCUMENT_ROOT]/../bin/_main.php";

if (isset($_GET["search"])) {
    $db = new mydb();

    $q = strtolower($_GET['search']);

    $query = "SELECT * FROM " . DATA_DB . ".tblpolicytype
        WHERE lower(policyTypeDesc) LIKE '%" . $q . "%' or lower(alias) LIKE '%" . $q . "%'
         LIMIT " . SEARCH_LIMIT . " OFFSET " . $_GET['skip'];

    $db->query($query);

    $matches = [];

    while ($match = $db->next(MYSQLI_ASSOC)) {
        // append to matches
        $pt = new PolicyType($match['policyTypeID'], true);
        $matches["results"][] = ['value' => $pt->id(), 'text' => $pt->name()];
    }

    echo json_encode($matches);
}