<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "loadResults":
            // for pretty printing
            function det_class($pt)
            {
                $rt = time();

                if ($pt > $rt) {
                    if ($pt < $rt + 86400) {
                        $class = "amber";
                    } else {
                        $class = "green";
                    }
                } else {
                    $class = "red";
                }
                return $class;
            }

            $json = [
                "error"  => false,
                "status" => null,
                "data"   => null,
                "dueCount" => 0,
                "overdueCount" => 0,
                "amberdueCount" => 0,
                "closedCount" => 0
            ];

        //If a choice hasn't been made, default the selection to all
            $s = new Search(new Complaints());
            if (isset($_GET['cid']) && $_GET['cid'] != 0) {
                $s->eq('clientID', $_GET['cid']);
            }
            if (isset($_GET['statusFilter']) && $_GET['statusFilter'] != 0) {
                $s->eq('Status', $_GET['statusFilter']);
            }

            $s->add_order("complaint_received", Search::ORDER_DESCEND);

            while ($complaint = $s->next(MYSQLI_ASSOC)) {
                if ($complaint->record_closed() == false) {
                    //get the workflow object for each complaint
                    $workflowObj = new Search(new Workflow);
                    $workflowObj->eq("object", "Complaints");
                    $workflowObj->eq("object_index", $complaint->id());
                    if ($workflow = $workflowObj->next()) {
                        $colour = det_class($workflowObj->due());
                        $workflowtd = "<td class=".$colour.">" .  date('d/m/Y H:i:s', $workflow->due()) . "</td>";
                    } else {
                        // fall back for when no workflow ticket has been added
                        $colour = "green";
                        $workflowtd = "<td class=".$colour."> Ongoing - No Workflow</td>";
                    }
                } else {
                    $colour = "blue";
                    $workflowtd = "<td class=".$colour."> Record Closed</td>";
                }

                $json['data'] .= "<tr><td>" . $complaint->client->link() . "</td>
                <td>" . substr($complaint->complaint_received, 0, 10) . "</td>
                    <td>" . $complaint->category . "</td>
                    <td>" . $complaint->partner . "</td>
                    <td>" . $complaint->status . "</td>
                    ".$workflowtd."
                    <td><button id='" . $complaint->id . "' class='open_complaint'>Open</button>";

                $json['data'] .= "</td></tr>";

                if ($colour == 'green') {
                    $json['dueCount'] = $json['dueCount'] + 1;
                } elseif ($colour == 'amber') {
                    $json['amberdueCount'] = $json['amberdueCount'] + 1;
                } elseif ($colour == 'red') {
                    $json['overdueCount'] = $json['overdueCount'] + 1;
                } elseif ($colour == 'blue') {
                    $json['closedCount'] = $json['closedCount'] + 1;
                }
            }

            if ($json['data'] == null) {
                $json['data'] = "<tr><td>There are no complaints.</td></tr>";
            }

            echo json_encode($json);


            break;

        case "deleteComplaint":
            $c = new Complaints($_GET['complaint_id'], true);

            $db = new mydb();
            $q = "DELETE FROM " . COMPLAINTS_TBL . " WHERE id = " . $_GET['complaint_id'];
            if ($db->query($q)) {
                $json['error'] = false;
                $json['status'] = "Complaint Successfully Deleted";
            } else {
                $json['error'] = true;
                $json['status'] = "Entry failed to delete, please contact IT.";
            }

            echo json_encode($json);

            break;

        case "complaints_view":

            if (isset($_GET['complaint_id'])) {
                //Get existing complaint record and load into template
                $complaint = new Complaints($_GET['complaint_id'], true);
                $client = $complaint->client();
            } else if (isset($_GET['cid'])) {
                //Create a new template
                $complaint = new Complaints();
                $client = new Client($_GET['cid'], true);
            }
            $editable = false;
            if (User::get_default_instance('department') == 2 || User::get_default_instance('department') == 7) {
                if ($complaint->record_closed() == 1) {
                    $editable = false;
                } else {
                    $editable = true;
                }
            }

            echo new Template("complaints/complaints_view.html", ["client"=>$client, "complaint"=>$complaint, "editable"=>$editable]);

            break;

        case "check_complaint_workflow":
            $json = [
                "error"  => false,
                "status" => null,
                "data"   => null
            ];

        // outstanding complaints on this policy/client
            $comp = new Search(new Complaints());
            $comp->eq("clientID", $_GET['id']);
            $comp->eq("record_closed", 0);

            while ($c = $comp->next(MYSQLI_ASSOC)) {
                if (isset($c)) {
                    $data = "";
                    if ($c->status() < 59 || $c->status() != 59) {
                        // outstanding complaints on this policy/client
                        $s = new Search(new Workflow());
                        $s->eq("object", "Complaints");
                        $s->eq("object_index", $c->id());
                        $data .= "";
                        while ($x = $s->next(MYSQLI_ASSOC)) {
                            $user = User::get_default_instance();
                            if (($user->department() == 7 || $user->department() == 2)) {
                                $data .= " " . $x->link($x->id()) . " ";
                            } else {
                                $data .= " " . $x->id() . " ";
                            }
                        }
                    }

                    $merge_request = "";
                    if (isset($data)) {
                        $merge_request = "<div class='notification static' style='display:block;'>
                                                This client has workflow open on an ongoing complaint "
                                . $data . "</div>";
                        $json['data'] =  $merge_request;
                    }
                }
            }
            echo json_encode($json);


            break;
    }
} else {
    echo new Template("complaints/index.html");
}
