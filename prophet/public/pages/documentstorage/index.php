<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "../bin/_main.php");

use Aws\S3\S3Client;

// Define our AWS credentials
$bucket = AWS_BUCKET;
$s3StorageKey = AWS_STORAGE_KEY;
$s3Secret = AWS_SECRET;

// Instantiate the S3 client with AWS credentials
$s3Client = S3Client::factory([
    'credentials' => [
        'key'    => $s3StorageKey,
        'secret' => $s3Secret
    ],
    'region'  => 'eu-west-2',
    'version'  => '2006-03-01'
]);


if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        /**
         * our internal document storage functionality
         */
        case "upload":

            $json = [
                "status" => false,
                "error" => false,
                "file_id" => false
            ];

            // assign file information to variable
            $f = $_FILES["file"];
            // get file extension
            $ext = pathinfo($f['name'], PATHINFO_EXTENSION);

            // directory
            $hash = md5_file( $f['tmp_name'] );
            $path = str_split( substr($hash,0,8), 2 );
            $path = implode( "/", $path );

            // full directory path
            $dir = PROPHET_FILE_UPLOAD_DIR."/".$path;

            // confirm that directory does not already exist then proceed to create directory
            if (!file_exists($dir)) {
                if (!mkdir($dir, 0777, true)) {
                    $json['error'] = true;
                    $json['status'] = "Failed to create directory, Please contact IT.";
                    echo json_encode($json);
                    exit();
                }
            }

            // create random file name and concatenate extension
            $unique_name = random_string(15).".".$ext;

            // if display name specified use it - otherwise use filename
            (!empty($_GET["filename"])) or $_GET["filename"] = $f['name'];

            $file = new FileMapping();
            $file->display_name($_GET["filename"]);
            $file->filename($f['name']);
            $file->size($f['size']);
            $file->mime_type($f['type']);
            $file->object($_GET['object']);
            $file->object_id($_GET['object_id']);
            $file->path( "$path/$unique_name" );

            try {
                if (!$file->save()) {
                    $json['error'] = true;
                    $json['status'] = "Could not save file mapping instance, Contact IT.";
                } else {
                    // mapping entry created and configuration variables set, now transfer file into it's new home
                    if (file_exists($f['tmp_name'])) {
                        if (!move_uploaded_file($f['tmp_name'], $dir."/".$unique_name )) {
                            $json['error'] = true;
                            $json['status'] = "Could not move file to new location, Contact IT.";
                        } else {
                            $json['status'] = "Upload successful.";
                            $json['file_id'] = $file->id();
                        }
                    } else {
                        $json['error'] = true;
                        $json['status'] = "Temp file no longer exists, Contact IT.";
                    }
                }
            } catch( Exception $e ) {
                $json['error'] = true;
                $json['status'] = $e->getMessage();
            }

            echo json_encode($json);
            break;

        case "view_file":
            if (isset($_GET['id'])) {
                $file_object = new FileMapping($_GET['id'], true);

                try {
                    $file_object->read_file( );
                } catch( Exception $e ){
                    $json['error'] = true;
                    $json['status'] = $e->getMessage();
                }

                try {
                    // increment view counter
                    $file_object->view_count($file_object->view_count() + 1);
                    $file_object->save();
                } catch( Exception $e ){
                    $json['error'] = true;
                    $json['status'] = $e->getMessage();
                }
            }
            break;

        case "delete_file_local":
            $json = [
                'error' => true,
                'status' => false
            ];

            if (isset($_GET['id'])) {
                // get storage object for file
                $file_object = new FileMapping($_GET['id'], true);

                if ($file_object) {
                    if (file_exists(PROPHET_FILE_UPLOAD_DIR."/".$file_object->path())) {
                        if (unlink(PROPHET_FILE_UPLOAD_DIR."/".$file_object->path())) {
                            $db = new mydb();
                            $q = "DELETE FROM prophet.file_mapping WHERE id = ".$_GET['id'];

                            if ($db->query($q)) {
                                $json['error'] = false;
                                $json['status'] = "File successfully deleted";
                            } else {
                                $json['status'] = "file removed, could not delete database entry";
                            }
                        } else {
                            $json['status'] = "could not unlink file";
                        }
                    } else {
                        $json['status'] = "could not locate file to delete";
                    }
                } else {
                    $json['status'] = "file object not found";
                }
            } else {
                $json['status'] = "No ID given";
            }

            echo json_encode($json);
            break;

        /**
         * our external document storage functionality (AWS S3)
         */

        /**
         * Display all resources relating to particular partner(s) and/or practice
         */
        case "get_files":
            $json = [
                'status'           => false,
                'data'             => null,
                'filecount'        => 0,
                'totalFileSize'    => 0,
                'totalFilePercent' => 0,
            ];

            if (isset($_GET['id'])){
                $totalFileSize = 0;
                $data = "";

                if ($_GET['object_type'] == "Partner") {
                        $s = new Search(new DocumentStorage());
                        $s->eq('partnerID', $_GET['id']);
                        $s->add_order('id', 'DESC');

                        while ($file = $s->next(MYSQLI_ASSOC)) {
                            $class = ($file->servicing_document()) ? "servicing" : '';
                            $class .= " Y-".Carbon\Carbon::createFromTimestamp($file->added_when())->format("Y");
                                // Build up the table rows with file data
                                $data .= "<tr class='".$class."'><td>" . $file->link() . "</td>
                                      <td>" . $file->getObject()->link() . "</td>
                                      <td>" . $file->category . "</td>
                                      <td>" . date("d F Y H:i:s", $file->added_when()) . "</td>
                                      <td>" . $file->servicing_document . "</td>
                                      <td>" . $file->declaration . "</td>
                                      <td>" . $file->review_offer_evidence . "</td>
                                      </tr>";

                                $json['filecount'] = $json['filecount'] + 1;
                                $totalFileSize = $totalFileSize + $file->file_size();
                            }

                        // Some nicey nicey file metrics  --Total amount of files and all file sizes added
                        $json['totalFileSize'] = DocumentStorage::formatBytes($totalFileSize);
                        $json['totalFilePercent'] = round($totalFileSize / 1073741824, 2);
                } else if ($_GET['object_type'] == "Practice") {
                    // builds up array of partners in a practice
                    // Had to do this way as the prophet object search freaks out with loops
                    $s = new Search(new PracticeStaffEntry());
                    $s->eq('practice', $_GET['id']);
                    $s->eq('active', 1);
                    $dump = [];
                    while ($d = $s->next(MYSQLI_ASSOC)) {
                        $dump[] = $d->partner();
                    }

                    //foreach partner in the practice - build up table rows of files
                    foreach ($dump as $du) {
                        $s = new Search(new DocumentStorage());
                        $s->eq('partnerID', $du);
                        $s->add_order('id', 'DESC');

                        while ($file = $s->next(MYSQLI_ASSOC)) {
                            $class = ($file->servicing_document()) ? "servicing" : '';
                            $class .= " Y-".Carbon\Carbon::createFromTimestamp($file->added_when())->format("Y");

                            //Build up the table rows with file data
                            $data .= "<tr class='".$class."'><td>" . $file->link() . "</td>
                                      <td>" . $file->getObject()->link() . "</td>
                                      <td>" . $file->category . "</td>
                                      <td>" . date("d F Y H:i:s", $file->added_when()) . "</td>
                                      <td>" . $file->servicing_document . "</td>
                                      <td>" . $file->declaration . "</td>
                                      <td>" . $file->review_offer_evidence . "</td>
                                      </tr>";

                            $json['filecount'] = $json['filecount'] + 1;
                            $totalFileSize = $totalFileSize + $file->file_size();
                        }

                        //Some nicey nicey file metrics  --Total amount of files and all file sizes added
                        $json['totalFileSize'] = DocumentStorage::formatBytes($totalFileSize);
                        $json['totalFilePercent'] = round($totalFileSize / 1073741824, 2);
                    }
                } elseif ($_GET['object_type'] == "Client") {
                    $db = new mydb(REMOTE);

                    $db->query("SELECT * FROM myps.document_storage WHERE object = 'Client' and object_id = " . $_GET['id'] . "
                                UNION ALL
                                SELECT document_storage.* 
                                FROM myps.document_storage 
                                INNER JOIN myps.ongoing_service ON document_storage.object_id = ongoing_service.id
                                WHERE object = 'OngoingService'
                                AND client_id = " . $_GET['id'] . "
                                UNION ALL
                                SELECT document_storage.*
                                FROM myps.document_storage
                                INNER JOIN myps.review_contact_attempts ON document_storage.object_id = review_contact_attempts.id
                                INNER JOIN myps.ongoing_service ON review_contact_attempts.os_id = ongoing_service.id
                                WHERE object = 'ReviewAttempt'
                                AND client_id = " . $_GET['id'] ."
                                order by id desc"
                    );

                    while ($file = $db->next(MYSQLI_ASSOC)) {
                        $file = new DocumentStorage($file['id'], true);
                        $class = ($file->servicing_document()) ? "servicing" : '';
                            $class .= " Y-".Carbon\Carbon::createFromTimestamp($file->added_when())->format("Y");

                        //Build up the table rows with file data
                        $data .= "<tr class='".$class."'><td>" . $file->link() . "</td>
                                      <td>" . $file->getObject()->link() . "</td>
                                      <td>" . $file->category . "</td>
                                      <td>" . date("d F Y H:i:s", $file->added_when()) . "</td>
                                      <td>" . $file->servicing_document . "</td>
                                      <td>" . $file->declaration . "</td>
                                      <td>" . $file->review_offer_evidence . "</td>
                                      </tr>";

                        $json['filecount'] = $json['filecount'] + 1;
                        $totalFileSize = $totalFileSize + $file->file_size();
                    }

                } else {
                    // this case should fit for anything else, stored at specific levels and not below
                    $s = new Search(new DocumentStorage());
                    $s->eq("object", $_GET['object_type']);
                    $s->eq("object_id", $_GET['id']);
                    $s->add_order('id', 'DESC');

                    while ($file = $s->next(MYSQLI_ASSOC)) {
                        $class = ($file->servicing_document()) ? "servicing" : '';
                            $class .= " Y-".Carbon\Carbon::createFromTimestamp($file->added_when())->format("Y");
                        
                        //Build up the table rows with file data
                        $data .= "<tr class='".$class."'><td>" . $file->link() . "</td>
                                      <td>" . $file->getObject()->link() . "</td>
                                      <td>" . $file->category . "</td>
                                      <td>" . date("d F Y H:i:s", $file->added_when()) . "</td>
                                      <td>" . $file->servicing_document . "</td>
                                      <td>" . $file->declaration . "</td>
                                      <td>" . $file->review_offer_evidence . "</td>
                                      </tr>";

                        $json['filecount'] = $json['filecount'] + 1;
                        $totalFileSize = $totalFileSize + $file->file_size();
                    }

                    //Some nicey nicey file metrics  --Total amount of files and all file sizes added
                    $json['totalFileSize'] = DocumentStorage::formatBytes($totalFileSize);
                    $json['totalFilePercent'] = round($totalFileSize / 1073741824, 2);
                }
            }
            $json['data'] = $data;

            echo json_encode($json);
            break;

        /**
         * Display all resources relating to particular partner.
         */
        case "browse_partner":
            if (isset($_GET['id'])) {
                // ge the object we are trying to upload a file against
                $obj = new Partner($_GET['object_id'], true);

                if ($obj) {
                    $partnerID = $obj->id();
                    $practiceID = $obj->get_practice()->id();

                    if (isset($partnerID) && isset($practiceID)) {
                        // build our S3 directory prefix
                        $prefix = $practiceID . "/" . $partnerID;

                        if (isset($prefix)) {
                            $iterator = $s3Client->getIterator('ListObjects', [
                                'Bucket' => $bucket,
                                'Prefix' => $prefix
                            ]);

                            // build a list of all items stored on S3 for our required object
                            foreach ($iterator as $object) {
                                echo $object['Key'] . "\n";
                            }
                        } else {
                            dd("cannot access S3 directory");
                        }
                    } else {
                        dd("could not get prefix for directory to iterate through");
                    }
                } else {
                    dd("could not find partner");
                }
            } else {
                dd("ensure partnerID have been passed through");
            }
            break;

        /**
         * Display all resources relating to particular partner.
         */
        case "browse_object":
            // similar to above case but do a look up to the base for only bring back file at object level #todo
            break;

        /**
         * Display (Download) the specified resource.
         */
        case "show":
            // See: https://docs.aws.amazon.com/aws-sdk-php/v3/guide/service/s3-presigned-url.html

            $json = [
                'status' => false,
                'url' => null
            ];

            $s = new Search(new DocumentStorage());
            $s->eq('id', $_GET['id']);
            if ($file = $s->next(MYSQLI_ASSOC)) {
                $path = $file->filepath();
            }

            if (isset($path)) {
                $cmd = $s3Client->getCommand('GetObject', [
                    'Bucket' => $bucket,
                    'Key'    => $path
                ]);

                $request = $s3Client->createPresignedRequest($cmd, '+20 minutes');

                // Get the actual presigned-url
                $presignedUrl = (string) $request->getUri();


                if ($presignedUrl) {
                    $json['url'] = $presignedUrl;
                    $json['status'] = true;
                }
            }

            echo json_encode($json);

            break;

        /**
         * Show the form for editing the specified resource.
         */
        case "edit":
            break;

        /**
         * Update the specified resource in storage.
         */
        case "update":
            break;

        /**
         * Show the form for creating a new resource.
         */
        case "create":
            // get object variables and use D&D functionality for uploads
            echo new Template("documentstorage/add.html", [
                "object"    => $_GET['object'],
                "object_id" => $_GET['object_id']
            ]);
            break;

        /**
         * Store a newly created resource in storage. (S3)
         */
        case "store":

            $json = [
                'status'   => false,
                'feedback' => '',
            ];

            // check a file has been selected for uploading
            //if (isset($_GET['files_array']) || isset($_FILES)) {
            if (isset($_FILES)) {
                //(isset($_GET['files_array'])) ? $files_array = $_GET['files_array'] : $files_array = $_FILES;

                if (isset($_GET['object_id']) && isset($_GET['object'])) {
                    // ge the object we are trying to upload a file against
                    $obj = new $_GET['object']($_GET['object_id'], true);
                    $object_type = get_class($obj);

                    // get partner ID based on current object
                    if ($object_type == "Client") {
                        $partnerID = $obj->partner->id();
                        $practiceID = $obj->partner->get_practice()->id();
                    } elseif ($object_type == "Policy") {
                        $partnerID = $obj->client->partner->id();
                        $practiceID = $obj->client->partner->get_practice()->id();
                    } elseif ($object_type == "Partner") {
                        $partnerID = $obj->id();
                        $practiceID = $obj->get_practice()->id();
                    } elseif ($object_type == "ActionRequiredMessage") {
                        //This checks whether the action required message is for either a partner or client
                        if ($obj->action_required_id->object() == "Partner") {
                            $partner = new Partner($obj->action_required_id->object_id());
                            $partnerID = $partner->id();
                            $practiceID = $partner->get_practice()->id();
                        } elseif ($obj->action_required_id->object() == "Client") {
                            $client = new Client($obj->action_required_id->object_id(), true);
                            $partner = $client->partner;
                            $partnerID = $partner->id;
                            $practiceID = $partner->get_practice()->id();
                        } elseif ($obj->action_required_id->object() == "Policy") {
                            $policy = new Policy($obj->action_required_id->object_id(), true);
                            $partner = $policy->client->partner;
                            $partnerID = $partner->id;
                            $practiceID = $partner->get_practice()->id();
                        }
                    }
                } else {
                    dd("ensure object & object_id have been passed through");
                }
            }

            if (isset($partnerID) && isset($practiceID)) {
                foreach ($_FILES as $key => $file) {
                    $hash = md5(file_get_contents($file['tmp_name']));
                    $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

                    $unique_name = $hash.".".$fileExtension;
                    $directory = $practiceID . "/" . $partnerID . "/" . $unique_name;

                    $result = $s3Client->putObject([
                        'Bucket' => $bucket,
                        'Key'    => $directory,
                        'Body'   => file_get_contents($file['tmp_name']),
                        'ServerSideEncryption' => 'AES256'
                    ]);

                    if ($result['@metadata']['statusCode'] == '200') {
                        // file successfully uploaded, add entry to database
                            $ds = new DocumentStorage();

                            $ds->partnerID($partnerID);
                            $ds->object_id($_GET['object_id']);
                            $ds->object($_GET['object']);
                            $ds->filepath($directory);
                            $ds->original_name($file['name']);
                            $ds->friendly_name($file['name']);
                            $ds->hash($unique_name);
                            $ds->mime_type($fileExtension);
                            $ds->file_size($file['size']);
                    }

                    if ($ds->save()) {
                        // file uploaded and entry in DB, all good!
                        $json['status'] = true;
                        $json['feedback'] = "File successfully uploaded.";
                    } else {
                        // file failed to add to DB, inform user
                        $json['status'] = false;
                        $json['feedback'] = "File not added to database.";
                    }
                }
            } else {
                // file failed to upload, inform user
                // file failed to add to DB, inform user
                $json['status'] = false;
                $json['feedback'] = "File failed to upload to S3.";
            }

             echo json_encode($json);

            break;

        /**
         * Remove the specified resource from storage.
         */
        case "destroy":
            $json = [
                'status' => false,
                'feedback' => '',
            ];

            if (isset($_GET['id'])) {
                // get storage object for file
                $ds = new DocumentStorage($_GET['id'], true);

                if ($ds) {
                    $path = $ds->filepath();
                    if (isset($path)) {
                        $response = $s3Client->doesObjectExist($bucket, $ds->filepath());
                        if ($response) {
                            // file exists on s3
                            $delete = $s3Client->deleteObject([
                                'Bucket' => $bucket,
                                'Key'    => $ds->filepath()
                            ]);

                            if ($delete['@metadata']['statusCode'] == '200' || $delete['@metadata']['statusCode'] == '204') {
                                    // delete doesn't return feedback in this branch, will when merged with mandate re-write
                                    $ds->delete();
                                    // file removed from S3 & deleted from DB
                                    $json['status'] = true;
                                    $json['feedback'] = "File successfully deleted.";
                            } else {
                                dd("could not delete from S3");
                            }

                        } else {
                            dd("file does not exist on s3");
                        }

                    } else {
                        dd("no path");
                    }

                } else {
                    dd("could not locate object in database");
                }
            } else {
                dd("no file ID passed through");
            }

            echo json_encode($json);

            break;
    }
}
