<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "wipe":
            if ($fh = fopen(ERROR_LOG, 'w')) {
                fclose($fh);
            }

            break;
    }
}

// generate <tbody> html for the User table

$t = new Template("admin/error_row.html");

$errors = [];
if ($fh = fopen(ERROR_LOG, 'r')) {
    $i = 0;
    while (($row = fgetcsv($fh)) !== false && $i < 25) {
        $t->tag($row, "row");
        $t->tag(new User($row[4], true), "user");
        $errors[] = "$t";
        ++$i;
    }
    fclose($fh);
}

$errors = array_reverse($errors);
print implode("\n", $errors);
