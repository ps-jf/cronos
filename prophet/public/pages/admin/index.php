<?php

require $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";

error_reporting(E_ALL);

if (isset($_GET["section"])) {
} else {
    if(isset($_GET['do'])){
        switch($_GET['do']){
            case "view_permissions":

                $type = $_GET['type'];
                $object = new $type($_GET['id'], true);

                echo new Template("admin/permissions.html", compact([
                    "object"
                ]));

                break;

            case "save_permissions":
                $response = [
                    "status" => true,
                    "id" => null,
                    "data" => null,
                    "error" => null
                ];

                try{
                    $type = $_POST['object_type'];

                    if($type === "Groups"){
                        $type = "Group";
                    }

                    foreach($_POST['permissions'] as $pID){
                        $s = new Search(new ObjectPermission());

                        $s->eq("object_type", $type);
                        $s->eq("object_id", $_POST['object_id']);
                        $s->eq("permission", $pID);

                        if(!$s->next(MYSQLI_ASSOC)){
                            // this object does not currently have this permission, add it
                            $op = new ObjectPermission();
                            $op->object_type($type);
                            $op->object_id($_POST['object_id']);
                            $op->permission($pID);

                            $op->save();
                        }
                    }

                    // get any permissions which werent in the list and delete them for this object
                    $s = new Search(new ObjectPermission());
                    $s->eq("object_type", $type);
                    $s->eq("object_id", $_POST['object_id']);
                    $s->nt("permission", $_POST['permissions']);

                    while($op = $s->next(MYSQLI_ASSOC)){
                        $op->delete();
                    }

                } catch (Exception $e) {
                    $response['status'] = false;
                    $response['error'] = $e->getMessage();
                }

                echo json_encode($response);

                break;
        }
    } else {
        // display administration front-page
        echo Template("admin/home.html");
    }
}
