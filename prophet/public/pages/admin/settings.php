<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

define("MINIMUM_PASSWORD_LENGTH", 6);


function get_myps_account($id, $db = false)
{

    # # #   Get myps user account where user id = $id

    if (!$db) {
        // establish database connection to website
        $db = new mydb(REMOTE, E_USER_WARNING);
        if (mysqli_connect_errno()) {
            echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
            exit();
        }
    }

    if ($db -> connection) {
        if ($db -> query("SELECT * FROM ". REMOTE_USR_TBL ." WHERE id = ".(int)"$id")) {
            return $db -> next(MYSQLI_OBJECT);
        }
    }
    
    return false;
}

function validate_password($password)
{
    $why = false;

    while (!$why) {
        // --------------------------------
        // 1. Less than 6 characters long
        // --------------------------------
        if (strlen($password) < MINIMUM_PASSWORD_LENGTH) {
            $why = "Less than ".MINIMUM_PASSWORD_LENGTH." characters in length";
            break;
        }


        // -----------------------------
        // 2. occurs in forbidden list
        // -----------------------------

        // load password xml file into object
        foreach (file("forbidden_pwd.xml") as $line) {
            $xml .= $line;
        }
        $xml = new SimpleXMLElement($xml);

        // loop through each password pattern
        foreach ($xml as $p) {
            if (property_exists($p, 'pattern')) {
                $pwd = $password;

                // replace all tags
                $pattern = preg_match_all('/(@\{.*?\})/', $p, $match, PREG_SET_ORDER);
                for ($i = 0; $i < count($match); $i++) {
                    $pwd = strreplace($match[$i], $pwd);
                }

                if (preg_match($p -> pattern, $_POST["pwd"])) {
                    $why = (string)$p -> description;
                    break;
                }
            } elseif (preg_match('/'.$password.'/i', $p)) {
                $why = "Common word/phrase";
                break;
            }
        }

        // ----------------------------------
        // 3. composed of sequential digits
        // ----------------------------------
        if (preg_match('/^\d+$/', $password)) {
            $is_sequential = true;

            // iterate over each digit in password
            for ($i = 0; $i < strlen($password); $i++) {
                // compare this digit with last
                if (isset($last_dig)) {
                    if ($password[$i] != ( $last_dig + 1 )) {
                        $is_sequential = false;
                        break;
                    }
                }

                $last_dig = $password[$i];
            }

            if ($is_sequential) {
                $why = "String of sequential digits i.e. '1234...'";
                break;
            }
        }

        break;
    }

    if ($why !== false) {
        return [
            'valid'  => false,
            'reason' => $why
        ];
    } else {
        return ['valid' => true];
    }
}


if (isset($_GET["sek"])) {
    switch ($_GET["sek"]) {
        case "myps":
            /* Procedures relating to website interaction */

            $db = new mydb(REMOTE, E_USER_WARNING); // establish remote db conn
            if (mysqli_connect_errno()) {
                echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                exit();
            }

            if ($db -> connection) {
                $myps_user = new WebsiteUser($_USER->myps_user_id);

                switch ($_GET["do"]) {
                    case "setAccount":
                        if (isset($_POST["matching-users"])) {
                            $_USER -> myps_user_id -> set((int)$_POST["matching-users"]);
                            echo json_encode(['successful'=> $_USER -> save()]);
                        } elseif (isset($_GET["matches"])) {
                            if ($_GET["matches"] == "*") {
                                /* wildcard, perform auto search on Prophet user */
                                $where = "email = \"". $_USER -> email ."\" ".
                                         "OR user_name = \"". $_USER -> username ."\"";
                            } elseif (preg_match('/^[A-Za-z][A-Za-z0-9_.]*@[A-Za-z\.]+\.[A-Za-z]+$/', $_GET["matches"])) {
                                /* search on email address */
                                $where = "email = \"". $_GET["matches"] ."\"";
                            } elseif (preg_match('/^[-0-9A-Za-z_]+\.[-0-9A-Za-z_]+$/', $_GET["matches"])) {
                                /* basic forename.surname format */
                                $where = "user_name = \"". $_GET["matches"] ."\"";
                            } else {
                                die(json_encode(false));
                            }

                            $query = "SELECT id, user_name ".
                                     "FROM ". REMOTE_USR_TBL ." ".
                                     "WHERE $where";


                            $db -> query($query);

                            while ($mu[] = $db -> next(MYSQLI_OBJECT)) {
/*pass*/
                            }
                            array_pop($mu);
                            
                            echo json_encode($mu);
                        } else {
                            echo new Template("admin/myps_user.html");
                        }

                        break;


                    default:
                        echo json_encode(["name"=>"$myps_user->user_name"]);
                        break;
                }
            }

            break;


        case "pwd":
            if (isset($_POST["newPwd"], $_POST["confirmPwd"], $_POST["oldPwd"])) {
                // --- Save new user password --

                $x = validate_password($_POST["newPwd"]);

                // validate password + check 'New' compliments 'Confirm'
                if ($x['valid'] && ( $_POST['newPwd'] == $_POST['confirmPwd'] )) {
                    $db = new mydb();

                    $query = "UPDATE ".USR_TBL." ".
                             "SET passwd = PASSWORD('".$_POST['newPwd']."') ".
                             "WHERE passwd = PASSWORD('".$_POST['oldPwd']."') ".
                             "AND id = ". $_USER ->id;

                    $r = $db -> query($query);
                    if ($r) {
                        $r = ( mysqli_affected_rows($db -> connection) > 0 );
                    }
            
                    echo json_encode($r);
                } else {
                    echo "false";
                }
            } elseif (isset($_POST["check"], $_POST["pwd"])) {
                // --- check password integrity --------
            
                switch ($_POST["check"]) {
                    case "oldPwd":
                        // --- ensure that old password is correct

                        $db = new mydb();

                        $db -> query("SELECT true FROM ". USR_TBL ." ".
                                      "WHERE id = ". $_USER ->id ." ".
                                      "AND passwd = PASSWORD('".$_POST['pwd']."')");

                        echo json_encode([
                            'correct_password' => ($db->next(MYSQLI_NUM) !== false)
                        ]);

                        break;

                    case "newPwd":
                        // ensure new password is valid, give reason why if not
                        echo json_encode(validate_password($_POST["pwd"]));
                        break;
                }
            }

            break;

        case "activity":
            // -----------------------------------------------------------
            // Load a historical summary of what this user has been upto
            // -----------------------------------------------------------

            define("PER_PAGE", 25);

            // ------ deal with filtering parameters ------
            if (isset($_POST["a-objects"])) {
                $conditions = [];

                // ------------ Start date / end date / date range -------------- //
                if (!empty($_POST["a-from"])) {
                    $start = Date::toUnix($_POST["a-from"]);
                }
                if (!empty($_POST["a-until"])) {
                    $stop   = Date::toUnix($_POST["a-until"]);
                }

                // form condition based on above declarations

                # Date range #
                if (isset($start, $stop)) {
                    // ensure stop time falls after start, or swap
                    if ($start > $stop) {
                        list( $stop, $start ) = [$start, $stop];
                    }

                    $conditions[ ] = "datetime BETWEEN $start AND $stop";
                } # Start date #
                elseif (isset($start)) {
                    $conditions[ ] = "datetime >= $start";
                } # End date #
                elseif (isset($stop)) {
                    $conditions[ ] = "datetime <= $stop";
                }


                // --------- Returning specified object types only ------------- //
                foreach ($_POST["a-objects"] as $obj) {
                    $objects[ ] = "object = '$obj'";
                }

                if (count($objects) > 0) {
                    $conditions[ ] = "( ". implode(" OR ", $objects) . " )";
                }

                $conditions = "AND ".implode(" AND ", $conditions) . " ";
            } else {
                $conditions = false;
            }

            // ------------ Handle page number offset if necessary -------------- //
            $offset = false;
            if (isset($_POST["page-number"]) && $_POST["page-number"] != 1) {
                $n = (int)$_POST["page-number"];
                $offset = ( --$n * PER_PAGE ) . ", " . PER_PAGE;
            }


            $db = new mydb();

            $query = "SELECT datetime, action_type, object, object_id ".
                     "FROM ". ACTIONS_TBL ." ".
                     "WHERE user_id = ". $_USER ->id ." ".
                     $conditions .
                     "ORDER BY datetime DESC " .
                     "LIMIT ". ( ( $offset ) ? $offset : PER_PAGE );

            $db -> query($query);

            $t = new Template('misc/recent-activity-row.html');
            $t -> tag(true, 'ignore_user');
            
            while ($d = $db -> next(MYSQLI_OBJECT)) {
                eval("\$obj = new ". $d -> object ."( ". $d -> object_id .", true );");
                $obj -> _is_deleted = ( $d -> action_type == "delete" );

                $t -> tag($obj, 'object');
                $t -> tag($d, 'd');
                $results .= $t;
            }

            if (!$offset) {
                // ---------- get number of pages required ------------ //

                // amend query to return number of available results
                $query = preg_replace('/^.*? FROM/', 'SELECT COUNT(id) FROM', $query);

                $db -> query($query);
                $d = $db -> next(MYSQLI_NUM);
                $count = $d[0];

                // round up if number of records is not
                // divisible by max records per page
                $pages = $count / PER_PAGE;
                if (is_float($pages)) {
                    $pages = ceil($pages);
                }
            }

            echo json_encode([
                'results' => $results,
                'pages' => $pages
            ]);

            break;
    }
} else {
    $template = new Template("admin/index.html");
    $template -> tag($_USER, 'user');
    echo $template;
}
