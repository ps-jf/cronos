<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
} else if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "new":
            if (isset($_POST["user"])) {
                print_r($_POST["user"]);
            } else {
                echo Template("admin/user_form.html");
            }

            break;
    }
} else {
    // generate <tbody> html for the User table

    $t = new Template("admin/user_row.html");
    $html = "";

    $s = new Search(new User);
    $s->add_order("username");


    while ($user = $s->next()) {
        $t->tag($user, "user");
        $html .= "$t";
    }

    echo $html;
}
