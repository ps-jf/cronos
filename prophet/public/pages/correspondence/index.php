<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

// function to recursively remove files and folders for temp files
function removeDirectory($path)
{
    $files = glob($path . '/*');
    foreach ($files as $file) {
        is_dir($file) ? removeDirectory($file) : unlink($file);
    }
    rmdir($path);
}

function removePRFiles($path)
{
    $files = glob($path . '/*');
    foreach ($files as $file) {

        if (is_file($file) && strpos($file, 'PR_') !== false) {
            unlink($file);
        }
    }
}

function set_headers($report, $file_name)
{
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename=' . $file_name . '.pdf');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Content-Length: ' . strlen($report));
    header('Content-Type: application/pdf');
}

function jasper_generate($uri, $format = 'pdf', $pages = null, $attachmentsPrefix = null, $inputControls = null, $file_name = false)
{
    try {
        $c = new \Jaspersoft\Client\Client(
            JASPER_HOST,
            JASPER_USER,
            JASPER_PASSWD,
            ""
        );

        $report = $c->reportService()->runReport($uri, $format, $pages, $attachmentsPrefix, $inputControls);

        set_headers($report, $file_name);

        return $report;

    } catch (Exception $e) {
        $json['error'] = true;
        $json['status'] = "Caught exception: " . $e->getMessage();
        echo json_encode($json);
        exit();
    }
}

function create_file($file_path, $file_name, $file)
{
    $page = $file_path . $file_name;
    file_put_contents($page, $file);

    // check that temporary coversheet has been created, if so continue..
    if (!file_exists($page)) {
        $json['error'] = true;
        $json['status'] = "An error has occurred creating " . $file_name . ", please contact IT.";
        echo json_encode($json);
        exit();
    }
}

if (isset($_GET["id"])) {
    /*** Actions etc. relating to item ***/

    $c = new Correspondence(( int ) $_GET[ "id" ], true);

    switch ($_GET["do"]) {
        case "print_pdf":
            $cover_only = false;
            $file = $_GET['url'];
            $filename = $_GET['id'] . ".pdf";

            $corr = new Correspondence($_GET['id'], true);

            if (in_array($corr->letter(), [33, 30,31])) {
                $dir = dirname($_GET['url'])."/";
                $output = "output.pdf";
                // let ghost script works its magic
                exec("gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=".$dir.$output." ".$file);
                $file = $dir.$output;
                $cover_only = true;
            }

            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Description: File Transfer');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Content-Type: application/pdf');
            header('Accept-Ranges: bytes');
            header('Content-Length: ' . filesize($file));
            header('Content-Disposition: inline; filename="' . $filename . '"');
            @readfile($file);

            echo $file;

            if ($cover_only) {
                unlink($file);
            }

            break;

        case "print_coversheet":
            // letter = coversheet
            $letter_obj = new Letter(6, true);

            $c = new \Jaspersoft\Client\Client(
                JASPER_HOST,
                JASPER_USER,
                JASPER_PASSWD,
                ""
            );

            $arr = [];
            $arr['ITEM_ID'] = $_GET['id'];


            // create multidimensional array for passing values through in expected format
            $form_values = [];
            foreach ($arr as $key => $value) {
                $form_values[$key][] = $value;
            }

            // get constant values for standard letter footer
            $std_letter_foot = unserialize(STANDARD_LETTER_FOOTER);
            foreach ($std_letter_foot as $key => $value) {
                $form_values[$key][] = $value;
            }

            // point to prophet_reports dir on rosie
            $report_url = JASPER_STANDARD_LETTERS . $letter_obj->rosie_file();

            // check if we have any form data to pass through
            if (!empty($form_values)) {
                try {
                    $report = $c->reportService()->runReport($report_url, 'pdf', null, null, $form_values);
                } catch (Exception $e) {
                    exit('Caught exception: ' . $e->getMessage() . "\n");
                }
            } else {
                exit("Report parameters missing, please re-run report.");
            }

            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Description: File Transfer');
            header('Content-Disposition: inline; filename=' . $letter_obj->rosie_file() . '.pdf');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Content-Length: ' . strlen($report));
            header('Content-Type: application/pdf');

            echo $report;

            break;

        case "corr_milestone":
            $user = new User(User::get_default_instance('id'), true);
            echo new Template("correspondence/corr_milestone.html", ["count" => $_GET['count'], "user" => $user]);
            break;

        case "view":
            $path = $c->get_path(false, false);

            if (file_exists($path) && $f = fopen($path, "r")) {
                header("Cache-control: private");
                header("Content-Type: application/pdf");
                header("Pragma: public");
                $contents = fread($f, filesize($path));
                print $contents;
                fclose($f);
                exit();
            }

            break;

        case "delete":
            $return = [
                "id" => false,
                "status" => false,
                "error" => false
            ];

            try {
                $corr_json = addslashes(json_encode($c->flat_array()));
                //$corr_json = addslashes(serialize($c->flat_array()));

                $id = $_GET['id'];

                $db = new mydb();
                $log_actions_q = "INSERT INTO " . ACTIONS_TBL . " (datetime, user_id, type, object, object_id, snapshot)
                 VALUES (" . time() . ", " . User::get_default_instance('id') . ", 'DELETE', 'Correspondence', " . $id . ", '".$corr_json."')";

                if (!$db->query($log_actions_q)) {
                    $return['error'] = true;
                    echo json_encode($return);
                }

                $db2 = new mydb();
                $delete_q = "DELETE FROM " . CORR_TBL . " WHERE corrid = " . $id;
                if (!$db2->query($delete_q)) {     $return['error'] = true;
                    echo json_encode($return);
                }

                $ca = new CorrespondenceAudit();
                $ca->corr_id($id);

                if (isset($_GET['spotcheck']) && $_GET['spotcheck']){
                    $ca->accepted(0);
                    $ca->spot_checked(1);
                }

                $ca->predicted($c->ppp_predicted());
                $ca->save();

                //check if we should delete the file too
                if (isset($_GET['delete_file']) && $_GET['delete_file']){
                    $filepath = $_GET['filepath'];
                    $filename = $_GET['filename'];

                    if (file_exists($filepath)){
                        if (rename($filepath, CORRESPONDENCE_DECLINED_DIR . $filename)){
                            $return['status'] = true;
                        } else {
                            $return['error'] = true;
                        }
                    } else {
                        $return['error'] = true;
                    }

                    echo json_encode($return);
                } else {
                    $return['status'] = true;
                    echo json_encode($return);
                }
            } catch (Exception $e) {
                $return['error'] = true;
                die($e->getMessage());
            }
            return;

            break;

        case "delete_proreport":

            $path = realpath(PROREPORT_PROCESSED_SCAN_DIR);
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path)) as $filename) {
                if (strpos($filename, $_GET['id'] . ".pdf") !== false) {
                    if (!unlink($filename)) {
                        $return['error'] = "Error deleting file from mailing house folder, contact IT, : " . $_GET['id'] . ".pdf";
                        $return['status'] = false;
                    } else {
                        $return['error'] = false;
                        $return['status'] = "Correspondence Item successfully deleted.";
                    }
                } else {
                    $return['error'] = false;
                    $return['status'] = "No pdf to remove from mailing house folder.";
                }
            }

            $s = new Search(new ProreportInvoice());
            $s->eq("corr", $_GET['id']);
            if (($invoice = $s->next()) !== false) {
                $invoice->delete();
            }

            echo json_encode($return);

            return;
            break;

        case "log":
            try {
                $id = $_GET['id'];

                $db = new mydb(REMOTE, E_USER_WARNING);

                if (mysqli_connect_errno()) {
                    throw new Exception("Can't connect to mypsaccount server: " . mysqli_connect_errno());
                }

                $d = $db->query("select from_unixtime(datime, '%d/%m/%Y %H:%i:%s') as time, user_name, email 
                      from " . WEB_CORRESPONDENCE_AUDIT_TBL .
                    " inner join " . REMOTE_USR_TBL .
                    " on " . WEB_CORRESPONDENCE_AUDIT_TBL . ".user = " . REMOTE_USR_TBL . ".id " .
                    "where item_id = " . $id .
                    " order by time desc");

                while ($d = $db->next(MYSQLI_ASSOC)) {
                    ($d['email'] && $d['email'] != "") ? $audit_by = $d['email'] : $audit_by = $d['user_name'];
                    echo "<tr><td>" . $d['time'] . "</td><td>" . $audit_by . "</td></tr>";
                }

                echo "No actions have been logged for this item of correspondence";
            } catch (Exception $e) {
                die($e->getMessage());
            }
            return;
            break;

        case "spot_check":
            $filename = $_GET['id'] . ".pdf";
            $filepath = CORRESPONDENCE_HOLDING_DIR . $_GET['queue'] . "/" . $filename;

            echo new Template("correspondence/spot_check.html", [
                "filepath" => $filepath,
                "filename" => $filename,
                "correspondence" => $c,
                "client" => $client
            ]);

            break;

        case "pass_spot_check":
            $return = [
                "id" => false,
                "status" => false,
                "error" => false
            ];

            try{
                $id = $_GET['id'];

                $ca = new CorrespondenceAudit();
                $ca->corr_id($id);
                $ca->accepted(1);
                $ca->spot_checked(1);
                $ca->save();

                $return["status"] = true;

            } catch (Exception $e) {
                $return["error"] = true;
                die($e->getMessage());
            }

            echo json_encode($return);

            break;

    }
} elseif (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "home":
            $corr_admin = User::get_default_instance()->has_permission(44); // Correspondence Admin
            $queue_select = Correspondence::get_frontend_queues();
            $spot_check_queue_select = Correspondence::get_spotcheck_queues();

            echo new Template("correspondence/home.html", [
                "corr_admin" => $corr_admin,
                "queue_select" => $queue_select,
                "spot_check_queue_select" => $spot_check_queue_select
            ]);
            break;

        case "corr_count":
            // count correspondence items in FES queue
            $json = [
                "corr_count" => false,
                "spot_check_count" => 0
            ];

            if ($_GET['dir'] != 0) {
                $dir = opendir(CORRESPONDENCE_PENDING_SCAN_DIR . "/" . $_GET['dir']);

                $files = [];
                while ($file = readdir($dir)) {
                    // We are only interested in the PDF files in the directory
                    if (strpos($file, '.pdf')) {
                        $files[] = $file;
                    }
                }
                $json['corr_count'] = count($files);
            }

            if ($_GET['dir'] != 0 && is_dir(CORRESPONDENCE_HOLDING_DIR . $_GET['dir'])) {
                //count items in spot check queue
                $scDir = opendir(CORRESPONDENCE_HOLDING_DIR . $_GET['dir']);

                while ($file = readdir($scDir)) {
                    if (strpos($file, ".pdf")) {
                        $json['spot_check_count']++;
                    }
                }
            }

            echo json_encode($json);

            break;

        case "spot_check_corr_count":
            // count correspondence items in FES queue
            $json = [
                "corr_count" => false
            ];

            if ($_GET['dir'] != 0) {
                $dir = opendir(CORRESPONDENCE_HOLDING_DIR . "/" . $_GET['dir']);

                $files = [];
                while ($file = readdir($dir)) {
                    // We are only interested in the PDF files in the directory
                    if (strpos($file, '.pdf')) {
                        $files[] = $file;
                    }
                }
                $json['corr_count'] = count($files);
            }

            echo json_encode($json);

            break;

        case "list":

            if ($_GET['queue'] != 0) {
                $dir = opendir(CORRESPONDENCE_PENDING_SCAN_DIR . "/" . $_GET['queue']);

                $corr_admin = User::get_default_instance()->has_permission(44); // Correspondence Admin

                if ($_GET['queue'] == 96 || $_GET['queue'] == 98 || $_GET['queue'] == 99) {
                    $list_row = Correspondence::get_template("row_flagged.html", ["corr_admin" => $corr_admin]);
                } else {
                    $list_row = Correspondence::get_template("row.html", ["corr_admin" => $corr_admin]);
                }

                $files = [];
                while ($file = readdir($dir)) {
                    // We are only interested in the PDF files in the directory
                    if (strpos($file, '.pdf')) {
                        $files[] = $file;
                    }
                }
                // sort array in a natural order, alphanumerical
                natsort($files);

                echo $list_row(["queue" => $_GET['queue'], "correspondence" => $files]);
            } else {
                //get count of all correspondence per directory
                $main_dir = scandir(CORRESPONDENCE_PENDING_SCAN_DIR);
                $main_dir = array_slice($main_dir, 2);

                $dir_array = [];
                foreach ($main_dir as $key => $dir) {
                    if (strlen($dir) < 4) {
                        $dir_array[] = $dir;
                    }

                }
                sort($dir_array);


                foreach ($dir_array as $key => $value) {
                    /*
                     * List only directories.
                     * Ignore any single files that are in pending directory as this folder should
                     * only contain sub-folders to represent each queue.
                    */
                    if (is_dir(CORRESPONDENCE_PENDING_SCAN_DIR . "/" . $value) && (strlen($value) < 4)) {
                        $queue = $value;

                        // Count only PDF files in each sub-directory ( each queue )
                        $scans = count(glob(CORRESPONDENCE_PENDING_SCAN_DIR . "/$queue" . "/*.pdf"));

                        $stat = stat(CORRESPONDENCE_PENDING_SCAN_DIR . "/$queue");
                        $mod_date = date('d/m/Y H:i:s', $stat['mtime']);

                        // Get name of staff member queue is assigned to and label queue
                        $s = new Search(new CorrespondenceQueue);
                        $s->eq('queue', $queue);
                        if ($user = $s->next(MYSQLI_ASSOC)) {
                            $assigned = $user->staff;
                        } else {
                            $assigned = "Unassigned";
                        }

                        $named_q = [
                            99, 98, 96, 20, 21, 22, 23
                        ];

                        if (!in_array($queue, $named_q)) {
                            $q_name = "Queue No. " . $queue . " - " . $assigned;
                        } elseif ($queue == 99) {
                            $q_name = "Flagged - Client Services";
                        } elseif ($queue == 98) {
                            $q_name = "Flagged - Acquisition Support";
                        } elseif ($queue == 96) {
                            $q_name = "Flagged - New Business";
                        } elseif ($queue == 20) {
                            $q_name = "Queue No. 20 - Drawdown";
                        } elseif ($queue == 21) {
                            $q_name = "Queue No. 21 - OMO";
                        } elseif ($queue == 22) {
                            $q_name = "Queue No. 22 - NB Send outs";
                        } elseif ($queue == 23) {
                            $q_name = "Queue No. 23 - NB online mail";
                        } elseif ($queue == 13) {
                            $q_name = "Queue No. 13 - NB mail";
                        }

                        echo "<tr>
                                <td>" . $q_name . "</td>
                                <td class='mod'>" . $mod_date . "</td>
                                <td id='" . $queue . "' class='items'>" . $scans . "</td>
                                <td><button id='" . $queue . "' name='" . $queue . "' class='assign_queue'>
                                Select Queue
                                </button></td>.
                            </tr>";
                    }
                }
            }

            break;

        case "list_spot_check":

            if ($_GET['queue'] != 0) {
                $dir = opendir(CORRESPONDENCE_HOLDING_DIR . $_GET['queue']);

                $corr_admin = User::get_default_instance()->has_permission(44); // Correspondence Admin

                $files = [];
                while ($file = readdir($dir)) {
                    // We are only interested in the PDF files in the directory
                    if (strpos($file, '.pdf')) {
                        $files[] = $file;
                    }
                }
                // sort array in a natural order, alphanumerical
                natsort($files);

                $list_rows = "";

                foreach ($files as $file){
                    //get the correspondence ID from the filename
                    $corrID = str_replace(".pdf", "", $file);

                    //find the item of correspondence in the database
                    $s = new Search(new Correspondence());

                    $s->eq("CorrID", $corrID);

                    $process_date = "";

                    if ($c = $s->next(MYSQLI_ASSOC)){
                        $process_date = $c->date();
                    };

                    $list_rows .= "<tr>
                                        <td width='420px'>".$file."</td>
                                        <td width='420px'>".$process_date."</td>
                                        <td width='180px'><button id='".$corrID."' class='spot-check-item' disabled>Check Item</button></td>
                                        <td width='180px'><button class='view-spot-check' data-id='".$corrID."'>View (optional)</button></td>
                                    </tr>";
                }

                echo $list_rows;
            } else {
                //get count of all correspondence per directory
                $main_dir = scandir(CORRESPONDENCE_HOLDING_DIR);
                $main_dir = array_slice($main_dir, 2);

                $dir_array = [];
                foreach ($main_dir as $key => $dir) {
                    if (strlen($dir) < 4) {
                        $dir_array[] = $dir;
                    }

                }
                sort($dir_array);


                foreach ($dir_array as $key => $value) {
                    /*
                     * List only directories.
                     * Ignore any single files that are in pending directory as this folder should
                     * only contain sub-folders to represent each queue.
                    */
                    if (is_dir(CORRESPONDENCE_HOLDING_DIR . "/" . $value) && (strlen($value) < 4)) {
                        $queue = $value;

                        // Count only PDF files in each sub-directory ( each queue )
                        $scans = count(glob(CORRESPONDENCE_HOLDING_DIR . "/$queue" . "/*.pdf"));

                        $stat = stat(CORRESPONDENCE_HOLDING_DIR . "/$queue");
                        $mod_date = date('d/m/Y H:i:s', $stat['mtime']);

                        // Get name of staff member queue is assigned to and label queue
                        $s = new Search(new CorrespondenceQueue);
                        $s->eq('queue', $queue);
                        if ($user = $s->next(MYSQLI_ASSOC)) {
                            $assigned = $user->staff;
                        } else {
                            $assigned = "Unassigned";
                        }

                        $named_q = [
                            99, 98, 96, 20, 21, 22, 23
                        ];

                        if (!in_array($queue, $named_q)) {
                            $q_name = "Queue No. " . $queue . " - " . $assigned;
                        } elseif ($queue == 99) {
                            $q_name = "Flagged - Client Services";
                        } elseif ($queue == 98) {
                            $q_name = "Flagged - Acquisition Support";
                        } elseif ($queue == 96) {
                            $q_name = "Flagged - New Business";
                        } elseif ($queue == 20) {
                            $q_name = "Queue No. 20 - Drawdown";
                        } elseif ($queue == 21) {
                            $q_name = "Queue No. 21 - OMO";
                        } elseif ($queue == 22) {
                            $q_name = "Queue No. 22 - NB Send outs";
                        } elseif ($queue == 23) {
                            $q_name = "Queue No. 23 - NB online mail";
                        } elseif ($queue == 13) {
                            $q_name = "Queue No. 13 - NB mail";
                        }

                        echo "<tr>
                                <td>" . $q_name . "</td>
                                <td class='mod'>" . $mod_date . "</td>
                                <td id='" . $queue . "' class='items'>" . $scans . "</td>
                                <td><button id='" . $queue . "' name='" . $queue . "' class='spot_check_assign_queue'>
                                Select Queue
                                </button></td>.
                            </tr>";
                    }
                }
            }

            break;

        case "process":
            $filename = $_GET["item"];
            $filepath = CORRESPONDENCE_PENDING_SCAN_DIR . "/" . $_GET['queue'] . "/" . $filename;

            $flagDept = 0;

            switch ($_GET['queue']){
                case 99:
                    $flagDept = 6;
                    break;
                case 98:
                    $flagDept = 4;
                    break;
                case 96:
                    $flagDept = 3;
                    break;
            }

            $search_policy = new Search(new Policy);
            $search_client = new Search(new Client);
            $search_partner = new Search(new Partner);
            $search_practice = new Search(new Practice);
            $search_multiple_clients = new Search(new Partner);
            $search_flagged = new Search(new FlaggedCorrespondence);

            $search_policy
                ->flag("number", Search::DEFAULT_STRING, Search::DEFAULT_INT)
                ->flag("id")
                ->flag("partner", "client->partner")
                ->flag("issuer", "issuer")
                ->flag("has_mandate", Search::CUSTOM_FUNC, "policy_has_mandate")
                ->flag("type")
                ->flag("mandate");

            $search_client
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "client_search_name", Search::DEFAULT_STRING)
                ->flag("postcode", "address->postcode", Search::PATTERN, "/" . Patterns::UK_POSTCODE . "/")
                ->flag(
                    "nino",
                    Search::CUSTOM_FUNC,
                    "client_search_nino",
                    Search::PATTERN,
                    "/" . Patterns::NINO . "/"
                )
                ->flag("dob", Search::CUSTOM_FUNC, "client_search_dob")
                ->flag("partner")
                ->flag("s", Search::CUSTOM_FUNC, "client_search_scheme");

            $search_partner
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING)
                ->flag("postcode", "address->postcode", Search::PATTERN, "/" . Patterns::UK_POSTCODE . "/")
                ->flag("email", "email")
                ->flag("sjp", "agencyCode", Search::PATTERN, "/^\d{6}[A-Z]+$/");

            $search_practice
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "practice_name_search", Search::DEFAULT_STRING);

            $search_multiple_clients
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING)
                ->flag("postcode", "address->postcode", Search::PATTERN, "/" . Patterns::UK_POSTCODE . "/")
                ->flag("email", "email")
                ->flag("sjp", "agencyCode", Search::PATTERN, "/^\d{6}[A-Z]+$/");

            $search_flagged->eq("file_name", $filename);

            $flagReason1 = null;
            $flagReason2 = null;
            $flagReason3 = null;
            $flagReason4 = null;
            $notes = null;

            if ($sf = $search_flagged->next()){
                $flagReason1 = $sf->reason1();
                $flagReason2 = $sf->reason2();
                $flagReason3 = $sf->reason3();
                $flagReason4 = $sf->reason4();
                $notes = $sf->note();
            }

            echo new Template(
                "correspondence/process.html", [
                    "filename" => $filename,
                    "filepath" => $filepath,
                    "queue" => $_GET['queue'],
                    "flagReason1" => $flagReason1,
                    "flagReason2" => $flagReason2,
                    "flagReason3" => $flagReason3,
                    "flagReason4" => $flagReason4,
                    "notes" => $notes,
                    "practice_help" => $search_practice->help_html(),
                    "policy_help" => $search_policy->help_html(),
                    "partner_help" => $search_partner->help_html(),
                    "client_help" => $search_client->help_html(),
                    "multiple_clients_help" => $search_multiple_clients->help_html()
                ]
            );

            break;

        case "run_ppp":
            $return = [
                'status' => false,
                'feedback' => false,
                'output' => null,
            ];

            $dir = opendir(CORRESPONDENCE_PENDING_SCAN_DIR . "/" . $_GET['queue']);

            //get the time the last file in the queue was scanned
            $lastScan = 0;
            $files = [];
            while ($file = readdir($dir)) {
                // We are only interested in the PDF files in the directory
                if (strpos($file, '.pdf')) {
                    $stat = stat(CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['queue'] . "/" . $file);
                    if($stat['mtime'] > $lastScan){
                        $lastScan = $stat['mtime'];
                    }
                }
            }

            //get the time the output-json directory was last updated
            $stat = stat(CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['queue'] . "/output-json");
            $lastAnalysis = $stat['mtime'];

            //check if another file has been scanned since last analysis
//            if ($lastScan > $lastAnalysis){
            $python = '/usr/bin/python3';
            $pyscript = '/usr/local/bin/ppp/app/main3.py';

            if (!file_exists($python)) {
                $return['feedback'] = "The python executable '$python' does not exist!";
            }
            if (!is_executable($python)) {
                $return['feedback'] = "The python executable '$python' is not executable!";
            }
            if (!file_exists($pyscript)) {
                $return['feedback'] = "The python script file '$pyscript' does not exist!";
            }

            //Build up the command for the cmd line
            $cmd = "$python $pyscript";
            exec($cmd . " " . $_GET['queue'], $output, $return_code);

            if (!$return_code) {
                $return['status'] = true;
                $return['feedback'] = $output;
            } else {
                $return['status'] = false;
                $return['feedback'] = "Failed with code " . $return_code;
                $return['output'] = $output;
            }
//            } else {
//                $return['status'] = true;
//                $return['feedback'] = "Queue already analysed";
//            }

            echo json_encode($return);
            exit();
            break;

        case "ppp_check":
            $json = [
                'status' => false,
                'policy_count' => 0
            ];

            $file_path = $_GET['file'];
            // turn file path string into array so we can modify
            $file_path_array = explode('/', $_GET['file']);
            // add output-json into array in position
            array_splice($file_path_array, CORR_JSON_PATH_POSITION, 0, ['output-json']);
            // implode back to file path string
            $new_file_path = implode('/', $file_path_array);
            // replace extension of file we are viewing
            $file_str = str_replace('pdf', 'json', $new_file_path);

            // check if we have a json file with predicted policy number in it
            if (file_exists($file_str)) {
                // read json file
                $string = file_get_contents($file_str);
                $json_a = json_decode($string, true);

                $policy_quess_row = "";
                $multiple_array = [];

                foreach ($json_a['guessed-policynums'] as $key => $guess) {

                    // search db for any policy number matches so we have id/client/partner
                    $search = new Search(new Policy);
                    $search->eq('number', $guess);

                    while ($policy = $search->next()) {
                        $cli = new Client($policy->client->id(), true);
                        // check for valid client object here
                        if ($cli->partner()) {
                            //check for workflow tickets
                            $wf_ticket = "";

                            if ($wf = $policy->outstanding_workflow(3)){
                                $wf_ticket = el::bt( "<img src=".el::image("wf.png")." alt='workf'>",false, "href='".$wf->get_common_url(array('id'=>$wf->id(),'get'=>"profile"))."' class='workflow'" );
                            }

                            //check for new business ticket
                            $nb_ticket = "";

                            if ($nb = $policy->has_newbusiness()){
                                $nb_ticket = el::bt( "<img src=".el::image("newbus.png")." alt=nb>", false, "href='".NewBusiness::get_common_url(array('get'=>'profile','id'=>$nb->id()))."' class='new_business'" );
                            }

                            $policy_quess_row .= '<tr class="even">
                            <td><input type="radio" name="policy_result" value="' . $policy->id() . '"/>' . $wf_ticket . $nb_ticket . '</td>
                            <td id="' . $policy->id() . '" style="width: 116px;" class="policy_num">' . $policy->link() . '</td>
                            <td style="width: 87px;">' . $policy->issuer->link() . '</td>
                            <td class="client_name" id="' . $policy->client->id() . '" style="width: 85px;">' . $policy->client->link() . '</td>
                            <td class="policy_type" style="width: 72px;">' . $policy->type . '</td>
                            <td style="width: 109px;">' . $policy->client->partner->link() . '</td>
                            <td style="width: 93px;">' . $policy->status . '</td>
                            
                             <td class="merge_request" style="width: 4px;">
		                        <input id="' . $policy->id() . '" type="checkbox">
	                        </td>
                        </tr>';
                            $json['policy_count']++;

                            // build up our partner/policy/issuer counter
                            if (key_exists($policy->client->partner->id(), $multiple_array)) {
                                $multiple_array[$policy->client->partner->id()][$policy->issuer->id()] =
                                    $multiple_array[$policy->client->partner->id()][$policy->issuer->id()] + 1;
                            } else {
                                $multiple_array[$policy->client->partner->id()][$policy->issuer->id()] = 1;
                            }
                        }
                    }
                }

                $issuer_check_array = [];
                foreach ($multiple_array as $partnerID => $issuerID) {
                    foreach ($issuerID as $iid => $count) {
                        // check if we have more than one policy under the same provider for a partner
                        if ($count > 1) {
                            // we want to show multiple client policy for this partner/issuer combo
                            $search_1 = new Search(new Client);
                            $search_1->eq("one->surname", "%multiple%", true, 0);
                            $search_1->eq("partner", $partnerID);
                            // search for a multiple clients 'Client'
                            while ($client = $search_1->next()) {
                                // search for a multiple clients 'Policy'
                                $search_2 = new Search(new Policy);
                                $search_2->eq("client", $client->id());
                                $search_2->eq("number", "%multiple%", true, 1);
                                $search_2->eq("issuer", $iid);
                                if ($policy = $search_2->next()) {
                                    //check for workflow tickets
                                    $wf_ticket = "";

                                    if ($wf = $policy->outstanding_workflow(3)){
                                        $wf_ticket = el::bt( "<img src=".el::image("wf.png")." alt='workf'>",false, "href='".$wf->get_common_url(array('id'=>$wf->id(),'get'=>"profile"))."' class='workflow'" );
                                    }

                                    //check for new business ticket
                                    $nb_ticket = "";

                                    if ($nb = $policy->has_newbusiness()){
                                        $nb_ticket = el::bt( "<img src=".el::image("newbus.png")." alt=nb>", false, "href='".NewBusiness::get_common_url(array('get'=>'profile','id'=>$nb->id()))."' class='new_business'" );
                                    }

                                    // multiple clients policy found
                                    $policy_quess_row .= '<tr class="even">
                                    <td><input type="radio" name="policy_result" value="' . $policy->id() . '"/>' . $wf_ticket . $nb_ticket . '</td>
                                    <td id="' . $policy->id() . '" style="width: 116px;">' . $policy->link() . '</td>
                                    <td style="width: 87px;">' . $policy->issuer->link() . '</td>
                                    <td id="' . $policy->client->id() . '" style="width: 85px;">' . $policy->client->link() . '</td>
                                    <td style="width: 72px;">' . $policy->type . '</td>
                                    <td style="width: 109px;">' . $policy->client->partner->link() . '</td>
                                    <td style="width: 93px;">' . $policy->status . '</td>
                                    <td class="merge_request" style="width: 4px;">
                                        <input id="' . $policy->id() . '" type="checkbox">
                                    </td>
                                </tr>';

                                }
                            }
                        }
                    }
                }

                $json['guessed_polnums'] = $policy_quess_row;

                if ($json['guessed_polnums']) {
                    $json['status'] = true;
                }
            }

            echo json_encode($json);

            break;

        case "ppp_subject_check":
            $json = [
                'status' => false,
                'subject_count' => 0,
                'subjects' => false
            ];

            $file_path = $_GET['file'];
            // turn file path string into array so we can modify
            $file_path_array = explode('/', $_GET['file']);
            // add output-json into array in position
            array_splice($file_path_array, CORR_JSON_PATH_POSITION, 0, ['output-json']);
            // implode back to file path string
            $new_file_path = implode('/', $file_path_array);
            // replace extension of file we are viewing
            $file_str = str_replace('.pdf', '-subjects.json', $new_file_path);

            // check if we have a json file with predicted policy number in it
            if (file_exists($file_str)) {
                // read json file
                $string = file_get_contents($file_str);
                $json_a = json_decode($string, true);

                $subjects = "<select id='correspondence[subject]' name='correspondence[subject]'>";
                $checked = 'checked';

                foreach ($json_a['subjects'] as $key => $guess) {
                    $subjects .= "<option>" . $guess[0] . "</option>";
                    $json['subject_count']++;
                }

                $subjects .= "</select>";

                if ($json['subject_count'] > 0) {
                    $json['subjects'] = $subjects;
                    $json['status'] = true;
                }
            }

            echo json_encode($json);

            break;

        case "fe_scan":
            if (file_exists($_GET["file"])) {
                if ($f = fopen($_GET["file"], "r")) {
                    header("Cache-control: private");
                    header("Content-Type: application/pdf");
                    header("Pragma: public");
                    $contents = fread($f, filesize($_GET["file"]));
                    print $contents;
                    fclose($f);
                    exit();
                }
            } else {
                print "File not found";
                exit();
            }

            break;

        case "spot_check_file":
            if (file_exists($_GET["file"])) {
                if ($f = fopen($_GET["file"], "r")) {
                    header("Cache-control: private");
                    header("Content-Type: application/pdf");
                    header("Pragma: public");
                    $contents = fread($f, filesize($_GET["file"]));
                    print $contents;
                    fclose($f);
                    exit();
                }
            } else {
                print "File not found";
                exit();
            }

            break;

        case "delete_correspondence":
            $return = [
                "status" => false,
                "error" => false
            ];

            $selected_file = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['dir'] . "/" . $_GET['policyID'];

            try {
                if (file_exists($selected_file)) {
                    if (!unlink($selected_file)) {
                        $return['error'] = "Error deleting file: " . $selected_file;
                        $return['status'] = false;
                    } else {
                        $return['error'] = false;
                        $return['status'] = "Correspondence Item successfully deleted.";
                    }
                } else {
                    throw new Exception("The file you are trying to delete does not exist");
                }
            } catch (Exception $e) {
                $return['error'] = "Error: " . $e->getMessage();
            }

            echo json_encode($return);

            return;
            break;

        case "policy_search":

            if (strlen($_GET["search"]) < 3) {
                echo json_encode("Do not search for less than 3 characters");
                exit();
            } else {
                $file_path = SEARCH_LOG_DIR.User::get_default_instance('id').'.txt';
                if (!file_exists($file_path)) {
                    $file = fopen($file_path,"wb");
                    fclose($file);
                }

                // log search for audit / debugging purposes
                $text = $_GET["search"]." - ".Carbon\Carbon::now();

                $audit_file = file_put_contents(
                    $file_path,
                    $text.PHP_EOL ,
                    FILE_APPEND | LOCK_EX
                );
            }

            $search = new Search(new Policy);

            $search
                ->flag("number", Search::DEFAULT_STRING, Search::DEFAULT_INT)
                ->flag("id")
                ->flag("partner", "client->partner")
                ->flag("issuer", "issuer")
                ->flag("has_mandate", Search::CUSTOM_FUNC, "policy_has_mandate")
                ->flag("type")
                ->flag("mandate");

            if (isset($_GET["search"])) {
                if (isset($_GET["filter"]) && is_array($_GET["filter"])) {
                    $search->apply_filters($_GET["filter"]);
                }

                $show_client = $show_partner = $show_type = true;

                // apply profile filters
                if (isset($_GET["client"])) {
                    $show_client = false;
                    $show_partner = false;

                    $search->eq("client", (int)$_GET["client"]);
                    $search->add_order("issuer->name", "ASC");
                }

                //if we want to restrict search to one specific issuer
                if (isset($_GET["issuer"])) {
                    $search->eq("issuer", (int)$_GET["issuer"]);
                }

                if (!isset($excludes)) {
                    $excludes = [];
                }

                // do not return any archived policies
                $search->eq("archived", null);

                $t = Correspondence::get_template('list_row_policy.html');
                $i = 0;
                while ($policy = $search->next($_GET["search"])) {
                    $t->tags([
                        "excludes" => $excludes,
                        "policy" => $policy,
                        "client" => $policy->client,
                        "issuer" => $policy->issuer,
                        "partner" => $policy->client->get_object()->partner
                    ]);

                    echo "$t";
                }
            }

            break;

        case "client_search":
            function client_search_scheme(&$search, $str)
            {
                $search->add_and($search->get_object()->group_scheme->to_sql(true));
            }

            function client_search_dob(&$search, $str)
            {
                list($d, $m, $y) = explode("/", $str["text"]);
                $dob = "$y-$m-$d";
                $search->add_or([
                    $search->eq("one->dob", $dob, false, 0),
                    $search->eq("two->dob", $dob, false, 0)
                ]);
            }

            function client_search_nino(&$search, $str)
            {
                $search->add_or([
                    $search->eq("one->nino", $str["text"], true, 0),
                    $search->eq("two->nino", $str["text"], true, 0)
                ]);
            }

            function client_search_name(&$search, $str)
            {
                if (preg_match('/([\d\w\s]+) [&+] ([\d\w\s]+)/', $str["text"], $match)) {
                    $str["text"] = $match[1];
                    client_search_name($search, $str);

                    $str["text"] = $match[2];
                    client_search_name($search, $str);

                    return;
                }

                $x = parse_name($str["text"]);

                $sname_wildcard = ($x["wildcard"] == "surname");

                ($sname_wildcard) ? $x["surname"] .= "%" : $x["forename"] .= "%";

                $conditions = [];

                if (isset($x["forename"])) {
                    //we always require a wildcard here due to middle names
                    $search->add_or([
                        $search->eq("one->forename", $x["forename"] . "%", true, 0),
                        $search->eq("two->forename", $x["forename"] . "%", true, 0)
                    ]);
                }

                $search->add_or([
                    $search->eq("one->surname", $x["surname"], $sname_wildcard, 0),
                    $search->eq("two->surname", $x["surname"], $sname_wildcard, 0)
                ]);
            }

            $client = new Client;
            $search = new Search($client);
            $search->calc_rows = true;

            $search
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "client_search_name", Search::DEFAULT_STRING)
                ->flag("postcode", "address->postcode", Search::PATTERN, "/" . Patterns::UK_POSTCODE . "/")
                ->flag("nino", Search::CUSTOM_FUNC, "client_search_nino", Search::PATTERN, "/" . Patterns::NINO . "/")
                ->flag("dob", Search::CUSTOM_FUNC, "client_search_dob")
                ->flag("partner")
                ->flag("s", Search::CUSTOM_FUNC, "client_search_scheme");

            if (isset($_GET["search"])) {
                if (isset($_GET["limit"]) && (int)$_GET["limit"] > 0) {
                    $search->set_limit((int)$_GET["limit"]);
                } else {
                    $search->set_limit(100);
                }

                if (isset($_GET["skip"]) && (int)$_GET["skip"] > 0) {
                    $search->set_offset((int)$_GET["skip"]);
                }

                $matches = [
                    "results" => [],
                    "counter" => 0,
                    "remaining" => 0,
                ];

                $search->order($client->NAME_ORDER);

                // Additional options
                if (isset($_REQUEST["partner"])) {
                    $search->add_and($search->object->partner->to_sql((int)$_REQUEST["partner"]));
                }

                // do not return any archived clients
                $search->eq("archived", null);

                while ($m = $search->next($_GET["search"])) {
                    $postcode = (strlen($m->address->postcode))
                        ? "(" . $m->address->postcode . ")"
                        : false;

                    $matches["results"][] = [
                        "value" => "$m->id",
                        "link" => $m->link("$m $postcode"),
                        "partner" => $m->partner->link(),
                        "partnerid" => $m->partner->id()
                    ];
                }

                $matches["counter"] = count($matches['results']);
                $matches["remaining"] = $search->remaining;

                echo json_encode($matches);
            }

            break;

        case "partner_search":
            function search_on_name(&$search_obj, $text)
            {
                $x = parse_name($text["text"]);

                ($x["wildcard"] == "forename") ? $x["forename"] .= "%" : $x["surname"] .= "%";

                $conditions = [];

                if (isset($x["forename"])) {
                    $conditions[] = $search_obj->eq("forename", $x["forename"], ($x["wildcard"] == "forename"), false);
                }

                $conditions[] = $search_obj->eq("surname", $x["surname"], ($x["wildcard"] == "surname"), false);
                $search_obj->add_and($conditions);
            }

            $partner = new Partner;
            $search = new Search($partner);
            $search->calc_rows = true;

            $search
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING)
                ->flag("postcode", "address->postcode", Search::PATTERN, "/" . Patterns::UK_POSTCODE . "/")
                ->flag("email", "email")
                ->flag("sjp", "agencyCode", Search::PATTERN, "/^\d{6}[A-Z]+$/");

            if (isset($_GET["search"])) {
                $search->add_order("surname");
                $search->set_limit(SEARCH_LIMIT);
                $search->set_offset((int)$_GET["skip"]);

                $matches = [];

                while ($partner = $search->next($_GET["search"])) {
                    $add_client = "<button id='$partner->id' class='add_client add'>&nbsp;Add Client</button>";
                    $matches["results"][] = ["partner" =>
                        "<a href='common/Partner/?id=$partner->id' class='profile Partner'>
                                                   <span id='$partner->id'>$partner</span></a>",
                        "action" => "$add_client"
                    ];
                }

                $matches["remaining"] = $search->remaining;

                echo json_encode($matches);
            }

            break;

        case "practice_search":
            function practice_name_search(&$search, $str)
            {
                $search->eq($search->object->name, "%$str[text]%", true);
            }

            $practice = new Practice;
            $search = new Search($practice);

            $search
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "practice_name_search", Search::DEFAULT_STRING);

            if (isset($_GET["search"])) {
                $search->set_limit(SEARCH_LIMIT);
                if (isset($_GET['skip'])) {
                    $search->set_offset((int)$_GET["skip"]);
                }

                $matches = [];

                //will do nothing if $matches["results"] is being set again below
                while ($match = $search->next($_GET["search"])) {
                    $matches["results"][] = ["text" => "$match", "value" => "$match->id"];
                }

                $db = new mydb();
                $q = 'SELECT practice.id FROM ' . PRACTICE_TBL . ' 
                      LEFT JOIN ' . PRACTICE_ALIAS_TBL . ' ON ' . PRACTICE_TBL . '.id = ' . PRACTICE_ALIAS_TBL . ' .practice_ID
                      WHERE ' . PRACTICE_ALIAS_TBL . '.alias 
                      LIKE "%' . addslashes($_GET['search']) . '%" OR name LIKE "%' . $_GET['search'] . '%"';

                $db->query($q);

                $results = "";
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $p = new Practice($row['id'], true);

                    $db2 = new mydb();
                    $q = "SELECT * FROM tblpartner
                             INNER JOIN practice_staff on tblpartner.partnerID = practice_staff.partner_id
                             where practice_id = " . $p->id();

                    $db2->query($q);
                    while ($row = $db2->next(MYSQLI_ASSOC)) {
                        $pa = new Partner($row['partnerID'], true);
                        $results .= "
						<tr id='" . $p->id() . "'>
						<td>" . $pa->link() . "</td>
						<td>" . $p->link() . "</td>
						</tr>";
                    }
                }

                // if no policies returned
                if ($results == "") {
                    $results .= "<tr><td colspan='2'>No practice found</td></tr>";
                }

                $matches["results"] = $results;
                echo json_encode($matches);
            }

            break;

        case "multiple_clients_search":
            function search_on_name(&$search_obj, $text)
            {
                $x = parse_name($text["text"]);
                ($x["wildcard"] == "forename") ? $x["forename"] .= "%" : $x["surname"] .= "%";

                $conditions = [];

                if (isset($x["forename"])) {
                    $conditions[] = $search_obj->eq("forename", $x["forename"], ($x["wildcard"] == "forename"), false);
                }

                $conditions[] = $search_obj->eq("surname", $x["surname"], ($x["wildcard"] == "surname"), false);
                $search_obj->add_and($conditions);
            }

            $partner = new Partner;
            $search = new Search($partner);
            $search->calc_rows = true;

            $search
                ->flag("id", Search::DEFAULT_INT)
                ->flag("name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING)
                ->flag("postcode", "address->postcode", Search::PATTERN, "/" . Patterns::UK_POSTCODE . "/")
                ->flag("email", "email")
                ->flag("sjp", "agencyCode", Search::PATTERN, "/^\d{6}[A-Z]+$/");

            if (isset($_GET["search"])) {
                $search->add_order("surname");
                $search->set_limit(SEARCH_LIMIT);
                $search->set_offset((int)$_GET["skip"]);

                $partner_ids = [];
                $partner_count = 0;
                while ($partner = $search->next($_GET["search"])) {
                    $partner_ids[] = $partner->id();
                    $partner_count = $partner_count + 1;
                }

                $loop_count = 0;
                $results = "";

                while ($loop_count < $partner_count) {
                    $p = new Partner($partner_ids[$loop_count], true);

                    $q = "SELECT * FROM tblpolicy
                    INNER JOIN tblclient on tblpolicy.clientID = tblclient.clientID
                    WHERE partnerID = " . $p->id() . "
                    AND (clientFname1
					LIKE '%multiple%' OR clientSname1 
					LIKE '%multiple%' OR clientFname2 
					LIKE '%multiple%' OR clientSname2 
					LIKE '%multiple%')
                    AND policyNum like '%multiple%' and tblclient.archived is null and tblpolicy.archived is null";

                    $db = new mydb();
                    $db->query($q);
                    $results .= "<tr><td colspan='3'>Add multiple client policy for partner " . $p->link() . "</td>
                                 <td><button class='add' id='" . $p->id() . "'>Add policy</button></td></tr>";
                    $entries = false;
                    while ($result = $db->next(MYSQLI_ASSOC)) {
                        $pol = new Policy($result['policyID'], true);

                        $results .= "
						<tr>
                            <td>" . $pol->link() . "</td>
                            <td>" . $pol->issuer->link() . "</td>
                            <td>" . $pol->client->link() . "</td>
                            <td>" . $p->link() . "</td>
						</tr>";

                        $entries = true;
                    }
                    $loop_count = $loop_count + 1;
                }

                $matches["results"] = $results;

                echo json_encode($matches);
            }

            break;

        case "add_correspondence":

            $return = [
                "id" => false,
                "status" => false,
                "error" => false,
                "url" => null,
                "corr_count" => null
            ];

            try {
                if (isset($_GET['policyID'])) {
                    $pol_obj = new Policy($_GET['policyID'], true);
                    $policy = $pol_obj->id();
                    $client = $pol_obj->client->id();
                    $partner = $pol_obj->client->partner->id();
                    $policy_status = $_GET['status'];
                    $policy_owner = $_GET['owner'];
                    $policy_type = $_GET['type'];
                    $subject = addslashes($_GET['subject']);
                    $message = addslashes($_GET['message']);
                    $front_end = $_GET['front_end'];

                    //get the queue number
                    $filepath = str_replace("/".$_GET['filename'], "", $_GET['filepath']);
                    $filepathParts = explode("/", $filepath);

                    $queue = end($filepathParts);

                    if ($partner == 394 && ($_GET['letter_type'] != 15 && $_GET['letter_type'] != 37)) {
                        $return['error'] = true;
                        $return['status'] = "Correspondence cannot be added under the untraced account (394)";
                        echo json_encode($return);
                        exit();
                    }

                    // HACK todo - look ino the proper behavior of traditional scan on Prophet & myps
                    ($front_end) ? $traditional = 1 : $traditional = 1;

                    // letter type = coversheet
                    $letter_type = 6;
                    $date = date("Y-m-d H:i:s");

                    $ppp_predicted = $_GET['ppp_predicted'];

                    $db = new mydb();

                    $q = "INSERT INTO " . CORR_TBL . " 
                    (Policy, Client, Partner, dateStamp, Message,
                     Sender, Subject, letter_type, traditional_scan, processed_fes, ppp_predicted)
                                VALUES ('" . $policy . "','" . $client . "','" . $partner . "','" . $date . "','" . $message . "',
                                '" . User::get_default_instance('id') . "','" . $subject . "'," . $letter_type . ",
                                " . $traditional . "," . $front_end . ", " . $ppp_predicted . ")";

                    $db->query($q);

                    $q = "SELECT LAST_INSERT_ID()";
                    $db->query($q);

                    // correspondence item successfully added
                    if ($x = $db->next(MYSQLI_ASSOC)) {
                        $last_id = array_values($x);
                        $return['id'] = $last_id[0];

                        switch($subject){
                            case "Ad-hoc Statement":
                            case "Annual Statement":
                            case "Quarterly Statement":
                            case "Quarterly Valuation":
                            case "Valuation":
                            case "Correspondence - Original Sent directly to client":
                            case "Correspondence - Original Sent to partner":
                                if(!empty($_GET['policy_val'])){
                                    $pval = new Valuation();

                                    $valuation = str_replace([",","£"], "", trim($_GET['policy_val']));

                                    if(substr($valuation, -1) === "."){
                                        $valuation = substr($valuation, 0, -1);
                                    }

                                    $pval->object_type("Policy");
                                    $pval->object_id($policy);
                                    $pval->value_at($_GET['policy_val_date']);
                                    $pval->value($valuation);
                                    $pval->comments("From Correspondence Upload");
                                    $pval->currency($_GET['policy_val_currency']);

                                    $pval->save();
                                }

                                break;

                            case "Client ID":
                                if(!empty($_GET['client_index'])){
                                    $clientIDdoc = new ClientIDDocument();

                                    $clientIDdoc->client($client);
                                    $clientIDdoc->index($_GET['client_index']);
                                    $clientIDdoc->correspondence($last_id[0]);

                                    $clientIDdoc->save();
                                }

                                break;
                        }

                        $polChanged = false;
                        $omission_subjects = unserialize(CORR_STATUS_OMISSION);
                        if ($policy_status == 1 && (!in_array($subject, $omission_subjects))) {
                            // update policy status from awaiting confirmation to servicing transferred
                            $pol_obj->status(2);
                            $pol_obj->transferred(date("Y-m-d"));
                            $polChanged = true;
                        } elseif ($pol_obj->status() != $policy_status) {
                            // policy status has been changed, update to new status
                            $pol_obj->status($policy_status);
                            $polChanged = true;
                        }

                        if ($pol_obj->owner() != $policy_owner) {
                            // if policy owner has changed, update
                            if ($policy_owner) {
                                $pol_obj->owner($policy_owner);
                            }
                            $polChanged = true;
                        }

                        if($pol_obj->type() != $policy_type) {
                            if ($policy_type) {
                                $pol_obj->type($policy_type);
                            }
                            $polChanged = true;
                        }

                        // if there were any changes
                        if ($polChanged){
                            $pol_obj->save();
                        }
                    } else {
                        $return['error'] = true;
                        $return['status'] = "Correspondence item failed to insert to database";
                        echo json_encode($return);
                        exit();
                    }

                    // here we want to run jasper report and create a coversheet
                    if (isset($last_id[0])) {
                        try {
                            $correspondence_obj = new Correspondence();

                            // get letter object based on ID
                            $letter_obj = new Letter($letter_type, true);

                            $c = new \Jaspersoft\Client\Client(
                                JASPER_HOST,
                                JASPER_USER,
                                JASPER_PASSWD,
                                ""
                            );

                            $form_values = [];
                            $form_values["ITEM_ID"] = $last_id[0];
                            $form_values["correspondence[letter]"][] = $letter_type;

                            // get constant values for standard letter footer
                            $std_letter_foot = unserialize(STANDARD_LETTER_FOOTER);
                            foreach ($std_letter_foot as $key => $value) {
                                $form_values[$key][] = $value;
                            }

                            // point to prophet_reports dir on rosie
                            $report_url = JASPER_STANDARD_LETTERS . $letter_obj->rosie_file();

                            $report_url = Letter::url_path_corrections($report_url);

                            // check if we have any form data to pass through
                            if (!empty($form_values)) {
                                try {
                                    $report = $c->reportService()
                                        ->runReport($report_url, 'pdf', null, null, $form_values);

                                } catch (Exception $e) {
                                    exit('Caught exception: ' . $e->getMessage() . "\n");
                                }
                            } else {
                                $return['error'] = true;
                                $return['status'] = "Report parameters missing, please re-run report.";
                                echo json_encode($return);
                                exit();
                            }

                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Description: File Transfer');
                            header('Content-Disposition: attachment; filename=' . $letter_obj->rosie_file() . '.pdf');
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Content-Length: ' . strlen($report));
                            header('Content-Type: application/pdf');

                            // temporary create coversheet for merging with correspondence
                            $temp_coversheet = CORRESPONDENCE_PENDING_SCAN_DIR . $last_id[0] . "_temp_coversheet.pdf";
                            file_put_contents($temp_coversheet, $report);

                            // check that temporary coversheet has been created, if so continue..
                            if (!file_exists($temp_coversheet)) {
                                $return['error'] = true;
                                $return['status'] = "An error has occurred creating coversheet, please contact IT.";
                            } else {
                                // now that we have the cover sheet we want to use ghost script to join this with our
                                // current item of correspondence
                                $correspondence = str_replace('//', '/', $_GET['filepath']);

                                // merge files contained in this array
                                $fileArray = [$temp_coversheet, $correspondence];

                                //create the spot check queue directory if it doesnt exist
                                if (!is_dir(CORRESPONDENCE_HOLDING_DIR.$queue)){
                                    mkdir(CORRESPONDENCE_HOLDING_DIR.$queue);
                                }

                                // specify directory to place new file in
                                $corr_file = CORRESPONDENCE_HOLDING_DIR . "/". $queue . "/" . $last_id[0]. ".pdf";
                                //$corr_file = CORRESPONDENCE_PROCESSED_SCAN_DIR . $last_id[0] . ".pdf";

                                // let ghost script works its magic
                                $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH  -sOutputFile=$corr_file ";

                                //Add each pdf file to the end of the command
                                foreach ($fileArray as $file) {
                                    $cmd .= $file . " ";
                                }

                                //merge and create final pdf
                                $result = exec($cmd);

                                // check final pdf has ben created
                                if (file_exists($corr_file)) {
                                    $return['url'] = $corr_file;
                                    $return['status'] = "Correspondence item successfully created.";
                                    // delete temp coversheet now that merged pdf has been created
                                    if (file_exists($temp_coversheet)) {
                                        unlink($temp_coversheet);
                                    }
                                    // delete pending corr item from queue
                                    if (file_exists($correspondence)) {
                                        unlink($correspondence);
                                    }

                                    $return["corr_count"] =
                                        Correspondence::correspondence_count(User::get_default_instance('id'));
                                } else {
                                    $return['error'] = true;
                                    $return['status'] = "An error has occurred,
                                     please try again or contact IT if error persists.";
                                }
                            }
                        } catch (Exception $e) {
                            die($e->getMessage());
                        }
                    } else {
                        $return['error'] = true;
                        $return['status'] = "Correspondence failed to add to database, contact IT.";
                    }
                }

                echo json_encode($return);
            } catch (Exception $e) {
                die($e->getMessage());
            }

            return;
            break;

        case "policy_added":
            $db = new mydb();
            $q = "SELECT * FROM tblpolicy
                  WHERE addedby = " . User::get_default_instance('id') . " 
                  ORDER BY policyID DESC LIMIT 1";

            $db->query($q);
            while ($p = $db->next(MYSQLI_ASSOC)) {
                $policyArray = $p;
            }

            $policy = new Policy($policyArray['policyID'], true);

            $added_pol = [];
            $added_pol[] = "<td>" . $policy->link($policy->number) . "</td>";
            $added_pol[] = "<td>" . $policy->issuer->link($policy->issuer) . "</td>";
            $added_pol[] = "<td>" . $policy->client->link($policy->client) . "</td>";
            $added_pol[] = "<td>" . $policy->client->get_object()->partner->link($policy->client->get_object()->partner) . "</td>";
            $added_pol[] = "<td>" . $policy->status . "</td>";
            $added_pol[] = "<td>" . $policy->getOwnerAttribute($policy->owner()) . "</td>";

            echo json_encode($added_pol);

            break;

        case "client_added":
            $db = new mydb();
            $q = "SELECT * FROM tblclient 
                  WHERE addedBy = " . User::get_default_instance('id') . " 
                  ORDER BY clientID DESC LIMIT 1";

            $db->query($q);
            while ($c = $db->next(MYSQLI_ASSOC)) {
                $clientArray = $c;
            }

            $client = new Client($clientArray['clientID'], true);

            $added_client = [];
            $added_client[] = "<td>" . $client->id . "</td>";
            $added_client[] = "<td>" . $client->link($client) . "</td>";
            $added_client[] = "<td>" . $client->link($client->partner) . "</td>";

            echo json_encode($added_client);

            break;

        case "add_multiple_policy":
            $partner = new Partner($_GET['partner'], true);
            $policy = new Policy();

            if (isset($_GET['issuer'])) {
                $json = [
                    "status" => false,
                    "error" => false
                ];

                // check if partner has a 'multiple client' client
                $db = new mydb();
                $db->query("select clientID from tblclient 
                            where partnerID = ".$partner->id()." 
                            AND (clientFname1
					        LIKE '%multiple%' OR clientSname1 
					        LIKE '%multiple%' OR clientFname2 
					        LIKE '%multiple%' OR clientSname2 
					        LIKE '%multiple%')");

                if ($q = $db->next(MYSQLI_ASSOC)) {
                    // add policy to 'multiple client' client
                    $pol = new Policy();
                    $pol->number("multiple clients");
                    $pol->type(17);
                    $pol->issuer($_GET['issuer']);
                    $pol->client($q['clientID']);
                    if ($pol->save()) {
                        $json['status'] = "Multiple Client policy successfully added";
                    } else {
                        $json['status'] = "An error occurred, contact IT";
                    }
                } else {
                    // no 'multiple client' client so add new client first then add policy
                    $client = new Client();
                    $client->one->surname("* Multiple clients");
                    $client->partner($partner->id());
                    if ($client->save()) {
                        // add policy to 'multiple client' client
                        $pol = new Policy();
                        $pol->number("multiple clients");
                        $pol->type(17);
                        $pol->issuer($_GET['issuer']);
                        $pol->client($client->id());
                        $pol->owner(1);
                        if ($pol->save()) {
                            $json['status'] = "Multiple Client policy successfully added";
                        } else {
                            $json['status'] = "An error occurred, contact IT";
                        }
                    }
                }
                echo json_encode($json);
            } else {
                echo new Template(
                    "correspondence/new_policy_multiple_client.html",
                    ["partner"=>$partner, "policy"=>$policy]
                );
            }

            break;

        case "move":
            $return = [
                "status" => false,
                "error" => false
            ];

            $rand = "";

            function move_file($rand)
            {
                $current = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['dir'] . "/" . $_GET['corr_id'];
                if ($_GET['dept'] == 6 || $_GET['dept'] == 14 || $_GET['dept'] == 15) {
                    // cs flagged queue
                    $new = CORRESPONDENCE_PENDING_SCAN_DIR . "99/" . $rand . $_GET['corr_id'];
                } elseif ($_GET['dept'] == 4) {
                    // acq support flagged queue
                    $new = CORRESPONDENCE_PENDING_SCAN_DIR . "98/" . $rand . $_GET['corr_id'];
                } elseif ($_GET['dept'] == 3) {
                    // nb support flagged queue
                    $new = CORRESPONDENCE_PENDING_SCAN_DIR . "96/" . $rand . $_GET['corr_id'];
                }


                if (file_exists($new)) {
                    $rand = rand(1, 100);

                    return move_file($rand);
                } else {
                    if (rename($current, $new)) {
                        // remove path and give us filename only
                        $tokens = explode('/', $new);
                        $filename = trim(end($tokens));

                        // add entry to flag mapping table
                        $flagged = new FlaggedCorrespondence();
                        $flagged->file_name($filename);
                        $flagged->dept($_GET['dept']);
                        $flagged->reason1($_GET['flag_reason1']);
                        $flagged->reason2($_GET['flag_reason2']);
                        $flagged->reason3($_GET['flag_reason3']);
                        $flagged->reason4($_GET['flag_reason4']);
                        $flagged->note($_GET['notes']);
                        if ($flagged->save()) {
                            $return['status'] = "File moved to flagged folder";
                        }
                    } else {
                        $return['error'] = true;
                    }
                }
            }

            move_file($rand);

            echo json_encode($return);

            break;

        case "assign":
            $response = [
                "status" => false,
                "error" => false,
                "json_dir" => false
            ];

            $user = new User(User::get_default_instance("id"), true);

            $directory = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['dir'];
            $assignedFile = $directory . "/assigned.json";

            $response['json_dir'] = file_exists($directory . "/output-json");

            if (!in_array($_GET['dir'], [96, 98, 99])){
                // check if the assigning file exists, if not, create one
                if (file_exists($assignedFile)){
                    $assigned = json_decode(file_get_contents($assignedFile));

                    if ($assigned->user){
                        // queue is assigned to someone
                        $assigned_user = new User($assigned->user, true);
                        $corr_admin = User::get_default_instance()->has_permission(44); // Correspondence Admin

                        if ($assigned->user == $user->id()){
                            $response['status'] = "Queue " . $_GET['dir'] . " is currently assigned to " . $assigned_user->username;
                        } elseif($corr_admin == true) {
                            $response['status'] = "Queue " . $_GET['dir'] . " is currently assigned to " . $assigned_user->username . " - Admin Viewing";
                        } elseif ($assigned->user != $user->id() && $corr_admin == false) {
                            $response['error'] = true;
                            $response['status'] = "Queue " . $_GET['dir'] . " is already assigned to " . $assigned_user->username;
                        } else {
                            $response['error'] = true;
                            $response['status'] = "An error has occurred when checking assigned queue. Contact IT";
                        }
                    } else {
                        // queue is not assigned, assign to me
                        if(assignQueue($user->id(), $assignedFile)){
                            $response['status'] = "Queue " . $_GET['dir'] . " is now assigned to " . $user->username;
                        } else {
                            $response['error'] = true;
                            $response['status'] = "Failed to assign you to the queue";
                        }
                    }
                } else {
                    if(assignQueue($user->id(), $assignedFile)){
                        $response['status'] = "Queue " . $_GET['dir'] . " is now assigned to " . $user->username;
                    } else {
                        $response['error'] = true;
                        $response['status'] = "Failed to create new assigned file and assign you to the queue";
                    }
                }
            } else {
                // this is a query queue, leave the door open
                $response['status'] = "Open Queue";
            }


            echo json_encode($response);

            break;

        case "spot_check_assign":
            $return = [
                "status" => false,
                "error" => false,
                "json_dir" => false
            ];

            $current_user = new User(User::get_default_instance("id"), true);

            $directory = CORRESPONDENCE_HOLDING_DIR . $_GET['dir'];
            $oldAssignedFile = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['dir'] . "/assigned.json";
            $fileLocation = $directory . "/assigned.json";

            $user_id = User::get_default_instance('id');

            if (!in_array((int)$_GET['dir'], [96, 98, 99])) {
                if (file_exists($fileLocation)){
                    $assigned = json_decode(file_get_contents($fileLocation));

                    if ($assigned->user) {
                        $assigned_user = new User($assigned->user, true);
                        $corr_admin = User::get_default_instance()->has_permission(44); // Correspondence Admin

                        if ($assigned_user->id() == $current_user->id()) {
                            $return['status'] = "Queue " . $_GET['dir'] .
                                " is currently assigned to " . $assigned_user->username;
                        } elseif ($corr_admin == true) {
                            $return['status'] = "Queue " . $_GET['dir'] .
                                " is currently assigned to " . $assigned_user->username . " - Admin viewing";
                        } elseif (($assigned_user->id() != $current_user->id()) && ($corr_admin == false)) {
                            $return['error'] = true;
                            $return['status'] = "Queue " . $_GET['dir'] .
                                " has already been assigned to " . $assigned_user->username;
                        } else {
                            $return['error'] = true;
                            $return['status'] = "An error has occurred, please contact IT";
                        }
                    } else {
                        if (assignQueue($current_user->id(), $fileLocation)){
                            $return['status'] = "Queue " . $_GET['dir'] .
                                " has now been assigned to " . $current_user->username;
                        } else {
                            $return['error'] = true;
                            $return['status'] = "Failed to assign you to the queue";
                        }
                    }
                } else {
                    if (assignQueue($current_user->id(), $fileLocation)){
                        $return['status'] = "Queue " . $_GET['dir'] .
                            " has now been assigned to " . $current_user->username;
                    } else {
                        $return['error'] = true;
                        $return['status'] = "Failed to create new assigned file and assign you to the queue";
                    }
                }
            } else {
                $return['status'] = "Open directory";
            }

            echo json_encode($return);

            break;

        case "unassign":
            $return = [
                "status" => false,
                "error" => false
            ];

            $directory = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['dir'];
            $fileLocation = $directory . "/assigned.json";

            if (!in_array($_GET['dir'], [96, 98, 99])) {
                if (file_exists($fileLocation)) {
                    $assigned = json_decode(file_get_contents($fileLocation));

                    $assigned->user = null;
                    $assigned->time = null;

                    if (file_put_contents($fileLocation, json_encode($assigned))){
                        $return['status'] = "Queue successfully unassigned";
                    } else {
                        $return['status'] = "Failed to unassign queue";
                    }
                } else {
                    $return['error'] = true;
                    $return['status'] = "An error has occurred. Please contact IT";
                }
            }

            echo json_encode($return);

            break;

        case "unassign_spot_check":
            $return = [
                "status" => false,
                "error" => false
            ];

            $directory = CORRESPONDENCE_HOLDING_DIR . $_GET['dir'];
            $fileLocation = $directory . "/assigned.json";

            if (!in_array($_GET['dir'], [96, 98, 99])) {
                if (file_exists($fileLocation)) {
                    $assigned = json_decode(file_get_contents($fileLocation));

                    $assigned->user = null;
                    $assigned->time = null;

                    if (file_put_contents($fileLocation, json_encode($assigned))){
                        $return['status'] = "Queue successfully unassigned";
                    } else {
                        $return['status'] = "Failed to unassign queue";
                    }
                } else {
                    $return['error'] = true;
                    $return['status'] = "An error has occurred. Please contact IT";
                }
            }

            echo json_encode($return);

            break;

        case "reassign":
            $return = [
                "status" => false,
                "error" => false
            ];

            $current_dir = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['current_dir'];
            $new_dir = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['new_dir'];

            $no_of_files = sizeof($_GET['files']);
            $file_counter = 0;

            foreach ($_GET['files'] as $key => $file) {
                // firstly check file still exists
                if (file_exists($current_dir . "/" . $file)) {
                    // move file to specified queue
                    if (rename($current_dir . "/" . $file, $new_dir . "/" . $file)) {
                        $file_counter++;
                    } else {
                        $return['error'] = true;
                        $return['status'] = $file . "  was unable to be moved.";
                        echo json_encode($return);
                        exit();
                    }
                } else {
                    $return['error'] = true;
                    $return['status'] = $file . "  no longer exists.";
                    echo json_encode($return);
                    exit();
                }
            }

            // check all files were successfully moved
            if ($no_of_files == $file_counter) {
                // determine wording for user feedback based on number of items moved
                if ($file_counter == 1) {
                    $return['status'] = "Correspondence item successfully moved to queue " . $_GET['new_dir'] . ".";
                } else {
                    $return['status'] = "All items of correspondence have been successfully moved to queue "
                        . $_GET['new_dir'] . ".";
                }
            } else {
                $return['error'] = true;
                $return['status'] = "One or more correspondence items have failed to move,
                 please try again or contact IT.";
            }

            echo json_encode($return);

            break;

        case "upload_form":
            $policy = new Policy($_GET['object_index'], true);
            $client = new Client($policy->client(), true);
            $partner = new Partner($client->partner(), true);
            $c = new Correspondence();

            $letter = !empty($_GET['life_event']);

            echo new Template(
                "correspondence/upload_form.html", compact([
                    "policy",
                    "client",
                    "partner",
                    "c",
                    "letter"
                ])
            );

            break;

        case "proreport_upload":
            $json = [
                "status" => false,
                "error" => false,
                "dir" => false,
                "counter" => false
            ];

            try {
                if (isset($_FILES["file"])) {
                    //Filter the file types , if you want.
                    if ($_FILES["file"]["error"] > 0) {
                        $json['error'] = true;
                        $json['status'] = "Error: " . $_FILES["file"]["error"] . " ";
                        echo json_encode($json);
                        exit();
                    }

                    $timestamp = $_GET['timestamp'];

                    $output_dir = PROREPORT_PENDING_SCAN_DIR."/". date('Y-m-d')."/".$timestamp."/";

                    if (!file_exists($output_dir)) {
                        $oldmask = umask(0);
                        if (!mkdir($output_dir, 0777, true) && !is_dir($output_dir)) {
                            throw new RuntimeException(sprintf('Directory "%s" was not created', $output_dir));
                        }
                        umask($oldmask);
                    }

                    $file_order = explode(',', $_GET['file_order']);

                    foreach ($file_order as $key => $item) {
                        if ((htmlspecialchars($_FILES["file"]["name"]) === $item) && file_exists($_FILES["file"]["tmp_name"])) {
                            if (move_uploaded_file($_FILES["file"]["tmp_name"], $output_dir.$key.".pdf")) {
                                $json['counter'] = true;
                            } else {
                                $json['counter'] = false;
                                $json['error'] = "Failed to upload file, Please contact IT.";
                                echo json_encode($json);
                                exit();
                            }
                        }
                    }

                    $json['status'] = "Uploaded File :".$_FILES["file"]["name"];
                    $json['dir'] = $timestamp;

                    echo json_encode($json);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            break;

        case "proreport_creation":

            $json = [
                "status" => false,
                "error" => false,
                "filename" => false,
                "filepath" => false,
                "preview" => false
            ];

            try {
                $preview = $_GET['preview'];

                $dm = $_GET['delivery_method'];
                $do = $_GET['delivery_option'];

                $pr_cost = (isset($_GET['pr_cost'])) ? preg_replace('/\s+/', '', $_GET['pr_cost']) : 0;
                $invoice_id = (isset($_GET['invoice_id'])) ? preg_replace('/\s+/', '', $_GET['invoice_id']) : 0;
                $pr_wf = (isset($_GET['pr_wf'])) ? $_GET['pr_wf'] : '0';

                // create array for getting page count
                $output = [];

                // function to calculate next multiple of 4 from current page number
                function roundUpToAny($n, $x = 4)
                {
                    return (ceil($n) % $x === 0) ? ceil($n) : round(($n + $x / 2) / $x) * $x;
                }

                $temp_dir = PROREPORT_PENDING_SCAN_DIR . date('Y-m-d') . "/" . $_GET['dir'] . "/";

                $delivery_methods = [
                    "Myps Upload Only" => "Myps",
                    "Deliver to Partner" => "Partner",
                    "Deliver to Client" => "Client"
                ];

                $delivery_options = [
                    "--" => "Myps",
                    "Standard Delivery" => "2nd",
                    "Express Delivery" => "1st",
                    "Email" => "Email"
                ];

                $combined_options = [
                    "undefined" => "0",
                    "individually" => "0",
                    "together" => "1"
                ];

                $delivery_method = $delivery_methods[$dm];
                $delivery_option = $delivery_options[$do];
                $client_option = $_GET['recipient'];
                $combined_option = $combined_options[$_GET['postage']];

                if (strtolower($delivery_option) === "email") {
                    $delivery_method = "Myps";
                }

                $output_dir = PROREPORT_PROCESSED_SCAN_DIR . date('Y-m-d') . "/" . $delivery_method . "/" . $delivery_option . "/";

                //get relevant objects
                $po = new Policy($_GET['policy_id'], true);
                // need to declare before Jaspersoft
                $cr = new Correspondence();
                $cl = new Client($po->client(), true);
                $par = new Partner();

                $recipient_array = [];
                $recipient_array['client'] = $cl->id();

                if (!filter_var($preview, FILTER_VALIDATE_BOOLEAN)) {
                    $i = 0;
                    foreach (explode(',', $_GET['trustee_ids']) as $key => $trustee) {
                        if (isset($trustee) && $trustee != "") {
                            // we want to create a report for each trustee
                            $recipient_array['trustee_' . $i] = $trustee;
                            $i++;
                        }
                    }
                }

                if (in_array($_GET['subject'], ["Portfolio Valuation", "Portfolio Valuation - Elevate Only"]) && isset($_GET['portfolio_value']) && $_GET['portfolio_value'] != "") {
                    $v = new Valuation();
                    $v->object_id($po->client->id);
                    $v->object_type("Client");
                    $v->value(str_replace(',', '', trim($_GET['portfolio_value'])));
                    $v->value_at(date("U"));
                    $v->comments("From Correspondence Upload");
                    $v->currency($_GET['portfolio_value_currency']);
                    $v->save();
                }

                foreach ($recipient_array as $recipient_type => $recipient_id) {
                    // reset (used in counter)
                    $output = false;

                    if (file_exists($temp_dir)) {
                        $files = scandir($temp_dir);
                        $files = glob($temp_dir . '*.{pdf}', GLOB_BRACE);

                        $file_order = [];

                        $i = 0;

                        foreach ($files as $file) {
                            $file_order[$i] = $file;
                            $i++;
                        }

                        $input = implode(' ', $file_order);

                        $form_values = [];
                        $form_values["clientid"] = $po->client->id;
                        $form_values["client_id"] = $po->client->id;
                        $form_values["user_id"] = User::get_default_instance('id');

//                        // if recipient = 0 (ie going to partner or upload only, then we use joint client details for addressee)
//                        if ($client_option === 0) {
//                            $client_option = 3;
//                        }

                        // otherwise addressee will be based upon user input
                        $form_values['addressee'] = $client_option;
                        $form_values['salutation'] = trim($_GET['salutation']);

                        $subject = $_GET['subject'];

                        $message = ($_GET['message'] == "undefined") ?
                            "We are pleased to enclose correspondence that we have received for the above client for your attention." :
                            $_GET['message'];

                        $date = date("Y-m-d H:i:s");

                        if (!filter_var($preview, FILTER_VALIDATE_BOOLEAN)) {

                            $pr_corr = new Correspondence();
                            $pr_corr->policy($po->id);
                            $pr_corr->client($po->client->id);
                            $pr_corr->partner($po->client->partner->id);
                            $pr_corr->date($date);
                            $pr_corr->message(addslashes($message));
                            $pr_corr->sender(User::get_default_instance('id'));
                            $pr_corr->subject(addslashes($subject));
                            $pr_corr->traditional_scan(1);
                            $pr_corr->dragdropflag(1);

                            if ($pr_corr->save()) {
                                $corr_id = $pr_corr->id();
                            } else {
                                $json['error'] = true;
                                $json['status'] = "Correspondence item failed to insert to database, contact IT.";
                                echo json_encode($json);
                                exit();
                            }

                            if ($po->status() !== $_GET['policyStatus']) {
                                $po->status($_GET['policyStatus']);
                                $po->save();
                            }

                            if ($po->type() !== $_GET['policyType']) {
                                $po->type($_GET['policyType']);
                                $po->save();
                            }

                            $form_values['ITEM_ID'] = $json['id'] = $corr_id;

                            if (empty($corr_id)) {
                                $json['error'] = true;
                                $json['status'] = "Correspondence ID not set, 'Error: proreport_creation case', contact IT.";
                                echo json_encode($json);
                                exit();
                            }

                        } else {
                            $corr_id = $form_values['ITEM_ID'] = $json['id'] = random_string(10);
                        }

                        if ($_GET['subject'] === 'Portfolio Valuation - Virtue Money') {
                            // VM Report
                            $front_uri = '/reports/proreport/proreport_front_vm';
                            $intro_uri = "/reports/proreport/proreport_intro_1_1";
                            $id_uri = "/reports/proreport/income_disclosure_1";
                            $back_uri = '/reports/proreport/proreport_back_vm';
                            $notes_uri = '/reports/proreport/proreport_notes_vm';
                        } else {
                            // PS Report

                            // generate ProReport front cover
                            if ($delivery_option === "Email") {
                                // use email jasper
                                $front_uri = "/reports/proreport/proreport_front_2";
                            } else if (strpos($recipient_type, 'trustee_') !== false) {
                                // use trustee copy jasper
                                $form_values["trusteeid"] = $recipient_id;
                                $front_uri = "/reports/proreport/proreport_front_1";
                            } else {
                                // use client jasper
                                $front_uri = "/reports/proreport/proreport_front";
                            }

                            // generate ProReport intro letter
                            if($_GET['subject'] === 'Portfolio Valuation - Elevate Only') {
                                $intro_uri = "/reports/proreport/proreport_intro_elevate";
                            } else {
                                if (strpos($recipient_type, 'trustee_') !== false) {
                                    // use trustee copy jasper
                                    $form_values["trusteeid"] = $recipient_id;
                                    $intro_uri = "/reports/proreport/proreport_intro_1";
                                } else {
                                    // use client jasper
                                    $intro_uri = "/reports/proreport/proreport_intro";
                                }
                            }

                            // income disclosure
                            $id_uri = "/reports/proreport/income_disclosure";
                            $back_uri = "/reports/proreport/proreport_back";
                            $notes_uri = "/reports/proreport/proreport_notes";
                        }

                        // generate Front page
                        $front = jasper_generate($front_uri, 'pdf', null, null, $form_values, "PR_Front.pdf");
                        // create front cover
                        create_file($temp_dir, "PR_Front.pdf", $front);
                        $front_cover = $temp_dir . "PR_Front.pdf";

                        // generate intro page
                        $intro = jasper_generate($intro_uri, 'pdf', null, null, $form_values, "PR_Intro.pdf");
                        // create intro page
                        create_file($temp_dir, "PR_Intro.pdf", $intro);
                        $intro_page = $temp_dir . "PR_Intro.pdf";
                        // add intro letter before our two uploaded files
                        array_unshift($file_order, $intro_page);

                        // create blank page used for print versions
                        $blank = jasper_generate("/reports/proreport/proreport_front");

                        create_file($temp_dir, "PR_blank.pdf", $blank);
                        $blank_page = $temp_dir . "PR_blank.pdf";

                        if ($delivery_method !== "Myps" && $delivery_option !== "Email") {
                            // add blank page before intro letter
                            array_unshift($file_order, $blank_page);
                        }

                        // add front cover before blank page
                        array_unshift($file_order, $front_cover);

                        // generate ProReport income disclosure letter
                        $disclose = jasper_generate($id_uri, 'pdf', null, null, $form_values, "PR_income_disclosure.pdf");
                        // create disclosure letter
                        create_file($temp_dir, "PR_income_disclosure.pdf", $disclose);
                        $disclosure = $temp_dir . "PR_income_disclosure.pdf";

                        // ensure page is not blank
                        if (filesize($disclosure) > 1000) {
                            // add income disclosure page
                            $file_order[] = $disclosure;
                        }

                        // generate back cover
                        $back = jasper_generate($back_uri, 'pdf', null, null, $form_values, "PR_back.pdf");
                        // create back cover
                        create_file($temp_dir, "PR_back.pdf", $back);
                        $back_page = $temp_dir . "PR_back.pdf";

                        if ($delivery_method !== "Myps" && $delivery_option !== "Email") {
                            // add blank page before intro letter
                            $file_order[] = $blank_page;
                        }

                        // add back cover to end of array
                        $file_order[] = $back_page;

                        if ($_GET['subject'] !== 'Portfolio Valuation - Virtue Money') {
                            // PS report

                            $page_count = 0;
                            // lets count all pages
                            foreach ($file_order as $file) {
                                $file = str_replace(['\/', '//', '\\'], '/', $file);

                                exec(
                                    'gs -q -c \'(' . $file . ')\' \'(r)\' file runpdfbegin pdfpagecount = quit',
                                    $output,
                                    $return_code
                                );
                            }

                            $page_count = array_sum($output);

                            // check if multiple of 4 for printing
                            if ($page_count % 4 !== 0) {
                                // $count is not a multiple of 4, round to next multiple
                                $count_needed = roundUpToAny($page_count);

                                // generate notes page
                                $notes = jasper_generate($notes_uri, 'pdf', null, null, $form_values, "PR_notes.pdf");
                                // create disclosure letter
                                create_file($temp_dir, "PR_notes.pdf", $notes);
                                $notes_page = $temp_dir . "PR_notes.pdf";

                                if ($delivery_method !== "Myps" && $delivery_option !== "Email") {
                                    // remove back cover for now
                                    array_pop($file_order);
                                    // remove blank page for now
                                    array_pop($file_order);

                                    $notes_needed = $count_needed - $page_count;

                                    // add each notes page that is needed
                                    for ($i = 0; $i < $notes_needed; $i++) {
                                        $file_order[] = $notes_page;
                                    }

                                    // add blank cover back to end of array
                                    $file_order[] = $blank_page;
                                    // add back cover back to end of array
                                    $file_order[] = $back_page;
                                }
                            }

                            /*
                            * lets double check that our count of pages is a multiple of 4 as we may
                            * now have notes pages
                            */

                            $page_count_two = 0;
                            $output_two = [];

                            foreach ($file_order as $file) {
                                $file = str_replace(['\/', '//', '\\'], '/', $file);

                                // let ghost script works its magic and count pages
                                exec(
                                    'gs -q -c \'(' . $file . ')\' \'(r)\' file runpdfbegin pdfpagecount = quit',
                                    $output_two,
                                    $return_code
                                );
                            }

                            $page_count_two = array_sum($output_two);

                            if (!filter_var($preview, FILTER_VALIDATE_BOOLEAN)) {
                                $db_ninja = new mydb();
                                $db_ninja->query("SELECT * FROM " . NINJA_INVOICES . " WHERE po_number = " . $pr_wf);

                                if ($invoice = $db_ninja->next(MYSQLI_ASSOC)) {
                                    $db = new mydb();
                                    $q = "INSERT INTO feebase.proreport_invoicing (corrID, amount, `date`, client, combined, workflow_id, invoice_id) VALUES ('" . $corr_id . "','" . $invoice['balance'] . "','" . time() . "', '" . $client_option . "', '" . $combined_option . "', '" . $pr_wf . "', '" . $invoice['id'] . "')";
                                    if (!$db->query($q)) {
                                        $json['error'] = true;
                                        $json['status'] = "Portfolio Report successfully created, cost not recorded, contact IT";
                                        echo json_encode($json);
                                        exit();
                                    }
                                } else {
                                    $json['error'] = true;
                                    $json['status'] = "Portfolio Report successfully created, couldn't match workflow ID on invoice ninja, contact IT";
                                    echo json_encode($json);
                                    exit();
                                }
                            }
                        } else {
                            // VM report, add a notes page

                            // generate notes page
                            $notes = jasper_generate($notes_uri, 'pdf', null, null, $form_values, "PR_notes.pdf");
                            // create disclosure letter
                            create_file($temp_dir, "PR_notes.pdf", $notes);
                            $notes_page = $temp_dir . "PR_notes.pdf";

                            // remove back cover for now
                            array_pop($file_order);

                            // add a single notes page
                            $file_order[] = $notes_page;
                            // add back cover back to end of array
                            $file_order[] = $back_page;
                        }

                        if ((($page_count_two % 4 === 0 && $delivery_method !== "Myps" && $delivery_option !== "Email")
                            || ($delivery_method === "Myps" || $delivery_option === "Email")) ||
                            $_GET['subject'] === 'Portfolio Valuation - Virtue Money') {

                            // specify directory to place new file in
                            $corr_file = $temp_dir . "temp_proreport" . ".pdf";

                            // let ghost script works its magic
                            $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH  -sOutputFile=$corr_file ";

                            //Add each pdf file to the end of the command
                            foreach ($file_order as $file) {
                                $file = str_replace(['\/', '//'], '/', $file);
                                $cmd .= $file . " ";
                            }

                            // merge and create final pdf
                            exec($cmd);

                            // check final pdf has ben created
                            if (file_exists($corr_file)) {

                                if (!file_exists($output_dir) && $delivery_method !== 'Myps') {
                                    $oldmask = umask(0);
                                    if (!mkdir($output_dir, 0777, true) && !is_dir($output_dir)) {
                                        throw new RuntimeException(sprintf('Directory "%s" was not created', $output_dir));
                                    }
                                    umask($oldmask);
                                }

                                $output_dir = str_replace(['\/', '//'], '/', $output_dir);

                                if (!filter_var($preview, FILTER_VALIDATE_BOOLEAN)) {
                                    // full produced report
                                    $destination = CORRESPONDENCE_PROCESSED_SCAN_DIR . $corr_id . ".pdf";
                                    $output_dir_filename = $output_dir . $corr_id . ".pdf";

                                    if (in_array($delivery_method, ['Partner', 'Client']) && $_GET['subject'] !== 'Portfolio Valuation - Virtue Money') {
                                        // move from pending PR folder to processed PR folder for CFH upload
                                        if (rename($corr_file, $output_dir_filename)) {
                                            if (!file_exists(PROREPORT_PENDING_SCAN_DIR)) {
                                                $oldmask = umask(0);
                                                if (!mkdir($concurrentDirectory = PROREPORT_PENDING_SCAN_DIR, 0777, true) && !is_dir($concurrentDirectory)) {
                                                    throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                                                }
                                                umask($oldmask);
                                            }
                                        } else {
                                            $json['error'] = true;
                                            $json['status'] = "Could not move merged Portfolio Report to processed directory, contact IT.";
                                            echo json_encode($json);
                                            exit();
                                        }
                                    }

                                    // move a copy to our upload directory
                                    if (copy($corr_file, $destination)) {
                                        if ($recipient_type === 'client') {
                                            $json['filepath'] = $destination;
                                            $json['url'] = $destination;
                                            $json['error'] = false;
                                            $json['status'] = "Portfolio Valuation successfully created.";
                                        }
                                    } else {
                                        $json['error'] = true;
                                        $json['status'] = "Portfolio Report created but not moved to upload folder, contact IT.";
                                        echo json_encode($json);
                                        exit();
                                    }
                                } else {
                                    // preview
                                    $output_dir_filename = $destination = $corr_file;

                                    $json['preview'] = true;
                                    $json['id'] = $corr_id;
                                    $json['url'] = $destination;
                                    $json['status'] = "Portfolio Report preview loading....";
                                }

                            } else {
                                $json['error'] = true;
                                $json['status'] = "An error occurred creating Portfolio Report during merge (" . $corr_file . "), Contact IT.";
                                echo json_encode($json);
                                exit();
                            }
                        } else {
                            $json['error'] = true;
                            $json['status'] = "Report not multiple of 4 after notes pages, contact IT.";
                            echo json_encode($json);
                            exit();
                        }
                    }
                }

            } catch (Exception $e) {
                $json['error'] = $e -> getMessage();
            }

            // tidy up pending folder
            if (!$json['preview'] && isset($temp_dir)) {
                // remove files from this loop
                removePRFiles($temp_dir);
                removeDirectory($temp_dir);
            }

            echo json_encode($json);

            break;

        case "add_corr_pr":

            $json = [
                "query" => false
            ];

            $delivery_methods = [
                "Myps Upload Only" => "Myps",
                "Deliver to Partner" => "Partner",
                "Deliver to Client" => "Client"
            ];

            $delivery_options = [
                "--" => "Myps",
                "Standard Delivery" => "2nd",
                "Express Delivery" => "1st",
                "Email" => "Email"
            ];

            $combined_options = [
                "undefined" => "0",
                "individually" => "0",
                "together" => "1"
            ];

            $delivery_method = $delivery_methods[$_GET['delivery_method']];
            $delivery_option = $delivery_options[$_GET['delivery_option']];
            $client_option = $_GET['recipient'];
            $combined_option = $combined_options[$_GET['postage']];

            if (strtolower($delivery_option) === "email"){
                $delivery_method = "Myps";
            }

            $output_dir = PROREPORT_PROCESSED_SCAN_DIR.date('Y-m-d')."/".$delivery_method."/".$delivery_option."/";
            $pending_dir = pathinfo($_GET['url'])['dirname'];
            $pending_file = $_GET['url'];

            $po = new Policy($_GET['policy_id'], true);
            $cl = new Client($po->client(), true);
            $message = $_GET['message'];
            $subject = $_GET['subject'];
            $date = date("Y-m-d H:i:s");

            $co = new Correspondence();
            $co->policy($po->id);
            $co->client($po->client->id);
            $co->partner($po->client->partner->id);
            $co->date($date);
            $co->message($message);
            $co->sender(User::get_default_instance("id"));
            $co->subject($subject);
            $co->traditional_scan(1);
            $co->dragdropflag(1);
            if (! $co->save()) {
                $json['error'] = true;
                $json['status'] = "Correspondence item failed to insert to database, contact IT.";
                echo json_encode($json);
                exit();
            }

            $form_values['ITEM_ID'] = $co->id();
            $json['id'] = $co->id();
            $corr_id = $co->id();

            $destination = CORRESPONDENCE_PROCESSED_SCAN_DIR . $corr_id . ".pdf";

            if (empty($corr_id)) {
                $json['error'] = true;
                $json['status'] = "Correspondence ID not set, 'error: add_corr_pr case', contact IT.";
                echo json_encode($json);
                exit();
            }

            if ($po->status() !== $_GET['policyStatus']) {
                $po->status($_GET['policyStatus']);
                $po->save();
            }

            if ($po->type() !== $_GET['policyType']) {
                $po->type($_GET['policyType']);
                $po->save();
            }

            if ($subject !== 'Portfolio Valuation - Virtue Money') {
                // let ghost script works its magic and count pages
                exec(
                    'gs -q -c \'('.$_GET['url'].')\' \'(r)\' file runpdfbegin pdfpagecount = quit',
                    $pagecount,
                    $return_code
                );

                // remove last page from page count
                $last_page = ($pagecount[0] -1);

                $time = time()."_".User::get_default_instance('id');

                exec("gs -sDEVICE=pdfwrite -o ".$pending_dir."/removed-last-page_".$time.".pdf  -dLastPage=".$last_page." ".$_GET['url']);

                // create back cover
                $back = jasper_generate("/reports/proreport/proreport_back", 'pdf', null, null, $form_values, "PR_back.pdf");
                $back_page = $pending_dir."/".$time."_PR_back.pdf";
                create_file($pending_dir.'/', $time."_PR_back.pdf", $back);

                // add back page back on to report
                exec("gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=".$pending_dir."/".$time."_updated_back.pdf -dBATCH ".$pending_dir."/removed-last-page_".$time.".pdf ".$back_page);
                $pending_file = $pending_dir."/".$time."_updated_back.pdf";
            }

            // move from pending PR folder to processed PR folder
            if (copy($pending_file, $destination)) {
                $json['error'] = true;
                $json['status'] = "Portfolio Valuation created but not moved to " . $destination . ", contact IT.";
            }

            // move PS Delivery Report file from pending folder to CFH upload folder
            if (($subject !== 'Portfolio Valuation - Virtue Money') && ($delivery_method !== 'Myps') &&
                ! copy($pending_dir . "/" . $time . "_updated_back.pdf", PROREPORT_PROCESSED_SCAN_DIR . date('Y-m-d') . "/" . $delivery_method . "/" . $delivery_option . "/" . $corr_id . ".pdf")) {
                $json['error'] = true;
                $json['status'] = "Portfolio Valuation Uploaded to myPSaccount but not copied for the mailing house, contact IT";
                echo json_encode($json);
                exit();
            }

            if ($subject !== 'Portfolio Valuation - Virtue Money') {
                $db_ninja = new mydb();
                $db_ninja->query("SELECT * from ".NINJA_INVOICES." where po_number = ".$_GET['pr_wf']);

                if ($invoice = $db_ninja->next(MYSQLI_ASSOC)) {
                    $db = new mydb();
                    $q = "INSERT INTO feebase.proreport_invoicing (corrID, amount, `date`, client, combined, workflow_id, invoice_id) VALUES ('" . $corr_id . "','" . $invoice['balance'] . "','" . time() . "', '" . $client_option . "', '" . $combined_option . "', '" . $_GET['pr_wf'] . "', '" . $invoice['id'] . "')";
                    $json['query'] = $q;
                    $db->query($q);
                } else {
                    $json['error'] = true;
                    $json['status'] = ($delivery_method !== 'Myps') ?
                    "Portfolio Report successfully created, file will be uploaded to myPSaccount and Mailing House" :
                    "Portfolio Report successfully created, file will be uploaded to myPSaccount.";
                    $json['status'] .= "<br />Couldn't match workflow ID on invoice ninja, Please contact IT to link/check manually";
                    $json['trigger'] = "alert";
                    echo json_encode($json);
                    exit();
                }
            }

            $json['url'] = $destination;
            $json['error'] = false;
            $json['status'] = "Portfolio Valuation successfully created.";
//                        TODO: sending emails for portfolio reports is currently under review
//                    // send the partner and all viewers an email
//                        $recipient = [];
//
//                        $s = new Search(new WebsiteUser());
//
//                        $s->eq("partner", $po->client->partner());
//                        $s->eq("proreport_notification", 1);
//
//                        while($wu = $s->next(MYSQLI_ASSOC)){
//                            $recipient[] = [
//                                'address' => [
//                                    'email' => $wu->email_address()
//                                ]
//                            ];
//                        }
//
//                        $s2 = new Search(new ViewerEntry());
//                        $s2->eq("partner", $po->client->partner());
//                        $s2->eq("proreport_notification", 1);
//                        $s2->eq("stealth", 0);
//
//                        while($ve = $s2->next(MYSQLI_ASSOC)){
//                            $wu = new WebsiteUser($ve->user(), true);
//
//                            $recipient[] = [
//                                'address' => [
//                                    'email' => $wu->email_address()
//                                ]
//                            ];
//                        }
//
//                        if (!empty($recipient)){
//                            if(date("H") >= 18){
//                                $ampm = "Evening";
//                            } elseif(date("H") >= 12){
//                                $ampm = "Afternoon";
//                            } else {
//                                $ampm = "Morning";
//                            }
//
//                            // Send email to user with their username and password
//                            $local_template['html'] = new Template("sparkpost/emailTemplates/portfolioReport/uploaded.html", [
//                                "client" => $po->client,
//                                "ampm" => $ampm
//                            ]);
//
//                            $from = [
//                                'name' => 'Policy Services - Portfolio Reports',
//                                'email' => 'portfolioreports@policyservices.co.uk',
//                            ];
//
//                            if(!sendSparkEmail($recipient, "Portfolio Report Uploaded", "", false, $from, [], $local_template)){
//                                $json['status'] .= " Failed to send email notifications.";
//                            }
//                        }

            // tidy up pending folder
            if (isset($pending_dir)) {
                // remove files from this loop
                removePRFiles($pending_dir);
                removeDirectory($pending_dir);
            }

            echo json_encode($json);

            break;

        case "upload":
            $json = [
                "status" => false,
                "error" => false,
                "dir" => false,
                "counter" => false
            ];

            try {
                if (isset($_FILES["file"])) {
                    //Filter the file types , if you want.
                    if ($_FILES["file"]["error"] > 0) {
                        $json['error'] = true;
                        $json['status'] = "Error: " . $_FILES["file"]["error"] . " ";
                        echo json_encode($json);
                        exit();
                    } else {
                        $temp_dir = CORRESPONDENCE_PENDING_SCAN_DIR;
                        $timestamp = User::get_default_instance('id').$_GET['timestamp'];
                        $output_dir = $temp_dir.$timestamp;

                        if (!file_exists($output_dir)) {
                            $oldmask = umask(0);
                            mkdir($output_dir, 0777, true);
                            umask($oldmask);
                        }

                        $output_dir .= "/";

                        $file_order = explode(',', $_GET['file_order']);

                        foreach ($file_order as $key => $item) {
                            // If file name doesn't match, send back error.
                            if (htmlspecialchars($_FILES["file"]["name"]) == $item) {
                                if (move_uploaded_file($_FILES["file"]["tmp_name"], $output_dir . $key . ".pdf")) {
                                    $json['counter'] = true;
                                } else {
                                    $json['counter'] = false;
                                    $json['error'] = "Failed to upload file, Please contact IT.";
                                    echo json_encode($json);
                                    exit();
                                }
                            }
                        }

                        $json['status'] = "Uploaded File :".$_FILES["file"]["name"];
                        $json['dir'] = $timestamp;
                    }

                    echo json_encode($json);
                } else {
                    // no files set, must be a standard letter, do required pending logic

                    $temp_dir = CORRESPONDENCE_PENDING_SCAN_DIR;
                    $timestamp = User::get_default_instance('id').$_GET['timestamp'];
                    $output_dir = $temp_dir.$timestamp."/";

                    if (!file_exists($output_dir)) {
                        $oldmask = umask(0);
                        mkdir($output_dir, 0777, true);
                        umask($oldmask);
                    }

                    $json['counter'] = true;
                    $json['status'] = true;
                    $json['dir'] = $timestamp;
                    echo json_encode($json);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }


            break;

        case "upload_spot_check":
            $json = [
                "status" => false,
                "error"  => false,
                "uid"  => false,
                "processed_from"  => false,
                "processed_to"  => false
            ];

            $queue = $_GET['queue'];

            if ($_GET['queue'] != 0) {
                $dir = opendir(CORRESPONDENCE_HOLDING_DIR . $queue);

                $files = [];
                while ($file = readdir($dir)) {
                    // We are only interested in the PDF files in the directory
                    if (strpos($file, '.pdf')) {
                        //move this file
                        rename(CORRESPONDENCE_HOLDING_DIR . $queue . "/" . $file, CORRESPONDENCE_PROCESSED_SCAN_DIR . $file);
                        $files[] = $file;
                    }
                }

                //create report and print
                $directory = CORRESPONDENCE_PENDING_SCAN_DIR . $queue;
                $file = $directory."/assigned.json";

                // remove all json files
                $json_directory = CORRESPONDENCE_PENDING_SCAN_DIR.$queue."/output-json";
                if (file_exists($json_directory)) {
                    if ($handle = opendir($json_directory)) {
                        while (false !== ($json_file = readdir($handle))) {
                            if (preg_match('/\.json/i', $json_file)) {
                                unlink($json_directory ."/". $json_file);
                            }
                        }
                    }
                }

                $json['processed_to'] = date("Y-m-d H:i:s");

                if (!in_array($queue, [96, 98, 99])){
                    if (file_exists($file)) {
                        $assigned = json_decode(file_get_contents($file));

                        if ($assigned->time){
                            $formatted_datetime_from = date("Y-m-d H:i:s", $assigned->time);
                            $json['uid'] = $assigned->user;
                            $json['processed_from'] = $formatted_datetime_from;
                        } else {
                            $json['error'] = true;
                            $json['status'] = "Failed to find start time, please run report manually.";
                        }

                        // clear assigned files
                        $scAssignedFile = CORRESPONDENCE_HOLDING_DIR . $queue . "/assigned.json";

                        unlink($scAssignedFile);
                        unlink($file);
                    } else {
                        $json['status'] = "File does not exist. Contact IT.";
                        $json['error'] = true;
                    }
                }
            } else {
                //invalid queue
                $json["error"] = true;
                $json["status"] = $queue . " is an invalid queue!";
            }

            echo json_encode($json);

            break;

        case "merge":
            $json = [
                "status" => false,
                "error" => false,
                "filename" => false,
                "filepath" => false
            ];

            try {
                $policy = new Policy();

                $temp_dir = CORRESPONDENCE_PENDING_SCAN_DIR.$_GET['dir']."/";
                $output_dir = CORRESPONDENCE_PROCESSED_SCAN_DIR;

                if (file_exists($temp_dir)) {
                    $files = scandir($temp_dir);
                    $files = glob($temp_dir.'/*.{pdf}', GLOB_BRACE);

                    $file_order = [];
                    $i = 0;

                    foreach ($files as $file) {
                        $file_order[$i] = $file;
                        $i++;
                    }

                    $input = implode(' ', $file_order);

                    //get relevant objects
                    $po = new Policy($_GET['policy_id'], true);
                    $cl = new Client($po->client(), true);
                    $pa = new Partner($cl->partner(), true);
                    $letter = new Letter($_GET['letter_type'], true);

                    // block correspondence from untraced account
                    if ($pa->id() == 394 && ($_GET['letter_type'] != 15 && $_GET['letter_type'] != 37)) {
                        $json['error'] = true;
                        $json['status'] = "Correspondence cannot be added under the untraced account (394)";
                        echo json_encode($json);
                        exit();
                    }

                    if ($pa->id() == 5527) {
                        if (!in_array(User::get_default_instance('id'), [60, 44, 201, 248]) && !User::get_default_instance('department') == 4) {
                            $json['error'] = true;
                        $json['status'] = "All correspondence for John Scarratt's account must be passed to Kayleigh";
                            echo json_encode($json);
                            exit();
                        }
                    }

                    if ($_GET['subject'] ==  "undefined" || $_GET['subject'] == "default") {
                        $subject = $letter->name();
                    } else {
                        $subject = $_GET['subject'];
                    }

                    if ($_GET['message'] ==  "undefined") {
                        $message = "We are pleased to enclose correspondence that we have received for the above client for your attention.";
                    } else {
                        $message = $_GET['message'];
                    }

                    //Add new correspondence item
                    $cr = new Correspondence();
                    $cr->client($cl->id);
                    $cr->partner($pa->id);
                    $cr->policy($po->id);
                    $cr->subject($subject);
                    $cr->message($message);
                    if ($_GET['letter_type'] != "--") {
                        $cr->letter($_GET["letter_type"]);
                        $letter_type = $_GET["letter_type"];
                    } else {
                        $letter_type = 6;
                    }
                    $cr->traditional_scan(1);
                    $cr->dragdropflag(1);
                    $cr->save();


//                    if ($cr->subject == "Portfolio Valuation - Non PS") {
//
//                        /* if this is not the partners first 'own' report for this client then we wish to end the
//                       current chase partner for report ticket */
//                        $s = new Search( new Workflow );
//                        $s -> eq('step', 60);
//                        $s -> eq('_object', 'Client');
//                        $s -> eq('index', $cl->id);
//                        while ($ticket = $s->next(MYSQLI_ASSOC)) {
//                            //ob_start();
//                            $old_wf = new Workflow($ticket->id, true);
//                            $old_wf->workflow_next(
//                                false,
//                                1,
//                                'Partner doing own reports for this client'
//                            );
//                            $ticket_ended = ob_get_flush();
//                            ob_end_clean();
//                        }
//
//                        /* we want to end current valuation workflow and create a new WF ticket to chase partner
//                        in 1 year */
//                        $wf_array = [
//                            'process' => '27',
//                            'step' => '60',
//                            'priority' => '2',
//                            'due' => time() + 31556926,
//                            'desc' => 'Chase partner for equivalent report to our Portfolio reports',
//                            '_object' => 'Client',
//                            'index' => $cl->id,
//                        ];
//
//                        /* Messy!! Output workflow details into buffer and then clear buffer -> stops confusion when
//                        redirect to partner profile on addition */
//                        $ticket_added = false;
//                        ob_start();
//                        $wf = new Workflow();
//                        $wf->workflow_add($wf_array, false, false, true);
//
//                        // get response
//                        $ticket_added = ob_get_contents();
//                        ob_end_clean();
//                        $ticket_added_array = json_decode($ticket_added);
//
//                        if ($ticket_added_array->status) {
//                            /* update timestamp for next report */
//                            if (isset($_GET['pv_mandatory'])) {
//                                $cl->pv_mandatory_start(strtotime($_GET['pv_mandatory']));
//                            } else {
//                                $cl->pv_mandatory_start(time());
//                            }
//                            $cl->save();
//
//                            /* now that we have added a new workflow ticket to chase partner in  year, end any current
//                            client delivery wf tickets */
//                            $s = new Search(new Workflow);
//                            $s->eq('step', [41,193]);
//                            $s->eq('_object', 'Client');
//                            $s->eq('index', $cl->id);
//                            while ($ticket = $s->next(MYSQLI_ASSOC)) {
//                                // only end client delivery tickets
//                                if (strpos($ticket->desc, 'deliver to Client') !== false) {
//                                    //ob_start();
//                                    $old_wf = new Workflow($ticket->id, true);
//                                    $old_wf->workflow_next(
//                                        false,
//                                        1,
//                                        'Partner doing own report for this client'
//                                    );
//                                    $ticket_ended = ob_get_flush();
//                                    ob_end_clean();
//                                }
//                            }
//                        }
//                    }

                    switch($cr->subject){
                        case "Portfolio Valuation - Non PS":
                            // Was previously only used for PV - non PS, after a workflow was added, moved to here as we no
                            // longer need the workflow ticket
                            if (isset($_GET['pv_mandatory'])) {
                                $cl->pv_mandatory_start(strtotime($_GET['pv_mandatory']));
                            } else {
                                $cl->pv_mandatory_start(time());
                            }
                            $cl->save();

                            break;

                        case "Ad-hoc Statement":
                        case "Annual Statement":
                        case "Quarterly Statement":
                        case "Quarterly Valuation":
                        case "Valuation":
                        case "Correspondence - Original Sent directly to client":
                        case "Correspondence - Original Sent to partner":
                            if(!empty($_GET['policy_val'])){
                                $pval = new Valuation();

                                $valuation = str_replace([",","£"], "", trim($_GET['policy_val']));

                                if(substr($valuation, -1) === "."){
                                    $valuation = substr($valuation, 0, -1);
                                }

                                $pval->object_type("Policy");
                                $pval->object_id($cr->policy());
                                $pval->value_at($_GET['policy_val_date']);
                                $pval->value($valuation);
                                $pval->comments("From Correspondence Upload");
                                $pval->currency($_GET['policy_val_currency']);

                                $pval->save();
                            }

                            break;

                        case "Client ID":
                            if(!empty($_GET['client_index'])){
                                $clientIDdoc = new ClientIDDocument();

                                $clientIDdoc->client($cr->client());
                                $clientIDdoc->index($_GET['client_index']);
                                $clientIDdoc->correspondence($cr->id());

                                $clientIDdoc->save();
                            }

                            break;

                        case "Adviser Charging Form sent to Provider":
                            $db = new mydb(REMOTE);

                            $db->query("SELECT id, object 
                                        FROM myps.action_required 
                                        WHERE subject_id = 2
                                            AND (
                                                (object = \"Client\" AND object_id = " . $cl->id() . ")
                                                OR
                                                (object = \"Policy\" AND object_id = " . $po->id() . ") 
                                            )
                                            AND completed_when is null
                                            AND archived = 0");

                            while($arid = $db->next(MYSQLI_ASSOC)){
                                $ar = new ActionRequired($arid['id'], true);

                                // add a final message to the ticket
                                $arm = new ActionRequiredMessage();

                                // am/pm check
                                if(date("H") >= 18){
                                    $ampm = "Evening";
                                } elseif(date("H") >= 12){
                                    $ampm = "Afternoon";
                                } else {
                                    $ampm = "Morning";
                                }

                                $arm->action_required_id($ar->id());
                                $arm->message("Good " . $ampm . ",
                                
We have now received an Adviser Charging form for this " . $ar->object() . " and it has been processed. We will chase the provider to ensure this is set up within 2 weeks from today. If any further action is required we will be in touch. Please be aware it can take around 8 weeks before you will see monthly payments for this policy in your account and may be slightly longer if payments are set up quarterly.
      
Kind regards,
Policy Services");

                                $arm->save();

                                // close the ticket
                                $ar->completed_when(time());
                                $ar->completed_by(User::get_default_instance("myps_user_id"));

                                $ar->save();
                            }

                            break;

                        case "GDPR Consent - Joint Reports":
                            $cl->joint_consent(1);
                            $cl->save();

                            break;
                    }

                    // get id of correspondence item just added
                    $corr = $cr->id();

                    if ($corr == 0 || is_null($corr)) {
                        $json['error'] = true;
                        $json['status'] = "Correspondence ID not set, 'Error: merge case', contact IT.";
                        echo json_encode($json);
                        exit();
                    }

                    $json['id'] = $corr;

                    //use correspondence id as file name
                    $output = $output_dir . $corr. ".pdf";

                    // do the following behaviour only if a file has been dragged and dropped
                    if (isset($files) && $_GET['letter_type'] == "--") {

                        // check for errors
                        if (isset($_FILES) && isset($_FILES["file"]) && isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {
                            $json['error'] = true;
                            $json['status'] = "Error: " . $_FILES["file"]["error"] . " ";
                        } else {
                            // get letter object based on ID
                            $letter_obj = new Letter($letter_type, true);

                            $c = new \Jaspersoft\Client\Client(
                                JASPER_HOST,
                                JASPER_USER,
                                JASPER_PASSWD,
                                ""
                            );

                            $form_values = [];
                            $form_values["ITEM_ID"] = $corr;
                            $form_values["correspondence[letter]"][] = $letter_type;

                            // get constant values for standard letter footer
                            $std_letter_foot = unserialize(STANDARD_LETTER_FOOTER);
                            foreach ($std_letter_foot as $key => $value) {
                                $form_values[$key][] = $value;
                            }

                            // point to prophet_reports dir on rosie
                            $report_url = JASPER_STANDARD_LETTERS . $letter_obj->rosie_file();

                            $report_url = Letter::url_path_corrections($report_url);

                            // check if we have any form data to pass through
                            if (!empty($form_values)) {
                                try {
                                    $report = $c->reportService()->runReport($report_url, 'pdf', null, null, $form_values);
                                } catch (Exception $e) {
                                    exit('Caught exception: ' . $e->getMessage() . "\n");
                                }
                            } else {
                                $return['error'] = true;
                                $return['status'] = "Report parameters missing, please re-run report.";
                                echo json_encode($return);
                                exit();
                            }

                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Description: File Transfer');
                            header('Content-Disposition: attachment; filename=' . $letter_obj->rosie_file() . '.pdf');
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Content-Length: ' . strlen($report));
                            header('Content-Type: application/pdf');

                            // temporary create coversheet for merging with correspondence
                            $temp_coversheet = CORRESPONDENCE_PENDING_SCAN_DIR . $corr . "_temp_coversheet.pdf";
                            file_put_contents($temp_coversheet, $report);

                            // check that temporary coversheet has been created, if so continue..
                            if (!file_exists($temp_coversheet)) {
                                $return['error'] = true;
                                $return['status'] = "An error has occurred creating coversheet, please contact IT.";
                            } else {
                                // now that we have the cover sheet we want to use ghost script to join this with our current item of correspondence
                                $correspondence = str_replace('//', '/', $files);

                                // merge files contained in this array
                                $fileArray = [$temp_coversheet];

                                $fileArray = array_merge($fileArray, $correspondence);

                                // specify directory to place new file in
                                $corr_file = CORRESPONDENCE_PROCESSED_SCAN_DIR . $corr . ".pdf";

                                // let ghost script works its magic
                                $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH  -sOutputFile=$corr_file ";

                                //Add each pdf file to the end of the command
                                foreach ($fileArray as $file) {
                                    $cmd .= $file . " ";
                                }

                                //merge and create final pdf
                                $result = exec($cmd);

                                // check final pdf has ben created
                                if (file_exists($corr_file)) {
                                    $json['url'] = $corr_file;

                                    // delete temp coversheet now that merged pdf has been created
                                    unlink($temp_coversheet);
                                    // delete all files within temp_dir
                                    array_map('unlink', glob("$temp_dir/*.*"));
                                    // remove temp_dir
                                    rmdir($temp_dir);

                                    /*** Automatically update Item's Policy to 'Servicing Transferred to PS' status if
                                     * it is still waiting for confirmation etc. ***/
                                    if (isset($po)) {
                                        if ($po->status() < 2 && $_GET['policyStatus'] == 5) {
                                            $po->status(5);
                                            if ($po->save()) {
                                                $json['status'] = "Correspondence item successfully created.";
                                            } else {
                                                $json['status'] = "File was successfully uploaded, error updating status.";
                                            }
                                        } elseif ($po->status() < 2 ) {
                                            $omission_subjects = unserialize(CORR_STATUS_OMISSION);
                                            if ((!in_array($subject, $omission_subjects))) {
                                                $po->transferred(date("Y-m-d"));
                                                $po->status(2);
                                                if ($po->save()) {
                                                    $json['status'] = "Correspondence item successfully created.";
                                                } else {
                                                    $json['status'] = "File was successfully uploaded, error updating status.";
                                                }
                                            } else {
                                                $json['status'] = "Correspondence item successfully created.";
                                            }

                                        } else {
                                            //If policy status does not match, then add new policy status to policy
                                            if ($po->status() != $_GET['policyStatus']) {
                                                $po->status($_GET['policyStatus']);
                                                if ($po->save()) {
                                                    $json['status'] = "Correspondence item successfully created.";
                                                } else {
                                                    $json['status'] = "File was successfully uploaded, error updating status.";
                                                }
                                            } else {
                                                $json['status'] = "Correspondence item successfully created.";
                                                $json['error'] = false;
                                            }
                                        }

                                        if (isset($_GET['policyType'])){
                                            if ($po->type() != $_GET['policyType']) {
                                                $po->type($_GET['policyType']);
                                                $po->save();
                                            }
                                        }
                                    }

                                } else {
                                    $json['error'] = true;
                                    $json['status'] = "An error has occurred, please try again or contact IT if error persists.";
                                }
                            }
                        }
                    } elseif ($_GET['letter_type'] != "--") {
                        // this gives us a stdClass object
                        $stdObj = json_decode($_GET['data']);

                        // gives us one array..
                        function objToarray($obj)
                        {
                            if (!is_array($obj) && !is_object($obj)) {
                                return $obj;
                            }
                            if (is_object($obj)) {
                                $obj = get_object_vars($obj);
                            }
                            return array_map(__FUNCTION__, $obj);
                        }
                        $arr = objToarray($stdObj);

                        // create multidimensional array for passing values through in expected format
                        $form_values = [];
                        foreach ($arr as $key => $value) {
                            $form_values[$key][] = $value;
                        }

                        // get letter object based on ID
                        $letter_obj = new Letter($letter_type, true);

                        $c = new \Jaspersoft\Client\Client(
                            JASPER_HOST,
                            JASPER_USER,
                            JASPER_PASSWD,
                            ""
                        );

                        $form_values["ITEM_ID"] = $corr;
                        $form_values["correspondence[letter]"][] = $letter_type;

                        // get constant values for standard letter footer
                        $std_letter_foot = unserialize(STANDARD_LETTER_FOOTER);
                        foreach ($std_letter_foot as $key => $value) {
                            $form_values[$key][] = $value;
                        }

                        if ($_GET['letter_type'] == 48) {
                            $form_values['clientid'] = $form_values['clientid']['0'];
                        }

                        // point to prophet_reports dir on rosie
                        $report_url = JASPER_STANDARD_LETTERS . $letter_obj->rosie_file();

                        $report_url = Letter::url_path_corrections($report_url);

                        // check if we have any form data to pass through
                        if (!empty($form_values)) {
                            try {
                                $report = $c->reportService()->runReport($report_url, 'pdf', null, null, $form_values);
                            } catch (Exception $e) {
                                exit('Caught exception: ' . $e->getMessage() . "\n");
                            }
                        } else {
                            $return['error'] = true;
                            $return['status'] = "Report parameters missing, please re-run report.";
                            echo json_encode($return);
                            exit();
                        }

                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Description: File Transfer');
                        header('Content-Disposition: attachment; filename=' . $letter_obj->rosie_file() . '.pdf');
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Content-Length: ' . strlen($report));
                        header('Content-Type: application/pdf');

                        // temporary create coversheet for merging with correspondence
                        $temp_coversheet = CORRESPONDENCE_PENDING_SCAN_DIR . $corr . "_temp_coversheet.pdf";
                        file_put_contents($temp_coversheet, $report);

                        // check that temporary coversheet has been created, if so continue..
                        if (!file_exists($temp_coversheet)) {
                            $return['error'] = true;
                            $return['status'] = "An error has occurred creating coversheet, please contact IT.";
                        } else {
                            // now that we have the cover sheet we want to use ghost script to join this with our current item of correspondence
                            $correspondence = str_replace('//', '/', $files);

                            // merge files contained in this array
                            $fileArray = [$temp_coversheet];

                            $fileArray = array_merge($fileArray, $correspondence);

                            // specify directory to place new file in
                            $corr_file = CORRESPONDENCE_PROCESSED_SCAN_DIR . $corr . ".pdf";

                            $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH  -sOutputFile=$corr_file ";

                            //Add each pdf file to the end of the command
                            foreach ($fileArray as $file) {
                                $cmd .= $file . " ";
                            }

                            //merge and create final pdf
                            $result = exec($cmd);

                            // check final pdf has ben created
                            if (file_exists($corr_file)) {
                                $json['url'] = $corr_file;
                                $json['error'] = false;
                                $json['status'] = "Standard Letter successfully uploaded.";

                                // delete temp coversheet now that merged pdf has been created
                                if (file_exists($temp_coversheet)){
                                    unlink($temp_coversheet);
                                }
                                // delete all files within temp_dir
                                array_map('unlink', glob("$temp_dir/*.*"));
                                // remove temp_dir
                                if (file_exists($temp_dir)){
                                    rmdir($temp_dir);
                                }

                                /*** Automatically update Item's Policy to 'Servicing Transferred to PS' status if
                                 * it is still waiting for confirmation etc. ***/
                                if (isset($po)) {
                                    if ($po->status() < 2 && $_GET['policyStatus'] == 5) {
                                        $po->status(5);
                                        if ($po->save()) {
                                            $json['status'] = "Correspondence item successfully created.";
                                        } else {
                                            $json['status'] = "File was successfully uploaded, error updating status.";
                                        }
                                    } elseif ($po->status() < 2) {
                                        $omission_subjects = unserialize(CORR_STATUS_OMISSION);
                                        if (!in_array($subject, $omission_subjects)) {
                                            $po->transferred( date("Y-m-d") );
                                            $po->status(2);
                                            if ($po->save()) {
                                                $json['status'] = "Correspondence item successfully created.";
                                                $json['error'] = false;
                                            } else {
                                                $json['status'] = "File was successfully uploaded, error updating status.";
                                            }
                                        } else {
                                            $json['status'] = "Correspondence item successfully created.";
                                            $json['error'] = false;
                                        }
                                    } else {
                                        //If policy status does not match, then add new policy status to policy
                                        if ($po->status() != $_GET['policyStatus']) {
                                            $po->status($_GET['policyStatus']);
                                            if ($po->save()) {
                                                $json['status'] = "Correspondence item successfully created.";
                                                $json['error'] = false;
                                            } else {
                                                $json['status'] = "File was successfully uploaded, error updating status.";
                                            }
                                        } else {
                                            $json['status'] = "Correspondence item successfully created.";
                                            $json['error'] = false;
                                        }
                                    }

                                    if ($po->type() != $_GET['policyType']) {
                                        $po->type($_GET['policyType']);
                                        $po->save();
                                    }
                                }

                                /* after adding standard letter, check whether letter type is
                                Portfolio Valuation - Non Production, is so Create invoicing entry for portfolio */
                                if ($_GET['letter_type'] == 29) {
                                    $invoice_amount_clean = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $form_values['INVOICE_AMOUNT'][0]);
                                    $workflow_id_clean = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $form_values['WORKFLOW_ID'][0]);
                                    $invoice_id_clean = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $form_values['INVOICE_ID'][0]);
                                    
                                    $db_ninja = new mydb();
                                    $db_ninja->query("SELECT * from ".NINJA_INVOICES." where po_number = ".$workflow_id_clean);

                                    if ($invoice = $db_ninja->next(MYSQLI_ASSOC)) {
                                        $db = new mydb();
                                        $q = "INSERT INTO feebase.proreport_invoicing (corrID, amount, `date`,  workflow_id, invoice_id) VALUES ('".$corr."','".$invoice['balance']."','".time()."', '".$workflow_id_clean."', '".$invoice['id']."')";

                                        if ($db->query($q)) {
                                            $json['error'] = false;
                                            $json['status'] = "Standard Letter successfully uploaded.";
                                        } else {
                                            $json['error'] = false;
                                            $json['status'] = "Standard Letter successfully uploaded, invoice not linked.";
                                        }
                                    } else {
                                        $json['error'] = false;
                                        $json['status'] = "Standard Letter successfully uploaded, Could not link invoice.";
                                    }
                                } else {
                                    $json['error'] = false;
                                    $json['status'] = "Standard letter uploaded";
                                }
                            } else {
                                $json['error'] = true;
                                $json['status'] = "An error has occurred creating coversheet, please contact IT. ";
                            }
                        }
                    }
                } else {
                    $json['error'] = true;
                    $json['status'] = "Temp directory not found.";
                }
            } catch (Exception $e) {
                $json['error'] = $e -> getMessage();
            }

            echo json_encode($json);

            break;

        case "fe_q_report":
            $json = [
                "status" => false,
                "error"  => false,
                "uid"  => false,
                "processed_from"  => false,
                "processed_to"  => false
            ];

            $directory = CORRESPONDENCE_PENDING_SCAN_DIR.$_GET['dir'];
            $file = $directory."/assigned.json";

            // remove all json files
            $json_directory = CORRESPONDENCE_PENDING_SCAN_DIR.$_GET['dir']."/output-json";
            if (file_exists($json_directory)) {
                if ($handle = opendir($json_directory)) {
                    while (false !== ($json_file = readdir($handle))) {
                        if (preg_match('/\.json/i', $json_file)) {
                            unlink($json_directory ."/". $json_file);
                        }
                    }
                }
            }

            $json['processed_to'] = date("Y-m-d H:i:s");

            if (file_exists($file)) {
                $assigned = json_decode(file_get_contents($file));

                if ($assigned->time){
                    $formatted_datetime_from = DATE("Y-m-d H:i:s", $assigned->time);
                    $json['uid'] = $assigned->user;
                    $json['processed_from'] = $formatted_datetime_from;
                } else {
                    $json['error'] = true;
                    $json['status'] = "Failed to get start time, please run report manually";
                }

                // clear assigned files
                $scAssignedFile = CORRESPONDENCE_HOLDING_DIR . $_GET['dir'] . "/assigned.json";

                unlink($scAssignedFile);
                unlink($file);
            } else {
                $json['status'] = "File does not exist. Contact IT.";
                $json['error'] = true;
            }

            echo json_encode($json);

            break;

//        case "queue_start_time":
//            $json = [
//                "status" => false,
//                "error"  => false
//            ];
//
//            $directory = CORRESPONDENCE_PENDING_SCAN_DIR.$_GET['current_dir'];
//            $file = $directory."/assigned.txt";
//
//            if ($_GET['current_dir'] != 99 && $_GET['current_dir'] != 98 && $_GET['current_dir'] != 96) {
//                if (file_exists($file)) {
//                    // Open the file
//                    $file_contents = file_get_contents($file);
//                    // string to array
//                    $contents = explode("\n", $file_contents);
//                    // get second line
//                    $timestamp = $contents[1];
//
//                    $start_time = date('Y-m-d H:i:s', $timestamp);
//
//                    if ($start_time) {
//                        $json['status'] = "Queue Start time: ".$start_time;
//                        $json['error'] = false;
//                    } else {
//                        $json['status'] = "Start time missing.. Contact IT";
//                        $json['error'] = true;
//                    }
//                }
//            } else {
//                $json['status'] = "Open queue";
//                $json['error'] = false;
//            }
//
//            echo json_encode($json);
//
//            break;

        case "queue_start_time":
            $json = [
                "status" => false,
                "error"  => false
            ];

            $directory = CORRESPONDENCE_PENDING_SCAN_DIR.$_GET['current_dir'];
            $assignedFile = $directory."/assigned.json";

            if (!in_array($_GET['current_dir'], [96, 98, 99])) {
                if (file_exists($assignedFile)) {
                    // Open the file
                    $assigned = json_decode(file_get_contents($assignedFile));

                    $start_time = \Carbon\Carbon::createFromTimestamp($assigned->time)->toDayDateTimeString();

                    if ($start_time) {
                        $json['status'] = "Queue Start time: ".$start_time;
                        $json['error'] = false;
                    } else {
                        $json['status'] = "Start time missing. Contact IT";
                        $json['error'] = true;
                    }
                }
            } else {
                $json['status'] = "Open queue";
                $json['error'] = false;
            }

            echo json_encode($json);

            break;

        case "spot_check_queue_start_time":
            $json = [
                "status" => false,
                "error"  => false
            ];

            $directory = CORRESPONDENCE_HOLDING_DIR.$_GET['current_dir'];
            $assignedFile = $directory."/assigned.json";

            if (!in_array($_GET['current_dir'], [96, 98, 99])) {
                if (file_exists($assignedFile)) {
                    // Open the file
                    $assigned = json_decode(file_get_contents($assignedFile));

                    $start_time = \Carbon\Carbon::createFromTimestamp($assigned->time)->toDayDateTimeString();

                    if ($start_time) {
                        $json['status'] = "Queue Start time: ".$start_time;
                        $json['error'] = false;
                    } else {
                        $json['status'] = "Start time missing. Contact IT";
                        $json['error'] = true;
                    }
                }
            } else {
                $json['status'] = "Open queue";
                $json['error'] = false;
            }

            echo json_encode($json);

            break;

        case "empty_queue":
            $json = [
                "status" => false,
                "error"  => false
            ];

            $directory = CORRESPONDENCE_PENDING_SCAN_DIR.$_GET['dir'];
            // remove all correspondence PDFS
            if ($handle = opendir($directory)) {
                while (false !== ($file = readdir($handle))) {
                    if (preg_match('/\.pdf$/i', $file)) {
                        unlink($directory ."/". $file);
                        $json['status'] = "Queue Emptied";
                        $json['error'] = false;
                    } else {
                        $json['status'] = "Failed to empty.. Contact IT";
                        $json['error'] = true;
                    }
                }
            }

            // remove all json files
            $json_directory = CORRESPONDENCE_PENDING_SCAN_DIR.$_GET['dir']."/output-json";
            if (file_exists($json_directory)) {
                if ($handle = opendir($json_directory)) {
                    while (false !== ($json_file = readdir($handle))) {
                        if (preg_match('/\.json/i', $json_file)) {
                            unlink($json_directory ."/". $json_file);
                        }
                    }
                }
            }

            echo json_encode($json);

            break;

        case "super_search":

            $json = [
                "status" => false,
                "error" => false,
                "corr_list" => false,
                "page_no" => null,
                "pages" => null,
                "record_count" => null
            ];

            $years = 0;

            $s = new Search(new Correspondence());

            if ($_GET["from_date"] != '--') {
                $fd = $_GET["from_date"] . " 00:00:00";
            }

            if ($_GET["until_date"] != '--') {
                $ud = $_GET["until_date"] . " 23:59:59";
            }

            if (isset($ud) && isset($fd)) {
                $diff = abs(strtotime($ud) - strtotime($fd));
                $years = $diff / (365 * 60 * 60 * 24);
            }


            if ((isset($_GET["until_date"]) && $_GET["until_date"] != "--" &&
                    isset($_GET["from_date"]) && $_GET["from_date"] != "--")
                || !empty($_GET["corrid"])) {
                if ($years > 1) {
                    $json['error'] = true;
                    echo json_encode($json);
                } else {
                    // date filters
                    if (isset($fd) && isset($ud)) {
                        // if both dates in range supplied
                        $s->btw("date", $fd, $ud);
                    } elseif (isset($fd)) {
                        // if only from_date supplied
                        $s->gt("date", $fd);
                    } elseif (isset($ud)) {
                        // if only until_date supplied
                        $s->lt("date", $ud);
                    }

                    // if corrID supplied
                    if (!empty($_GET["corrid"])) {
                        $s->eq("id", $_GET["corrid"]);
                    }

                    // if clientID supplied
                    if (!empty($_GET["client"])) {
                        $s->eq("client", $_GET["client"]);
                    }

                    // if policyID supplied
                    if (!empty($_GET["policy"])) {
                        $s->eq("policy", $_GET["policy"]);
                    }

                    // if partnerID supplied
                    if (!empty($_GET["partner"])) {
                        $s->eq("partner", $_GET["partner"]);
                    }

                    // if subject supplied
                    if (!empty($_GET["subject"])) {
                        if ($_GET['subject'] != "default") {
                            $s->eq("subject", $_GET["subject"]);
                        }
                    }

                    // if issuer supplied
                    if (!empty($_GET["issuer"])) {
                        $s->eq("policy->issuer", $_GET["issuer"]);
                    }

                    // if sender supplied
                    if (!empty($_GET["sender"])) {
                        $s->eq("sender", $_GET["sender"]);
                    }

                    // if message/content keyword supplied
                    if (!empty($_GET["messagekey"])) {
                        //Added functionality for the message keywords search whilst also sanitizing the user input
                        $keyword = $_GET['messagekey'];
                        $replace = [" ", ".", ",", "-", ">", "<", "/", ":", "+", "*"];
                        $keyword = str_replace($replace, "%", $keyword);

                        $s->add_or($s->eq("message", "$keyword%", true, false));
                    }

                    // limit results to 50 records
                    if (isset($_GET['limit'])){
                        $limit = $_GET['limit'];
                    } else {
                        $limit = 50;
                    }

                    $s->set_limit($limit);

                    // set offset
                    if (!isset($_GET['offset'])) {
                        $offset = 0;
                    } else {
                        $offset = $_GET['offset'] * $limit;
                    }
                    $s->set_offset($offset);

                    //result($x) & page count
                    $cx = (int)$s->count();
                    $pages = $cx / $limit;

                    //create array used to populate select#page_number
                    $i = 0;
                    $page_no = [];
                    while ($i <= $pages) {
                        $i++;
                        $page_no[] = $i;
                    }

                    if (count($page_no) > ceil($pages)) {
                        array_pop($page_no);
                    }

                    $i = 0;


                    while ($c = $s->next()) {
                        $comment = str_replace(utf8_decode("£"), "&pound;", $c->message());
                        $comment = str_replace(utf8_decode("Â"), "&nbsp;", $comment);

                        $extra_cols = "";

                        if(in_array($_GET['subject'], ["Portfolio Valuation", "Portfolio Valuation - Elevate Only"])){
                            $content = "";

                            $sPI = new Search(new ProreportInvoice());
                            $sPI->eq("corr", $c->id());

                            if($pi = $sPI->next(MYSQLI_ASSOC)){
                                $sVR = new Search(new ValuationRequest());
                                $sVR->eq("workflow", $pi->workflow_id());

                                if($vr = $sVR->next(MYSQLI_ASSOC)){
                                    $sOS = new Search(new OngoingService());
                                    $sOS->eq("val_req_id", $vr->id());

                                    if($os = $sOS->next(MYSQLI_ASSOC)){
                                        $content = strval($os->review_type);
                                    }
                                }
                            }
                            $extra_cols .= "<td>$content</td>";
                        }

                        $json['corr_list'] .= "<tr>
											<td>" . $c->link($c->id) . "</td>
											<td>" . $c->date() . "</td>
											<td>" . $c->subject() . "</td>
											" . $extra_cols . "
											<td>" . $c->partner->link() . "</td>
											<td>" . $c->client->link() . "</td>
											<td>" . $c->sender->link() . "</td>
											<td>" . $c->policy->link() . "</td>
											<td>" . $comment . "</td>
											</tr>";

                        $i++;
                    }

                    // give user some feedback if no results are found
                    if ($json['corr_list'] == false) {
                        $json['corr_list'] = "<tr><td>No results found, please re-define search criteria.</td></tr>";
                    }


                    $json['page_no'] = $page_no;
                    $json['record_count'] = $cx;
                    $json['pages'] = ceil($pages);

                    echo json_encode($json);
                }
            } else {
                $json['error'] = "noinput";
                echo json_encode($json);
            }
            break;

        case "delete-range":
            $return = [
                "status" => false,
                "error" => false
            ];

            if(User::get_default_instance('team_leader') || User::get_default_instance('department') == 7){
                $flaggedDir = CORRESPONDENCE_PENDING_SCAN_DIR . $_GET['dir'];
                $no_files = sizeof($_GET['files']);
                $file_counter = 0;

                foreach($_GET['files'] as $file){
                    //check file exists
                    if(file_exists($flaggedDir . "/" . $file)){
                        //delete the file
                        if(unlink($flaggedDir . "/" . $file)){
                            $file_counter++;
                        } else {
                            $return['error'] = true;
                            $return['status'] = $flaggedDir . "/" . $file . " could not be deleted";
                            echo json_encode($return);
                            exit();
                        }
                    } else {
                        $return['error'] = true;
                        $return['status'] = $flaggedDir . "/" . $file . " no longer exists.";
                        echo json_encode($return);
                        exit();
                    }
                }

                // check all files were successfully moved
                if ($no_files == $file_counter) {
                    // all files successfully deleted
                    $return['status'] = "All selected items deleted!";
                } else {
                    $return['error'] = true;
                    $return['status'] = "One or more correspondence items have failed to delete,
                 please try again or contact IT.";
                }
            } else {
                $return['error'] = true;
                $return['status'] = "Only team leaders and IT can delete flagged correspondence!";
            }

            echo json_encode($return);

            break;
    }
} else {
    /*** perform a search based on string ***/

    $search = new Search(new Correspondence);
    $search->calc_rows = true;

    $search->set_flags([
        ["id", Search::DEFAULT_INT ],
        ["issuer", "policy->issuer->name"],
        ["policy", "policy->number"],
        ["sender"],
        ["subject"]
    ]);

    if (isset($_GET["search"])) {
        if (isset($_GET["filter"])) {
            /* ugly.. but couldn't have multiple subject filters as the previous one was over written
            by the next and this workaround still allows us to use the correspondence plugin */
            if (isset($_GET['filter']['subject'])) {
                if ($_GET['filter']['subject'] == "death_or_poa") {
                    // apply death certificate & POA correspondence subjects
                    $search -> add_or([
                        $search->eq("subject", "Death Certificate - Copy", true, 0),
                        $search->eq("subject", "Death Certificate to Provider", true, 0),
                        $search->eq("subject", "Death Certificate returned from Provider", true, 0),
                        $search->eq("subject", "Death Certificate returned to Partner", true, 0),
                        $search->eq("subject", "GOP to Provider", true, 0),
                        $search->eq("subject", "GOP returned from Provider", true, 0),
                        $search->eq("subject", "GOP returned to Partner", true, 0),
                        $search->eq("subject", "Birth Certificate - Sent to provider", true, 0),
                        $search->eq("subject", "Birth Certificate - Returned from provider", true, 0),
                        $search->eq("subject", "Birth Certificate - Returned to partner", true, 0),
                        $search->eq("subject", "Marriage Certificate - Sent to provider", true, 0),
                        $search->eq("subject", "Marriage Certificate - Returned from provider", true, 0),
                        $search->eq("subject", "Marriage Certificate - Returned to partner", true, 0),
                        $search->eq("subject", "POA sent to Provider", true, 0),
                        $search->eq("subject", "POA returned from Provider", true, 0),
                        $search->eq("subject", "POA returned to Partner", true, 0),
                        $search->eq("subject", "Trustee Documents", true, 0),
                        $search->eq("subject", "Beneficiary Documents", true, 0),
                        $search->eq("subject", "Death Claim forms", true, 0)
                    ]   );
                }
            } else {
                $search->apply_filters($_GET["filter"]);
            }

            if (isset($_GET['filter']['client'])) {
                // apply client id
                $search->eq("client", $_GET['filter']['client']);
            }

        }

        if (isset($_GET["limit"])) {
            $search->set_limit((int) $_GET["limit"]);
        }

        if (isset($_GET["offset"])) {
            $search->set_offset((int) $_GET["offset"]);
        }

        if (isset($_GET["order"]) && is_array($_GET["order"])) {
            $search->order($_GET["order"]);
        } else {
            $search->add(["date","DESC"]);
        }

        if (empty($_GET["excludes"])) {
            $_GET["excludes"] = [];
        }

        $t = Correspondence::get_template('list_row.html', ['excludes'=>$_GET["excludes"]]);
        $out = "";

        while ($c = $search->next($_GET["search"])) {
            $t->tag($c, "correspondence");
            $out .= "$t";
        }

        echo json_encode([
                "src"     => $out,
                "offset"  => $search->get_offset(),
                "total"  =>  $search->count_total_rows()
            ]
        );
    } else {
        $correspondence = new Correspondence();
        $policy = new Policy();

        echo new Template("correspondence/search.html", ["help"=>$search->help_html(), "correspondence"=>$correspondence, "policy"=>$policy]);
    }
}

function assignQueue($userID, $assignedFile){
    $assigned = [
        "user" => $userID,
        "time" => time(),
    ];

    if (file_put_contents($assignedFile, json_encode($assigned))){
        return true;
    } else {
        return false;
    }
}

