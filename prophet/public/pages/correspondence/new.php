<?php

require_once($_SERVER['DOCUMENT_ROOT']."/../bin/_main.php");

$letter = new Letter;

if (isset($_REQUEST["policy"]) && (int)$_REQUEST["policy"]) {
    // check for compulsory policy id, and that it is an integer
    $policy = new Policy($_REQUEST["policy"], true);

    if (isset($_REQUEST["letter"]) && intval($_REQUEST["letter"])) {
        $letter->id($_REQUEST["letter"]);
    }

    if (isset($_GET["form"])) {
        // get Letter's custom input form
        if ($letter->id($_GET["form"]) && $letter->get() && $letter->form()) {
            die(Template($letter->form(), ["policy"=>$policy, "correspondence" => new Correspondence]));
        }
    } else if (isset($_POST["submit"])) {
        // onsubmit

        $letter->get();
            
        $c = new Correspondence(new Policy($_REQUEST["policy"]));
        $c->load($_POST);
        $c->sender($_USER->id());
        $c->letter($letter->id());
        $c->subject((isset($_REQUEST["subject"])) ? $_REQUEST["subject"] : $letter->name());
        $status = $c->save();
        
        
        /*** Automatically update Item's Policy to 'Servicing Transferred to PS' status if
			it is still waiting for confirmation etc. ***/
        $policy = $c->policy->get_object();
        $omission_subjects = unserialize(CORR_STATUS_OMISSION);
        if (!in_array($c->subject(), $omission_subjects)) {
            if ($policy->status() < 2) {
                $policy->transferred(date("Y-m-d"));
                $policy->status(2);
                $policy->save();
            }
        }

        if ($status) {
            $applet =  Template("letter/print.html", ["data"=>$_POST,"item_id"=>$c->id()]);
        }
        
        if (isset($_REQUEST["return"])) {
            if ($_REQUEST["return"] == "json") {
                echo json_encode(["status"=>$status, "applet"=>"$applet"]);
            }
        } else {
            echo $applet;
        }
    } else {
        // GUI
        if ($letter->get() && $letter->form) {
            // get this Letter type's custom form
            $form = Template($letter->form->get(), ["correspondence"=>new Correspondence]);
        }
        
        // and embed it in a generic template
        echo Template("letter/new.html", ["form"=>$form,"letter"=>$letter->id()]);
    }
} else if (isset($_GET["xml"], $_GET["item"]) && intval($_GET["xml"].$_GET["item"])) {
    // Get the Letter's XML source

    if (isset($_POST["data"])) {
        $data = json_decode($_POST["data"]);
    }

    $letter = new Letter($_GET["xml"]);

    $item = new Correspondence($_GET["item"], true);

    if ($letter->get()) {
        header("Content-type: text/xml");
        echo Template($letter->xml(), ["data"=>(array)$data,"item"=>$item]);
    } else {
        header("Status: 404");  // this is not the xml you're looking for
    }
}
