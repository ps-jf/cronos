<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";
        
if (isset($_GET["id"]) && !isset($_GET["object"])) {
    $call = new Call();
    $user = new User($_COOKIE["uid"], true);
    
    if (isset($_GET["list"])) {
    // Create a list of calls

        $row_t = Template("call/directory_row.html");

        $iter = Search(new Call);
        $filters = ["number1"=>$user->internal_extension, "number2"=>$user->internal_extension];
        $iter->apply_filters($filters, true);
        $iter->set_limit(50);
        $iter->add_order("id", "desc");

        $call_list = "";
    
        while ($call = $iter->next()) {
            $row_t->tag($call, "call");
            $call_list .= $row_t;
        }

        echo Template("call/directory.html", ["call_list"=>$call_list]);
    }
} else if (isset($_GET["object"]) && isset($_GET["id"])) {
    if ($_GET["object"] == "Partner") {
        $call = new Call();
        $partner = new Partner($_GET["id"], true);

        if (isset($_GET["list"])) {
            $iter = Search(new Call);

            $iter->add_or([
                $iter->eq("number1", trim(str_replace(" ", "", $partner->phone)), true, 0),
                $iter->eq("number1", trim(str_replace(" ", "", $partner->mobile)), true, 0),
                $iter->eq("number2", trim(str_replace(" ", "", $partner->phone)), true, 0),
                $iter->eq("number2", trim(str_replace(" ", "", $partner->mobile)), true, 0)
            ]);

            $iter->set_limit(100);
            $iter->add_order("id", "desc");

            while ($call = $iter->next()) {
                echo Template("call/directory_row.html", ["call"=>$call]);
            }
            echo "No calls recorded for partner";
        }
    } else if ($_GET["object"] == "Client") {
        $call = new Call();
        $client = new Client($_GET["id"], true);

        if (isset($_GET["list"])) {
            $iter = Search(new Call);

            $iter->add_or([
                $iter->eq("number1", trim(str_replace(" ", "", $client->phone)), true, 0),
                $iter->eq("number1", trim(str_replace(" ", "", $client->mob_phone)), true, 0),
                $iter->eq("number2", trim(str_replace(" ", "", $client->phone)), true, 0),
                $iter->eq("number2", trim(str_replace(" ", "", $client->mob_phone)), true, 0)
            ]);

            $iter->set_limit(100);
            $iter->add_order("id", "desc");

            while ($call = $iter->next()) {
                echo Template("call/directory_row.html", ["call"=>$call]);
            }
            echo "No calls recorded for client";
        }
    } else {
        echo "No valid object supplied to retrieve calls for";
    }
}
