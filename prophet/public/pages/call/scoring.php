<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";
		
if( isset($_GET["id"]) ){

    //  Allow user to update score entry

    // if flag set then return old style form
    if(isset($_GET['flag'])){
        $callscore = new CallScoring($_GET["id"], true);
        echo new Template("call/scoring_update.html", ["cs" => $callscore]);
    }else{
        $callscore2018 = new CallScoring2018($_GET["id"], true);
        echo new Template("call/scoring_update_2018.html", ["cs" => $callscore2018]);
    }

}
else if( isset($_GET["do"]) ){

	switch( $_GET["do"] ) {

		case "new":
            echo Template( "call/scoring2018.html" );
        break;

		case "list":

            $cs = new CallScoring();
            $cs2018 = new CallScoring2018();
            $score_list = false;

            echo Template( "call/scoring_list.html", ["score_list"=>$score_list, "cs"=>$cs, 'cs2018'=>$cs2018]);

        break;

        case "user_defined":

            $json = [
                "status" => false,
                "error"  => false,
                "score_list"  => false,
                "page_no" => null,
                "pages" => null,
                "record_count" => null
            ];

            //default limit 10 rows per page
            $limit = 20;

            if( !isset( $_GET['offset'] ) ){
                $offset = 0;
            }else{
                $offset = (int)$_GET['offset'] * $limit;
            }
            $db = new mydb();

            //use specific query for individual users or all records depending what is selected in the user dropdown
            if($_GET['user_id'] == NULL){
                $q = "SELECT SQL_CALC_FOUND_ROWS * FROM prophet.call_scoring
                      ORDER BY date DESC LIMIT ".$limit." OFFSET ".$offset;
            }else{
                $q = "SELECT SQL_CALC_FOUND_ROWS * FROM prophet.call_scoring
                      WHERE user_id = ".$_GET['user_id']."
                      ORDER BY date DESC LIMIT ".$limit." OFFSET ".$offset;
            }

            $q1 = "SELECT FOUND_ROWS();";

            $db->query( $q );
            $db->query( $q1 );

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM );
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages ){
                $i++;
                $page_no[] = $i;
            }

            if(count($page_no) > ceil($pages)){
                array_pop($page_no);
            }

            $db->query( $q );
            $json['score_list'] = "";

            //build up the rows
            while( $score = $db->next( MYSQLI_ASSOC  )){

                $u = new User($score['user_id'], true);
                $date = date('d/m/Y H:i:s', strtotime($score['date']));

                $json['score_list'] .= "<tr>
                                        <td>".$score['id']."</td>
                                        <td>".ucwords(str_replace('.', ' ', $u->username()))."</td>
                                        <td>".$date."</td>
                                        <td>".$score['score']."</td>
                                        <td>
                                            <button id='".$score['id']."' name='".$score['id']."' class='update_old'>View/Update</button>
                                            <button id='".$score['id']."' name='".$score['id']."' class='print'><img src='/lib/images/printer.png'></button>
                                            <button id='".$score['id']."' name='".$score['id']."' class='delete'></button>
                                        </td>
                                    </tr>";

            }

            if($q && $q1){
                $json['status'] = true;
            }else{
                $json['status'] = false;
                $json['error'] = "An error has occurred while trying to query the database. Please try again, if this persists contact IT.";
            }

            $json['page_no'] = $page_no;
            $json['record_count'] = $x;
            $json['pages'] = ceil($pages);

            echo json_encode($json);


        break;

        case "user_defined_2018":

            $json = [
                "status" => false,
                "error"  => false,
                "score_list"  => false,
                "page_no" => null,
                "pages" => null,
                "record_count" => null
            ];

            //default limit 10 rows per page
            $limit = 20;

            if( !isset( $_GET['offset'] ) ){
                $offset = 0;
            }else{
                $offset = (int)$_GET['offset'] * $limit;
            }

            $db = new mydb();

            //use specific query for individual users or all records depending what is selected in the user dropdown
            if($_GET['user_id'] == NULL){
                $q = "SELECT SQL_CALC_FOUND_ROWS * FROM prophet.call_scoring2018
                      ORDER BY date DESC LIMIT ".$limit." OFFSET ".$offset;
            }else{
                $q = "SELECT SQL_CALC_FOUND_ROWS * FROM prophet.call_scoring2018
                      WHERE user_id = ".$_GET['user_id']."
                      ORDER BY date DESC LIMIT ".$limit." OFFSET ".$offset;
            }

            $q1 = "SELECT FOUND_ROWS();";

            $db->query( $q );
            $db->query( $q1 );

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM );
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages ){
                $i++;
                $page_no[] = $i;
            }

            if(count($page_no) > ceil($pages)){
                array_pop($page_no);
            }

            $db->query( $q );
            $json['score_list'] = "";

            //build up the rows
            while( $score = $db->next( MYSQLI_ASSOC  )){

                $u = new User($score['user_id'], true);
                $date = date('d/m/Y H:i:s', strtotime($score['date']));

                $json['score_list'] .= "<tr>
                                        <td>".$score['id']."</td>
                                        <td>".ucwords(str_replace('.', ' ', $u->username()))."</td>
                                        <td>".$date."</td>
                                        <td>".$score['score']."</td>
                                        <td>
                                            <button id='".$score['id']."' name='".$score['id']."' class='update_new'>View/Update</button>
                                            <button id='".$score['id']."' name='".$score['id']."' class='print'><img src='/lib/images/printer.png'></button>
                                            <button id='".$score['id']."' name='".$score['id']."' class='delete'></button>
                                        </td>
                                    </tr>";

            }

            if($q && $q1){
                $json['status'] = true;
            }else{
                $json['status'] = false;
                $json['error'] = "An error has occurred while trying to query the database. Please try again, if this persists contact IT.";
            }

            $json['page_no'] = $page_no;
            $json['record_count'] = $x;
            $json['pages'] = ceil($pages);

            echo json_encode($json);


        break;

	break;

    }
}
