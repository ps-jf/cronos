<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 11/12/2018
 * Time: 12:58
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET['search'])) {

    $s = str_replace("@", "*", $_GET['search']);
    $s = addslashes(preg_quote($s));
    $skip = $_GET['skip'];
    $limit = $_GET['limit'];

    $matches = [
        "results" => []
    ];

    $db = new mydb();
    $q = "SELECT MATCH (tblclient.clientfname1, tblclient.clientsname1, tblclient.clientfname2, tblclient.clientsname2,
            tblclient.clientaddress1, tblclient.clientaddress2, tblclient.clientaddress3,
            tblclient.clientaddresspostcode, tblclient.clienttelno, tblclient.clientNINO1, tblclient.clientNINO2, tblclient.email1, tblclient.email2, salutation, scheme_name)
                AGAINST ('$s' IN BOOLEAN MODE) as relevance,
		'client' as type,
        concat(
			ifnull(tblclient.clientfname1, \"\"), \" \", ifnull(tblclient.clientsname1, \"\"),
			IF (tblclient.clientsname2 != \"\" OR tblclient.clientfname2 != \"\", 
				concat(\" & \", ifnull(tblclient.clientfname2, \"\"), \" \", ifnull(tblclient.clientsname2, \"\")), 
                \"\"),
			IF(tblclient.clientaddresspostcode != \"\", concat(\" (\", tblclient.clientaddresspostcode, \")\"), \"\")) AS text,
		tblclient.clientID as value
        FROM feebase.tblclient
        WHERE (MATCH (tblclient.clientfname1, tblclient.clientsname1, tblclient.clientfname2, tblclient.clientsname2,
                    tblclient.clientaddress1, tblclient.clientaddress2, tblclient.clientaddress3,
                    tblclient.clientaddresspostcode, tblclient.clienttelno, tblclient.clientNINO1, tblclient.clientNINO2, tblclient.email1, tblclient.email2, salutation, scheme_name)
                        AGAINST ('$s' IN BOOLEAN MODE) AND group_scheme != 1)
            OR (tblclient.clientID = '$s' AND group_scheme != 1)
            
        UNION
        
        SELECT MATCH (tblclient.clientfname1, tblclient.clientsname1, tblclient.clientfname2, tblclient.clientsname2,
            tblclient.clientaddress1, tblclient.clientaddress2, tblclient.clientaddress3,
            tblclient.clientaddresspostcode, tblclient.clienttelno, tblclient.clientNINO1, tblclient.clientNINO2, tblclient.email1, tblclient.email2, salutation, scheme_name)
                AGAINST ('$s' IN BOOLEAN MODE) as relevance,
		'scheme' as type,
        concat(
			ifnull(tblclient.clientfname1, \"\"), \" \", ifnull(tblclient.clientsname1, \"\"),
			IF (tblclient.clientsname2 != \"\" OR tblclient.clientfname2 != \"\", 
				concat(\" & \", ifnull(tblclient.clientfname2, \"\"), \" \", ifnull(tblclient.clientsname2, \"\")), 
                \"\"),
			IF(tblclient.clientaddresspostcode != \"\", concat(\" (\", tblclient.clientaddresspostcode, \")\"), \"\")) AS text,
		tblclient.clientID as value
        FROM feebase.tblclient
        WHERE (MATCH (tblclient.clientfname1, tblclient.clientsname1, tblclient.clientfname2, tblclient.clientsname2,
                    tblclient.clientaddress1, tblclient.clientaddress2, tblclient.clientaddress3,
                    tblclient.clientaddresspostcode, tblclient.clienttelno, tblclient.clientNINO1, tblclient.clientNINO2, tblclient.email1, tblclient.email2, salutation, scheme_name)
                        AGAINST ('$s' IN BOOLEAN MODE) AND group_scheme = 1)
            OR (tblclient.clientID = '$s' AND group_scheme = 1)
             
        UNION
        
        SELECT MATCH(tblpolicy.policyNum)
                        AGAINST ('$s' IN BOOLEAN MODE) as relevance,
                'policy' as type,
                concat(ifnull(tblpolicy.policyNum, \"\"), \" - \",
                    ifnull(tblclient.clientfname1, \"\"), \" \", ifnull(tblclient.clientsname1, \"\"),
                    IF (tblclient.clientsname2 != \"\" OR tblclient.clientfname2 != \"\", 
                        concat(\" & \", ifnull(tblclient.clientfname2, \"\"), \" \", ifnull(tblclient.clientsname2, \"\")), 
                        \"\")) AS text,
                tblpolicy.policyID As value
        FROM feebase.tblpolicy JOIN feebase.tblclient ON tblpolicy.clientID = tblclient.clientID
        WHERE MATCH(tblpolicy.policyNum)
                        AGAINST ('$s' IN BOOLEAN MODE)
            OR tblpolicy.policyID = '$s'
        
        UNION
        
        SELECT MATCH(tblissuer.issuerName)
                        AGAINST ('$s' IN BOOLEAN MODE) as relevance,
                'issuer' as type,
                tblissuer.issuerName AS text,
                tblissuer.issuerID as value
        FROM feebase.tblissuer
        WHERE MATCH(tblissuer.issuerName)
                        AGAINST ('$s' IN BOOLEAN MODE)
            OR tblissuer.issuerID = '$s'
        
        UNION
        
        SELECT MATCH(tblpartner.partnerFname, tblpartner.partnerSname, tblpartner.partnerAdd1, tblpartner.partnerAdd2, tblpartner.partnerAdd3, 
                    tblpartner.partnerPostcode, tblpartner.partnerEmail, tblpartner.partnerAccountName, tblpartner.SJPAgencyCode)
                        AGAINST ('$s' IN BOOLEAN MODE) as relevance,
            'partner' as type,
            concat(ifnull(tblpartner.partnerTitle, \"\"), \" \",
                   ifnull(tblpartner.partnerFname, \"\"), \" \",
                   ifnull(tblpartner.partnerSname, \"\"), \" \") AS text,
            tblpartner.partnerID AS value
        FROM feebase.tblpartner
        WHERE MATCH(tblpartner.partnerFname, tblpartner.partnerSname, tblpartner.partnerAdd1, tblpartner.partnerAdd2, tblpartner.partnerAdd3, 
                    tblpartner.partnerPostcode, tblpartner.partnerEmail, tblpartner.partnerAccountName, tblpartner.SJPAgencyCode)
                        AGAINST ('$s' IN BOOLEAN MODE)
            OR partnerID = '$s'
        
        UNION
        
        SELECT MATCH(practice.name, practice.address1, practice.address2, practice.address3, practice.postcode)
            AGAINST('$s' IN BOOLEAN MODE) as relevance,
            'practice' as type,
            practice.name as text,
            practice.id as value
        FROM feebase.practice
        WHERE MATCH(practice.name, practice.address1, practice.address2, practice.address3, practice.postcode)
            AGAINST('$s' IN BOOLEAN MODE)
            OR practice.id = '$s'
        ORDER BY relevance DESC
        LIMIT $skip, $limit";

    $db->query($q);

    while ($m = $db->next(MYSQLI_ASSOC)) {

        $match = [];
        $match["text"] = $m['text'];
        $match["value"] = $m['value'];
        $match["type"] = $m['type'];
        $matches["results"][] = $match;
    }

    echo json_encode($matches);
}
