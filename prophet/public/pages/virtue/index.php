<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

define("DEFAULT_CLIENT_SUGGEST_LIMIT", 15);

if (isset($_GET["id"])) {
    $client = new Client($_GET["id"], true);

    if (isset($_GET["get"])) {
        switch ($_GET["get"]) {
            case "profile":
                echo Template('virtue/profile.html', ["client"=>$client]);

                break;
        }
    }

    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "save":
                $db = new mydb();

                $json = [
                    "id" => null,
                    "status" => false,
                    "error" => null
                ];

                try {
                    // Watch for the correct ID here, Virtue ID vs Client ID
                    $s = new Search(new Virtue);
                    $s -> eq("client_id", $_POST['virtue']['client']);

                    // If first profile load, create new virtue object and save
                    if (!$virtue = $s -> next(MYSQLI_ASSOC)) {
                        $virtue = new Virtue();
                        $virtue -> client($_POST['virtue']['client']);
                        $virtue -> known_as($_POST['virtue']['known_as']);
                        $virtue -> active($_POST['virtue']['active']);
                        $virtue -> company($_POST['virtue']['company']);
                        $virtue -> left_sjp($_POST['virtue']['left_sjp']);
                        $virtue -> date_left_sjp($_POST['virtue']['date_left_sjp']);
                        $virtue -> stage($_POST['virtue']['stage']);
                        $virtue -> further_action($_POST['virtue']['further_action']);
                        $virtue -> soft_facts($_POST['virtue']['soft_facts']);
                        $virtue -> six_monthly($_POST['virtue']['six_monthly']);
                        $virtue -> annually($_POST['virtue']['annually']);
                        $virtue -> send_report_on($_POST['virtue']['send_report_on']);
                        $virtue -> call_notes($_POST['virtue']['call_notes']);
                        $virtue -> money_info($_POST['virtue']['money_info']);
                        //$virtue -> intelligent_office($_POST['virtue']['intelligent_office']);
                        $virtue -> ps_adviser($_POST['virtue']['ps_adviser']);
                        $virtue -> separated($_POST['virtue']['separated']);
                        $virtue -> service_package($_POST['virtue']['service_package']);
                        $virtue -> letter_sent($_POST['virtue']['letter_sent']);
                        $virtue -> satisfaction_letter($_POST['virtue']['satisfaction_letter']);
                        $virtue -> report_sent($_POST['virtue']['report_sent']);
                        $virtue -> call_back($_POST['virtue']['call_back']);
                        $virtue -> no_deal($_POST['virtue']['no_deal']);

                        $virtue -> save();

                        $client = new Client($_POST['virtue']['client'], true);
                        $client -> intelligent_office($_POST['virtue']['intelligent_office']);
                        $client -> save();
                    } // Subsequent save requests, obtain virtue object ID from search, load object and save.
                    else {
                        $virtue = new Virtue($virtue->id(), true);

                        $virtue -> client($_POST['virtue']['client']);
                        $virtue -> known_as($_POST['virtue']['known_as']);
                        $virtue -> active($_POST['virtue']['active']);
                        $virtue -> company($_POST['virtue']['company']);
                        $virtue -> left_sjp($_POST['virtue']['left_sjp']);
                        $virtue -> date_left_sjp($_POST['virtue']['date_left_sjp']);
                        $virtue -> stage($_POST['virtue']['stage']);
                        $virtue -> further_action($_POST['virtue']['further_action']);
                        $virtue -> soft_facts($_POST['virtue']['soft_facts']);
                        $virtue -> six_monthly($_POST['virtue']['six_monthly']);
                        $virtue -> annually($_POST['virtue']['annually']);
                        $virtue -> send_report_on($_POST['virtue']['send_report_on']);
                        $virtue -> call_notes($_POST['virtue']['call_notes']);
                        $virtue -> money_info($_POST['virtue']['money_info']);
                        //$virtue -> intelligent_office($_POST['virtue']['intelligent_office']);
                        $virtue -> ps_adviser($_POST['virtue']['ps_adviser']);
                        $virtue -> separated($_POST['virtue']['separated']);
                        $virtue -> service_package($_POST['virtue']['service_package']);
                        $virtue -> letter_sent($_POST['virtue']['letter_sent']);
                        $virtue -> satisfaction_letter($_POST['virtue']['satisfaction_letter']);
                        $virtue -> report_sent($_POST['virtue']['report_sent']);
                        $virtue -> call_back($_POST['virtue']['call_back']);
                        $virtue -> no_deal($_POST['virtue']['no_deal']);

                        $virtue -> save();

                        $client = new Client($_POST['virtue']['client'], true);
                        $client -> intelligent_office($_POST['virtue']['intelligent_office']);
                        $client -> save();
                    }
                } catch (Exception $e) {
                    $json['status'] = false;
                    $json['error'] = $e -> getMessage();
                }

                echo json_encode($json);

                break;

            case "add_review":
                 $s = new Search(new VirtueReview);
                 $s->eq("policy", $_GET['id']);

                if ($review = $s -> next(MYSQLI_ASSOC)) {
                    echo new Template("virtue/add_review.html", ["policyID"=>$_GET['id'], "reviewID"=>$review->id()]);
                } else {
                    echo new Template("virtue/add_review.html", ["policyID"=>$_GET['id'], "reviewID"=>null]);
                }

            
                break;
            
            

            case "save_cleansing":
                $json = [
                    "id" => null,
                    "status" => false,
                    "error" => null
                ];

                try {
                    $virtue = new Virtue($_GET["id"], true);

                    $virtue -> experian_date($_POST['virtue']['experian_date']);
                    $virtue -> pension_plan($_POST['virtue']['pension_plan']);
                    $virtue -> investment_plan($_POST['virtue']['investment_plan']);
                    $virtue -> term_plan($_POST['virtue']['term_plan']);

                    $virtue -> save();
                } catch (Exception $e) {
                    $json['status'] = false;
                    $json['error'] = $e -> getMessage();
                }

                echo json_encode($json);

                break;

            case "save_prospecting":
                $json = [
                    "id" => null,
                    "status" => false,
                    "error" => null,
                    "d" => null,
                    "m" => null,
                    "y" => null,
                ];

                try {
                    if ($_POST['virtue']['date_letter'] != "--") {
                        $follow_up_date = new DateTime($_POST['virtue']['date_letter']);
                        $follow_up_date->modify('+4 week');
                        $follow_up_date = date_format($follow_up_date, 'Y-m-d');
                    } else {
                        $follow_up_date = "0000-00-00";
                    }
                    $virtue = new Virtue($_GET["id"], true);

                    $s = new Search(new VirtueCleansing);
                    $s->eq('client', $virtue->client());
                    while ($d = $s -> next(MYSQLI_ASSOC)) {
                        $vc = new VirtueCleansing($d->id(), true);
                    }
                    $vc -> tel_pref($_POST['virtue']['tel_pref']);
                    $vc -> save();

                    $virtue -> mail_pref($_POST['virtue']['mail_pref']);
                    $virtue -> letter_type($_POST['virtue']['letter_type']);
                    $virtue -> date_letter($_POST['virtue']['date_letter']);
                    $virtue -> outcome($_POST['virtue']['outcome']);
                    $virtue -> follow_up_date($follow_up_date);

                    if ($virtue -> save()) {
                        if ($follow_up_date != "0000-00-00") {
                            $pieces = explode("-", $follow_up_date);
                            $json['y'] = $pieces[0];
                            $json['m'] = $pieces[1];
                            $json['d'] = $pieces[2];
                        }
                    }
                } catch (Exception $e) {
                    $json['status'] = false;
                    $json['error'] = $e -> getMessage();
                }

                echo json_encode($json);

                break;

            case "save_reporting":
                $json = [
                    "id" => null,
                    "status" => false,
                    "error" => null,
                    "d" => null,
                    "m" => null,
                    "y" => null
                ];

                try {
                    if ($_POST['virtue']['report_sent_date'] != "--") {
                        $report_follow_up = new DateTime($_POST['virtue']['report_sent_date']);
                        $report_follow_up->modify('+4 week');
                        $report_follow_up = date_format($report_follow_up, 'Y-m-d');
                    } else {
                        $report_follow_up = "0000-00-00";
                    }

                    $virtue = new Virtue($_GET["id"], true);

                    $virtue -> report_sent_date($_POST['virtue']['report_sent_date']);
                    $virtue -> report_follow_up($report_follow_up);
                    $virtue -> report_results($_POST['virtue']['report_results']);
                    $virtue -> contact($_POST['virtue']['contact']);
                    $virtue -> follow_up_letter_sent($_POST['virtue']['follow_up_letter_sent']);

                    if ($virtue -> save()) {
                        if ($report_follow_up != "0000-00-00") {
                            $pieces = explode("-", $report_follow_up);
                            $json['y'] = $pieces[0];
                            $json['m'] = $pieces[1];
                            $json['d'] = $pieces[2];
                        }
                    }
                } catch (Exception $e) {
                    $json['status'] = false;
                    $json['error'] = $e -> getMessage();
                }

                echo json_encode($json);

                break;

            case "save_ongoing":
                $json = [
                    "id" => null,
                    "status" => false,
                    "error" => null,
                ];

                try {
                    if ($_POST['virtue']['report_sent_date'] != "--") {
                        $report_follow_up = new DateTime($_POST['virtue']['report_sent_date']);
                        $report_follow_up->modify('+4 week');
                        $report_follow_up = date_format($report_follow_up, 'Y-m-d');
                    } else {
                        $report_follow_up = "0000-00-00";
                    }

                    $virtue = new Virtue($_GET["id"], true);

                    $virtue -> report_sent_date($_POST['virtue']['report_sent_date']);
                    $virtue -> report_due($_POST['virtue']['report_due']);
                    $virtue -> frequency($_POST['virtue']['frequency']);
                    $virtue -> service_package($_POST['virtue']['service_package']);
                    $virtue -> ps_adviser($_POST['virtue']['ps_adviser']);
                    $virtue -> review_date($_POST['virtue']['review_date']);

                    if ($virtue -> save()) {
                        if ($report_follow_up != "0000-00-00") {
                            $pieces = explode("-", $report_follow_up);
                            $json['y'] = $pieces[0];
                            $json['m'] = $pieces[1];
                            $json['d'] = $pieces[2];
                        } else {
                            $pieces = explode("-", "--");
                            $json['y'] = $pieces[0];
                            $json['m'] = $pieces[1];
                            $json['d'] = $pieces[2];
                        }
                    }

                    $client = new Client($virtue->client(), true);
                    $client -> clientAgreement($_POST['virtue']['client_agreement']);
                    $client -> save();
                } catch (Exception $e) {
                    $json['status'] = false;
                    $json['error'] = $e -> getMessage();
                }

                echo json_encode($json);

                break;

            case "provider_letter_date":
                if (isset($_GET['partner'])) {
                    try {
                        $json = [
                            "status" => false,
                            "error" => false,
                        ];


                        $db = new mydb();
                        $q = "SELECT DISTINCT
                                feebase.tblclient.clientID
                                FROM feebase.tblpolicy
                                INNER JOIN feebase.tblclient
                                  ON feebase.tblpolicy.clientID = feebase.tblclient.clientID
                                INNER JOIN feebase.tblissuer
                                  ON feebase.tblpolicy.issuerID = feebase.tblissuer.issuerID
                                INNER JOIN feebase.issuer_contact
                                  ON issuer_contact.issuer_id = tblissuer.issuerid
                                WHERE tblclient.partnerid in " . $_GET['partner'] . "
                                AND issuer_contact.is_main = 1
                                AND tblpolicy.status in (2,7,18,8,13,26)
                                AND tblpolicy.issuerid not in (11764,1045,1197,1254,11391)
                                AND tblpolicy.agency_id is null
                                AND tblpolicy.policynum not like '*%'
                                ORDER BY issuername,policyID";
                        $db->query($q);

                        $clientID = [];
                        while ($row = $db->next(MYSQLI_ASSOC)) {
                            $clientID[] = $row['clientID'];
                        }

                        if (empty($clientID)) {
                            $json['status'] = "No accounts available to update";
                            $json['error'] = "No accounts available to update";
                        } else {
                            foreach ($clientID as $client) {
                                $s = new Search(new VirtueCleansing);
                                $s->eq('client', $client);
                                if ($r = $s->next(MYSQLI_ASSOC)) {
                                    $vc = new VirtueCleansing($r->id(), true);
                                } else {
                                    $vc = new VirtueCleansing();
                                    $vc->client($client);
                                }

                                $vc->sent_provider(date('Y-m-d'));
                                if ($vc->save()) {
                                    $json['status'] = "Virtue field 'Letter sent to Provider' updated for relevant Clients";
                                } else {
                                    $json['error'] = $e->getMessage();
                                }
                            }
                        }
                    } catch (Exception $e) {
                            $json['error'] = $e -> getMessage();
                    }

                    echo json_encode($json);
                } else {
                    echo new Template("virtue/letter_params.html", ["virtue"=>$_GET['id']]);
                }

                break;

            case "vm_agency_transfers":
                try {
                    $json = [
                        "status" => false,
                        "error" => false,
                        "data" => false
                    ];

                    $s = new Search(new Policy);
                    $s->eq('client', $_GET['client']);
                    $s->eq('status', 2);
                    $s->nt('issuer', 1045);
                    $s->nt('type', 27);
                    $s->nt('issuer', 1197);
                    $s->nt('issuer', 1254);
                    $s->nt('issuer', 11391);
                    $s->nt('issuer', 11764);
                    $s->add_order('issuer', 'desc');

                    while ($a = $s -> next(MYSQLI_ASSOC)) {
                        $agency_bool = ($a->agency_id() != '')
                            ? "<img id='tick' src='/lib/images/tick.png'/>"
                            : "<img id='cross' src='/lib/images/cross.png'/>";

                        $json['data'] .= "<tr>
                                            <td>" . $a->link($a->number()) . "</td>
                                            <td>" . $a->issuer->link() . "</td>
                                            <td>" . $a->status . "</td>
                                            <td title=$a->agency_id>" . $agency_bool . "</td>
                                        </tr>";
                    }

                    if ($json['data'] == null) {
                        $json['data'] = "<tr><td>No records to display</td></tr>";
                    }
                } catch (Exception $e) {
                    $json['error'] = $e -> getMessage();
                }

                echo json_encode($json);

                break;
        }
    }
} else {

    function client_search_scheme(&$search, $str)
    {
        $search->add_and($search->get_object()->group_scheme->to_sql(true));
    }


    function client_search_dob(&$search, $str)
    {
        list($d,$m,$y) = explode("/", $str["text"]);
        $dob = "$y-$m-$d";
        $search->add_or([
            $search->eq("one->dob", $dob, false, 0),
            $search->eq("two->dob", $dob, false, 0)
        ]);
    }

    function client_search_nino(&$search, $str)
    {
        $search->add_or([
            $search->eq("one->nino", $str["text"], true, 0),
            $search->eq("two->nino", $str["text"], true, 0)
        ]);
    }
    

    function client_search_name(&$search, $str)
    {
        if (preg_match('/([\d\w\s]+) [&+] ([\d\w\s]+)/', $str["text"], $match)) {
            $str["text"] = $match[1];
            client_search_name($search, $str);

            $str["text"] = $match[2];
            client_search_name($search, $str);

            return;
        }

        $x = parse_name($str["text"]);
        
        $sname_wildcard=($x["wildcard"]=="surname");

        if ($sname_wildcard) {
            $x["surname"].="%";
        } else {
            $x["forename"].="%";
        }
            
        $conditions = [];

        if (isset($x["forename"])) {
            //we always require a wildcard here due to middle names
            $search->add_or([
                $search->eq("one->forename", $x["forename"]."%", true, 0),
                $search->eq("two->forename", $x["forename"]."%", true, 0)
            ]);
        }
        
        $search->add_or([
            $search->eq("one->surname", $x["surname"], $sname_wildcard, 0),
            $search->eq("two->surname", $x["surname"], $sname_wildcard, 0)
        ]);
    }
    
    $client = new Client;
    $search = new Search($client);
    $search->calc_rows = true;

    $search
        -> flag("id", Search::DEFAULT_INT)
        -> flag("name", Search::CUSTOM_FUNC, "client_search_name", Search::DEFAULT_STRING)
        -> flag("postcode", "address->postcode", Search::PATTERN, "/".Patterns::UK_POSTCODE."/")
        -> flag("nino", Search::CUSTOM_FUNC, "client_search_nino", Search::PATTERN, "/".Patterns::NINO."/")
        -> flag("dob", Search::CUSTOM_FUNC, "client_search_dob")
        -> flag("s", Search::CUSTOM_FUNC, "client_search_scheme");

    if (isset($_GET["search"])) {
        if (isset($_GET["limit"]) && (int)$_GET["limit"] > 0) {
            $search->set_limit((int) $_GET["limit"]);
        } else {
            $search->set_limit(15);
        }
        
        if (isset($_GET["skip"]) && (int)$_GET["skip"] > 0) {
            $search->set_offset((int) $_GET["skip"]);
        }

        $matches = [];
        
        $search->order($client->NAME_ORDER);

        //search for all partnerID's that are part of the virtue practice then place these ID's into an array
        $s = new Search(new PracticeStaffEntry);
        $s->eq("practice", 2894);
        while ($v = $s -> next(MYSQLI_ASSOC)) {
            $virtue_partners[] = $v->partner();
        }

        //search for clients that belong to any of the virtue partner accounts
        $search->add_or($search->object->partner->to_sql($virtue_partners));

        while ($m = $search->next($_GET["search"])) {
            $postcode = (strlen($m->address->postcode))
                ? "(".$m->address->postcode.")"
                : false;
            $matches["results"][] = ["text"=>"$m $postcode", "link"=>$m->link("$m $postcode"), "value"=>"$m->id"];
        }

        echo json_encode($matches);
    } else {
        echo new Template("generic/search.html", ["help"=>$search->help_html(), "url"=>"pages/virtue/?get=profile"]);
    }
}
