<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "view":
            if (isset($_GET["object_id"]) && isset($_GET["object_type"])) {
                /* A list of valuations for this policy */
                $m = new Search(new Valuation);
                $m -> eq('object_type', $_GET["object_type"]);
                $m -> eq('object_id', (int)$_GET["object_id"]);
                $m -> add_order('added_when', 'desc');

                while ($v = $m -> next()) {
                    echo new Template("valuation/list_row.html", ["valuation"=>$v]);
                }
            }

            break;

        case "edit":
            $json = [
                "success" => true,
                "error" => null
            ];

            if (isset($_GET['id'])){
                $v = new Valuation($_GET['id'], true);

                try{
                    echo new Template("valuation/edit.html", ["valuation"=>$v]);
                } catch (Exception $e){
                    $json['success'] = false;
                    $json['error'] = "Failed to load template: ".$e;
                    echo json_encode($json);
                }

            } else {
                $json['success'] = false;
                $json['error'] = "No valuation ID was passed";
                echo json_encode($json);
            }

            break;

        case "update":

            if (isset($_GET["id"])) {
                $v = new Valuation($_GET["id"], true);
                $v->value($_GET["value"]);
                $v->comments($_GET["comments"]);
                $v->currency($_GET['currency']);
                echo ( $v -> save() ) ? "true" : "false";
            }

            break;

        case "delete":
            $json = [
                "success" => true,
                "error" => null
            ];

            if (isset($_GET['id'])){
                try{
                    $v = new Valuation($_GET['id'], true);
                    $v->delete();
                } catch (Exception $e){
                    $json['success'] = false;
                    $json['error'] = "Failed to delete: " . $e;
                }
            } else {
                $json['success'] = false;
                $json['error'] = "No valuation ID was passed";
            }



            echo json_encode($json);

            break;
    }
} else if (isset($_GET["client"])) {
    $v = new Valuation;
    $client = new Client($_GET["client"]);

    //// Get a list of policies for this client
    $s = new Search(new Policy);
    $s->eq("client", $client->id());

    $policy_list = "";

    if (isset($v->policy)){
        // subquery to ensure that only policies without outstanding valuations are returned
        $s->eq("SELECT COUNT(*) ".
            "FROM ".$v->get_table()." ".
            "WHERE ".$v->policy->get_field_name()." = ".$s->get_object()->id->get_field_name()." ".
            "AND ".$v->stage->get_field_name()." < ". ValuationStage::COMPLETE, 0);

        $t = Valuation::get_template("policy_list_row.html");
        while ($p = $s->next()) {
            $t->tag($p, "policy");
            $policy_list .= "$t";
        }
    }

    echo Valuation::get_template("client.html", ["client"=>$client,"policy_list"=>$policy_list]);
}
