<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

use Carbon\Carbon;

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "viewAllClients":
            /**
                 * code here not complete as not sure if functionality to view all invoice
                 * clients via prophet is needed
                 */

            // create curl resource
            $ch = curl_init(INVOICE_NINJA_URL.'/api/v1/clients');

            // set url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set your auth headers
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'X-Ninja-Token: ' . INVOICE_NINJA_TOKEN
            ]);

            // get stringified data/output. See CURLOPT_RETURNTRANSFER
            $data = curl_exec($ch);
            $error = curl_error($ch);

            if ($error) {
                dd($error);
            } else if ($data) {
                dd($data);
            } else {
                dd("something went wong");
            }

            // get info about the request
            $info = curl_getinfo($ch);

            // close curl resource to free up system resources
            curl_close($ch);

            dd($info);

            break;

        case "addClient":

            $client = new Client($_GET['id'], true);

            // build up array depending on 'object' we are invoicing in relation to client
            if ($_GET['object'] == "client") {
                $array = [
                    'name' => strval($client),
                    'id_number' => $client->id(),
                    'address1' => $client->address->line1(),
                    'address2' => $client->address->line2(),
                    'city' => $client->address->line3(),
                    'postal_code' => $client->address->postcode(),
                    'contacts'=> [
                        [
                            'first_name' => $client->one->forename(),
                            'last_name' => $client->one->surname(),
                            'email' => $client->one->email(),
                        ],
                        [
                            'first_name' => $client->two->forename(),
                            'last_name' => $client->two->surname(),
                            'email' => $client->two->email()
                        ]
                    ]
                ];

                if($client->one->client_deceased()){
                    array_splice($array['contacts'], 0, 1);
                }

                if($client->two->client_deceased()){
                    array_splice($array['contacts'], 1, 1);
                }
            } elseif ($_GET['object'] == "partner") {
                $array = [
                'name' => strval($client->partner),
                'id_number' => $client->id(),
                'address1' => $client->partner->address->line1(),
                'address2' => $client->partner->address->line2(),
                'city' => $client->partner->address->line3(),
                'postal_code' => $client->partner->address->postcode(),
                'contacts'=> [
                    [
                        'first_name' => $client->one->forename(),
                        'last_name' => $client->one->surname(),
                        'email' => $client->one->email(),
                    ],
                    [
                        'first_name' => $client->two->forename(),
                        'last_name' => $client->two->surname(),
                        'email' => $client->two->email()
                    ]
                ]
                ];
            } elseif ($_GET['object'] == "provider") {
                $issuer = new Issuer($_GET['provider'], true);

                $array = [
                'name' => strval($issuer),
                'id_number' => $client->id(),
                'address1' => $issuer->bulkaddress1(),
                'address2' => $issuer->bulkaddress2(),
                'city' => $issuer->bulkaddress3(),
                'postal_code' => $issuer->bulkpostcode(),
                'contacts'=> [
                    [
                        'first_name' => $client->one->forename(),
                        'last_name' => $client->one->surname(),
                        'email' => $client->one->email(),
                    ],
                    [
                        'first_name' => $client->two->forename(),
                        'last_name' => $client->two->surname(),
                        'email' => $client->two->email()
                    ]
                ]
                ];
            }

            /**
             * first we want to check if client already exists
             */
            // create curl resource
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, INVOICE_NINJA_URL.'/api/v1/clients?id_number='.$client->id());
            // set url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //Set your auth headers
            $headers = [];
            $headers[] = "Content-Type: application/json";
            $headers[] = "X-Ninja-Token: ".INVOICE_NINJA_TOKEN;
            $headers[] = "X-Requested-With: XMLHttpRequest";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            } else {
                // get info about the request
                $info = curl_getinfo($ch);
                $result = json_decode(curl_exec($ch));
            }

            // next we want to check if we have a client/object match
            if ($result->data  ){
                foreach ($result->data as $key => $data) {
                    // check name here
                    if ($data->name == $array['name']) {
                        $exists = true;
                        break;
                    } else {
                        $exists = false;
                    }
                }
            } else {
                $exists = false;
            }

            // close curl resource to free up system resources
            curl_close($ch);

            if (!$exists) {
                /**
                 * if client does not exists, add new client
                 */

                // create curl resource
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, INVOICE_NINJA_URL.'/api/v1/clients');
                // set url
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $json = json_encode($array);

                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

                // POST method
                curl_setopt($ch, CURLOPT_POST, 1);

                //Set your auth headers
                $headers = [];
                $headers[] = "Content-Type: application/json";
                $headers[] = "X-Ninja-Token: ".INVOICE_NINJA_TOKEN;
                $headers[] = "X-Requested-With: XMLHttpRequest";
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                } else {
                    // get info about the request
                    $info = curl_getinfo($ch);
                    $result = json_decode(curl_exec($ch));
                    if (!is_null($result->data)) {
                        $invoice_client_id = $result->data->id;
                    } else {
                        var_dump(INVOICE_NINJA_URL);
                        var_dump(INVOICE_NINJA_TOKEN);
                    }
                }

                // close curl resource to free up system resources
                curl_close($ch);
            } else {
                $invoice_client_id = $result->data['0']->id;
            }

            (isset($invoice_client_id)) ?
                $url = INVOICE_NINJA_URL."/clients/".$invoice_client_id : $url = false;

             echo json_encode($url);
            exit();
        break;

        case "count":
            $response = [
                "success" => true,
                "count" => 0,
                "od_count" => 0,
                "up_count" => 0
            ];

            $db = new mydb();

            $q = "SELECT count(id) as totalCount FROM ".NINJA_INVOICES." where is_recurring = 0 AND is_deleted = 0 AND invoice_status_id > 1 AND invoice_status_id < 5 AND due_date is not null";

            $db->query($q);

            $totalCount = 0;

            if ($row = $db->next(MYSQLI_ASSOC)){
                $response["count"] = $row["totalCount"];
            } else {
                $response["success"] = false;
            }

            $q = "SELECT count(id) as totalCount FROM ".NINJA_INVOICES." where is_recurring = 0 AND is_deleted = 0 AND invoice_status_id > 1 AND invoice_status_id < 5 AND due_date < CURDATE()";

            $db->query($q);

            $totalCount = 0;

            if ($row = $db->next(MYSQLI_ASSOC)){
                $response["od_count"] = $row["totalCount"];
            } else {
                $response["success"] = false;
            }

            $q = "SELECT count(id) as totalCount FROM ".NINJA_INVOICES." where is_recurring = 0 AND is_deleted = 0 AND invoice_status_id > 1 AND invoice_status_id < 5 AND due_date >= CURDATE()";

            $db->query($q);

            $totalCount = 0;

            if ($row = $db->next(MYSQLI_ASSOC)){
                $response["up_count"] = $row["totalCount"];
            } else {
                $response["success"] = false;
            }

            echo json_encode($response);

            break;

        case "load_invoices":
            $type = $_GET['type'];
            $repsonse = [
                'success' => false,
                'rows' => false
            ];

            $db = new mydb();

            switch ($type){
                case "single":
                    $q = "SELECT invoice_number, po_number, due_date, amount FROM ".NINJA_INVOICES." where is_recurring = 0 AND is_deleted = 0 AND invoice_status_id > 1 AND invoice_status_id < 5 and due_date >= CURDATE();";
                    break;
                case "overdue":
                    $q = "SELECT invoice_number, po_number, due_date, amount FROM ".NINJA_INVOICES." where is_recurring = 0 AND is_deleted = 0 AND invoice_status_id > 1 AND invoice_status_id < 5 and due_date < CURDATE();";
                    break;
                default:
                    echo json_encode($response);
                    exit(0);
                    break;
            }

            $db->query($q);

            $invoices = [];

            while ($row = $db->next(MYSQLI_ASSOC)){
                $invoices[] = $row;
            }

            $rows = [];

            foreach($invoices as $invoice){
                $dueDate = date("U", strtotime($invoice['due_date']));
                $dueDate = Carbon::createFromTimestamp($dueDate);
                $dueDate = $dueDate->toFormattedDateString();

                $amount = number_format((float)$invoice['amount'], 2, '.', ',');

                $rows[] = "<tr>
                <td>" . $invoice['invoice_number'] . "</td>
                <td>" . $invoice['po_number'] . "</td>
                <td>" . $dueDate . "</td>
                <td>£" . $amount . "</td>
            </tr>";
            }

            $response = [
                "success" => true,
                "rows" => $rows
            ];

            echo json_encode($response);
            break;
    }
}
