<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["list"])) {
    $db = new mydb();

    $dir = opendir("".MANDATE_PENDING_SCAN_DIR);

    $row = new Template("department/mandates/list-row.html");
    $error_row = new Template("department/mandates/error-list-row.html");

    $rem_db = new mydb(REMOTE, E_USER_WARNING); // establish remote db conn
    if (mysqli_connect_errno()) {
        echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
        exit();
    }

    $rem_db->allowed_cache = false;

    // order files by creation timestamp
    $files = [];
    while ($file = readdir($dir)) {
        $stat = stat(MANDATE_PENDING_SCAN_DIR.$file);
        $ctime = $stat["ctime"];
        
        while (array_key_exists($ctime, $files)) {
            ++$ctime;
        }

        $files[$ctime] = $file;
    }
    ksort($files);

    foreach ($files as $file) {
        // skip . .. and what have you
        if (is_dir($file) || $file == "." || $file == "..") {
            continue;
        }

        $mandate = new Mandate;
 
        $mandate->stat = stat(MANDATE_PENDING_SCAN_DIR.$file);

        // Already encountered, should have a local entry
        if (preg_match('/^M(\d+)\.(tif|pdf)$/', $file, $capture)) {
            $ext = $capture[2];

            if (!$mandate->get($capture[1])) {
                // no local entry found, filter down to creation below
                unset($mandate);
            }
        } // Prepopulated mandate with [partner ID]+[mandate ID]-[datestamp].[file type] format
        else if (preg_match('/^(\d+)\+(\d+)(\-\d+)?\.(tif|pdf)$/', $file, $capture)) {
            if (!$rem_db) {
                echo $error_row(["error" => "Cannot contact mypsaccount for prepop data"]);
            }

            // Get remote record
            if ($rem_db -> query("select * from ".REMOTE_SYS_DB.".mandate_prepop where id=".intval($capture[2])) == 1) {
                if ($rem_row = $rem_db -> next(MYSQLI_OBJECT)) {
                    if (empty($rem_row->client_id)) {
                        // Adding a new client to the pending client table

                        $keys = [
                            "title1" => "clientTitle1",
                            "fname1" => "clientFname1",
                            "sname1" => "clientSname1",
                            "dob1" => "clientDOB1",
                            "nino1" => "clientNINO1",
                            "title2" => "clientTitle2",
                            "fname2" => "clientFname2",
                            "sname2" => "clientSname2",
                            "dob2" => "clientDOB2",
                            "nino2" => "clientNINO2",
                            "line1" => "clientaddress1",
                            "line2" => "clientaddress2",
                            "line3" => "clientaddress3",
                            "postcode" => "clientaddresspostcode",
                            "partner_id" => "partner_id",
                        ];

                        $vals = [];
                        
                        foreach ($keys as $rem_key => $local_key) {
                            $value = "NULL";
                            if (!empty($rem_row->{$rem_key})) {
                                $value = "'".mysqli_real_escape_string($rem_row->{$rem_key})."'";
                            }
                            
                            $vals[ ] = "$local_key = $value";
                        }
                        
                        // hash the client data, to look for other mandates with this prepop data
                        $data_hash = md5(implode($vals));
                        $vals[] = "data_hash = '".mysqli_real_escape_string($data_hash)."'";
                        
                        $pending_client = $live_id = false;
                        
                        $db->query("select clientID, live_id from ".MANDATE_CLIENT_TBL." where data_hash='".mysqli_real_escape_string($data_hash)."'");
                        
                        if ($match = $db->next(MYSQLI_NUM)) {
                            // a match has been found
                            
                            list( $pending_client, $live_id ) = $match;
                        
                            if (!empty($live_id)) {
                                // has already been added to the the live tblclient
                                // update mandate with real client id
                                $rem_row->client_id = $live_id;
                            }
                        } else {
                            // create a new pending client entry
                            $pending_client = $db -> query("insert into ". MANDATE_CLIENT_TBL ." set ".implode(", ", $vals));
                        }
                    }

                    $mandate->load([
                        "added_when" => time(),
                        "partner" => $rem_row->partner_id,
                        "client" => $rem_row->client_id,
                        "pending_client" => $pending_client,
                        "track_id" => $rem_row->id,
                        "prepop" => true
                    ]);

                    $ext = $capture[4];

                    if ($mandate->save()) {
                        $mandate->file("M".$mandate->id().".$ext");
                        $mandate->save();
                    } else {
                        unset($mandate);
                    }
                }
            }
        } // Unpopulated mandate with [partner ID]-[datestamp].[file type] format
        else if (preg_match('/^(\d+)(\-?\d*)\.(tif|pdf)$/', $file, $capture)) {
            $mandate->load([
                "partner" => $capture[1],
                "added_when" => time(),
                "added_by"   => $_USER->id()
            ]);

            $ext = $capture[3];

            if ($mandate->save()) {
                $mandate->file("M".$mandate->id().".$ext");
                $mandate->save();
            } else {
                unset($mandate);
            }
        } else if (preg_match('/^(-?\d*)\.(tif|pdf)$/', $file, $capture)) {
            $mandate->load(["added_when" => time()]);

            if ($mandate->save()) {
                $mandate->file("M".$mandate->id().".$capture[2]");
                $mandate->save();
            } else {
                unset($mandate);
            }
        } else {
            continue;
        }

        if (!$mandate) {
            // A local entry has not been found/generated for this mandate
            // but all signs suggest that this file is a valid mandate.
            // Therefore, create one.

            $mandate = new Mandate;
            $mandate->load(["added_when"=>time(), "added_by"=>$_USER->id()]);

            if ($mandate->save()) {
                if (!$ext) {
                    $ext = "tif";
                }

                $mandate->file("M".$mandate->id().".$ext");
                $mandate->save();
            } else {
                echo $error_row([ "error" => "Local database entry missing for #$mandate->id", "mandate"=>$mandate] );
                continue;
            }
        }

        $mandate->rename_scan($file);

        echo $row(["mandate" => $mandate]);
    }
} else if (isset($_GET["id"])) {
    $mandate = new Mandate($_GET["id"], true);

    if (isset($_GET["view"])) {
        if (isset($_GET["filename"])) {
            // old scan reference, must be in archive
            $filename = $_GET["filename"];
            $filepath = MANDATE_PROCESSED_SCAN_DIR."$filename";
        } else {
            // modern post-2.1 scan
            $filename = $mandate->file();
            $filepath = ( (!$mandate->sent_when()) ? MANDATE_PENDING_SCAN_DIR : MANDATE_PROCESSED_SCAN_DIR ) ."$filename";
        }

        if (in_array($_GET["view"], ["file", "download"])) {
            header("Content-type: image/tiff");
            
            //Streams as attachment if download requested
            if ($_GET["view"] == "download") {
                header("Content-Disposition: attachment; filename=$filename");
            }
            
            if ($file = file_get_contents($filepath)) {
                echo $file;
            }
        } else {
            echo Template("department/mandates/view.html", ["id"=>$mandate->id, "filename"=>$filename]);
        }
    } else if (isset($_GET[ "send" ])) {
        /* A spot of preliminary input validation */

        // ensure that required data has been supplied
        if (!isset($error)) {
            foreach (["partner","client","issuer"] as $var) {
                if (isset($_POST[$var]) && intval($_POST[$var])) {
                    ${$var} = $_POST[$var];
                } else {
                    $error = ucwords($var)." must be specified";
                    break;
                }
            }
        }

        $issuer = new Issuer($issuer);

        // handle policies separately as it's an array
        $policies = [];
        if (!isset($error) && is_array($_POST["policy"])) {
            foreach ($_POST["policy"] as $p) {
                // ensure that policy is complete ( number + type )
                if (!empty($p["number"])) {
                    if (empty($p["type"])) {
                        $error = "Policy $p[number] incomplete";
                        break;
                    } else {
                        $policies[] = $p;
                    }
                }
            }

            if (!sizeof($policies)) {
                $error = "At least one policy required";
            }
        }
        


        if (!isset($error)) {
            /* Insert policies */

            $policy_count = 0; // count policies added
            
            foreach ($policies as $p) {
                $policy = new Policy;

                $policy->load([
                      "client"  => $client,
                      "issuer"  => $issuer->id(),
                      "mandate" => $mandate->id(),
                      "number" => $p["number"],
                      "type"   => $p["type"]
                ]);

                if (isset($_POST["requires-original"])) {
                    $policy->status(1);
                }

                if (!$policy->save()) {
                    // rollback new policies
                    foreach ($policy_ids as $id) {
                        $policy->id($id);
                        $policy->delete();
                    }

                    $error = "Could not save policy '$p[number]'";
                    break;
                } else {
                    ++$policy_count;
                }
            }
        }

        if (!isset($error)) {
            /* Close & archive mandate  */
            
            $mandate->load($_POST);
            $mandate->load([
                  "sent_when"    => time(),
                  "sent_by"      => $_USER->id(),
                  "policy_count" => $policy_count,
            ]);

            $mandate->save();
            $mandate->move_to_processed_directory();
            die(json_encode(["status"=>0]));
        } else {
            die(json_encode(["status"=>1, "error"=>$error]));
        }
    } else {         // Open overlay for processing this mandate
        echo Template("department/mandates/process.html", ["mandate"=>$mandate]);
    }
} else if (isset($_GET["client"])) {
    if ($_GET["client"] == "dedupe") {
        // Based on the data entered so far, attempt
        // to compile a list of potential duplicate
        // clients.

        if (!intval($_GET["partner"])) {
            die(); // require partner, could get messy
        }

        
        $search = new Search(new Client);
        $search->eq("partner", $_GET["partner"]);

        for ($i=1; $i<3; $i++) {
            // forename provided: match wildcarded (allowing middle names) or initial
            if (!empty($_POST["forename$i"])) {
                $search->add_or([
                    $search->eq("one->forename", $_POST["forename$i"]."%", true, 0),
                    $search->eq("two->forename", $_POST["forename$i"]."%", true, 0),
                    $search->eq("one->forename", $_POST["forename$i"][0]."%", true, 0),
                    $search->eq("two->forename", $_POST["forename$i"][0]."%", true, 0)
                ]);
            }

            // wildcard surname match
            if (!empty($_POST["surname$i"])) {
                $search->add_or([
                    $search->eq("one->surname", $_POST["surname$i"]."%", true, 0),
                    $search->eq("two->surname", $_POST["surname$i"]."%", true, 0),
                ]);
            }

            // if postcode provided, match similar or empty
            if (!empty($_POST["postcode"])) {
                $search->add_or([
                    $search->eq("address->postcode", "", 0, 0),
                    $search->eq("address->postcode", str_replace(' ', '%', $_POST["postcode"]), true, 0)
                ]);
            }
        }

        $matches = [];
        while ($m = $search->next()) {
            $matches[ ] = ["id"=>$m->id(), "link"=>$m->link()];
        }

        die(json_encode($matches));
    } else if (isset($_GET["save"])) {
        /*** Either making a new client or saving changes to an existing one ***/

        try {
            if (isset($_POST["pending"])) {
                /*** Saving a pending client as an actual client ***/
            
                if ($pending_id = intval($_POST["pending"])) {
                    // convert PendingClient to Client
                    $client = new Client;
                    $client->load($_POST);
                    $client->partner($_GET["partner"]);

                    $error = "Client record not created";
                    $client->save();


                    // Update this all mandates for this new client with the new active id
                    // #TODO implement a way to update multiple databaseobjects in one query i.e. id non-specific
                    $mandate = new Mandate;
                                    
                    $db = new mydb();

                    $query = "update ".$mandate->get_table()." ".
                             "set ".$mandate->client->get_field_name()." = ".$client->id().", ".
                             $mandate->pending_client->get_field_name()." = NULL ".
                             "where ".$mandate->pending_client->get_field_name()." = $pending_id";

                    $error = "Client saved, but mandates will not reflect changes";
                    $db->query($query);
                }
            } else {
                /*** Saving changes to an existing client ***/

                $client = new Client($_GET["client"], true);
                if (isset($_GET["partner"])) {
                    $client->partner($_GET["partner"]);
                }
                $client->load($_POST);
                
                $error = "Could not save changes to this client";
                $client->save();
            }

            // return successful
            echo json_encode(["client"=> $client->id()]);
        } catch (mydbException $e) {
            die(json_encode(["error"=>$error]));
        }
    } else {
        $client = ( isset($_GET["pending"]) ) ? new PendingClient : new Client;
        $client->get($_GET["client"]);

        if (isset($_GET["pending"])) {
            $custom = el::hide("pending", $_GET["client"]);
        }

        echo new Template("client/form.html", ["client" => $client, "custom"=>$custom]);
    }
} else if (isset($_GET["requires_original"])) {
    // inform client of whether selected providers requires original mandates
    $i = new Issuer($_GET["requires_original"]);
    
    if (!$i->get()) {
        die("error: not found");
    }
    echo intval(!$i->accepts_fax_mandates());
} else { // Display homescreen, with a list of all mandates
    echo new Template("department/mandates/home.html");
}
