<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

use Carbon\Carbon;
use Aws\S3\S3Client;
use Predis\Collection\Iterator;

$whitelist = [
    '127.0.0.1',
    '127.0.0.1:8080',
    '127.0.0.1:8088',
    '::1',
    'localhost:8080',
    'localhost:8088',
    '10.0.2.2'
];

// Define our AWS credentials
$bucket = AWS_BUCKET;
$s3StorageKey = AWS_STORAGE_KEY;
$s3Secret = AWS_SECRET;

// Instantiate the S3 client with AWS credentials
$s3Client = S3Client::factory([
    'credentials' => [
        'key'    => $s3StorageKey,
        'secret' => $s3Secret
    ],
    'region'  => 'eu-west-2',
    'version'  => '2006-03-01'
]);

if (isset($_GET["get"])) {
    switch ($_GET["get"]) {
        /**
         * Simple table that is populated with any action required of this profile.
         * Params in (object_type, object_id)
         * Params out JSON(ar_items, total_rows)
         */
        case "plugin":

            $r = [];

            $s = new Search(new ActionRequired);

            if ($_GET['level'] == "false" || $_GET['level'] == $_GET['object_type']){
                $s->eq('object', $_GET["object_type"]);
                $s->eq('object_id', $_GET["object_id"]);
            } else {
                $s->eq('object', $_GET['level']);

                switch($_GET['object_type']){
                    case "Practice":
                        $s2 = new Search(new PracticeStaffEntry());

                        $s2->eq("practice", $_GET['object_id']);
                        $s2->eq("active", 1);
                        $s2->nt("partner", null);

                        $partners = [];

                        while($staff = $s2->next(MYSQLI_ASSOC)){
                            $partners[] = $staff->partner();
                        }

                        switch($_GET['level']){
                            case "Partner":
                                $s->eq("object_id", $partners);

                                break;

                            case "Client":
                            case "Policy":
                                $s3 = new Search(new Client());

                                $s3->eq("partner", $partners);

                                $clients = [];

                                while($c = $s3->next(MYSQLI_ASSOC)){
                                    $clients[] = $c->id();
                                }

                                switch($_GET['level']){
                                    case "Client":
                                        $s->eq("object_id", $clients);

                                        break;

                                    case "Policy":
                                        $s4 = new Search(new Policy());

                                        $s4->eq("client", $clients);

                                        $policies = [];

                                        while($p = $s4->next(MYSQLI_ASSOC)){
                                            $policies[] = $p->id();
                                        }

                                        $s->eq("object_id", $policies);

                                        break;
                                }

                                break;
                        }

                        break;
                    case "Partner":
                        //get a list of object ID's relevant to the object being viewed
                        $s2 = new Search(new Client());

                        $s2->eq('partner', $_GET['object_id']);

                        $clients = [];

                        while($client = $s2->next(MYSQLI_ASSOC)){
                            $clients[] = $client->id();
                        }

                        if(!empty($clients)){
                            switch($_GET['level']){
                                case "Client":
                                    $s->eq('object_id', $clients);

                                    break;

                                case "Policy":
                                    //we need to loop through each client and get each policy
                                    $s3 = new Search(new Policy());

                                    $s3->eq('client', $clients);

                                    $policies = [];

                                    while($policy = $s3->next(MYSQLI_ASSOC)){
                                        $policies[] = $policy->id();
                                    }

                                    $s->eq('object_id', $policies);

                                    break;
                            }
                        } else {
                            $s->eq("object_id", false);
                        }

                        break;
                    case "Client":
                        $s2 = new Search(new Policy());

                        $s2->eq('client', $_GET['object_id']);

                        $policies = [];

                        while($policy = $s2->next(MYSQLI_ASSOC)){
                            $policies[] = $policy->id();
                        }

                        $s->eq('object_id', $policies);

                        break;
                }
            }

            $s->add_order('archived');
            $s->add_order('completed_when');
            $s->add_order('id');

            $i = 0;
            while ($item = $s->next(MYSQLI_ASSOC)) {
                $r["ar_items"] .= "<tr id='ticket-" . $item->id() . "'>
       
                    <td width='60px'>" . $item->id() . "</td>
                    <td width='200px'>" . $item->link($item->subject_id) . "</td>
                    <td width='150px'>" . Date("H:i d/m/y", $item->addedwhen()) . "</td>
                    <td width='150px'>" . $item->completed_when . "</td>
                    <td>" . $item->completed_by->email_address() . "</td>";

                if (($item->completed_when() == null) && (!$item->archived())) {
                    $r["ar_items"] .= " <td width='350px'><button class='ar_view actionBadge' id='" . $item->id() . "'>
                                            <i class=\"fa fa-reply\" aria-hidden=\"true\"></i>
                                            Message
                                            </button>
                                            <button class='ar_delete warningBadge' id='" . $item->id() . "'>
                                            <i class=\"fa fa-archive\" aria-hidden=\"true\"></i>
                                            Archive
                                            </button>
                                             <button class='ar_complete completeBadge' id='" . $item->id() . "'>
                                            <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
                                            Mark as Complete
                                            </button>
                                        </td></tr>";
                } else {
                    if ($item->archived()) {
                        $r["ar_items"] .= '<td width="350px"><div class="tag">Archived</div></td></tr>';
                    } elseif ($item->completed_when() != null) {
                        $r["ar_items"] .= "<td width='350px'><div><span class='good_news'>Complete</span></div></td></tr>";
                    }
                }
                $i++;
            }

            $r["total_rows"] = $i;

            echo json_encode($r);

            break;

        /**
         * Simple add case for creating a new ActionRequired
         * Params in (object, id)
         * Params out $ar, $partnerID, $obj, `$object_type
         */
        case "ar_new":
            // get new action required object
            $ar = new ActionRequired();

            $partnerID = "";
            $obj = new $_GET['object']($_GET['id'], true);
            $object_type = get_class($obj);

            //this will only get used for tickets at client level
            $policies = null;

            // get partner ID based on current object
            if ($object_type == "Client") {
                $partnerID = $obj->partner->id();

                $s = new Search(new Policy);
                $s->eq("client", $obj->id());

                $policies = [];

                while($p = $s->next(MYSQLI_ASSOC)){
                    $policies[] = $p;
                }
            } elseif ($object_type == "Policy") {
                $partnerID = $obj->client->partner->id();
            } elseif ($object_type == "Partner") {
                $partnerID = $obj->id();
            }

            echo new Template(('actionrequired/new.html'), [
                'ar' => $ar,
                'partnerID' => $partnerID,
                'object' => $obj,
                'object_type' => $object_type,
                'policies' => $policies
            ]);

            break;

        /**
         * Gets and loads data into the action required profile
         * Params in (id)
         * Params out Template(array($fs))
         */
        case "ar_view":
            //Get the action required object
            $ar = new Search(new ActionRequired);
            $ar->eq('id', $_GET["id"]);
            $fs = $ar->next(MYSQLI_ASSOC);

            echo new Template('actionrequired/profile.html', ["actionrequired" => $fs]);

            break;

        case "ar_complete_confirm":
            $json = [
                "id" => null,
                "message" => false,
                "error" => false
            ];

            if (isset($_GET['policy_status']) && $_GET['policy_status'] != "null"){
                $policy = new Policy($_GET['related_id'], true);

                $policy->status($_GET['policy_status']);

                if (!$policy->save()){
                    $json['error'] = true;
                    $json['message'] = "Failed to update policy status";
                }
            }

            //Get object
            $ar = new ActionRequired($_GET["object_id"], true);

            if ($_GET["verification_code"] == $ar->verification_code()) {
                //Change the archived boolean to true
                $ar->completed_by(User::get_default_instance('myps_user_id'));
                $ar->completed_when(time());

                if (!$ar->save()) {
                    $json['error'] = true;
                    $json['message'] = "Failed to complete Action Required, contact IT";
                } else {
                    // call the on_update method
                    $ar->on_update();

                    // send an email to the partner
                    $partner_addressee = addslashes(str_replace("&", "and", $ar->partner->addressee()));

                    // check time and give custom greeting in email
                    (date('G') <= 11) ? $greeting = "Good Morning" : $greeting = "Good Afternoon";
                    ($partner_addressee && $partner_addressee != "") ?
                        $greeting = $greeting . " " . $partner_addressee . ", " :
                        $greeting = $greeting . ", ";

                    // we want to email partner to inform them of change
                    //TODO check if this is where we should be sending from
                    $sender_email = strval($ar->get_allocated()->email_address());
//                    $sender_email = "partnersupport@policyservices.co.uk";

                    // use internal email address for testing otherwise use partner email
                    if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
                        $recipient[]['address']['email'] = "sean.ross@policyservices.co.uk";
                        $recipient[]['address']['name'] = "Sean";
                    } else {
                        $recipient[]['address']['email'] = $ar->partner->email();
                        $recipient[]['address']['name'] = $partner_addressee;
                    }

                    $subject = "Action Required - Ticket Closed";
                    $from_name = "Policy Services Ltd";
                    $sender = ['name' => $from_name, 'email' => $sender_email];

                    // build up main content of email body
                    $content_string = "<p>An action required ticket (ref: " . $ar->id() . ") which had previously been opened against your account has now been closed.</p>
                                <p>For more information on the ticket, and to see why it was closed, please visit your <a href='https://www.mypsaccount.com/action_required/id/" . $ar->id() . "'>myPSaccount</a>.</p>";

                    $body = json_encode([
                        "SUBJECT" => $subject,
                        "FROM_NAME" => $from_name,
                        "SENDER" => $sender_email,
                        "REPLY_TO" => $sender_email,
                        "ADDRESSEE" => $greeting,
                        "CONTENT_STRING" => $content_string
                    ]);

                    // attempt email with ol' sparky
                    if (sendSparkEmail($recipient, $subject, 'generic-partner', $body, $sender)) {
                        $json['message'] = "Action Required marked as complete, partner emailed";
                    } else {
                        $json['message'] = "Action Required marked as complete, partner email failed";
                    }
                }
            } else {
                $json['error'] = true;
                $json['message'] = "Verification code incorrect";
            }

            echo json_encode($json);

            break;

        /**
         * This is the case which provides a popup for validating an archive of an action required
         * Params in (object_id, verification_code)
         * Params out $json(id, message, error)
         */
        case "ar_archive_confirm":
            $json = [
                "id" => null,
                "message" => false,
                "error" => false
            ];

            if (isset($_GET['policy_status']) && $_GET['policy_status'] != "null"){
                $policy = new Policy($_GET['related_id'], true);

                $policy->status($_GET['policy_status']);

                if (!$policy->save()){
                    $json['error'] = true;
                    $json['message'] = "Failed to update policy status";
                }
            }

            //Get object
            $ar = new ActionRequired($_GET["object_id"], true);

            if ($_GET["verification_code"] == $ar->verification_code()) {
                //Change the archived boolean to true
                $ar->archived(1);
                $ar->completed_by(User::get_default_instance('myps_user_id'));
                $ar->completed_when(time());

                if (!$ar->save()) {
                    $json['error'] = true;
                    $json['message'] = "Everything saved incorrectly";
                } else {
                    // call the on_update method
                    $ar->on_update();

                    $json['error'] = false;
                    $json['message'] = "Everything saved correctly";
                };
            } else {
                $json['error'] = true;
                $json['message'] = "Verification code incorrect";
            }
            echo json_encode($json);

            break;

        /** NOT USED YET
         * This case provides a popup for to confirm the deletion of an action required
         * Params in id
         * Params out Template(ar_object)
         */
        case "ar_delete":
            //Get the action required message object
            $ar = new ActionRequired($_GET["id"], true);
            echo new Template('actionrequired/delete.html', ["ar_object" => $ar]);

            break;

        case "ar_complete":
            //Get the action required message object
            $ar = new ActionRequired($_GET["id"], true);
            echo new Template('actionrequired/complete.html', ["ar_object" => $ar]);
            break;

        /**
         * This gets the messages
         * Params in (object_id, verification_code)
         * Params out Template(array($fs))
         */
        case "messages_get":
            $json = [
                "id" => null,
                "message" => false,
                "error" => null,
                "uploads" => null
            ];

            //Get the action required message object
            $s = new Search(new ActionRequiredMessage);
            $s->eq('action_required_id', $_GET["id"]);

            //Get the action required object
            $ar = new Search(new ActionRequired);
            $ar->eq('id', $_GET["id"]);
            $fs = $ar->next(MYSQLI_ASSOC);

            $dd = "";
            $upload = "";

            while ($message = $s->next(MYSQLI_ASSOC)) {
                //Get the action required object
                $arm = new Search(new DocumentStorage);
                $arm->eq('object_id', $message->id());
                $arm->eq('object', "ActionRequiredMessage");

                //Check whether it was someone from policy services who sent the message or not
                // TRUE = PS / FALSE = Partner/Client/Etc

                if($message->alert()){
                    $dd .= "<div class='bg-light text-secondary'>
                              <div class='px-2'>
                                  <div class='row no-gutters'>
                                    <div class='col-3'>
                                        <p class='my-1 text-break'>" . $message->addedby . "</p>
                                    </div>
                                    <div class='col-6 text-center'>
                                        <p class='my-1'>" . $message->message . "</p>
                                    </div>
                                    <div class='col-3 text-right'>
                                        <p class='my-1'>" . $message->addedwhen . "</p>
                                    </div>
                                  </div>
                              </div>
                            </div>";
                } else {
                    if ($message->internal()) {
                        $dd .= "<div class='clearfix bg-white'><span class='chat-img pull-right'>
                            <img src=" . el::image('avatar-ps.png') . " alt='User Avatar' class='img-circle' />
                        </span>
                            <div class='chat-body clearfix'>
                                <div class='header'>
                                    <small class='text-muted'>
                                    <span class='glyphicon glyphicon-time'></span>
                                    </small>
                                    <strong class='pull-right primary-font'>" . $message->addedby . "</strong>";


                        if ($message->open_by() != null && $message->open_when() != null) {
                            $dd .= "<strong class='primary - font  pull-right' style='margin-bottom:5px;'  title='Status: Read'>
                                <i class='fa fa-check' aria-hidden='true'></i>
                                </strong>";
                        } else {
                            $dd .= "<strong class='primary - font' style='margin-bottom:5px;'  title='Status: Unread'>
                                </strong>";
                        }

                        // If the message has been edited
                        if ($message->updated_by() != null && $message->updated_when() != null) {
                            $dd .= "<strong class=\"primary - font\" style='margin-bottom:5px;'  >
                                <span>Edited</span>
                                </strong>";
                        } else {
                            $dd .= "<strong class=\"primary - font\" style='margin-bottom:5px;' >
                                <span></span>
                                </strong>";
                        }

                        $dd .= " </div>
                                <div style='font-size:smaller;margin-bottom:5px;'>" . $message->addedwhen . "</div>
                                 <div class='messagebox internal-reply'><p data-message='" . $message->id . "' contenteditable='" . ($message->open_when() == null ? 'true' : 'false') . "'>" . htmlspecialchars_decode($message->message) . "</p></div>
                            </div>
                             <div style='visibility: hidden' class='messageID' id='" . $message->id . "'></div>
                             </div>
                   ";
                    } elseif ($message->internal() === false || $message->internal() != 1) {
                        //If the message is not from someone within policy services
                        $dd .= "<div class='clearfix bg-white'><span class='chat-img pull-left'>
                            <img  src=" . el::image('avatar-SJP.png') . " alt='User Avatar' class='img-circle' />
                        </span>
                            <div class='chat-body clearfix'>
                                <div class='header'>
                                    <strong class='primary-font' style='margin-bottom:5px;'>" . $message->addedby . "</strong> 
                                   ";

                        if ($message->open_by() != null && $message->open_when() != null) {
                            $dd .= "<strong class='primary - font' style='margin-bottom:5px;'  title='Status: Read'>
                                <i class='fa fa-check' aria-hidden='true'></i>
                                </strong>";
                        } else {
                            $dd .= "<strong class='primary - font' style='margin-bottom:5px;'  title='Status: Unread'>
                                </strong>";
                        }

                        //If the message has been edited
                        if ($message->updated_by() != null && $message->updated_when() != null) {
                            $dd .= "<strong class='primary - font' style='margin-bottom:5px;'  >
                                <span>Edited</span>
                                </strong>";
                        } else {
                            $dd .= "<strong class='primary - font' style='margin-bottom:5px;' >
                                <span></span>
                                </strong>";
                        }

                        $redisClient = RedisConnection::connect();

                        if ($redisClient->exists("ar_acknowledge." . $message->id())){
                            $dd .= "<span class='badge badge-pill badge-success'><i class='fas fa-check'></i> Acknowledged</span>";
                        } else {
                            $dd .= "<button class='acknowledge-message' data-id='" . $message->id() . "'><i class='fas fa-check'></i> acknowledge</button>";
                        }

                        $dd .= " <div class='pull-right' style='font-size:smaller;margin-bottom:5px;'>" . $message->addedwhen .
                            "</div> 
                                    </div>
                                    <small class='pull-right text-muted'>
                                        <span class='glyphicon glyphicon-time'></span>
                                    </small>
                                </div>";

                        $dd .= "<div class='messagebox' contenteditable='false' ><p>" . htmlspecialchars_decode(utf8_encode($message->message)) . "</p></div>
                            </div>
                            <div style='visibility: hidden'  class='messageID' id='" . $message->id . "'></div>
                  ";
                    }
                }

                while ($armid = $arm->next(MYSQLI_ASSOC)) {
                    if ($armid) {
                        $iconClass = 'fa fa-paperclip';

                        //Build up download link for s3 stored files
                        $file_links = "<div id='doc-link-" . $armid->id() . "' class='link'>
                           <i class='" . $iconClass . " pull-left' aria-hidden='true'></i>
                            <div class='filename' >
                                <span id='" . $armid->id() . "'  class='viewAttachment'>" . $armid->friendly_name() . "</span>
                                <i class='fa fa-trash' aria-hidden='true'></i>
                            </div>
                           </div>";

                        $dd .= $file_links;
                        $upload .= $file_links;
                    }
                }

                $dd .= "</ul>";
            }

            $ards = new Search(new DocumentStorage());
            $ards->eq("object", "ActionRequired");
            $ards->eq("object_id", $_GET['id']);

            while ($armid = $ards->next(MYSQLI_ASSOC)) {
                if ($armid) {
                    $iconClass = 'fa fa-paperclip';

                    //Build up download link for s3 stored files
                    $file_links = "<div id='doc-link-" . $armid->id() . "' class='link'>
                           <i class='" . $iconClass . " pull-left' aria-hidden='true'></i>
                            <div class='filename' >
                                <span id='" . $armid->id() . "'  class='viewAttachment'>" . $armid->friendly_name() . "</span>
                                <i class='fa fa-trash' aria-hidden='true'></i>
                            </div>
                           </div>";

                    $upload .= $file_links;
                }
            }

            $json['message'] = $dd;
            $json['uploads'] = $upload;

            echo json_encode($json);

            break;

        case "all":
            $response = [
                "data" => "",
                "pages" => 0,
            ];

            $resultsPerPage = $_GET['show'];

            $db = new mydb(REMOTE);

            //grab the action required ticket, its last message, and whoever is assigned to the ticket (if any)
            $query = "SELECT ar.id, arm.added_when, arm.internal, arm.id as message_id
                      FROM " . REMOTE_SYS_DB . ".action_required ar
                        left join (
							SELECT *
							from " . REMOTE_SYS_DB . ".action_required_message arm
								inner join (
									SELECT MAX(id) as max_id
									FROM " . REMOTE_SYS_DB . ".action_required_message
									WHERE alert = 0
									group by action_required_id
								) marm on marm.max_id = arm.id
                        ) arm on arm.action_required_id = ar.id
                        left join (
							select * 
							from " . REMOTE_SYS_DB . ".action_required_assigned ara
								inner join (
									select max(id) as max_id
									from " . REMOTE_SYS_DB . ".action_required_assigned ara
									group by ara.ar_id
								) mara on mara.max_id = ara.id
                        ) ara on ar.id = ara.ar_id ";

            if ($_GET['oc'] == "closed"){
                $query.= " WHERE (completed_by is not null OR archived = 1) ";
            } else {
                $query .= " WHERE completed_by is null AND archived = 0 ";
            }

            //filter by the ticket chase date
            if ((isset($_GET['dateFrom']) && $_GET['dateFrom'] != "//")
                || (isset($_GET['dateTo']) && $_GET['dateTo'] != "//")){
                $dateFrom = 0;
                $dateTo = date("U");

                if (isset($_GET['dateFrom']) && $_GET['dateFrom'] != "//"){
                    $dateFrom = Carbon::parse(str_replace("/", "-", $_GET['dateFrom']))->startOfDay()->format("U");
                }

                if (isset($_GET['dateTo']) && $_GET['dateTo'] != "//"){
                    $dateTo = Carbon::parse(str_replace("/", "-", $_GET['dateTo']))->endOfDay()->format("U");
                }

                $query .= " AND (ar.date_due BETWEEN " . (int)$dateFrom . " AND " . (int)$dateTo . ") ";
            }

            //filter by partner
            if (isset($_GET['partner']) && $_GET['partner'] != 0){
                $query .= " AND ar.partnerID = " . $_GET['partner'] . " ";
            }

            //filter by subject
            if (isset($_GET['subject']) && $_GET['subject'] != ""){
                $query .= " AND ar.subject_id = " . $_GET['subject'] . " ";
            }

            //filter by PS staff assigned department
            if (isset($_GET['dept']) && $_GET['dept'] != "--"){
                $s = new Search(new User);
                $s->eq('department', $_GET['dept']);

                $myps_id_array = [];

                while($user = $s->next(MYSQLI_ASSOC)){
                    if ($user->myps_user_id()){
                        $myps_id_array[] = (int)$user->myps_user_id();
                    }
                }

                $extras = [];

                //special cases
                switch($_GET['dept']){
                    case 14:
                        //IO
                        $myps_id_array[] = 3419; //Michelle

                        break;

                    case 3:
                    case 16:
                        //New Business Admin & Advice
                        $myps_id_array[] = 2586; //Louise Gibson
                }

                $myps_id_array = array_unique($myps_id_array);

                //check if ticket has assigned user, if not, just use whoever added it
                $query .= " AND IF(ara.user_id is not null, ara.user_id in (" . implode(', ', $myps_id_array) . "), ar.added_by in (" . implode(', ', $myps_id_array) . ")) ";
            }

            if (isset($_GET['user']) && $_GET['user'] != "-"){
                //check if ticket has assigned user, if not, just use whoever added it
                $query .= " AND IF(ara.user_id is not null, ara.user_id = " . $_GET['user'] . ", ar.added_by = " . $_GET['user'] . ") ";
            }

            if (isset($_GET['id']) && $_GET['id'] != ""){
                $query .= " AND ar.id = " . $_GET['id'] . " ";
            }

            if (isset($_GET['lastMessage']) && $_GET['lastMessage'] != "all"){
                $query .= " AND arm.internal = " . $_GET['lastMessage'] . " ";

                if ($_GET['lastMessage'] == 0){
                    $ids = [];

                    $redisClient = RedisConnection::connect();

                    foreach(new Iterator\Keyspace($redisClient, "ar_acknowledge.*") as $key){
                        $ids[] = str_replace("ar_acknowledge.", "", $key);
                    }

                    if (!empty($ids)){
                        $query .= " AND arm.id not in (" . implode(',', $ids) . ") ";
                    }
                }
            }

            $query .= " GROUP BY ar.id 
                ORDER BY ar.date_due, ar.added_when ";

            $db->query($query);

            $response['pages'] = ceil($db->affected_rows/$resultsPerPage);

            $start = $_GET['offset'] - 1;

            $start < 0 ? $start = 0 : $start = $start;

            $query .= " LIMIT " . ($start * $resultsPerPage) . ", " . $resultsPerPage;

            $db->query($query);

            while($row = $db->next(MYSQLI_ASSOC)){
                $ar = new ActionRequired($row['id'], true);

                $lastMessageTime = null;
                $lastMessageText = null;

                if ($row['added_when']){
                    $lastMessageTime = $row['added_when'];

                    if ($row['internal']){
                        $lastMessageText = "To Partner: ";
                    } else {
                        $lastMessageText = "From Partner: ";
                    }

                    $lastMessageText .= Carbon::createFromTimestamp($lastMessageTime)->toDayDateTimeString();
                }

                $rowClasses = [];

                if ($ar->date_due() != null){
                    $dateDue = Carbon::createFromTimestamp($ar->date_due());
                    $ddDiff = $dateDue->copy()->diffInDays(Carbon::now());
                    $dateDueString = $dateDue->toFormattedDateString();

                    //if chase date is has passed
                    if ($dateDue->isPast() || $dateDue->isToday()){
                        $rowClasses[] = "table-danger";
                    } elseif($ddDiff < 7) {
                        //if chase date is upcoming in 7 days
                        $rowClasses[] = "table-warning";
                    }
                }

                $redisClient = RedisConnection::connect();

                //if last message was from partner, turn row red if not yet acknowledged
                if (!$row['internal'] && !$redisClient->exists("ar_acknowledge." . $row['message_id'])){
                    $rowClasses[] = "table-danger";
                }

                $object = $ar->object();
                $ob = new $object($ar->object_id(), true);

                $inProgress = "badge-warning";
                $progressTitle = "Ticket not being worked";
                $progressIcon = "fas fa-clock";

                if ($redisClient->exists("ar_progress." . $ar->id())){
                    $parts = explode(".", $redisClient->get("ar_progress." . $ar->id()));

                    $u = new User($parts[0], true);

                    $inProgress = "badge-success";
                    $progressTitle = "Ticket is being worked by " . $u->staff_name();
                    $progressIcon = "fas fa-check";
                }

                $relatesTo = $ar->object . " : " . $ob->link();
                if (!empty($ar->client_label)) {
                    $relatesTo .= "<br /><span class='amber_news' title='Client Label'><i>".$ar->client_label."</i></span>";
                }

                $response['data'] .= "<tr class='" . end($rowClasses) . "'>
                        <td><input class='ar_bulk_check' type='checkbox' value='" . $ar->id() . "'></td>
                        <td class='py-2'><span class='badge badge-pill " . $inProgress . "' title='" . $progressTitle . "'><i class='" . $progressIcon . "'></i></span>&nbsp;" . $ar->link($ar->subject()) . "</td>
                        <td data-sort='" . $ar->date_due . "'>" . $dateDueString . "</td>
                        <td>" . $relatesTo . "</td>
                        <td>" . $ar->partner->link() . "</td>
                        <td>" . $ar->get_allocated()->link() . "</td>
                        <td data-sort='" . $lastMessageTime . "'>" . $lastMessageText . "</td>
                    </tr>";
            }

            echo json_encode($response);

            break;
    }
} elseif (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "save_reply":
            $json = [
                "id" => null,
                "message" => false,
                "error" => null
            ];

            if (empty($_GET['data'])) {
                $json["error"] = true;
                $json["message"] = "Please enter a reply.";
            } else {
                //Internal left as 1 because only internal can reply through prophet.
                $arm = new ActionRequiredMessage();
                $arm->action_required_id($_GET['id']);
                $arm->message($_GET['data']);
                $arm->internal(1);
                $arm->addedwhen(time());

                if ($arm->save()) {
                    $json["error"] = false;
                    $json["message"] = "message added successfully";
                    $json["id"] = $arm->id();

                    $ar = new ActionRequired($_GET['id'], true);

                    if ($ar->get_allocated()->id() !== User::get_default_instance("myps_user_id")){
                        // this ticket is not assigned to me, change that!
                        $ara = new ActionRequiredAssigned();

                        $ara->action_required($ar->id());
                        $ara->assigned(User::get_default_instance("myps_user_id"));

                        if (!$ara->save()){
                            $json["message"] = "message added successfully, but failed to reassign ticket";
                        }
                    }
                } else {
                    $json["error"] = true;
                    $json["message"] = "message not added";
                };
            }
            echo json_encode($json);

            break;

//            case "delete_message":
//
//            $json = [
//                "status" => false,
//                "error" => false,
//                "dir" => false,
//                "counter" => false
//            ];
//
//                $db = new mydb;
//
//                $q = " DELETE FROM webauth.action_required_message WHERE id = ".$_GET["id"];
//
//                if ($db->query($q))
//                {
//                    $json["error"] = false;
//                    $json["message"] = "message deleted successfully";
//                } else
//                {
//                    $json["error"] = true;
//                    $json["message"] = "message not deleted";
//                };
//
//
//            echo json_encode($json);
//
//            break;

        case "edit_message":
            $json = [
                "status" => false,
                "error" => false,
                "message" => false
            ];
            $arm = new ActionRequiredMessage($_GET["id"], true);

            // get text when request submitted
            $arm->message($_GET['text']);
            $arm->updated_by($_COOKIE['uid']);
            $arm->updated_when(time());

            if ($arm->save()) {
                $json["error"] = false;
                $json["message"] = "message edited successfully";
            } else {
                $json["error"] = true;
                $json["message"] = "failed to update message";
            };

            echo json_encode($json);

            break;

        case "uploadpopup":
//            //Get the action required object
//            $ar = new Search(new ActionRequired);
//            $ar->eq('id', $_GET["id"]);
//            $fs = $ar->next(MYSQLI_ASSOC);

            echo new Template('actionrequired/uploadform.html');

            break;

        case "existing_check":
            $json = [
                "data" => []
            ];

            $search = new Search(new ActionRequired());
            $search->eq("object", $_GET['object']);
            $search->eq("object_id", $_GET['object_id']);
            $search->eq("completed_when", null);

            $i = 0;
            while ($ar = $search->next((MYSQLI_OBJECT))) {
                $json['data'][$i]['link'] = $ar->link();
                $json['data'][$i]['id'] = $ar->id();
                $i++;
            }

            echo json_encode($json);

            break;

        case "add_fes_ar":

            $json = [
                "status" => false,
                "error" => false,
                "ar_id" => false,
                "ar_message_id" => false
            ];

            $ar = new ActionRequired();
            $ar->subject_id($_GET['subject']);
            $ar->object($_GET['object']);
            $ar->object_id($_GET['object_id']);
            //$ar->priority($_GET['priority']);
            $ar->partner($_GET['partner']);

            if (isset($_GET['date_due']) && $_GET['date_due'] != "--"){
                $dateDue = date("U", strtotime($_GET['date_due']));
                $ar->date_due($dateDue);
            }

            if ($ar->save()) {
                $json['ar_id'] = $ar->id();
                $arm = new ActionRequiredMessage();
                $arm->action_required_id($ar->id());
                $arm->message($_GET['message']);
                if ($arm->save()) {
                    $json['ar_message_id'] = $arm->id();
                    $json['status'] = "Action Required message saved successfully";
                } else {
                    $json['error'] =  true;
                    $json['status'] =  "Action required ticket added but message failed to add, Contact IT";
                }
            } else {
                $json['error'] =  true;
                $json['status'] =  "Action required ticket failed to add, Contact IT";
            }

            echo json_encode($json);
            break;

        case "add_fes_ar_message":

            $json = [
                "status" => false,
                "error" => false,
                "ar_id" => $_GET['ar_id'],
                "ar_message_id" => false
            ];

            // make sure we have an action required object that matches the ID passed through
            $ar = new ActionRequired($_GET['ar_id']);

            // add new action required message and pass ID back to front end
            $arm = new ActionRequiredMessage();
            $arm->action_required_id($ar->id());
            $arm->message($_GET['message']);
            if ($arm->save()) {
                $json['ar_message_id'] = $arm->id();
                $json['status'] = "Action Required message saved successfully";
            } else {
                $json['error'] =  true;
                $json['status'] =  "Action required ticket added but message failed to add, Contact IT";
            }
            echo json_encode($json);
            break;

        case "fes_upload":

            $json = [
                "status" => false,
                "error" => false,
                "file_id" => false
            ];

            $policy = new Policy($_GET['policy_id'], true);
            $partnerID = $policy->client->partner->id();
            $practiceID = $policy->client->partner->get_practice()->id();


            if (isset($_FILES) && !empty($_FILES)) {
                // D&D upload
                foreach ($_FILES as $key => $file) {

                    try {
                        $hash = md5(file_get_contents($file['tmp_name']));
                        $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

                        $unique_name = $hash.".".$fileExtension;
                        $directory = $practiceID . "/" . $partnerID . "/" . $unique_name;

                        $result = $s3Client->putObject([
                            'Bucket' => $bucket,
                            'Key'    => $directory,
                            'Body'   => file_get_contents($file['tmp_name']),
                            'ServerSideEncryption' => 'AES256'
                        ]);

                        if ($result['@metadata']['statusCode'] == '200') {
                            // file successfully uploaded, add entry to database
                            $ds = new DocumentStorage();
                            $ds->partnerID($partnerID);
                            $ds->object_id($_GET['object_id']);
                            $ds->object($_GET['object']);
                            $ds->filepath($directory);
                            $ds->original_name($file['name']);
                            $ds->friendly_name($file['name']);
                            $ds->hash($unique_name);
                            $ds->mime_type($fileExtension);
                            $ds->file_size($file['size']);
                        }

                        if ($ds->save()) {
                            // file uploaded and entry in DB, all good!
                            $json['status'] = true;
                            $json['feedback'] = "File successfully uploaded.";
                        } else {
                            // file failed to add to DB, inform user
                            $json['status'] = false;
                            $json['feedback'] = "File not added to database.";
                        }

                    } catch( Exception $e ) {
                        $json['error'] = true;
                        $json['status'] = $e->getMessage();
                    }
                }
            } elseif (isset($_GET['filename'])) {
                // FES upload
                // assign file information to variable
                $f = pathinfo($_GET["filename"]);
                // get file extension
                $fileExtension = $f['extension'];
                $hash = md5(file_get_contents($_GET["filename"]));
                $unique_name = $hash.".".$fileExtension;
                $directory = $practiceID . "/" . $partnerID . "/" . $unique_name;

                try {
                    $result = $s3Client->putObject([
                        'Bucket' => $bucket,
                        'Key'    => $directory,
                        'Body'   => file_get_contents($_GET["filename"]),
                        'ServerSideEncryption' => 'AES256'
                    ]);

                    if ($result['@metadata']['statusCode'] == '200') {
                        // file successfully uploaded, add entry to database
                        $ds = new DocumentStorage();
                        $ds->partnerID($partnerID);
                        $ds->object_id($_GET['object_id']);
                        $ds->object($_GET['object']);
                        $ds->filepath($directory);
                        $ds->original_name($f['basename']);
                        $ds->friendly_name($f['basename']);
                        $ds->hash($unique_name);
                        $ds->mime_type($fileExtension);
                        $ds->file_size(filesize($_GET["filename"]));

                        if ($ds->save()) {
                            // file uploaded and entry in DB, all good!
                            $json['file_id'] = $ds->id();
                            $json['status'] = true;
                            $json['feedback'] = "File successfully uploaded.";
                        } else {
                            // file failed to add to DB, inform user
                            $json['status'] = false;
                            $json['feedback'] = "File not added to database.";
                        }
                    } else {
                        $json['status'] = false;
                        $json['feedback'] = "File not uploaded.";
                    }

                } catch( Exception $e ) {
                    $json['error'] = true;
                    $json['status'] = $e->getMessage();
                }
            } else {
                $json['error'] = true;
                $json['feedback'] = "File not found.";
            }

            echo json_encode($json);
            break;

        case "update_chase_date":
            $response = [
                "success" => true
            ];

            $ar = new ActionRequired($_GET['id'], true);

            $date = Carbon::createFromFormat("d/m/Y", $_GET['date'])->format("U");

            $ar->date_due($date);

            if ($ar->save()){
                $arm = new ActionRequiredMessage();

                $arm->action_required_id($ar->id());
                $arm->message("Chase Date updated to " . $_GET['date']);
                $arm->alert(1);

                $arm->save();
            } else {
                $response['success'] = false;
            }

            echo json_encode($response);

            break;

        case "list":
            echo new Template("actionrequired/list.html");

            break;

        case "reallocate":
            $response = [
                'success' => true,
                'link' => null,
            ];

            $user = new User($_GET['user'], true);

            $ara = new ActionRequiredAssigned();

            $ara->action_required($_GET['id']);
            $ara->assigned($user->myps_user_id());

            if ($ara->save()){
                // send the user an email to let them know
                $recipient = [
                    [
                        "address" => [
                            "email" => $user->email(),
                            "name" => $user->staff_name(),
                        ]
                    ]
                ];

                $subject = "Action Required Allocated to You!";
                $from_name = "Policy Services Ltd";
                $sender = ["name" => "IT", "email" => "it@policyservices.co.uk"];

                $content_string = "<p>An action required ticket (ref: " . $_GET['id'] . ") has been allocated to you!</p>";

                $body = json_encode([
                    "SUBJECT" => $subject,
                    "FROM_NAME" => $from_name,
                    "SENDER" => "it@policyservices.co.uk",
                    "REPLY_TO" => "it@policyservices.co.uk",
                    "ADDRESSEE" => $user->staff_name(),
                    "CONTENT_STRING" => $content_string,
                ]);

                sendSparkEmail($recipient, $subject, 'generic-staff', $body, $sender);

                $wuser = new WebsiteUser($user->myps_user_id(), true);

                $response['link'] = $wuser->link($wuser->first_name . " " . $wuser->last_name);
            } else {
                $response['success'] = false;
            }

            echo json_encode($response);

            break;

        case "leave_note":
            $response = [
                'success' => true,
                'feedback' => null,
            ];

            $ar = new ActionRequired($_GET['id'], true);

            $arm = new ActionRequiredMessage();

            $arm->action_required_id($_GET['id']);
            $arm->message($_GET['message']);
            $arm->alert(1);

            if($arm->save()){
                if (intval($_GET['reassign']) === 1){
                    if ($ar->get_allocated()->id() !== User::get_default_instance("myps_user_id")){
                        // this ticket is not assigned to me, change that!
                        $ara = new ActionRequiredAssigned();

                        $ara->action_required($ar->id());
                        $ara->assigned(User::get_default_instance("myps_user_id"));

                        if (!$ara->save()){
                            $response['feedback'] = "note added successfully, but failed to reassign ticket";
                        } else {
                            $response['feedback'] = "Saved note and reassigned ticket";
                        }
                    } else {
                        $response['feedback'] = "Saved note, ticket is already assigned to you";
                    }
                } else {
                    $response['feedback'] = "Saved note";
                }
            } else {
                $response['success'] = false;
                $response['feedback'] = "Failed to save note";
            }

            echo json_encode($response);

            break;

        case "start_progress":
            $ar = new ActionRequired($_GET['id'], true);

            $redisClient = RedisConnection::connect();
            $redisClient->set("ar_progress." . $ar->id(), User::get_default_instance("id") . "." . time());

            $indicator = $ar->progress_indicator();

            $response = [
                'success' => true,
                'data' => $indicator
            ];

            echo json_encode($response);

            break;

        case "end_progress":
            $ar = new ActionRequired($_GET['id'], true);

            $redisClient = RedisConnection::connect();
            $redisClient->del("ar_progress." . $ar->id());

            $response = [
                'success' => true,
                'data' => $ar->progress_indicator()
            ];

            echo json_encode($response);

            break;

        case "acknowledge_message":
            $redisClient = RedisConnection::connect();

            $redisClient->set("ar_acknowledge." . $_GET['id'], User::get_default_instance("id") . "." . time());

            echo json_encode([
                "success" => true,
                "data" => "Message Acknowledged",
            ]);

            break;

        case "bulk_update":
            $response = [
                "success" => true,
                "data" => null,
            ];

            try{
                $tickets = explode(",", $_POST['tickets']);

                foreach($tickets as $tid){
                    $ticket = new ActionRequired($tid, true);

                    if(!empty($_POST['chase']) && strlen($_POST['chase']) >= 4){
                        // full date has been supplied. update each of the chase dates
                        $ticket->date_due(Carbon::parse($_POST['chase'])->format("U"));

                        if($ticket->save()){
                            $arm = new ActionRequiredMessage();

                            $arm->action_required_id($ticket->id());
                            $arm->message("Chase Date updated to " . $_POST['chase']);
                            $arm->alert(1);

                            $arm->save();
                        } else {
                            throw new Exception("Bulk Update - Failed to save action required ticket: " . $ticket->id());
                        }
                    }

                    if(intval($_POST['completed'])){
                        // full date has been supplied, close each of the tickets
                        $ticket->completed_when(Carbon::now()->format("U"));
                        $ticket->completed_by(User::get_default_instance("id"));

                        if($ticket->save()){
                            $arm = new ActionRequiredMessage();

                            $arm->action_required_id($ticket->id());
                            $arm->message("Ticket was closed off via bulk update");
                            $arm->alert(1);

                            $arm->save();
                        } else {
                            throw new Exception("Bulk Update - Failed to close action required ticket: " . $ticket->id());
                        }
                    }

                    if(!empty($_POST['assigned']) && $_POST['assigned'] != "-"){
                        $user = new User($_POST['assigned'], true);

                        $ara = new ActionRequiredAssigned();

                        $ara->action_required($ticket->id());
                        $ara->assigned($user->myps_user_id());

                        if(!$ara->save()){
                            throw new Exception("Failed to update assigned person on bulk update for ticket: " . $ticket->id());
                        }
                    }

                    if(!empty($_POST['message'])){
                        if(!$ticket->add_message($_POST['message'])){
                            throw new Exception("Failed to add bulk message to ticket: " . $ticket->id());
                        }
                    }
                }

                if(!empty($_POST['assigned']) && $_POST['assigned'] != "-"){
                    // tickets were reassigned, send the user an email
                    $user = new User($_POST['assigned'], true);

                    $recipient = [
                        [
                            "address" => [
                                "email" => $user->email(),
                                "name" => $user->staff_name(),
                            ]
                        ]
                    ];

                    $subject = "Action Required Allocated to You!";
                    $from_name = "Policy Services Ltd";
                    $sender = ["name" => "IT", "email" => "it@policyservices.co.uk"];

                    $content_string = "<p>The following action required tickets have been allocated to you:<br>" . $_POST['tickets'] . "</p>";

                    $body = json_encode([
                        "SUBJECT" => $subject,
                        "FROM_NAME" => $from_name,
                        "SENDER" => "it@policyservices.co.uk",
                        "REPLY_TO" => "it@policyservices.co.uk",
                        "ADDRESSEE" => $user->staff_name(),
                        "CONTENT_STRING" => $content_string,
                    ]);

                    sendSparkEmail($recipient, $subject, 'generic-staff', $body, $sender);
                }
            } catch (Exception $e) {
                $response['success'] = false;
                $response['data'] = $e->getMessage();
            }

            echo json_encode($response);
    }
} else {
    echo new Template('actionrequired/plugin.html');
}
