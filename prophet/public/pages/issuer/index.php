<?php

require "$_SERVER[DOCUMENT_ROOT]/../bin/_main.php";

if (isset($_GET["id"])) {
    // Actions relating to one particular Issuer
    $issuer = new Issuer($_GET["id"], true);
    
    if (isset($_GET['get'])) {
        switch ($_GET['get']) {

            case "address_choice":

                $json = [
                    "data" => false,
                    "status" => false,
                    "error" => false,
                ];

                // get the issuer object
                $issuer = new Issuer($_GET['id'], true);

                $contact = new Search(new IssuerContact());
                $contact->eq('issuer_id', $issuer->id());
                $select .= "<option disabled>Issuer Address</option>";

                while ($c = $contact->next(MYSQLI_ASSOC)) {
                    if ($c->main() == 1) {
                        $select .= "<option value='" . $c->id() . "' name='" . $c->id() . "' > <b>Default</b> - " . $c->address . "</option>";
                    } else if (isset($c->group) && $c->group != "" && $c->group != null && strlen($c->group) >= 3) {
                        if (isset($c->address) && $c->address != "" && $c->address != null && strlen($c->address) > 3) {
                            $select .= "<option value='" . $c->id() . "' name='" . $c->id() . "' >" . $c->group . " - " . $c->address . "</option>";
                        }
                    }
                }

                $json['data'] = $select;

              echo json_encode($json);
            break;

            case "outstanding":
                if (isset($_GET['mode'])) {
                    $mode = $_GET['mode'];
                    $date = (isset($_POST["datefrom_y"]))?true:false;
                    $to = mktime(0, 0, 0, $_POST["dateto_m"], $_POST["dateto_d"], $_POST["dateto_y"]);
                    $from = mktime(0, 0, 0, $_POST["datefrom_m"], $_POST["datefrom_d"], $_POST["datefrom_y"]);
                    
                    if ($mode === "mandates") {
                        # return a tbody structure of this issuer's commission agencies
                        $s = Search(new Policy)
                            ->eq("status", 1)
                            ->eq("addedhow", 2)
                            ->eq("issuer", $issuer->id());
                            
                        $property = "addedwhen";
                    } elseif ($mode === "valuations") {
                        # return a tbody structure of this issuer's commission agencies
                        $s = Search(new Valuation)
                            ->eq("stage", 4)
                            ->eq("policy -> issuer", $issuer->id());

                        $property = "next_chase";
                    }

                    if ($date) {
                        $s -> btw($property, $from, $to);
                    }
                    
                    $tmpl = new Template($issuer->get_template_path("outstanding_row.html"));

                    while ($instance = $s->next()) {
                        $tmpl->tag($instance, "item");
                        $tmpl->tag($property, "outdate");
                        echo $tmpl;
                    }
                }

                break;
            
            case "list_alias":
                $json = [
                    "status" => false,
                    "error"  => false,
                    "data"  => false
                ];
                
                $db = new mydb();
                $q = "SELECT * FROM ".ISSUER_ALIAS_TBL." WHERE issuer_id = ".$_GET['id'];
                
                $db->query($q);

                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $json["data"] .= "<tr><td>".$row['alias']."</td></tr>";
                }
                
                if ($json['data'] != false) {
                    $json['status'] = true;
                }
                
                echo json_encode($json);
                
                break;
            
            case "add_alias":
                if (isset($_GET['alias'])) {
                    $i_a = new IssuerAlias();

                    $i_a->alias->set($_GET['alias']);
                    $i_a->issuer_id->set($_GET['id']);
                    
                    $json->status = (bool)$i_a->save();
                    
                    echo json_encode($json);
                } else {
                    echo Issuer::get_template("add_alias.html", ["issuer"=>$issuer]);
                }
                
                break;
        }
    } else {
        echo Issuer::get_template("profile.html", ["issuer"=>$issuer,"show_faxes"=>$issuer->accepts_fax_mandates]);
    }
} else {
    if (isset($_GET['do'])){
        switch($_GET['do']){
            case "client_search":
                $return = [
                    "success" => true,
                    "data" => ""
                ];

                $db = new mydb();

                $query = "SELECT " . CLIENT_TBL . ".clientID 
                    FROM " . POLICY_TBL . " 
                    INNER JOIN " . CLIENT_TBL . " 
                        ON " . POLICY_TBL . ".clientID = " . CLIENT_TBL . ".clientID  
                    WHERE issuerID = " . $_GET['issuer'] . " 
                    AND (clientFname1 LIKE '%" . $_GET['q'] . "%' 
                        OR clientSname1 LIKE '%" . $_GET['q'] . "%' 
                        OR clientFname2 LIKE '%" . $_GET['q'] . "%' 
                        OR clientSname2 LIKE '%" . $_GET['q'] . "%') 
                    GROUP BY " . POLICY_TBL . ".clientID";

                $db->query($query);

                while($c = $db->next(MYSQLI_ASSOC)){
                    $client = new Client($c['clientID'], true);

                    $return['data'] .= "<tr>
                        <td>" . $client->link($client->id()) . "</td>
                        <td>" . $client->link($client->one->name()) . "</td>
                        <td>" . $client->link($client->two->name()) . "</td>
                        <td>" . $client->partner->link($client->partner->name()) . "</td>
                        <td>" . $client->address . "</td>
                    </tr>";
                }

                echo json_encode($return);

                break;
        }

        exit(0);
    }

    function issuer_name_search(&$search, $str)
    {
        $search->eq($search->object->name, "%$str[text]%", true);
    }

    $search = new Search(new Issuer);
    $search->calc_rows = true;
    
    $search
        -> flag("id", Search::DEFAULT_INT)
        -> flag("name", Search::DEFAULT_STRING, Search::CUSTOM_FUNC, "issuer_name_search");

    if (isset($_GET["search"])) {
        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);

        $matches = [];

        while ($match = $search->next($_GET["search"])) {
            // append to matches
            $matches[ "results" ][] = ['value'=> "$match->id", 'text'=>"$match->name"];
        }
        
        $db = new mydb();
        $q = "SELECT * FROM ".ISSUER_TBL." INNER JOIN ".ISSUER_ALIAS_TBL." ON ".ISSUER_TBL.".issuerID = ".ISSUER_ALIAS_TBL." .issuer_id
				WHERE ".ISSUER_ALIAS_TBL.".alias LIKE '%".$_GET['search']."%'";
        
        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            $matches[ "results" ][] = ['value'=> $row['issuerID'], 'text'=>$row['alias']." (".$row['issuerName'].")"];
        }
        $matches["remaining"] = $search->remaining;

        echo json_encode($matches);
    } else {
        echo new Template("generic/search.html", ["help"=>$search->help_html(), "object"=>$search->get_object()]);
    }
}
