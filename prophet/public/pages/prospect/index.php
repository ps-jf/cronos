<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    $prospect = new Prospect;

    if (!(int)$_GET["id"]) {
        trigger_error("Invalid Prospect ID '$_GET[id]'", E_USER_ERROR);
    }

    if (!$prospect->get($_GET["id"])) {
        trigger_error("Prospect $_GET[id] not found", E_USER_ERROR);
    }

    if (isset($_GET['do'])) {
        switch ($_GET['do']) {
            case "open_account":
                $args = $prospect->flat_array();
                $args["do"] = "create";

                return Partner::get_common_url($args);

            case "map":
                if (!isset($_POST['prospect']['partner'])) {
                    echo new Template($prospect->get_template_path("map_partner.html"), ['prospect' => $prospect]);
                } else {
                    if (!(int)$_POST['prospect']['partner']) {
                        trigger_error("Invalid Partner ID '$_GET[id]'", E_USER_ERROR);
                    }

                    $partner = new Partner;

                    if (!$partner->get($_POST['prospect']['partner'])) {
                        trigger_error("Partner $_GET[id] not found", E_USER_ERROR);
                    }


                    //trigger a prospect save
                    $prospect->partner->set($_POST['prospect']['partner']);

                    $json = (object)[];

                    if ($prospect->save()) {
                        $json->status = true;
                    } else {
                        $json->status = false;
                    }

                    echo json_encode($json);
                }
                break;

            case "delete":
                try {
                    $p = new Prospect($_GET['id'], true);
                    $p->hidden(1);
                    $p->save();
                } catch (Exception $e) {
                    die($e->getMessage());
                }

                break;

            case "img_upload":
                $response = [
                    'success' => true,
                    'feedback' => null
                ];

                $file = $_FILES['prospect_img'];

                if ($file['type'] === "image/jpeg") {
                    try {
                        // check to see if we already have an image for the recruit
                        if (azureCheckExists(CDN_DIR . "recruit_images", $prospect->id() . ".jpg")) {
                            // remove the old file, keep things tidy
                            azureRemoveFile(CDN_DIR . "recruit_images", $prospect->id() . ".jpg");
                        }

                        // upload the new file
                        azureAddFile(CDN_DIR . "recruit_images", $file['tmp_name'], $prospect->id() . ".jpg");

                    } catch (Exception $e) {
                        $response['success'] = false;
                        $response['feedback'] = "Failed to upload prospect image: " . $e;
                    }
                } else {
                    // not allowed
                    $response['success'] = false;
                    $response['feedback'] = "Only .jpg files are allowed here";
                }

                echo json_encode($response);

                break;
        }
    }
} elseif (isset($_GET["partner_id"])) {
    if (isset($_GET['get']) && $_GET['get'] === 'notes') {
        $json = (object)['id' => false, 'link' => false];

        //get the recruit id
        $s = new Search(new Prospect);
        $s->eq('partner', $_GET['partner_id']);
        $p = $s->next();

        if ($p) {
            $json->id = $p->id();
            $json->link = $p->link();
        }

        echo json_encode($json);
    }
} elseif (isset($_GET["list"])) {
    echo new Template("prospect/index.html", ["object" => $prospect]);
} else if (isset($_GET['checklist'])) {
    $json = [
        "status" => false,
        "error" => false,
        "checklist_id" => false
    ];

    if ($_GET['checklist_id']) {
        $checklist = new TransferChecklist($_GET['checklist_id'], true);
    } else {
        $checklist = new TransferChecklist();
        $checklist->object_type($_GET['object_type']);
        $checklist->object_id($_GET['object_id']);
    }


    $field = str_replace("]", "", str_replace("transferchecklist[", "", $_GET['element']));
    $value = $_GET['value'];

    $checklist->$field($value);
    if ($checklist->save()) {
        $json["checklist_id"] = $checklist->id();

        $checklist_audit = new TransferChecklistAudit();
        $checklist_audit->checklist_id($checklist->id());
        $checklist_audit->checklist_item($field);
        $checklist_audit->field_value($value);

        if ($checklist_audit->save()) {
            $json["status"] = "Checklist updated";
        } else {
            $json["error"] = true;
            $json["status"] = "Checklist updated but not archived. Contact IT";
        }
    } else {
        $json["error"] = true;
        $json["status"] = "Checklist not updated. Try again or contact IT";
    }

    echo json_encode($json);
} else {

    function search_on_name(&$search_obj, $text)
    {

        $x = parse_name($text["text"]);
        $prospect = $search_obj->get_object();

        if ($x["wildcard"] == "forename") {
            $x["forename"] .= "%";
        } else {
            $x["surname"] .= "%";
        }

        $conditions = [];

        if (isset($x["forename"])) {
            $conditions[] = $search_obj->eq($prospect->firstname, $x["forename"], ($x["wildcard"] == "forename"), false);
        }

        $conditions[] = $search_obj->eq($prospect->surname, $x["surname"], ($x["wildcard"] == "surname"), false);
        $search_obj->add_and($conditions);
    }

    $prospect = new Prospect;
    $search = new Search($prospect);
    $search->set_flags([
        ["id", Search::DEFAULT_INT],
        ["name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING],
        ["practice", "practice"],
        ["postcode", "address->postcode", Search::PATTERN, "/" . Patterns::UK_POSTCODE . "/"]
    ]);


    if (isset($_GET["search"])) {
        $s = $_GET["search"];

        if (strlen($s) == 0) {
            exit();
        }

        $matches = [];

        $search->set_limit(SEARCH_LIMIT);
        $search->nt("hidden", 1);
        $search->set_offset((int)$_GET["skip"]);


        while ($match = $search->next($s)) {
            $matches["results"][] = ["text" => "$match", "value" => "$match->id"];
        }

        $matches["remaining"] = $search->remaining;

        echo json_encode($matches);
    } else {
        echo new Template("prospect/search.html", ["help" => $search->help_html(), "object" => $prospect]);
    }
}
