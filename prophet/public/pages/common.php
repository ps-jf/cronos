<?php
/**
 * Our version of a base controller.
 *
 * Mostly everything can be routed through here except for edge cases
 * It defines common(ha!) methods for database objects, like fetching profiles,
 * listing table rows etc
 *
 *
 * @package prophet.core
 * @author daniel.ness
 */

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

function response()
{
    return (object)[
        "id" => false,
        "status" => false,
        "error" => false,
        "data" => null
    ];
}

if (!isset($_GET["object"])) {
    trigger_error("Further parameters required ( GET=" . json_encode($_GET) . " )", E_USER_ERROR);
} elseif (!ctype_alnum($_GET["object"])) {
    trigger_error("Invalid object name '$_GET[object]'", E_USER_ERROR);
}

$object_name = ucfirst($_GET["object"]);
$object_arg = strtolower($object_name);
$object = new $object_name;

if (isset($_GET["id"]) || isset($_POST[$object_arg]["id"])) {
    $id = (isset($_GET["id"])) ? $_GET["id"] : $_POST[$object_arg]["id"];
    $object->primary_key($id);

    if (isset($_GET["get"])) {
        switch ($_GET["get"]) {
            case "json":
                echo json_encode($object->flat_array());
                break;

            case "form":
                $object->get();
                echo $object->get_template("form.html", ["$object_arg" => $object]);
                break;

            case "context":
                /***  Find + load correct XML-based context file ***/

                if (!$object->get()) {
                    trigger_error("Could not generate context menu for $object_name with ID '" . $object->id() . "'", E_USER_ERROR);
                }

                $file_path = $object->get_template_path("context.xml");

                if (file_exists($file_path)) {
                    $context = new ContextMenu($object, $file_path);
                    echo "$context";
                } else {
                    echo "false";
                }

                break;

            case "notes":

                $db = new mydb();

                $db->query("SELECT notes.*, user_name ".
                    "FROM " . NOTES_TBL . " " .
                    "INNER JOIN ".USR_TBL." ON ".USR_TBL.".id =".NOTES_TBL.".added_by ".
                    "WHERE object = '$object_name' " .
                    "AND is_visible = true ".
                    "AND object_id = ".$object->primary_key()." ".
                    "ORDER BY sticky DESC, parent_id, added_when DESC");

                $children = [];
                $root = [];

                while ($row = $db->next(MYSQLI_OBJECT)) {
                    if (empty($row->parent_id)) {
                        $root[] = $row;
                    } else {
                        $key = $row->parent_id;
                        if (!array_key_exists($key, $children)) {
                            $children[$key] = [];
                        }
                        $children[$key][] = $row;
                    }
                }

                function output_level($notes)
                {
                    global $children;
                    $thread = "";

                    $t = new Template("misc/notes/thread.html");
                    foreach ($notes as $n) {
                        $sublevel = (array_key_exists($n->id, $children)) ? output_level($children[$n->id]) : "";
                        $t->tag($n, "note");
                        $t->tag($sublevel, "child_notes");

                        $thread .= "$t";
                    }

                    return $thread;
                }

                echo Template("misc/notes/base.html", ["content"=>output_level($root)]);
                break;

            case "profile":
                $object->get();

                // register
                $_USER->register_recently_viewed($object);
                $redisClient->lpush(User::get_default_instance("id").".recently_viewed", json_encode(["object" => $object_name, "id" => $object->id(), "time" => date("U")]));
                $redisClient->ltrim(User::get_default_instance("id").".recently_viewed", 0, 9);

                echo Template($object->get_template_path("profile.html"), [$object_arg=>$object]);

                break;

            case "history":
                $s = new Search(new LocalLog);
                $s->eq("object", get_class($object));
                $s->eq("object_id", $object->id());
                $s->nt("snapshot", "");
                $s->add_order("id", Search::ORDER_DESCEND);


                function gen_audit_row($obj, $last_obj)
                {
                    $row = "";

                    if (!empty($obj) && is_array($obj->get_field_name_map())){
                        foreach ($obj->get_field_name_map() as $n => $f) {
                            $f = $obj->$n;

                            if (is_a($f, "Group")) {
                                $lo = ($last_obj !== false) ? $last_obj->{$n} : false;
                                $row .= gen_audit_row($f, $lo);
                            } else if (!$f->get_var(Field::BLOCK_UPDATE)) {
                                if ($f->display_name() == "Updatedby") {
                                    //dont display column
                                } else if ($f->display_name() == "Updatedwhen") {
                                    // don't display column
                                } else {
                                    $class = ($last_obj != false && $last_obj->{$n}() != $f()) ? "edit" : "";
                                    $row .= "<td class='$class'>" . ((is_a($f, "Sub")) ? $f->link() : $f) . "</td>\n";
                                }
                            }
                        }
                    }

                    return $row;
                }

                // output current data for selected Policy
                $last_obj = $object->get();

                $rows = "<tr>" .
                        "<td class='CURRENT'>CURRENT</td>" .
                        "<td>&nbsp;</td>" .
                        "<td>&nbsp;</td>" .
                        gen_audit_row($last_obj, false) .
                    "</tr>";

                while ($l = $s->next()) {

                    if ($l->user->id == 80) {

                        // establish remote db conn
                        $db = new mydb(REMOTE, E_USER_WARNING);
                        if (mysqli_connect_errno()) {
                            echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                            exit();
                        } else {
                            $db->query("select user_info.id, user_name
                            from ".ACTIONS_TBL."
                            inner join myps.data_change on ".ACTIONS_TBL.".datetime = data_change.committed_time
                            and ".ACTIONS_TBL.".object_id = data_change.object_id
                            inner join myps.user_info on data_change.proposed_by = user_info.id
                            where ".ACTIONS_TBL.".id = ".$l->id." and user_id = 80 ");

                            if ($row = $db->next(MYSQLI_OBJECT)) {

                                $s_1 = new Search(new WebsiteUser);
                                $s_1->eq("id", $row->id);

                                if ($web = $s_1->next(MYSQLI_ASSOC)) {
                                    $rows .= "<tr id='".$l->id."'>" .
                                        "<td class='" . $l->type . "'>" . $l->type . "</td>" .
                                        "<td>" . $web->link() . "</td>" .
                                        "<td>$l->date</td>" .
                                        gen_audit_row($l->object, $last_obj) .
                                        "</tr>";
                                } else {
                                    $rows .= "<tr id='".$l->id."'>" .
                                        "<td class='" . $l->type . "'>" . $l->type . "</td>" .
                                        "<td>Admin.</td>" .
                                        "<td>$l->date</td>" .
                                        gen_audit_row($l->object, $last_obj) .
                                        "</tr>";
                                }
                            } else {
                                $rows .= "<tr id='".$l->id."'>" .
                                    "<td class='" . $l->type . "'>" . $l->type . "</td>" .
                                    "<td>Admin</td>" .
                                    "<td>$l->date</td>" .
                                    gen_audit_row($l->object, $last_obj) .
                                    "</tr>";
                            }
                        }
                    } else {
                        $rows .= "<tr id='".$l->id."'>" .
                            "<td class='" . $l->type . "'>" . $l->type . "</td>" .
                            "<td>" . $l->user->link() . "</td>" .
                            "<td>$l->date</td>" .
                            gen_audit_row($l->object, $last_obj) .
                            "</tr>";
                    }
                    $last_obj = $l->object;
                }

                // display insert details
                ($object->addedby() != '0' && $object->addedby() != '') ?
                    $rows .= "<tr><td class='side_heading'>INSERT</td><td>" . $object->addedby->link() . "</td><td> " . $object->addedwhen . "</td><td colspan='100%'>&nbsp;</td></tr>" :
                    $rows .= "<tr><td class='side_heading'>INSERT</td><td>Unknown</td><td>Unknown</td><td colspan='100%'>&nbsp;</td></tr>";

                echo $rows;

                break;
        }
    } else if (isset($_GET["do"])) {
        $json = (object) ["status"=>false];

        switch ($_GET["do"]) {
            case "save":
                $json = response();

                if ($object_name == "Policy") {
                    if ($_REQUEST[$object_arg]['agency_id'] == "") {
                        unset($_REQUEST[$object_arg]['agency_id']);
                    }

                    if ($_REQUEST[$object_arg]['mandate'] == 0) {
                        unset($_REQUEST[$object_arg]['mandate']);
                    }
                }

                try {
                    //validates client date of birth
                    if((array_key_exists("one", $_REQUEST[$object_arg])
                            && !Date::valid_date($_REQUEST[$object_arg]['one']['dob']['D'],
                                     $_REQUEST[$object_arg]['one']['dob']['M'],
                                     $_REQUEST[$object_arg]['one']['dob']['Y']))
                        || (array_key_exists("two", $_REQUEST[$object_arg])
                            && !Date::valid_date($_REQUEST[$object_arg]['two']['dob']['D'],
                            $_REQUEST[$object_arg]['two']['dob']['M'],
                            $_REQUEST[$object_arg]['two']['dob']['Y'])))
                    {
                        throw new Exception("Invalid Date Of Birth");
                    }

                    if (!isset($_REQUEST[$object_arg])) {
                        throw new Exception("No compatible REQUEST array detected for import");
                    }

                    if (!$object->get()) {
                        throw new Exception("Could not load existing entry");
                    }

                    $object->load($_REQUEST[$object_arg]);

                    if (isset($_POST['breaches']['manager_response'])){
                        //fix for strange character when copy pasting emails
                        $object->manager_response(str_replace("‘", "'", $_POST['breaches']['manager_response']));
                    }

                    if($object->save()){
                        $redisClient->lpush(User::get_default_instance("id").".recent_activity", json_encode(["type" => "UPDATE", "object" => $object_name, "id" => $object->id(), "time" => date("U")]));
                        $redisClient->ltrim(User::get_default_instance("id").".recent_activity", 0 ,9);
                    }
                    $json->data = $object->on_update($_REQUEST[$object_arg]);

                    $json->id = $object->id();
                    $json->status = true;
                } catch (Exception $e) {
                    trigger_error($e->getMessage());
                    $json->error = $e->getMessage();
                }

                break;
            
            case "delete":
                /*
                  Delete $object's database entry and perform any
                  action in DatabaseObject->on_delete( )
                 */

                $db = new mydb($object->database);
                $db->autocommit(false);

                $object->use_db_link($db);

                try {
                    $object->on_delete();
                    if($object->delete()){ // maybe MyISAM table, so delete last so we can rollback
                        $redisClient->lpush(User::get_default_instance("id").".recent_activity", json_encode(["type" => "DELETE", "object" => $object_name, "id" => $object->id(), "time" => date("U")]));
                        $redisClient->ltrim(User::get_default_instance("id").".recent_activity", 0 ,9);
                    }
                    $db->autocommit(true); // commit changes
                    $json->status = true;
                } catch (Exception $e) {
                    $db->rollback();
                    $json->error = $e->getMessage();
                }

                break;
        }

        echo json_encode($json);
    }
} else if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "list":
            $s = new Search($object);
            if (isset($_GET["filter"])) {
                $or_mode = (isset($_GET["ormode"])) ? true : false;
                $s->apply_filters($_GET["filter"], $or_mode);
            }

            if (isset($_GET["limit"])) {
                if (isset($_GET["offset"])) {
                    $s->set_offset($_GET["offset"]);
                }

                $s->set_limit($_GET["limit"]);
            }

            switch ($_GET["type"] = (isset($_GET["type"]) ? $_GET["type"] : null)) {
                case "json":
                    $loop_func = function ($instance) {
                        return $instance->flat_array();
                    };

                    $return_func = function ($list) {
                        return json_encode($list);
                    };

                    break;

                default:
                    if (empty($_GET["excludes"])) {
                        $_GET["excludes"] = [];
                    }

                    $tmpl = new Template($object->get_template_path("list_row.html"), ["excludes" => $_GET["excludes"]]);

                    $loop_func = function ($instance) {
                        global $tmpl;
                        global $object_arg;
                        $tmpl->tag($instance, $object_arg);
                        return "$tmpl";
                    };

                    $return_func = function ($list) {
                        return implode("\n", $list);
                    };

                    break;
            }

            $list = [];
            while ($instance = $s->next()) {
                $list[] = $loop_func($instance);
            }

            echo $return_func($list);

            break;

        case "create":
            if (isset($_POST[$object->group_name])) {
                // Is client being added via mandate system?
                if (isset($_GET['mandate'])) {
                    $mandate = $_GET['mandate'];
                }
                $json = response();

                try {
                    // block correspondence from untraced account
                    if (isset($_POST['correspondence']['policy'])) {
                        $pol = new Policy($_POST['correspondence']['policy'], true);
                        if ($pol->client->partner->id() == 394 && ($_POST['correspondence']['letter'] != 15 && $_POST['correspondence']['letter'] != 37)) {
                            $json->status = false;
                            $json->id = false;
                            $json->error = "Correspondence cannot be added under the untraced account (394)";
                            echo json_encode($json);
                            exit();
                        }

                        if ($pol->client->partner->id() == 5527) {
                            if (!in_array(User::get_default_instance('id'), [60, 44, 201, 248]) && !User::get_default_instance('department') == 4) {
                                $json->status = false;
                                $json->id = false;
                            $json->error = "All correspondence for John Scarratt's account must be passed to Kayleigh";
                                echo json_encode($json);
                                exit();
                            }
                        }
                    }

                    if (isset($mandate)) {
                        // block client add at _settings level
                        if (array_key_exists($_POST['client']['partner'], PARTNER_CLIENT_ADD_BLOCK)){
                            $allowed = PARTNER_CLIENT_ADD_BLOCK[$_POST['client']['partner']];

                            if(!in_array(User::get_default_instance("id"), $allowed['users'])){
                                if(!in_array(User::get_default_instance("department"), $allowed['depts'])){
                                    if(!User::get_default_instance('team_leader') || !$allowed['team_leaders']){
                                        $json->status = false;
                                        $json->id = false;
                                        $json->error = "Clients are blocked from being added under this partner except for certain users, contact IT for more details.";
                                        echo json_encode($json);
                                        exit();
                                    }
                                }
                            }
                        }
                    }

                    $object->load($_POST[$object->group_name]);

                    // If client added via mandate system, set client->addedhow = 2
                    if (isset($mandate) && $mandate == 'yes') {
                        $object->addedhow(2);
                    }

                    if($object->save()){
                        $redisClient->lpush(User::get_default_instance("id").".recent_activity", json_encode(["type" => "CREATE", "object" => $object_name, "id" => $object->id(), "time" => date("U")]));
                        $redisClient->ltrim(User::get_default_instance("id").".recent_activity", 0 ,9);
                    }

                    /** If adding correspondence, we also wants to update policy
                     * parse policy_status into correspondence object from policy object
                     * PLEASE NOTE ONLY CORRESPONDENCE OBJECT GETS SAVED IN ' common.php:do=create ' **/
                    if (isset($_POST['correspondence']) && isset($_POST['policy'])) {
                        $_POST['correspondence']['policy_status'] = $_POST['policy']['status'];
                    }

                    $object->on_create($_POST[$object->group_name]);

                    $json->status = true;
                    $json->id = $object->id();
                } catch (Exception $e) {
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);
            } else {
                // Allow the passing of setup values e.g. a policy's client
                unset($_GET["do"]);
                $object->load($_GET);
                echo Template($object->get_template_path("new.html"), [$object_arg => $object]);
            }

            break;
    }
} else {
    // display front page
    echo Template($object->get_template_path("home.html"), [$object_arg=>$object]);
}
