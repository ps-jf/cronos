<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET['search'])) {
    $search = new Search(new OngoingService());

    $_GET['excludes'] = json_decode($_GET['excludes']);

    if (isset($_GET['filter'])) {
        $search->apply_filters(json_decode($_GET['filter']));
    }

    if (empty($_GET["excludes"])) {
        $_GET["excludes"] = [];
    }

    $t = OngoingService::get_template('list_row.html', ['excludes' => $_GET["excludes"]]);
    $out = "";

    $search->add_order('id', 'DESC');

    $array = [];

    while ($s = $search->next($_GET["search"])) {
        if (empty($s->parent_id())) {
            $array[$s->id()][$s->id()] = $s;
        } else {
            $array[$s->parent_id()][$s->id()] = $s;
        }
    }

    $i = count($array);
    foreach ($array as $k => $sub_array) {
        ksort($sub_array, SORT_NUMERIC);
        $t->tag($sub_array, "os");
        $t->tag($i, "review");
        $out .= "$t";
        $i--;
    }

    echo json_encode([
        "src" => $out
    ]);
}