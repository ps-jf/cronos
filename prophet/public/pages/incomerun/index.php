<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$redisClient = RedisConnection::connect();

if (isset($_GET['do'])){
    switch ($_GET['do']) {
        case "start_run":
            $response = [
                "success" => true,
                "data" => null,
            ];

            if(!$redisClient->set("incomerun", time())){
                $response['success'] = false;
                $response['data'] = "Failed to set income run flag";
            }

            echo json_encode($response);

            break;

        case "stop_run":
            $response = [
                "success" => true,
                "data" => null,
            ];

            if(!$redisClient->del("incomerun")){
                $response['success'] = false;
                $response['data'] = "Failed to unset income run flag";
            }

            echo json_encode($response);

            break;

        case "edit_instructions":
            $response = [
                "success" => true,
                "data" => "Successfully saved new instructions",
            ];

            if (!file_put_contents(INCOME_RUN_INSTRUCTIONS, $_POST['instructions'])){
                $response['success'] = false;
                $response['data'] = "Failed to save new instructions";
            }

            echo json_encode($response);

            break;
    }
} else {
    $runStarted = $redisClient->get("incomerun");

    $instructions = file_get_contents(INCOME_RUN_INSTRUCTIONS);

    echo new Template("incomerun/index.html", compact([
        "runStarted",
        "instructions"
    ]));
}