<?php
require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

$db = new mydb(REMOTE, E_USER_WARNING); // establish remote db conn
if (mysqli_connect_errno()) {
    echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
    exit();
}

if (isset($_GET["id"])) {
    if (isset($_GET["get"])) {
        switch ($_GET["get"]) {
            case "partners":
                  $q = "SELECT * FROM ".REMOTE_VIEWER_TBL." v
						INNER JOIN ".REMOTE_USR_TBL." u
						ON v.user_id = u.id WHERE v.user_id = ".$_GET['id'];

                  $db->query($q);
                    
                    
                while ($row=$db->next()) {
                    $partner = new Partner($row['partner_id'], true);

                    echo "<tr>
									<td>".$partner->link()."</td>
									<td>".$partner->address."</td>
									<td>".$partner->phone."</td>
								</tr>";
                }
            
                break;
                
            case "profile":
                $db = new mydb(REMOTE, E_USER_WARNING); // establish remote db conn
                if (mysqli_connect_errno()) {
                    echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                    exit();
                }
                $q = "SELECT * FROM ".REMOTE_USR_TBL." WHERE id = ".$_GET['id'];
                $db->query($q);
                    
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $user = $row;
                }

                echo Template("mypsusers/profile.html", ["id"=>$_GET['id'], "user"=>$user]);

                break;
        }
    }
} else {
    $matches = [];

    // look up subusers in REMOTE_SYS_DB.user_info
    $db = new mydb(REMOTE, E_USER_WARNING); // establish remote db conn
    if (mysqli_connect_errno()) {
        echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
        exit();
    }

    if (isset($_GET['search'])) {
        $search = "%" . str_replace(" ", "%", $_GET['search']) . "%";

        $limit = 15;
        $offset = empty($_GET['skip']) ? 0 : $_GET['skip'];

        $s = new Search(new WebsiteUser());
        $s->add_or([
            $s->eq("concat_ws(fname, sname)", $search, true, 0),
            $s->eq("email", $search, true, 0)
        ]);

        $s->set_limit($limit);
        $s->set_offset($offset);

        while ($row = $s->next(MYSQLI_ASSOC)) {
            $matches["results"][] = ["text" => $row->first_name()." ".$row->last_name(), "value" => $row->id(), "myPS"=>true];
        }


        if ($_GET['search'] != "") {
            echo json_encode($matches);
        }
    } else {
        echo new Template("mypsusers/search.html", []);
    }
}
