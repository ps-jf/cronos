<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["partner"])) {
    if (isset($_GET['subusers'])) {

        switch($_GET['subusers']){
            case "save_email_notifications":
                if (isset($_GET['id']) && isset($_GET['emails'])) {
                    $json = [
                        "status" => null,
                        "error" => false,
                    ];

                    $db = new mydb(REMOTE, E_USER_WARNING);
                    if (mysqli_connect_errno()) {
                        $json['status'] = "Update failed. Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }
                    $q = "UPDATE viewer_tbl SET email_notification = " . $_GET['emails'] . " WHERE id = " . $_GET['id'];
                    $update = $db->query($q);

                    if ($update) {
                        $json['status'] = "Entry successfully updated";
                    } else {
                        $json['status'] = "An error has occurred, Please contact IT.";
                        $json['error'] = true;
                    }

                    echo json_encode($json);
                }

                break;

            case "save_mandate_notifications":
                if (isset($_GET['id']) && isset($_GET['emails'])) {
                    $json = [
                        "status" => null,
                        "error" => false,
                    ];

                    $db = new mydb(REMOTE, E_USER_WARNING);
                    if (mysqli_connect_errno()) {
                        $json['status'] = "Update failed. Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }
                    $q = "UPDATE viewer_tbl SET mandate_notification = " . $_GET['emails'] . " WHERE id = " . $_GET['id'];
                    $update = $db->query($q);

                    if ($update) {
                        $json['status'] = "Entry successfully updated";
                    } else {
                        $json['status'] = "An error has occurred, Please contact IT.";
                        $json['error'] = true;
                    }

                    echo json_encode($json);
                }

                break;

            case "save_proreport_notifications":
                if (isset($_GET['id']) && isset($_GET['emails'])) {
                    $json = [
                        "status" => null,
                        "error" => false,
                    ];

                    $db = new mydb(REMOTE, E_USER_WARNING);
                    if (mysqli_connect_errno()) {
                        $json['status'] = "Update failed. Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }
                    $q = "UPDATE viewer_tbl SET proreport_notification = " . $_GET['emails'] . " WHERE id = " . $_GET['id'];
                    $update = $db->query($q);

                    if ($update) {
                        $json['status'] = "Entry successfully updated";
                    } else {
                        $json['status'] = "An error has occurred, Please contact IT.";
                        $json['error'] = true;
                    }

                    echo json_encode($json);
                }

                break;
        }

    } else {
        // Get a list of WebsiteUsers with access to Partner account
        $s = new Search(new ViewerEntry);
        $s->eq("partner", intval($_GET["partner"]));
        //$s->add_order( array("user->user_name") );

        #HACK
        // Search requires group() method

        $t = ViewerEntry::get_template("websiteuser_list_row.html");

        while ($ve = $s->next()) {
            $t->tag($ve, "viewerentry");
            $t->tag($ve->user->get_object(), "websiteuser");
            echo $t;
        }
    }
} else if (isset($_GET["user"])) {
    /// Get a list of Partners accessible by WebsiteUser account
    $s = new Search(new ViewerEntry);
    $s->eq("user", intval($_GET["user"]));
    $s->order(["partner->surname", "partner->forename"]);

    $t = ViewerEntry::get_template("partner_list_row.html");
    while ($ve = $s->next()) {
        $t->tag($ve, "viewerentry");
        $t->tag($ve->partner->get_object(), "partner");
        echo $t;
    }
} elseif (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "delete":
            $json = [
                "status" => null,
                "error" => false,
            ];

            $db = new mydb(REMOTE, E_USER_WARNING);
            if (mysqli_connect_errno()) {
                $json['status'] = "Update failed. Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                $json['error'] = true;
                echo json_encode($json);
                exit();
            }
            $q = "DELETE FROM viewer_tbl  WHERE id = " . $_GET['id'];
            $delete = $db->query($q);

            if ($delete) {
                $json['status'] = "Entry successfully deleted";
            } else {
                $json['status'] = "An error has occurred, Please contact IT.";
                $json['error'] = true;
            }

            echo json_encode($json);
            break;
    }
}
