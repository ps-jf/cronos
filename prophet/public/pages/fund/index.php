<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {


        case 'create':
            $json = [
                "status" => "An error occurred, try again or contact IT",
                "error" => true
            ];

            //get data passed through
            $fund_name = $_GET['name'];
            $fund_sedol = $_GET['sedol'];

            if (strlen($fund_name) > 0) {
                //check whether sedol code exists in table
                $exists = new Search(new Fund());
                $exists->eq('sedol', $fund_sedol);

                //if the sedol code does not exists
                if ($exists->next(MYSQLI_ASSOC) == false || $fund_sedol == "") {
                    $fund = new Fund();
                    $fund->name($fund_name);
                    $fund->sedol($fund_sedol);
                    if ($fund->save()) {
                        $json['status'] = 'added correctly';
                        $json['error'] = false;
                    }
                } else {
                    $json['status'] = 'SEDOL code already exists, fund not added.';
                    $json['error'] = true;
                }
            } else {
                $json['status'] = 'Please fill in both fields';
                $json['error'] = true;
            }

            echo json_encode($json);

            break;

        case 'get_create_form':

            //get form  for creating fund
            $fund = new Fund();
            echo NewBusiness::get_template("create_fund.html", ["fund" => $fund, "nbid" => $_GET['nbid']]);
            break;


        case 'getfunds':

            $funds_in = [];
            $funds_out = [];

            if (isset($_GET['id'])) {

                $s = new Search(new FundMap);
                $s->eq("nbid", $_GET['id']);
                $s->eq("direction", 'in');
                while ($f = $s->next(MYSQLI_ASSOC)) {
                    $funds_in[$f->id()]['fund_nb_map_id'] = $f->id();
                    $funds_in[$f->id()]['name'] = $f->fund_id->name();
                    $funds_in[$f->id()]['sedol'] = $f->fund_id->sedol();
                    $funds_in[$f->id()]['direction'] = ucfirst($f->direction());
                }

                $s_1 = new Search(new FundMap);
                $s_1->eq("nbid", $_GET['id']);
                $s_1->eq("direction", 'out');
                while ($f_1 = $s_1->next(MYSQLI_ASSOC)) {
                    $funds_out[$f_1->id()]['fund_nb_map_id'] = $f_1->id();
                    $funds_out[$f_1->id()]['name'] = $f_1->fund_id->name();
                    $funds_out[$f_1->id()]['sedol'] = $f_1->fund_id->sedol();
                    $funds_out[$f_1->id()]['direction'] = ucfirst($f_1->direction());
                }

                echo NewBusiness::get_template("funds.html", [
                    "funds_in" => $funds_in,
                    "funds_out" => $funds_out,
                    "nbid" => $_GET['id']
                ]);
            }

            break;

        case 'newfund':
            $fund = new Fund();
            echo NewBusiness::get_template("new_fund.html", ["fund" => $fund, "nbid" => $_GET['nbid']]);
            break;

        case "addMapping":

            $json = [
                "status" => "An error occurred, try again or contact IT",
                "error" => true
            ];

            if (isset($_GET['fund_id'])) {
                $mapping = new FundMap();
                $mapping->fund_id($_GET['fund_id']);
                $mapping->nbid($_GET['nbid']);
                $mapping->direction($_GET['fund_direction']);
                if ($mapping->save()) {
                    $json['status'] = "Fund added to New Business ticket";
                    $json['error'] = false;
                }
            } else {
                $json['status'] = "Fund is required";
            }

            echo json_encode($json);

            break;
    }
} elseif (isset($_GET['search'])) {

    try {

        $search = new Search(new Fund);
        $search->calc_rows = true;

        $search
            ->flag("name")
            ->flag("sedol");

        $search->add_or([
            $search->eq("name", "%" . $_GET['search'] . "%", true, 0),
            $search->eq("sedol", "%" . $_GET['search'] . "%", true, 0)
        ]);

        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);
        $matches = [];

        while ($match = $search->next()) {
            $matches["results"][] = ['value' => "$match->id", 'text' => $match->name . " (" . $match->sedol . ")"];
        }

        $matches["remaining"] = $search->remaining;

        echo json_encode($matches);

    } catch (Exception $e) {
        $e->getMessage();
    }
}
