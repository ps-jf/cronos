<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

$redisClient = RedisConnection::connect();

if (isset($_GET["id"])) {

    $partner = new Partner();

    if (!(int)$_GET["id"]) {
        trigger_error("Invalid Partner ID '$_GET[id]'", E_USER_ERROR);
    }

    if (!$partner->get($_GET["id"])) {
        trigger_error("Partner $_GET[id] not found", E_USER_ERROR);
    }

    if (isset($_GET['get'])) {
        switch ($_GET['get']) {

            case "hold_reason":
                $json = [
                  'feedback' => 'Reason not stored',
                  'status' => false,
                ];

                // add an entry to action reason
                $ar = new ActionReason();
                $ar->object("Partner");
                $ar->object_id($partner->id());
                $ar->reason($_GET['reason']);
                $ar->action("Hold Income");
                if ($ar->save()) {
                    $json['status'] = true;
                    $json['feedback'] = "Reason stored, this will be viewable on the History tab.";
                }

                echo json_encode($json);

                break;
            case "user":
                //// Find Partner's WebsiteUser account and return user id

                $json = (object)[];

                try {
                    $db = new mydb(REMOTE, E_USER_WARNING);
                    if (mysqli_connect_errno()) {
                        $json->status = "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                        $json->error = true;
                        echo json_encode($json);
                        exit();
                    }

                    $s = new Search(new WebsiteUser);
                    $s->eq("partner", $partner->id());
                    $s->limit(1);

                    if ($wu = $s->next()) {
                        $t = WebsiteUser::get_template("profile.html", ["websiteuser" => $wu]);
                        $json->profile = "$t";
                        $json->status = true;
                    } else {
                        $json->status = false;
                    }
                } catch (Exception $e) {
                    $json->error = $e->getMessage();
                    $json->status = false;
                }

                echo json_encode($json);

                break;

            case "partner_info":
                $s = new Search(new PartnerInfo());
                $s->eq("partner", $partner->id());

                if(!$partnerInfo = $s->next(MYSQLI_ASSOC)){
                    $partnerInfo = new PartnerInfo();
                    $partnerInfo->partner($partner->id());

                    $partnerInfo->save();
                }

                echo new Template("partner/info.html", compact([
                    "partner",
                    "partnerInfo"
                ]));

                break;

            case "pending_changes":
                $json = [];

                $db = new mydb(REMOTE, E_USER_WARNING);
                if (mysqli_connect_errno()) {
                    echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                    exit();
                }

                $db->query(
                    "SELECT " . REMOTE_CHANGES_TBL . ".*, user_name FROM " . REMOTE_CHANGES_TBL . " " .
                    "INNER JOIN " . REMOTE_USR_TBL . " ON changeBy=" . REMOTE_USR_TBL . ".id " .
                    "WHERE " . REMOTE_CHANGES_TBL . ".partnerID = " . $partner->id()
                );

                if (!$d = $db->next(MYSQLI_ASSOC)) {
                    exit;
                }

                foreach ($d as $key => $value) {
                    if (empty($d[$key])) {
                        unset($d[$key]);
                    }
                }

                // Updated Partner Object
                $partner->get();

                $up = new Partner($partner->id(), true);
                $up->load($d);

                if (isset($_POST["submit"])) {
                    $user = User::get_default_instance();

                    // Allow user to edit certain details pre-commit
                    if (isset($_POST["partner"])) {
                        $up->load($_POST["partner"]);
                    }

                    try {
                        if (isset($_POST["deny"])) {
                            $key = "cancelled";
                        } else {
                            $up->save();
                            $key = "enacted";
                        }

                        $db->query("UPDATE " . REMOTE_CHANGES_TBL . " set $key = true, authWhen = " . time() . ", authBy = " . $user->id() . " WHERE changeid = " . intval($d['changeID']));
                        $db->query("INSERT INTO " . REMOTE_CHANGES_AUDIT_TBL . " SELECT * FROM " . REMOTE_CHANGES_TBL . " WHERE changeid =" . intval($d['changeID']));
                        $db->query("DELETE FROM " . REMOTE_CHANGES_TBL . " WHERE changeid=" . intval($d["changeID"]));

                        $json["status"] = true;
                    } catch (Exception $e) {
                        $json["status"] = false;
                        $json["error"] = $e->getMessage();
                    }

                    echo json_encode($json);
                } else {
                    $change_flag = false;
                    $skip_array = ["changeID", "partnerID", "changeMade", "changeBy", "user_name"];
                    $partner_fields = $partner->get_field_name_map();
                    $address_array = [
                        'line1' => 'partnerAdd1',
                        'line2' => 'partnerAdd2',
                        'line3' => 'partnerAdd3',
                        'postcode' => 'partnerPostcode'
                    ];

                    // for each submitted change
                    foreach ($d as $key => $value) {
                        // check that we have real changes and not just generic DB columns
                        if (!in_array($key, $skip_array)) {
                            $new_value = $value;

                            // address array needs a little manual work
                            if (in_array($key, $address_array)) {
                                $k = array_search($key, $address_array);
                                $current_value = $partner->address->$k();
                            } else {
                                $k = array_search($key, $partner_fields);
                                $current_value = $partner->$k;
                            }

                            // compare changed value with current and ensure they are not matching
                            if ($new_value != $current_value) {
                                $change_flag = true;
                            }
                        }
                    }

                    if ($change_flag != false) {
                        /* Display changes in a comparative form */
                        $t = Partner::get_template(
                            "partner_changes_form.html",
                            [
                                "partner" => $partner,
                                "updated_partner" => $up,
                                "d" => $d,
                                "edit" => $edit
                            ]
                        );
                    } else {
                        /*
                         * we are here due to an entry in the partner_changes table which was either duplicated, blank
                         * or has already been actioned. We do not want to pester the user with an empty change box
                         * telling the to reject the blank change.
                         * Delete entry.
                        */
                        $db->query("DELETE FROM " . REMOTE_CHANGES_TBL . " WHERE changeid=" . intval($d["changeID"]));
                    }

                    echo "$t";
                }

                break;

            case "statistics":
                /* Graph monthly commission since last year */
                $json = [
                    "cGraph" => []
                ];

                // Compile a list of the last 12 months
                $months = [];
                for ($i = 12; $i >= 0; $i--) {
                    $months[] = date('M Y', mktime(0, 0, 0, date('n') - $i));
                }

                $json["cGraph"]["months"] = $months;


                $db = new mydb();
                $c = new Commission;

                // Commission since this time last year
                $db->query(
                    "SELECT DATE_FORMAT(" . $c->date->get_field_name() . ", '%b %Y'), " .
                    "ROUND(SUM(" . $c->paid->get_field_name() . "),2) " .
                    "FROM " . $c->get_table() . " " .
                    "WHERE " . $c->partner->to_sql($partner->id()) . " " .
                    "AND " . $c->date->get_field_name() . " BETWEEN '" . (date("Y") - 1) . "-" . date("m") . "-01 00:00:00' AND '" . date("Y") . "-" . date("m") . "-" . date("d") . " " . date('G:i:s') . "' " .
                    "GROUP BY DATE_FORMAT(" . $c->date->get_field_name() . ", '%b %Y')"
                );

                $a = [];
                while ($row = $db->next(MYSQLI_NUM)) {
                    $a[$row[0]] = $row[1];
                }

                $amounts = [];
                foreach ($months as $m) {
                    if (!array_key_exists($m, $a)) {
                        $amounts[$m] = 0;
                        continue;
                    }

                    $amounts[$m] = $a[$m];
                }

                $json["cGraph"]["data"] = array_values($amounts);

                /* Various figures */
                $figures = [];

                // number of clients
                $s = new Search(new Client);
                $s->eq("partner", $partner->id());
                $figures["Clients"] = $s->count();

                // number of policies
                $s = new Search(new Policy);
                $s->eq("client->partner", $partner->id());
                $figures["Policies"] = $s->count();

                // number of outstanding policies
                $s->eq("status", 1);
                $s->eq("addedhow", 2);
                $figures["Outstanding Mandate Policies"] = $s->count();

                // number of new business cases
                $s = new Search(new NewBusiness);
                $s->eq("policy->client->partner", $partner->id());
                $figures["New Business"] = $s->count();

                // number of unique paying policies
                $s = new Search(new Commission);
                $s->eq("partner", $partner->id());
                $figures["Paying Policies"] = $s->count("policy");

                // total commission for partner
                $ps = new Search(new Commission);
                $ps->eq("partner", $partner->id());
                $figures["Total Income Paid To Partner"] = "&pound; " . number_format($ps->sum("paid"), 2);

                /* GRI calculated as (renewal(0) + clawback(2) + adviser_charging_ongoing(8) + client_direct_ongoing(10) + dfm_fees(12) +
                OAS Income(14) + Trail(15) + PPB Income(16)) - VAT */
                $figures["12 Month Rolling GRI"] = Partner::partner_gri($partner->id());
                $figures["Lifetime GRI"] = Partner::partner_gri($partner->id(), true);

                $figures["12 Month Client Worth"] = $partner->clientsWorth();
                $figures["Lifetime Client Worth"] = $partner->clientsWorth(true);

                // number of correspondence items
                $s = new Search(new Correspondence);
                $s->eq("partner", $partner->id());
                $figures["Correspondence Items"] = $s->count();

                foreach ($figures as $key => $value) {
                    $figures[$key] = "<tr><th>$key</th><td>$value</td></tr>";
                }

                $json["figures"] = implode("\n", $figures);

                echo json_encode($json);

                break;

            case "outstanding":
                //// Show all the outstanding mandate transfers for this partner

                $s = new Search(new Policy);
                $s->eq("status", 1);
                $s->eq("addedhow", 2);
                $s->eq("client->partner", $partner->id());

                $t = Partner::get_template("pending_transfers_row.html");
                while ($policy = $s->next()) {
                    $t->tag($policy, "policy");
                    echo $t;
                }

                break;

            case "clients_by_issuer":
                $out = [];

                if (!isset($_GET["issuer"])) {

                    /** need to prepare a list of issuers **/

                    // necessary objects
                    $i = new Issuer;
                    $c = new Client;
                    $p = new Policy;

                    $db = new mydb();

                    $db->query(
                        "SELECT " . $i->id->get_field_name() . ", " . $i->name->get_field_name() . " " .
                        "FROM " . $c->get_table() . " " .
                        "LEFT JOIN " . $p->get_table() . " ON " . $c->id->get_field_name() . " = " . $p->client->get_field_name() . " " .
                        "INNER JOIN " . $i->get_table() . " ON " . $p->issuer->get_field_name() . " = " . $i->id->get_field_name() . " " .
                        "WHERE " . $c->partner->get_field_name() . " = " . $partner->id() . " " .
                        "GROUP BY " . $i->id->get_field_name() . " " .
                        "ORDER BY " . $i->name->get_field_name()
                    );

                    $out["issuers"] = el::dropdown(false, $db, false, false, true);
                } else {
                    $s = new Search(new Policy);
                    $s->eq("client->partner", $partner->id());
                    if ($_GET["issuer"] != "all") {
                        $s->eq("issuer", (int)$_GET["issuer"]);
                    }

                    $out["clients"] = "";
                    $t = Template(Partner::get_template_path("client_by_issuer_row.html"));
                    while ($policy = $s->next()) {
                        $t->tag($policy, "policy");
                        $out["clients"] .= $t;
                    }
                }

                echo json_encode($out);

                break;

            case "clients":
                function get_initials()
                {
                    global $partner;

                    $numeric = $special = false;
                    $letters = [];
                    $db = new mydb();
                    $c = new Client;

                    $db->query(
                        "select distinct(initial) from (" .
                        "( select distinct(ucase(substr(" . $c->one->surname->get_field_name() . ",1,1))) as initial " .
                        "  from " . $c->get_table() . " where " . $c->partner->get_field_name() . " = " . $partner->id() . " ) " .
                        "union all " .
                        "( select distinct(ucase(substr(" . $c->two->surname->get_field_name() . ",1,1))) as initial " .
                        "from " . $c->get_table() . " where " . $c->partner->get_field_name() . " = " . $partner->id() . " ) " .
                        ") as clients"
                    );

                    while (list($i) = $db->next(MYSQLI_NUM)) {
                        if (ctype_alpha($i)) {
                            $letters[] = $i;
                        } else if (ctype_digit($i)) {
                            $numeric = true;
                        } else if (ctype_graph($i)) {
                            $special = true;
                        }
                    }
                    sort($letters);

                    return ["letters" => $letters, "numeric" => $numeric, "special" => $special];
                }

                function get_list($initial = false, $limit, $offset = 0, $search = false)
                {
                    global $partner;

                    $s = Client::get_search()
                        ->eq("partner", $partner->id());

                    if ($initial !== false) {
                        if (ctype_alpha($initial)) {
                            $s->add_or(
                                $s->eq("one->surname", "$initial%", true, false),
                                $s->eq("two->surname", "$initial%", true, false)
                            );
                        } else if ($initial == "0-9") {
                            $s->add_or(
                                $s->object->one->surname->get_field_name() . " REGEXP'^[0-9]'",
                                $s->object->two->surname->get_field_name() . " REGEXP'^[0-9]'"
                            );
                        } else if ($initial == "*") {
                            $s->add_or(
                                $s->object->one->surname->get_field_name() . " REGEXP'^\\\*'",
                                $s->object->two->surname->get_field_name() . " REGEXP'^\\\*'"
                            );
                        }
                    }

                    if ($search !== false) {
                        // if we're searching postcodes, we should input properly with spaces.
                        // Let this line take care of whether or not we have the postcode stored with a space
                        $wSearch = str_replace(" ", "%", $search);

                        $s->add_or(
                            $s->eq("one->forename", "%$search%", true, false),
                            $s->eq("two->forename", "%$search%", true, false),
                            $s->eq("one->surname", "%$search%", true, false),
                            $s->eq("two->surname", "%$search%", true, false),
                            $s->eq("address->postcode", "%$search%", true, false),
                            $s->eq("address->postcode", "%$wSearch%", true, false)
                        );
                    }

                    $s->eq('group_scheme', 0);
                    $s->order(["one->surname", "one->forename", "two->surname", "two->forename"]);

                    $s->set_limit($limit);
                    $s->set_offset($offset);

                    $src = "";

                    $t = Template(Client::get_template_path("list_row.html"));
                    while ($c = $s->next()) {
                        $t->tag($c, "client");
                        $src .= $t;
                    }

                    return $src;
                }

                if (isset($_GET["initial"])) {
                    $initial = ($_GET["initial"] != "all") ? $_GET["initial"] : false;
                    $list = get_list($initial, $_GET["limit"], $_GET["offset"]);
                    echo json_encode(["clients" => $list]);
                } elseif (isset($_GET["search"])) {
                    $list = get_list(false, $_GET["limit"], $_GET["offset"], $_GET["search"]);
                    echo json_encode(["clients" => $list]);
                } else {
                    $initials = get_initials();

                    $i = (!empty($initials["letters"][0])) ? $initials["letters"][0] : false;

                    foreach (["*" => $initials["special"], "0-9" => $initials["numeric"]] as $sym => $x) {
                        if ($x) {
                            $initials["letters"][] = $sym;
                        }
                    }

                    $r = [];
                    $r["initials"] = $initials["letters"];
                    $r["clients"] = get_list($i, $_GET["limit"], $_GET["offset"]);
                    echo json_encode($r);
                }

                break;

            case "helpdesk":
                $out = [];

                $db = new mydb(json_encode(["82.113.133.220", "officedog", "F3eL1N~SAaD{", "cerb5"]), E_USER_WARNING);
                if (mysqli_connect_errno()) {
                    $out['data'] = "<td colspan='3'>Can't connect to the helpdesk server: Error#" . mysqli_connect_errno()."</td>";
                    echo json_encode($out);
                    exit();
                }

                $db->query(
                    "SELECT cerb5.ticket.subject,cerb5.ticket.is_closed,cerb5.ticket.created_date,cerb5.ticket.mask FROM cerb5.ticket " .
                    "INNER JOIN cerb5.address ON cerb5.ticket.first_wrote_address_id=cerb5.address.id " .
                    "WHERE cerb5.address.email = '" . $partner->email . "' ORDER BY cerb5.ticket.id DESC"
                );

                if ($db->affected_rows() <= 0) {
                    $out['data'] = "<td colspan='3'>No Helpdesk tickets requested from partners email</td>";
                    echo json_encode($out);
                    exit(); // no helpdesk tickets
                } else {
                    $out['data'] = false;

                    while ($t = $db->next(MYSQLI_NUM)) {
                        $b = new Boolean();
                        $b->set($t[1]);

                        $out['data'] .= "<tr><td><a target='_BLANK' href='http://www.policyservices.net/cerb6/index.php/profiles/ticket/" . $t[3] . "'>" . $t[0] . "</a></td><td>" . date("d/m/Y", $t[2]) . "</td><td>" . $b . "</td></tr>";
                    }

                    echo json_encode($out);

                    exit();
                }

                break;

            case "review_contract":
                try {
                    $json = (object)["status" => false, "error" => false];
                    $partnerid = $_GET["id"];
                    $contract = $_GET["contractid"];

                    $partner->id($partnerid);
                    $partner->reviewable($_GET["contractid"]);
                    $partner->updatedby($_COOKIE["uid"]);
                    $partner->updatedwhen(time());
                    $partner->save();

                    $json->status = true;
                } catch (Exception $e) {
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);

                break;

            case "emails":
                $e = new AdditionalEmail;
                $s = new Search($e);
                $s->eq("partnerID", $_GET['id']);

                $additionalemail = new AdditionalEmail($addemailID, true);

                echo "<tr>
					<th class='additionalEmail_th_padding' >Additional&nbsp;</th><th>Email Addresses</th><td><button id='add_email' name='add_email' class='add'>Add</button></td>
				</tr>";

                while ($x = $s->next(MYSQLI_ASSOC)) {
                    $partnerarray = (array)$x;

                    $addemailID = $partnerarray['id'];
                    $partnerid = $partnerarray['partnerID'];
                    $addemail = $partnerarray['email'];
                    $emailtag = $partnerarray['tag'];

                    echo "<tr id='" . $addemailID . "'>
							<td>" . $emailtag . ": </td>
							<td>" . el($addemail, false, 'disabled="disabled"', false) . "</td>
							<td><button id=" . $addemailID . " name=" . $addemailID . " class='update'>Update</button></td>
							<td><button id=" . $addemailID . " name=" . $addemailID . " class='delete'></button></td>
						</tr>";
                }

                break;

            case "add_email":
                if ($_GET['save']) {
                    $db = new mydb();

                    //Declare variables to hold data being passed through
                    $email = $_POST['additionalemail']['email'];
                    $tag = $_POST['additionalemail']['tag'];
                    $partnerID = $_POST['additionalemail']['partnerID'];

                    $i = 0;
                    $j = 0;
                    $json = [];

                    foreach ($email as $value) {
                        try {
                            $additionalemail = new AdditionalEmail();
                            $additionalemail->partnerID($partnerID);
                            $additionalemail->email($email[$i]);
                            $additionalemail->tag($tag[$i]);

                            $additionalemail->save();

                            $i++;
                        } catch (Exception $e) {
                            $json["error"] = $e->getMessage();
                            $j++;
                        }
                    }

                    $json["failure"] = $j;
                    $json["count"] = $i;

                    echo json_encode($json);
                } else {
                    echo Partner::get_template("add_email.html", ["id" => $_GET['id']]);
                }
                break;

            case "update_email":
                echo Partner::get_template("update_email.html", ["id" => $_GET['id']]);
                break;

            case "at_risk":
                $json = [
                    "status" => false,
                    "error" => false,
                    "at_risk" => false,
                ];

                try {
                    $p = new Partner($_GET['id'], true);

                    $renewal_12 = Partner::partner_gri_atrisk($_GET['id'], $life = false);
                    $renewal_lifetime = Partner::partner_gri_atrisk($_GET['id'], $life = true);

                    if ($renewal_12 != "&pound;0.00;" and $renewal_lifetime != "&pound;0.00") {
                        $notification = "<div class='alert alert-warning' style='display:block;'><i class='fas fa-info-circle'></i>" . $p->__toString() . " has Clients with Income at Risk. This is viewable via the at risk page on mypsaccount.
                        <br />Total Income at risk (12 Months): " . $renewal_12 . ", Lifetime Renewal: " . $renewal_lifetime . "</div>";
                        $json['at_risk'] = $notification;
                    }
                } catch (Exception $e) {
                    $json['error'] = $e->getMessage();
                }

                echo json_encode($json);

                break;

            case "ac_percent":
                $json = [
                    "total_at_risk" => null,
                    "percent" => null
                ];

                $plats = [
                    1152, # CoFunds
                    86,   # Fidelity
                    1216, # Old Mutual Wealth Platform, formally know as @SiS
                ];
                $plat_str = implode(', ', $plats);

                $db = new mydb();
                $db->query("select client_id, sum(renewal_12) as renewal_12, sum(renewal_lifetime)
                    from feebase.policy_gri
                    inner join feebase.tblclient
                    on tblclient.clientid = policy_gri.client_id
                    inner join feebase.tblpolicy
                    on tblpolicy.policyid = policy_gri.policy_id
                    where tblclient.partnerid = ".$_GET['id']."
                    and tblpolicy.issuerid in (".$plat_str.")
                    and tblpolicy.status = 2
                    and tblpolicy.ac_exempt = 0
                    and renewal_12 IS NOT NULL
                    group by tblclient.clientid
                    order by renewal_12 desc");

                $clients = [];
                $gri_12_total = 0;
                $gri_12_submitted = 0;

                //make an array of clients that are brought back from the above query
                while ($x = $db->next(MYSQLI_ASSOC)) {
                    $gri_12_total += $x['renewal_12'];
                    if (!in_array($x['client_id'], $clients)) {
                        $clients[] = $x['client_id'];
                    }
                }


                /* for each client we want to check that every policy under the platforms has a correspondence item with
                subject 'Adviser Charging Form sent to Provider', if not we will define this as at risk */
                $client_to_remove = [];

                foreach ($clients as $c) {
                    $db = new mydb();
                    $db->query("SELECT DISTINCT policyID FROM feebase.tblpolicy
                        WHERE clientID = ".$c."
                        AND issuerID IN(".$plat_str.")
                        AND status = 2 AND tblpolicy.ac_exempt = 0");

                    $policies_returned = [];

                    while ($p = $db->next(MYSQLI_NUM)) {
                        //add unique policyIDs to an array for checking, $p[0] == policyID
                        $policies_returned[] = $p[0];
                    }

                    $safe_policies = [];

                    foreach ($policies_returned as $pol) {
                        //check if policy has correspondence against it
                        $db->query("SELECT Policy FROM feebase.tblcorrespondencesql
                          WHERE Policy = ".$pol." AND subject = 'Adviser Charging Form sent to Provider'");

                        while ($corr = $db->next(MYSQLI_NUM)) {
                            //if row returned, add to safe_policies array,  $corr[0] == policy id
                            if ($corr[0]) {
                                $safe_policies[] = $corr[0];
                            }
                        }

                        //check if policy has ac applied
                        $db->query("SELECT policyID FROM feebase.tblpolicy
                          WHERE policyID = ".$pol." AND ac_applied = 1");

                        while ($ac_applied = $db->next(MYSQLI_NUM)) {
                            //if row returned, add to safe_policies array,  $ac_applied[0] == policy id
                            if ($ac_applied[0]) {
                                if (!in_array($ac_applied[0], $safe_policies)) {
                                    $safe_policies[] = $ac_applied[0];
                                }
                            }
                        }
                    }

                    //create a new array with the safe policies removed
                    $policies_array = array_diff($policies_returned, $safe_policies);

                    //array is empty there for all policies under this client are safe to be removed.
                    if (empty($policies_array)) {
                        $client_to_remove[] = $c;
                    }
                }

                /* if a partner has no clients to show, add value of 0 to the array so to not break the
                    IN clause on the search below  */
                if (empty($client_to_remove)) {
                    $client_to_remove[] = 0;
                }
                $client_str = implode(', ', $client_to_remove);

                $db = new mydb();
                $db->query("select sum(renewal_12) as renewal_12
                    from feebase.policy_gri
                    inner join feebase.tblclient
                    on tblclient.clientid = policy_gri.client_id
                    inner join feebase.tblpolicy
                    on tblpolicy.policyid = policy_gri.policy_id
                    where tblclient.clientID in(".$client_str.")
                    and tblpolicy.issuerid in (".$plat_str.")
                    and tblpolicy.status = 2
                    and tblpolicy.ac_exempt = 0
                    and renewal_12 IS NOT NULL
                    group by tblclient.clientid
                    order by renewal_12 desc");


                while ($d = $db->next(MYSQLI_NUM)) {
                    $gri_12_submitted += $d[0];
                }

                if ($gri_12_submitted != 0) {
                    $percent = $gri_12_submitted / $gri_12_total * 100;
                } else {
                    $percent = 0;
                }

                $json['percent'] = number_format((float)$percent, 2, '.', '');

                //$json['total_at_risk'] = number_format((float)$gri_12_total, 2, '.', '');

                echo json_encode($json);

                break;

            case "client_merge_request":
                $json = [
                    "error" => null,
                    "status" => null
                ];

                # we need to ensure that the merge_time is always the same for client merges
                $merge_time = time();

                //add clients to array
                $client_array = explode(",", $_GET['clients']);

                //variable for checking if all servicing levels are the same
                $servLevel = null;

                //safe guard, ensure all clients belong to the same partner
                foreach ($client_array as $key=>$client) {
                    $c = new Client($client, true);
                    if ($c->partner() != $_GET['id']) {
                        $json['status'] = "Please only select client from the same partner to merge";
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }

                    //check for matching servicing levels
                    if ($servLevel === null){
                        if ($c->servicingPropositionLevel()){
                            //first time around, just set the variable
                            $servLevel = $c->servicingPropositionLevel();
                        }
                    } else {
                        if ($c->servicingPropositionLevel()){
                            if ($servLevel !== $c->servicingPropositionLevel()){
                                $json['status'] = "Only clients with matching servicing levels can be merged. Please contact Client Relations or IT to request a merge and provide the correct servicing level.";
                                $json['error'] = true;
                                echo json_encode($json);
                                exit();
                            }
                        }
                    }
                }

                $client_merge_requested = 0;

                //add entry for merge for each client
                foreach ($client_array as $client) {
                    $dc = new DataChange;
                    $dc->load([
                        'data'=> $_GET['clients'],
                        'comments' => DataChange::COMMENT_CLIENT_MERGE,
                        'object_type' => "Client",
                        'object_id'=> $client,
                        'proposed_time'=> $merge_time
                    ]);

                    if ($dc->proposed_by() != null) {
                        if ($dc->save()) {
                            $client_merge_requested++;
                        }
                    } else {
                        $json['status'] = "Client merge failed, no myps id, Please contact IT.";
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }
                }

                //check that 'merge entries added' match number of 'clients requested to merge'
                if ($client_merge_requested == sizeof($client_array)) {
                    $json['status'] = "Client merge successfully requested";
                    $json['error'] = false;
                } else {
                    $json['status'] = "Client merge failed, Please contact IT.";
                    $json['error'] = true;
                }

                echo json_encode($json);

                break;

            case "bsp":
                $json = [
                    "error" => null,
                    "status" => null,
                    "data" => null
                ];

                try{
                    if (isset($_GET['action'])) {
                        if ($_GET['action'] == "add_template") {
                            $bsp = new BSP();
                            echo new Template("partner/bsp_add.html", ["bsp"=>$bsp, "id"=>$_GET['id']]);
                            exit();
                        } else if ($_GET['action'] == "add_bsp") {
                            $bsp = new BSP();
                            $bsp->sold_from($_GET['bsp_from']);
                            $bsp->sold_to($_GET['bsp_to']);
                            $bsp->type($_GET['bsp_type']);
                            $bsp->bsp_date(strtotime($_GET['bsp_date']));

                            if ($bsp->save()) {
                                $json['error'] = false;
                                $json['status'] = "Entry successfully added";

                                /* Now we want to add a new wf ticket due in 1 year */

                                //Set variable "nextyear" as time at processing + 1 year
                                $nextyear  = mktime(0, 0, 0, date("m"), date("d"), date("Y")+1);

                                //creates new workflow object
                                $w = new Workflow;

                                //creates an array for passing into the workflow add function
                                $wfdata = [
                                    "step" => "10",
                                    "priority" => "2",
                                    "due" => $nextyear,
                                    "desc" =>"Check CA received for AC  clients",
                                    "index" => $_GET["bsp_to"],
                                    "_object" => "Partner",
                                ];

                                //Create an array, fills it with the user id from the cookie
                                $assigned = ['id'=>$_COOKIE["uid"]];

                                //Open buffer
                                ob_start();

                                //Call workflow add function
                                $w->workflow_add($wfdata, $assigned, false, false);

                                //Open buffer end, cleans buffer out
                                ob_end_clean();
                            } else {
                                $json['error'] = true;
                                $json['status'] = "Failed to add BSP entry. Contact IT.";
                            }
                        } else if ($_GET['action'] == "remove_bsp") {
                            $db = new mydb();
                            $q = "DELETE FROM tblpartnerbsp WHERE id = ".$_GET['bsp_id'];
                            if ($db->query($q)) {
                                $json['error'] = false;
                                $json['status'] = "Entry deleted";
                            } else {
                                $json['error'] = true;
                                $json['status'] = "Failed to delete. Contact IT.";
                            }
                        }
                    } else {
                        $s = new Search(new BSP);
                        $s->add_or(
                            $s->eq("sold_from", $_GET['id'], true, false),
                            $s->eq("sold_to", $_GET['id'], true, false)
                        );

                        if(empty($_GET['pending'])){
                            $s->nt("bsp_date", null);
                        } else {
                            $s->eq("bsp_date", null);
                        }

                        while ($bsp = $s->next()) {
                            $json['data'] .="<tr id='".$bsp->id."'><td>".$bsp->sold_from->link()."</td><td>".$bsp->sold_to->link()."</td>
                        <td>".$bsp->type."</td>";

                            if(empty($_GET['pending'])){
                                $json['data'] .= "<td>".$bsp->bsp_date."</td>";
                            }

                            $json['data'] .= "<td><button id='" . $bsp->id . "' class='edit'>Edit</button>
                        <button id='".$bsp->id."' class='delete'>&nbsp;</button></td></tr>";
                        }

                        if ($json['data'] == null) {
                            $json['data'] = "<tr><td>There are currently no recorded BSP's to show for this client.</td></tr>";
                        }
                    }
                } catch (Exception $e) {
                    $json['error'] = true;
                    $json['status'] = $e->getMessage();
                }

                echo json_encode($json);



                break;

            case "bsp_clients":
                $response = [
                    "data" => ""
                ];

                $s = new Search(new Client());
                $s->eq("partner", $partner->id());

                if($s->count() <= 100){
                    foreach($partner->clients() as $client){
                        $response['data'] .= "<tr>
                                                <td><input type=\"checkbox\" class=\"partial_clients\" value='" . $client->id() . "'></td>
                                                <td>" . $client->link() . "</td>
                                                <td>" . $client->address . "</td>
                                            </tr>";
                    }
                } else {
                    $response['data'] .= "<tr><td colspan='3'>This partner has too many clients to perform a partial BSP through Prophet (Limited at 100). Contact IT</td></tr>";
                }

                echo json_encode($response);

                break;

            case "autoBsp":
                $return = [
                    'error' => false,
                    'description' => ''
                ];

                if($redisClient->get("incomerun")){
                    $return['error'] = true;
                    $return['description'] = "BSP's cannot be performed whilst the income run is being processed.";
                } else {
                    if (isset($_GET['id']) && !empty($_GET['toPartner'])) {
                        // Set paths to the python script to run
                        $python = '/usr/bin/python2.7';
                        $py_script = '/usr/local/bin/auto_bsp_20200303.py';

                        $params = ' '.$_GET['id'].' '.$_GET['toPartner'].' '.$_GET['starClients'];

                        if(!empty($_GET['bsp'])){
                            $bsp = new BSP($_GET['bsp'], true);
                            $bsp->bsp_date(time());

                            $bsp->save();
                        }

                        if (!file_exists($python)) {
                            #print("The python executable '$python' does not exist!");
                            $return["error"] = true;
                            $return["description"] .= "The python executable '$python' does not exist!";
                        }
                        if (!is_executable($python)) {
                            #print("The python executable '$python' is not executable!");
                            $return["error"] = true;
                            $return["description"] .= "The python executable '$python' is not executable!";
                        }
                        if (!file_exists($py_script)) {
                            #print("The python script file '$py_script' does not exist!");
                            $return["error"] = true;
                            $return["description"] .= "The python script file '$py_script' does not exist!";
                        }
                        if (!is_executable($py_script)) {
                            #print("The python script '$python' is not executable!");
                            $return["error"] = true;
                            $return["description"] .= "The python script '$python' is not executable!";
                        }

                        if ($return["error"]) {
                            echo json_encode($return);
                            exit();
                        }

                        // Build up the command for the cmd line
                        $cmd = "$python $py_script $params";

                        exec($cmd ." 2>&1", $output, $return_code);

                        //	If return code returns 0, no problems have arisen
                        if ($return_code == 0) {
                            $return["description"] = "BSP successful";
                        } else {
                            // Loop through the return array and create a string of errors.
                            foreach ($output as $r) {
                                $return["description"] .= $r . " ";
                            }
                            // If array returned with data in it, then json return the data
                            $return["error"] = true;
                        }

                    } else {
                        $return["error"] = true;
                        $return["description"] = "Please ensure you have selected a partner to move clients to and from.";
                    }
                }

                echo json_encode($return);
                break;

            case "partialBsp":
                $return = [
                    "success" => true,
                    "data" => null,
                ];

                $currentClient = null;

                try {
                    if ($redisClient->get("incomerun")) {
                        $return['success'] = false;
                        $return['data'] = "BSP's cannot be performed whilst the income run is being processed.";
                    } else {
                        $newPartner = new Partner($_GET['toPartner'], true);

                        $clientCount = $successCount = 0;

                        foreach ($_GET['clients'] as $cid) {
                            $clientCount++;

                            $client = new Client($cid, true);

                            $currentClient = $client;

                            if ($client->changeOwningPartner($_GET['toPartner'], "Partial BSP")) {
                                $successCount++;
                            }
                        }

                        $return['success'] = ($clientCount === $successCount);

                        if ($return['success']) {
                            if (!empty($_GET['bsp'])) {
                                // we're using a pending BSP, update the BSP date to complete the process
                                $bsp = new BSP($_GET['bsp'], true);

                                $bsp->bsp_date(time());
                            } else {
                                // add a record to the BSP table
                                $bsp = new BSP();

                                $bsp->sold_from($partner->id());
                                $bsp->sold_to($newPartner->id());
                                $bsp->type(2); // Partial
                            }

                            $bsp->save();

                            $return['data'] = "Successfully moved all clients to " . $newPartner->link();
                        } else {
                            $return['data'] = "Failed to move all client to " . $newPartner->link();
                        }
                    }
                } catch (Exception $e) {
                    $return['success'] = false;

                    $detailsString = "";

                    if(!empty($currentClient)){
                        $detailsString = " at client " . $currentClient->id() . " (" . $currentClient . ")";
                    }

                    $return['data'] = "Partial BSP failed" . $detailsString . ". Some clients may have already transferred over. " . $e->getMessage();
                }

                echo json_encode($return);

                break;

            case "partnerBSPedit":
                if (isset($_GET['bspFrom'])) {
                    $json = [
                        "status" => false,
                        "error" => false,
                    ];

                    try {
                        //Creates a new BSP object and then fills it with the relevant data
                        $bsp = new BSP($_GET["bspid"], true);
                        $bsp->sold_from($_GET["bspFrom"]);
                        $bsp->sold_to($_GET["bspTo"]);
                        $bsp->type($_GET["type"]);
                        $bsp->bsp_date(strtotime($_GET["bspDate"]));
                        $bsp->updated_by(User::get_default_instance("id"));
                        $bsp->updated_when(time());

                        if ($bsp->save()) {
                            $json['status'] = "BSP entry has been successfully updated";
                        } else {
                            $json['status'] = "BSP entry update has failed, contact IT.";
                            $json['error'] = true;
                        }
                    } catch (Exception $e) {
                        $json["error"] = $e->getMessage();
                    }

                    echo json_encode($json);
                } else {
                    $bsp = new BSP($_GET["bspid"], true);
                    echo Template("partner/edit_bsp.html", ["partner_id" => $_GET["id"], "bsp" => $bsp]);
                }

                break;

            case "testRun":

                $json = [
                    "data" => ""
                ];

                $s = new Search(new Client());
                $s->eq("partnerID", $_GET['id']);

                while($p = $s->next(MYSQLI_ASSOC)) {
                    $clientID = $p->id;

                    $s2 = new Search(new Policy());
                    $s2->eq("clientID", $clientID);

                    while($p2 = $s2->next(MYSQLI_ASSOC)){
                        $policies .= "
                            <tr>
                                <td></td>
                                <td>".$p2->client->link()."</td>
                                <td>".$p2->client->address."</td>
                                <td>".$p2->link()."</td>
                                <td>".$p2->issuer->link()."</td>
                                <td>".$p2->status."</td>
                                <td>".$p2->type."</td>
                            </tr>
                        ";
                    }
                }

                $json['data'] = $policies;

                echo json_encode($json);

                die();

                //return a json string back to the table

                break;

            case "schemeClients":
                $json = [
                    'error' => false,
                    'feedback' => '',
                    'data' => ''
                ];

                $search = new Search(new Client());
                $search->eq('partner', $partner->id());
                $search->eq('group_scheme', 1);

                if (isset($_GET['search']) && !empty($_GET['search'])){
                    $search->add_or(
                        [
                            $search->eq("id", $_GET['search'], true, 0),
                            $search->eq("clientSname1", "%" . $_GET['search'] . "%", true, 0),
                        ]
                    );
                }

                while ($x = $search->next(MYSQLI_OBJECT)) {
                    $json['data'] .= '<tr><td><input type="checkbox" class="scheme_select_box" data-id="' . $x->id() . '"></td><td>'.$x->link().'</td><td>'.$x->address.'</td></tr>';
                }

                echo json_encode($json);
                break;

            case "account_transfer_check":
                $db = new mydb();

                $db->query("select tblclient.clientid as 'Client ID', concat_ws(\" \", clientfname1, clientsname1) as 'Client 1', concat_ws(\" \", clientfname2, clientsname2) as 'Client 2', date_format(level_when, '%d/%m/%Y') as 'Client Agreement Processed', 
                    tblpolicy.policyid as 'Policy ID', tblissuer.issuerName as 'Provider', tblpolicy.policynum as 'Policy Number', tblpolicystatussql.field1 as 'Policy Status', ' ' as 'Fees Payable', ' ' as 'Frequency',
                    date_format(from_unixtime(mandate_tbl.sent_when), '%d/%m/%Y') as 'Mandate Processed', 
                    date_format(dateservtrfd, '%d/%m/%Y') as 'Policy Transferred',
                    a.pipeline as 'Pending Payment',
                    date_format(b.first_paid, '%d/%m/%Y') as 'First Payment', 
                    if(b.first_paid is not null, concat_ws(' ', '~21st ', monthname(b.first_paid), year(b.first_paid)), '') as 'First Payment To Partner', b.amount_paid as 'Amount Paid',
                    date_format(f.paiddate, '%d/%m/%Y') as '-3 Months Payment', 
                    if(f.paiddate is not null, concat_ws(' ', '~21st ', monthname(f.paiddate), year(f.paiddate)), '') as '-3 Months Payment To Partner', f.amount_paid as 'Amount Paid',
                    date_format(e.paiddate, '%d/%m/%Y') as '-2 Months Payment', 
                    if(e.paiddate is not null, concat_ws(' ', '~21st ', monthname(e.paiddate), year(e.paiddate)), '') as '-2 Months Payment To Partner', e.amount_paid as 'Amount Paid',
                    date_format(d.paiddate, '%d/%m/%Y') as '-1 Month Payment', 
                    if(d.paiddate is not null, concat_ws(' ', '~21st ', monthname(d.paiddate), year(d.paiddate)), '') as '-1 Month Payment To Partner', d.amount_paid as 'Amount Paid',
                    date_format(c.paiddate, '%d/%m/%Y') as 'Last Payment', 
                    if(c.paiddate is not null, concat_ws(' ', '~21st ', monthname(c.paiddate), year(c.paiddate)), '') as 'Last Payment To Partner', c.amount_paid as 'Amount Paid'
                    from tblclient
                    inner join tblpolicy
                    on tblclient.clientid = tblpolicy.clientid
                    inner join tblpolicystatussql
                    on tblpolicy.Status = tblpolicystatussql.id
                    inner join tblissuer
                    on tblpolicy.issuerid = tblissuer.issuerid
                    left join mandate_tbl
                    on tblpolicy.mandate_id = mandate_tbl.id
                    left join (
                        select tblcommission.policyid, round(SUM(amount),2) as pipeline
                        from feebase.tblcommission
                        group by policyid    
                    ) as a on a.policyid = tblpolicy.policyid
                    left join (
                        select tblcommissionauditsql.policyid, round(IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)), 2) as amount_paid, min(paiddate) as first_paid
                        from feebase.tblcommissionauditsql
                        inner join feebase.tblpartner
                        on tblcommissionauditsql.partnerid = tblpartner.partnerid
                        where tblcommissionauditsql.partnerid in (" . $partner->id() . ")
                        and paidDate >= date_sub(now(), interval 12 month)
                        and commntype in (0,2,8,10,12)
                        group by policyid, paiddate
                        order by paiddate asc
                    ) as b on b.policyid = tblpolicy.policyid
                    left join (
                        select tblcommissionauditsql.policyid, round(IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)), 2) as amount_paid, date_format(curdate(), '%Y-%m') as 'Current Month Payment Date', paiddate
                        from feebase.tblcommissionauditsql
                        inner join feebase.tblpartner
                        on tblcommissionauditsql.partnerid = tblpartner.partnerid
                        where tblcommissionauditsql.partnerid in (" . $partner->id() . ")
                        and paidDate like concat(date_format(curdate(), '%Y-%m'),'%')
                        and commntype in (0,2,8,10,12)
                        group by policyid, paiddate
                        order by paiddate asc
                    ) as c on c.policyid = tblpolicy.policyid
                    left join (
                        select tblcommissionauditsql.policyid, round(IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)), 2) as amount_paid, date_format(date_sub(curdate(), interval 1 month), '%Y-%m') as '-1 Month Payment Date', paiddate
                        from feebase.tblcommissionauditsql
                        inner join feebase.tblpartner
                        on tblcommissionauditsql.partnerid = tblpartner.partnerid
                        where tblcommissionauditsql.partnerid in (" . $partner->id() . ")
                        and paidDate like concat(date_format(date_sub(curdate(), interval 1 month), '%Y-%m'),'%')
                        and commntype in (0,2,8,10,12)
                        group by policyid, paiddate
                        order by paiddate asc
                    ) as d on d.policyid = tblpolicy.policyid
                    left join (
                        select tblcommissionauditsql.policyid, round(IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)), 2) as amount_paid, date_format(date_sub(curdate(), interval 2 month), '%Y-%m') as '-2 Months Payment Date', paiddate
                        from feebase.tblcommissionauditsql
                        inner join feebase.tblpartner
                        on tblcommissionauditsql.partnerid = tblpartner.partnerid
                        where tblcommissionauditsql.partnerid in (" . $partner->id() . ")
                        and paidDate like concat(date_format(date_sub(curdate(), interval 2 month), '%Y-%m'),'%')
                        and commntype in (0,2,8,10,12)
                        group by policyid, paiddate
                        order by paiddate asc
                    ) as e on e.policyid = tblpolicy.policyid
                    left join (
                        select tblcommissionauditsql.policyid, round(IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)), 2) as amount_paid, date_format(date_sub(curdate(), interval 3 month), '%Y-%m') as '-3 Months Payment Date', paiddate
                        from feebase.tblcommissionauditsql
                        inner join feebase.tblpartner
                        on tblcommissionauditsql.partnerid = tblpartner.partnerid
                        where tblcommissionauditsql.partnerid in (" . $partner->id() . ")
                        and paidDate like concat(date_format(date_sub(curdate(), interval 3 month), '%Y-%m'),'%')
                        and commntype in (0,2,8,10,12)
                        group by policyid, paiddate
                        order by paiddate asc
                    ) as f on f.policyid = tblpolicy.policyid
                    left join (
                        select client_id, level_when
                        from feebase.servicing_proposition_levels
                        order by level_when desc
                        limit 1
                    ) as spl on tblclient.clientid = spl.client_id
                    where tblclient.partnerid = " . $partner->id() . "
                    and clientsname1 not like '*%'
                    and policynum not like '*%'
                    and issuername not like '*%'
                    group by tblpolicy.policyid
                    order by clientsname1, clientfname1, issuername, policynum");

                $file = fopen("php://memory", "w");

                $printKeys = true;

                while($row = $db->next(MYSQLI_ASSOC)){
                    if ($printKeys){
                        $keys = array_keys($row);
                        fputcsv($file, $keys);
                        $printKeys = false;
                    }

                    fputcsv($file, $row);
                }

                fseek($file, 0);

                header('Content-Type: application/csv');
                header('Content-Disposition: attachment; filename="'. date("Y-m-d") . '"account_transfer_check.csv"');

                fpassthru($file);

                break;

            case "ongoing_service":
                $response = [
                    'success' => true,
                    'data' => ""
                ];

                $db = new mydb(REMOTE);

                $query = "SELECT os.id FROM myps.ongoing_service os inner join feebase.tblclient c on os.client_id = c.clientID where partnerID = " . $partner->id() . " ORDER BY id DESC ";

                $db->query($query);

                while($osid = $db->next(MYSQLI_ASSOC)){
                    $os = new OngoingService($osid['id'], true);

                    $response['data'] .= "<tr>
                        <td>" . $os->client_id->link() . "</td>
                        <td>" . $os->review_offered . " </td>
                        <td>" . $os->review_accepted . "</td>
                        <td>" . $os->review_type() . "</td>
                        <td>" . ($os->proposed_review_date() ? \Carbon\Carbon::parse($os->proposed_review_date())->toDayDateTimeString() : '') . "</td>
                        <td>" . $os->review_done . "</td>
                        <td>" . $os->added_when . "</td>
                        <td>" . $os->added_by->link() . "</td>
                    </tr>";
                }

                echo json_encode($response);

                break;

            case "servicing":
                $response = [
                    "success" => true,
                    "data" => null,
                ];

                $db = new mydb(REMOTE);

                $clients = [];

                switch($_GET['level']){
                    case "U":
                        $db->query("select tblclient.clientID
                                from feebase.tblclient
                                inner join feebase.tblpolicy on tblclient.clientID = tblpolicy.clientID
                                left join feebase.servicing_proposition_levels on tblclient.clientID = servicing_proposition_levels.client_id
                                left join myps.handback_removals on tblclient.clientID = handback_removals.clientID
                                left join myps.client_servicing on tblclient.clientID = client_servicing.client_id
                                where tblclient.partnerID = " . $_GET['id'] . "
                                and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                                and tblpolicy.issuerID != 1197
                                and tblclient.archived is null
                                and tblpolicy.archived is null
                                and (
                                    (clientSname1 is not null and clientSname1 != '' and client1_deceased = 0) or
                                    (clientSname2 is not null and clientSname2 != '' and client2_deceased = 0)
                                    )
                                and (pure_protection != 1 or pure_protection is null)
                                and servicing_proposition_levels.id is null
                                and handback_removals.id is null
                                and (client_servicing.id is null or client_servicing.servicing_level != 'R')
                                and clientSname1 not like '*%'
                                group by tblclient.clientID");

                        while($cid = $db->next(MYSQLI_ASSOC)){
                            $client = new Client($cid["clientID"], true);

                            $clients[] = $client;
                        }

                        break;
                    case "P":
                        $db->query("select tblclient.clientID
                                    from feebase.tblclient
                                    inner join feebase.tblpolicy on tblclient.clientID = tblpolicy.clientID
                                    left join feebase.servicing_proposition_levels on tblclient.clientID = servicing_proposition_levels.client_id
                                    left join myps.handback_removals on tblclient.clientID = handback_removals.clientID
                                    left join myps.client_servicing on tblclient.clientID = client_servicing.client_id
                                    where tblclient.partnerID = " . $_GET['id'] . "
                                    and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                                    and tblpolicy.issuerID != 1197
                                    and tblclient.archived is null
                                    and tblpolicy.archived is null
                                    and (
                                        (clientSname1 is not null and clientSname1 != '' and client1_deceased = 0) or
                                        (clientSname2 is not null and clientSname2 != '' and client2_deceased = 0)
                                        )
                                    and (pure_protection = 1)        
                                    and handback_removals.id is null
                                    and (client_servicing.id is null or client_servicing.servicing_level != 'R')
                                    and clientSname1 not like '*%'
                                    group by tblclient.clientID");

                        while($cid = $db->next(MYSQLI_ASSOC)){
                            $client = new Client($cid["clientID"], true);

                            $clients[] = $client;
                        }

                        break;

                    default:
                        $db->query("select tblclient.clientID
                                    from feebase.tblclient
                                    inner join feebase.tblpolicy on tblclient.clientID = tblpolicy.clientID
                                    left join feebase.servicing_proposition_levels on tblclient.clientID = servicing_proposition_levels.client_id
                                    left join myps.handback_removals on tblclient.clientID = handback_removals.clientID
                                    left join myps.client_servicing on tblclient.clientID = client_servicing.client_id
                                    where tblclient.partnerID = " . $_GET['id'] . "
                                    and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                                    and tblpolicy.issuerID != 1197
                                    and tblclient.archived is null
                                    and tblpolicy.archived is null
                                    and (
                                        (clientSname1 is not null and clientSname1 != '' and client1_deceased = 0) or
                                        (clientSname2 is not null and clientSname2 != '' and client2_deceased = 0)
                                        )
                                    and (pure_protection != 1 or pure_protection is null)
                                    and servicing_proposition_levels.level = '" . $_GET['level'] . "'
                                    and handback_removals.id is null
                                    and (client_servicing.id is null or client_servicing.servicing_level != 'R')
                                    and clientSname1 not like '*%'
                                    group by tblclient.clientID");

                        while($cid = $db->next(MYSQLI_ASSOC)){
                            $client = new Client($cid["clientID"], true);

                            $clients[] = $client;
                        }

                        break;
                }

                switch($_GET['level']){
                    case "U":
                    case "T":
                        usort($clients, function($a, $b){
                            $valueA = str_replace("£ ", "", $a->clientWorth());
                            $valueB = str_replace("£ ", "", $b->clientWorth());

                            if($valueA == $valueB){
                                return 0;
                            }
                            return $valueA<$valueB ? 1 : -1;
                        });

                        foreach($clients as $client){
                            $data = "<td>" . $client->link() . "</td>";
                            $data .= "<td>" . $client->clientWorth() . "</td>";
                            $data .= "<td>" . $client->client_gri($client->id()) . "</td>";

                            $response['data'] .= "<tr>" . $data . "</tr>";
                        }

                        break;

                    case "P":
                        foreach($clients as $client){
                            $data = "<td>" . $client->link() . "</td>";

                            $response['data'] .= "<tr>" . $data . "</tr>";
                        }

                        break;

                    default:
                        usort($clients, function($a, $b){
                            $aReview = $a->reviewCountdown()['next'];
                            $bReview = $b->reviewCountdown()['next'];

                            if($aReview->equalTo($bReview)){
                                return 0;
                            }

                            return $aReview->lessThan($bReview) ? -1 : 1;
                        });

                        foreach($clients as $client){
                            $spl = $client->servicingPropositionLevel(false, true);

                            if($spl){
                                $data = "";

                                $data .= "<td>" . $client->link() . "</td>";
                                $data .= "<td>" . $spl->percent . "</td>";
                                $data .= "<td>" . $spl->level_when . "</td>";

                                if($_GET['level'] === "B"){
                                    $data .= "<td>" . $spl->frequency . " months</td>";
                                    $data .= "<td>£ " . $spl->amount . "</td>";
                                }

                                $review_countdown = $client->reviewCountdown();
                                $due = $client->reviewCountdownString();

                                $classes = [];

                                if($review_countdown['next']->isPast()){
                                    $classes[] = "table-danger";
                                }

                                $data .= "<td>" . $due . "</td>";

                                $response['data'] .= "<tr class='" . implode(" ", $classes) . "'>" . $data . "</tr>";
                            }
                        }

                        break;
                }

                echo json_encode($response);

                break;
        }
    } elseif (isset($_GET['do'])) {
        switch($_GET['do']){
            case "img_upload":
                $response = [
                    'success' => true,
                    'feedback' => null
                ];

                $file = $_FILES['partner_img'];

                if ($file['type'] === "image/jpeg"){
                    try{
                        // check to see if we already have an image for the recruit
                        if (azureCheckExists(CDN_DIR . "partner_images", $partner->id() . ".jpg")){
                            // remove the old file, keep things tidy
                            azureRemoveFile(CDN_DIR . "partner_images", $partner->id() . ".jpg");
                        }

                        // upload the new file
                        azureAddFile(CDN_DIR . "partner_images", $file['tmp_name'], $partner->id() . ".jpg");

                    } catch (Exception $e) {
                        $response['success'] = false;
                        $response['feedback'] = "Failed to upload partner image: " . $e;
                    }
                } else {
                    // not allowed
                    $response['success'] = false;
                    $response['feedback'] = "Only .jpg files are allowed here";
                }

                echo json_encode($response);

                break;
        }
    }
} elseif (isset($_GET['do'])) {

    if ($_GET['do'] == "new_partner_prefill") {
        // create a json array - put any variables assigned
        $json = [
            "error" => null,
            "status" => null
        ];

        $practiceID = $_GET['practice_id'];


        $db = new mydb();

        $practice =  $db->query(
            "SELECT ".PRACTICE_TBL.".name, ".PRACTICE_STAFF_TBL.".partner_id, ".PRACTICE_STAFF_TBL.".practice_id, ".
            PARTNER_TBL.".partnerBulkrate, 
            count(DISTINCT ".PARTNER_TBL.".partnerBulkRate) as rateCount,
             ".PARTNER_TBL.".accountReviewDate,
            count(DISTINCT ".PARTNER_TBL.".accountReviewDate) as reviewDateCount,
            ".PARTNER_TBL.".band,
            count(DISTINCT ".PARTNER_TBL.".band) as bandCount,
            ".PARTNER_TBL.".reviewable_contract,
            count(DISTINCT ".PARTNER_TBL.".reviewable_contract) as contractCount
            FROM ".PRACTICE_TBL.
            " INNER JOIN ".PRACTICE_STAFF_TBL." on ".PRACTICE_STAFF_TBL.".practice_id = ".PRACTICE_TBL.".id".
            " INNER JOIN ".PARTNER_TBL." on ".PARTNER_TBL.".partnerid = ".PRACTICE_STAFF_TBL.".partner_id".
            " WHERE ".PRACTICE_STAFF_TBL.".practice_id = ".$practiceID." AND active=1"
        );

        while ($practice = $db->next(MYSQLI_ASSOC)) {
            if ($practice['rateCount'] != 1 || $practice['reviewDateCount'] != 1
                || $practice['bandCount'] != 1 || $practice['contractCount'] != 1 ) {
                $json['error'] = true;
            } else {
                //Pass dem datas back bruh

                $json['partnerBulkRate'] = $practice['partnerBulkrate'];
                $json['accountReviewDate'] = $practice['accountReviewDate'];
                $json['band'] = $practice['band'];
                $json['reviewable_contract'] = $practice['reviewable_contract'];
            }
        };


        echo json_encode($json); // Pass JSON data through to AJAX
    } elseif ($_GET['do'] == "viewUnmatchedTemplate") {
        $practiceID = $_GET['practice_id'];
        $pa = new Practice($practice['practice_id'], true);
        echo Template("partner/practiceUnmatched.html", ["practice_id" => $practiceID,"pa" => $pa]);
    } elseif ($_GET['do'] == "viewUnmatched") {
        // create a json array - put any variables assigned
        $json = [
            "error" => null,
            "status" => null,
            "data" => null
        ];

        $practiceID = $_GET['practice_id'];

        $db = new mydb();

        $practice =  $db->query(
            "SELECT ".PRACTICE_TBL.".name, ".PRACTICE_STAFF_TBL.".partner_id, ".PRACTICE_STAFF_TBL.".practice_id, ".
            PARTNER_TBL.".partnerBulkrate, 
             ".PARTNER_TBL.".accountReviewDate,
            ".PARTNER_TBL.".band,
            ".PARTNER_TBL.".reviewable_contract
            FROM ".PRACTICE_TBL.
            " INNER JOIN ".PRACTICE_STAFF_TBL." on ".PRACTICE_STAFF_TBL.".practice_id = ".PRACTICE_TBL.".id".
            " INNER JOIN ".PARTNER_TBL." on ".PARTNER_TBL.".partnerid = ".PRACTICE_STAFF_TBL.".partner_id".
            " WHERE ".PRACTICE_STAFF_TBL.".practice_id = ".$practiceID." AND active=1"
        );

        while ($practice = $db->next(MYSQLI_ASSOC)) {
            $pr = new Practice($practice['practice_id'], true);
            $pa = new Partner($practice['partner_id'], true);

            $json['data'] .= "<tr width='50%'><td>" . $pr->link() . "</td><td> " . $pa->link() . "</td><td>" .  $practice["partnerBulkrate"] . "</td><td> ". $practice["band"] ." </td></tr>";
        };


        echo json_encode($json); // Pass JSON data through to AJAX
    }

}  elseif (isset($_GET["update_ac_status"])) {
    // Update ac status in "My Adviser Charging Partners" on right click popup
    // create a json array - put any variables assigned
    $json = [
        "error" => null,
        "status" => null
    ];

    $db = new mydb();  // create a database connection
    //Build update "query" using error and "status" JSON variables
    $q = "UPDATE feebase.tblpartner
          SET ac_status = ".$_GET['status']."
          WHERE partnerID =".$_GET['partner_id'];


    $db->query($q); // RUN QUERY

    if ($db) { // If database query runs
        $json['status'] = "Adviser Charging status successfully updated"; // Display message to user confirming successful update of status
        $json['error'] = false; // Set error variable to false
    } //  execute query
    else {
        $json['status'] = "Adviser Charging status NOT updated";  // Display message to user confirming update of status was not successful
        $json['error'] = true; // Set error value as true
    }

    echo json_encode($json); //  Pass JSON data to AJAX
} elseif (isset($_GET['mypartners'])) {
    if ($_GET['display'] == true) {
        $json = [
            "error" => null,
            "status" => null,
            "data" => null
        ];

        $db = new mydb();
        $q = "SELECT partnerID FROM tblpartner
              INNER JOIN charging_allocation
              ON tblpartner.partnerid = charging_allocation.partner_id
              WHERE charging_allocation.staff_id =".User::get_default_instance('id');


        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            $partner = new Partner($row['partnerID'], true);

            $json['data'] .= "<tr id='".$partner->id."'>  // Create a table which will hold data for AC Partners
                <td class = 'partner_name' width = 160px>". $partner->link()."</td>
                <td width= 60px>".($partner->ac_status)  ."</td>
                <td width= 60px>". $partner->top20percent."</td>
                <td>".ChargingAllocation::packs_produced($partner->id)."</td>
                <td>".ChargingAllocation::packs_to_provider($partner->id)."</td>
                <td>".ChargingAllocation::ac_percentage($partner->id)."</td>
                <td width = 60px>". Partner::partner_gri_atrisk($partner->id)."</td>
            </tr>";
        }


        echo json_encode($json); // Pass JSON data through to AJAX
    } else {
        echo new Template("partner/mypartners.html", []);
    }
} elseif (isset($_GET['check_bday'])) {
    $today = date("-m-d");
    
    $db = new mydb();
    $q = "SELECT partnerID, partnerTitle, partnerFname, partnerSname, partnerDOB
			FROM  ".PARTNER_TBL."
			WHERE  DATE_ADD(partnerDOB, INTERVAL YEAR(CURDATE())-YEAR(partnerDOB) YEAR)
			BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 6 DAY)
			ORDER BY DATE_FORMAT(partnerDOB, '%m-%d')";

    $db->query($q);
    
    while ($row = $db->next(MYSQLI_ASSOC)) {
        $bdays[] = $row['partnerID'];
    }
    
    $first_itr = true;
    $daysofweek = ["0"=>"Monday", "1"=>"Tuesday", "2"=>"Wednesday", "3"=>"Thursday", "4"=>"Friday", "5"=>"Saturday", "6"=>"Sunday"];
    
    foreach ($bdays as $value) {
        $partner = new Partner($value, true);
        $year = date('Y');
        $age = (int)$year - substr($partner->dob(), 0, 4);

        $month_day = substr($partner->dob(), 5);
        $day = strftime("%A", strtotime($year."-".$month_day));
                        
        if (in_array($day, $daysofweek)) {
            $key = array_search($day, $daysofweek);
            $birthdays .= "<li class='bday_heading $day'>".$day."</li>";
            unset($daysofweek[$key]);
        }

        $birthdays .= "<li class='birthday'><a href='common/Partner/?id=".$partner->id()."' class='profile'>".$partner->title()." ".$partner->forename()." ".$partner->surname()." (".$age.")</a></li>";
    }
    
    echo json_encode($birthdays);
} elseif (isset($_GET['tariff_check'])) {
    //use 2013 as default tariff if is not set I.E "--"
    if ($_GET['tariff'] == "" || $_GET['tariff'] == null) {
        $tariff = "2";
    } else {
        $tariff = $_GET['tariff'];
    }


    if ($tariff == "1") {
        // Reviewable contract - 2009 Tariff
        $tariff = [
            ['min' => 0, 'max' => 1000, 'percentage' => 0, 'band' => 0],
            ['min' => 1001, 'max' => 5000, 'percentage' => 60, 'band' => 1],
            ['min' => 5001, 'max' => 10000, 'percentage' => 65, 'band' => 2],
            ['min' => 10001, 'max' => 15000, 'percentage' => 70, 'band' => 3],
            ['min' => 15001, 'max' => 25000, 'percentage' => 75, 'band' => 4],
            ['min' => 25001, 'max' => 50000, 'percentage' => 80, 'band' => 5],
            ['min' => 50001, 'max' => 75000, 'percentage' => 82, 'band' => 6],
            ['min' => 75001, 'max' => 100000, 'percentage' => 84, 'band' => 7],
            ['min' => 100001, 'max' => 150000, 'percentage' => 86, 'band' => 8],
            ['min' => 150001, 'max' => 200000, 'percentage' => 88, 'band' => 9],
            ['min' => 200001, 'max' => 250000, 'percentage' => 90, 'band' => 10],
            ['min' => 250001, 'max' => 300000, 'percentage' => 92, 'band' => 11],
            ['min' => 300001, 'max' => 9999999, 'percentage' => 94, 'band' => 12]
        ];

        $json['contract'] = 1;
    } elseif ($tariff == "2") {
        // Reviewable contract - 2013 Tariff
        $tariff = [
            ['min' => 0, 'max' => 1000, 'percentage' => 30, 'band' => 0],
            ['min' => 1001, 'max' => 10000, 'percentage' => 60, 'band' => 1],
            ['min' => 10001, 'max' => 20000, 'percentage' => 65, 'band' => 2],
            ['min' => 20001, 'max' => 30000, 'percentage' => 70, 'band' => 3],
            ['min' => 30001, 'max' => 50000, 'percentage' => 75, 'band' => 4],
            ['min' => 50001, 'max' => 75000, 'percentage' => 80, 'band' => 5],
            ['min' => 75001, 'max' => 100000, 'percentage' => 82, 'band' => 6],
            ['min' => 100001, 'max' => 150000, 'percentage' => 84, 'band' => 7],
            ['min' => 150001, 'max' => 200000, 'percentage' => 86, 'band' => 8],
            ['min' => 200001, 'max' => 250000, 'percentage' => 88, 'band' => 9],
            ['min' => 250001, 'max' => 300000, 'percentage' => 90, 'band' => 10],
            ['min' => 300001, 'max' => 400000, 'percentage' => 92, 'band' => 11],
            ['min' => 400001, 'max' => 9999999, 'percentage' => 94, 'band' => 12]
        ];

        $json['contract'] = 2;
    } elseif ($tariff == "3") {
        $json['contract'] = 3;
    }

    if (count($tariff)){
        foreach ($tariff as $current) {
            if (isset($_GET['band']) && $tariff != 3) {
                if ($_GET['band'] == $current['band']) {
                    $json['suggested_rate'] = $current['percentage'];
                    $json['renewals'] = $current['max'];
                    break;
                }
            }

            if ((isset($_GET['rate']) && $_GET['rate'] != 'null') && $tariff != 3) {
                if ($_GET['rate'] == $current['percentage']) {
                    $json['band'] = $current['band'];
                    $json['renewals'] = $current['max'];
                    break;
                }
            }

            $precision = 0.01 ;
            if ((isset($_GET['renewal']) && $_GET['renewal'] != "null") && $tariff != 3) {
                $renewal = number_format((float)$_GET['renewal'], 2);
                if ((str_replace(',', '', $renewal) - $current['min']) > $precision and
                    (str_replace(',', '', $renewal) - $current['max']) <= $precision ) {
                    $json['suggested_rate'] = $current['percentage'];
                    $json['band'] = $current['band'];
                    break;
                }
            }
        }
    }

    echo json_encode($json);
} else {

    function search_on_name(&$search_obj, $text)
    {

        $x = parse_name($text["text"]);
        
        if ($x["wildcard"]=="forename") {
            $x["forename"] .= "%";
        } else {
            $x["surname"] .= "%";
        }
        
        $conditions = [];
        
        if (isset($x["forename"])) {
            $conditions[] = $search_obj->eq("forename", $x["forename"], ( $x["wildcard"]=="forename" ), false);
        }
        
        $conditions[] = $search_obj->eq("surname", $x["surname"], ( $x["wildcard"]=="surname" ), false);
        $search_obj->add_and($conditions);
    }

    function search_on_manager(&$search_obj, $text)
    {
        $x = $text["text"] ;
        $db = new mydb();
        $q = "SELECT id 
			FROM  ". USR_TBL ."
			WHERE '".  $x ."' LIKE user_name ";
        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            $conditions = $row['id'];
        }

        $search_obj->eq("manager", $conditions);
    }
    
    $partner = new Partner;
    $search = new Search($partner);
    $search->calc_rows = true;


    $search
        -> flag("id", Search::DEFAULT_INT)
        -> flag("name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING)
        -> flag("postcode", "address->postcode", Search::PATTERN, "/".Patterns::UK_POSTCODE."/")
        -> flag("email", "email")
        -> flag("sjp", "agencyCode", Search::PATTERN, "/^\d{6}[A-Za-z]+$/")
        -> flag("manager", Search::CUSTOM_FUNC, "search_on_manager")
        -> flag("prevstatus", "previous_status")
        -> flag("phone", "phone")
        -> flag("mobile", "mobile");

    if (isset($_GET["search"])) {
        $search->nteq("is_closed", (int)1);
        $search->add_order("surname");
        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);

        $matches = [];

        $i = 0;

        while ($match = $search->next($_GET["search"])) {
            $matches["results"][$i] = ["text" => "$match", "value" => "$match->id", "is_closed"=>$match->is_closed()];

            //fix for bug 543, do not show partners who have accounts that are closed when changing owning partner of a client
            if (isset($_GET['change_partner']) && $_GET['change_partner'] == 1 && $matches['results'][$i]['is_closed'] == 1) {
                unset($matches['results'][$i]);
                $i -= 1;
            }
            $i += 1;
        }

            $matches["remaining"] = $search->remaining;

            echo json_encode($matches);
    } else {
        echo new Template("generic/search.html", ["help"=>$search->help_html(),"object"=>$partner]);
    }
}
