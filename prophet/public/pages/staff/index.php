<?php
require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

$db = new mydb();

if (isset($_GET["id"])) {
    $staff = new Staff($_GET['id'], true);

    if (isset($_GET["get"])) {
        switch ($_GET["get"]) {
            case "practices":
                $q = "SELECT ".PRACTICE_TBL.".id,".PRACTICE_STAFF_TBL.".active".
                " FROM ".PRACTICE_TBL." INNER JOIN ".PRACTICE_STAFF_TBL.
                " ON practice_id = ".PRACTICE_TBL.".id".
                " WHERE ".PRACTICE_STAFF_TBL.".staff_id = ".$staff ->id;
  
                $db -> query($q);
                 
                while ($row = $db -> next()) {
                    $practice = new Practice($row["id"], true);
                    $class = ($row["active"])?"":"staff_inactive";

                    echo "<tr class='$class'>".
                      "<td>".$practice -> link()."</td>".
                      "<td>".$practice -> address -> line1."</td>".
                      "<td>".$practice -> address -> line2."</td>".
                      "<td>".$practice -> address -> line3."</td>".
                      "<td>".$practice -> address -> postcode."</td>".
                      "</tr>";
                }
            
                break;

            case "emails":
                $e = new AdditionalEmail;
                $s = new Search($e);
                $s -> eq("staffID", $_GET['id']);
                
                $additionalemail = new AdditionalEmail($addemailID, true);
                
                echo "<tr>
					<th class='additionalEmail_th_padding' >Additional&nbsp;</th><th>Email Addresses</th><td><button id='add_email' name='add_email' class='add'>Add</button></td>
				</tr>";
                
                while ($x = $s -> next(MYSQLI_ASSOC)) {
                    $staffarray = (array)$x;

                    $addemailID = $staffarray['id'];
                    $staffID = $staffarray['staffID'];
                    $addemail = $staffarray['email'];
                    $emailtag = $staffarray['tag'];

                    echo "<tr id='".$addemailID."'>
							<td>".$emailtag.": </td>
							<td>".el($addemail, false, 'disabled="disabled"', false)."</td>
							<td><button id=".$addemailID." name=".$addemailID." class='update'>Update</button></td>
							<td><button id=".$addemailID." name=".$addemailID." class='delete'></button></td>
						</tr>" ;
                }
      
                break;
             
            case "add_email":
                if ($_GET['save']) {
                        $db = new mydb();
                    
                        //Declare variables to hold data being passed through
                        $email = $_POST['additionalemail']['email'];
                        $tag =  $_POST['additionalemail']['tag'];
                        $staffID = $_POST['additionalemail']['staffID'];

                        $i=0;
                        $j=0;
                        $json = [];
                        
                    foreach ($email as $value) {
                        try {
                            $additionalemail = new AdditionalEmail();
                            $additionalemail -> staffID($staffID);
                            $additionalemail -> email($email[$i]);
                            $additionalemail -> tag($tag[$i]);
                                
                            $additionalemail -> save();

                            $i++;
                        } catch (Exception $e) {
                            $json["error"] = $e -> getMessage();
                            $j++;
                        }
                    }
                        
                        $json["failure"]=$j;
                        $json["count"]=$i;
                        
                        echo json_encode($json);
                } else {
                    echo Staff::get_template("add_email.html", ["id" => $_GET['id']]);
                }
                
                break;
                
            case "update_email":
                echo Staff::get_template("update_email.html", ["id"=>$_GET['id']]);
                break;
        }
    }
} elseif (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "create":
            $staff = new Staff;
            $db = new mydb();
        
            if (isset($_POST[ "staff" ])) {
                $json = (object) [
                    "id"     => false,
                    "status" => false,
                    "error"  => false
                ];

                try {
                    $existing = ($_POST["exist_staff"] != 0)?true:false;
                    
                    if (!$existing) {
                        try {
                            $staff -> load($_POST["staff"]);

                            $staff -> save();

                            $json->id = $staff->id();
                            $db -> query("INSERT INTO ".PRACTICE_STAFF_TBL." set practice_id = ".$_POST["practice"]." , staff_id = ".$staff ->id());
                            $json->status = true;
                        } catch (Exception $e) {
                            $json->error = $e->getMessage();
                        }
                    } else {
                        try {
                            $db -> query("INSERT INTO ".PRACTICE_STAFF_TBL." set practice_id = ".$_POST["practice"]." , staff_id = ".$_POST["exist_staff"]);
                            $json->status = true;
                        } catch (Exception $e) {
                            $json->error = $e->getMessage();
                        }
                    }
                } catch (Exception $e) {
                    $json->error = $e->getMessage();
                }
               
                echo json_encode($json);
            } else {
                echo Template($staff->get_template_path("new.html"), ["practice"=> $_GET["practice"],"staff"=> $staff]);
            }

            break;


            case "delete":
            $staff = new Staff;
            $db = new mydb();

            if (isset($_POST[ "staff" ])) {
                $json = (object) [
                    "id"     => false,
                    "status" => false,
                    "error"  => false
                ];

                try {
                    $existing = ($_POST["exist_staff"] != 0)?true:false;

                    if (!$existing) {
                        try {
                            $staff -> load($_POST["staff"]);

                            $staff -> save();

                            $json->id = $staff->id();
                            $db -> query("INSERT INTO ".PRACTICE_STAFF_TBL." set practice_id = ".$_POST["practice"]." , staff_id = ".$staff ->id());
                            $json->status = true;
                        } catch (Exception $e) {
                            $json->error = $e->getMessage();
                        }
                    } else {
                        try {
                            $db -> query("INSERT INTO ".PRACTICE_STAFF_TBL." set practice_id = ".$_POST["practice"]." , staff_id = ".$_POST["exist_staff"]);
                            $json->status = true;
                        } catch (Exception $e) {
                            $json->error = $e->getMessage();
                        }
                    }
                } catch (Exception $e) {
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);
            } else {
                echo Template($staff->get_template_path("new.html"), ["practice"=> $_GET["practice"],"staff"=> $staff]);
            }

            break;


            case "list":

                $json = (object) [
                    "id"     => false,
                    "status" => false,
                    "data" => false,
                    "error"  => false
                ];

                try
                {
                    $s = new Search(new PracticeStaffEntry());
                    $s -> eq("practice_id", $_GET['practice_id']);

                    $user = new User($_COOKIE["uid"], true);
                    $staffrows = "";
                    while ($x = $s -> next(MYSQLI_ASSOC)) {

                        if($x->staff() != null)
                        {
                            $staff = new Staff($x->staff(), true);
                            if ($staff->active() == 1 && $x->active() == 1)
                            {
                                $staffrows .= "<tr><td>" . $staff->link($staff->firstname() . " " . $staff->surname()) . "</td>
                                              <td colspan='3'>  </td> 
                                              <td>" . $staff->role() . "</td>";
                                //if user is IT then give delete permissions
                                if ($user->department() == 7)
                                {
                                    $staffrows .= "<td><button class='delete' id='" . $staff->id() . "'></button></td>";
                                }
                                $staffrows .= "</tr>";
                            }
                        }else if ($x->partner != null){
                            $partner = new Partner($x->partner(), true);
                            if ($x->active() == 1){
                                $staffrows .= "<tr><td>".$partner->link($partner->forename() ." ". $partner->surname()." (". $partner->id().")")."</td>
                                              <td>".$partner->agencyCode()  ."</td>
                                              <td>".$partner->single_rate() ."</td>
							                  <td>".$partner->bulk_rate() ."</td>
							                  <td colspan='2'> Partner </td>
							                  </tr>";
                            }
                        }
                        $json->data = $staffrows;
                    }



                }
                catch
                    (Exception $e) {
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);


            break;

            case "soft_delete_staff":

                $json = (object) [
                    "id"     => false,
                    "status" => false,
                    "data" => false,
                    "error"  => false
                ];

                try {
                    $staff = new Staff($_GET['user_id'], true);
                    $staff->active(0);

                    if ($staff->save()) {
                        $json->status = true;
                        $json->error = false;
                    };

                } catch
                (Exception $e) {
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);


            break;
    }
} else {

    function search_on_name(&$search_obj, $text)
    {

        $x = parse_name($text["text"]);
        $staff = $search_obj->get_object();

        if ($x["wildcard"]=="forename") {
            $x["forename"] .= "%";
        } else {
            $x["surname"] .= "%";
        }

        $conditions = [];

        if (isset($x["forename"])) {
            $conditions[ ] = $search_obj->eq($staff->firstname, $x["forename"], ( $x["wildcard"]=="forename" ), false);
        }

        $conditions[ ] = $search_obj->eq($staff->surname, $x["surname"], ( $x["wildcard"]=="surname" ), false);
        $search_obj->add_and($conditions);
    }
     
    $staff = new Staff;
    $search = new Search($staff);

    $search -> set_flags([["name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING]]);

    if (isset($_GET["search"])) {
        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);

        $matches = [];
         
        while ($match = $search->next($_GET["search"])) {
            $matches["results"][] = ["text" => "$match", "value" => "$match->id"];
        }

        $matches["remaining"] = $search->remaining;

        echo json_encode($matches);
    } else {
        echo new Template("generic/search.html", ["help"=>$search->help_html(),"object"=>$staff]);
    }
}
