<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

use Knp\Snappy\Pdf;
use \Milon\Barcode\DNS2D;

$binary_location = '/usr/local/bin/wkhtmltopdf';

function getPartnerBarcode($partner = false)
{
    if ($partner) {
        $barcode = $partner->id();
        $bc = new DNS2D();
        $bc->setStorPath(__DIR__."/cache/");
        return $bc->getBarcodeHTML($barcode, "DATAMATRIX", 3, 3);
    }
    return false;
}

function getRenderedHTML($path, $params)
{
    ob_start();
    include ($path);
    $var = ob_get_contents();
    sprintf($var, $params);
    ob_end_clean();
    return $var;
}

if ((isset($_GET['partner_id'])) && (isset($_GET['partner_mandate']) || isset($_GET['scheme_mandate']))) {
    if (file_exists($binary_location)) {

        $params = [];

        $snappy = new Pdf($binary_location);

        $snappy->setOption('margin-top', 0);
        $snappy->setOption('margin-right', 0);
        $snappy->setOption('margin-bottom', 0);
        $snappy->setOption('margin-left', 0);

        header('Content-Type: application/pdf');
        // force download, comment out below to preview in browser
        header('Content-Disposition: attachment; filename="'.time().'.pdf"');

        if (isset($_GET['client_id'])) {
            $params['client'] = new Client($_GET['client_id'], true);
            if ($params['client']->one->client_deceased() == 1 || $params['client']->one->deceased_date() != NULL){
                $params['client']->one = $params['client']->two;
                $params['client']->two = new Client();
            }
            if ($params['client']->two->client_deceased() == 1 || $params['client']->two->deceased_date() != NULL){
                $params['client']->two = new Client();
            }
        } else {
            $params['client'] = new Client();
        }

        $partner = $params['partner'] = new Partner($_GET['partner_id'], true);
        $params['partner_address'] = strval($partner->address);
        $params['chunked_array'] = [];

        if (!boolval(isset($_GET['scheme_mandate']))) {
            $params['sheets_per_pack'] = ($_GET['client_sheets']) ?? 5;
            $params['trustee_sheets_per_pack'] = ($_GET['trustee_sheets']) ?? 5;
            $params['trustee_mandate'] =  true;
        } else {
            $params['sheets_per_pack'] = 0;
            $params['trustee_sheets_per_pack'] = 0;
            $params['trustee_mandate'] =  false;
        }

        $params['group_scheme'] =  (boolval(isset($_GET['scheme_mandate']))) ?? false;
        $params['parental_consent'] = (filter_var($_GET['parental'], FILTER_VALIDATE_BOOLEAN)) ?? false;
        $params['client_agreement'] = true;
        $params['client_poa_mandate'] =  (boolval(isset($_GET['partner_mandate']))) ?? false;
        $params['bespoke_mandate'] = (boolval(isset($_GET['bespoke_mandate']))) ?? false;
        $params['aspect_of_service'] = json_decode($_GET['aos']) ?? false;
        $params['bespoke_percent'] = $_GET['bespoke_percent'] ?? false;
        $params['bespoke_add_info'] = nl2br($_GET['addInfo']) ?? false;
        // don't show trustee sheets on scheme mandates


        $params['barcode'] = getPartnerBarcode($params['partner']);

        $template = TEMPLATE_DIR."mandate/pdfs/layout_sjp.html";
        $contents = getRenderedHTML($template, $params);

        echo $snappy->getOutputFromHtml($contents);

    } else {
        dd("Binary not found");
    }
}


