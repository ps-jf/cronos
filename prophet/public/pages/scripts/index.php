<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 08/08/2019
 * Time: 11:06
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET['do'])){
    switch($_GET['do']){
        case "load_scripts":
            $response = [
                'success' => true,
                'data' => null
            ];

            $s = new Search(new Script());

            $s->eq("category", $_GET['id']);
            $s->eq("archived", 0);

            while($script = $s->next(MYSQLI_ASSOC)){
                $response['data'] .= new Template("scripts/script-row.html", ["script" => $script]);
            }

            echo json_encode($response);

            break;

        case "load_form":
            $response = [
                'success' => true,
                'form' => false,
                'name' => null,
            ];

            $script = new Script($_GET['id'], true);

            $response['name'] = strval($script);

            if(!empty($script->parameters())){
                $form = new Template("scripts/form.html", compact(["script"]));
                $response['form'] = $form->output();
            }

            echo json_encode($response);

            break;

        case "run_script":
            $response = [
                'success' => true,
                'feedback' => [],
                'files' => [],
            ];

            try {
                $script = new Script($_GET['id'], true);

                if (!empty($script->function())) {
                    $response = $script->{$script->function()}();
                } else {
                    $script_name = SCRIPTS_DIR . $script->filename();

                    if (!file_exists($script_name)) {
                        $response["success"] = false;
                        $response["feedback"][] = "The python executable '$script_name' does not exist!";
                    }

                    if (!is_executable($script_name)) {
                        $response["success"] = false;
                        $response["feedback"][] = "The python executable '$script_name' is not executable!";
                    }


                    if (!$response["success"]) {
                        echo json_encode($response);
                        exit();
                    }

                    if (isset($_GET['queue_name'])) {
                        // pass parameter for mandate queue
                        $script_name.= ' "'.$_GET['queue_name'].'"';
                    }

                    //exec($script->command() . " " . $script_name. " 2>&1", $output, $return);  // 2>&1 to provide full output
                    exec($script->command() . " " . $script_name . " 2>&1", $output, $return);

                    if (!$return){
                        foreach ($output as $o){
                            if (substr($o, 0, 5) === "file:") {
                                $response['files'][] = str_replace("file:", "", $o);
                            } else {
                                $response['feedback'][] = $o;
                            }
                        }
                    } else {
                        $response['success'] = false;
                        $response['feedback'][] = "Error: " . $return;
                    }
                }

            } catch (Exception $e){
                $response['success'] = false;
                $response['feedback'][] = $e->getMessage();
            }

            echo json_encode($response);

            break;

        case "download":
            $file = SCRIPT_OUTPUT . "/" . $_GET['name'];
            if (file_exists($file)){
                header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
                header("Cache-Control: public"); // needed for internet explorer
                header("Content-Type: " . mime_content_type($file));
                header("Content-Transfer-Encoding: Binary");
                header("Content-Length:".filesize($file));
                header("Content-Disposition: attachment; filename=" . basename($file));
                readfile($file);
                die();
            } else {
                echo "Failed to load file: " . $file;
            }

            break;
    }
} else {
    echo new Template('scripts/index.html');
}
