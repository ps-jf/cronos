<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $db = new mydb();

    $query = "SELECT * FROM prophet.articles
                INNER JOIN prophet.articles_groups ON prophet.articles.id = prophet.articles_groups.article_id
              WHERE prophet.articles.id = " . $_GET["id"];

    if ($db->query($query) && ($d = $db->next(MYSQLI_OBJECT))) {
        $t = new Template("misc/articles/article.html");
        $t->tag($d, 'article');
        echo $t;
    }

} elseif (isset($_GET["get"])) {
    switch ($_GET["get"]) {
        case "latest_partner":
            $db = new mydb();

            $query = "SELECT partnerid,partnerfname,partnersname, band " .
                "FROM " . PARTNER_TBL . " " .
                "ORDER BY partnerid DESC LIMIT 0,1";

            if ($db->query($query) && ($d = $db->next(MYSQLI_OBJECT))) {
                $t = new Template("misc/articles/recent-partner.html");
                $t->tag($d, 'partner');
                echo $t;
            }

            break;

        case "events":
            $db = new mydb();

            $query = "SELECT prophet.articles.ID,title,introduction FROM prophet.articles
                        INNER JOIN prophet.articles_groups ON prophet.articles.id = prophet.articles_groups.article_id
                      WHERE label = 'Event' AND is_published = 1 LIMIT 0,5";

            if ($db->query($query)) {
                echo "<ul>";
                while ($d = $db->next(MYSQLI_OBJECT)) {
                    $t = new Template("misc/articles/event.html");
                    $t->tag($d, 'event');
                    echo $t;
                }
                echo "</ul>";
            }

            break;

/*        case "thumbs":
            $jsons = [];

            $xmlDoc = new DOMDocument();

            // if file on terry cant be read, use file stored locally in the repo as a back up
            if (!$xmlDoc->load(RSS_FEED_LOCATION . 'ps.xml')) {
                // if back up missing (should never happen) throw error, else use file
                if (!$xmlDoc->load(TEMPLATE_DIR . 'ps.xml')) {
                    throw new Exception("backup ps_rss.xml file missing");
                } else {
                    $searchNode = $xmlDoc->getElementsByTagName("department");
                    $jsons["updated_at"] = false;
                }
            } else {
                // file is readable so count how many sets of graph data we have
                $searchNode = $xmlDoc->getElementsByTagName("department");
                $i = 0;
                foreach ($searchNode as $node) {
                    $i++;
                }

                // if we don't have 11 sets of graph data, file is incomplete, fall back
                if ($i != 11) {
                    // if back up missing (should never happen) throw error, else use file
                    if (!$xmlDoc->load(TEMPLATE_DIR . 'ps.xml')) {
                        throw new Exception("backup ps_rss.xml file missing");
                    } else {
                        $searchNode = $xmlDoc->getElementsByTagName("department");
                        $jsons["updated_at"] = false;
                    }
                } else {
                    //get last updated time and date
                    $dateTime = $xmlDoc->getElementsByTagName("created")->item(0)->nodeValue;
                    $jsons["updated_at"] = date("d/m/Y H:i:s", strtotime($dateTime));
                }
            }

            foreach ($searchNode as $node) {
                $values = [];

                $department = $node->getAttribute('name');
                $items = $node->getElementsByTagName("item");

                foreach ($items as $item) {
                    $type = $item->getAttribute('type');

                    if ($type == "list") {
                        $list = [];

                        $descrip = $item->getElementsByTagName("description")->item(0)->nodeValue;

                        $lst = $item->getElementsByTagName("list");
                        $lst_items = $item->getElementsByTagName("list_item");

                        foreach ($lst_items as $l) {
                            $list[$l->getElementsByTagName("key")->item(0)->nodeValue] = $l->getElementsByTagName("value")->item(0)->nodeValue;
                        }
                    } else {
                        $values[$item->getElementsByTagName("description")->item(0)->nodeValue] = $item->getElementsByTagName("value")->item(0)->nodeValue;
                    }
                }
                $json["values"] = $values;
                $json["pie"] = $list;

                $jsons[$department] = $json;
            }

            echo json_encode($jsons);

            break;

        case "masterChart":
            $jsons = [];
            $xmlDoc = new DOMDocument();

            // if file on terry cant be read, use file stored locally in the repo as a back up
            if (!$xmlDoc->load(RSS_FEED_LOCATION . 'ps.xml')) {
                // if back up missing (should never happen) throw error, else use file
                if (!$xmlDoc->load(TEMPLATE_DIR . 'ps.xml')) {
                    throw new Exception("backup ps_rss.xml file missing");
                } else {
                    $searchNode = $xmlDoc->getElementsByTagName("department");
                }
            } else {
                // file is readable so count how many sets of graph data we have
                $searchNode = $xmlDoc->getElementsByTagName("department");
                $i = 0;
                foreach ($searchNode as $node) {
                    $i++;
                }

                // if we don't have 11 sets of graph data, file is incomplete, fall back
                if ($i != 11) {
                    // if back up missing (should never happen) throw error, else use file
                    if (!$xmlDoc->load(TEMPLATE_DIR . 'ps.xml')) {
                        throw new Exception("backup ps_rss.xml file missing");
                    } else {
                        $searchNode = $xmlDoc->getElementsByTagName("department");
                    }
                }
            }

            foreach ($searchNode as $node) {
                $values = [];

                $department = $node->getAttribute('name');

                $items = $node->getElementsByTagName("item");

                foreach ($items as $item) {
                    $values[$item->getElementsByTagName("description")->item(0)->nodeValue] = $item->getElementsByTagName("value")->item(0)->nodeValue;
                }
                $json["values"] = $values;

                $jsons[$department] = $json;
            }

            $json = [
                "label" => false,
                "value" => false,
                "graph_title" => false,
                "graph_type" => false,
                "yAxis" => false,
                "pound" => false
            ];


            if ($_GET['graphID'] == "acquisition") {
                $json["graph_type"] = "column";
                $json["graph_title"] = "Acquisition";
                $json["yAxis"] = "Amount This Month";
                $value = [
                    (float)$jsons['Acquisition']['values']['Agencies transferred'],
                    (float)$jsons["Client Services"]["values"]["Policies Transferred"],
                    (float)$jsons['Acquisition']['values']['Bulk Transfers completed'],
                    (float)$jsons['Acquisition']['values']['Bulk accounts opened'],
                    (float)$jsons['Acquisition']['values']['Non-bulk accounts opened'],
                    (float)$jsons['Acquisition']['values']['Outstanding Mandates']
                ];
                $label = [
                    "Agencies transferred",
                    "Policies Transferred",
                    "Bulk Transfers completed",
                    "Bulk accounts opened",
                    "Non-bulk accounts opened",
                    "Outstanding Mandates"
                ];
            } else if ($_GET['graphID'] == "commission") {
                $json["graph_type"] = "column";
                $json["graph_title"] = "Commission";
                $json["yAxis"] = "£ This Month";
                $json["pound"] = "£";
                $label = [
                    "Commission received",
                    "Commission processed",
                    "Renewal commission processed",
                    "Adviser Charging processed",
                    "Client Direct payment processed",
                    "Initial commission processed",
                    "Initial Clawback  processed",
                    "Clawback processed",
                ];
                $value = [
                    (float)str_replace(',', '', (isset($jsons["Finance"]["values"]["Commission received"]))) ?? '',
                    (float)str_replace(',', '', $jsons["Finance"]["values"]["Commission processed"]),
                    (float)str_replace(',', '', $jsons["Finance"]["values"]["Renewal commission processed"]),
                    (float)str_replace(',', '', $jsons["Finance"]["values"]["Adviser Charging processed"]),
                    (float)str_replace(',', '', $jsons["Finance"]["values"]["Client Direct Payment processed"]),
                    (float)str_replace(',', '', $jsons["Finance"]["values"]["Initial commission processed"]),
                    (float)str_replace(',', '', $jsons["Finance"]["values"]["Initial Clawback processed"]),
                    (float)str_replace(',', '', $jsons["Finance"]["values"]["Clawback processed"]),
                ];
            } else if ($_GET['graphID'] == "newbusiness") {
                $json["graph_type"] = "column";
                $json["graph_title"] = "New Business";
                $json["yAxis"] = "Amount This Month";
                $value = [
                    (float)$jsons['New Business']['values']['Advice cases received'],
                    (float)$jsons['New Business']['values']['Advice cases issued'],
                    (float)$jsons['New Business']['values']['DO cases issued'],
                    (float)$jsons['New Business']['values']['EO cases issued'],
                    (float)$jsons['New Business']['values']['Fund Switch issued'],
                    (float)$jsons['New Business']['values']['Partial Withdrawal Issued'],
                    (float)$jsons['New Business']['values']['Full Withdrawal Issued'],
                ];
                $label = [
                    "Advice cases received",
                    "Advice cases issued",
                    "DO cases issued",
                    "EO cases issued",
                    "Fund Switch issued",
                    "Partial Withdrawal Issued",
                    "Full Withdrawal Issued"
                ];
            } else if ($_GET['graphID'] == "ac") {
                $json["graph_type"] = "column";
                $json["graph_title"] = "Adviser Charging Packs";
                $json["yAxis"] = "";
                $value = [
                    (float)$jsons['Adviser Charging']['values']['Packs to Partners'],
                    (float)$jsons['Adviser Charging']['values']['Packs to Providers'],
                    (float)$jsons['Adviser Charging']['values']['Completed packs returned']
                ];
            } else if ($_GET['graphID'] == "ac_converted") {
                $json["graph_type"] = "areaspline";
                $json["graph_title"] = "12 Month Renewal Converted";
                $json["yAxis"] = "";
                $value = [
                    (float)$jsons['Twelve month Converted']['values']['Month 0 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 1 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 2 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 3 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 4 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 5 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 6 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 7 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 8 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 9 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 10 Converted'],
                    (float)$jsons['Twelve month Converted']['values']['Month 11 Converted']
                ];

                $months = [];
                for ($i = 0; $i < 12; $i++) {
                    $timestamp = mktime(0, 0, 0, date('n') - $i, 1);
                    $months[date('n', $timestamp)] = date('F', $timestamp);
                }
                $label = array_values($months);
            }

            $json["label"] = $label;
            $json["value"] = $value;

            echo json_encode($json);

            break;*/

        case "getJiraData":
            $json = [
                "error" => null,
                "status" => null,
                "data" => null
            ];

            //Gets the policy service jira data and populates a XML file on the server
            $username = JIRA_USERNAME;
            $password = JIRA_PASSWORD;
            $url = JIRA_URL;

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

            $issue_list = (curl_exec($curl));

            if (!$issue_list) {
                $json['error'] = 'Fetching data from JIRA server error: ' . curl_error($curl);
            }

            curl_close($curl);

            $string_data = $issue_list;
            $xml = simplexml_load_string($string_data);


            simplexml_load_string($string_data)->asXML(JIRA_RSS_FEED_LOCATION . 'jira.xml');

            echo json_encode($json);

            break;

        case "jiraData":
            $json = [
                "error" => null,
                "status" => null,
                "data" => null,
            ];

            $xml2 = simplexml_load_file(JIRA_RSS_FEED_LOCATION . 'jira.xml');

            $massive = "";
            foreach ($xml2->entry as $entry) {

                $massive .= '<div class="card">
                                      <div class="card-header">
                                      <img id="avatar" src="' . $entry->author->link['href'] . '">  ' . $entry->author->name . '
                                      </div>
                                      <div class="card-body">
                                        <h5 class="card-title">  ' . $entry->title . '</h5>
                                        <p class="card-text"> <div ="activityDescription">' . $entry->content . '</p>
                                        <a href="' . $entry->link['href'] . '" class="btn btn-primary">View</a>
                                        </div>
                                      </div>
                                       <div class="card-footer text-muted">
                                        <p class="card-text"><b>Time Published</b>  ' . date("H:i:s", strtotime($entry->published)) . ' -
                                        <b>Date Published</b>  ' . date("d-m-Y", strtotime($entry->published)) . '</p>
                                        </div>
                                    </div>
                                    <!--end activity item-->';
            }
            $json['data'] = $massive;

            echo json_encode($json);
            break;

        case "systemUpdatesFeed":
            $db = new mydb();

            $query = "SELECT * FROM prophet.articles
                        INNER JOIN prophet.articles_groups ON prophet.articles.id = prophet.articles_groups.article_id
                      WHERE prophet.articles_groups.label = 'System' AND is_published = 1
                      ORDER BY prophet.articles.date DESC LIMIT 10";

            if ($db->query($query) && ($d = $db->next(MYSQLI_OBJECT))) {
                while ($d = $db->next(MYSQLI_OBJECT)) {
                    $t = new Template("misc/articles/article.html");
                    $t->tag($d, 'article');
                    echo $t;
                }
            }

            break;


        case "system":
            $db = new mydb();

            $query = "SELECT prophet.articles.ID,title,class FROM prophet.articles
                        INNER JOIN prophet.articles_groups ON prophet.articles.id = prophet.articles_groups.article_id
                      WHERE is_published = 1
                      ORDER BY prophet.articles.date DESC LIMIT 0,10";

            if ($db->query($query)) {
                $t = new Template("misc/articles/system.html");
                echo "<ul>";
                while ($d = $db->next(MYSQLI_OBJECT)) {
                    $t->tag($d, 'system');
                    echo $t;
                }
                echo "</ul>";
            }

            break;

        case "system_alerts":
            $json = [
                "error" => null,
                "status" => null,
                "data" => null,
                "assigned_ids" => null
            ];

            $db = new mydb();

            $active_alerts = "SELECT DISTINCT article_id " .
                "FROM prophet.articles_assigned " .
                "WHERE user_id = " . User::get_default_instance('id') . " " .
                "and active = 1 and actioned is null ORDER BY added_when";

            $db->query($active_alerts);

            $article_array = [];

            while ($u = $db->next(MYSQLI_ASSOC)) {
                $article_array[] = $u['article_id'];
            }

            // if any active and unread notifications for user..
            if (!empty($article_array)) {
                // array to comma separated string
                $article_str = implode(', ', $article_array);

                $json['assigned_ids'] = $article_str;

                $query = "SELECT title, body, `date`, label " .
                    "FROM prophet.articles " .
                    "INNER JOIN prophet.articles_groups " .
                    "ON prophet.articles.id = prophet.articles_groups.article_id " .
                    "WHERE is_published = 1 AND articles.id in (" . $article_str . ")";

                if ($db->query($query)) {
                    while ($s = $db->next(MYSQLI_ASSOC)) {
                        $json['data'] .= "<p><u>" . $s['label'] . " - " . $s['title'] . " | " . date('H:i d-m-Y', $s['date']) . "</u><br/>" . $s['body'] . "</p>";
                    }
                }
            }

            echo json_encode($json);

            break;

        case "action_notification":
            $db = new mydb();
            $db->query("UPDATE prophet.articles_assigned
                            SET actioned = " . time() . "
                            WHERE user_id = " . User::get_default_instance('id') . "
                            AND article_id in (" . $_GET['assigned_ids'] . ")");
            break;

        case "news":
            $db = new mydb();

            $query = "SELECT prophet.articles.ID,title,introduction FROM prophet.articles
                        INNER JOIN prophet.articles_groups
                            ON prophet.articles.id = prophet.articles_groups.article_id
                      WHERE label = 'News' AND is_published = 1 ORDER BY ID DESC LIMIT 0,7";

            if ($db->query($query)) {
                echo "<ul>";
                while ($d = $db->next(MYSQLI_OBJECT)) {
                    $t = new Template("misc/articles/news.html");
                    $t->tag($d, 'news');
                    echo $t;
                }
                echo "</ul>";
            }

            break;
    }
}
