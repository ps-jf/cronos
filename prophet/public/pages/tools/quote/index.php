<?php

require_once($_SERVER[ "DOCUMENT_ROOT" ] . "/../bin/_main.php");

if (isset($_GET['do'])){
    switch ($_GET['do']){
        case "load_quote":
            $response = [
                "success" => true,
                "data" => null,
            ];

            $db = new mydb();

            $db->query("SELECT * FROM prophet.quotes ORDER BY RAND() LIMIT 1");

            if ($quote = $db->next(MYSQLI_ASSOC)){
                $response['data'] = [
                    "text" => $quote['text'],
                    "quotee" => $quote['quotee'],
                ];
            } else {
                $response['success'] = false;
            }

            echo json_encode($response);

            break;
    }
}