<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    if (isset($_GET["note"])) {
        $id = (int)$_GET["note"];

        switch ($_GET["do"]) {
            case "get":
                $db = new mydb();

                $query = "SELECT * FROM ". NOTES_TBL ." WHERE id =".$id." ORDER BY sticky DESC, parent_id, added_when DESC";

                if ($db -> query($query) && ( $d = $db -> next(MYSQLI_OBJECT) )) {
                    if ($_GET['modal']) {
                        $t = new Template("misc/notes/read-note-modal.html");
                    } else {
                        $t = new Template("misc/notes/read-note.html");
                    }

                    $t -> tag($d, 'note');
                    echo json_encode(''.$t);
                } else {
                    echo "false";
                }

                break;

            case "delete":
                $db = new mydb();
                echo json_encode($db -> query("UPDATE ". NOTES_TBL ." SET is_visible = false WHERE id = $id"));
                
                break;

            case "lock":
                $uid = User::get_default_instance("id");
                $db = new mydb();
                echo json_encode($db->query("UPDATE " . NOTES_TBL . " SET is_locked = 1 WHERE id = $id AND added_by = $uid"));
                break;
            
            case "unlock":
                $uid = User::get_default_instance("id");
                $db = new mydb();
                echo json_encode($db -> query("UPDATE ". NOTES_TBL ." SET is_locked = 0 WHERE id = $id AND added_by = $uid"));

                break;


            case "unstick":
                $db = new mydb();
                echo json_encode($db -> query("UPDATE ". NOTES_TBL ." SET sticky = 0 WHERE id = $id"));

                break;

            case "stick":
                $db = new mydb();
                echo json_encode($db -> query("UPDATE ". NOTES_TBL ." SET sticky = 1 WHERE id = $id"));

                break;

            case "tag-user":
                $response = [
                    'success' => true,
                    'data' => null,
                ];

                $note = new Notes($id, true);

                $tu = json_decode($note->tagged);

                if (!isset($tu)){
                    $tu = [];
                }

                $tu[] = $_GET['user'];

                $note->tagged(json_encode($tu));

                if ($note->save()){
                    // add to staff notifications table
                    $sn = new StaffNotifications();

                    $object = $note->object_type();
                    $object_id = $note->object_id();

                    $ob = new $object($object_id, true);

                    $message = $ob->link() . "<br>" . preg_replace("/[\n\r]/","",$note->text);

                    $sn->sender_id(User::get_default_instance("id"));
                    $sn->recipient_id($_GET['user']);
                    $sn->message($message);
                    $sn->added_when(time());
                    $sn->type("tag");

                    if ($sn->save()){
                        $u = new User($_GET['user'], true);

                        $response['data'] = $u->link();
                    } else {
                        $response['success'] = false;
                        $response['data'] = "Tagged user but failed to notify them";
                    }
                } else {
                    $response['success'] = false;
                    $response['data'] = "Failed to tag user";
                }

                echo json_encode($response);

                break;
        }
    } else {
        $object = $_GET["object"];
        $object_id = (int)$_GET["object_id"];
        if (isset($_GET["note_type"])) {
            $note_type = $_GET["note_type"];
        }
        if (isset($_GET["sticky"])) {
            $sticky = $_GET["sticky"];
        }
        
        $db = new mydb();

        switch ($_GET["do"]) {
            case "new":
                $query = "INSERT INTO ". NOTES_TBL ." ".
                    "SET object = '". $object."', ".
                    "object_id = ".$object_id.", ".
                    "sticky = ".$sticky.", ".
                    "note_type = '".$note_type."', ".
                    (  ( (int)$_GET["parent"] != 0 )
                        ? "parent_id = ". (int)$_GET["parent"] .", "
                        : false ).
                    "added_when = ".time().", ".
                    "added_by = ".User::get_default_instance('id').", ".
                    "text = '". addslashes(strip_tags($_POST["text"])) ."',".
                    "subject = '". addslashes(strip_tags($_POST["subject"])) ."'";

                if ($db -> query($query)) {
                    echo json_encode($db -> insert_id);
                } else {
                    echo "false";
                }
                break;

            case "edit":
                $db = new mydb();

                // get original text
                $query = "SELECT LTRIM(RTRIM(text)) as 'text', LTRIM(RTRIM(subject)) as 'subject' FROM " . NOTES_TBL ." WHERE id = ".$_GET['id'];
                $db -> query($query);

                while ($d = $db -> next(MYSQLI_ASSOC)) {
                    $orig_text = $d['text'];

                    if ($d['subject'] == "Enter Subject.." ||  $d['subject'] == "" ||  $d['subject'] == null) {
                        $orig_subject = "Enter Subject.." ;
                    } else {
                        $orig_subject = $d['subject'];
                    }
                }

                // get text when request submitted
                $new_text = trim(addslashes(strip_tags(htmlspecialchars($_GET['text']))));
                $new_subject = trim(addslashes(strip_tags(htmlspecialchars($_GET['subject']))));


                // if text has changed do update and audit
                if ($orig_text != $new_text || $orig_subject != $new_subject) {
                    //Used to make a backup copy of the note and put it in the notes audit table.
                    echo json_encode($db -> query("INSERT INTO    ". NOTES_AUDIT_TABLE ." (
                                                 note_id , object , object_id , is_visible , text , parent_id ,
                                                 added_when , added_by , note_type, sticky , subject ,
                                                 updated_when, updated_by )
                                                 SELECT  id ,object , object_id , is_visible , LTRIM(RTRIM(text)), LTRIM(RTRIM(subject)), parent_id , added_when , added_by ,
                                                         note_type, sticky , " .time()." , ".User::get_default_instance('id')."
                                                 FROM " . NOTES_TBL ."
                                                 WHERE id = ".($_GET['id'])." "));


                    echo json_encode($db -> query("UPDATE ". NOTES_TBL ." SET text = '". addslashes(strip_tags($_GET['text'])) ."', subject = '". addslashes(strip_tags($_GET['subject'])) ."' WHERE id = ".($_GET['id'])));
                }


                break;

            case "list":

                if ($_GET['object'] == "WorkflowAudit") {

                    /* if we are viewing a historic workflow ticket we need to check if there are any notes from the
                    original workflow ticket to show */
                    $wfaudit = new WorkflowAudit($_GET['object_id'], true);

                    $query = "SELECT notes.*, first_name, last_name 
                    FROM " . NOTES_TBL . "
                    INNER JOIN " . USR_TBL . " ON " . USR_TBL . ".id = " . NOTES_TBL . ".added_by
                    WHERE (
                        object = 'Workflow'
                        AND object_id = " . $wfaudit->workid() . "
                    ) or (
                        object = 'WorkflowAudit'
                        AND object_id = " . $object_id . "
                    )
                    AND is_visible = true 
                    ORDER BY parent_id, sticky DESC, added_when DESC";
                } else {

                    $query = "SELECT notes.*, first_name, last_name ".
                        "FROM " . NOTES_TBL . " " .
                        "INNER JOIN ".USR_TBL." ON ".USR_TBL.".id =".NOTES_TBL.".added_by ".
                        "WHERE object = '" . $object . "' " .
                        "AND is_visible = true ".
                        "AND object_id = " . $object_id . " " .
                        "ORDER BY parent_id, sticky DESC, added_when DESC";
                }



                $db -> query($query);

                function push_note($id, &$notes, $n)
                {
                    $found = $note = false;

                    for ($i=0; $i < count($notes); $i++) {
                        if ($notes[ $i ][ 'data' ] -> id == $id) {
                            $notes[ $i ][ 'replies' ][] = $n;
                            return true;
                        } else if (count($notes[$i]['replies']) > 0) {
                            $found = push_note($id, $notes[ $i ]['replies'], $n);
                        }
                    }
                
                    return $found;
                }

                function output_thread($thread)
                {
                    $out = "<ul>";

                    foreach ($thread as $note) {

                        // tweak icons which are shown
                        if ($note['data']->note_type == "nb_email") {
                            $note['data']->note_type = "email";
                        } elseif ($note['data']->note_type == "nb_phone") {
                            $note['data']->note_type = "phone";
                        } elseif ($note['data']->note_type == "nb_meet") {
                            $note['data']->note_type = "meet";
                        }  elseif ($note['data']->note_type == "nb_additional_note") {
                            $note['data']->note_type = "note";
                        }

                        ($note['data']->subject) ? $placeholder = "" : $placeholder = "subject_placeholder";

                        if ($note['data']->first_name && $note['data']->last_name) {
                            $user = new User($note['data']->added_by);
                            $firstname = $note['data']->first_name;
                            $lastname = $note['data']->last_name;
                            $fullname = $user->link($firstname). " " . $user->link($lastname);
                        } else {
                            $username = User::get_default_instance('username');
                            $fullname = $username;
                        }

                        $taggedUsers = json_decode($note['data']->tagged);

                        $userLinks = "";

                        if (!empty($taggedUsers)){
                            foreach ($taggedUsers as $uid){
                                $u = new User($uid, true);

                                $userLinks .= " " . $u->link();
                            }
                        }

                        $t = new Template("misc/notes/read-note-modal.html", get_defined_vars());
                        $t -> tag($note['data'], 'note');

                        if (count($note['replies']) > 0) {
                            $t -> tag(output_thread($note['replies']), 'replies');
                        }

                        $out .= "<ul>".$t -> output()."</ul>";
                    }

                    $out = str_replace(utf8_decode("£"), "&pound;", $out);
                    $out = str_replace(utf8_decode("Â"), "&nbsp;", $out);

                    return $out . "</ul>";
                }


                $notes = [];
                $noteCount = 0;
                while ($d = $db -> next(MYSQLI_OBJECT)) {
                    $note = [
                        'data'    => $d,
                        'replies' => []
                    ];

                    $noteCount++;

                    if ($d -> parent_id !== null) {
                        push_note($d -> parent_id, $notes, $note);
                    } else {
                        $notes[] = $note;
                    }
                }


                echo "<div class='row no-gutters'>";
                echo "<div class='col-5'>";

                if ($object == "NewBusiness") {
                    echo el::bt("NB Additional Note", false, "class=\"add-note\" object=\"$object\" object_id=\"$object_id\" note_type=\"nb_additional_note\"");
                } else {
                    echo el::bt("Add Note", false, "class=\"add-note\" object=\"$object\" object_id=\"$object_id\" note_type=\"note\"");
                }
                echo
                    el::bt("&#9660;", false, "class=\"alter-note\"").
                    "<ul id='note_types'>";

                if ($object == "NewBusiness") {
                    echo " <li id='nb_phone'>NB Phonecall</li>
                        <li id='nb_email'>NB Email</li>
                        <li id='nb_meet'>NB Meeting</li>
                        <li id='nb_internal'>NB Internal</li>
                        <li id='compliance'>Compliance</li>";
                } else {
                    echo " <li id='phone'>Phonecall</li>
                        <li id='email'>Email</li>
                        <li id='meet'>Meeting</li>
                        <li id='other'>Other</li>
                        <li id='vunerable'>Vunerable Client</li>";
                }

                echo "</ul></div>";
                echo "<div class='col-2 text-left'><span class='badge badge-pill badge-danger'>" . $noteCount . "</span> " . pluralize($noteCount, "note") . "</div>";
                echo "<div class='col-5 text-right'>";
                echo "<button class='note_subject_search_btn'><i class='fas fa-search'></i> Search Note Content</button>";
                echo "<input type='text' class='collapse note_subject_search' placeholder='Filter By Subject/Text'>";
                echo "</div>";
                echo "</div>";

                echo "<ul class=\"notes\" object=\"$object\" object_id=\"$object_id\">".
                     output_thread($notes).
                     "</ul>";
                echo "</div>";

                break;

            case "list_modal":

                $query = "SELECT notes.*, first_name, last_name ".
                    "FROM " . NOTES_TBL . " " .
                    "INNER JOIN ".USR_TBL." ON ".USR_TBL.".id =".NOTES_TBL.".added_by ".
                    "WHERE object = '" . $object . "' " .
                    "AND is_visible = true ".
                    "AND object_id = " . $object_id . " " .
                    "ORDER BY parent_id, sticky DESC, added_when DESC";

                $db->query($query);

                function push_note($id, &$notes, $n)
                {
                    $found = $note = false;

                    for ($i=0; $i < count($notes); $i++) {
                        if ($notes[ $i ][ 'data' ] -> id == $id) {
                            $notes[ $i ][ 'replies' ][] = $n;
                            return true;
                        } else if (count($notes[$i]['replies']) > 0) {
                            $found = push_note($id, $notes[ $i ]['replies'], $n);
                        }
                    }

                    return $found;
                }

                function output_thread($thread)
                {
                    $out = "<ul>";

                    foreach ($thread as $note) {

                        // tweak icons which are shown
                        if ($note['data']->note_type == "nb_email") {
                            $note['data']->note_type = "email";
                        } elseif ($note['data']->note_type == "nb_phone") {
                            $note['data']->note_type = "phone";
                        } elseif ($note['data']->note_type == "nb_meet") {
                            $note['data']->note_type = "meet";
                        }  elseif ($note['data']->note_type == "nb_additional_note") {
                            $note['data']->note_type = "note";
                        }

                        ($note['data']->subject) ? $placeholder = "" : $placeholder = "subject_placeholder";

                        if ($note['data']->first_name && $note['data']->last_name) {
                            $user = new User($note['data']->added_by);
                            $firstname = $note['data']->first_name;
                            $lastname = $note['data']->last_name;
                            $fullname = $user->link($firstname). " " . $user->link($lastname);
                        } else {
                            $username = User::get_default_instance('username');
                            $fullname = $username;
                        }

                        $taggedUsers = json_decode($note['data']->tagged);

                        $userLinks = "";

                        if (!empty($taggedUsers)){
                            foreach ($taggedUsers as $uid){
                                $u = new User($uid, true);

                                $userLinks .= " " . $u->link();
                            }
                        }

                        $t = new Template("misc/notes/read-note-modal.html", get_defined_vars());
                        $t -> tag($note['data'], 'note');

                        if (count($note['replies']) > 0) {
                            $t -> tag(output_thread($note['replies']), 'replies');
                        }

                        $out .= "<ul>".$t -> output()."</ul>";
                    }

                    $out = str_replace(utf8_decode("£"), "&pound;", $out);
                    $out = str_replace(utf8_decode("Â"), "&nbsp;", $out);

                    return $out . "</ul>";
                }

                $notes = [];

                while ($d = $db -> next(MYSQLI_OBJECT)) {
                    $note = [
                        'data'    => $d,
                        'replies' => []
                    ];

                    if ($d->parent_id !== null) {
                        push_note($d -> parent_id, $notes, $note);
                    } else {
                        $notes[] = $note;
                    }
                }


                echo "<div class='row no-gutters'>";
                echo "<div class='col-6'>";

                if ($object == "NewBusiness") {
                    echo el::bt("NB Additional Note", false, "class=\"add-note\" object=\"$object\" object_id=\"$object_id\" note_type=\"nb_additional_note\"");
                } else {
                    echo el::bt("New Note", false, "class=\"add-note\" object=\"$object\" object_id=\"$object_id\" note_type=\"note\"");
                }

                echo
                    el::bt("&#9660;", false, "class=\"alter-note\"").
                    "<ul id='note_types'>";

                if ($object == "NewBusiness") {
                    echo " <li id='nb_phone'>NB Phonecall</li>
                        <li id='nb_email'>NB Email</li>
                        <li id='nb_meet'>NB Meeting</li>
                        <li id='nb_internal'>NB Internal</li>
                        <li id='compliance'>Compliance</li>";
                } else {
                    echo " <li id='phone'>Phonecall</li>
                        <li id='email'>Email</li>
                        <li id='meet'>Meeting</li>
                        <li id='other'>Other</li>
                        <li id='vunerable'>Vulnerable Client</li>";
                }
                echo "</ul></div>";
                echo "<div class='col-6 text-right'>";
                echo "<button class='note_subject_search_btn'><i class='fas fa-search'></i> Search By Note Content</button>";
                echo "<input type='text' class='collapse note_subject_search' data-modal='true' placeholder='Filter By Subject/Text'>";
                echo "</div>";
                echo "</div>";

                echo "<ul class=\"notes-modal\" object=\"$object\" object_id=\"$object_id\">".
                    output_thread($notes).
                    "</ul>";
                echo "</div>";

                break;
        }
    }
} else {
    if (isset($_GET["search"])) {
        $json = (object) [
            "search" => null,
            "status" => false,
            "error" => null,
            "page_no" => null,
            "pages" => null,
            "record_count" => null,
            "data" => null
        ];

        if (strlen($_GET["search"]) < 3) {
            $json -> error = "Do not search for less than 3 characters";
            echo json_encode($json);
            exit();
        }

        $row_t = Template("misc/notes/list_row.html");

        //convert any spaces in $_GET['search'] to %
        $search = str_replace(" ", "%", $_GET['search']);

        //blacklist of words that bring back an excessive number of results
        $blacklist = ["and","the","not","left","email","send","sent","from","have","been","call","date",
                    "his","this","her","help","helpdesk","partner","policy","client","letter"];

        if (in_array($_GET['search'], $blacklist)) {
            $json -> error = "Too many results, Please make search more specific";
            echo json_encode($json);
            exit();
        }

        if (!isset($_GET['offset'])) {
            $offset = 0;
        } else {
            $offset = (int)$_GET['offset'] * 20;
        }
            
        if (strpos($search, "'") !== false) {
            $search = str_replace(utf8_decode("'"), "''", $search);
        }
            
        $db = new mydb();
        $q = "SELECT SQL_CALC_FOUND_ROWS * FROM notes WHERE text LIKE '%".addslashes($search)."%' AND is_visible = 1 LIMIT 20 OFFSET ".$offset;
        $q1 = "SELECT FOUND_ROWS();";

        $db->query($q);
        $db->query($q1);

        //result($x) & page count
        $x = $db->next(MYSQLI_NUM);
        $x = (int)array_shift($x);
        $pages = $x / 20;
            
        //create array used to populate select#page_number
        $i = 0;
        $page_no = [];
        while ($i <= $pages) {
            $i++;
            $page_no[] = $i;
        }

        //query content
        $notes = "";
        $db->query($q);
        while ($note = $db->next(MYSQLI_ASSOC)) {
            $row_t->tag($note, "note");
            $notes .= $row_t;
        }

        if ($q && $q1) {
            $json->status=true;
        } else {
            $json->status=false;
        }

        //fix for encoding issue with certain characters
        $notes = str_replace(utf8_decode("£"), "&pound;", $notes);
        $notes = str_replace(utf8_decode("Â"), "&nbsp;", $notes);

        $json -> search = $_GET['search'];
        $json -> page_no = $page_no;
        $json -> record_count = $x;
        $json -> pages = ceil($pages);
        $json -> data = $notes;

        echo json_encode($json);
    } else {
        echo new Template("misc/notes/search.html");
    }
}
