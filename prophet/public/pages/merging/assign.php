<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 13/08/2019
 * Time: 09:14
 */
require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

use Carbon\Carbon;

if (isset($_GET['id'])){
    //create a datachange object using the ID given
    $dc = new DataChange($_GET['id'], true);

    if (isset($_GET['do'])){
        switch ($_GET['do']){
            case "assign":
                $response = [
                    'success' => true,
                    'data' => null,
                ];

                try{
                    $user  = new User($_GET['user'], true);

                    $s = new Search(new DataChange());
                    $s->eq("proposed_by", $dc->proposed_by());
                    $s->eq("proposed_time", $dc->proposed_time());

                    if ($s->count()){
                        while($d = $s->next(MYSQLI_ASSOC)){
                            $d->allocated($user->id());

                            if(!$d->save()){
                                $response['success'] = false;
                                $response['data'] = "Failed to assign merge " . $dc->id() . " to " . $user->username;
                            } else {
                            }
                        }
                    } else {
                        $response['success'] = false;
                        $response['data'] = "Failed to find merges with these credentials!";
                    }

                    if ($response['success']){
                        $response['data'] = "Assigned merge " . $dc->id() . " to " . $user->username;
                    }
                } catch (Exception $e){
                    $response['success'] = false;
                    $response['data'] = $e->getMessage();
                }

                echo json_encode($response);

                break;
        }
    }
} elseif (isset($_GET['do'])){
    switch ($_GET['do']){
        case "list":
            $response = [
                'success' => true,
                'error' => null,
                'data' => null,
            ];

            $db = new mydb(REMOTE);

            $query = "SELECT id, count(*) as total, proposed_by, proposed_time 
                      FROM " . REMOTE_SYS_DB . ".data_change 
                      WHERE comments = \"" . $_GET['type'] . "\" 
                        and committed_by is null 
                        and allocated is null 
                      group by proposed_by, proposed_time";

            $db->query($query);

            while($row = $db->next(MYSQLI_ASSOC)){
                $proposed = new WebsiteUser($row['proposed_by'], true);
                $when = Carbon::createFromTimestamp($row['proposed_time']);

                $response['data'] .= "<tr data-id='" . $row['id'] . "' id='merge_row_" . $row['id'] . "'>
                    <td><input type='checkbox' class='merge_check' value='" . $row['id'] . "'></td>
                    <td>" . $row['id'] . "</td>
                    <td>" . $row['total'] . "</td>
                    <td>" . strval($proposed) . "</td>
                    <td>" . $when->format('D, jS M Y g:i a') . "</td>
                    <td class='assigner'>" . User::active_users(7) . "</td>
                </tr>";
            }

            echo json_encode($response);

            break;
    }
} else {
    echo new Template("merging/assign.html");
}
