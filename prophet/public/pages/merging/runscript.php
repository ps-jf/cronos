<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$redisClient = RedisConnection::connect();

if (isset($_GET["id"])) {
	
    $return = [
        "id"     => false,
		"status" => false, 
        "description" => "",
        "error"  => false,
        "policies" => []
	];

    if ($redisClient->get("incomerun")){
        $return['error'] = true;
        $return['description'] = "Merges cannot be actioned whilst the income run is being processed.";
    } else {
        $main = $_GET["id"];
        $dups = $_GET["duplicates"];
        $ptime = '';
        $problem = $_GET["problem"];
        $dcid = $_GET['dcid'];
        $dc = new DataChange($dcid, true);

        // get a list of all policies belonging to the clients that will be merged
        $s = new Search(new Policy());

        $dupClients = explode(",", $dups);

        $s->eq("client", $dupClients);

        while($p = $s->next(MYSQLI_ASSOC)){
            $return['policies'][] = [
                "id" => $p->id(),
                "client" => strval($p->client),
                "client_id" => $p->client->id(),
                "owner" => strval($p->getOwnerAttribute($p->owner())),
            ];
        }

        // Set paths to the python script to run
        $python = '/usr/bin/python2.7';
        if ($_GET['object'] == "client") {
            $ptime = $_GET["proposed_time"];
            $py_script = '/usr/local/bin/merge_client_bigmummy.py';
        } elseif ($_GET['object'] == "policy") {
            $py_script = '/usr/local/bin/merge_policy_bigmummy.py';
        }

        // lets store an instance of the old objects in memory, just for any reporting afterwards
        $objects = [];

        $s = new Search(new DataChange());

        $s->eq("object_type", $dc->object_type());
        $s->eq("proposed_time", $dc->proposed_time());
        $s->eq("proposed_by", $dc->proposed_by());

        while($adc = $s->next(MYSQLI_ASSOC)){
            $object = ucfirst($adc->object_type());
            $objects[] = new $object($adc->object_id(), true);
        }

        if (!file_exists($python)) {
            #print("The python executable '$python' does not exist!");
            $return["error"] = true;
            $return["description"] .= "The python executable '$python' does not exist!";
        }
        if (!is_executable($python)) {
            #print("The python executable '$python' is not executable!");
            $return["error"] = true;
            $return["description"] .= "The python executable '$python' is not executable!";
        }
        if (!file_exists($py_script)) {
            #print("The python script file '$py_script' does not exist!");
            $return["error"] = true;
            $return["description"] .= "The python script file '$py_script' does not exist!";
        }
        if (!is_executable($py_script)) {
            #print("The python script '$python' is not executable!");
            $return["error"] = true;
            $return["description"] .= "The python script '$py_script' is not executable!";
        }

        if ($return["error"]) {
            echo json_encode($return);
            exit();
        }

        // Build up the command for the cmd line
        $cmd = "$python $py_script";
        exec($cmd . " " . $main . " " . $dups . " " . $ptime. " 2>&1", $output, $return_code);

        //	If return code returns 0, no problems have arisen
        if ($return_code == 0) {
            $return["description"] = "Merge successful";
            $return["status"] = true;
            $return["id"] = $main;
            /* If previously a 'problem merge' - Remove data change error after problem has been rectified
            and clients have been merged */
            if ($problem == true) {
                $db = new mydb();
                $db->query("DELETE FROM " . DATA_CHANGE_ERROR_TBL . " WHERE data_change_id = " . $dcid);
            }

            if ($_GET['object'] == "client"){
                if($dc->proposed_by->staff_member()){
                    // send the staff member an email to let them know its done
                    $dc->successEmail($objects);
                }

            }

        } else {
            $errormessage = "";
            // Loop through the return array and create a string of errors.
            foreach ($output as $r) {
                $errormessage .= $r . " ";
            }

            // If array returned with data in it, then json return the data
            $return["error"] = true;
            $return["description"] = $errormessage;
        }
    }

    echo json_encode($return);
}
