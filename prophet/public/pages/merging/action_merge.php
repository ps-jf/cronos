<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $ids = [];

    $db = new mydb(REMOTE, E_USER_WARNING);
    if (mysqli_connect_errno()) {
        echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
        exit();
    }

    if ($_GET['object'] == "client") {
        $object = "Client";
        $template = "client_action_form";
    } else if ($_GET['object'] == "policy") {
        $object = "Policy";
        $template = "policy_action_form";
    }

    $db->query("SELECT " . REMOTE_DATA_CHANGE_TBL . ".id, object_id 
				  FROM " . REMOTE_DATA_CHANGE_TBL . "
				  INNER JOIN " . REMOTE_USR_TBL . " 
				  ON " . REMOTE_DATA_CHANGE_TBL . ".proposed_by = " . REMOTE_USR_TBL . ".id 
                  WHERE object_type = '" . $object . "' AND comments = '" . $object . " Merge' 
                  AND committed_by IS NULL AND committed_time IS NULL AND proposed_time = " . $_GET['time'] . " 
                  AND proposed_by = " . $_GET['proposed_by']);

    while ($d = $db->next()) {
        array_push($ids, $d['object_id']);
    }

    $merge = new Merge($_GET["id"], true);

    echo new Template("merging/" . $template . ".html", [
        "merge" => $merge,
        "ids" => $ids,
        "proposed_time" => $_GET['time'],
        "problem" => $_GET['problem'] ?? '',
        "dcid" => $_GET['id'],
        "object" => $object
    ]);
} elseif(isset($_GET['do'])) {
    switch($_GET['do']){
        case "policy_owners":
            if(isset($_GET['save'])) {
                $response = [
                    "success" => true,
                    "data" => null,
                ];

                foreach($_POST['policies'] as $id => $owner){
                    $p = new Policy($id, true);

                    $p->owner($owner);

                    if(!$p->save()){
                        $response['success'] = false;
                        $response['data'] .= "Failed to save owner of policy ID: " . $p->id() . ".\r\n";
                    }
                }

                echo json_encode($response);
            } elseif(isset($_GET['policies'])){
                $response = [
                    "success" => true,
                    "data" => null,
                ];

                foreach($_GET['policies'] as $pol){
                    // $pol is the old policy details
                    $p = new Policy($pol['id'], true);
                    // $p is the new policy details


                    $response['data'] .= "<tr class='owner_row' data-policy='" . $p->id() . "'>
                                            <td>" . $p->link() . "</td>
                                            <td>" . $pol['client'] . " (" . $pol['client_id'] . ")</td>
                                            <td>" . $pol['owner'] . "</td>
                                            <td>" . $p->getOwner($p->owner(), "policy[owner]", "JSON") . "</td>
                                        </tr>";
                }

                echo json_encode($response);
            } else {
                echo new Template("merging/policy_owners.html");
            }
    }
} else {
    die("Profile not found");
}
