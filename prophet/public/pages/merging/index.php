<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$merge = new Merge();

if (isset($_GET["id"])) {
    if (isset($_GET["list"])) {
        switch ($_GET["list"]) {
            case "client":
                $db = new mydb(REMOTE, E_USER_WARNING);
                if (mysqli_connect_errno()) {
                    echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                    exit();
                }

                // Query now limited by user id to allow more people to merge at the same time 'allocated = .......'
                $db->query("SELECT " . REMOTE_DATA_CHANGE_TBL . ".id, object_type, object_id, 
                " . REMOTE_USR_TBL . " .email as req_by, 
                " . REMOTE_USR_TBL . ".id as req_id, proposed_time, 
                from_unixtime(proposed_time, '%d-%m-%Y %h:%i:%s') as p_time 
							FROM " . REMOTE_DATA_CHANGE_TBL . "
							INNER JOIN " . REMOTE_USR_TBL . " 
							ON " . REMOTE_DATA_CHANGE_TBL . ".proposed_by = " . REMOTE_USR_TBL . ".id 
                    INNER JOIN prophet.users
                    ON " . REMOTE_DATA_CHANGE_TBL . ".allocated = prophet.users.id
                    WHERE committed_by IS NULL AND committed_time IS NULL
                    AND data_change.allocated = " . $_COOKIE['uid'] . "
                    AND " . REMOTE_DATA_CHANGE_TBL . ".comments = 'Client Merge'
                    AND proposed_time NOT IN (select proposed_time from " . DATA_CHANGE_ERROR_TBL . "
                    INNER JOIN " . REMOTE_DATA_CHANGE_TBL . "
                    ON " . DATA_CHANGE_ERROR_TBL . ".data_change_id = " . REMOTE_DATA_CHANGE_TBL . ".id
                    WHERE " . REMOTE_DATA_CHANGE_TBL . ".comments = 'Client Merge')
							GROUP BY p_time, req_by
							ORDER BY proposed_time
							LIMIT 100");


                while ($d = $db->next()) {

                    echo "<tr id='$d[id]' name='$d[id]'>
                <td>" . $d['id'] . "</td>
                <td>" . $d['object_type'] . "</td>
                <td>" . $d['req_by'] . "</td>
                <td>" . $d['p_time'] . "</td>
                <td>
                    <button href=\"/pages/merging/action_merge.php?id=" .
                        $d['id'] . "&time=" .
                        $d['proposed_time'] . "&object=client&proposed_by=" .
                        $d['req_id'] . "\">Action</button>
                    <button class='delete-merge' data-id=" . $d['id'] . ">
                        <i class='fas fa-trash-alt text-danger'></i>
                    </button>
                </td>
                </tr>";
                }

                break;

            case "policy":
                $db = new mydb(REMOTE);

                // Query now limited by user id to allow more people to merge at the same time 'allocated = .......'
                $db->query("SELECT " . REMOTE_DATA_CHANGE_TBL . ".id, object_type, object_id, " . REMOTE_USR_TBL . "
				              .email as req_by, " . REMOTE_USR_TBL . ".id as req_id, proposed_time, 
				              from_unixtime(proposed_time, '%d-%m-%Y %h:%i:%s') as p_time
						FROM " . REMOTE_DATA_CHANGE_TBL . " 
						INNER JOIN " . REMOTE_USR_TBL . " 
						ON " . REMOTE_DATA_CHANGE_TBL . ".proposed_by = " . REMOTE_USR_TBL . ".id
						INNER JOIN prophet.users
						ON " . REMOTE_DATA_CHANGE_TBL . ".allocated = prophet.users.id
						WHERE committed_by IS NULL AND committed_time IS NULL
                        AND data_change.allocated = " . User::get_default_instance('id') . "
                    AND " . REMOTE_DATA_CHANGE_TBL . ".comments = 'Policy Merge'
						AND proposed_time NOT IN (select proposed_time from " . DATA_CHANGE_ERROR_TBL . "
						INNER JOIN " . REMOTE_DATA_CHANGE_TBL . " 
						ON " . DATA_CHANGE_ERROR_TBL . ".data_change_id = " . REMOTE_DATA_CHANGE_TBL . ".id 
                    WHERE " . REMOTE_DATA_CHANGE_TBL . ".comments = 'Policy Merge')
						GROUP BY p_time, req_by 
						ORDER BY proposed_time 
						LIMIT 100");


                while ($d = $db->next()) {
                    echo "<tr id='$d[id]' name='$d[id]'>
                                <td>" . $d['id'] . "</td>
                                <td>" . $d['object_type'] . "</td>
                                <td>" . $d['req_by'] . "</td>
                                <td>" . $d['p_time'] . "</td>
                                <td><button href=\"/pages/merging/action_merge.php?id=" .
                        $d['id'] . "&time=" .
                        $d['proposed_time'] . "&object=policy&proposed_by=" .
                        $d['req_id'] . "\">Action</button>
                        <button class='delete-merge' data-id=" . $d['id'] . ">
                            <i class='fas fa-trash-alt text-danger'></i>
                        </button></td>
					</tr>";
                }

                break;
        }
    } else if (isset($_GET["report"])) {

        // Add merge request to error table to lookup later
        $json = [
            "status" => false,
            "error" => false
        ];



        if (isset($_GET['object'])) {

            $dc = new DataChange($_GET['id'], true);

            $object_type = ucfirst($_GET['object']);

            $db = new mydb();
            $q = "INSERT INTO feebase.data_change_error(data_change_id, addedby, addedwhen, comments, object) VALUES(" . $_GET['id'] . "," . $_GET['user'] . "," . time() . ",'" . addslashes($_GET['comments']) . "','" . $object_type . "')";

            if ($db->query($q)) {
                if ($dc->problemEmail($_GET['object'], $_GET['comments'])) {
                    $json['status'] = " An email has been sent to " . $dc->proposed_by->email_address();
                } else {
                    $json['status'] = "Email failed to send";
                    $json['error'] = true;
                }
            } else {
                $json['status'] = "Failed to mark as problem.";
                $json['error'] = true;
            }
        } else {
            $json['status'] = "Object not passed through, contact IT.";
            $json['error'] = true;
        }

        echo json_encode($json);
    } elseif (isset($_GET['do'])){
        switch ($_GET['do']){
            case "resend_email":
                $response = [
                    "success" => false,
                    "data" => null
                ];

                $problem = new MergeError($_GET['id'], true);
                $dc = new DataChange($problem->data_change_id(), true);

                if ($response['success'] = $dc->problemEmail($_GET['object'], $_GET['comments'])){
                    $problem->contacted(1);

                    $problem->save();
                } else {
                    $response['data'] = "Failed to send problem email";
                }

                echo json_encode($response);

                break;

            case "delete":
                $json = [
                    "success" => true,
                    "data" => "",
                ];

                $m = new Merge($_GET['id'], true);

                $merges = $deleted = 0;

                $s = new Search(new Merge());
                $s->eq("proposed_time", $m->proposed_time());
                $s->eq("proposed_by", $m->proposed_by());
                $s->eq("comments", $m->comments());

                while($merge = $s->next(MYSQLI_ASSOC)){
                    $merges++;

                    if($merge->delete()){
                        $deleted++;
                    }
                }

                if ($merges !== $deleted){
                    $json['success'] = false;
                    $json['data'] = "Number of records deleted does not match! Contact IT!";
                }

                echo json_encode($json);

                break;
        }
    }
} elseif (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "reason_popup":
            echo new Template("merging/reason_popup.html", ["merge" => $merge]);
            break;
    }
} else {
    if ($_GET['object'] == "client") {
        echo new Template("merging/client_home.html", ["merge" => $merge]);
    } else if ($_GET['object'] == "policy") {
        echo new Template("merging/policy_home.html", ["merge" => $merge]);
    }
}
