<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

use Carbon\Carbon;

if (isset($_GET["do"])) {

    switch ($_GET["do"]) {
        case "show":
            try {
                if (isset($_GET['id'])) {
                    $action = Breaches::get_common_url(['id'=>$_GET['id'], 'do'=>'save']);
                    $breach = new Breaches($_GET['id'], true);

                    $users = Breaches::allUsers($breach->caused_by);

                    $s = new Search(new FileMapping());
                    $s->eq("object_id", $breach->id());
                    $s->eq("object", "Breaches");

                } else {
                    $users = Breaches::allUsers();
                    $action = Breaches::get_common_url(['do'=>'create']);
                    $breach = new Breaches();
                    $s = false;
                }

                if(isset($_GET['assigned_view'])){
                    echo new Template("breaches/show_assigned.html", ['breach' => $breach, 'action' => $action, 'files' => $s, 'users' => $users]);
                } else {
                    echo new Template("breaches/show.html", ['breach' => $breach, 'action' => $action,
                        "files" => $s, 'users' => $users]);
                }

            } catch (Exception $e) {
                $e->getMessage();
            }
        break;

        case "uploadForm":
            try {
                if (isset($_GET['id'])) {
                    $breach = new Breaches($_GET['id'], true);
                    echo new Template("documentstorage/upload_form.html", ['object' => $breach]);
                }

            } catch (Exception $e) {
                $e->getMessage();
            }
            break;

        case "latest":

            $s = new Search(new Breaches);
            $s->add_order('id', 'DESC');
            $s->limit(1);

            if (($b = $s->next()) !== false) {
                $carbon = Carbon::createFromTimestamp($b->added_when())->startofDay();
                if ($carbon->diffInDays() == 0) {
                    echo "New breach recorded today";
                } else {
                    echo $carbon->diffInDays(). " ".pluralize($carbon->diffInDays(), 'day', 'days')." since last recorded breach";
                }
            }
            break;

        case "archive":
            $json = [
                "status" => "An error occurred, try again or contact IT",
                "error" => true
            ];

            try{
                if (isset($_GET['id'])) {
                    $breach = new Breaches($_GET['id'], true);
                    if ($breach) {
                        $breach->archived_by(User::get_default_instance('id'));
                        $breach->archived_when(time());
                        if ($breach->save()) {
                            $json['status'] = "Breach successfully archived";
                            $json['error'] = false;
                        }
                    }
                }
            } catch (Exception $e){
                $json['error'] = true;
                $json['status'] = $e->getMessage();
            }



            echo json_encode($json);

            break;

        case "report":

            $users = Breaches::allUsers();
            $action = Breaches::get_common_url(['do'=>'create']);
            $breach = new Breaches();
            $s = false;

            echo new Template("breaches/report.html", ['breach' => $breach, 'action' => $action,
                "files" => $s, 'users' => $users]);

            break;

        case "checkSpotCheck":
            $json = [
                "success" => true,
                "data" => null
            ];

            $db = new mydb();

            $q = "SELECT * FROM " . DATA_DB . ".correspondence_audit where corrID = " . $_GET['id'] . " AND spot_checked = 1";

            $db->query($q);

            if ($sc = $db->next(MYSQLI_ASSOC)){
                $checker = new User($sc['checkedBy'], true);
                $checkTime = Carbon::parse($sc['dateStamp']);

                $ct = $checkTime->toFormattedDateString();

                $json['data'] = "<span class='text-success'><i class='fas fa-check'></i> Spot Checked by " . $checker->username . " on " . $ct . "</span>";
            } else {
                $json['data'] = "<span class='text-danger'><i class='fas fa-times'></i> Not Spot Checked</span>";
            }

            echo json_encode($json);

            break;

        case "count_assigned":
            $response = [
                "success" => true,
                "count" => 0
            ];

            $s = new Search(new Breaches());

            $s->eq("assigned", User::get_default_instance("id"));
            $s->eq("archived_by", null);

            $response['count'] = $s->count();

            echo json_encode($response);

            break;
    }

} else {
    try {
        if(isset($_GET['get'])){
            switch($_GET['get']){
                case "assigned":
                    $breaches = [];

                    $s = new Search(new Breaches());

                    $s->eq("assigned", User::get_default_instance("id"));
                    $s->eq("archived_by", null);

                    while($b = $s->next(MYSQLI_ASSOC)){
                        $breaches[] = $b;
                    }

                    echo new Template("breaches/assigned.html", compact([
                        "breaches"
                    ]));

                    break;
            }
        } else {
            $active = [];
            $archived = [];

            $s = new Search(new Breaches);
            while ($breach = $s->next(MYSQLI_ASSOC)) {

                if (empty($breach->archived_by())) {
                    $active[] = $breach;
                } else {
                    $archived[] = $breach;
                }
            }

            echo new Template("breaches/index.html", [
                'active' => $active,
                'archived' => $archived
            ]);
        }

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
