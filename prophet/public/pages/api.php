<?php
/**
 * First attempt at a policy services RESTful API.
 *
 *
 * A REST api would be useful in so many cases throughout Prophet
 * and will especially be useful going forward if we want to get fancy with
 * most of our features.
 *
 * I will implement GET,POST,PUT and DELETE on top of our existing structure.
 *
 *
 * @package prophet.core
 * @author kevin.dorrian
 * @version 0.1a
 * @todo this is massively flakey. e.g doesnt like upper and lowercase nouns
 */

//we want to skip the normal cookie based auth here

require $_SERVER["DOCUMENT_ROOT"]."/../bin/_lib.php";

class Request
{
    public $url_elements;
    public $verb;
    public $parameters;

    public function __construct()
    {
        $this->verb = $_SERVER['REQUEST_METHOD'];
        $this->url_elements = explode('/', $_SERVER['PATH_INFO']);

        $this->checkPermissions();

        $this->parseIncomingParams();

        // initialise json as default format
        $this->format = 'json';

        if (isset($this->parameters['format'])) {
            $this->format = $this->parameters['format'];
        }

        $this->completeRequest();
    }

    public function parseIncomingParams()
    {
        $parameters = [];

        // first of all, pull the GET vars
        if (isset($_SERVER['SCRIPT_NAME'])) {
            //remove /api and trailing slash
            $req = preg_replace('/\/api\//', '', $_SERVER['SCRIPT_NAME']);
            $parameters = explode("/", $req);
        } else {
            header('HTTP/1.0 400 Bad Request');
            echo "It appears as though your request was guff, try again";
            exit;
        }

        // now how about PUT/POST bodies? These override what we got from GET
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);
                if ($body_params) {
                    foreach ($body_params as $param_name => $param_value) {
                        $parameters[$param_name] = $param_value;
                    }
                }
                $this->format = "json";
                break;

            case "application/x-www-form-urlencoded":
                parse_str($body, $postvars);
                foreach ($postvars as $field => $value) {
                    $parameters[$field] = $value;
                }
                $this->format = "html";
                break;

            default:
                // we could parse other supported formats here
                break;
        }
        $this->parameters = $parameters;
    }

    public function checkPermissions()
    {

        if (isset($_COOKIE["uid"], $_COOKIE[ "u_enc" ])) {
            $_USER = new User($_COOKIE["uid"]);

            if (!$_USER->get() || !$_USER->valid_cookies()) {
                /* If user cannot be found on db or the security cookies
                 * appear to have been tampered with, reset cookies and
                 * throw an unauthorised header
                */

                $_USER->logout();
                //@todo should actually be 401, doesnt seem to work
                header('HTTP/1.0 403 Forbidden');
                exit;
            }
        } else {
            //user is not authenticated
            //@todo should actually be 401, doesnt seem to work
            header('HTTP/1.0 403 Forbidden');
            exit;
        }

        return true;
    }

    #@todo, next to no security here whatsoever. sql-injection anyone?!?!?
    public function completeRequest()
    {

        //@todo this is simply for GET just now, I dont even switch between verbs just now

        /*
         * if we access $this parameters in reverse order, we can ascertain what
         * the client is asking for
        */

        //@todo, will we ever have more than 3 levels here?
        $params = array_reverse($this->parameters);

        //check to see if last param is an id, if it is then we have a primary key
        if (is_numeric($params[0])) {
            $obj = ucwords($params[1]);

            $obj = new $obj;

            //we can ignore all the rest, just traverse up one level
            $s = new Search($obj);
            $s->eq('id', $params[0]);
        } else {
            $obj1 = ucwords($params[0]);

            $obj = new $obj1;
            //we have been given a sub object, lets try to resolve

            $s = new Search($obj);
            $s->eq($params[2], $params[1]);
        }

        //@todo, just now I do not listen to the format, 'tis always json

        $response = [];

        while ($o = $s->next()) {
            #@todo implement list_row.json for all objects to be included in API
            $tmp =  new Template($o->get_template_path("list_row.json"), array("object"=>$o));

            $response[] = "$tmp";
        }

        echo json_encode($response);
    }
}

$request = new Request();
