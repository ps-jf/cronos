<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "save":
            $json = [
                "status" => false,
                "error"  => false,
                "message"  => false
            ];

            if ($_GET['is_added'] == 'true') {
                $_GET['is_added'] = 1;
            }
            if ($_GET['is_updated'] == 'true') {
                $_GET['is_updated'] = 1;
            }

            $db = new mydb();
            $db -> query("INSERT INTO ".ALERTS_TBL." (object, object_id, is_added, object_added, added_active, is_updated, update_active, addedwhen, addedby)
							VALUES ('".$_GET['object']."',".$_GET['object_index'].",'".$_GET['is_added']."','".$_GET['object_added']."','".$_GET['is_added']."','".$_GET['is_updated']."','".$_GET['is_updated']."',".time().",'".User::get_default_instance('id')."')");

            if ($db) {
                $json["status"] = true;
                $json["message"] = "Alert successfully added";
            } else {
                $json["error"] = true;
                $json["message"] = "Error occurred, Please inform IT";
            }

            echo json_encode($json);

            break;
            
        case "load_alerts":
            $db = new mydb();
                
            $db->query("SELECT * FROM ".ALERTS_TBL." WHERE addedby = ".User::get_default_instance('id')." AND is_active = ".$_GET['is_active']);
            $count = 0;
                
            if ($_GET['is_active'] == 1) {
                $btn_action = "delete";
            } else {
                $btn_action = "add";
            }
                
            while ($row = $db->next()) {
                if ($row['is_updated'] == 1) {
                    $row['is_updated'] = "Yes";
                } else if ($row['ac_applied'] == 1) {
                    $row['is_updated'] = "AC Applied";
                } else if ($row['ac_exempt'] == 1) {
                    $row['is_updated'] = "AC Exempt";
                } else {
                    $row['is_updated'] = "No";
                }

                $obj = new $row['object']($row['object_id'],true);

                if ($row['pack_produced'] == 1) {
                    $row['object_added'] = "Pack to Partner";
                } else if ($row['form_to_provider'] == 1) {
                    $row['object_added'] = "Form to Provider";
                } else if ($row['is_added'] == 0) {
                    $row['object_added'] = "--";
                }

                if ($row['object'] == "NewBusiness") {
                    $row['object'] = "New Business";
                }
                if ($row['object_added'] == "NewBusiness") {
                    $row['object_added'] = "New Business";
                }
                if ($row['id']) {
                    $count=$count+1;
                }

                echo "<tr id='".$row['id']."'>
							<td>".$row['object']."</td>
							<td>".$obj->link()."</a></td>
							<td>".$row['object_added']."</td>
							<td>".$row['is_updated']."</td>
							<td><button id='".$row['id']."' class='".$btn_action."'>&nbsp;</button></td>
						</tr>";
            }

            if ($count == 0) {
                echo "<tr colspan='5'><td>You currently have no previous alerts</td></tr>";
            }

            break;

        case "alerts":
            //Display the users custom alerts
            $user = User::get_default_instance();
            echo new Template("alerts/alerts.html", ["user"=>$user]);
            break;

        case "remove":
            $db = new mydb();
            $q = "UPDATE ".ALERTS_TBL." SET is_active=0, update_active=0, added_active=0 WHERE addedby = ".User::get_default_instance('id')." AND id = ".$_GET['id'];
            $db->query($q);
            break;

        case "re-add":
            $db = new mydb();
            $q = "UPDATE ".ALERTS_TBL." SET is_active=1, update_active=1 , addedwhen=".time()." WHERE is_updated=1 AND addedby = ".User::get_default_instance('id')." AND id = ".$_GET['id'];
            $db->query($q);
            $q = "UPDATE ".ALERTS_TBL." SET is_active=1, added_active=1, addedwhen=".time()." WHERE is_added=1 AND addedby = ".User::get_default_instance('id')." AND id = ".$_GET['id'];
            $db->query($q);
            $q = "UPDATE ".ALERTS_TBL." SET is_active=1, addedwhen=".time()." WHERE addedby = ".User::get_default_instance('id')." AND id = ".$_GET['id'];
            $db->query($q);
            break;

        case "check":

            $json = (object)[];

            $json->data = [
                "status" => false,
                "updates"  => false,
                "added"  => false
            ];

            $db = new mydb();
            $q = "SELECT * FROM ".ALERTS_TBL." WHERE addedby = ".User::get_default_instance('id')." AND update_active = '1'";
            $db->query($q);

            $alerts = [];
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $alerts[] = $row;
            }

            $objects = [];
            $object_ids = [];
            $addedwhen = [];

            foreach ($alerts as $value) {
                $objects[] = $value["object"];
                $object_ids[] = $value["object_id"];
                $addedwhen[] = $value["addedwhen"];
            }

            $i=0;
            $json->data['status'] = false;
            foreach ($objects as $object) {
                $updated_col = "updated_when";
                if ($object == "Client") {
                    $table = CLIENT_TBL;
                    $value = "clientID";
                }
                if ($object == "NewBusiness") {
                    $table = NB_TBL;
                    $value = "nbID";
                }
                if ($object == "Partner") {
                    $table = PARTNER_TBL;
                    $value = "partnerID";
                }
                if ($object == "Policy") {
                    $table = POLICY_TBL;
                    $value = "policyID";
                    $updated_col = "update_when";
                }

                $q = "SELECT * FROM ".$table." WHERE ". $value." = ".$object_ids[$i]." AND ".$updated_col." > ".$addedwhen[$i];
                $db->query($q);

                $updated = 'false';
                while ($alert = $db->next(MYSQLI_ASSOC)) {
                    $alerts .= $alert;
                    if ($object == "Client") {
                        if (isset($alert['clientID'])) {
                            $updated = 'true';
                        }
                    }
                    if ($object == "NewBusiness") {
                        if (isset($alert['nbID'])) {
                            $updated = 'true';
                        }
                    }
                    if ($object == "Partner") {
                        if (isset($alert['partnerID'])) {
                            $updated = 'true';
                        }
                    }
                    if ($object == "Policy") {
                        if (isset($alert['policyID'])) {
                            $updated = 'true';
                        }
                    }
                }
                if ($updated == 'true') {
                    $json->data['status'] = true;
                    $obj = new $object($object_ids[$i], true);
                    $json->data['updates'] .= "<br />".$object . " <b> ".$obj->link()."</b>";
                    $q = "UPDATE ".ALERTS_TBL." SET update_active=0 WHERE object_id = ".$object_ids[$i];
                    $db->query($q);
                }
                $i++;
            }

            ##END updates check
            ##BEGIN added check

            $db = new mydb();
            $q = "SELECT * FROM ".ALERTS_TBL." WHERE addedby = ".User::get_default_instance('id')." AND added_active = '1'";
            $db->query($q);

            $alerts = [];
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $alerts[] = $row;
            }

            $objects = [];
            $object_id = [];
            $addedwhen = [];
            foreach ($alerts as $value) {
                $objects[] = $value["object"];
                $object_id[] = $value["object_id"];
                $object_added[] = $value["object_added"];
                $addedwhen[] = $value["addedwhen"];
            }

            $i=0;
            foreach ($objects as $object) {
                $date2 = "";
                if ($object == "Policy") {
                    $table2 = "tblpolicy";
                    $t2column = "policyID";
                    $t1column = "policyID";
                }
                if ($object == "Client") {
                    $table2 = "tblclient";
                    $t2column = "clientID";
                }

                if ($object_added[$i] == "Commission") {
                    $table1 = "tblcommission";
                    $added_col = "editedWhen";
                    $date2 = " OR t1.".$added_col." > '". date("Y-m-d H:i:s", $addedwhen[$i])."'";
                }
                if ($object_added[$i] == "Correspondence") {
                    $table1 = "tblcorrespondencesql";
                    $t1column = "policy";
                    $added_col = "dateStamp";
                    $addedwhen[$i] = date("Y-m-d H:i:s", $addedwhen[$i]);
                }
                if ($object_added[$i] == "NewBusiness") {
                    $table1 = "tblnewbusiness";
                    $added_col = "addedWhen";
                }
                if ($object_added[$i] == "Policy") {
                    $table1 = "tblpolicy";
                    $added_col = "addedWhen";
                }

                if ($object == "Client" && $object_added[$i] == "Correspondence") {
                    $t1column = "client";
                }
                if ($object == "Client" && $object_added[$i] == "Policy") {
                    $t1column = "clientID";
                }

                if ($object == "Client" && ($object_added[$i] == "NewBusiness" || $object_added[$i] == "Commission")) {
                    $q = "SELECT * FROM ".$table1." t1 INNER JOIN tblpolicy t3 ON t3.policyID = t1.policyID INNER JOIN tblclient t2 ON t2.clientID = t3.clientID WHERE t2.clientID = ".$object_id[$i]." AND (t1.".$added_col." > '".$addedwhen[$i]."' ".$date2.")";
                } else {
                    $q = "SELECT * FROM ".$table1." t1 INNER JOIN ".$table2." t2 ON t1.".$t1column." = t2.".$t2column." WHERE t2.".$t2column." = ".$object_id[$i]." AND (t1.".$added_col." > '".$addedwhen[$i]."' ".$date2.")";
                }

                $db->query($q);

                $added = 'false';
                while ($alert = $db->next(MYSQLI_ASSOC)) {
                    $alerts .= $alert;
                    if ($object == "Client") {
                        if (isset($alert['clientID'])) {
                            $added = 'true';
                        }
                    }
                    if ($object == "Policy") {
                        if (isset($alert['policyID'])) {
                            $added = 'true';
                        }
                    }
                }
                if ($added == 'true') {
                    $json->data['status'] = true;
                    $obj = new $object($object_id[$i], true);
                    $json->data['added'] .= "<b>".$object_added[$i]."</b> has been added to <b> ".$obj->link()."</b><br />";
                    $q = "UPDATE ".ALERTS_TBL." SET added_active=0 WHERE object_id = ".$object_id[$i];
                    $db->query($q);
                }
                $i++;
            }

            echo json_encode($json);

            // if user has received all notifications on an object, archive alert
            $q = "UPDATE prophet.alerts
                    SET is_active = 0
                    WHERE addedby = 117
                    AND update_active = 0
                    AND added_active = 0
                    AND (ac_applied != 1 AND ac_exempt != 1 AND pack_produced != 1 AND form_to_provider != 1)";
            $db->query($q);

            break;

        case "ac_save":
            $json = [
                "status" => false,
                "error"  => false,
                "message"  => false
            ];


            if ($_GET['pack_produced'] == 'true') {
                $_GET['pack_produced'] = 1;
            }
            if ($_GET['form_to_provider'] == 'true') {
                $_GET['form_to_provider'] = 1;
            }
            if ($_GET['ac_applied'] == 'true') {
                $_GET['ac_applied'] = 1;
            }
            if ($_GET['ac_exempt'] == 'true') {
                $_GET['ac_exempt'] = 1;
            }



            foreach ($_GET as $key => $value) {
                if ($key == "pack_produced" || $key == "form_to_provider" || $key == "ac_applied"
                || $key == "ac_exempt" || $key == "ac_ongoing") {
                    if ($value == 1) {
                        if ($key == "pack_produced") {
                            $set_values = $_GET['pack_produced'].",0,0,0";
                        } else if ($key == "form_to_provider") {
                            $set_values = "0,".$_GET['form_to_provider'].",0,0";
                        } else if ($key == "ac_applied") {
                            $set_values = "0,0,".$_GET['ac_applied'].",0";
                        } else if ($key == "ac_exempt") {
                            $set_values = "0,0,0,".$_GET['ac_exempt'];
                        } else {
                            $set_values = "0,0,0,0";
                        }

                        $db = new mydb();
                        $db -> query("INSERT INTO ".ALERTS_TBL." (object, object_id, is_added, object_added,
                                                        added_active, is_updated, update_active, addedwhen, addedby,
                                                        pack_produced, form_to_provider, ac_applied, ac_exempt)
							VALUES ('".$_GET['object']."',
                                    ".$_GET['object_index'].",
                                    '0', '', '0', '0', '0',
							        ".time().",
							        '".User::get_default_instance('id')."', $set_values )");
                    }
                }
            }


            if ($db) {
                $json["status"] = true;
                $json["message"] = "Alert(s) successfully added";
            } else {
                $json["error"] = true;
                $json["message"] = "Error occurred, Please inform IT";
            }

            echo json_encode($json);

            break;

        case "check_ac_applied":
            $json = [
                "status"=> false,
                "updates"   => null
            ];

            $updates_to_show = "";

            //current timestamp minus fifteen minutes
            $time = time()-900;

            //get all active ac applied  / exempt alerts
            $db = new mydb();
            $q = "SELECT * FROM ".ALERTS_TBL." WHERE addedby = ".User::get_default_instance('id')."
                      AND is_active = 1 AND added_active = 0 AND update_active = 0 AND ac_applied = 1";
            $db->query($q);

            //add all active alerts to an array
            $alerts = [];
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $alerts[] = $row;
            }

            //initialize arrays
            $objects = [];
            $object_ids = [];
            $addedwhen = [];

            //add values from alerts array into separate arrays
            foreach ($alerts as $value) {
                $objects[] = $value["object"];
                $object_ids[] = $value["object_id"];
                $addedwhen[] = $value["addedwhen"];
            }

            $i=0;

            //for each alert entry..
            foreach ($objects as $object) {
            //get all policyID's (subject to main ac criteria) under partner $i
                $db = new mydb();
                $q = "select policyID from tblpolicy
                          inner join tblclient
                          on tblpolicy.clientID = tblclient.clientID
                          inner join policy_gri
                          on tblpolicy.policyID = policy_gri.policy_id
                          where partnerID = ".$object_ids[$i]." and status = 2 and issuerID in (1152, 86, 1216)
                          and renewal_12 IS NOT NULL
                          AND update_when >= ".$time;
                $db->query($q);

            //add to policy array
                $policy_array = [];
                while ($policies = $db->next(MYSQLI_ASSOC)) {
                    $policy_array[] = $policies['policyID'];
                }

                foreach ($policy_array as $pol) {
                    $db2 = new mydb();
                    //check if any log actions for partners policies within the last 15 minutes
                    $q2 = "SELECT snapshot FROM prophet.".ACTIONS_TBL."
                                WHERE object = 'Policy'
                                AND type = 'Update'
                                AND object_id = ".$pol."
                                AND datetime >= ".$time."
                                ORDER BY id DESC";


                    $db2->query($q2);

                    if ($policy_update = $db2->next(MYSQLI_ASSOC)) {
                        //explode json string into 'bits'
                        //$bits = explode("\"", stristr($policy_update['snapshot'], 's:10:"ac_applied";b:'));

                        $snapshot = json_decode($policy_update->snapshot());
                        $ac_applied_prev = $snapshot->ac_applied;

                        //$ac_applied_prev = $bits[2];
                        //$ac_applied_prev = str_replace(";b:", "", $ac_applied_prev);
                        //$ac_applied_prev = str_replace(";s:8:", "", $ac_applied_prev);

                        $q3 = "SELECT ac_applied, policyNum, policyID, clientID FROM tblpolicy where policyID = ".$pol;
                        $db2->query($q3);

                        while ($policy = $db2->next(MYSQLI_ASSOC)) {
                            // check if ac applied matches up with current value, if not alert is needed
                            if ($policy['ac_applied'] != $ac_applied_prev) {
                                $p = new Policy($policy['policyID'], true);

                                $updates_to_show .= "AC Applied has been updated for policy number ".$p->link().", Client ".$p->client->link()."<br /><br />";
                                $json['status'] = true;
                            }
                        }
                    }
                }
                $i++;
            }

            $json['updates'] = $updates_to_show;

            echo json_encode($json);

            break;

        case "check_ac_exempt":
            $json = [
                "status"=> false,
                "updates"   => null
            ];

            $updates_to_show = "";

            //current timestamp minus fifteen minutes
            $time = time()-900;

            //get all active ac applied  / exempt alerts
            $db = new mydb();
            $q = "SELECT * FROM ".ALERTS_TBL." WHERE addedby = ".User::get_default_instance('id')."
                      AND is_active = 1 AND added_active = 0 AND update_active = 0 AND ac_exempt = 1";
            $db->query($q);

            //add all active alerts to an array
            $alerts = [];
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $alerts[] = $row;
            }

            //initialize arrays
            $objects = [];
            $object_ids = [];
            $addedwhen = [];

            //add values from alerts array into separate arrays
            foreach ($alerts as $value) {
                $objects[] = $value["object"];
                $object_ids[] = $value["object_id"];
                $addedwhen[] = $value["addedwhen"];
            }

            $i=0;

            //for each alert entry..
            foreach ($objects as $object) {
            //get all policyID's (subject to main ac criteria) under partner $i
                $db = new mydb();
                $q = "select policyID from tblpolicy
                          inner join tblclient
                          on tblpolicy.clientID = tblclient.clientID
                          inner join policy_gri
                          on tblpolicy.policyID = policy_gri.policy_id
                          where partnerID = ".$object_ids[$i]." and status = 2 and issuerID in (1152, 86, 1216)
                          and renewal_12 IS NOT NULL
                          AND update_when >= ".$time;
                $db->query($q);

                //add to policy array
                $policy_array = [];
                while ($policies = $db->next(MYSQLI_ASSOC)) {
                    $policy_array[] = $policies['policyID'];
                }

                foreach ($policy_array as $pol) {
                    $db2 = new mydb();
                    //check if any log actions for partners policies within the last 15 minutes
                    $q2 = "SELECT snapshot FROM prophet.".ACTIONS_TBL."
                                WHERE object = 'Policy'
                                AND type = 'Update'
                                AND object_id = ".$pol."
                                AND datetime >= ".$time."
                                ORDER BY id DESC";


                    $db2->query($q2);

                    if ($policy_update = $db2->next(MYSQLI_ASSOC)) {
                        //explode json string into 'bits'
                        //$bits = explode("\"", stristr($policy_update['snapshot'], 's:9:"ac_exempt";b:'));
                        //$ac_exempt_prev = $bits[8];
                        //$ac_exempt_prev = str_replace(";b:", "", $ac_exempt_prev);
                        //$ac_exempt_prev = str_replace(";s:5:", "", $ac_exempt_prev);

                        $snapshot = json_decode($policy_update->snapshot());
                        $ac_exempt_prev = $snapshot->ac_exempt;

                        $q3 = "SELECT ac_exempt, policyNum, policyID, clientID FROM tblpolicy where policyID = ".$pol;
                        $db2->query($q3);

                        while ($policy = $db2->next(MYSQLI_ASSOC)) {
                            // check if ac exempt matches up with current value, if not alert is needed
                            if ($policy['ac_exempt'] != $ac_exempt_prev) {
                                $p = new Policy($policy['policyID'], true);

                                $updates_to_show .= "AC Exempt has been updated for policy number ".$p->link().", Client ".$p->client->link()."<br /><br />";
                                $json['status'] = true;
                            }
                        }
                    }
                }
                $i++;
            }

            $json['updates'] = $updates_to_show;

            echo json_encode($json);

            break;

        case "check_ac_pack_produced":
            $json = [
                "status"=> false,
                "added" => null
            ];

            $updates_to_show = "";

            //current timestamp minus fifteen minutes
            $time = time()-900;
            $time = date("Y-m-d H:i:s", $time);

            //get all active ac correspondence alerts
            $db = new mydb();
            $q = "SELECT * FROM ".ALERTS_TBL." WHERE addedby = ".User::get_default_instance('id')."
                      AND is_active = 1 AND added_active = 0 AND update_active = 0 AND pack_produced = 1";
            $db->query($q);

            //add all active alerts to an array
            $alerts = [];
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $alerts[] = $row;
            }

            //initialize arrays
            $objects = [];
            $object_ids = [];
            $addedwhen = [];

            //add values from alerts array into separate arrays
            foreach ($alerts as $value) {
                $objects[] = $value["object"];
                $object_ids[] = $value["object_id"];
                $addedwhen[] = $value["addedwhen"];
            }

            $i=0;

            //for each alert entry..
            foreach ($objects as $object) {
            //get all policyID's (subject to main ac criteria) under partner $i
                $db = new mydb();
                $q = "select CorrID, Policy, Client, Partner, Subject from tblcorrespondencesql
                        inner join tblpolicy
                        on tblcorrespondencesql.Policy = tblpolicy.policyID
                        inner join policy_gri
                        on tblpolicy.policyID = policy_gri.policy_id
                        where Partner = ".$object_ids[$i]."
                        and subject = 'Adviser Pack Sent to Partner'
                        and status = 2 and issuerID in (1152, 86, 1216)
                        and renewal_12 IS NOT NULL
                        and dateStamp >= '".$time."'
                        order by CorrID desc";
                $db->query($q);

            //add to policy array
                $corr_array = [];
                while ($correspondence = $db->next(MYSQLI_ASSOC)) {
                    $corr_array[] = $correspondence;
                }

                foreach ($corr_array as $key => $value) {
                    $policy = new Policy($value['Policy'], true);

                    // check if pack produced
                    if ($value['Subject'] == "Adviser Pack Sent to Partner") {
                        $updates_to_show .= "Adviser Pack Sent to Partner for policy number: ".$policy->link().", Client: ".$policy->client->link()."<br /><br />";
                        $json['status'] = true;
                    }
                }
                $i++;
            }

            $json['updates'] = $updates_to_show;

            echo json_encode($json);

            break;

        case "check_ac_form_to_provider":
            $json = [
                "status"=> false,
                "added" => null
            ];

            $updates_to_show = "";

            //current timestamp minus fifteen minutes
            $time = time()-900;
            $time = date("Y-m-d H:i:s", $time);

            //get all active ac correspondence alerts
            $db = new mydb();
            $q = "SELECT * FROM ".ALERTS_TBL." WHERE addedby = ".User::get_default_instance('id')."
                      AND is_active = 1 AND added_active = 0 AND update_active = 0 AND form_to_provider = 1";
            $db->query($q);

            //add all active alerts to an array
            $alerts = [];
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $alerts[] = $row;
            }

            //initialize arrays
            $objects = [];
            $object_ids = [];
            $addedwhen = [];

            //add values from alerts array into separate arrays
            foreach ($alerts as $value) {
                $objects[] = $value["object"];
                $object_ids[] = $value["object_id"];
                $addedwhen[] = $value["addedwhen"];
            }

            $i=0;

            //for each alert entry..
            foreach ($objects as $object) {
            //get all policyID's (subject to main ac criteria) under partner $i
                $db = new mydb();
                $q = "select CorrID, Policy, Client, Partner, Subject from tblcorrespondencesql
                        inner join tblpolicy
                        on tblcorrespondencesql.Policy = tblpolicy.policyID
                        inner join policy_gri
                        on tblpolicy.policyID = policy_gri.policy_id
                        where Partner = ".$object_ids[$i]."
                        and subject = 'Adviser Charging Form sent to Provider'
                        and status = 2 and issuerID in (1152, 86, 1216)
                        and renewal_12 IS NOT NULL
                        and dateStamp >= '".$time."'
                        order by CorrID desc";
                $db->query($q);

            //add to policy array
                $corr_array = [];
                while ($correspondence = $db->next(MYSQLI_ASSOC)) {
                    $corr_array[] = $correspondence;
                }

                foreach ($corr_array as $key => $value) {
                    $policy = new Policy($value['Policy'], true);

                    // check if pack produced
                    if ($value['Subject'] == "Adviser Charging Form sent to Provider") {
                        $updates_to_show .= "Adviser Charging Form sent to Provider for policy number: ".$policy->link().", Client: ".$policy->client->link()."<br /><br />";
                        $json['status'] = true;
                    }
                }
                $i++;
            }

            $json['updates'] = $updates_to_show;

            echo json_encode($json);

            break;
    }
} else {
    echo new Template("alerts/profile.html", ["object_index"=>$_GET['object_index'],"object"=>$_GET['object']]);
}
