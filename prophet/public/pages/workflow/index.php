<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$whitelist = [
    '127.0.0.1',
    '127.0.0.1:8080',
    '127.0.0.1:8088',
    '::1',
    'localhost:8080',
    'localhost:8088',
    '10.0.2.2'
];

if (isset($_GET["id"])) {

    $wf = new Workflow($_GET["id"], true);

    if (isset($_GET["do"])) {

        switch ($_GET["do"]) {

            case "wf_date_change":
                $json = [
                    'feedback' => '',
                    'error' => false
                ];

                try{
                    /**
                     * case for auditing a due date change for a portfolio report
                     * partner will be emailed with reason, old/new date, workflow ID
                     * user informed with feedback regarding save status and if partner emailed.
                     */

                    $wf->due($_GET['full_due']);
                    $wf->save();

                    if (isset($_GET['reason']) && $_GET['reason'] != "") {
                        if($wf->date_change_reason($_GET['due'], $_GET['reason'])){
                            $json['feedback'] = "Reason successfully logged, partner emailed";
                        }
                    } else {
                        $json['feedback'] = "Email not sent to Partner";
                        $json['error'] = false;
                    }
                } catch(Exception $e) {
                    $json['error'] = true;
                    $json['feedback'] = $e->getMessage();
                }

                echo json_encode($json);

                break;

            case "action_wf":
                if (isset($_GET['bulk'])) {
                    //create array from string of ids
                    $wf_id_array = explode(',', $_GET['id']);

                    //for each item in array get workflow object and do updates
                    foreach ($wf_id_array as $item) {
                        if (!empty($_POST['workflow']['due']["Y"])){
                            //create date string
                            $date = $_POST['workflow']["due"]["Y"] . "-" . $_POST['workflow']["due"]["M"] . "-" . $_POST['workflow']["due"]["D"] . " " . $_POST['workflow']["due"]["h"] . ":" . $_POST['workflow']["due"]["m"];
                            //convert date string to unix timestamp
                            $wf_bulk_update = new Workflow($item, true);

                            if ($date != "-- :") {
                                if (substr($date, strlen($date) - 1, 1) == ":"){
                                    $date = str_replace(":", "", $date);
                                }

                                if(in_array($wf_bulk_update->step(), [41,193])){
                                    // portfolio report workflow
                                    if(!empty($_POST['date_change_reason'])){
                                        $wf_bulk_update->date_change_reason($date, $_POST['date_change_reason']);
                                    }
                                }

                                $date = new DateTime($date);
                                $due_date = $date->format("U");

                                $wf_bulk_update->priority($_POST['workflow']['priority']);
                                $wf_bulk_update->due($due_date);
                                $wf_bulk_update->desc($wf_bulk_update->desc() . "\n\n" . $_POST['workflow']['desc']);

                                if ($wf_bulk_update->save()) {
                                    if ($_GET['action'] != "update") {
                                        $wf_bulk_update->workflow_next();
                                    }
                                }
                            } else {
                                $wf_bulk_update->workflow_next();
                            }
                        }

                        if (!empty($_POST['assigned'])){
                            // delete any existing assigned entries
                            $s = new Search(new WorkflowAssigned());
                            $s->eq("workflow_id", $item);

                            while($wfa = $s->next(MYSQLI_ASSOC)){
                                $wfa->delete();
                            }

                            foreach($_POST['assigned'] as $assigned){
                                $wa = new WorkflowAssigned();

                                $wa->workflow_id($item);
                                $wa->user_id($assigned);
                                $wa->is_global(0);

                                $wa->save();
                            }
                        }
                    }
                    echo json_encode(true);
                } else {
                    //move single workflow ticket on to next step
                    $wf->load($_POST["workflow"]);

                    if ($wf->save()) {
                        echo json_encode($wf->workflow_next());
                    } else {
                        echo json_encode(false);
                    }
                }

                break;

            case "step_builder":

                $json = (object)[
                    "id" => null,
                    "status" => false,
                    "error" => null,
                    "data" => null
                ];

                $db = new mydb();

                if (isset($_GET['current_step']) && !empty($_GET['current_step'])){
                    //get the process id of the current list
                    $q = "select * from feebase.workflow_step where  stepID = " . $_GET['current_step'];
                    $db->query($q);
                    if ($getSteps = $db->next(MYSQLI_ASSOC)) {
                        //get the steps list
                        $q = "select * from feebase.workflow_step where  processID = " . $getSteps['processID'] . " and active = 1";
                        $db->query($q);

                        $i = 1;
                        while ($figs = $db->next(MYSQLI_ASSOC)) {

                            if ($figs['stepID'] == $_GET['current_step']) {
                                $json->data .= ' 
                              <div class="stepwizard-step  step-' . $i . '">
                            <button href="" type="button" class="btn btn-primary btn-circle" disabled="disabled" data-toggle="tooltip" data-placement="top" title="' . $figs['stepDesc'] . '">' . $i . '</a>
                        </div>';
                            } else {
                                $json->data .= '   
                            <div class="stepwizard-step  step-' . $i . '">
                            <button href="" type="button" class="btn btn-circle" disabled="disabled" data-toggle="tooltip" data-placement="top" title="' . $figs['stepDesc'] . '">' . $i . '</a>
                        </div>';
                            }


                            $i++;
                        }

                        echo json_encode($json);

                    }
                }

                break;

            case "rollback_wf":
                $json = (object)[
                    "id" => null,
                    "status" => false,
                    "error" => null
                ];

                if (isset($_GET['bulk'])) {
                    //create array from string of ids
                    $wf_id_array = explode(',', $_GET['id']);

                    //for each item in array, preform rollback
                    foreach ($wf_id_array as $item) {
                        $wf_bulk_update = new Workflow($item, true);

                        $rollback = Workflow::workflow_rollback($item);

                        $json->status = $rollback->status;
                        $json->error = $rollback->error;
                    }
                } else {
                    $id = $_GET["id"];
                    $rollback = Workflow::workflow_rollback($id);

                    $json->status = $rollback->status;
                    $json->error = $rollback->error;
                }

                echo json_encode($json);
                break;

            case "earlyend_wf":
                echo new Template ("workflow/confirm_end.html", ["workflow" => $_GET['id']]);
                break;

            case "reopen_wf":

                $json = (object)[
                    "id" => null,
                    "clickme" => null,
                    "newid" => null,
                    "status" => false,
                    "error" => null
                ];

                // Get the last workflow audit entry for workflow id
                $db = new mydb();
                $q = "select * from feebase.workflow_audit  where workID = " . $_GET['id'] . " order by ID desc limit 1 ";
                $db->query($q);

                if ($wfa = $db->next(MYSQLI_ASSOC)) {
                    $auditid = $wfa['ID'];
                }

                // Get workflow audit object and then remove
                $wfaudit = new WorkflowAudit($auditid, true);

                $wf = new Workflow;
                $array = [
                    'step' => $wfaudit->step(),
                    'priority' => $wfaudit->priority(),
                    'due' => $wfaudit->due(),
                    'desc' => $wfaudit->desc(),
                    '_object' => $wfaudit->_object(),
                    'index' => $wfaudit->index()
                ];

                $array2 = [
                    'id' => $wfaudit->completedby()
                ];

                ob_start();
                if (in_array($wfaudit->step(), [41,193])) {
                    // portfolio workflow should be assigned to all staff
                    $wf->workflow_add($array, false, false, true);
                } else {
                    // re-assigned to person who closed ticket
                    $wf->workflow_add($array, $array2, false);
                }

                $wfid = $wf->id();
                ob_end_clean();

                $json->status = 'Workflow ticket re-opened';
                $json->newid = $wfid;
                $json->error = false;

                // Update the old workflow notes to the new workflow ticket
                $q_1 = "UPDATE feebase.notes SET object = 'Workflow', object_id =" . $wfid . " WHERE object = 'WorkflowAudit' AND object_id =" . $auditid;

                if ($db->query($q_1)) {
                    $json->error = false;
                } else {
                    $json->status = 'Workflow notes not imported';
                    $json->error = true;
                }

                $q_4 = "UPDATE feebase.notes SET object_id =" . $wfid . " WHERE object = 'Workflow' AND object_id =" . $_GET['id'];
                if ($db->query($q_4)) {
                    $json->error = false;
                } else {
                    $json->status = 'Workflow notes not imported';
                    $json->error = true;
                }

                if (in_array($wfaudit->step(), [41,193])) {
                    // keep all portfolio report behaviour intact
                    $db = new mydb();
                    $q_2 = "UPDATE feebase.proreport_invoicing SET workflow_id = " . $wfid . " WHERE workflow_id =" . $_GET['id'];

                    if ($db->query($q_2)) {
                        $json->error = false;
                    } else {
                        $json->status = 'Portfolio report invoicing workflow not updated';
                        $json->error = true;
                    }

                    // establish remote db conn
                    $remote_db = new mydb(REMOTE, E_USER_WARNING);
                    if (mysqli_connect_errno()) {
                        echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
                        exit();
                    } else {
                        $q_3 = "UPDATE myps.valuation_requests SET workflow_id = " . $wfid . " WHERE workflow_id =" . $_GET['id'];
                        if ($remote_db->query($q_3)) {
                            $json->error = false;
                        } else {
                            $json->status = 'Portfolio report request workflow not updated';
                            $json->error = true;
                        }
                    }
                }

                echo json_encode($json);

                break;

            case "end_ticket":
                $json = [];

                if (isset($_GET['bulk']) && $_GET['bulk'] == "action") {
                    //create array from string of ids
                    $wf_id_array = explode(',', $_GET['id']);

                    //for each item in array, audit wf
                    foreach ($wf_id_array as $item) {
                        $w = new Workflow($item, true);

                        try{
                            $w->end_ticket($_GET['end_reason']);

                            $json['status'] = true;
                            $json['error'] = false;
                        } catch (Exception $e) {
                            $json['status'] = false;
                            $json['error'] = $e->getMessage();
                        }
                    }
                } else {
                    $w = new Workflow($_GET['id'], true);

                    try{
                        $w->end_ticket($_GET['end_reason']);

                        $json['status'] = true;
                        $json['error'] = false;
                    } catch (Exception $e) {
                        $json['status'] = false;
                        $json['error'] = $e->getMessage();
                    }
                }

                echo json_encode($json);

                break;

            case "bulk_update_template":
                //get new workflow object for printing form elements on template
                $wf_obj = new Workflow();

                //make list of ids more readable for use on template
                $ids = str_replace("checkall,", "", $_GET['id']);
                $ids = str_replace(",", ", ", $ids);

                //get id of first ticket, use this to check if step has previous/next/end actions
                $id = reset(explode(',', $ids));
                $wf = new Workflow($id, true);

                echo new Template ("workflow/bulk_update.html", ["ids" => $ids, "wf_obj" => $wf_obj, "wf" => $wf]);
                break;

            case "wf_progress_check":
                $wfid = $_GET['id'];
                $userID = $_GET['userID'];

                $db = new mydb();

                $db->query("select * from " . WFPROGRESS_TBL . " where workflowID = " . $wfid);

                if ($d = $db->next(MYSQLI_ASSOC)) {
                    # Search the user object for the returned user profile
                    $m = new Search(new User());
                    $m->eq('id', $d['userID']);
                    $m->set_limit(1);
                    $match = $m->next();
                    if ($match) {
                        $username = $match->link();
                        $json["username"] = $username;
                    }

                    if ($userID == $d['userID']) {
                        $json["sameuser"] = 1;
                    }

                    $json["progress"] = 1;
                    $json["status"] = "found";
                } else {
                    $json["status"] = "not found";
                }
                echo json_encode($json);
                break;

            case "wf_progress_click":
                $wfid = $_GET['id'];
                $userID = $_GET['userID'];

                $db = new mydb();

                $db->query("select * from " . WFPROGRESS_TBL . " where workflowID = " . $wfid);

                if ($d = $db->next(MYSQLI_ASSOC)) {
                    $db->query("delete from " . WFPROGRESS_TBL . " where workflowID = " . $wfid . " and userID = " . $userID);

                    $m = new Search(new User());
                    $m->eq('id', $userID);
                    $m->set_limit(1);
                    $match = $m->next();
                    if ($match) {
                        $username = $match->link();
                        $json["username"] = $username;
                    }
                    $json["status"] = "found";
                } else {
                    $db->query("insert into " . WFPROGRESS_TBL . " set workflowID = " . $wfid . ", userID = " . $userID);

                    $m = new Search(new User());
                    $m->eq('id', $userID);
                    $m->set_limit(1);
                    $match = $m->next();
                    if ($match) {
                        $username = $match->link();
                        $json["username"] = $username;
                    }
                    $json["status"] = "inserted";
                }

                echo json_encode($json);
                break;

            case "add-assignee":
                $assignees = $_GET['assignees'];

                $response = [
                    "success" => true,
                    "error" => false
                ];

                $db = new mydb();

                $query = "UPDATE " . DATA_DB . ".workflow_assigned SET is_global = 0 WHERE workflow_id = " . $_GET['id'];

                $db->query($query);

                foreach ($assignees as $assignee) {
                    $wa = new WorkflowAssigned();

                    $wa->user_id->set($assignee);
                    $wa->workflow_id->set($_GET['id']);
                    $wa->is_global->set(0);

                    if ($wa->save()) {
                        $query = "DELETE FROM " . DATA_DB . ".workflow_assigned WHERE workflow_id = " . $_GET['id'] . " AND group_id IS NOT NULL";

                        $db->query($query);
                    } else {
                        $response["success"] = false;
                        $response["error"] = "Failed to add user " . $assignee . " to workflow_assigned table";
                    }
                }

                echo json_encode($response);

                break;

            case "remove-assignee":
                $db = new mydb();
                $query = "DELETE FROM " . DATA_DB . ".workflow_assigned WHERE workflow_id = " . $_GET['id'];
                if ($db->query($query)) {
                    $assignTo = [
                        36 => "all"
                    ];

                    $wa = new WorkflowAssigned();
                    $wa->workflow_id->set($_GET['id']);

                    $assigned_to = null;

                    switch($assignTo[$wf->step->process()]){
                        case "all":
                            $wa->is_global->set(1);

                            $assigned_to = "all";

                            break;

                        case "group":
                        default:
                            $wa->group_id->set(User::get_default_instance('department'));

                            $wa->is_global->set(0);

                            $assigned_to = "group";

                            break;
                    }

                    if ($wa->save()) {
                        $response["success"] = true;
                        $response["feedback"] = "Workflow assigned to " . $assigned_to;
                    } else {
                        $response["success"] = false;
                        $response["feedback"] = "Failed to assign to " . $assigned_to . ", contact IT and quote WF ID ".$_GET['id'];
                    }
                } else {
                    $response["success"] = false;
                    $response["feedback"] = "Failed to unassign, contact IT and quote WF ID ".$_GET['id'];
                }

                echo json_encode($response);
                break;
        }
    }

    if (isset ($_GET["get"])) {
        switch ($_GET["get"]) {
            case "profile":
                # @TODO what the hell? Possible multiple inheritance issue
                $workflow = new Workflow();
                $workflow_audit = new WorkflowAudit($_GET["id"], true);

                echo new Template ("workflow/profile_audit.html", ["workflow_audit" => $workflow_audit]);

                break;
        }
    }
} elseif (isset($_GET["get"])) {
    $db = new mydb();

    //for pretty printing
    function det_class($pt)
    {
        $rt = time();

        if ($pt > $rt) {
            if ($pt < $rt + 86400) {
                $class = "amber";
            } else {
                $class = "green";
            }
        } else {
            $class = "red";
        }
        return $class;
    }

    switch ($_GET["get"]) {
        case "figures":
            $w = new Workflow;
            $wa = new WorkflowAssigned;
            $ug = new UsersGroups;

            $q = "SELECT COUNT(*) as `total`, " . // total
                "SUM(CASE WHEN " . $w->due->get_field_name() . " < UNIX_TIMESTAMP() THEN 1 ELSE 0 END) as `late`, " .
                "SUM(CASE WHEN " . $w->due->get_field_name() . " > UNIX_TIMESTAMP() " .
                "  AND " . $w->due->get_field_name() . " < (UNIX_TIMESTAMP()+900) THEN 1 ELSE 0 END) as `due_soon`, " .
                "SUM(CASE WHEN " . $w->due->get_field_name() . " > (UNIX_TIMESTAMP()+900) THEN 1 ELSE 0 END) as `due_later` " .
                "FROM " . $wa->get_table() . " " .
                "INNER JOIN " . $w->get_table() . " ON workflow_id=" . $w->id->get_field_name() . " " .
                "WHERE " . $wa->group_id->get_field_name() . " IN (" .
                "  SELECT " . $ug->user_group->get_field_name() . " " .
                "  FROM " . $ug->get_table() . " " .
                "  WHERE " . $ug->user->get_field_name() . " = " . User::get_default_instance("id") .
                ") " .
                "OR " . $wa->user_id->get_field_name() . " = " . User::get_default_instance("id");

            $db->query($q);

            if ($figs = $db->next(MYSQLI_ASSOC)) {
                echo json_encode($figs);
            }

            break;

        case "list":
            $json = (object)[
                "search" => null,
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $limit = 50;
            if (!isset($_GET['offset'])) {
                $offset = 50;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            $w = new Workflow;
            $wa = new WorkflowAssigned;
            $ug = new UsersGroups;

            $s = new Search(new User());

            $s->eq("department", User::get_default_instance("department"));

            $teamids = [];

            while($u = $s->next(MYSQLI_ASSOC)){
                $teamids[] = $u->id();
            }

            $my_team = implode(",", $teamids);

            $q_mine = "SELECT workflow_step.processID, step,priority,object,object_index,due,'Mine' as `group`,workflow_id FROM " . $wa->get_table() .
                " INNER JOIN " . $w->get_table() . " ON workflow_id=" . $w->id->get_field_name() . " 
				INNER JOIN workflow_step 
			ON " . WF_TBL . ".step = workflow_step.stepID
             WHERE " . $wa->user_id->get_field_name() . "  = " . User::get_default_instance("id");

            $q_group = "SELECT workflow_step.processID, step,priority,object,object_index,due,groups.name as `group`,workflow_id FROM " . $wa->get_table() .
                " INNER JOIN " . $w->get_table() . " ON workflow_id=" . $w->id->get_field_name() . " " .
                " INNER JOIN prophet.groups ON prophet.groups.id=" . $wa->group_id->get_field_name() . " 
			INNER JOIN workflow_step 
					ON " . WF_TBL . ".step = workflow_step.stepID 
             WHERE " . $wa->group_id->get_field_name() . " IN (" .
                "  SELECT " . $ug->user_group->get_field_name() . " " .
                "  FROM " . $ug->get_table() . " " .
                "  WHERE " . $ug->user->get_field_name() . " = " . User::get_default_instance("id") .
                ")";

            $q_staff = "SELECT workflow_step.processID, step,priority,object,object_index,due,'My Team' as `group`,workflow_id FROM " . $wa->get_table() .
                " INNER JOIN " . $w->get_table() . " ON workflow_id=" . $w->id->get_field_name() . " 
				INNER JOIN workflow_step 
			    ON " . WF_TBL . ".step = workflow_step.stepID
                WHERE " . $wa->user_id->get_field_name() . "  in (" . $my_team . ")";

            $q_all = "SELECT workflow_step.processID, step,priority,object,object_index,due,'All PS staff' as 'group',workflow_id
					FROM " . WFASS_TBL . "
					INNER JOIN " . WF_TBL . " 
					  ON workflow_id = workflow.id
					  INNER JOIN workflow_step 
					ON " . WF_TBL . ".step = workflow_step.stepID 
					  WHERE " . WFASS_TBL . ".is_global  = 1";


            if (isset($_GET['whose'])) {
                $whose = $_GET['whose'];
            } else {
                $whose = "all";
            }

            switch ($whose) {
                case "usr":
                    $q = $q_mine;
                    break;

                case "grp":
                    $q = $q_group;
                    break;

                case "my_team":
                    $q = $q_staff;
                    break;

                case "all":
                    $q = $q_mine . " UNION " . $q_group . " UNION " . $q_all;
                    break;
            }

            $q .= " ORDER BY due asc";


            $q1 = "SELECT FOUND_ROWS();";
            $db->query($q);
            $db->query($q1);


            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages) {
                $i++;
                $page_no[] = $i;
            }

            if (count($page_no) > ceil($pages)) {
                array_pop($page_no);
            }


            if (isset($_GET['offset']) && $_GET['offset'] != "all") {
                $q .= " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $db->query($q);
            $return = "";
            while ($d = $db->next(MYSQLI_ASSOC)) {
                $partner = "";

                $wf = new Workflow($d["workflow_id"], true);
                if (strtolower($d["object"]) == "none") {
                    $row = "";
                } else {
                    $obj = (String)$wf->_object;
                    $m = new Search(new $obj);
                    $m->eq('id', $d["object_index"]);
                    $m->set_limit(1);
                    $match = $m->next();
                    if ($match) {
                        $row = $match->link();
                    } else {
                        $row = "error";
                    }
                }

                if ($d["object"] == "Client") {
                    $c = new Client($d["object_index"], true);
                    $partner = $c->partner->link();
                }

                if ($d["object"] == "Policy") {
                    $p = new Policy($d["object_index"], true);
                    $partner = $p->issuer->link();
                }

                #check for entry in workflow_progress
                $db1 = new mydb();

                $db1->query("select * from " . WFPROGRESS_TBL . " join prophet.users on userID = users.id where workflowID = " . $wf->id);
                $progress = "";
                if ($ed = $db1->next(MYSQLI_ASSOC)) {
                    if ($ed['userID'] == User::get_default_instance("id")) {
                        $progress = " - <img title='In Progress by Me' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user.png' />";
                    } else {
                        $progressUser = new User($ed['userID'], true);
                        $progress = " - <img title='In Progress by " . $progressUser->staff_name() . "' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user2.png' />";
                    }
                }

                $return .= "<tr>
                    <td><input id='" . $wf->id . "' class='" . $wf->step->desc . "' type='checkbox'></td>
                    <td>" . $wf->link($wf->id) . $progress . "</td>
                    <td>" . $d["group"] . "</td>
                    <td class=" . det_class($d["due"]) . ">" . Date("H:i d/m/y", $d["due"]) . "</td>
                    <td>" . $wf->step->desc . "</td>
                    <td>" . $row . "</td>
                    <td title='" . strval($wf->desc) . "'>" . truncate($wf->desc, 70, false) . "</td>
                    <td>" . $partner . "</td>";

                if ($d['processID'] == 15) {
                    $virtue = new Virtue();

                    $virtue_users = [22, 26, 49, 56, 105, 108, 109, 111];

                    if (in_array(User::get_default_instance('id'), $virtue_users)) {
                        $return .= "<td><a href='/pages/virtue/?id=" . $d['object_index'] . "' class='profile'>Virtue Link</a></td>";
                    } else {
                        $return .= "<td>&nbsp;</td>";
                    }
                }
                $return .= "</tr>";
            }


            if ($q && $q1) {
                $json->status = true;
            } else {
                $json->status = false;
                $json->error = "An error has occurred while trying to query the database. Please try again, if this persists contact IT.";
            }

            if ($return == null) {
                $return = "<tr><td>There are no workflow tickets to show.</td></tr>";
            }

            $json->page_no = $page_no;
            $json->record_count = $x;
            $json->pages = ceil($pages);
            $json->data = $return;

            echo json_encode($json);

            break;

        case "plugin":

            $r = [
                'wf_items' => false
            ];

            $s = new Search(new Workflow);

            if ($_GET['level'] == "false" || $_GET['level'] == $_GET['object_type']) {
                $s->eq('_object', $_GET["object_type"]);
                $s->eq('index', $_GET["object_id"]);
            } else {
                $s->eq("_object", $_GET['level']);

                switch ($_GET['level']) {

                    case "policy":
                        //get all workflow for policies under client
                        $s2 = new Search(new Policy());
                        $s2->eq("client", $_GET['object_id']);

                        $policies = [];

                        while($p = $s2->next(MYSQLI_ASSOC)){
                            $policies[] = $p->id();
                        }

                        $s->eq('index', $policies);

                        break;

                    case "LifeEventTracking":
                        $events = [];
                        $tracking = [];

                        $s2 = new Search(new LifeEvent());
                        $s2->eq("client", $_GET['object_id']);
                        while ($e = $s2->next(MYSQLI_ASSOC)) {
                            $events[] = $e->id();
                            $s3 = new Search(new LifeEventTracking());
                            $s3->eq("life_event", $e->id());
                            while ($t = $s3->next(MYSQLI_ASSOC)) {
                                $tracking[] = $t->id();
                            }
                        }
                        if (empty($tracking)) {
                            $tracking = 0;
                        }
                        $s->eq('index', $tracking);

                        break;
                }
            }

            $i = 0;
            while ($item = $s->next(MYSQLI_ASSOC)) {
                #check for entry in workflow_progress
                $db1 = new mydb();

                $db1->query("select * from " . WFPROGRESS_TBL . " where workflowID = " . $item->id());
                $progress = "";
                if ($ed = $db1->next(MYSQLI_ASSOC)) {
                    if ($ed['userID'] == User::get_default_instance("id")) {
                        $progress = " - <img title='In Progress by Me' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user.png' />";
                    } else {
                        $wfUser = new User($ed['userID'], true);
                        $progress = " - <img title='In Progress by " . $wfUser->staff_name() . "' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user2.png' />";
                    }
                }


                $r["wf_items"] .= "<tr>
                    <td><input id='" . $item->id . "' class='" . $item->step->desc . "' type='checkbox'></td>
                    <td>" . $item->link($item->id()) . $progress .
                    "</td><td class=" . det_class($item->due()) . ">" . Date("H:i d/m/y", $item->due()) .
                    "</td><td>" . $item->step .
                    "</td><td>" . $item->addedby .
                    "</td><td>" . truncate($item->desc, 70, false) . "</td><td>&nbsp;</td></tr>";

                $i = $i + 1;
            }

            $s = new Search(new WorkflowAudit);

            if ($_GET['level'] == "false" || $_GET['level'] == $_GET['object_type']) {
                $s->eq('_object', $_GET["object_type"]);
                $s->eq('index', $_GET["object_id"]);
            } else {
                $s->eq("_object", $_GET['level']);

                switch ($_GET['level']){
                    case "policy":
                        //this is currently only implemented on the client profile therefore policy is on the case required here
                        $s2 = new Search(new Policy());
                        $s2->eq("client", $_GET['object_id']);

                        $policies = [];
                        while($p = $s2->next(MYSQLI_ASSOC)){
                            $policies[] = $p->id();
                        }

                        $s->eq('index', $policies);

                        break;

                    case "LifeEventTracking":
                        $events = [];
                        $tracking = [];

                        $s2 = new Search(new LifeEvent());
                        $s2->eq("client", $_GET['object_id']);
                        while ($e = $s2->next(MYSQLI_ASSOC)) {
                            $events[] = $e->id();
                            $s3 = new Search(new LifeEventTracking());
                            $s3->eq("life_event", $e->id());
                            while ($t = $s3->next(MYSQLI_ASSOC)) {
                                $tracking[] = $t->id();
                            }
                        }

                        if (empty($tracking)) {
                            $tracking = 0;
                        }

                        $s->eq('index', $tracking);

                        break;
                }
            }

            $j = 0;

            while ($item = $s->next(MYSQLI_ASSOC)) {
                $r["wf_items"] .= "<tr>
                    <td>&nbsp;</td>
                    <td><a href='/pages/workflow/?id=" . $item->id() . "' class='profile'>" . $item->workid() . "</a></td><td class=" . det_class($item->due()) . ">" . Date("H:i d/m/y", $item->due()) .
                    "</td><td>" . $item->step .
                    "</td><td>" . $item->addedby .
                    "</td><td>" . truncate($item->desc, 70, false) . "</td>";

                if ($item->early_end() == 1) {
                    $r["wf_items"] .= "<td><span class='tag'>Premature End</span></td></tr>";
                } else {
                    $r["wf_items"] .= "<td><span class='tag'>Historic</span></td></tr>";
                }


                $j = $j + 1;
            }

            $r["total_rows"] = $i + $j;

            echo json_encode($r);

            break;

        case "wf_assigned":
            $w = new Workflow;
            $wa = new WorkflowAssigned;
            $db = new mydb();

            $r = [];
            $users = "";
            $groups = "";

            $s = new Search($wa);
            $s->eq('workflow_id', $_GET["wfid"]);
            $s->nt('user_id', 'NULL');

            # get results
            while ($d = $s->next(MYSQLI_ASSOC)) {
                $users .= "<tr><td>" . $d->user_id->username . "</td></tr>";
            }

            $db->query("SELECT " . GRP_TBL . ".id," . GRP_TBL . ".name FROM " . GRP_TBL .
                " INNER JOIN " . WFASS_TBL .
                " ON " . WFASS_TBL . ".`group_id` = " . GRP_TBL . ".id " .
                "WHERE " . WFASS_TBL . ".workflow_id = '" . $_GET["wfid"] . "'");

            # get results
            while ($d = $db->next(MYSQLI_ASSOC)) {
                $groups .= "<tr><td>" . $d["name"] . "</td></tr>";
            }

            $r["users"] = $users;
            $r["groups"] = $groups;

            if ((!$r["users"]) && (!$r["groups"] || User::get_default_instance("team_leader") == 1 || User::get_default_instance("department") == 7)) {
                if (User::get_default_instance("team_leader") == 1 || User::get_default_instance("department") == 7 || in_array(User::get_default_instance("id"), [98, 215, 248])) {
                    $r["users"] = "<tr><td id=\"choose-assignee\">
                            Select people
                            <button id='add-assigned'>+</button>
                            <button id='submit-wf-assigned' style='display: none;'>Submit</button>
                            <div id='assigned' style=\"display:none;height: 100px;overflow-y: scroll;\">
                                <table>
                                </table>
                            </div>
                        </td></tr>";
                } else {
                    $r["users"] = "<tr><td>This ticket is open to all PS staff</td></tr>";
                }
            }

            echo json_encode($r);

            break;

        case "history":
            echo new Template('workflow/historic.html', ["wf" => new Workflow]);

            break;

        case "wf_history":

            //Gets the audit history for workflow

            $json = [
                "status" => null,
                "data" => null
            ];

            function get_user($id)
            {
                $user = new User($id, true);
                if ($user) {
                    return $user->link();
                } else {
                    return 'Non user';
                }
            }

            $db->query('SELECT * FROM feebase.workflow WHERE id =' . $_GET['wid'] . ' limit 1');

            while ($c = $db->next(MYSQLI_NUM)) {
                $rows .= '<tr>';
                $rows .= '<td class="green"> Initial add</td><td>' . get_user($c[8]) . '</td>';
                $rows .= '<td>' . $_GET['wid'] . '</td><td > ' . $c[2] . '</td><td>' . $c[3] . '</td>';
                $rows .= '<td>' . get_user($c[8]) . '</td><td>' . date('d/m/Y H:i:s', $c[7]) . '</td>';
                $rows .= '</tr>';
            }


            $db->query('select * from feebase.workflow_audit where workID = ' . $_GET['wid']);

            while ($d = $db->next(MYSQLI_NUM)) {
                $rows .= '<tr>';
                $rows .= '<td>' . date('m/d/Y H:i:s', $d[8]) . '</td><td>' . get_user($d[11]) . '</td>';
                $rows .= '<td>' . $_GET['wid'] . '</td><td>' . $d[3] . '</td><td>' . $d[4] . '</td>';
                $rows .= '<td>' . get_user($d[10]) . '</td><td>' . date('d/m/Y H:i:s', $d[9]) . '</td>';
                $rows .= '</tr>';
            }

            if (!isset($rows)) {
                $rows .= '<tr aria-rowspan="3"><td colspan="7">No workflow audit has been recorded</td></tr>';
            }

            $json['data'] = $rows;
            echo json_encode($json);
            break;

        case "process":
//            $db -> query(  "SELECT ".WFPRO_TBL.".processID, ".WFPRO_TBL.".processDesc FROM ".WFPRO_TBL.
//            " INNER JOIN ".WFS_TBL." ON ".
//            WFPRO_TBL.".processID = ".WFS_TBL.".processID".
//            " WHERE ".WFPRO_TBL.".is_active = 1 AND (".WFS_TBL.".group IN (".
//                "SELECT group_id FROM ".TBL_USERS_GROUPS.
//                " INNER JOIN ".GRP_TBL." ON ".GRP_TBL.".id = ".TBL_USERS_GROUPS.".group_id".
//                " WHERE user_id = ".User::get_default_instance("id").
//            ") AND `order` = 1) ".
//            " OR `is_global` = 1") ;

            $db->query("SELECT feebase.workflow_process.processID, feebase.workflow_process.processDesc
            FROM feebase.workflow_process
            INNER JOIN feebase.workflow_step ON feebase.workflow_process.processID = feebase.workflow_step.processID
            WHERE feebase.workflow_process.is_active = 1
            AND ((
                feebase.workflow_step.group IN (
                    SELECT group_id
                    FROM prophet.users_groups
                    INNER JOIN prophet.groups ON prophet.groups.id = prophet.users_groups.group_id
                     WHERE user_id = " . User::get_default_instance("id") . "
            ) AND `order` = 1) OR `is_global` = 1)
            group by feebase.workflow_process.processID
            order by processDesc");


            $flds = [];

            while ($d = $db->next(MYSQLI_NUM)) {
                $flds[$d[0]] = $d[1];
            }
            echo el::dropdown("process", $flds);

            break;

        case "step":
            $db->query("SELECT stepID,stepDesc FROM " . WFS_TBL .
                " w WHERE processID = '" . $_GET['process'] . "' AND active = 1" .
                " ORDER BY w.order");

            $flds = [];

            while ($d = $db->next(MYSQLI_NUM)) {
                $flds[$d[0]] = $d[1];
            }

            echo el::dropdown("workflow[step]", $flds);

            break;

        case "group_users":
            $array = [];
            $db->query('select * from feebase.workflow_step where stepID =' . $_GET['step'] . ' limit 1');

            # get results
            while ($s = $db->next(MYSQLI_ASSOC)) {
                $group_id = $s['group'];
            }


            if ($group_id == 0) {
                $db->query("select " . USR_TBL . ".id,CONCAT(first_name,' ', last_name) as name from " . USR_TBL .
                    " WHERE " . USR_TBL . ".active = '1'" .
                    " ORDER BY " . USR_TBL . ".first_name");

                # get results
                while ($d = $db->next(MYSQLI_ASSOC)) {
                    $row = "<tr><td>" . $d['name'] . "</td><td><input type='checkbox' name='assigned[]' id='assigned_" . $d['id'] . "'  value='" . $d['id'] . "'></td></tr>";
                    if (!in_array($row, $array) ) {
                        $array[] = $row;
                    }
                }
            } else {
                $db->query("select " . USR_TBL . ".id,CONCAT(first_name,' ', last_name) as name from " . USR_TBL .
                    " INNER JOIN " . TBL_USERS_GROUPS . " ON " . USR_TBL . ".id = " . TBL_USERS_GROUPS . " .user_id" .
                    " INNER JOIN " . WFS_TBL . " ON " . WFS_TBL . ".`group` = " . TBL_USERS_GROUPS . " .group_id" .
                    " WHERE " . WFS_TBL . ".stepID = '" . $_GET['step'] . "'" .
                    " AND " . USR_TBL . ".active = '1'" .
                    " ORDER BY " . USR_TBL . ".first_name");

                # get results
                while ($d = $db->next(MYSQLI_ASSOC)) {
                    $row = "<tr><td>" . $d['name'] . "</td><td><input type='checkbox' name='assigned[]' id='assigned_" . $d['id'] . "'  value='" . $d['id'] . "'></td></tr>";
                    if (!in_array($row, $array) ) {
                        $array[] = $row;
                    }
                }
            }

            $string = implode($array);

            echo $string;

            break;

        case "group":
            $db->query("SELECT " . GRP_TBL . ".id," . GRP_TBL . ".name from " . GRP_TBL .
                " INNER JOIN " . WFS_TBL .
                " ON " . WFS_TBL . ".`group` = " . GRP_TBL . " .id" .
                " WHERE " . WFS_TBL . ".stepID = '" . $_GET['step'] . "'");

            # get results
            while ($d = $db->next(MYSQLI_ASSOC)) {
                echo $d['name'] . "<input type='checkbox' name='assignedgrp' id='assigned_" . $d['id'] . "'  value='" . $d['id'] . "'>";
            }

            break;

        case "addingto":
            $db->query("SELECT DISTINCT object from " . WF_TBL . " WHERE object = '" . $_GET['obj'] . "'");

            # get results
            while ($d = $db->next(MYSQLI_ASSOC)) {
                $obj = $d["object"];

                $search = new Search(new $obj);
                $search->eq("id", $_GET["objid"]);
                echo json_encode($search->next()->__toString());
            }

            break;

        case "quick_search":
            $json = (object)[
                "search" => null,
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $from = strtotime(str_replace('/', '-', $_GET['fd'] . " 00:00:00"));
            $to = strtotime(str_replace('/', '-', $_GET['td'] . " 23:59:59"));

            $limit = 50;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            $user = USER::get_default_instance('id');

            $db = new mydb();
            $q = "SELECT workflow_step.processID, step,priority,object,object_index,due,'Mine' as 'group',workflow_id FROM " . WF_TBL . " w 
					INNER JOIN " . WFASS_TBL . " wa 
					ON w.id = wa.workflow_id 
					INNER JOIN workflow_step 
					ON w.step = workflow_step.stepID
					WHERE wa.user_id = " . User::get_default_instance('id') . "
					AND w.due BETWEEN " . $from . " AND " . $to . "

					UNION ALL 
					
						SELECT workflow_step.processID, step,priority,object,object_index,due,'All PS staff' as 'group',workflow_id
						FROM " . WF_TBL . " w
						INNER JOIN " . WFASS_TBL . " wa ON w.id = wa.workflow_id
						INNER JOIN workflow_step ON w.step = workflow_step.stepID
						WHERE wa.is_global = 1 AND w.due BETWEEN " . $from . " AND " . $to . "

					UNION ALL

					SELECT workflow_step.processID, step,priority,object,object_index,due,groups.name as 'group',workflow_id 
					FROM " . WFASS_TBL . " 
					INNER JOIN " . WF_TBL . " 
					ON workflow_id=" . WF_TBL . ".id 
					INNER JOIN workflow_step 
					ON " . WF_TBL . ".step = workflow_step.stepID 
					INNER JOIN " . GRP_TBL . " 
					ON " . GRP_TBL . ".id=" . WFASS_TBL . ".group_id 
					WHERE " . WFASS_TBL . ".group_id IN ( SELECT " . TBL_USERS_GROUPS . ".group_id FROM " . TBL_USERS_GROUPS . " WHERE " . TBL_USERS_GROUPS . ".user_id = " . User::get_default_instance('id') . ")
					AND due BETWEEN " . $from . " AND " . $to . " ORDER BY due ASC";

            $q1 = "SELECT FOUND_ROWS();";
            $db->query($q);
            $db->query($q1);

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages) {
                $i++;
                $page_no[] = $i;
            }

            if (count($page_no) > ceil($pages)) {
                array_pop($page_no);
            }

            if (isset($_GET['offset']) && $_GET['offset'] != "all") {
                $q .= " LIMIT " . $limit . " OFFSET " . $offset;
            }


            $db->query($q);
            $return = "";

            while ($d = $db->next(MYSQLI_ASSOC)) {
                $partner = "";

                $j = 0;

                $wf = new Workflow($d["workflow_id"], true);
                if (strtolower($d["object"]) == "none") {
                    $row = "";
                } else {
                    $obj = (String)$wf->_object;
                    $m = new Search(new $obj);
                    $m->eq('id', $d["object_index"]);
                    $m->set_limit(1);
                    $match = $m->next();
                    if ($match) {
                        $row = $match->link();
                    } else {
                        $row = "error";
                    }
                }

                if ($d["object"] == "Client") {
                    $c = new Client($d["object_index"], true);
                    $partner = $c->partner->link();
                }

                if ($d["object"] == "Policy") {
                    $p = new Policy($d["object_index"], true);
                    $partner = $p->issuer->link();
                }

                #check for entry in workflow_progress
                $db1 = new mydb();


                $db1->query("select * from " . WFPROGRESS_TBL . " where workflowID = " . $wf->id);
                $progress = "";
                if ($ed = $db1->next(MYSQLI_ASSOC)) {
                    if ($ed['userID'] == User::get_default_instance("id")) {
                        $progress = " - <img title='In Progress by Me' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user.png' />";
                    } else {
                        $progressUser = new User($ed['userID'], true);
                        $progress = " - <img title='In Progress by " . $progressUser->staff_name() . "' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user2.png' />";
                    }
                }

                $return .= "<tr>
                    <td><input id='" . $wf->id . "' class='" . $wf->step->desc . "' type='checkbox'></td>
                    <td>" . $wf->link($wf->id) . $progress .
                    "</td><td>" . $d["group"] .
                    "</td><td class=" . det_class($d["due"]) . ">" . Date("H:i d/m/y", $d["due"]) .
                    "</td><td>" . $wf->step->desc .
                    "</td><td>" . $row .
                    "</td><td>" . truncate($wf->desc, 70, false) . "</td>
                <td>" . $partner . "</td>";

                if ($d['processID'] == 15) {
                    $virtue = new Virtue();

                    $virtue_users = [22, 26, 49, 56, 105, 108, 109, 111];

                    if (in_array(User::get_default_instance('id'), $virtue_users)) {
                        $return .= "<td><a href='/pages/virtue/?id=" . $d['object_index'] . "' class='profile'>Virtue Link</a></td>";
                    }
                } else {
                    $return .= "<td>&nbsp;</td>";
                }
                $return .= "</tr>";
                $j++;
            }


            if ($q && $q1) {
                $json->status = true;
            } else {
                $json->status = false;
                $json->error = "An error has occurred while trying to query the database. Please try again, if this persists contact IT.";
            }

            if ($return == null) {
                $return = "<tr><td>There are no workflow tickets to show.</td></tr>";
            }

            $json->page_no = $page_no;
            $json->record_count = $x;
            $json->pages = ceil($pages);
            $json->data = $return;

            echo json_encode($json);


            break;

        case "populate_selects":
            $json = [
                "process_select" => false,
                "step_select" => false
            ];

            $db = new mydb();
            $q = "SELECT * FROM " . WFPRO_TBL . " WHERE is_active = 1  ORDER BY processDesc ASC";
            $db->query($q);

            $json['process_select'] = "<select id='process_select'><option value=''>-- Select Process --</option>";
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $p_id = $row['processID'];
                $p_label = $row['processDesc'];

                $json['process_select'] .= "<option value='" . $p_id . "'>" . $p_label . "</option>";
            }

            $json['process_select'] .= "</select>";

            $q = "SELECT * FROM " . WFPRO_TBL . " WHERE is_active = 1 ";
            $db->query($q);

            $json['step_select'] = "<select id='step_select'><option value=''>-- Select Step --</option>";
            while ($row = $db->next(MYSQLI_ASSOC)) {
                if (isset($row['stepID'])) {
                    $s_id = $row['stepID'];
                    $s_label = $row['stepDesc'];

                    $json['step_select'] .= "<option value='" . $s_id . "'>" . $s_label . "</option>";
                }
            }

            $json['step_select'] .= "</select>";


            $q = "SELECT * FROM " . GRP_TBL . " WHERE id = " . USER::get_default_instance('department');
            $db->query($q);

            $json['assign_select'] = "<select id='ass_to'><option value=''>-- Select --</option><option value='me'>Me</option>";
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $a_id = $row['id'];
                $a_label = $row['name'];

                $json['assign_select'] .= "<option value='group'>Group</option>";

                if (User::get_default_instance("team_leader")){
                    $json['assign_select'] .= "<option value='my_team'>My Team</option>";
                }
            }

            $json['assign_select'] .= "<option value='all'>All PS staff </option></select>";

            echo json_encode($json);

            break;

        case "populate_steps":
            $json['step_select'] = "<select id='step_select'><option value=''>-- Select Step --</option>";

            if (isset($_GET['process']) && $_GET['process'] != ""){
                $q = "SELECT * FROM " . WFS_TBL . " w WHERE processID = " . $_GET['process'] . " ORDER BY w.order ASC";
                $db->query($q);

                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $s_id = $row['stepID'];
                    $s_label = $row['stepDesc'];

                    $json['step_select'] .= "<option value='" . $s_id . "'>" . $s_label . "</option>";
                }
            }

            $json['step_select'] .= "</select>";

            echo json_encode($json);

            break;

        case "workflow_history":

            $s = new Search(new LocalLog);
            $s->eq("object", get_class($object));
            $s->eq("object_id", $object->id());
            $s->nt("snapshot", "");
            $s->add_order("id", Search::ORDER_DESCEND);

            function gen_audit_row($obj, $last_obj)
            {

                $row = "";
                foreach ($obj->get_field_name_map() as $n => $f) {
                    $f = $obj->$n;

                    if (is_a($f, "Group")) {
                        $lo = ($last_obj !== false) ? $last_obj->{$n} : false;
                        $row .= gen_audit_row($f, $lo);
                    } else if (!$f->get_var(Field::BLOCK_UPDATE)) {

                        if ($f->display_name() == "Updatedby") {
                            //dont display column
                        } else if ($f->display_name() == "Updatedwhen") {
                            //dont display column
                        } else {
                            $class = ($last_obj != false && $last_obj->{$n}() !== $f()) ? "edit" : "";
                            $row .= "<td class='$class'>" . ((is_a($f, "Sub")) ? $f->link() : $f) . "</td>\n";
                        }

                    }
                }
                return $row;
            }

            //output current data for selected Policy
            $last_obj = $object->get();

            $rows = "<tr>" .
                "<td class='CURRENT'>CURRENT</td>" .
                "<td>&nbsp;</td>" .
                "<td>&nbsp;</td>" .
                gen_audit_row($last_obj, false) .
                "</tr>";

            while ($l = $s->next()) {

                $rows .= "<tr>" .
                    "<td class='" . $l->type . "'>" . $l->type . "</td>" .
                    "<td>" . $l->user->link() . "</td>" .
                    "<td>$l->date</td>" .
                    gen_audit_row($l->object, $last_obj) .
                    "</tr>";

                $last_obj = $l->object;
            }

            //display insert details
            if ($object->addedby() != '0' && $object->addedby() != '') {
                $rows .= "<tr><td class='side_heading'>INSERT</td><td>" . $object->addedby->link() . "</td><td> " . $object->addedwhen . "</td><td colspan='100%'>&nbsp;</td></tr>";
            } else {
                $rows .= "<tr><td class='side_heading'>INSERT</td><td>Unknown</td><td>Unknown</td><td colspan='100%'>&nbsp;</td></tr>";
            }

            echo $rows;


            break;

        case "adv_search":
            $json = (object)[
                "search" => null,
                "status" => false,
                "error" => null,
                "page_no" => null,
                "pages" => null,
                "record_count" => null,
                "data" => null
            ];

            $from = strtotime(str_replace('/', '-', $_GET['fd'] . " 00:00:00"));
            $to = strtotime(str_replace('/', '-', $_GET['td'] . " 23:59:59"));

            $limit = 50;
            if (!isset($_GET['offset'])) {
                $offset = 0;
            } else {
                $offset = (int)$_GET['offset'] * $limit;
            }

            $db = new mydb();
            $q = "SELECT * FROM " . GRP_TBL . " WHERE id = " . USER::get_default_instance('department');
            $db->query($q);

            while ($row = $db->next(MYSQLI_ASSOC)) {
                $dept = $row['name'];
            }

            if ($_GET['assigned'] == "me") {
                $assigned_to = "Mine";
            } else if ($_GET['assigned'] == "group") {
                $assigned_to = "My Group";
            } else if ($_GET['assigned'] == "all") {
                $assigned_to = "All";
            }

            $db = new mydb();
            $q = "SELECT workflow_step.processID, step,priority,object,object_index,due, wa.group_id,workflow_id FROM " . WF_TBL . " w 
					INNER JOIN " . WFASS_TBL . " wa 
					ON w.id = wa.workflow_id 
					LEFT JOIN " . WFPROGRESS_TBL . " wp
					ON w.id = wp.workflowID
					LEFT JOIN " . USR_TBL . " u
					ON wp.userID = u.id
					INNER JOIN workflow_step
							ON w.step = workflow_step.stepID ";

            if (isset($_GET['wf_id']) && $_GET['wf_id'] != ""){
                $q .= "WHERE w.id = " . $_GET['wf_id'];
            } else {
                if ($_GET['dealing'] == "Partner") {
                    $q .= " INNER JOIN " . POLICY_TBL . " 
						ON w.object_index = " . POLICY_TBL . ".clientID 
						INNER JOIN " . CLIENT_TBL . " 
						ON " . POLICY_TBL . ".clientID = " . CLIENT_TBL . ".clientID 
						INNER JOIN " . PARTNER_TBL . " 
						ON " . CLIENT_TBL . ".partnerID = " . PARTNER_TBL . ".partnerID ";
                }

                if ($_GET['dealing'] == "Issuer") {
                    $q .= " INNER JOIN " . POLICY_TBL . " 
						ON w.object_index = " . POLICY_TBL . ".clientID
						INNER JOIN " . ISSUER_TBL . "
						ON " . POLICY_TBL . ".issuerID = " . ISSUER_TBL . ".issuerID ";
                }

                if($_GET['dealing'] == "Practice") {
                    $q .= " INNER JOIN " . POLICY_TBL . " 
						ON w.object_index = " . POLICY_TBL . ".clientID 
						INNER JOIN " . CLIENT_TBL . " 
						ON " . POLICY_TBL . ".clientID = " . CLIENT_TBL . ".clientID 
						INNER JOIN " . PRACTICE_STAFF_TBL . "
						ON " . CLIENT_TBL . ".partnerID = " . PRACTICE_STAFF_TBL . ".partner_id ";
                }

                if (($_GET['assigned'] == "") || ($_GET['assigned'] == "me")) {
                    $q .= "WHERE wa.user_id = " . USER::get_default_instance("id") . " ";
                } else if ($_GET['assigned'] == "group") {
                    $q .= " INNER JOIN " . TBL_USERS_GROUPS . " 
						ON wa.group_id = " . TBL_USERS_GROUPS . ".group_id 
						WHERE " . TBL_USERS_GROUPS . ".user_id = " . USER::get_default_instance("id") . " ";
                } else if ($_GET['assigned'] == "all") {
                    $q .= "	WHERE ( wa.is_global = 1 ) ";
                } else if ($_GET['assigned'] == "my_team") {
                    $q .= " INNER JOIN " . USR_TBL . " ON wa.user_id = " . USR_TBL . ".id 
                        WHERE " . USR_TBL . ".department = " . User::get_default_instance("department") . " ";
                }

                if ($_GET['dealing'] == "Issuer") {
                    $q .= " AND " . ISSUER_TBL . ".issuerID = " . $_GET['issuer'] . " ";
                }

                if ($_GET['dealing'] == "Partner") {
                    $q .= " AND " . PARTNER_TBL . ".partnerID = " . $_GET['partner'] . " ";
                }

                if($_GET['dealing'] == "Practice") {
                    $q .= " AND " . PRACTICE_STAFF_TBL . ".practice_id = " . $_GET['practice'] . " AND " . PRACTICE_STAFF_TBL . ".active = 1 ";
                }

                if (isset($_GET['priority']) && ($_GET['priority'] != "")) {
                    $q .= "AND priority = " . $_GET['priority'] . " ";
                }

                if (isset($_GET['process']) && $_GET['process'] != ""){
                    if (isset($_GET['step']) && ($_GET['step'] != "")) {
                        $q .= "AND step = " . $_GET['step'] . " ";
                    } else {
                        $db2 = new mydb();
                        $q2 = "SELECT * FROM " . WFS_TBL . " WHERE processID = " . $_GET['process'];

                        $db2->query($q2);

                        $steps = [];

                        while($step = $db2->next(MYSQLI_ASSOC)){
                            $steps[] = $step['stepID'];
                        }

                        $q .= "AND step in (" . implode(',', $steps) . ")";
                    }
                }

                if (($from != false) && ($to != false)) {
                    $q .= "AND due BETWEEN " . $from . " AND " . $to;
                }

                if (!empty($_GET['progress']) && $_GET['progress'] !== "--") {
                    $q .= " AND wp.userID = " . $_GET['progress'] . " ";
                }

                if(!empty($_GET['progress_dept']) && $_GET['progress_dept'] != "--") {
                    $q .= " AND u.department = " . $_GET['progress_dept'] . " ";
                }

                if ($_GET['dealing'] == "Partner") {
                    $q .= " UNION
						SELECT workflow_step.processID, step,priority,object,object_index,due,'" . $assigned_to . "' as 'group',workflow_id FROM " . WF_TBL . " w
						INNER JOIN " . WFASS_TBL . " wa
							ON w.id = wa.workflow_id
						INNER JOIN workflow_step
							ON w.step = workflow_step.stepID
						WHERE object = 'Partner'
						AND object_index = " . $_GET['partner'] . " AND step = " . $_GET['step'] . " ";

                    if (isset($_GET['priority']) && ($_GET['priority'] != "")) {
                        $q .= "AND priority = " . $_GET['priority'] . " ";
                    }
                }

                if ($_GET['dealing'] == "Practice") {
                    $q .= " UNION
						SELECT workflow_step.processID, step,priority,object,object_index,due,'" . $assigned_to . "' as 'group',workflow_id FROM " . WF_TBL . " w
						INNER JOIN " . WFASS_TBL . " wa
							ON w.id = wa.workflow_id
						INNER JOIN workflow_step
							ON w.step = workflow_step.stepID
						WHERE object = 'Partner'
						AND object_index IN (
						    SELECT partner_id FROM " . PRACTICE_STAFF_TBL . " WHERE practice_id = " . $_GET['practice'] . " AND active = 1
						) AND step = " . $_GET['step'] . " ";

                    if (isset($_GET['priority']) && ($_GET['priority'] != "")) {
                        $q .= "AND priority = " . $_GET['priority'] . " ";
                    }
                }

                if ($_GET['dealing'] == "Issuer") {
                    $q .= " UNION
						SELECT workflow_step.processID, step,priority,object,object_index,due,'" . $assigned_to . "' as 'group',workflow_id FROM " . WF_TBL . " w 
						  INNER JOIN " . WFASS_TBL . " wa
							ON w.id = wa.workflow_id
							INNER JOIN workflow_step
							ON w.step = workflow_step.stepID
						  INNER JOIN " . CLIENT_TBL . "
						    ON w.object_index = " . CLIENT_TBL . ".clientID
						  INNER JOIN " . POLICY_TBL . "
						    ON " . CLIENT_TBL . ".clientid = " . POLICY_TBL . ".clientID
						  INNER JOIN " . ISSUER_TBL . " i 
							ON " . POLICY_TBL . ".issuerid = i.issuerID
						  WHERE i.issuerID = " . $_GET['issuer'] . "
							AND step = " . $_GET['step'] . "
							AND object = 'Issuer'";

                    if (isset($_GET['priority']) && ($_GET['priority'] != "")) {
                        $q .= " AND priority = " . $_GET['priority'] . " ";
                    }
                }
            }

            $q .= " ORDER BY due ASC, priority DESC, workflow_id ASC ";

            $q1 = "SELECT FOUND_ROWS();";
            $db->query($q);
            $db->query($q1);

            //result($x) & page count
            $x = $db->next(MYSQLI_NUM);
            $x = (int)array_shift($x);
            $pages = $x / $limit;

            //create array used to populate select#page_number
            $i = 0;
            $page_no = [];
            while ($i <= $pages) {
                $i++;
                $page_no[] = $i;
            }

            if (count($page_no) > ceil($pages)) {
                array_pop($page_no);
            }

            if (isset($_GET['offset']) && $_GET['offset'] != "all") {
                $q .= " LIMIT " . $limit . " OFFSET " . $offset;
            }

            $db->query($q);

            $return = "";

            while ($d = $db->next(MYSQLI_ASSOC)) {
                $wf = new Workflow($d["workflow_id"], true);
                //display department as group so user knows what dept the ticket belongs to
                if (isset($d['group_id']) && $d['group_id'] != "") {
                    $db1 = new mydb();
                    $db1->query("SELECT * FROM " . GRP_TBL . " WHERE id = " . $d['group_id']);
                    while ($g = $db1->next(MYSQLI_ASSOC)) {
                        $group = $g['name'];
                    }
                }

                if (!isset($group)) {
                    $group = "Mine";
                }

                $partner = "";

                if (strtolower($d["object"]) == "none" || strval($wf->_object) == '') {
                    $row = "";
                } else {
                    $obj = (String)$wf->_object;
                    $m = new Search(new $obj);
                    $m->eq('id', $d["object_index"]);
                    $m->set_limit(1);
                    $match = $m->next();
                    if ($match) {
                        if (strtolower($d["object"]) == "lifeeventtracking") {
                            // get client linked to tracking wf
                            $let = new LifeEventTracking($d["object_index"], true);
                            $row = $let->life_event->client->link();
                        } else {
                            $row = $match->link();
                        }
                    } else {
                        $row = "error";
                    }
                }

                if ($d["object"] == "Client") {
                    $c = new Client($d["object_index"], true);
                    $partner = $c->partner->link();
                }

                if ($d["object"] == "Policy") {
                    $p = new Policy($d["object_index"], true);
                    $partner = $p->issuer->link();
                }

                #check for entry in workflow_progress
                $db1 = new mydb();

                $db1->query("select * from " . WFPROGRESS_TBL . " where workflowID = " . $wf->id);
                $progress = "";
                if ($ed = $db1->next(MYSQLI_ASSOC)) {
                    if ($ed['userID'] == User::get_default_instance("id")) {
                        $progress = " - <img title='In Progress by Me' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user.png' />";
                    } else {
                        $progressUser = new User($ed['userID'], true);
                        $progress = " - <img title='In Progress by " . $progressUser->staff_name() . "' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user2.png' />";
                    }
                }

                //if the workflow is a valuation, add in the delivery method column
                $method = $req_for = $extra_class = $high_priority = "";

                $valSteps = Workflow::VALUATION_STEPS;
                $valSteps[] = 189;

                if (in_array($wf->step(), $valSteps)) {
                    $sVal = new Search(new ValuationRequest());
                    $sVal->eq("workflow", $wf->id());

                    $valReq = $sVal->next(MYSQLI_ASSOC);

                    $method = $req_for = "<td>";
                    $extra_class = "show_val_col";

                    if(!empty($valReq)){
                        $method .= strval($valReq->delivery_method);

                        if ($valReq->delivery_method() == 3) {
                            if ($valReq->servicing_report()) {
                                $method .= " (Servicing Screen)";
                            } else {
                                $method .= " (Client Profile)";
                            }
                        }

                        if($valReq->servicing_report()){
                            $sos = new Search(new OngoingService());

                            $sos->eq("val_req_id", $valReq->id());

                            if ($os = $sos->next(MYSQLI_ASSOC)) {
                                if ($os->review_type() === "PS Forced") {
                                    $method .= "<span class='badge badge-pill badge-danger'>Forced</span>";
                                }

                                if(!empty($os->proposed_review_date())){
                                    $req_for .= \Carbon\Carbon::parse($os->proposed_review_date())->format('d/m/Y');
                                } elseif(!empty($valReq->requested_for())) {
                                    $req_for .= \Carbon\Carbon::createFromTimestamp($valReq->requested_for())->format('d/m/Y');
                                }
                            } elseif(!empty($valReq->requested_for())) {
                                $req_for .= \Carbon\Carbon::createFromTimestamp($valReq->requested_for())->format('d/m/Y');
                            }
                        } else {
                            if(!empty($valReq->requested_for())){
                                $req_for .= \Carbon\Carbon::createFromTimestamp($valReq->requested_for())->format('d/m/Y');
                            }
                        }
                    }

                    if (!empty($c)) {
                        // we have a client, check their policies for type 39 (Discretionary Managed Investment)
                        $sp = new Search(new Policy());
                        $sp->eq("client", $c->id());
                        $sp->eq("dfm", 1);
                        $sp->eq("status", 2);

                        if ($sp->next(MYSQLI_ASSOC)){
                            $method .= "&nbsp;<span class='badge badge-pill badge-primary'>DFM</span>";
                        }
                    }

                    $method .= "</td>";
                    $req_for .= "</td>";
                }

                if($wf->priority() == 3){
                    $extra_class .= " table-warning";
                    $high_priority = "<i class='far fa-info-circle text-warning' title='High Priority'></i>";
                }

                $return .= "<tr class='" . $extra_class . "'>
                    <td><input id='" . $wf->id . "' class='" . $wf->step->desc . "' type='checkbox'></td>
                    <td >" . $wf->link($wf->id) . " " . $high_priority . $progress . "</td>
                    <td>" . $group . "</td>
                    <td class=" . det_class($d["due"]) . ">" . Date("H:i d/m/y", $d["due"]) . "</td>
                    " . $req_for . "
                    <td>" . $wf->step->desc . "</td>
                    " . $method . "
                    <td>" . $row . "</td>
                    <td>" . truncate(str_replace("£", "&pound;", $wf->desc()), 70, false) . "</td>
                    <td>" . $partner . "</td>";

                //if wf ticket is on a step belonging to the virtue process..
                if ($d['step'] == 24 || $d['step'] == 25 || $d['step'] == 26 || $d['step'] == 27 || $d['step'] == 28) {
                    $return .= "<td><a href='/pages/virtue/?id=" . $d['object_index'] . "' class='profile'>Virtue Link</a></td>";
                } else {
                    $return .= "<td>&nbsp;</td>";
                }
                $return .= "</tr>";
            }

            if ($q && $q1) {
                $json->status = true;
            } else {
                $json->status = false;
                $json->error = "An error has occurred while trying to query the database. Please try again, if this persists contact IT.";
            }

            if ($return == null) {
                $return = "<tr><td>There are no workflow tickets to show.</td></tr>";
            }

            $json->page_no = $page_no;
            $json->record_count = $x;
            $json->pages = ceil($pages);
            $json->data = $return;

            echo json_encode($json);

            break;

        case "step_form":
            switch($_GET['step']){
                case 188:
                    echo new Template("valuation/request/new.html");
                    break;
            }

            break;

        case "count_day":
            $response = [
                "success" => true,
                "data" => null,
            ];

            $db = new mydb();

            if(strlen($_GET['date']) === 10){
                $dueDate = \Carbon\Carbon::parse($_GET['date']);

                $db->query("SELECT count(*) as 'tickets' FROM " . WF_TBL . " 
                            WHERE step = " . $_GET['step'] . "
                            AND due >= " . $dueDate->startOfDay()->format('U') . "
                            AND due <= " . $dueDate->endOfDay()->format('U') . "
                            ");

                $result = $db->next(MYSQLI_ASSOC);

                $response['data'] = "There are " . $result['tickets'] . " tickets currently due on " . $dueDate->toFormattedDateString() . " for this step.";
            } else {
                $response['data'] = "Invalid date.";
            }

            echo json_encode($response);

            break;
    }
} elseif (isset($_GET["new"])) {
    if (isset($_GET["add"])) {
        $w = new Workflow;
        $is_global = $_GET['all'];
        $assigned = false;
        $assigned_grp = (isset($_POST["assignedgrp"])) ? $_POST["assignedgrp"] : false;

        if (isset($_POST["assigned"])) {
            $assigned = $_POST["assigned"];
        } else {
            $assigned = null;
        }

        ob_start();
        $w->workflow_add($_POST["workflow"], $assigned, $assigned_grp, $is_global);
        $wfContents = json_decode(ob_get_contents());
        ob_end_clean();

        if (!empty($_POST['valuationrequest'])){
            $vr = new ValuationRequest();

            $vr->load($_POST['valuationrequest']);

            $vr->analytics("none");

            if (!empty($_POST['valuationrequest']['analytics'])){
                $vr->analytics(implode(", ", $_POST['valuationrequest']['analytics']));
            }

            $vr->workflow($w->id());
            $vr->requested_for($w->due());

            if ($w->_object() === "Client"){
                $vr->client($w->index());
            } else {
                $className = $w->_object();
                $p = new $className($w->index(), true);

                $vr->client($p->client->id());
            }

            if(!$vr->save()){
                $wfContents['status'] = false;
                $wfContents['error'] = "Created workflow but failed to add valuation request! Contact IT!";
            }
        }

        echo json_encode($wfContents);
    } else {
        // display form
        echo new Template("workflow/new.html", ["wf" => new Workflow, "obj" => $_GET["obj"], "objid" => $_GET["objid"]]);
    }
} elseif (isset($_GET["search"])) {
    //if this gets any more complicated Ill create an object. Just now old-style is fine

    $json = (object)[
        "search" => $_GET["search"],
        "page_no" => null,
        "pages" => null,
        "total_count" => null,
        "page_count" => null,
        "data" => null
    ];

    $filters = $_GET["filter"];

    $limit = 20;
    if ($_GET['offset']) {
        $offset = $_GET['offset'] * $limit;
    } else {
        $offset = 0;
    }

    $fd = Date::factory()
        ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

    $td = Date::factory()
        ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

    if (!empty($filters["from_date"])) {
        $fd->set($filters["from_date"]);
    }

    if (!empty($filters["to_date"])) {
        $td->set($filters["to_date"]);
    }

    if ((isset($_GET['fd'])) && ($_GET['fd'] != "//")) {
        $fd->value = DateTime::createFromFormat('j/n/Y', $_GET['fd'])->getTimestamp();
    }

    if ((isset($_GET['td'])) && ($_GET['td'] != "//")) {
        $td->value = DateTime::createFromFormat('j/n/Y', $_GET['td'])->getTimestamp();
    }

    if (isset($_GET['field'])) {
        $filters["field"] = $_GET['field'];
    }

    $db = new mydb();

    $sql = "SELECT SQL_CALC_FOUND_ROWS  max(id), id, due, completed, workID,desctext FROM " . WFA_TBL . " WHERE added_by = " . User::get_default_instance("id");
    $sql .= ($fd() && $td()) ? " AND (" . $filters["field"] . " BETWEEN " . $fd->value . " AND " . $td->value . ")" : false;
    $sql .= " GROUP BY workID LIMIT " . $limit . " OFFSET " . $offset;

    $db->query($sql);

    $sql1 = "SELECT FOUND_ROWS();";

    $db->query($sql);
    $db->query($sql1);

    //result($x) & page count
    $x = $db->next(MYSQLI_NUM);
    $x = (int)array_shift($x);

    $pages = $x / $limit;

    //create array used to populate select#page_number
    $i = 0;
    $page_no = [];
    while ($i <= $pages) {
        $i++;
        $page_no[] = $i;
    }

    if (count($page_no) > ceil($pages)) {
        array_pop($page_no);
    }

    $db->query($sql);

    # get results
    $r = 0;
    while ($d = $db->next(MYSQLI_ASSOC)) {
        $class = ($d['due'] < $d['completed']) ? "red" : "green";

        #check for entry in workflow_progress
        $db1 = new mydb();

        $db1->query("select * from " . WFPROGRESS_TBL . " where workflowID = " . $d->id);
        $progress = "";
        if ($ed = $db1->next(MYSQLI_ASSOC)) {
            if ($ed['userID'] == User::get_default_instance("id")) {
                $progress = " - <img title='In Progress by Me' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user.png' />";
            } else {
                $progressUser = new User($ed['userID'], true);
                $progress = " - <img title='In Progress by " . $progressUser->staff_name() . "' id='wf_progress_guy' style='height:15px;width:10px;' src='/lib/images/user2.png' />";
            }
        }

        $results .= "<tr>
						<td><a href='/pages/workflow/?id=" . $d["id"] . "' class='profile'>" . $d["workID"] . "</a>" . $progress . "</td>" .
            "<td>" . Date("H:i d/m/y", $d["due"]) . "</td>" .
            "<td class=" . $class . ">" . Date("H:i d/m/y", $d["completed"]) . "</td>" .
            "<td>" . truncate($d["desctext"], 70) . "</td>
					</tr>";
        $r = $r + 1;
    }

    $json->page_no = $page_no;
    $json->total_count = $x;
    $json->page_count = $r;
    $json->pages = ceil($pages);
    $json->data = $results;

    echo json_encode($json);
} else {
    echo new Template('workflow/search.html');
}
