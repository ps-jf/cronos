<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

class FCCImportFile extends DatabaseObject
{
    var $table = "prophet.fcc_import_files";

    public function __construct()
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);
        $this->uploaded_datetime = new Field("uploaded_datetime");
        $this->imported_by = new Sub("User", "imported_by");
        $this->filename = new Field("filename");
        $this->safe_name = new Field("safe_name");
        $this->hash = new Field("SHA1_hash");

        $this->CAN_DELETE = true;
        call_user_func_array(["parent", "__construct"], func_get_args());
    }

    public function get_pending_import_dir_fcc()
    {
        return COMMISSION_IMPORT_FCC;
    }
}


if (isset($_FILES['file']) && sizeof($_FILES["file"])) {
    //// Bank Account files are being uploaded by the User

    // this page is being loaded into an iframe due to the ajax form file upload
    // problem. Therefore prepare a JSON-object containing information about each
    // uploaded file and it's result to be returned to the parent frame.

    header("Content-type: text/plain");

    $json = [];

    $file = $_FILES["file"];

    $json = (object)[
        "id" => null,
        "status" => false, // file upload status
        "error" => null,
        "preview" => null,  // first ten data lines from import file
        "safename" => null,  // randomly generated filename for import file
    ];

    // only accept csv files
    if (in_array($file["type"], ["text/csv", "text/plain", "application/vnd.ms-excel"])) {
        // give file a randomly generated file name so that it doesn't overwrite exisiting file
        $safe_name = random_string(10);

        // check if file being uploaded already exists
        file_exists(FCCImportFile::get_pending_import_dir_fcc() . $safe_name);

        // to prevent bank_account files being imported multiple times, prepare a hash of
        // the file to store and compare against previously imported files
        $content = file_get_contents($file["tmp_name"]);
        $hash = sha1($content);

        // check for potential duplicates
        $s = new Search(new FCCImportFile);
        $s->eq("hash", $hash);

        $json->matches = [];
        while ($m = $s->next()) {
            $json->matches[] = "$m->filename imported by $m->imported_by at " . date('d/m/Y h:i:s', $m->uploaded_datetime());
        }

        $fccf = new FCCImportFile;
        $fccf->load([
            "type" => $_POST["type"],
            "imported_by" => $_USER->id(),
            "uploaded_datetime" => time(),
            "safe_name" => $safe_name,
            "filename" => $file["name"],
            "hash" => $hash
        ]);

        if ($fccf->save()) {
            // Move file to upload directory
            move_uploaded_file($file["tmp_name"], FCCImportFile::get_pending_import_dir_fcc() . $safe_name);

            $json->status = true;
            $json->id = $fccf->id();

            // Open and read file then assign data to multidimensional array -> ten line preview for user
            $fccfile = [];

            // Open file in read mode
            if (($handle = fopen(FCCImportFile::get_pending_import_dir_fcc() . $safe_name, "r")) !== false) {
                flock($handle, LOCK_SH);

                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    $fccfile[] = $data; //add the row to the main array.
                }
            }

            // Close File
            flock($handle, LOCK_UN);
            fclose($handle);


            // Ensure that 'needed' columns all have data present
            $errors = [];

            for ($i = 1; $i < count($fccfile); $i++) {
                if (empty($fccfile[$i][9])) {           /* Date */
                    array_push($errors, $i);
                } else if (empty($fccfile[$i][10])) {     /* Narrative #1 (Description) */
                    array_push($errors, $i);
                } else if (empty($fccfile[$i][16]) && empty($fccfile[$i][17])) {   /* Check that either a debit or credit amount exists on line */
                    array_push($errors, $i);
                } else {
                    continue;
                }
            }

            $json->error = $errors;
            $json->preview = $fccfile;
            $json->safename = FCCImportFile::get_pending_import_dir_fcc() . $safe_name;
            $json->type = "FCC";
        }

        if (!$json->status) {
            $json->error = "System error, please try again";
        }
    } else {
        $json->error = $file["type"] . " not acceptable";
    }

    print json_encode($json);
} else if (isset($_GET["id"], $_GET["do"]) && intval($_GET["id"])) {
    $importid = (int)$_GET["id"];

    switch ($_GET["do"]) {
        case "delete":
            $json = (object)[
                "id" => null,
                "status" => false,
                "error" => false
            ];

            try {
                $db = new mydb();

                // Delete uploaded file from server
                $db->query("SELECT safe_name FROM prophet.fcc_import_files WHERE id = " . $importid);

                if ($d = $db->next(MYSQLI_ASSOC)) {
                    unlink(FCCImportFile::get_pending_import_dir_fcc() . $d['safe_name']);
                } else {
                    die("Cant find import file to be deleted");
                }

                // Delete database entry
                $db->query("DELETE FROM prophet.fcc_import_files WHERE id = " . $importid);

                $json->status = true;
            } catch (Exception $e) {
                $json->status = false;
                $json->error = $e->getMessage();
            }

            print json_encode($json);

            break;
    }
} else {
    echo Template("fcc/upload.html");
}
