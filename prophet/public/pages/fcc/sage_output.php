<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";
        
if (isset($_GET["do"]) && isset($_GET['date1']) && isset($_GET['date2'])) {

    switch ($_GET["do"]) {

        // Bank Entry CSV file export that can be imported into Sage
        case "be":
            try {
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];

                // Build filename to save guys renaming Sage file
                $time = strtotime($date1);
                $month = date("F", $time);
                $year = date("Y", $time);
                $filename = $month . $year;

                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename=" . $filename . ".csv");
                header("Pragma: no-cache");
                header("Expires: 0");

                $outstream = fopen("php://output", "w");
                $terry_file = fopen("/mnt/sage_imports/" . $filename . ".csv", "w");

                fputcsv($outstream, [
                    "Type",
                    "Account Reference",
                    "Nominal A/C Ref",
                    "Department Code",
                    "date",
                    "Reference",
                    "Details",
                    "Net Amount",
                    "Tax Code",
                    "Tax Amount",
                    "Exchange Rate",
                    "Extra Reference",
                    "User Name",
                    "Project Refn",
                    "Cost Code Refn"
                ]);
                fputcsv($terry_file, [
                    "Type",
                    "Account Reference",
                    "Nominal A/C Ref",
                    "Department Code",
                    "date",
                    "Reference",
                    "Details",
                    "Net Amount",
                    "Tax Code",
                    "Tax Amount",
                    "Exchange Rate",
                    "Extra Reference",
                    "User Name",
                    "Project Refn",
                    "Cost Code Refn"
                ]);

                $db = new mydb();

                $db->query("SELECT IF(amount = 0.00,'BP','BR') AS `Type`, ".
                            "'1200' AS `Account Reference`, ".
                            "accounts_categories.sage_nc AS `Nominal A/C Ref`, " .
                            "' ' AS `Department Code`, ".
                            "tblbankaccount.`date`, ".
                            "tblbankaccount.id AS `Reference`, ".
                            "tblbankaccount.descn AS `Details`, " .
                            "IF((amount = 0.00),(REPLACE(amountdr,'-','')),amount) AS `Net Amount`, ".
                            "'T9' AS `Tax Code`, " .
                            "0 AS `Tax Amount`, ".
                            "' ' AS `Exchange Rate`, ".
                            "' ' AS `Extra Reference`, ".
                            "' ' AS `User Name`, ".
                            "' ' AS `Project Refn`, ".
                            "' ' AS `Cost Code Refn` " .
                            "FROM tblbankaccount " .
                            "INNER JOIN accounts_categories " .
                            "ON tblbankaccount.CategoryId = accounts_categories.id " .
                            "WHERE tblbankaccount.`date` BETWEEN '" . $_GET['date1'] . "' AND '" . $_GET['date2'] . "'");

                $list = [];

                while ($so = $db->next(MYSQLI_ASSOC)) {
                    array_push($list, $so);
                }

                foreach ($list as $row) {
                    fputcsv($outstream, $row, ',', '"');
                    fputcsv($terry_file, $row, ',', '"');
                }
            } catch (Exception $e) {
                die("Error creating file, contact IT");
            }

            break;

        // VAT CSV file export that can be imported into Sage
        case "vat":
            try {
                $date1 = $_GET['date1'];

                // Build filename to save guys renaming Sage file
                $time = strtotime($date1);
                $month = date("F", $time);
                $year = date("Y", $time);
                $filename = $month . $year;

                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename=" . $filename . "-vat.csv");
                header("Pragma: no-cache");
                header("Expires: 0");

                $outstream = fopen("php://output", "w");
                $terry_file = fopen("/mnt/sage_imports/" . $filename . "-vat.csv", "w");

                fputcsv($outstream, [
                    "Type",
                    "Account Reference",
                    "Nominal A/C Ref",
                    "Department Code",
                    "date",
                    "Reference",
                    "Details",
                    "Gross Amount",
                    "Net Amount",
                    "Tax Code",
                    "Tax Amount",
                    "Exchange Rate",
                    "Extra Reference",
                    "User Name",
                    "Project Refn",
                    "Cost Code Refn"
                ]);
                fputcsv($terry_file, [
                    "Type",
                    "Account Reference",
                    "Nominal A/C Ref",
                    "Department Code",
                    "date",
                    "Reference",
                    "Details",
                    "Gross Amount",
                    "Net Amount",
                    "Tax Code",
                    "Tax Amount",
                    "Exchange Rate",
                    "Extra Reference",
                    "User Name",
                    "Project Refn",
                    "Cost Code Refn"
                ]);

                $db = new mydb();

                $db->query("SELECT
                    'BR' AS `Type`,
                    '1217' AS `Account Reference`, 
                    '4000' AS `Nominal A/C Ref`, 
                    ' ' AS `Department Code`, 
                    date_format(tblcommissionauditsql.paidDate, '%d/%m/%Y') as `date`,
                    ' ' AS `Reference`,                            
                    policyNum as `Details`,
                    IF((policyNum like 'Portfolio Report%' or policyNum like '*adjustments - new business%') and (sign(vat) = -1), round(SUM(tblcommissionauditsql.amount)*-1,2), round(SUM(tblcommissionauditsql.amount),2)) AS `Gross Amount`,
                    IF((policyNum like 'Portfolio Report%' or policyNum like '*adjustments - new business%') and (sign(vat) = -1), round((SUM(tblcommissionauditsql.amount)*-1) - (SUM(tblcommissionauditsql.vat)*-1),2), round((SUM(tblcommissionauditsql.amount)) - (SUM(vat)),2)) AS `Net Amount`, 
                    'T1' AS `Tax Code`, 
                    IF((policyNum like 'Portfolio Report%' or policyNum like '*adjustments - new business%') and (sign(vat) = -1), SUM(tblcommissionauditsql.vat*-1), SUM(vat)) AS `Tax Amount`,
                    ' ' AS `Exchange Rate`, 
                    ' ' AS `Extra Reference`, 
                    ' ' AS `User Name`, 
                    ' ' AS `Project Refn`, 
                    ' ' AS `Cost Code Refn` 
                FROM feebase.tblcommissionauditsql
                inner join tblbatch on tblbatch.batchID = tblcommissionauditsql.batchID
                WHERE (tblcommissionauditsql.vat is not null and tblcommissionauditsql.vat != '') 
                AND tblcommissionauditsql.paiddate between '". $date1 ."-01 00:00:00' and '".$_GET['date1']."-31 23:59:59'
                group by tblcommissionauditsql.policyID, Commntype, sign(vat)");

                $list = [];

                while ($so = $db->next(MYSQLI_ASSOC)) {
                    array_push($list, $so);
                }

                foreach ($list as $row) {
                    fputcsv($outstream, $row, ',', '"');
                    fputcsv($terry_file, $row, ',', '"');
                }
            } catch (Exception $e) {
                die("Error creating file, contact IT");
            }

            break;

    }
} else {
    echo Template("fcc/sage_output.html");
}
