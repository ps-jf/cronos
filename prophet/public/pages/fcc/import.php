<?php

require $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";

if (isset($_GET["file"])) {
    $json = (object) [
        "id"      => null,
        "status"  => false,
        "error"   => null,
        "added_ba" => null,
        "added_tba" => null,
        "comm_entries" => null,
        "partner_entries" => null
    ];

    try {
        // Connection to DB
        $db = new mydb();

        // Obtain most recent date of entry in bank_account table
        $db->query("SELECT MAX(date) as ba_max FROM feebase.bank_account");
        if ($d = $db -> next(MYSQLI_ASSOC)) {
            $ba_max = $d['ba_max'];
        } else {
            $json -> error = "Can't obtain most recent entries, contact IT";
            exit;
        }

        // Obtain most recent date of entry in tblbankaccount table
        $db->query("SELECT MAX(date) as tba_max FROM feebase.tblbankaccount");
        if ($d = $db -> next(MYSQLI_ASSOC)) {
            $tba_max = $d['tba_max'];
        } else {
            $json -> error = "Can't obtain most recent entries, contact IT";
            exit;
        }

        // Get the csv file and open
        $file = $_GET["file"];
        $handle = fopen($file, "r");

        // Counter for entries added
        $entries_added_ba = 0;
        $entries_added_tba = 0;

        // Loop through the csv file and insert new credit entries into database
        do {
            // Convert date field from csv file into correct format for database
            $date = str_replace("/", "-", $data[9]);
            $date = strtotime($date);
            $dateformat = date('Y-m-d', $date);


            // Build descn field based on Narrative #1 - Narrative #5
            $desc = [$data[10], $data[11], $data[12], $data[13], $data[14]];
            $desc_str = str_replace([',', '"', "'"], '', trim(implode(' ', $desc)));

            // If there is a credit amount or a debit amount, lets add the entry to tblbankaccount
            if (( $data[17] != '' ) || ( $data[16] != '' )) {
                // Add only entries with date newer than the latest in tblbankaccount table, excluding entries for current day
                if (( $dateformat > $tba_max ) && ( $dateformat < date('Y-m-d') )) {
                    $db->query("INSERT INTO feebase.tblbankaccount (date, descn, amount, amountdr) VALUES
                        (
                            '".addslashes($dateformat)."',
                            '".addslashes($desc_str)."',
                            '".addslashes($data[17])."',
                            '".addslashes($data[16])."'
                        )
                    ");
                    $entries_added_tba++;

                    /*
                    Obtain newly created id from tblbankaccount and use as id
                    in bank_account to tie bank entries up and keep in sync with each other and batches
                    */
                    $ba_id = $db->insert_id;

                    // If there is a credit amount and no debit amount, lets add the entry to bank_account
                    if (( $data[17] ) && $data[16] == '') {
                        // Add only entries with date newer than the latest in bank_account table, excluding entries for current day
                        if (( $dateformat > $ba_max ) && ( $dateformat < date('Y-m-d') ) && ( $desc_str != 'FROM 00202165' )) {
                            $db->query("INSERT INTO feebase.bank_account (id, date, amount, descn, assigned_to) VALUES
                                (
                                    '".addslashes($ba_id)."',
                                    '".addslashes($dateformat)."',
                                    '".addslashes($data[17])."',
                                    '".addslashes($desc_str)."',
                                    '0'
                                )
                            ");
                            $entries_added_ba++;
                        }
                    }
                }
            }
        } while ($data = fgetcsv($handle, 1000, ",", '"'));


        // Update credit payments to 'Commission Received'
        $db2 = new mydb();
        $db2 -> query("UPDATE tblbankaccount ".
                      "SET tblbankaccount.categoryid = 1 ".
                      "WHERE tblbankaccount.categoryid = 0 AND tblbankaccount.amount > 0");
        $comm_entries = $db2->affected_rows;

        // Update credit payments to 'Commission Paid to Partners'
        $db3 = new mydb();
        $db3 -> query("UPDATE tblbankaccount ".
                      "SET tblbankaccount.categoryid = 4 ".
                      "WHERE tblbankaccount.categoryid = 0 AND tblbankaccount.amountdr < 0");
        $partner_entries = $db3->affected_rows;

        // Return to user
        $json -> status = true;
        $json -> error = false;
        $json -> added_ba = $entries_added_ba;
        $json -> added_tba = $entries_added_tba;
        $json -> comm_entries = $comm_entries;
        $json -> partner_entries = $partner_entries;
    } catch (Exception $e) {
        $json -> status = false;
        $json -> error = $e -> getMessage();
    }

    echo json_encode($json);
} else {
    $json = (object) [
        "id"      => null,
        "status"  => false,
        "error"   => "file not found"
    ];

    $json -> status = false;
    echo json_encode($json);
    die();
}
