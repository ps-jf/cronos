<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";

if (isset($_GET["date1"]) && isset($_GET["date2"])) {
    try {
        //get dates as strings and convert them unix times
        $date1 = strtotime($_GET['date1']);
        $date2 = strtotime($_GET['date2']);

        //convert the month and days to first and last of the months
        $convert1 = date("Y-m-d", $date1);
        $convert2 = date("Y-m-t", $date2);

        // Build filename to save guys renaming
        $time = strtotime($convert1);
        $month = date("F", $time);
        $year = date("Y", $time);
        $filename = $month . $year;

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=".$filename.".csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $outstream = fopen("php://output", "w");

        fputcsv($outstream, ["Client", "Policy", "Issuer", "Batch", "Type", "Partner", "Amount", "Rate", "Share", "VAT", "CUR", "Paid Date"]);

        $db = new mydb();

        switch (strtolower($_GET['object'])){
            case "partner":
                $db -> query("select clientSname1, policyNum, issuerName, batchid, commission_type.name, partnerID, amount, 
                            Partnershare, CommtoPartner, tblcommissionauditsql.vat , currency_type , paidDate
                            from feebase.tblcommissionauditsql 
                            inner join feebase.commission_type on tblcommissionauditsql.Commntype = commission_type.id
                            where paidDate BETWEEN '" . $convert1 . "-%' AND '" . $convert2 . "-%'
                            and tblcommissionauditsql.partnerID = '" . $_GET['id']."'");

                break;

            case "client":
                $db -> query("select clientSname1, policyNum, issuerName, batchid, commission_type.name, partnerID, amount, 
                            Partnershare, CommtoPartner, tblcommissionauditsql.vat , currency_type , paidDate
                            from feebase.tblcommissionauditsql 
                            inner join feebase.commission_type on tblcommissionauditsql.Commntype = commission_type.id
                            where paidDate BETWEEN '" . $convert1 . "-%' AND '" . $convert2 . "-%'
                            and tblcommissionauditsql.clientID = '" . $_GET['id']."'");

                break;

            case "policy":
                $db -> query("select clientSname1, policyNum, issuerName, batchid, commission_type.name, partnerID, amount, 
                            Partnershare, CommtoPartner, tblcommissionauditsql.vat , currency_type , paidDate
                            from feebase.tblcommissionauditsql 
                            inner join feebase.commission_type on tblcommissionauditsql.Commntype = commission_type.id
                            where paidDate BETWEEN '" . $convert1 . "-%' AND '" . $convert2 . "-%'
                            and tblcommissionauditsql.policyID = '" . $_GET['id']."'");

                break;
        }

        $list = [];

        while ($so = $db -> next(MYSQLI_ASSOC)) {
            array_push($list, $so);
        }

        foreach ($list as $row) {
            fputcsv($outstream, $row, ',', '"');
        }
    } catch (Exception $e) {
        die("Error creating file, contact IT");
    }
}
