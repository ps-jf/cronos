<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";
        
if (isset($_GET["do"])) {
    switch (($_GET["do"])) {
        case "categorise_payments":
            try {
                $json = (object) [
                    "id"      => null,
                    "status"  => false,
                    "error"   => null,
                ];

                $update_cat = $_POST['bankaccountall']['category'];
                $entry_list = $_POST['entry'];

                foreach ($entry_list as $k => $v) {
                    $tba = new BankAccountAll($v, true);
                    $tba -> category($update_cat);
                    $tba -> save();
                }

                $json -> status = true;
                $json -> error = false;
            } catch (Exception $e) {
                $json -> status = false;
                $json -> error = "Error categorising entries, contact IT";
                die("Error categorising entries, contact IT");
            }

            echo json_encode($json);

            break;

        case "filter_payments":
            try {
                $tba = new BankAccountAll;
                $s = new Search($tba);

                // Filter results to category if category set
                !empty($_GET['cat']) ? $s -> eq("categoryid", $_GET['cat']) : '';

                // Filter using both 'from' and 'to' dates
                ($_GET['from'] != "--" && $_GET['to'] != "--") ? $s -> btw("date", $_GET['from'], $_GET['to']) : '';

                // Filter using only 'from' date if 'to' date is empty
                $today = date("Y-m-d");
                ($_GET['from'] != "--" && $_GET['to'] == "--") ? $s -> btw("date", $_GET['from'], $today) : '';

                //Filter using only 'to' date if 'from' date is empty.  Lets assume nothing existed before 01/01/1970!!!
                ($_GET['from'] == "--" && $_GET['to'] != "--") ? $s -> btw("date", '1970-01-01', $_GET['to']) : '';

                $s -> add_order("date", "DESC");

                // No entries found, let user know
                if ($s -> count() == 0) {
                    echo "<tr><td>No entries found using applied filters</td></tr>";
                }
                // Entries found, display results to user
                while ($x = $s -> next(MYSQLI_ASSOC)) {
                    echo "<tr>
                    <td><input type='checkbox' id='entry[]' name='entry[]' value='$x->id'></td>
					<td>".$x->date."</td>
					<td>".$x->reference."</td>
					<td>".$x->amount."</td>
					<td>".$x->amountdr."</td>
					<td>".$x->category. $tba::getSageNC($x->category) ."</td>
					</tr>";
                }
            } catch (Exception $e) {
                die("Error creating file, contact IT");
            }

            break;

        case "category_management":
            $ac = new AccountsCategories;

            $json = [
                "status" => false,
                "error" => false,
                "data" => false
            ];

            if (isset($_GET['load_list'])) {
                try {
                    $s = new Search($ac);
                    $s->add_order("category", "ASC");

                    // Entries found, display results to user
                    while ($x = $s -> next(MYSQLI_ASSOC)) {
                        $json['data'] .= "<tr id='".$x->id()."'>
                            <td>".el::txt($x->id(), $x->category(), "style='width:85%;'")."</td>
                            <td>".el::txt($x->id(), $x->sage_nc(), "style='width:85%;'")."</td>
                            <td style='text-align:right;'><button id='".$x->id()."' class='update'>Update</button><button id='".$x->id()."' class='delete'>&nbsp;</button></td>
                            <td id='".$x->id()."' class='status'>&nbsp;</td>
					    </tr>";
                    }

                    if ($json['data'] == false) {
                        $json['error'] = true;
                        $json['status'] = "<tr><td>Could not return rows, please contact IT</td></tr>";
                    }
                } catch (Exception $e) {
                    $json['status'] = "Error listing categories, contact IT";
                    $json['error'] = true;
                    die("Error listing categories, contact IT");
                }

                echo json_encode($json);
            } else if (isset($_GET['add'])) {
                $ac->category($_GET['category']);
                $ac->sage_nc($_GET['sage_nc']);
                if (!$ac->save()) {
                    $json['status'] = "Error adding category, contact IT";
                    $json['error'] = true;
                } else {
                    $json['status'] = "Category successfully added";
                }

                echo json_encode($json);
            } else if (isset($_GET['delete'])) {
                $db = new mydb();
                if (!$db->query("DELETE FROM ".FCC_CATS." WHERE id = ".$_GET['id'])) {
                    $json['status'] = "Error deleting category, contact IT";
                    $json['error'] = true;
                }

                echo json_encode($json);
            } else if (isset($_GET['update'])) {
                $ac = new AccountsCategories($_GET['id'], true);
                $ac->category($_GET['category']);
                $ac->sage_nc($_GET['sage_nc']);
                if (!$ac->save()) {
                    $json['status'] = "Error updating category, contact IT";
                    $json['error'] = true;
                }

                echo json_encode($json);
            } else {
                echo Template("fcc/edit_categories.html", ["ac"=>$ac]);
            }

            break;
    }
} else if (isset($_GET["get"])) {
    switch (isset($_GET["get"])) {
        case "list_payments":
            try {
                $tba = new BankAccountAll;
                $s = new Search($tba);
                $s -> btw("date", date("Y-m-d", strtotime("-1 months")), date("Y-m-d"));
                $s -> add_order("date", "DESC");

                while ($x = $s -> next(MYSQLI_ASSOC)) {
                    echo "<tr>
                    <td><input type='checkbox' id='entry[]' name='entry[]' value='$x->id'></td>
					<td>".$x->date."</td>
					<td>".$x->reference."</td>
					<td>".$x->amount."</td>
					<td>".$x->amountdr."</td>
					<td>".$x->category . $tba::getSageNC($x->category)  ."</td>
					</tr>";
                }
            } catch (Exception $e) {
                die("Failed to load tblbankaccount entries, contact IT");
            }

            break;
    }
} else {
    echo Template("fcc/categorise.html");
}
