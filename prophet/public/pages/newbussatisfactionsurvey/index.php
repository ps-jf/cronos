<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

switch ($_GET["do"]) {
    case "save":
        if (($_GET['id'])!= null) {
            $db = new mydb();  // create a database connection
            $json = [];

            //check if signed off was changed
            $q = "SELECT signed_off, signed_off_when FROM feebase.client_satisfaction_survey
                  WHERE id = " . $_GET['id'];

            $oldSignedOff = 0;

            $db->query($q);

            if($result = $db->next(MYSQLI_ASSOC)){
                $oldSignedOff = $result['signed_off'];
                $signedOffTime = $result['signed_off_when'];
            }

            if ($oldSignedOff != $_GET['newbussatisfactionsurvey']['signed_off'] && $_GET['newbussatisfactionsurvey']['signed_off'] == 1){
                $signedOffTime = date("U");
            } elseif ($_GET['newbussatisfactionsurvey']['signed_off'] == 0){
                $signedOffTime = "NULL";
            }

            //Build update "query" using error and "status" JSON variables
            $q = "UPDATE feebase.client_satisfaction_survey 
                  SET user_id=".User::get_default_instance('id').",
                       g1=".$_GET['newbussatisfactionsurvey']['g1'].",
                       ta1=".$_GET['newbussatisfactionsurvey']['ta1'].",
                       ta2=".$_GET['newbussatisfactionsurvey']['ta2'].",
                       ta3=".$_GET['newbussatisfactionsurvey']['ta3'].",
                       ta4=".$_GET['newbussatisfactionsurvey']['ta4'].",
                       ta5=".$_GET['newbussatisfactionsurvey']['ta5'].",
                       ta6=".$_GET['newbussatisfactionsurvey']['ta6'].",
                       ta7=".$_GET['newbussatisfactionsurvey']['ta7'].",
                       ta8=".$_GET['newbussatisfactionsurvey']['ta8'].",
                       ta9='".addslashes($_GET['newbussatisfactionsurvey']['ta9'])."',
                       o1=".$_GET['newbussatisfactionsurvey']['o1'].",
                       o2=".$_GET['newbussatisfactionsurvey']['o2'].",
                      updated_when=".time().",
                      updatedby=".User::get_default_instance('id').", 
                      nbid=".$_GET['nbid'].", 
                      comments= '".addslashes($_GET['newbussatisfactionsurvey']['comments'])."',
                      signed_off=".$_GET['newbussatisfactionsurvey']['signed_off'].",
                      signed_off_when=".$signedOffTime.",
                      feedback='".$_GET['newbussatisfactionsurvey']['feedback']."'
                  WHERE id =".$_GET['id'];

            $db->query($q); // RUN QUERY
            $json["newID"] = $_GET['id'];
            echo json_encode($json);
        } else {
            $db = new mydb();
            $json = [];

            $nbsc = new NewBusSatisfactionSurvey();

            $nbsc->user_id(User::get_default_instance('id'));
            $nbsc->g1($_GET['newbussatisfactionsurvey']['g1']);
            $nbsc->ta1($_GET['newbussatisfactionsurvey']['ta1']);
            $nbsc->ta2($_GET['newbussatisfactionsurvey']['ta2']);
            $nbsc->ta3($_GET['newbussatisfactionsurvey']['ta3']);
            $nbsc->ta4($_GET['newbussatisfactionsurvey']['ta4']);
            $nbsc->ta5($_GET['newbussatisfactionsurvey']['ta5']);
            $nbsc->ta6($_GET['newbussatisfactionsurvey']['ta6']);
            $nbsc->ta7($_GET['newbussatisfactionsurvey']['ta7']);
            $nbsc->ta8($_GET['newbussatisfactionsurvey']['ta8']);
            $nbsc->ta9($_GET['newbussatisfactionsurvey']['ta9']);
            $nbsc->o1($_GET['newbussatisfactionsurvey']['o1']);
            $nbsc->o2($_GET['newbussatisfactionsurvey']['o2']);
            $nbsc->addedby(User::get_default_instance('id'));
            $nbsc->addedwhen(time());
            $nbsc->nbid($_GET['nbid']);
            $nbsc->comments($_GET['newbussatisfactionsurvey']['comments']);
            $nbsc->signed_off($_GET['newbussatisfactionsurvey']['signed_off']);
            if ($_GET['newbussatisfactionsurvey']['signed_off'] == 1){
                $nbsc->signed_off_when(date("U"));
            }
            $nbsc->feedback($_GET['newbussatisfactionsurvey']['feedback']);

            if ($nbsc->save()) {
                $s = new Search(new newbussatisfactionsurvey);
                $s->eq("nbid", $_GET['nbid']);
                $s->add_order("id", "desc");
                $s->set_limit("1");
                if ($d = $s -> next(MYSQLI_ASSOC)) {
                    $newID = $d->id();
                }
            }
            $json["newID"] = $newID;

            echo json_encode($json);
        }
        break;
}
