<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["filter"])) {
    /* Display search results for all new business cases matching the search terms */

    
    $s = new Search(new NewBusiness);

    $filters = $_GET["filter"];

    if (!empty($filters["nbid"])) {
        $s->eq("id", $filters["nbid"]);
    }

    $fd = Date::factory()
        ->set_var(Date::FORMAT, Date::ISO_8601);

    $ud = Date::factory()
        ->set_var(Date::FORMAT, Date::ISO_8601);


    if (!empty($filters["from_date"])) {
        $fd->set(implode("-", $filters["from_date"]) ." 00:00:00");
    }

    if (!empty($filters["until_date"])) {
        $ud->set(implode("-", $filters["until_date"]) ." 23:59:59");
    }



    //// Apply date filters based on parameters received
    $field = ( array_key_exists("field", $filters) && !empty($filters["field"]) ) ? $filters["field"] : "addedwhen";

    if ($fd() && $ud()) {
        // ... items within range
        $s->btw($field, $fd(), $ud());
    } elseif ($fd()) {
        // ... items after date
        $s->gt($field, $fd());
    } elseif ($ud()) {
        // ... items before date
        $s->lt($field, $ud());
    }

    if (!empty($filters["status"])) {
        // Apply case status filter
        $s->eq("status", $filters["status"]);
    }

    if (!empty($filters["registeredUser"])) {
        $s->eq("registeredUser", $filters["registeredUser"]);
    }

    if (!empty($filters["policy_type"])) {
        $s->eq("policy->type", $filters["policy_type"]);
    }

    if (!empty($filters["nb_brand"])) {
        $s->eq("nb_brand", $filters["nb_brand"]);
    }

    if (!empty($filters["type"])) {
        $s->eq("type", $filters["type"]);
    }

    if (!empty($_GET['policy']['issuer'])) {
        $s->eq("policy->issuer", $_GET['policy']['issuer']);
    }

    if (!empty($_GET['client']['partner'])) {
        $s->eq("policy->client->partner", $_GET['client']['partner']);
    }

    if (isset($_GET["limit"]) && (int)$_GET["limit"] > 0) {
        $s->set_limit((int) $_GET["limit"]);
    } else {
        $s->set_limit(250);
    }
    
    if (isset($_GET["skip"]) && (int)$_GET["skip"] > 0) {
        $s->set_offset((int) $_GET["skip"]);
    }

    $t = NewBusiness::get_template('search-result.html');
    while ($nb = $s->next()) {
        $t->tag($nb, "newbusiness");
        $t->tag($field, "date_field");
        echo $t;
    }

} elseif (isset($_GET['plugin'])) {
    switch ($_GET["do"]) {
        case "plugin":
            $db = new mydb();
            if ($_GET['object'] == "client") {
                $q = "SELECT * FROM ".NB_TBL." nb INNER JOIN ".POLICY_TBL.
                    " p ON nb.policyID = p.policyID WHERE p.clientID =  ".$_GET['id'];
            }if ($_GET['object'] == "policy") {
                $q = "SELECT * FROM ".NB_TBL." nb INNER JOIN ".POLICY_TBL."
                 p ON nb.policyID = p.policyID WHERE p.policyID =  ".$_GET['id'];
            } elseif ($_GET['object'] == "partner") {
                $q = "SELECT * FROM ".NB_TBL."
							INNER JOIN ".NBTYPE_TBL." ON ".NBTYPE_TBL.".id = ".NB_TBL.".newBusinessType
							INNER JOIN ".NBSTATUS_TBL." ON ".NBSTATUS_TBL.".id = ".NB_TBL.".status
							INNER JOIN ".POLICY_TBL." ON ".POLICY_TBL.".policyID = ".NB_TBL.".policyID
							INNER JOIN ".CLIENT_TBL." ON ".POLICY_TBL.".clientID = ".CLIENT_TBL.".clientID
							INNER JOIN ".PARTNER_TBL." ON ".CLIENT_TBL.".partnerID = ".PARTNER_TBL.".partnerID
							INNER JOIN ".USR_TBL." ON ".NB_TBL.".registeredUser = ".USR_TBL.".id
						WHERE ".PARTNER_TBL.".partnerID = ".$_GET["id"];
            }
            $db->query($q);

            $nb = [];
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $nb = new NewBusiness($row['nbID'], true);
                $c = new Client($row["clientID"], true);

                if ($_GET['object'] == "partner") {
                    $client_td = "<td>".$c->link($c->name(), $c->id($row["clientID"]))."</td>";
                }
                echo "<tr id='".$row['nbID']."'>
						<td>".$nb->link()."</td>
						<td>".$nb->type."</td>
						<td>".$nb->status."</a></td>
						". $client_td ."
						<td>".$nb->registeredUser."</td>
						<td>".$nb->typeOfNB."</td>
						<td>".$nb->addedwhen."</td>
						<td>".$nb->get_latest_action()."</td>
					</tr>";
            }

            break;
    }
} elseif (isset($_GET['tracking']) && $_GET['tracking'] == true) {
    $json = [
        "status" => null,
        "error" => null,
        "data" => null
    ];

    try {
        $db = new mydb();

        //userIDs for Alan, Grant, Kayleigh
        $adviser_array= [22, 109, 60];

        foreach ($adviser_array as $adviser) {
            //query for checking if an adviser has any NB tickets with the specified statuses
            $q = "SELECT Distinct registeredUser ".
            "FROM tblnewbusiness n ".
            "INNER JOIN tblnewbusinessstatus nbs ".
            "ON n.status = nbs.ID ".
            "WHERE status IN (9,10,11,12,13,14)";

            $db->query($q);
            //add users returned to array
            while ($u = $db->next(MYSQLI_ASSOC)) {
                $user[] = $u['registeredUser'];
            }

            //query for data displayed on table
            $q2 = "select registeredUser, nb_brand, ".
            "sum(if(status = 9, 1, 0)) as `Research`, ".
            "sum(if(status = 10, 1, 0)) as `On Hold`, ".
            "sum(if(status = 11, 1, 0)) as `Report Issued to Client`, ".
            "sum(if(status = 12, 1, 0)) as `At Provider`, ".
            "sum(if(status = 13, 1, 0)) as `Complete - Awaiting Satisfaction Survey`, ".
            "sum(if(status = 14, 1, 0)) as `Case Closed` ".
            "from tblnewbusiness ".
            "where registereduser = " . $adviser . " ".
            "and tblnewbusiness.status in (9,10,11,12,13,14) ".
            "group by nb_brand";

            $db->query($q2);

            //display user name as a title row above the brand rows for each adviser
            if (in_array($adviser, $user)) {
                $user_obj = new User($adviser, true);
                $json['data'] .= "<tr><td colspan='9'><b>".$user_obj->forename."
                 ".$user_obj->surname."</b></td></tr>";
            }

            while ($n = $db->next(MYSQLI_ASSOC)) {
                //calculate total
                $total = $n['Research'] + $n['On Hold'] + $n['Report Issued to Client'] + $n['At Provider'] +
                    $n['Complete - Awaiting Satisfaction Survey'] + $n['Case Closed'];

                $json['data'] .= "<tr>".
                    "<td width='140px'>" . $n['nb_brand'] . "</td>".
                    "<td><span data-brand='".$n['nb_brand']."' 
                    data-user='".$n['registeredUser']."' 
                    data-status='9' >" . $n['Research'] . "</span></td>".
                    "<td><span data-brand='".$n['nb_brand']."' 
                    data-user='".$n['registeredUser']."' 
                    data-status='10' >" . $n['On Hold'] . "</span></td>".
                    "<td><span data-brand='".$n['nb_brand']."' 
                    data-user='".$n['registeredUser']."' 
                    data-status='11' >" . $n['Report Issued to Client'] . "</span></td>".
                    "<td><span data-brand='".$n['nb_brand']."' 
                    data-user='".$n['registeredUser']."' 
                    data-status='12' >" . $n['At Provider'] . "</span></td>".
                    "<td><span data-brand='".$n['nb_brand']."' 
                    data-user='".$n['registeredUser']."' 
                    data-status='13' >" . $n['Complete - Awaiting Satisfaction Survey'] . "</span></td>".
                    "<td><span data-brand='".$n['nb_brand']."' 
                    data-user='".$n['registeredUser']."' 
                    data-status='14' >" . $n['Case Closed'] . "</span></td>".
                    "<td><b>" . $total . "</b></td></tr>";
            }
        }
    } catch (Exception $e) {
        $json['status'] = false;
        $json["error"] = $e -> getMessage();
    }

    echo json_encode($json);
} elseif (isset($_GET['do'])) {
    switch ($_GET["do"])
    {
        case "risk_reason":

            //generate product and client risk reason dropdowns

            $json = (object)[
                "id"     => false,
                "status" => false,
                "error"  => false,
                "data"   => false
            ];

            if (isset($_GET['clientrisk'])) {
                if ($_GET['clientrisk'] == 1) {
                    $json->data = NewBusiness::client_risk_reason(1);
                } else {
                    if ($_GET['clientrisk'] == 2) {
                        $json->data = NewBusiness::client_risk_reason(2);
                    } else {
                        if ($_GET['clientrisk'] == 3) {
                            $json->data = NewBusiness::client_risk_reason(3);
                        }
                    }
                }
            } else {
                if (isset($_GET['productrisk'])) {
                    if ($_GET['productrisk'] == 1) {
                        $json->data = NewBusiness::product_risk_reason(1);
                    } else {
                        if ($_GET['productrisk'] == 2) {
                            $json->data = NewBusiness::product_risk_reason(2);
                        } else {
                            if ($_GET['productrisk'] == 3) {
                                $json->data = NewBusiness::product_risk_reason(3);
                            }
                        }
                    }
                }
            }

            echo json_encode($json);

            break;

        case "check_notes":
            $nb = new NewBusiness($_GET['id'], true);

            $response = [
                "success" => $nb->checkforNote($_GET['type']),
            ];

            echo json_encode($response);

            break;

    }


}elseif (isset($_GET['tracking_list']) && $_GET['tracking_list'] == true) {
    $s = new Search(new NewBusiness);
    $s->eq("registeredUser", $_GET['user']);
    $s->eq("nb_brand", $_GET['brand']);
    $s->eq("status", $_GET['status']);

    while ($t = $s->next(MYSQLI_ASSOC)) {
        $nbID[] = $t->id();
    }

    echo NewBusiness::get_template("tracking_list.html", ["nbID"=>$nbID]);
} elseif (isset($_GET['checklist']) && $_GET['checklist'] == true) {
    //Gets the basic form via jquery .load() function on the newbusiness profile
    if ($_GET['do'] == "getform") {
        //Search for checlist via the nbid

        $s = new Search(new NewBusinessChecklist);
        $s -> eq('nbID', $_GET['id']);
        //If found get object and load onto page
        if ($checklist = $s->next(MYSQLI_ASSOC)) {
            $new = $checklist->get();
        } else {
            //Else create a new object
            $new = new NewBusinessChecklist();
        }
        //Pass the checklist object and nbid along with the checklist template to the #checklist div/tab
          echo  Template(
              NewBusiness::get_template_path("checklist.html"),
              ["id"=>$_GET['id'],"checklist"=>$new]
          );
    } elseif ($_GET['do'] == "tickaction") {
        //Quite a lot of json variables here - most for fancy feedback widgets
        $json = [
            "status" => false,
            "error" => false,
            "elementUser" => null,
            "elementTime" => null,
            "elementUserContent" => null,
            "elementTimeContent" => null,
            "returnmsg" => null
        ];

        //Search for a checlist via the nbid
        $s = new Search(new NewBusinessChecklist);
        $s -> eq('nbID', $_GET['object_id']);
        if ($checklist = $s->next(MYSQLI_ASSOC)) {
            //get the newbus checklist object
            $cs = $checklist->get();
        } else {
            //create a new checklist and assign the nbid
            $cs = new NewBusinessChecklist();
            $cs->nbid($_GET['object_id']);
        }

        //Grab the element and dissect out the object magic method name
        $field = str_replace("]", "", str_replace("newbusinesschecklist[", "", $_GET['element']));
        //Create variable names by appending onto the field variable
        $user = $field . "_addedby";
        $timestamp = $field . "_addedwhen";
        //Check button value
        $value = $_GET['value'];

        //If value is 0 - set the fields to null
        if ($value == 0) {
            $cs->$user('NULL');
            $cs->$timestamp('NULL');
        } else {
            //Otherwise set the values to the person who added it and the timestamp of when
            $cs->$user($_COOKIE['uid']);
            $cs->$timestamp(time());
        }

        //Always save the value of the check button
        $cs->$field($value);

        //if the object save is successful
        if ($cs->save()) {
            //if the save if successful but the save was a delete - return the appropriate messages
            if ($value == "0") {
                $json["error"] = false;
                $json["returnmsg"] = 1;
                $json["status"] = "Checklist updated.";
                $json["elementUser"] = "#" . $user;
                $json["elementTime"] = "#" . $timestamp;

                $json["elementUserContent"] = 'Removed';
                $json["elementTimeContent"] = 'Removed';
            } else {
                //else the save if successful but the save was not a delete - return the appropriate messages
                $json["error"] = false;
                $json["returnmsg"] = 0;
                $json["status"] = "Checklist updated.";
                $json["elementUser"] = "#" . $user;
                $json["elementTime"] = "#" . $timestamp;

                $json["elementUserContent"] = 'Completed';
                $json["elementTimeContent"] = 'Completed';
            }
        } else {
            $json["error"] = true;
            $json["status"] = "Checklist not updated.";
        }


        echo json_encode($json);
    }
} else {
    $nb = new NewBusiness;
    $vars = [];

    // Dropdown of available Date columns
    $options = [];
    foreach ($nb as $name => $property) {
        if (is_a($property, "Date")) {
            $options[$name] = $property->display_name();
        }
    }
    ksort($options);
    $vars["date_field"] = el::dropdown("filter[field]", $options);

    // Dropdown of Policy types
    $vars["newbusiness"] = $nb;

    echo NewBusiness::get_template("search.html", $vars);
}
