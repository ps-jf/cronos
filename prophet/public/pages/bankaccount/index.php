<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET["get"])) {
    switch ($_GET["get"]) {
        case "bankentries":
            //get the bank entries
            $bankaccount = new BankAccount;
            $s = new Search($bankaccount);
            $s->eq("date", $_GET["mnth"]."%", true, true);

            while ($ba = $s->next()) {
                echo new Template($bankaccount->get_template_path("list_row.html"), ["ba"=>$ba]);
            }

            break;
    }
} elseif (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "statement_received":
            if (isset($_GET['statement_received'])) {
                $json = [
                    "status" => false,
                    "error" => false,
                ];

                $bankaccount = new BankAccount($_GET['id'], true);
                $bankaccount->statement_received($_GET['statement_received']);
                if ($bankaccount->save()) {
                    $json['status'] = "Entry updated.";
                } else {
                    $json['error'] = "Entry did not update.";
                }

                echo json_encode($json);
            }


            break;
        
        case "assign":
            if (isset($_POST["bankaccount"])) {
                $json = [
                    "status" => false,
                    "error"  => false
                ];

                //this is quite a horrible way of doing this, but it replicates the behaviour(?) of access(!) quite well
                //here we loop through the assignments and update the table
                $db = new mydb();

                $errors = [];

                //we have a list of assignees, loop through them
                foreach ($_POST["bankaccount"] as $k => $v) {
                    if (is_numeric($v)) {
                        //update bank account entry with new assigneee
                        try {
                            $db->query(
                                "UPDATE ".BANK_TBL." SET assigned_to = ".$v." WHERE id = ".$k
                            );
                        } catch (Exception $e) {
                            $errors[] = $e->getMessage();
                        }
                    } else {
                        $errors[] = "Must supply a user id to assign to";
                    }
                }

                //return control to the browser
                if (count($errors) > 0) {
                    $json["error"] = $errors;
                } else {
                    $json["status"] = true;
                }

                echo json_encode($json);
            } else {
                echo new Template("bankaccount/assign.html");
            }

            break;
    }
} else {

    function bank_acc_search_date(&$search, $str)
    {
        list($d,$m,$y) = explode("/", $str["text"]);
        $dt = "$y-$m-$d";
        $search->eq("date", $dt, false, true);
    }

    $bankaccount = new BankAccount;
    $search = new Search($bankaccount);
    $search->calc_rows = true;

    $search
        -> flag("id")
        -> flag("date", Search::CUSTOM_FUNC, "bank_acc_search_date")
        -> flag("amount", Search::DEFAULT_INT)
        -> flag("reference", Search::DEFAULT_STRING);

    if (isset($_GET["search"])) {
        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);
        $search->add_order("date", "desc");

        $matches = [];

        while ($match = $search->next($_GET["search"])) {
            $matches["results"][] = ["text" => "$match->date $match->reference ".str_replace("&pound;", "£", "$match->amount"), "value" => "$match->id"];
        }

        $matches["remaining"] = $search->remaining;

        echo json_encode($matches);
    } else {
        echo new Template("generic/search.html", ["help"=>$search->help_html(),"object"=>$bankaccount]);
    }
}
