<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use SparkPost\SparkPost;

if (isset($_GET["do"])) {
    switch ($_GET['do']) {
        //Send a simple email test to a specified recipient email and name.
        case "sendEmailTest":
            // create a json array - put any variables assigned
            $json = [
                "test_data" => null
            ];

            $receiver = ['name' => $_GET['recipient_email'], 'email' => $_GET['recipient_email']];
            $sender = ['name' => 'Policy Services', 'email' => 'partnersupport@policyservices.co.uk'];
            $templateHtml = file_get_contents(TEMPLATE_DIR . "/sparkpost/emailTemplates/7/marketing1.html");
            $templateText = file_get_contents(TEMPLATE_DIR . "/sparkpost/emailTemplates/test1.txt");
            $substitution = ['subject' => 'Test email?', 'person' => 'Test person'];
            $campaign_name = 'Test_email';

            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new Sparkpost($httpClient, ['key' => SPARKPOST_API_KEY]);

            $promise = $sparky->transmissions->post([
                "options" => [
                    "ip_pool" => DEDICATED_MAILER_IP
                ],
                'campaign_id' => $campaign_name,
                'content' => [
                    'from' => [
                        'name' => $sender['name'],
                        'email' => $sender['email'],
                    ],
                    'subject' => '{{subject}}',
                    'html' => $templateHtml,
                    'text' => $templateText,
                ],
                'substitution_data' => $substitution,
                'recipients' => [
                    [
                        'address' => [
                            'name' => $receiver['name'],
                            'email' => $receiver['email'],
                        ],
                    ],
                ],
            ]);
            try {
                $response = $promise->wait();
                $statusMessage .= $response->getStatusCode() . "\n";
            } catch (Exception $e) {
                $statusMessage .= $e->getCode() . "\n";
                $statusMessage .= $e->getMessage() . "\n";
            }

            $json['test_data'] = $statusMessage;

            echo json_encode($json);

            break;

        //ADMIN USE ONLY - Send a singular e-mail to a user defined email.
        //Perfect for when an e-mail address was left out of the recipient csv for whatever reason.
        case "sendSingleEmail":
            // create a json array - put any variables assigned
            $json = [
                "status" => null
            ];

            $campaign_name = $_GET['campaign_name'];
            $templateName = $_GET['template_name'];

            $recipientName = $_GET['recipient_name'];
            $recipientEmail = $_GET['recipient_email'];
            $returnAddress = $_GET['return_email'];
            $subby = $_GET['subData'];

            $subst = json_decode($subby);
            $array = (array)$subst;
            $substitution = $array;

            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new Sparkpost($httpClient, ['key' => SPARKPOST_API_KEY]);

            //Send the email request to sparkpost
            $promise = $sparky->transmissions->post([
                "options" => [
                    "ip_pool" => DEDICATED_MAILER_IP
                ],
                "campaign_id" => $campaign_name,
                "content" => [
                    "template_id" => $templateName
                ],
                "substitution_data" => $substitution,
                "recipients" => [
                    [
                        "address" => [
                            "name" => $recipientName,
                            "email" => $recipientEmail,
                        ],
                    ],
                ],

            ]);

            try {
                $response = $promise->wait();
                $jsonStatus = $response->getBody();
            } catch (Exception $e) {
                echo $e->getCode() . "\n";
                echo $e->getMessage() . "\n";

                return;
            }

            //Build up strings to return feedback to prophet
            $status = "Status Code:" . $response->getStatusCode() . "\n";
            $rejectedmessage = "The total number of addresses rejected is: " . $jsonStatus['results']['total_rejected_recipients'] . "\n";
            $acceptedmessage = "The total number of addresses accepted is: " . $jsonStatus['results']['total_accepted_recipients'];
            $ID = $jsonStatus['results']['id'];
            $messageID = "ID: " . $ID;

            $statusMessage = $status . "\n" .
                $messageID . "\n" .
                $acceptedmessage . "\n" .
                $rejectedmessage . "\n";

            $json['status'] = $statusMessage;

            echo json_encode($json);

            break;

        case "sendEmailAsset":
            // create a json array - put any variables assigned
            $json = [
                "status" => null,
                "campaign_id" => null,
                "error" => null
            ];

            //Assign variables
            $campaign_name = $_GET['campaign_name'];
            $templateName = $_GET['template_name'];
            $recipientListName = $_GET['recipient_list'];

            //Create connection to sparkpost
            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new Sparkpost($httpClient, ['key' => SPARKPOST_API_KEY]);

            #Todo - If recipient list is larger than 20,000 emails.
            #Todo - All emails after 20,000 will be rejected unless account is on paid plan
            // Get the recipient list data
            $promise = $sparky->request('GET', 'recipient-lists/' . $recipientListName, [
                "ip_pool" => "ps_mailer",
                "show_recipients" => true
            ]);

            try {
                $response = $promise->wait();
                $recipients_json = $response->getBody();
            } catch (Exception $e) {
                echo $e->getCode() . "<br>";
                echo $e->getMessage() . "<br>";
            }

            //Send the email request to sparkpost
            $promise = $sparky->transmissions->post([
                'options' => [
                    'ip_pool' => DEDICATED_MAILER_IP
                ],
                'campaign_id' => $campaign_name,
                'content' => [
                    'template_id' => $templateName
                ],
                'recipients' => $recipients_json['results']['recipients']
            ]);

            try {
                $response = $promise->wait();
                $jsonStatus = $response->getBody();
            } catch (Exception $e) {
                echo $e->getCode() . "<br>";
                echo $e->getMessage() . "<br>";
            }

            if ($response->getStatusCode() == "200") { //everything went fine
                $json['error'] = false;
            } else {
                $json['error'] = true;
            }

            //Build up strings to return feedback to prophet
            $status = "Status Code:" . $response->getStatusCode() . "<br>";
            $rejectedmessage = "The total number of addresses rejected is: " . $jsonStatus['results']['total_rejected_recipients'] . "<br>";
            $acceptedmessage = "The total number of addresses accepted is: " . $jsonStatus['results']['total_accepted_recipients'];
            $ID = $jsonStatus['results']['id'];
            $messageID = "ID: " . $ID;
            $messagename = "Name: " . $jsonStatus['results']['name'];
            $messagedesc = "Description: " . $jsonStatus['results']['description'] . "<br>";

            $messageRecipients = "";
            foreach ($recipients_json['results']['recipients'] as $entry) {
                $messageRecipients .= $entry['address']['email'] . ", ";
            }
            $emailList = "Addresses successfully sent to: <br> " . $messageRecipients;

            //echo variables back to controller
            $statusMessage = $status . "<br>" .
                $messageID . "<br>" .
                $messagename . "<br>" .
                $messagedesc . "<br>" .
                $acceptedmessage . "<br>" .
                $rejectedmessage . "<br>" .
                $emailList;

            $json['status'] = $statusMessage;

            echo json_encode($json);
            break;

        // Sends a request to the sparkpost api endpoint to return the desired template and then display accordingly.
        case "getTemplatePreview":
            // create a json array - put any variables assigned
            $json = [
                "template_data" => null,
                "template_subject" => null,
                "template_description" => null,
                "template_last_updated" => null,
                "template_last_use" => null
            ];

            $templateName = $_GET['template_name'];

            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new SparkPost($httpClient, ['key' => SPARKPOST_API_KEY]);

            // Get the template data
            $promise = $sparky->request('GET', 'templates/' . $templateName, [
                "ip_pool" => "ps_mailer",
                "draft" => true
            ]);
            try {
                $response = $promise->wait();
                $templatePreview = $response->getBody();
            } catch (Exception $e) {
                echo $e->getCode() . "\n";
                echo $e->getMessage() . "\n";
            }

            $json['template_data'] = $templatePreview['results']['content']['html'];
            $json['template_subject'] = $templatePreview['results']['content']['subject'];
            $json['template_id'] = $templatePreview['results']['id'];
            $json['template_description'] = $templatePreview['results']['description'];
            $json['template_last_updated'] = date('"D M j G:i:s T Y"', strtotime($templatePreview['results']['last_update_time']));
            $json['template_last_use'] = date('"D M j G:i:s Y"', strtotime($templatePreview['results']['last_use']));

            echo json_encode($json);
            break;

        // A sort of work around of grabbing a recipient list fromt the sparkpost server
        case "getRecipientList":
            // create a json array - put any variables assigned
            $json = [
                "list_data" => null
            ];

            $recipientListName = $_GET['list_name'];

            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new SparkPost($httpClient, ['key' => SPARKPOST_API_KEY]);

            // Get the recipient list data
            // Get the recipient list data
            $promise = $sparky->request('GET', 'recipient-lists/' . $recipientListName, [
                "ip_pool" => "ps_mailer",
                "show_recipients" => true
            ]);
            try {
                $response = $promise->wait();
                $listPreview = $response->getBody();
            } catch (Exception $e) {
                echo $e->getCode() . "\n";
                echo $e->getMessage() . "\n";
            }

            $massive = "\n";
            foreach ($listPreview['results']['recipients'] as $list) {
                $massive .= $list['address']['email'] . "\n";
            }

            $json['list_data'] = $massive;

            echo json_encode($json);
            break;


        //Sends a request to the sparkpost API asking for all the message events of the past week.
        case "getMessageEvents":
            // create a json array - put any variables assigned
            $json = [
                "message_data" => null
            ];

            $messageEvents = $_GET['campaign_name'];

            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new SparkPost($httpClient, ['key' => SPARKPOST_API_KEY]);

            // Get the recipient list data
            // Get the recipient list data
            $promise = $sparky->request('GET', 'events/message', [
                "ip_pool" => "ps_mailer",
                "campaign_id" => $messageEvents
            ]);
            try {
                $response = $promise->wait();
                $messageEventList = $response->getBody();
            } catch (Exception $e) {
                echo $e->getCode() . "\n";
                echo $e->getMessage() . "\n";
            }

            foreach ($messageEventList['results'] as $list) {
                $massive .= "<tr>
                                    <td style='width:100px;'>" . $list['type'] . "</td>
                                    <td>" . $list['rcpt_to'] . "<td/>
                                    <td >" . $list['template_id'] . "</td>
                                    <td style='width:300px;'>" . date('M j G:i:s Y', strtotime($list['timestamp'])) . "</td>
                                    <td  style='width:100px;'>" . $list['campaign_id'] . "</td>
                                </tr>";
            }

            $json['message_data'] = $massive;

            echo json_encode($json);
            break;

        //Grabs the data required to pre-fill the drop downs in the sparkpost mailer tab
        case "getSparkpostData":
            // create a json array - put any variables assigned
            $json = [
                "status" => null,
                "campaign_id" => null,
                "template_name" => null,
                "recipient_list_id" => null,
                "error" => null
            ];

            //Setup sparkpost request wrapper
            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new SparkPost($httpClient, ['key' => SPARKPOST_API_KEY]);


            // Get the message events data
            $promise = $sparky->request('GET', 'message-events', [
                "ip_pool" => "ps_mailer"
            ]);

            try {
                $response = $promise->wait();

                $json['status'] = $response->getStatusCode() . "\n";
            } catch (Exception $e) {
                echo $e->getCode() . "\n";
                echo $e->getMessage() . "\n";
            }

            $msg_events_json = $response->getBody();

            // Get the templates list data
            $promise = $sparky->request('GET', 'templates', [
                "ip_pool" => "ps_mailer"
            ]);

            try {
                $response = $promise->wait();
                $json['status'] = $response->getStatusCode() . "\n";
            } catch (Exception $e) {
                echo $e->getCode() . "\n";
                echo $e->getMessage() . "\n";
            }

            $templates_json = $response->getBody();

            // Get the recipient list data
            $promise = $sparky->request('GET', 'recipient-lists', [
                "ip_pool" => "ps_mailer"
            ]);

            try {
                $response = $promise->wait();
                $json['status'] = $response->getStatusCode() . "\n";
            } catch (Exception $e) {
                echo $e->getCode() . "\n";
                echo $e->getMessage() . "\n";
            }

            $recipients_json = $response->getBody();

            $campaign_select = [0 => "--"];
            $templates_select = [0 => "--"];
            $recipient_select = [0 => "--"];

            foreach ($msg_events_json['results'] as $entry) {
                array_push($campaign_select, $entry['campaign_id']);
            }

            $resultCampaign = array_unique($campaign_select);

            foreach ($resultCampaign as $key => $value) {
                $massive .= "<option value=" . $key . ">" . $value . "</option>";
            }

            $finishedCampaignSelect = "<select class='form-control'>" . $massive . "</select>";

            foreach ($templates_json['results'] as $entry) {
                array_push($templates_select, $entry['id']);
            }

            foreach ($templates_select as $key => $value) {
                $massive1 .= "<option value=" . $key . ">" . $value . "</option>";
            }

            $finishedTemplateSelect = "<select class='form-control'>" . $massive1 . "</select>";

            foreach ($recipients_json['results'] as $entry) {
                array_push($recipient_select, $entry['id']);
            }

            foreach ($recipient_select as $key => $value) {
                $massive2 .= "<option value=" . $key . ">" . $value . "</option>";
            }

            $finishedRecipientSelect = "<select class='form-control'>" . $massive2 . "</select>";


            $json['campaign_id'] = $finishedCampaignSelect;
            $json['template_name'] = $finishedTemplateSelect;
            $json['recipient_list_id'] = $finishedRecipientSelect;

            echo json_encode($json);
            break;


        //Unused but handy to have for future projects.
        //Point to a local copy of a recipient list in CSV format and html email and watch the emails go.
        case "loopTestCsvList":
            //Build up variables to pass to the sparkpost call loop

            //Open recipient list and then map it to an array
            #TODO - Change mailing list path to network path accessible by all staff
            $mailingListPath = TEMPLATE_DIR . "/sparkpost/emailTemplates/recipientList/recipients.csv";

            //Determine the original sender - please note - sparkpost does not allow spoofing of sender
            $sender = ['name' => 'Policy Services', 'email' => 'jonathan.foley@policyservices.co.uk'];

            //PLEASE NOTE - BOTH VARIABLES ARE NEEDED TO CREATE A MIME BASED EMAIL
            //Get the email html code assign it to a variable
            //Get the email text and assign to a variable
            $templateHtml = file_get_contents(TEMPLATE_DIR . "/sparkpost/emailTemplates/7/marketing1.html");
            $templateText = file_get_contents(TEMPLATE_DIR . "/sparkpost/emailTemplates/test1.txt");
            $campaign_name = 'Test_email';
            //SPARKPOST Connection/request stuff
            $httpClient = new GuzzleAdapter(new GuzzleClient());
            $sparky = new Sparkpost($httpClient, ['key' => SPARKPOST_API_KEY]);
            $csv = array_map('str_getcsv', file($mailingListPath));

            $i = 0;
            foreach ($csv as $c) {
                if ($i != 0) {
                    //Setup substitute data -  encode into appropriate type
                    $substitution = [json_decode($c[4])];

                    $promise = $sparky->transmissions->post([
                        "options" => [
                            "ip_pool" => DEDICATED_MAILER_IP
                        ],
                        'campaign_id' => $campaign_name,
                        'content' => [
                            'from' => [
                                'name' => $sender['name'],
                                'email' => $sender['email'],
                            ],
                            'subject' => '{{subject}}',
                            'html' => $templateHtml,
                            'text' => $templateText,
                        ],
                        'substitution_data' => $substitution[0],
                        'recipients' => [
                            [
                                'address' => [
                                    'name' => $c[1],
                                    'email' => $c[0],
                                ],
                            ],
                        ],
                    ]);
                    try {
                        $response = $promise->wait();
                        echo $response->getStatusCode() . "\n";
                        print_r($response->getBody()) . "\n";
                    } catch (Exception $e) {
                        echo $e->getCode() . "\n";
                        echo $e->getMessage() . "\n";
                    }
                }
                $i++;
            }
            break;

        case "prompt_substitution_data":
            if (isset($_GET['odd_array'])) {
                // create a json array - put any variables assigned
                $json = [
                    "status" => null,
                    "error" => null
                ];

                $odd_array = $_GET['odd_array'];
                $even_array = $_GET['even_array'];
                $dynamic_key = $_GET['dynamic_key'];
                $dynamic_value = $_GET['dynamic_value'];

                $stack = [];
                $dynam_stack = [];

                $index = 0;
                $index1 = 0;
                foreach ($odd_array as $value) {
                    $stack[strtoupper($odd_array[$index])] = strtoupper($even_array[$index]);
                    $index++;
                }
                foreach ($dynamic_value as $dynam_val) {
                    $dynam_stack[strtoupper($dynamic_key[$index1])] = $dynamic_value[$index1];
                    $index1++;
                }
                $stack['dynamic_html'] = $dynam_stack;

                $codeme = json_encode($stack);

                $json['status'] = $codeme;

                echo json_encode($json);
            } else {
                echo new Template("sparkpost/substitution_popup.html");
            }
            break;
    }
} else {
    echo Template("sparkpost/index.html");
}
