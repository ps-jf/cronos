<?php

require_once( $_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php" );

if( isset( $_GET["id"] ) ) {
    $id = (int)$_GET["id"];

    if( isset( $_GET['get'] ) ) {
        switch( $_GET['get'] ) {
            case 'wf':
                // Get workflow history for this policy
                $m = new Search( new Workflow );
                $m -> eq( 'index', $id );
                $m -> eq( '_object', 'Policy' );

                $t = new Template( "workflow/row.html" );
                while( $item = $m -> next( ) ){
                    $t -> tag( $item, 'item' );
                    echo $t;
                }

            break;
        }
    } elseif (isset($_GET['do'])) {
        $policy = new Policy((int)$_GET["id"], true);

        /* Performing actions on a specific policy */

        switch ($_GET['do']) {

            case "viewSchemeMembers":
                $scheme_members = [];

                $sm_search = new Search(new SchemeMember());
                $sm_search->eq('scheme_number', $policy->id());
                while ($x = $sm_search->next(MYSQLI_ASSOC)) {
                    $scheme_members[] = $x;
                }

                echo new Template("client/scheme_members.html", ["scheme_members" => $scheme_members]);
                break;

            // List all 'live' policies
            case "list_only_live":
                $rows = "";
                # Filter only live policies to be shown
                $s = new Search (new Policy);
                $s->eq("clientID", $_GET['id']);
                $s->add_or([
                    $s->eq("status", 2, true, 0), # Servicing Transferred to PS
                    $s->eq("status", 5, true, 0), # Action Required
                    $s->eq("status", 9, true, 0), # Authority for Information Only
                    $s->eq("status", 18, true, 0) # Policy Paid Up
                ]);

                $x = 0;
                while ($p = $s->next(MYSQLI_ASSOC)) {
                    $x++;
                    $rows .= "<tr>";
                    $rows .= "<td class='merge_request'><input id=" . $p->id() . " type='checkbox'/></td>
                            <td>" . $p->link() . ($p->ar_count() ? " <i class=\"far fa-exclamation-circle text-danger\" title=\"Action Required\"></i>" : "") . ($p->hasIncomeHold($policy->id()) ? " <i class=\"text-danger\">(income held)</i>" : "") . "</td>
                            <td>" . $p->issuer->link() . "</td>
                            <td>" . $p->type . "</td>
                            <td style='width:225px'>" . $p->status . "</td>
                            <td>" . $p->getOwnerAttribute($p->owner()) . "</td>
                            <td>";

                    if ($wf = $p->outstanding_workflow('Policy')) {
                        $rows .= el::bt( "<img src=".el::image("wf.png")." alt='workf'>",false, "href='".$wf->get_common_url(array('id'=>$wf->id(),'get'=>"profile"))."' class='workflow'" );
                    }

                    if ( $nb = $p->has_newbusiness() ) {
                        $rows .= el::bt( "<img src=".el::image("newbus.png")." alt=nb>", false, "href='".NewBusiness::get_common_url(array('get'=>'profile','id'=>$nb->id()))."' class='new_business'" );
                    }

                    if ( $vr = $p->has_reviewTicket() ){
                        $rows .= el::bt( "<img src=".el::image("review_book.png")." alt='virtue'>", false, "href='/pages/virtue/index.php?do=add_review&id=$p->id' class='has_virtuereview'" );
                    }

                    if ($p->archived() != null) {
                        $rows .=   "<div class=\"tag\">Archived</div>";
                    }

                    $rows .= "</td>";
                    $rows .= "</tr>";
                }

                //Check whether the search returns any live policies or not.
                if ($x == 0) {
                    $rows .= "<tr>";
                    $rows .= "<td>No live policies available.</td>";
                    $rows .= "</tr>";
                }

                echo json_encode($rows);

                break;

            case "view_proreports":
            $rows = "";
            $corr_array =  [];

            // get all relevant corr items under policy
            $s = new Search (new Correspondence);
            $s->eq("policy", $policy->id());
            $s->add_or([
                $s->eq("subject", "Portfolio Valuation", true, 0),
                $s->eq("subject", "Portfolio Valuation - Elevate Only", true, 0),
                $s->eq("subject", "ProReport", true, 0),
                $s->eq("subject", "ProReport Amendment", true, 0),
                $s->eq("subject", "Portfolio Valuation Amendment", true, 0),
                $s->eq("subject", "Portfolio Valuation Amendment - PS", true, 0),
                $s->eq("subject", "Portfolio Valuation Amendment - Partner", true, 0),
                $s->eq("subject", "Portfolio Valuation Amendment - Provider", true, 0)
            ]);
            $s->add_order("date", "DESC");

            // add these items to an array
            while ($c = $s->next( MYSQLI_ASSOC  )) {
                $corr_array[] = $c->id();
            }

            // check if any of these corr items have a relating PR invoice entry
            foreach ($corr_array as $corr) {
                $s = new Search (new ProreportInvoice());
                $s->eq("corr", $corr);

                if ($p = $s->next( )) {
                    $rows .= "<tr><td>".$p->corr->link()."</td><td>". date("d/m/Y", $p->date())."</td><td>".$p->amount."</td><td>".$p->invoice_id."</td></tr>";
                } else {
                    $c = new Correspondence($corr, true);
                    $date = DateTime::createFromFormat("Y-m-d H:i:s", $c->date());
                    $rows .= "<tr><td>".$c->link()."</td><td>". $date->format("d/m/Y")."</td><td>&pound;6.99</td><td>&nbsp;</td></tr>";
                }
            }

            // provide some placeholder text if no entries found
            if ($rows == "") {
                $rows = "<tr><td colspan='3'>No entries to display</td></tr>";
            }

            echo $rows;

            break;

            case "proreport_delivery":
                // check that ongoing_fee column is filled in for all relevant policies
                if ($_GET['client']) {
                    $search = new Search(new Policy);
                    $search->nt('number', '%*%');
                    $search->eq('client', $_GET['client']);

                    while ($policy = $search->next()) {
                        // we only care about non-archived policies, Skip client agreement policies
                        if ((is_null($policy->archived()) && ($policy->issuer() != 11764))) {
                            if (is_null($policy->ongoing_fee())) {
                                $json['status'] = "Ensure ongoing fee is filled in for all policies";
                                $json['error'] = true;
//                                echo json_encode($json);
//                                exit();
                            }
                        }
                    }
                }

                $json = [];

                $steps = [41,193]; // Create Portfolio, Create Portfolio - Elevate

                if (!empty($_GET['subject']) && in_array($_GET['subject'], ["Portfolio Valuation Amendment - Partner", "Portfolio Valuation Amendment - PS", "Portfolio Valuation Amendment - Provider"])){
                    $steps = [188]; // Amendment
                }

                $db = new mydb(REMOTE);
                $q = "select * from ".REMOTE_SYS_DB.".valuation_requests 
                inner join feebase.workflow on valuation_requests.workflow_id = workflow.id
                where object_index = ".$_GET['client']." and step in (". implode(',', $steps).") order by workflow.due";
                $db->query($q);

                while ($report = $db->next(MYSQLI_ASSOC)) {
                    $json[$report['workflow_id']]['method'] = "dm_" . $report['delivery_method'];
                    $json[$report['workflow_id']]['option'] = "do_" . $report['delivery_option'];
                    $json[$report['workflow_id']]['workflow'] = "wf_".$report['workflow_id'];
                    $json[$report['workflow_id']]['analytics'] = $report['analytics'] ?? "none";
                }

                echo json_encode($json);

                break;

            case 'status':
                if (isset($_POST["status"])) {
                    $policy->get( );

                    $r = ["saved" => false, "applet_src"=> null];
                    
                    $policy->status($_POST["status"]); // update status
                    
                    if ($policy->save()) {
                        $r[ "saved" ] = true;
                        
                        if (isset($_POST["include-coversheet"])) {
                            // create a new item of correspondence
                            $c = new Correspondence($policy);
                            $c->automatically_update_status = false;

                            $c->load([
                                'subject' => "$policy->status",
                                'message' => Correspondence::DEFAULT_MESSAGE,
                                'automatically_update_status' => false,
                                'sender'  => $_USER->id()
                            ]);

                             if ($c->save()) {
                                $data = ["letter"=>Letter::byName("Coversheet")->id()];
                                $r[ "applet_src" ] = "".Template("correspondence/print.html", [ "data"=> $data,"item_id"=>$c->id()]);
                            }
                        }
                    }
                    echo json_encode( $r );
                } else {
                    $policy->get();
                    $t = new Template( "policy/set-status.html" );
                    $t -> tag( $policy, 'policy' );
                    echo $t;
                }

            break;
			
			case 'status-frontend':
				$policy->get();
				$t = new Template( "policy/set-status-fe.html" );
				$t->tag($policy, 'policy');
				echo $t;

            break;

            case 'fes_multiple_client_dedup':

                $json = [
                    "status" => false,
                    "error" => false,
                    "data" => false
                ];

                $data = "";

                $db = new mydb();
                $db->query("SELECT * FROM tblpolicy INNER JOIN tblclient ON tblpolicy.clientID = tblclient.clientID
                    WHERE partnerID = ".$_GET['id']." AND policyNum like '%".$_GET['number']."%'");

                while ($q = $db -> next( MYSQLI_ASSOC  )){
                    $p = new Policy($q['policyID'], true);

                    $data .= "<tr><td>".$p->link()."</td><td>".$p->issuer->link()."</td><td>".$p->client->link()."</td><td>".$p->client->partner->link()."</td></tr>";
                }
                
                $json['data'] = $data;


                echo json_encode($json);

                break;

            case "archive":
                $json = [
                    "status"    => false,
                    "error"     => false,
                    "timestamp" => false
                ];

                $reason = addslashes($_GET['reason']);

                // If reason isn't 20 characters wrong - return error
                if (strlen($reason) >= 1){
                    //create new database instance and then add reasoning to tbl_action_reason
                    $db = new mydb();
                    $q = "INSERT INTO
                         `feebase`.`tbl_action_reason` (`object`, `object_id`, `reason`, `created_by`, `created_when`,`action`)
                         VALUES ('Policy', '" . $_GET['id'] . "', '" . $reason . "', '" . $_COOKIE['uid'] . "', '" . time() . "', 'Archived')";

                    // If reason is added to the database - then run the rest of the archive
                    if ($db->query($q)){
                        try{
                            //archive policy

                            $timestamp = time();

                            $policy = new Policy($_GET['id'], true);
                            $policy->archived($timestamp);
                            if ($policy->save()) {
                                $json['status'] = "This Policy has been archived.";
                                $json['timestamp'] = date("d/m/Y", $timestamp);
                            } else {
                                $json['error'] = true;
                                $json['status'] = "An error occurred please contact IT";
                            }
                        } catch (Exception $e){
                            $json['error'] = $e->getMessage();
                        }
                    } else{
                        $json['error'] = true;
                        $json['status'] = "Reason did not add to database. Please contact IT.";
                    }
                }else{
                    $json['error'] = true;
                    $json['status'] = "Please fill in the reason box.";
                }

                echo json_encode($json);

            break;

            case "unarchive":
                $json = [
                    "status" => false,
                    "error" => false
                ];

                //create new database instance and then add reasoning to tbl_action_reason
                $db = new mydb();
                $q = "DELETE FROM `feebase`.`tbl_action_reason` WHERE action = 'Archived' and `object_id`='". $_GET['id']."' and object = 'Policy' ";

                //If reason is added to the database - then run the rest of the archive
                $db->query($q);

                try {
                        //unarchive policy

                        $policy = new Policy($_GET['id'], true);
                        $policy->archived('null');
                    if ($policy->save()) {
                            $json['status'] = "This Policy has now been unarchived.";
                    } else {
                            $json['error'] = true;
                            $json['status'] = "An error occurred please contact IT";
                        }
                } catch (Exception $e) {
                        $json['error'] = $e->getMessage();
                    }

                echo json_encode($json);

                break;

            case "get_archive_reason":
                $json = [
                    "status" => false,
                    "error" => false,
                    "timestamp" => false,
                ];

                // create new database instance and then add reasoning to tbl_action_reason
                $db = new mydb();
                $q = "Select * from feebase.tbl_action_reason where action = 'Archived' and object = 'Policy' and object_id = " . $_GET['id'];

                // If reason is added to the database - then run the rest of the archive
                $db->query($q);

                while ($archive = $db->next(MYSQLI_ASSOC)) {
                    $json['status'] = 'Archive reason: ' . $archive['reason'];
                }

                echo json_encode($json);


                break;

            case "owning_client":
                $json = [
                    "status" => false,
                    "error" => false
                ];

                try{
                    $policy = new Policy($_GET['id'], true);

                    $policy->owner($_GET['owner']);
                    if($policy->save()){
                        $json['status'] = '<img src="/lib/images/tick.png">';
                    }else{
                        $json['error'] = true;
                        $json['status'] = '<img src="/lib/images/cross.png">';
                    }
                }catch( Exception $e ) {
                    $json['error'] = $e->getMessage();
                }

                echo json_encode($json);


            break;

            case "get_link":
                $response = [
                    'success' => true,
                    'link' => null
                ];

                if ($policy->id()){
                    $response['link'] = $policy->link();
                } else {
                    $response['success'] = false;
                }

                echo json_encode($response);

                break;

            case "load_details":
                $response = [
                    'success' => true,
                    'policy' => []
                ];

                $response['policy']['id'] = $policy->id();
                $response['policy']['number'] = $policy->number();
                $response['policy']['type_id'] = $policy->type();
                $response['policy']['type'] = strval($policy->type);
                $response['policy']['owner'] = $policy->owner();

                echo json_encode($response);

                break;

            case "load_json":
                $p = $policy->flat_array();
                $p['partner'] = $policy->client->partner->id();

                echo json_encode($p);

                break;

            case "get_ar_ticket_links":
                $response = [
                    'success' => true,
                    'has_ar' => false,
                    'links' => "",
                ];

                $links = $policy->ar_links();

                if ($links){
                    $response['has_ar'] = true;
                    $response['links'] = $links;
                }

                echo json_encode($response);

                break;

            case "get_wf_ticket_links":
                $response = [
                    'success' => true,
                    'has_wf' => false,
                    'links' => "",
                ];

                $links = $policy->wf_links([16]);

                if ($links){
                    $response['has_wf'] = true;
                    $response['links'] = $links;
                }

                echo json_encode($response);

                break;

            case "delete_records":
                if (!empty($_GET['reason'])){
                    $response = [
                        "success" => true,
                        "data" => null,
                    ];

                    try{
                        // add an entry to action reason
                        $ar = new ActionReason();

                        $ar->object("Policy");
                        $ar->object_id($policy->id());
                        $ar->reason($_GET['reason']);
                        $ar->action("Delete");

                        if ($ar->save()){
                            $policy->deleteRecords();
                        } else {
                            throw new Exception("Failed to audit delete reason");
                        }
                    } catch (Exception $e) {
                        $response['success'] = false;
                        $response['data'] = "Failed to delete policy: " . $e->getMessage();
                    }

                    echo json_encode($response);
                } else {
                    echo new Template("policy/delete_records.html", compact("policy"));
                }

                break;
        }
    }
} elseif( isset( $_GET['do'] ) ){
    switch( $_GET['do'] ){
        case "bulkUpdate":
            if (isset($_GET['update'])){
                $response = [
                    "success" => true,
                    "data" => null
                ];

                $policies = json_decode($_GET['policies']);

                foreach($policies as $policyID){
                    $p = new Policy($policyID, true);

                    if (isset($_GET['status']) && $_GET['status'] != "null"){
                        $p->status($_GET['status']);
                    }

                    if (isset($_GET['type']) && $_GET['type'] != 0){
                        $p->type($_GET['type']);
                    }

                    if (isset($_GET['owner']) && $_GET['owner'] != "null"){
                        $p->owner($_GET['owner']);
                    }

                    try{
                        $p->save();

                        $ar = new ActionReason();

                        $ar->object("Policy");
                        $ar->object_id($p->id());
                        $ar->reason($_REQUEST["reason"]);
                        $ar->action("status");

                        $ar->save();
                    } catch (Exception $e){
                        $response['success'] = false;
                        $response['data'] = "Failed to save policy details.";
                    }
                }

                echo json_encode($response);
            } else {
                echo new Template("policy/bulk_update.html", ['policies' => json_decode($_GET['policies']), 'callbackFrame' => $_GET['callbackFrame']]);
            }

            break;

        case "existing_policy_check":
//            $client = new Client($_GET['client'], true);
//            $partner = new Client($_GET['partner'], true);
//            $issuer = new Client($_GET['issuer'], true);
//
            $response = [
                'success' => true,
                'policies' => [],
            ];

            $s = new Search(new Policy);

            $s->eq("number", "%" . $_GET['policy'] . "%", true);
            $s->nt("issuer", 11764);
//            $s->eq("client", $_GET['client']);
            $s->eq("archived", null);

            while($p = $s->next(MYSQLI_ASSOC)){
                $policy = [];

                $policy['id'] = $p->id();
                $policy['client'] = $p->client->link();
                $policy['client_id'] = $p->client();
                $policy['link'] = $p->link();
                $policy['issuer'] = $p->issuer->link();
                $policy['issuer_id'] = $p->issuer();
                $policy['mandate'] = $p->mandate();

                $response['policies'][] = $policy;
            }

            if(empty($response['policies'])){
                $response['success'] = false;
            }

            echo json_encode($response);

            break;

        case "merge_request":
            $policyIDs = json_decode($_GET['policies']);

            $policies = [];

            foreach ($policyIDs as $id){
                $policies[] = new Policy($id, true);
            }

            echo new Template("policy/merge.html", ["policies" => $policies]);

            break;

        case "get_owner_dropdown":
            $response = [
                'success' => true,
                'dropdown' => null
            ];

            $p = new Policy();
            $p->client($_GET['client']);

            if (isset($_GET['system'])){
                switch($_GET['system']){
                    case "mandate":
                        // return 5 dropdowns
                        $response['dropdown'] = [];

                        for ($i = 0; $i < 5; $i++) {
                            $response['dropdown'][] = $p->getOwner(false, "policy[".$i."][owner]", "JSON");
                        }

                        break;

                    case "default":
                        $response['dropdown'] = $p->getOwner(false, "policy[owner]", "JSON");

                        break;
                }
            } else {
                $response['dropdown'] = $p->getOwner(false, "policy[owner]", "JSON");
            }

            echo json_encode($response);

            break;

        case "save_scheme_member":
            $response = [
                'error' => false,
                'feedback' => ''
            ];

            $sm = new SchemeMember($_POST['member_id'], true);
            $sm->member_number($_POST['member_number']);
            $sm->member_name($_POST['member_name']);
            $sm->dob($_POST['dob']);
            $sm->nino($_POST['nino']);
            $sm->email($_POST['email']);
            $sm->active($_POST['active']);

            $sm->address->line1($_POST['address_line1']);
            $sm->address->line2($_POST['address_line2']);
            $sm->address->line3($_POST['address_line3']);
            $sm->address->postcode($_POST['address_postcode']);

            if ($sm->save()) {
                $response['feedback'] = "Scheme member details successfully updated";
            } else {
                $response['feedback'] = "Something went wrong, try again or contact IT.";
                $response['error'] = true;
            }

            echo json_encode($response);
            break;
    }
} else {
    /* Display search results for all policies matching $search */

    $start = microtime(true);

    $search = new Search( new Policy );

    /*** callback for whether policy has a mandate behind it or not ***/
    function policy_has_mandate(&$object, $text)
    {
        // assert boolean value supplied in search string
        if (!in_array(strtolower($text['text']), ["true","false"])) {
            return false;
        }
        
        $field = $object->locate_property("mandate_stored")->get_field_name( );
        $object->add_and( "LENGTH($field) ". (($text["text"]=="true") ? "> 0" : "= 0 OR $field IS NULL") );
    }

    $search
        -> flag( "number", Search::DEFAULT_STRING, Search::DEFAULT_INT )
        -> flag( "id" )
        -> flag( "partner", "client->partner" )
        -> flag( "issuer", "issuer" )
        -> flag( "has_mandate", Search::CUSTOM_FUNC, "policy_has_mandate" )
        -> flag( "type" )
        -> flag( "mandate" );

    if (isset($_GET["search"])) {
        if (isset($_GET["filter"]) && is_array($_GET["filter"])) {
            $search->apply_filters($_GET["filter"]);
        }

        $show_client = $show_partner = $show_type = true;

        // apply profile filters
        if (isset($_GET["client"])) {
            $show_client = false;
            $show_partner = false;

            $search->eq("client", (int)$_GET["client"]);
            $search->add_order("number", "ASC");
        }

        //if we want to restrict search to one specific issuer
        if (isset($_GET["issuer"])) {
            $search->eq("issuer", (int)$_GET["issuer"]);
        }

        if (strlen($_GET["search"]) < 3) {
            echo json_encode("Do not search for less than 3 characters");
            exit();
        }

        //type is suggest, then we need to build a new return type
        if (isset($_GET["suggest"])) {
            if ($_GET["suggest"] == 1) {
                /* hide archived policies from suggest, eg. commission->process->update unmatched,
                we wouldn't want to update a policy number to a policy that had already been archived */
                if (isset($_GET['archived']) && $_GET['archived'] == "hide") {
                    $search->eq("archived", null);
                }

                $search->set_limit(SEARCH_LIMIT);
                $search->set_offset((int)$_GET["skip"]);

                $matches = [];
                $i = 0;
                while ($match = $search->next($_GET["search"])) {
                    $matches["results"][] = ["text" => "$match->number" . " - " . "$match->client", "value" => "$match->id"];
                }
                $matches["remaining"] = $search->remaining;

                echo json_encode($matches);
                return;
            }
        }

        //MAIN SEARCH BAR DOES NOT SET SUGGEST, SO LOOK FOR LIMIT INSTEAD
        if (isset($_GET["limit"])) {
            if ($_GET["limit"] > 0) {
                /* hide archived policies from suggest, eg. commission->process->update unmatched,
                we wouldn't want to update a policy number to a policy that had already been archived */
                if (isset($_GET['archived']) && $_GET['archived'] == "hide") {
                    $search->eq("archived", null);
                }

                $search->set_limit($_GET['limit']);
                $search->set_offset((int)$_GET["skip"]);

                $matches = [];
                $i = 0;
                while ($match = $search->next($_GET["search"])) {
                    $matches["results"][] = ["text" => "$match->number" . " - " . "$match->client", "value" => "$match->id"];
                }
                $matches["remaining"] = $search->remaining;

                echo json_encode($matches);
                return;
            }
        }

        if (!isset($excludes)) {
            $excludes = [];
        }

        $t = Policy::get_template( 'list_row.html' );
        $i = 0;	
        while( $policy = $search->next($_GET["search"]) ) {
            $t -> tags([
                "excludes" => $excludes,
                "policy" => $policy,
                "client" => $policy->client,
                "issuer" => $policy->issuer,
                "partner" => $policy->client->get_object()->partner
            ]);
                
            echo "$t";
        }
    } else {
        echo new Template("policy/search.html", ["help"=>$search->help_html()]);
    }
}
