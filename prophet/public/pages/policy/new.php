<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

$policy = new Policy();

if (isset($_GET["submit"])) {
    //  Check for duplicate possibilities

    $m = new Search(new Policy);
    $m->eq("number", $_POST["number"] . "%", true);
    $m->eq("issuer", (int)$_POST["issuer"]);
    $m->execute();

    //  If matches found...
    if (!isset($_POST["ignore-dupes"]) && $m->remaining !== false) {
        //  Return a list of dupe-possibilites
        $list = [];

        while ($d = $m->next()) {
            $issuer = $d->issuer->get_object();
            $client = $d->client->get_object();
            $partner = $client->partner->get_object();

            $list[] = [
                $d->link(),
                $client->link(),
                $partner->link()
            ];
        }
    } // Otherwise save the new policy
    else {
        $policy->load($_POST);
        $policy->addedhow->set(1);          // Catches which system added
        $policy->status->set(2);            // "Servicing Transferred to PS" by default
        $policy->bulk->set(1);              // Bulk rate as default
        $policy->save();
    }

    echo json_encode([
        "list" => $list,
        "id" => $policy->id->value
    ]);

} elseif (isset($_REQUEST["client"])) {
    $t = new Template("policy/new.html");
    $t->tag($policy, 'policy');
    $t->tag(new Client((int)$_REQUEST["client"]), 'client');
    echo $t;
}
