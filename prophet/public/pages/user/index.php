<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

use Carbon\Carbon;

if (isset($_GET["id"])) {
    $user = new User($_GET["id"], true);

    if (isset($_GET['get'])){

        switch ($_GET['get']){

            case "groups":
                $response = [
                    "success" => true,
                    "data" => ""
                ];

                $s = new Search(new UsersGroups());

                $s->eq('user_id', $user->id());

                while($ug = $s->next(MYSQLI_ASSOC)){
                    $response['data'] .= "<tr data-ug-id='" . $ug->id() . "'>
                            <td>" . $ug->user_group . "</td>
                            <td><button class='btn btn-danger remove-user-group'><i class='fas fa-times'></i></button></td>
                        </tr>";
                }

                echo json_encode($response);

                break;
        }

    } else {
        // A User viewing their own profile can make changes
        if (User::get_default_instance()->id() == $user->id() && (!isset($_GET['do']) || (isset($_GET['do']) && $_GET['do'] != "img_upload"))) {
            if (isset($_GET["do"])) {
                switch($_GET['do']){
                    case "passwd":
                        $json = (object)["status" => false, "error" => false];

                        /* Change user password */
                        if (isset($_POST["current_passwd"], $_POST["new_passwd"], $_POST["confirm_passwd"])) {
                            try {
                                // Check that current password is correct

                                if (!password_verify($_POST['current_passwd'], $user->password())) {
                                    throw new Exception("Incorrect user password");
                                }

                                /*
                                  Impose the following requirements:
                                   - At least 7 characters
                                   - Must contain at least one number/symbol
                                   - Cannot contain username or email address
                                   - Cannot be found in the forbidden passwords file
                                */

                                $new_passwd = $user->validatePassword($_POST["new_passwd"], $_POST['confirm_passwd']);

                                $user->get();
                                $user->password->set($new_passwd);

                                $user->password_expired->set(0);

                                $json->status = (bool)$user->save();
                            } catch (Exception $e) {
                                $json->error = $e->getMessage();
                            }

                            echo json_encode($json);
                            exit;
                        }

                        break;

                    case "change_status":
                        if (isset($_GET['status'])) {
                            $json = [
                                "status" => null,
                                "error" => false
                            ];

                            $user->status($_GET['status']);
                            if ($user->save()) {
                                $json['status'] = "Status updated";
                            } else {
                                $json['error'] = true;
                                $json['status'] = "Status failed to update";
                            }

                            echo json_encode($json);
                        } else {
                            // get all status options from enum
                            $db = new mydb();
                            $db->query("SHOW COLUMNS FROM prophet.users WHERE Field = 'status'");

                            if ($status = $db->next(MYSQLI_ASSOC)) {
                                $statuses = $status['Type'];

                                // remove 'enum' and all brackets
                                preg_match('/enum\((.*)\)$/', $statuses, $matches);
                                $vals = explode(',', $matches[1]);

                                // create array with value, remove all quotes
                                $trimmedvals = [];
                                foreach ($vals as $key => $value) {
                                    $value = trim($value, "'");
                                    $trimmedvals[] = $value;
                                }

                                // build up select & remove a couple of options as we don't want users to manually pick these
                                $status_select = "<select id='status_select'>";
                                foreach ($trimmedvals as $option) {
                                    if ($option != "lunch" && $option != "offline" && $option != "idle") {
                                        $status_select .= "<option id='" . $option . "'>" . ucwords($option) . "</option>";
                                    }
                                }
                                $status_select .= "</select>";

                                // Display status change template
                                $user->get();

                                echo Template(
                                    User::get_template_path("status_change.html"),
                                    ["user" => $user, "status_select" => $status_select]
                                );
                            }
                        }

                        break;

                    case "recent":
                        //Display users recent activity
                        $user->get();

                        $recentViewed = $redisClient->lrange(User::get_default_instance("id").".recently_viewed", 0, -1);
                        $recentActivity = $redisClient->lrange(User::get_default_instance("id").".recent_activity", 0, -1);

                        echo Template(User::get_template_path("recent.html"), ["user" => $user, "recentViewed" => $recentViewed, "recentActivity" => $recentActivity]);

                        break;

                    case "password":
                        // Display password form
                        $user->get();
                        echo Template(User::get_template_path("change_password.html"), ["user" => $user]);

                        break;

                    case "timer":
                        try{
                            $json = [
                                "status" => null,
                                "error" => null,
                                "init_login_timer" => null,
                                "lunchTime" => 'No lunch logged yet',
                                "breakTime" => null,
                                "total_worked" => null,
                                "time_banked" => null
                            ];

                            $totalBreakTime = 0;
                            $breakTimeAllowed = 20*60; // 20 minutes in seconds

                            $totalLunchTime = 0;
                            $lunchTimeAllowed = date("D") === "Fri" ? 20*60 : 0;

                            $deductible = 0;

                            $now = Carbon::now();

                            function searchObject(){
                                $startOfDay = Carbon::now()->startOfDay();

                                $s = new Search(new LogLogin());

                                $s->eq("user", User::get_default_instance("id"));
                                $s->gt("datime", $startOfDay->format("U"));

                                return $s;
                            }

                            function readable($seconds){
                                $hours = floor($seconds/60/60);
                                $minutes = floor($seconds/60);

                                $diffMinutes = $minutes - $hours * 60;
                                $diffSeconds = $seconds - $diffMinutes * 60 - $hours * 60 * 60;

                                $string = "";

                                if ($hours > 0){
                                    $string .= $hours . " " . pluralize($hours, "hour") . " ";
                                }

                                if ($diffMinutes > 0){
                                    $string .= $diffMinutes . " " . pluralize($diffMinutes, "minute") . " ";
                                }

                                if ($diffSeconds > 0){
                                    $string .= $diffSeconds . " " . pluralize($diffSeconds, "second");
                                }

                                return $string;
                            }

                            // get the very first login of the day
                            $s = searchObject();
                            $s->limit(1);

                            if ($l = $s->next(MYSQLI_ASSOC)){
                                // we have our first login time of the day
                                $firstLogin = Carbon::createFromTimestamp($l->time());
                            } else {
                                throw new Exception("Failed to find any login times for today");
                            }

                            // get the first break logout of the day
                            $s2 = searchObject();
                            $s2->eq("direction", "BREAK");

                            while($l = $s2->next(MYSQLI_ASSOC)){
                                // for each break taken, get the time of the next login
                                $bID = $l->id();

                                $s3 = searchObject();

                                $s3->eq("direction", "LOGIN");
                                $s3->limit(1);
                                $s3->gt("id", $bID);

                                if ($l2 = $s3->next(MYSQLI_ASSOC)){
                                    $totalBreakTime += Carbon::createFromTimestamp($l2->time())->diffInSeconds(Carbon::createFromTimestamp($l->time()));
                                } else {
                                    throw new Exception("No logins found after break (ID: " . $bID . ")");
                                }
                            }

                            $s4 = searchObject();
                            $s4->eq("direction", "LUNCH");

                            while($l = $s4->next(MYSQLI_ASSOC)){
                                // for each lunch taken (should only be 1), get the time between the next login
                                $lID = $l->id();

                                $s5 = searchObject();
                                $s5->eq("direction", "LOGIN");
                                $s5->limit(1);
                                $s5->gt("id", $lID);

                                if ($l2 = $s5->next(MYSQLI_ASSOC)){
                                    $totalLunchTime += Carbon::createFromTimestamp($l2->time())->diffInSeconds(Carbon::createFromTimestamp($l->time()));
                                } else {
                                    throw new Exception("No logins found after lunch (ID: " . $lID . ")");
                                }
                            }

                            $json['init_login_timer'] = readable($now->diffInSeconds($firstLogin));
                            $json['breakTime'] = $totalBreakTime > 0 ? readable($totalBreakTime) : "No breaks taken";
                            $json['lunchTime'] = $totalLunchTime > 0 ? readable($totalLunchTime) : "No lunch taken";

                            if ($totalBreakTime > $breakTimeAllowed){
                                $deductible += $totalBreakTime - $breakTimeAllowed;

                                $json['breakTime'] .= " (" . readable($totalBreakTime - $breakTimeAllowed) . " deductible)";
                            }

                            if ($totalLunchTime > $lunchTimeAllowed){
                                $deductible += $totalLunchTime - $lunchTimeAllowed;
                            }

                            $timeSinceLogin = $now->diffInSeconds($firstLogin);
                            $timeWorked = $timeSinceLogin - $deductible;
                            $bankedSeconds = $timeWorked - 7*60*60; // 7 hours in seconds
                            $timeBanked = $bankedSeconds > 0 ?floor($bankedSeconds/(10*60))*10*60 : 0;

                            $json['total_worked'] = readable($timeWorked);
                            $json['time_banked'] = readable($timeBanked);

                            echo json_encode($json);

                        } catch (Exception $e) {
                            $json['status'] = $e;
                            $json['error'] = true;
                        }

                        break;

                    case "teamTimer":
                        $json = [
                            "acquisition" => false,
                            "commission" => false,
                            "clientservices" => false,
                            "clientrelations" => false,
                            "io" => false,
                            "newbusinessadmin" => false,
                            "newbusinessadvice" => false,
                            "it" => false,
                            "management" => false,
                            "virtue" => false,
                            "service_delivery" => false,
                            "compliance" => false,
                            "exec" => false,
                            "sats" => false
                        ];

                        /* This gets the start time, lunch time, and current time. This is a timer to tell the user how long they have worked.*/
                        $id = $_GET["id"];
                        $dept = $_GET["dept"];

                        // #todo we do not need to search for every department on every call, wrap in user->dept condition

                        $acquisition = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 4)
                            ->nt("id", 60)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $commission = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 1)
                            ->nt("id", 62)
                            ->nt("id", 63)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $compliance = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 2)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $clientservices = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 6)
                            ->eq("weekend_staff", 0)
                            ->add_order("team_leader", "DESC")
                            ->add_order("weekend_staff", "ASC")
                            ->add_order("username");

                        $weekendstaff = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("weekend_staff", 1)
                            ->add_order("team_leader", "DESC")
                            ->add_order("weekend_staff", "ASC")
                            ->add_order("username");

                        $clientrelations = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 15)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $io = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 14)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

//                    $newbusiness = Search(User::get_default_instance())
//                        ->eq("active", true)
//                        ->eq("department", 3)
//                        ->add_order("team_leader", "DESC")
//                        ->add_order("username");

                        $newbusinessadmin = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 3)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $newbusinessadvice = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 16)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $it = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 7)
                            ->nt("id", 23)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $exec = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("team_leader", 1)
                            ->nt("id", 23)
                            ->nt("id", 26)
                            ->add_order("username");

                        $virtue = Search(User::get_default_instance())
                            ->eq("active", true)
                            ->eq("department", 10)
                            ->add_order("team_leader", "DESC")
                            ->add_order("username");

                        $management = User::get_search()->eq("active", 1);
                        $management->add_or(
                            $management->eq("id", 29, false, false),
                            $management->eq("id", 45, false, false),
                            $management->eq("id", 56, false, false),
                            $management->eq("id", 87, false, false)
                        );
                        $management->add_order("username");

                        $service_delivery = User::get_search()->eq("active", 1);
                        $service_delivery->add_or(
                            $service_delivery->eq("id", 44, false, false),
                            $service_delivery->eq("id", 108, false, false),
                            $service_delivery->eq("id", 116, false, false),
                            $service_delivery->eq("id", 214, false, false)
                        );
                        $service_delivery->add_order("username");


                        while ($a = $acquisition->next('MYSQLi_ASSOC')) {
                            $json['acquisition'] .= "<tr><td>" . $a->link($a->forename() . " " . $a->surname()) . "</td><td>" . User::getUserTime($a->id()) . "</td> </tr>";
                        }

                        while ($compl = $compliance->next('MYSQLi_ASSOC')) {
                            $json['compliance'] .= "<tr><td>" . $compl->link($compl->forename() . " " . $compl->surname()) . "</td><td>" . User::getUserTime($compl->id()) . "</td></tr>";
                        }

                        while ($c = $commission->next('MYSQLi_ASSOC')) {
                            $json['commission'] .= "<tr><td>" . $c->link($c->forename() . " " . $c->surname()) . "</td><td>" . User::getUserTime($c->id()) . "</td> </tr>";
                        }

                        while ($v = $virtue->next('MYSQLi_ASSOC')) {
                            $json['virtue'] .= "<tr><td>" . $v->link($v->forename() . " " . $v->surname()) . "</td><td>" . User::getUserTime($v->id()) . "</td> </tr>";
                        }

                        while ($cs = $clientservices->next('MYSQLi_ASSOC')) {
                            $json['clientservices'] .= "<tr><td>" . $cs->link($cs->forename() . " " . $cs->surname()) . "</td><td>" . User::getUserTime($cs->id()) . "</td></tr>";
                        }

                        while ($ws = $weekendstaff->next('MYSQLi_ASSOC')) {
                            $json['sats'] .= "<tr><td>" . $ws->link($ws->forename() . " " . $ws->surname()) . "</td><td>" . User::getUserTime($ws->id()) . "</td></tr>";
                        }

                        while ($cr = $clientrelations->next('MYSQLi_ASSOC')) {
                            $json['clientrelations'] .= "<tr><td>" . $cr->link($cr->forename() . " " . $cr->surname()) . "</td><td>" . User::getUserTime($cr->id()) . "</td></tr>";
                        }

                        while ($i_o = $io->next('MYSQLi_ASSOC')) {
                            $json['io'] .= "<tr><td>" . $i_o->link($i_o->forename() . " " . $i_o->surname()) . "</td><td>" . User::getUserTime($i_o->id()) . "</td></tr>";
                        }

//                    while ($nb = $newbusiness->next('MYSQLi_ASSOC')) {
//                        $json['newbusiness'] .= "<tr><td>" . $nb->link($nb->forename() . " " . $nb->surname()) . "</td><td>" . User::getUserTime($nb->id()) . "</td></tr>";
//                    }

                        while ($nb = $newbusinessadmin->next('MYSQLi_ASSOC')) {
                            $json['newbusinessadmin'] .= "<tr><td>" . $nb->link($nb->forename() . " " . $nb->surname()) . "</td><td>" . User::getUserTime($nb->id()) . "</td></tr>";
                        }

                        while ($nb = $newbusinessadvice->next('MYSQLi_ASSOC')) {
                            $json['newbusinessadvice'] .= "<tr><td>" . $nb->link($nb->forename() . " " . $nb->surname()) . "</td><td>" . User::getUserTime($nb->id()) . "</td></tr>";
                        }

                        while ($sd = $service_delivery->next('MYSQLi_ASSOC')) {
                            $json['service_delivery'] .= "<tr><td >" . $sd->link($sd->forename() . " " . $sd->surname()) . "</td><td>" . User::getUserTime($sd->id()) . "</td></tr>";
                        }

                        while ($i = $it->next('MYSQLi_ASSOC')) {
                            $json['it'] .= "<tr><td>" . $i->link($i->forename() . " " . $i->surname()) . "</td><td>" . User::getUserTime($i->id()) . "</td> </tr>";
                        }

                        while ($m = $management->next('MYSQLi_ASSOC')) {
                            $json['management'] .= "<tr><td >" . $m->link($m->forename() . " " . $m->surname()) . "</td><td >" . User::getUserTime($m->id()) . "</td> </tr>";
                        }

                        while ($ex = $exec->next('MYSQLi_ASSOC')) {
                            $json['exec'] .= "<tr><td>" . $ex->link($ex->forename() . " " . $ex->surname()) . "</td><td>" . User::getUserTime($ex->id()) . "</td></tr>";
                        }


                        echo json_encode($json);

                        break;

                    default:
                        // Display User's "My Account" page
                        $user->get();
                        echo Template(User::get_template_path("my_account.html"), ["user" => $user]);

                        break;
                }
            }
        } else {
            if (isset($_GET['do'])){
                switch($_GET['do']){
                    case "img_upload":
                        $response = [
                            'success' => true,
                            'feedback' => null
                        ];

                        $file = $_FILES['user_img'];

                        if ($file['type'] === "image/jpeg"){
                            try{
                                // check to see if we already have an image for the recruit
                                if (azureCheckExists(CDN_DIR . "staff_images", $user->id() . ".jpg")){
                                    // remove the old file, keep things tidy
                                    azureRemoveFile(CDN_DIR . "staff_images", $user->id() . ".jpg");
                                }

                                // upload the new file
                                azureAddFile(CDN_DIR . "staff_images", $file['tmp_name'], $user->id() . ".jpg");

                            } catch (Exception $e) {
                                $response['success'] = false;
                                $response['feedback'] = "Failed to upload user image: " . $e;
                            }
                        } else {
                            // not allowed
                            $response['success'] = false;
                            $response['feedback'] = "Only .jpg files are allowed here";
                        }

                        echo json_encode($response);

                        break;

                    case "deactivate":
                        if (!empty($_GET['deactivate'])){
                            $response = [
                                "success" => true,
                                "data" => null
                            ];

                            $user->load($_GET['user']);
                            $user->active(0);

                            if ($user->save()){
                                // deactivate myPSaccount
                                $webUser = new WebsiteUser($user->myps_user_id(), true);

                                $webUser->allowed_access(0);

                                if (!$webUser->save()){
                                    $response['success'] = false;
                                    $response['data'] = "Failed to deactivate user's myPSaccount";
                                }
                            } else {
                                $response['success'] = false;
                                $response['data'] = "Failed to deactivate account";
                            }

                            echo json_encode($response);
                        } else {
                            echo new Template("user/deactivate.html", compact("user"));
                        }

                        break;
                }
            } else {
                //// Output profile for an existing User
                echo Template("user/form.html", ["user" => new User($_GET["id"], true)]);
            }
        }
    }
} else {
    if (isset($_GET['do'])){
        switch($_GET['do']){
            case "current_corr_count":
                $json = [];

                $json["current_corr_count"] = Correspondence::correspondence_count(User::get_default_instance('id'));

                echo json_encode($json);

                break;

            case "leave_message":
                $sender = new User(User::get_default_instance('id'), true);
                $recipient = new User($_GET['recipient'], true);
                $message = new StaffNotifications();
                echo Template(
                    User::get_template_path("leave_message.html"),
                    ["message" => $message, "sender" => $sender, "recipient" => $recipient]
                );

                break;

            case "display_messages_template":
                $json = [];

                // search for any outstanding messages
                $s = new Search(new StaffNotifications());
                $s->eq("recipient_id", User::get_default_instance('id'));
                $s->eq("archived", null);

                $call_action = [
                    1 => "Left a message",
                    2 => "Called for you",
                    3 => "Returned your call"
                ];

                $rec_action = [
                    1 => "Please call them back.",
                    2 => "Caller will try again later.",
                    3 => "Caller will email you.",
                ];

                $i = 0;

                while ($m = $s->next(MYSQLI_ASSOC)) {
                    if (isset($call_action[$m->caller_action()])) {
                        $caller_action = $call_action[$m->caller_action()];
                    }

                    if (isset($rec_action[$m->caller_action()])) {
                        $recipient_action = $rec_action[$m->caller_action()];
                    }

                    $json[$i]['sender'] = ucwords($m->sender_id->forename()) . " " . ucwords($m->sender_id->surname());
                    $json[$i]['caller_name'] = ucwords($m->caller_name());
                    $json[$i]['caller_company'] = $m->caller_company();
                    $json[$i]['contact_details'] = $m->contact_details();
                    $json[$i]['caller_action'] = $caller_action;
                    $json[$i]['recipient_action'] = $recipient_action;
                    $json[$i]['message'] = $m->message();
                    $json[$i]['sent_when'] = date('H:i:s d/m/Y', $m->added_when());
                    $json[$i]['id'] = $m->id();
                    $i++;
                }


                echo json_encode($json);

                break;

            case "view_messages":
                $message = new StaffNotifications($_GET['rowid'], true);

                $flag = $_GET['flag'];

                if ($message->recipient_id() == $_COOKIE['uid']) {
                    $message->read(1);
                    $message->save();


                    if ($flag == true) {
                        $date = new DateTime();

                        $db = new mydb();
                        $q = "UPDATE  prophet.staff_notifications set archived = " . $date->getTimestamp() . " WHERE id = " . $message->id();
                        $db->query($q);
                    }
                }

                echo Template("user/view_message.html", ["message" => $message]);

                break;

            case "list_outstanding_messages":
                $json = [
                    "error" => false,
                    "data" => false
                ];

                // search for any outstanding messages
                $s = new Search(new StaffNotifications());
                $s->eq("recipient_id", User::get_default_instance('id'));

                while ($m = $s->next(MYSQLI_ASSOC)) {
                    if ($m->archived() == null) {
                        if ($m->read() == 0) {
                            $readspan = " <i class='fa fa-envelope-o ' Title='Not read'></i><i style='color:red' class='fa fa-circle ' Title='Not read'></i>";
                        } else if ($m->read() == 1) {
                            $readspan = " <i class='fa fa-envelope-open-o ' Title='Read'></i><i style='color:green' Title='Read' class='fa fa-circle '></i>";
                        }

                        $json['data'] .= "<tr width='50%'><td>" . ucwords($m->sender_id->forename()) . " " . ucwords($m->sender_id->surname()) . "</td><td> " . truncate($m->message(), 35) . "</td><td>" . date('H:i:s d/m/Y', $m->added_when()) . "</td><td><button id='" . $m->id() . "' class='viewMessage'>View</button></td><td>" . $readspan . "</td></tr>";
                    }
                }

                echo json_encode($json);

                break;

            case "list_archived_messages":
                $json = [
                    "error" => false,
                    "data" => false
                ];

                // search for any outstanding messages
                $s = new Search(new StaffNotifications());
                $s->eq("recipient_id", User::get_default_instance('id'));

                while ($m = $s->next(MYSQLI_ASSOC)) {
                    if ($m->archived() != null) {
                        if ($m->read() == 0) {
                            $readspan = " <i class='fa fa-envelope-o ' Title='Not read'></i><i style='color:red' class='fa fa-circle ' Title='Not read'></i>";
                        } else if ($m->read() == 1) {
                            $readspan = " <i class='fa fa-envelope-open-o ' Title='Read'></i><i style='color:green' Title='Read' class='fa fa-circle '></i>";
                        }

                        $json['data'] .= "<tr width='50%'><td>" . ucwords($m->sender_id->forename()) . " " . ucwords($m->sender_id->surname()) . "</td><td> " . truncate($m->message(), 35) . "</td><td>" . date('H:i:s d/m/Y', $m->added_when()) . "</td><td><button id='" . $m->id() . "' class='viewMessage'>View</button></td><td>" . $readspan . "</td></tr>";
                    }
                }

                echo json_encode($json);

                break;

            case "list_sent_messages":
                $json = [
                    "error" => false,
                    "data" => false
                ];

                // search for any outstanding messages
                $s = new Search(new StaffNotifications());
                $s->eq("sender_id", User::get_default_instance('id'));


                while ($m = $s->next(MYSQLI_ASSOC)) {
                    if ($m->read() == 0) {
                        $readspan = " <i class='fa fa-envelope-o ' Title='Not read'></i><i style='color:red' class='fa fa-circle ' Title='Not read'></i>";
                    } else if ($m->read() == 1) {
                        $readspan = " <i class='fa fa-envelope-open-o ' Title='Read'></i><i style='color:green' Title='Read' class='fa fa-circle '></i>";
                    }


                    $json['data'] .= "<tr width='50%'><td>" . ucwords($m->recipient_id->forename()) . " " . ucwords($m->recipient_id->surname()) . "</td><td> " . truncate($m->message(), 35) . "</td><td>" . date('H:i:s d/m/Y', $m->added_when()) . "</td><td><button id='" . $m->id() . "' class='viewMessage'>View</button></td><td>" . $readspan . "</td></tr>";
                }

                echo json_encode($json);

                break;

            case "check_messages":
                $json = [
                    "error" => false,
                    "data" => ""
                ];

                // search for any outstanding messages
                $s = new Search(new StaffNotifications());
                $s->eq("recipient_id", User::get_default_instance('id'));
                $s->eq("archived", null);

                $call_action = [
                    1 => "Left a message",
                    2 => "Called for you",
                    3 => "Returned your call"
                ];

                $rec_action = [
                    1 => "Please call them back.",
                    2 => "Caller will try again later.",
                    3 => "Caller will email you.",
                ];

                try {
                    while ($m = $s->next(MYSQLI_ASSOC)) {
                        $time = \Carbon\Carbon::createFromTimestamp($m->added_when());

                        if ($time->isToday()){
                            $timeString = "at " . $time->format("H:i");
                        } else {
                            $timeString = "on " . $time->toDayDateTimeString();
                        }

                        $json['data'] .= new Template("misc/toast.html", ['message' => $m, 'time' => $timeString]);
                    }

                    echo json_encode($json);
                } catch (Exception $e){
                    $json['error'] = true;
                    $json['data'] = $e->getMessage();
                }

                break;

            case "archive_message":
                $json = [
                    "status" => null,
                    "error" => false
                ];

                $sn = new StaffNotifications($_GET['message_id'], true);
                $sn->archived(time());
                if ($sn->save()) {
                    $json['status'] = "Message Acknowledged";
                } else {
                    $json['error'] = "Failed to acknowledge, try again or contact IT.";
                }

                echo json_encode($json);

                break;

            case "change":
                //prompt user to change password
                echo new Template(User::get_template_path("change_password.html"));

                break;

            case "expiry_check":
                $user = User::get_default_instance('id');

                $db = new mydb();
                $q = "SELECT * FROM " . USR_TBL . " WHERE id = " . $user;
                $db->query($q);

                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $pwd_exp = $row['password_expired'];
                }

                if ($pwd_exp == 1) {
                    $json['status'] = "expired";
                    echo json_encode($json);
                    return;
                } else {
                    if ($_GET['password'] == "policyservices") {
                        $json['status'] = "expired";
                        echo json_encode($json);
                        return;
                    } else {
                        $json['status'] = "valid";
                        echo json_encode($json);
                        return;
                    }
                }

                break;

            case "settings":
                $user = new User($_GET['user'], true);
                $tester = $_GET["tester"];

                $json = (object)[
                    "status" => false,
                    "error" => false
                ];

                try{
                    $user->beta_tester->set($tester);
                    $user->first_aider->set($_GET['first_aider']);
                    $user->fire_warden->set($_GET['fire_warden']);

                    $json->status = (bool)$user->save();

                } catch (Exception $e){
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);

                break;

            case "remove_group":
                $response = [
                    "success" => true
                ];

                $ug = new UsersGroups($_GET['ugID'], true);

                $ug->delete();

                echo json_encode($response);

                break;

            case "scroll_change_status":
                $json = [
                    "return" => null,
                    "error" => false
                ];

                if (isset($_GET['status'])) {
                    $user = User::get_default_instance();

                    if ($_GET['status'] == "active") {
                        $user->status("busy");
                    } else if ($_GET['status'] == "busy") {
                        $user->status("meeting");
                    } else if ($_GET['status'] == "meeting") {
                        $user->status("active");
                    }

                    if ($user->save()) {
                        $json['return'] = $user->status();
                    } else {
                        $json['error'] = true;
                        $json['return'] = $user->status();
                    }
                }
                echo json_encode($json);

                break;

            case "add_user_group":
                if (isset($_GET['user'])){
                    $response = [
                        "success" => true,
                    ];

                    $ug = new UsersGroups();

                    $ug->user($_GET['user']);
                    $ug->user_group($_GET['group']);

                    if (!$ug->save()){
                        $response['success'] = false;
                    }

                    echo json_encode($response);
                } else {
                    echo Template("user/new_group.html", ["uid" => $_GET['uid'], "frame" => $_GET['frame']]);
                }

                break;

            case "impersonate":
                $response = [
                    "success" => true,
                    "data" => null,
                ];

                if (empty($_SESSION['real_user'])){
                    $_SESSION['real_user'] = User::get_default_instance("id");
                } else {
                    if ($_SESSION['real_user'] == $_GET['user']){
                        //we are attempting to go back to our own account, unset it
                        unset($_SESSION['real_user']);
                    }
                }

                $user = new User($_GET['user'], true);

                $user->loginUsingId($user->id());

                echo json_encode($response);

                break;

            case "get_staff":
                $response = [
                    'success' => true,
                    'data' => [],
                ];

                $s = new Search(new User());

                $s->eq("active", 1);

                switch($_GET['team']){
                    case "managers":
                        $s->add_or(
                            $s->eq("department", 8, false, false),
                            $s->eq("id", 23, false, false), // Kev
                            $s->eq("id", 60, false, false) // Kayleigh
                        );

                        break;

                    case "coordinators":
                        $s->eq("id", 214); // Michaela

                        break;

                    case "partner-accounts":
                        $s->eq("department", 4);
                        $s->nt("id", 60); // Kayleigh

                        break;

                    case "finance":
                        $s->eq("department", 1);
                        $s->nt("id", 62); // EDI Import
                        $s->nt("id", 63); // Dataflow
                        $s->nt("id", 29); // Avril
                        $s->nt("id", 99); // Lisa
                        $s->nt("id", 214); // Michaela
                        $s->nt("id", 225); // Jacqueline

                        $s->add_order("FIELD(id, 203)", "DESC");

                        break;

                    case "service-delivery":
                        $s->eq("id", 98); // Louise G

                        break;

                    case "client-services":
                        $s->eq("department", 6);
                        $s->nt("id", 98); // Louise G

                        break;

                    case "client-relations":
                        $s->eq("department", 15);

                        break;

                    case "io":
                        $s->eq("department", 14);

                        break;

                    case "compliance":
                        $s->eq("department", 2);

                        break;

                    case "paye":
                        $s->add_or([
                            $s->eq("id", 29, false, false), // Avril
                            $s->eq("id", 99, false, false), // Lisa
                            $s->eq("id", 225, false, false), // Jacqueline
                        ]);

                        $s->add_order("FIELD(id, 29)", "DESC");

                        break;

                    case "it":
                        $s->eq("department", 7);
                        $s->nt("id", 23); // Kev

                        break;

                    case "nb-admin":
                        $s->eq("department", 3);

                        break;

                    case "nb-advice":
                        $s->eq("department", 16);

                        break;

                    case "virtue":
                        $s->eq("department", 10);

                        break;
                }

                $s->add_order("team_leader", "DESC");
                $s->add_order("username");

                while($u = $s->next(MYSQLI_ASSOC)){
                    $response['data'][] = [
                        "img" => STAFF_PROFILE_DIRECTORY . "thumbnails/" . $u->id() . ".jpg",
                        "name" => $u->link($u->staff_name()),
                        "status" => $u->statusBadge(),
                        "ext" => $u->internal_extension(),
                        "mobile" => $u->mobile(),
                        "email" => $u->email(),
                        "fire" => $u->fire_warden(),
                        "aid" => $u->first_aider(),
                    ];
                }

                echo json_encode($response);

                break;

            case "create":
                $json = (object)[
                    "id" => false,
                    "status" => false,
                    "error" => false,
                    "data" => null
                ];

                try{
                    $user = new User();

                    $user->load($_GET['user']);

                    if(!$user->checkUsername()){
                        throw new Exception("Username is already in use");
                    }

                    $user->password($user->validatePassword($user->password()));

                    $webUser = new WebsiteUser();

                    $webUser->first_name($user->forename());
                    $webUser->last_name($user->surname());
                    $webUser->email_address($user->email());

                    $webUser->save();
                    $webUser->on_create();

                    $user->myps_user_id($webUser->id());

                    $user->external_extension($user->internal_extension());

                    $user->save();

                    $viewer = new ViewerEntry();

                    // give internal users access to David's account by default
                    $viewer->user($user->myps_user_id());
                    $viewer->partner(79);

                    $viewer->email_notification(0);
                    $viewer->mandate_notification(0);
                    $viewer->proreport_notification(0);

                    $viewer->save();

                    foreach(explode(",", $_GET['user-groups']) as $group){
                        if (!empty($group)){
                            $ug = new UsersGroups();

                            $ug->user($user->id());
                            $ug->user_group($group);

                            $ug->save();
                        }
                    }

                    $json->status = true;
                    $json->id = $user->id();
                } catch (Exception $e) {
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);

                break;
        }
    } elseif (isset($_GET["list"])) {
        // Create a "telephone book"-style list of users
        if ((isset($_GET['dept'])) && $_GET['dept'] == "all") {
            $json = [
                "acquisition" => false,
                "commission" => false,
                "clientservices" => false,
                "clientrelations" => false,
                "io" => false,
                "newbusinessadmin" => false,
                "newbusinessadvice" => false,
                "it" => false,
                "management" => false,
                "virtue" => false,
                "service_delivery" => false,
                "compliance" => false,
                "exec" => false,
                "sjp" => false
            ];

            $acquisition = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 4)
                ->nt("id", 60)
                ->add_order("team_leader", "DESC")
                ->add_order("username");

            $commission = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 1)
                ->nt("id", 62)
                ->nt("id", 63)
                ->add_order("team_leader", "DESC")
                ->add_order("id", "asc")
                ->add_order("username");

            $compliance = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 2)
                ->add_order("team_leader", "DESC")
                ->add_order("id", "asc")
                ->add_order("username");

            $clientservices = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 6)
                ->add_order("team_leader", "DESC")
                ->add_order("weekend_staff", "ASC")
                ->add_order("username");

            $io = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 14)
                ->add_order("team_leader", "DESC")
                ->add_order("weekend_staff", "ASC")
                ->add_order("username");

            $clientrelations = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 15)
                ->add_order("team_leader", "DESC")
                ->add_order("weekend_staff", "ASC")
                ->add_order("username");

            $newbusinessadmin = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 3)
                ->add_order("team_leader", "DESC")
                ->add_order("username");

            $newbusinessadvice = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 16)
                ->add_order("team_leader", "DESC")
                ->add_order("username");

            $it = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 7)
                ->nt("id", 23)
                ->add_order("team_leader", "DESC")
                ->add_order("username");

            $management = User::get_search()->eq("active", 1);
            $management->add_or(
                $management->eq("department", 8, false, false),
                $management->eq("id", 23, false, false),
                $management->eq("id", 60, false, false)
            );
            $management->add_order("team_leader", "DESC");
            $management->add_order("username");

            $virtue = Search(User::get_default_instance())
                ->eq("active", true)
                ->eq("department", 10)
                ->add_order("team_leader", "DESC")
                ->add_order("username");

            while ($u = $acquisition->next(MYSQLI_ASSOC)) {
                $json['acquisition'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            while ($u = $commission->next(MYSQLI_ASSOC)) {
                switch ($u->id()){
                    case 29:
                        $avril = new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                    case 99:
                        $lisa = new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                    case 203:
                        $charmaine = new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                    case 214:
                        $json['exec'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                    default:
                        $json['commission'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                }
            }

            $json['commission'] = $charmaine.$json['commission'];
            $json['paye'] = $avril.$lisa;

            while ($u = $compliance->next(MYSQLI_ASSOC)) {
                $json['compliance'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            while ($u = $virtue->next(MYSQLI_ASSOC)) {
                switch($u->id()){
                    case 56:
                        $json['projects'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                    case 108:
                        $json['compliance'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                    default:
                        $json['virtue'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                }
            }

            while ($u = $clientservices->next(MYSQLI_ASSOC)) {
                switch($u->id()){
                    case 98:
                        $json['service_delivery'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                    default:
                        $json['clientservices'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
                        break;
                }
            }

            while ($u = $io->next(MYSQLI_ASSOC)) {
                $json['io'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            while ($u = $clientrelations->next(MYSQLI_ASSOC)) {
                $json['clientrelations'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            while ($u = $newbusinessadmin->next(MYSQLI_ASSOC)) {
                $json['newbusinessadmin'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            while ($u = $newbusinessadvice->next(MYSQLI_ASSOC)) {
                $json['newbusinessadvice'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            while ($u = $it->next(MYSQLI_ASSOC)) {
                $json['it'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            while ($u = $management->next(MYSQLI_ASSOC)) {
                $json['management'] .= new Template("user/directory_row.html", ["status" => User::check_user_status($u->id()), "object" => $u, "mobile" => ($u->mobile() ? $u->mobile : "--")]);
            }

            echo json_encode($json);
        } else {
            echo Template("user/directory.html");
        }
    } else {
        if (isset($_GET["search"])) {
            $search = new Search($user);
            $search
                ->flag("id", Search::DEFAULT_INT)
                ->flag("username", Search::DEFAULT_STRING);

            $search->order("username");

            if (isset($_REQUEST["group"]) && intval($_REQUEST["group"])) {
                $q = "SELECT true FROM " . TBL_USERS_GROUPS . " WHERE user_id = " . (int)$_REQUEST["group"];
                $search->add_and($q);
            }

            die($search->execute(false, true));
        }
    }
}
