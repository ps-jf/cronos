<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if ($_GET['do'] == "staff_training") {
    if ($_GET['action'] == "course_management") {
        $search = new Search(new StaffTrainingCourse());

        if (!isset($_GET['template'])) {
            $json = [
                "data" => false
            ];

            if ($_GET['category']) {
                $search->eq("category", $_GET['category']);
            }

            while ($course = $search->next(MYSQLI_ASSOC)) {
                $json['data'] .= "<tr id='".$course->id()."'>
                                    <td>".$course->course_name()."</td>
                                    <td>".$course->category."</td>
                                    <td>
                                        <button type='button' class='edit'>Edit</button>
                                        <button type='button' class='delete'>&nbsp;</button>
                                    </td>
                                </tr>";
            }

            if (!$json['data']) {
                $json['data'] = "<tr><td colspan='3'>No courses to display</td></tr>";
            }

            echo json_encode($json);
        } else {
            $course_obj = new StaffTrainingCourse();

            // template to display all courses and allow management of these
            echo Template("stafftraining/course_management.html", ["courses"=>$search, "course_obj"=>$course_obj]);
        }
    } elseif ($_GET['action'] == "add_course") {
        if (isset($_GET['category'])) {
            $json = [
                "error" => false,
                "status" => false
            ];

            $search = new Search(new StaffTrainingCourse());
            $search->eq("course_name", $_GET['course_name']);
            $search->eq("category", $_GET['category']);

            // if course does not exist
            if (!$search->execute()) {
                //add course entry
                $stc = new StaffTrainingCourse();
                $stc->course_name($_GET['course_name']);
                $stc->category($_GET['category']);
                if ($stc->save()) {
                    $json['status'] = "Course successfully added";
                } else {
                    $json['error'] = true;
                    $json['status'] = "Course failed to add, try again or contact IT";
                }
            } else {
                $json['error'] = true;
                $json['status'] = "Course name already exist, must be unique";
            }

            echo json_encode($json);
        } else {
            // display form to add new training course
            $course = new StaffTrainingCourse();
            $title = "Add new course";
            echo Template("stafftraining/course.html", ["course"=>$course, "title"=>$title, "new"=>true]);
        }
    } elseif ($_GET['action'] == "edit_course") {
        $course = new StaffTrainingCourse($_GET['course_id'], true);

        if (isset($_GET['save'])) {
            $json = [
                "error" => false,
                "status" => false
            ];

            $course->course_name($_GET['course_name']);
            $course->category($_GET['category']);
            if ($course->save()) {
                $json['status'] = "Course successfully updated";
            } else {
                $json['error'] = true;
                $json['status'] = "Course failed to update, try again or contact IT";
            }

            echo json_encode($json);
        } else {
            // display form to edit training course
            $title = "Edit course - ".$course->course_name();
            echo Template("stafftraining/course.html", ["course"=>$course, "title"=>$title]);
        }
    } elseif ($_GET['action'] == "delete_course") {
        $json = [
            "error" => false,
            "status" => false
        ];

        $search = new Search(new StaffTraining());
        $search->eq("course", $_GET['course_id']);

        $db = new mydb();

        if ($search->execute()) {
            // we have at least one training entry under this course, delete these first and then delete course

            $q = "DELETE FROM prophet.staff_training WHERE course_id = ".$_GET['course_id'];
            if ($db->query($q)) {
                // training entries deleted, now delete course
                $q = "DELETE FROM prophet.staff_training_courses WHERE id = ".$_GET['course_id'];
                if ($db->query($q)) {
                    $json['status'] = "Course successfully deleted";
                } else {
                    $json['error'] = true;
                    $json['status'] = "Error deleting course #".$_GET['course_id'].", staff training entries have been deleted, contact IT";
                }
            } else {
                $json['error'] = true;
                $json['status'] = "Error deleting staff training entries under course #".$_GET['course_id'].", contact IT";
            }
        } else {
            // we have no staff entries under this course, delete course only

            $q = "DELETE FROM prophet.staff_training_courses WHERE id = ".$_GET['course_id'];
            if ($db->query($q)) {
                $json['status'] = "Course successfully deleted";
            } else {
                $json['error'] = true;
                $json['status'] = "Error deleting course #".$_GET['course_id'].", contact IT";
            }
        }

        echo json_encode($json);
    } elseif ($_GET['action'] == "mass_assign_course") {
        if (isset($_GET['course'])) {
        } else {
            // get training entry object
            $training = new StaffTraining();
            $course = new StaffTrainingCourse();
            $groups = Groups::dept_groups();
            $users = User::active_users_array();


            // display form to assign training course to user
            echo Template("stafftraining/mass_assign_course.html", [
                "training"=>$training,
                "course"=>$course,
                "groups"=>$groups,
                "users"=>$users
            ]);
        }
    } elseif ($_GET['action'] == "category_to_course") {
        $json = [
            "courses" => false
        ];

        $search = new Search(new StaffTrainingCourse());
        $search->eq("category", $_GET['category']);

        $courses = [];

        while ($c = $search->next(MYSQLI_ASSOC)) {
            // build up array of each course within specified category
            $courses[$c->id()] = $c->course_name();
        }

        $json['courses'] = $courses;

        echo json_encode($json);
    } elseif ($_GET['action'] == "group_user_checkboxes") {
        $json = [
            "users" => false
        ];

        $users = [];

        $s = new Search(new User());
        $s->eq("department", $_GET['group_id']);
        $s->eq("active", 1);

        while ($u = $s->next(MYSQLI_ASSOC)) {
            // build up array of each course within specified category
            $users[] = $u->id();
        }

        $json['users'] = $users;

        echo json_encode($json);
    } elseif ($_GET['action'] == "add_course_to_user") {
        if (isset($_GET['course'])) {
            $json = [
                "error" => false,
                "status" => false
            ];

            $category = $_GET['category'];
            $course= $_GET['course'];


            // convert dates to unix timestamps
            $timestamp_start = strtotime($_GET['start']);
            $timestamp_completed = strtotime($_GET['completed']);
            $timestamp_next = strtotime($_GET['next']);

            // if a date has been passed through - update, else "--" has been passed though & variable will = false
            if ($timestamp_start) {
                $start = $timestamp_start;
            }
            if ($timestamp_completed) {
                $completed = $timestamp_completed;
            }
            if ($timestamp_next) {
                $next = $timestamp_next;
            }

            // if mass assign..
            if (isset($_GET['user_array'])) {
                $user_count = count($_GET['user_array']);
                $i = 0;

                foreach ($_GET['user_array'] as $uid) {
                    /*
                     * to save duplicating a bunch of code for an mass-update function, here we will simply do a
                     * search (on user_id and course) to check whether or not an entry already exists,
                     * if so we will update this entry, if not we will add a new one
                     */

                    $s = new Search(new StaffTraining());
                    $s->eq("user_id", $uid);
                    $s->eq("course", $course);

                    if ($s->execute()) {
                        if ($t = $s->next(MYSQLI_ASSOC)) {
                            // get existing training entry object
                            $training = new StaffTraining($t->id(), true);
                        }
                    } else {
                        // get new training entry object
                        $training = new StaffTraining();
                    }

                    // fill in rest of parameters
                    $training->user($uid);
                    $training->category($category);
                    $training->course($course);
                    $training->start($start);
                    $training->completed($completed);
                    $training->next($next);

                    if ($training->save()) {
                        $i++;
                    }
                }

                if ($i == $user_count) {
                    $json['status'] = "All Entries successfully added/updated";
                } else {
                    $json['error'] = true;
                    $json['status'] = "An error occurred adding/updating entries, contact IT";
                }
            } else {
                // check if user already has an entry for this course
                $s = new Search(new StaffTraining());
                $s->eq("user_id", $_GET['user_id']);
                $s->eq("course", $course);

                if (!$s->execute()) {
                    // get training entry object
                    $training = new StaffTraining();
                    $training->user($_GET['user_id']);
                    $training->category($category);
                    $training->course($course);
                    $training->start($start);
                    $training->completed($completed);
                    $training->next($next);

                    // update training entry
                    if ($training->save()) {
                        $json['status'] = "Entry successfully added";
                    } else {
                        $json['error'] = true;
                        $json['status'] = "An error occurred adding entry, try again or contact IT";
                    }
                } else {
                    $json['error'] = true;
                    $json['status'] = "This course has already been assigned to this user.";
                }
            }

            echo json_encode($json);
        } else {
            $courses = $category_id = $category_name = false;

            // get training entry object
            $training = new StaffTraining();

            // bring back training records for a specific user
            if ($_GET['user_id'] != "-") {
                $user_id = $_GET['user_id'];
            } else {
                $user_id = User::get_default_instance('id');
            }

            $user = new User($user_id, true);

            $search = new Search(new StaffTrainingCourse());
            $search->eq("category", $_GET['category']);


            if ($search->execute()) {
                $courses = [];

                while ($t = $search->next(MYSQLI_ASSOC)) {
                    // get category name
                    $category_id = $t->category();
                    $category_name = $t->category;

                    // build up array of each course within specified category
                    $courses[$t->id()] = $t->course_name();
                }
            }

            // display form to assign training course to user
            echo Template("stafftraining/add_user_training.html", [
                "user"=>$user,
                "category_id"=>$category_id,
                "category_name"=>$category_name,
                "training"=>$training,
                "courses"=>$courses
            ]);
        }
    } elseif ($_GET['action'] == "edit_user_training_entry") {
        $json = [
            "error" => false,
            "status" => false
        ];

        // get training entry object
        $training = new StaffTraining($_GET['training_id'], true);

        // if form submitted do updated, else we want to display the template to edit a training entry
        if (isset($_GET['start'])) {
            // convert dates to unix timestamps
            $timestamp_start = strtotime($_GET['start']);
            $timestamp_completed = strtotime($_GET['completed']);
            $timestamp_next = strtotime($_GET['next']);

            // if a date has been passed through - update, else "--" has been passed though & variable will = false
            if ($timestamp_start) {
                $training->start($timestamp_start);
            }
            if ($timestamp_completed) {
                $training->completed($timestamp_completed);
            }
            if ($timestamp_next) {
                $training->next($timestamp_next);
            }

            // update training entry
            if ($training->save()) {
                $json['status'] = "Entry successfully updated";
            } else {
                $json['error'] = true;
                $json['status'] = "An error occurred updating entry, try again or contact IT";
            }

            echo json_encode($json);
        } else {
            // display form to add new training course
            echo Template("stafftraining/edit_user_training.html", ["training"=>$training]);
        }
    } elseif ($_GET['action'] == "delete_user_training_entry") {
        $json = [
            "error" => false,
            "status" => false
        ];

        $db = new mydb();
        $q = "DELETE FROM prophet.staff_training WHERE id = ".$_GET['training_id'];
        if ($db->query($q)) {
            $json['status'] = "Entry successfully deleted";
        } else {
            $json['error'] = true;
            $json['status'] = "An error occurred deleting entry, try again or contact IT";
        }

        echo json_encode($json);
    } elseif ($_GET['action'] == "show_user") {
        $json = [
            "induction_data" => false,
            "department_data" => false,
            "company_data" => false
        ];

        /*
         * admin to receive filters, actions etc
         */
        $admin = StaffTraining::training_admin();

        // bring back training records for a specific user
        if (isset($_GET['user_id'])) {
            if ($_GET['user_id'] != "-") {
                $user_id = $_GET['user_id'];
            } else {
                $user_id = User::get_default_instance('id');
            }
        } else {
            $user_id = User::get_default_instance('id');
        }

        $user = new User($user_id, true);

        /*
         * find any training entries for viewing user
         */
        $s = new Search(new StaffTraining);
        $s->eq('user', $user->id());

        $induction_data = $department_data = $company_data = "";

        /*
         * gather data for individual categories/tabs
         */

        while ($t = $s->next(MYSQLI_ASSOC)) {
            // category 1 = induction
            if ($t->course->category() == 1) {
                $induction_data .= "<tr id='" . $t->id . "'>
                        <td>" . $t->link() . "</td>
                        <td>" . $t->start . "</td>
                        <td>" . $t->completed . "</td>
                        <td>" . $t->next . "</td>";

                if ($admin) {
                    $induction_data .= "<td><button class='edit'>Edit</button><button class='delete'>&nbsp;</button></td></tr>";
                } else {
                    $induction_data .= "</tr>";
                }
            }

            // category 2 = department
            if ($t->course->category() == 2) {
                $department_data .= "<tr id='" . $t->id . "'>
                        <td>" . $t->link() . "</td>
                        <td>" . $t->start . "</td>
                        <td>" . $t->completed . "</td>
                        <td>" . $t->next . "</td>";

                if ($admin) {
                    $department_data .= "<td><button class='edit'>Edit</button><button class='delete'>&nbsp;</button></td></tr>";
                } else {
                    $department_data .= "</tr>";
                }
            }

            // category 3 = company
            if ($t->course->category() == 3) {
                $company_data .= "<tr id='" . $t->id . "'>
                        <td>" . $t->link() . "</td>
                        <td>" . $t->start . "</td>
                        <td>" . $t->completed . "</td>
                        <td>" . $t->next . "</td>";

                if ($admin) {
                    $company_data .= "<td><button class='edit'>Edit</button><button class='delete'>&nbsp;</button></td></tr>";
                } else {
                    $company_data .= "</tr>";
                }
            }
        }

        /*
         * if no data, provide feedback so we do not have an empty table
         */
        if ($induction_data == "") {
            $induction_data = "<tr><td colspan='6'>No induction training entries to view.</td></tr>";
        }
        if ($department_data == "") {
            $department_data = "<tr><td colspan='6'>No department training entries to view.</td></tr>";
        }
        if ($company_data == "") {
            $company_data = "<tr><td colspan='6'>No company training entries to view.</td></tr>";
        }

        $json['induction_data'] = $induction_data;
        $json['department_data'] = $department_data;
        $json['company_data'] = $company_data;
        $json['viewing_user'] = ucwords(str_replace('.', ' ', $user->username));

        echo json_encode($json);
    } else {
        /*
         * admin to receive filters, actions etc
         */
        $admin = StaffTraining::training_admin();

        // show us the staff training template
        echo Template(StaffTraining::get_template_path("staff_training.html"), ["admin"=>$admin]);
    }
} elseif (isset($_GET['object_id'])) {
    if ($_GET['do'] == "upload") {
        $json = [
            "status" => false,
            "error" => false
        ];

        // assign file information to variable
        $f = $_FILES["file"];
        // get file extension
        $ext = pathinfo($f['name'], PATHINFO_EXTENSION);

        // directory
        $hash = md5_file($f['tmp_name']);
        $path = str_split(substr($hash, 0, 8), 2);
        $path = implode("/", $path);

        // full directory path
        $dir = PROPHET_FILE_UPLOAD_DIR."/".$path;

        /*
         * confirm that directory does not already exist then proceed to create directory
         */
        if (!file_exists($dir)) {
            if (!mkdir($dir, 0777, true)) {
                $json['error'] = true;
                $json['status'] = "Failed to create directory, Please contact IT.";
                echo json_encode($json);
                exit();
            }
        }

        // create random file name and concatenate extension
        $unique_name = random_string(15).".".$ext;


        // if display name specified use it - otherwise use filename
        ( !empty($_GET["filename"]) ) or $_GET["filename"] = $f['name'];

        $file = new FileMapping();
        $file->display_name($_GET["filename"]);
        $file->filename($f['name']);
        $file->size($f['size']);
        $file->mime_type($f['type']);
        $file->object("StaffTraining");
        $file->object_id($_GET['object_id']);
        $file->path("$path/$unique_name");
        //$file->is_published(true);


        try {
            if (!$file->save()) {
                $json['error'] = true;
                $json['status'] = "Could not save file mapping instance, Contact IT.";
            } else {
                /*
                 * mapping entry created and configuration variables set, now transfer file into it's new home
                 */

                if (file_exists($f['tmp_name'])) {
                    if (!move_uploaded_file($f['tmp_name'], $dir."/".$unique_name)) {
                        $json['error'] = true;
                        $json['status'] = "Could not move file to new location, Contact IT.";
                    } else {
                        $json['status'] = "File successfully uploaded.";
                    }
                } else {
                    $json['error'] = true;
                    $json['status'] = "Temp file no longer exists, Contact IT.";
                }
            }
        } catch (Exception $e) {
            $json['error'] = true;
            $json['status'] = $e->getMessage();
        }

        echo json_encode($json);
    } elseif ($_GET['do'] == "view_file") {
        if (isset($_GET['template'])) {
            $training = new StaffTraining($_GET['object_id'], true);

            $s = new Search(new FileMapping());
            $s->eq("object_id", $training->id());
            $s->eq("object", "StaffTraining");

            echo Template(StaffTraining::get_template_path("view_certificates.html"), ["training"=>$training, "search"=>$s]);
        } else {
            $file_object = new FileMapping($_GET['id'], true);

            try {
                $file_object->read_file();
            } catch (Exception $e) {
                $json['error'] = true;
                $json['status'] = $e->getMessage();
            }

            // increment view counter
            $file_object->view_count($file_object->view_count() + 1);
            $file_object->save();
        }
    } else {
        /*
        * we have training id, now present user with upload template
        */

        $training = new StaffTraining($_GET['object_id'], true);
        echo Template(StaffTraining::get_template_path("upload_form.html"), ["training"=>$training]);
    }
}
