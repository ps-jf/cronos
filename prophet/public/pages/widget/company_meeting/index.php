<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$meetingJSON = file_get_contents(WIDGETS_DATA."company_meeting.json");
$meeting = json_decode($meetingJSON, true);

if (isset($_GET["load_meeting"])) {
    if (isset($meeting['date'])){
        $meeting['date'] = \Carbon\Carbon::parse($meeting['date']);

        if ($meeting['date']->isPast()){
            $meeting['date'] = "No Meeting Scheduled";
            $meeting['text'] = "";
        } else {
            $meeting['date'] = $meeting['date']->toDayDateTimeString();
        }
    }

    $response = [
        "success" => true,
        "meeting" => $meeting
    ];

    echo json_encode($response);
} elseif (isset($_GET["save_meeting"])){
    $meeting['date'] = $_GET["datetime"];
    $meeting['text'] = $_GET["text"];

    if ($meeting['date'] == "-- :"){
        $meeting['date'] = null;
    }

    if (file_put_contents(WIDGETS_DATA."company_meeting.json", json_encode($meeting))){
        $response = [
            "success" => true
        ];
    } else {
        $response = [
            "success" => true
        ];
    }

    echo json_encode($response);
}
