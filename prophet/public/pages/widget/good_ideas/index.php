<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET['id'])){

} else {
    if (isset($_GET['do'])){
        switch($_GET['do']){
            case "load":
                $response = [
                    "success" => true,
                    "data" => null,
                    "total" => 0
                ];

                $s = new Search(new GoodIdea());

                $s->eq("addedby", User::get_default_instance("id"));

                if (isset($_GET['status'])){
                    switch($_GET['status']){
                        case "pending":
                            $s->eq("implement", null);

                            break;

                        case "awaiting development":
                            $s->nt("implement", null);
                            $s->nt("implement", -1);
                            $s->eq("implemented", null);

                            break;

                        case "awaiting payment":
                            $s->nt("implemented", null);
                            $s->eq("paid", null);

                            break;

                        case "paid":
                            $s->nt("paid", null);

                            break;

                        case "rejected":
                            $s->eq("implement", -1);

                            break;
                    }
                }

                $s->add_order("addedwhen", "DESC");

                while($gi = $s->next(MYSQLI_ASSOC)){
                    $response['total']++;

                    $response['data'] .= "<tr><td>" . $gi->link() . "</td><td>" . $gi->addedwhen . "</td></tr>";
                }

                echo json_encode($response);

                break;
        }
    }
}
