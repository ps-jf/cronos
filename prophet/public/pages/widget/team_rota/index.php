<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["load_rota"])){
    $rotasJSON = file_get_contents(WIDGETS_DATA."rotas.json");
    $rotas = json_decode($rotasJSON, true);

    $userDept = User::get_default_instance("department");

    $myRota = "";

    foreach ($rotas as $rota){
        if ($rota["team"] == $userDept){
            $myRota = $rota["rota"];
        }
    }

    $response = [
        "success" => true,
        "rota" => $myRota
    ];

    echo json_encode($response);

} elseif (isset($_GET["save_rota"])){
    $rotasJSON = file_get_contents(WIDGETS_DATA."rotas.json");
    $rotas = json_decode($rotasJSON, true);

    $userDept = User::get_default_instance("department");

    $myRota = $_GET["rota"];

    $i = 0;

    foreach ($rotas as $rota){
        if ($rota["team"] == $userDept){
            $rotas[$i]["rota"] = $myRota;
        }
        $i++;
    }

    $rotasJSON = json_encode($rotas);

    if (file_put_contents(WIDGETS_DATA."rotas.json", $rotasJSON)){
        $response["success"] = true;
    } else {
        $response["success"] = false;
    }

    echo json_encode($response);
}
