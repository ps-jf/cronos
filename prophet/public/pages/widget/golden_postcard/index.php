<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["load_quote"])) {
    //get user statistics
    $quote = file_get_contents(WIDGETS_DATA."golden_postcard.txt");

    $response = [
        "success" => true,
        "data" => $quote
    ];

    echo json_encode($response);
} elseif (isset($_GET["save_quote"])){
    $quote = $_GET["quote"];

    if (file_put_contents(WIDGETS_DATA."golden_postcard.txt", $quote)){
        $response = [
            "success" => true
        ];
    } else {
        $response = [
            "success" => true
        ];
    }

    echo json_encode($response);
}
