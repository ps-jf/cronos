<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$todaysDate = date("d-m-Y");
//$todaysDate = date("d-m-Y", strtotime("2016-01-25"));

if (isset($_GET["user"])) {
    //get user statistics
    $mandatesJSON = file_get_contents(WIDGETS_DATA."mandates_user.json");
    $mandates = json_decode($mandatesJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."mandates_user.json"));

    $userID = User::get_default_instance("id");
    //$userID = 219; //Louisa Riddell TESTING

    $processedToday = 0;
    $processedWeek = 0;

    if(is_array($mandates)){
        foreach($mandates as $item){
            if ($item["id"] == $userID){
                if (date("d-m-Y", strtotime($item["sent"])) == $todaysDate){
                    $processedToday += $item["mandates"];
                }
                $processedWeek += $item["mandates"];
            }
        }
    }

    $response = [
        "success" => true,
        "processedToday" => $processedToday,
        "processedWeek" => $processedWeek,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);
} elseif (isset($_GET["dept"])){
    //get user statistics
    $mandatesJSON = file_get_contents(WIDGETS_DATA."mandates_team.json");
    $mandates = json_decode($mandatesJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."mandates_team.json"));

    $userDept = User::get_default_instance("department");
    //$userDept = 6; //Client Services TESTING

    $processedToday = 0;
    $processedWeek = 0;

    if(is_array($mandates)){
        foreach($mandates as $item){
            if ($item["id"] == $userDept){
                if (date("d-m-Y", strtotime($item["sent"])) == $todaysDate){
                    $processedToday += $item["mandates"];
                }
                $processedWeek += $item["mandates"];
            }
        }
    }

    $response = [
        "success" => true,
        "processedToday" => $processedToday,
        "processedWeek" => $processedWeek,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);
}
