<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."income"));

$userID = User::get_default_instance("id");
//$userID = 208; //Ryan Ewing

$processedCount = 0;
$processedTotal = 0;

//load the multiple files
foreach(glob(WIDGETS_DATA."income/*.json") as $file){
    $json = file_get_contents($file);
    $income = json_decode($json);

    if (is_array($income)){
        foreach($income as $item){
            if ((isset($_GET['user']) && $item->editedBy == $userID) || isset($_GET['dept'])){
                $processedCount++;
                $processedTotal += $item->amount;
            }
        }
    }
}

$processedCount = number_format($processedCount, false, false, ',');
$processedTotal = number_format($processedTotal, 2, '.', ',');

$response = [
    "success" => true,
    "processedCount" => $processedCount,
    "processedTotal" => $processedTotal,
    "lastUpdate" => $lastUpdate
];

echo json_encode($response);
