<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$todaysDate = date("d/m/Y");
//$todaysDate = date("d/m/Y", strtotime("03/01/2018"));

if (isset($_GET['type'])){
    $type = $_GET['type'];

    if ($type == "time_audit"){
        $response = [
            "success" => true,
            "rows" => [],
            "lastUpdate" => null
        ];

        $timeAuditJSON = file_get_contents(WIDGETS_DATA."time_audit.json");
        $timeAudit = json_decode($timeAuditJSON, true);

        $response['lastUpdate'] = date("H:i:s", filemtime(WIDGETS_DATA."time_audit.json"));

        foreach ($timeAudit as $ta){
            $response['rows'][] = "<tr>
                <td>" . $ta['user_name'] . "</td>
                <td>" . $ta['items'] . "</td>
                <td>" . $ta['avgTime'] . " seconds</td>
            </tr>";
        }

        echo json_encode($response);
    }
} else {
    if (isset($_GET["user"])) {
        //get user statistics
        $correspondenceJSON = file_get_contents(WIDGETS_DATA."correspondence_user.json");
        $correspondence = json_decode($correspondenceJSON, true);

        $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."correspondence_user.json"));

        $userID = User::get_default_instance("id");
        //$userID = 141; //Catherine McClelland

        $processedToday = 0;
        $processedWeek = 0;

        if (count($correspondence)){
            foreach($correspondence as $item){
                if ($item["sender"] == $userID){
                    if (date("d-m-Y", strtotime($item["processed_date"])) == $todaysDate){
                        $processedToday += $item["items"];
                    }
                    $processedWeek += $item["items"];
                }
            }
        }

        $response = [
            "success" => true,
            "processedToday" => $processedToday,
            "processedWeek" => $processedWeek,
            "lastUpdate" => $lastUpdate
        ];

        echo json_encode($response);
    } elseif (isset($_GET["dept"])){
        //get department statistics
        //get user statistics
        $correspondenceJSON = file_get_contents(WIDGETS_DATA."correspondence_team.json");
        $correspondence = json_decode($correspondenceJSON, true);

        $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."correspondence_team.json"));

        $teamID = User::get_default_instance("department");
        //$teamID = 6; //Client Services

        $processedToday = 0;
        $processedWeek = 0;

        if (count($correspondence)){
            foreach($correspondence as $item){
                if ($item["team"] == $teamID){
                    if ($item["processed_date"] == $todaysDate){
                        $processedToday += $item["items"];
                    }
                    $processedWeek += $item["items"];
                }
            }
        }

        $response = [
            "success" => true,
            "processedToday" => $processedToday,
            "processedWeek" => $processedWeek,
            "lastUpdate" => $lastUpdate
        ];

        echo json_encode($response);
    }
}
