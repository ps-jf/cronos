<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    if (isset($_GET['delete_widget'])){
        $widgetID = $_GET['id'];
        $row = $_GET['row'];
        $col = $_GET['col'];

        $userID = User::get_default_instance("id");

        $db = new mydb();

        $q = "DELETE FROM feebase.user_widgets WHERE user_id = $userID AND widget_id = $widgetID AND row = $row AND col = $col";

        $result = $db->query($q);

        $response = array("success" => $result);

        echo json_encode($response);
    } else {
        $row = $_GET["row"];
        $col = $_GET["col"];

        $widgetID = $_GET["id"];

        //look up widget ID in database
        $s = new Search(new Widget);
        $s->eq("widgetID", $widgetID);

        $template = "";

        if ($widget = $s->next(MYSQLI_ASSOC)){
            $template = $widget->template();

            if (isset($_GET['save_widget'])) {
                //save the widget in the user_widget table
                $userWidget = new UserWidgets();
                $userWidget->widget_id($widgetID);
                $userWidget->row($row);
                $userWidget->col($col);
                $userWidget->save();
            }
        }

        echo new Template($template);
    }
} elseif (isset($_GET["user_widgets"])){
    $response = [];

    // check Terry is online
    if ($_SESSION['terry']){
        //get all the widgets for this user
        $db = new mydb();

        $userID = User::get_default_instance("id");

        $q = "SELECT widgetID, row, col, width, height
          FROM feebase.user_widgets as uw JOIN feebase.widgets as w
            ON uw.widget_id = w.widgetID
          WHERE user_id = $userID";

        $db->query($q);

        while ($w = $db->next(MYSQLI_ASSOC)){
            $response[] = $w;
        }
    }

    echo json_encode($response);

    die();
} elseif (isset($_GET["widget_options"])){
    $response = [];

    if ($_SESSION['terry']) {
        //get all the widgets for this user
        $db = new mydb();

        $userID = User::get_default_instance("id");
        $userDepts = [User::get_default_instance("department")];

        $s = new Search(new UsersGroups());
        $s->eq("user", $userID);

        while($g = $s->next(MYSQLI_ASSOC)){
            $userDepts[] = $g->user_group();
        }

        //get all widgets which the user does not currently have
        $q = "SELECT * 
        FROM feebase.widgets
        WHERE widgetID NOT IN
            (SELECT widget_id 
            FROM feebase.user_widgets 
            WHERE user_id = $userID)
        ORDER BY title ASC";

        $db->query($q);

        while ($w = $db->next(MYSQLI_ASSOC)){
            $matchFound = false;

            if ((isset($w['global']) && $w['global'] == 1) || in_array(7, $userDepts)){
                $response[] = $w;
            } else {
                if (isset($w['department']) && isset($w['user'])){
                    $departmentsArray = explode(",", $w['department']);
                    $usersArray = explode(",", $w['user']);

                    foreach($departmentsArray as $department){
                        if (!$matchFound && in_array(str_replace(" ", "", $department), $userDepts)){
                            $matchFound = true;
                            $response[] = $w;
                        }
                    }

                    if (!$matchFound){
                        foreach($usersArray as $user){
                            if (!$matchFound && str_replace(" ", "", $user) == $userID){
                                $matchFound = true;
                                $response[] = $w;
                            }
                        }
                    }
                }elseif (isset($w['department'])){
                    $departmentsArray = explode(",", $w['department']);
                    foreach($departmentsArray as $department){
                        if (!$matchFound && in_array(str_replace(" ", "", $department), $userDepts)){
                            $matchFound = true;
                            $response[] = $w;
                        }
                    }
                } elseif (isset($w['user'])) {
                    $usersArray = explode(",", $w['user']);
                    foreach($usersArray as $user){
                        if (!$matchFound && str_replace(" ", "", $user) == $userID){
                            $matchFound = true;
                            $response[] = $w;
                        }
                    }
                }
            }
        }
    }

    echo json_encode($response);

    die();
} else {
    echo new Template("widget/systemUpdatesFeed.html");
}
