<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 07/08/2019
 * Time: 14:57
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$filename = "io_reports.json";

if (isset($_GET['monthly'])){
    $filename = "io_reports_monthly.json";
}

//get the data
$reportsJSON = file_get_contents(WIDGETS_DATA.$filename);
$reports = json_decode($reportsJSON, true);

$lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA.$filename));

$userID = User::get_default_instance("id");
//$userID = 177; //Caitlin Barker

$processed = 0;
$amount = 0;
$average = 0.00;

if (is_array($reports)){
    foreach($reports as $report){
        if ((isset($_GET['user']) && $report['sender'] == $userID) || isset($_GET['dept'])){
            $processed++;
            $amount += $report['amount'];
        }
    }
}

if ($processed > 0){
    $average = number_format($amount/$processed, 2, '.', ',');
}

$amount = number_format($amount, 2, '.', ',');

$response = [
    "success" => true,
    "processed" => $processed,
    "amount" => $amount,
    "average" => $average,
    "lastUpdate" => $lastUpdate,
];

echo json_encode($response);