<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["user"])){
    $helpdeskJSON = file_get_contents(WIDGETS_DATA."helpdesk_user.json");
    $helpdesk = json_decode($helpdeskJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."helpdesk_user.json"));

    $userName = User::get_default_instance("forename") . " " . User::get_default_instance("surname");
    //$userName = "Lewis Barbour";

    $userDept = User::get_default_instance()->department;

    if ($userDept == "Commission"){
        $userDept = "Finance";
    }

    $teamTicketsOpen = 0;
    $ticketsClosed = 0;
    $avgTime = "00:00:00";
    $overallAvgTime = "00:00:00";

    $helpdeskAvgJSON = file_get_contents(WIDGETS_DATA."helpdesk_user_avg.json");
    $helpdeskAvg = json_decode($helpdeskAvgJSON, true);

    $helpdeskOpenJSON = file_get_contents(WIDGETS_DATA."helpdesk_team_open.json");
    $helpdeskOpen = json_decode($helpdeskOpenJSON, true);

    if (count($helpdesk)){
        foreach ($helpdesk as $userStat){
            if ($userStat["name"] == $userName){
                $ticketsClosed = $userStat["tickets_closed"];
                $avgTime = $userStat["average"];
            }
        }
    } else {
        $ticketsClosed = "Data Fail";
        $avgTime = "Data Fail";
    }

    if (count($helpdeskAvg)){
        foreach ($helpdeskAvg as $userStat){
            if ($userStat["name"] == $userName){
                $overallAvgTime = $userStat["average"];
            }
        }
    } else {
        $overallAvgTime = "Data Fail";
    }

    if (count($helpdeskOpen)){
        foreach ($helpdeskOpen as $teamStat){
            if ($teamStat["name"] == $userDept){
                $teamTicketsOpen = $teamStat["tickets_open"];
            }
        }
    } else {
        $teamTicketsOpen = "Data Fail";
    }

    $response = [
        "success" => true,
        "teamOpen" => $teamTicketsOpen,
        "ticketsClosed" => $ticketsClosed,
        "avgTime" => $avgTime,
        "overallAvg" => $overallAvgTime,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);

} elseif (isset($_GET['team'])){
    $helpdeskJSON = file_get_contents(WIDGETS_DATA."helpdesk_team.json");
    $helpdesk = json_decode($helpdeskJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."helpdesk_team.json"));

    $userDept = User::get_default_instance()->department;
    //$userDept = "Client Services";

    if ($userDept == "Commission"){
        $userDept = "Finance";
    }

    $teamTicketsOpen = 0;
    $ticketsClosed = 0;
    $avgTime = "00:00:00";
    $overallAvgTime = "00:00:00";

    $helpdeskAvgJSON = file_get_contents(WIDGETS_DATA."helpdesk_team_avg.json");
    $helpdeskAvg = json_decode($helpdeskAvgJSON, true);

    $helpdeskOpenJSON = file_get_contents(WIDGETS_DATA."helpdesk_team_open.json");
    $helpdeskOpen = json_decode($helpdeskOpenJSON, true);

    if (count($helpdesk)){
        foreach ($helpdesk as $teamStat){
            if ($teamStat["name"] == $userDept){
                $ticketsClosed = $teamStat["tickets_closed"];
                $avgTime = $teamStat["average"];
            }
        }
    } else {
        $ticketsClosed = "Data Fail";
        $avgTime = "Data Fail";
    }

    if (count($helpdeskAvg)){
        foreach ($helpdeskAvg as $teamStat){
            if ($teamStat["name"] == $userDept){
                $overallAvgTime = $teamStat["average"];
            }
        }
    } else {
        $overallAvgTime = "Data Fail";
    }

    if (count($helpdeskOpen)){
        foreach ($helpdeskOpen as $teamStat){
            if ($teamStat["name"] == $userDept){
                $teamTicketsOpen = $teamStat["tickets_open"];
            }
        }
    } else {
        $teamTicketsOpen = "Data Fail";
    }

    $response = [
        "success" => true,
        "teamOpen" => $teamTicketsOpen,
        "ticketsClosed" => $ticketsClosed,
        "avgTime" => $avgTime,
        "overallAvg" => $overallAvgTime,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);
}
