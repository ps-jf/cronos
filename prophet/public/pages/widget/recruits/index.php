<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["user"])){
    $recruitsJSON = file_get_contents(WIDGETS_DATA."recruits.json");
    $recruits = json_decode($recruitsJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."recruits.json"));

    $userID = User::get_default_instance("id");
    //$userID = 201; //Lauren Campbell TESTING

    $recruits1 = 0;
    $recruits2 = 0;
    $recruits3 = 0;
    $recruits4 = 0;

    foreach($recruits as $recruit){
        if ($recruit["ps_manager"] == $userID){
            switch ($recruit["trans_type"]){
                case 1:
                    $recruits1 = $recruit["recruits"];
                    break;

                case 2:
                    $recruits2 = $recruit["recruits"];
                    break;

                case 3:
                    $recruits3 = $recruit["recruits"];
                    break;

                case 4:
                    $recruits4 = $recruit["recruits"];
                    break;
            }
        }
    }

    $response = [
        "success" => true,
        "recruits1" => $recruits1,
        "recruits2" => $recruits2,
        "recruits3" => $recruits3,
        "recruits4" => $recruits4,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);

} elseif (isset($_GET["team"])){
    $recruitsJSON = file_get_contents(WIDGETS_DATA."recruits.json");
    $recruits = json_decode($recruitsJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."recruits.json"));

    $recruits1 = 0;
    $recruits2 = 0;
    $recruits3 = 0;
    $recruits4 = 0;

    foreach($recruits as $recruit){
        switch ($recruit["trans_type"]){
            case 1:
                $recruits1 += $recruit["recruits"];
                break;

            case 2:
                $recruits2 += $recruit["recruits"];
                break;

            case 3:
                $recruits3 += $recruit["recruits"];
                break;

            case 4:
                $recruits4 += $recruit["recruits"];
                break;
        }
    }

    $response = [
        "success" => true,
        "recruits1" => $recruits1,
        "recruits2" => $recruits2,
        "recruits3" => $recruits3,
        "recruits4" => $recruits4,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);
}
