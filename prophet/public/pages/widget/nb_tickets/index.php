<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$todaysDate = date("d-m-Y");
//$todaysDate = date("d-m-Y", strtotime("05/01/2018"));

if (isset($_GET["user"])){
    $nbTicketJSON = file_get_contents(WIDGETS_DATA."nb_ticket.json");
    $nbTickets = json_decode($nbTicketJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."nb_ticket.json"));

    $userID = User::get_default_instance("id");
    //$userID = 191; //Josh Wilson

    $myTickets = [];

    foreach($nbTickets as $ticket){
        if ($ticket["addedBy"] == $userID){
            $myTickets[] = $ticket;
        }
    }

    $response = [
        "success" => true,
        "myTickets" => $myTickets,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);

} elseif (isset($_GET["team"])){
    $nbTicketJSON = file_get_contents(WIDGETS_DATA."nb_ticket.json");
    $nbTickets = json_decode($nbTicketJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."nb_ticket.json"));

    $todaysTickets = [];

    foreach($nbTickets as $ticket){
        if (date("d-m-Y", strtotime($ticket["added"])) == $todaysDate){
            $todaysTickets[] = $ticket;
        }
    }

    $response = [
        "success" => true,
        "todaysTickets" => $todaysTickets,
        "weekTickets" => $nbTickets,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);
}
