<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["user"])){
    $servicedeskJSON = file_get_contents(WIDGETS_DATA."itsd.json");
    $servicedesk = json_decode($servicedeskJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."itsd.json"));

    $teamTicketsOpen = $servicedesk['open'];
    $ticketsClosed = 0;
    $avgTime = "00:00:00";
    $overallAvgTime = "00:00:00";

    $todayTimes = [];
    $times = [];

    if (count($servicedesk['closed_since_pm'])){
        foreach ($servicedesk['closed_since_pm'] as $issue){
            if(User::get_default_instance("email") === $issue['email']){
                if(\Carbon\Carbon::parse($issue['resolved'])->isToday()){
                    $ticketsClosed++;
                    $todayTimes[] = $issue['timeSpent'];
                }
                $times[] = $issue['timeSpent'];
            }
        }

        if(count($todayTimes)){
            $avgTime = gmdate("H:i:s", intval(array_sum($todayTimes)/count($todayTimes)));
        }

        if(count($times)){
            $overallAvgTime = gmdate("H:i:s", intval(array_sum($times)/count($times)));
        }
    }

    $response = [
        "success" => true,
        "teamOpen" => $teamTicketsOpen,
        "ticketsClosed" => $ticketsClosed,
        "avgTime" => $avgTime,
        "overallAvg" => $overallAvgTime,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);

} elseif (isset($_GET['team'])){
    $servicedeskJSON = file_get_contents(WIDGETS_DATA."itsd.json");
    $servicedesk = json_decode($servicedeskJSON, true);

    $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."itsd.json"));

    $teamTicketsOpen = $servicedesk['open'];
    $ticketsClosed = $servicedesk['closed_today'];
    $avgTime = "00:00:00";
    $overallAvgTime = "00:00:00";

    $todayTimes = [];
    $times = [];

    if (count($servicedesk['closed_since_pm'])){
        foreach ($servicedesk['closed_since_pm'] as $issue){
            if(\Carbon\Carbon::parse($issue['resolved'])->isToday()){
                $todayTimes[] = $issue['timeSpent'];
            }
            $times[] = $issue['timeSpent'];
        }

        if(count($todayTimes)){
            $avgTime = gmdate("H:i:s", intval(array_sum($todayTimes)/count($todayTimes)));
        }

        if(count($times)){
            $overallAvgTime = gmdate("H:i:s", intval(array_sum($times)/count($times)));
        }
    }

    $response = [
        "success" => true,
        "teamOpen" => $teamTicketsOpen,
        "ticketsClosed" => $ticketsClosed,
        "avgTime" => $avgTime,
        "overallAvg" => $overallAvgTime,
        "lastUpdate" => $lastUpdate
    ];

    echo json_encode($response);
}
