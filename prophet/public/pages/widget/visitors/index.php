<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["load_visitors"])){
    $visitorsJSON = file_get_contents(WIDGETS_DATA."visitors.json");
    $visitors = json_decode($visitorsJSON, true);

    $changedDay = false;

    $today = date("D");

    if (in_array($today, ["Sat", "Sun"])){
        $today = "Mon";
    }

    //tidy array on every search
    $i = 0;
    while ($visitors[$i]["day"] != $today){
        $changedDay = true;
        //remove this day from the array
        $newDay = [
            "day" => $visitors[$i]["day"],
            "visitors" => 0
        ];

        unset($visitors[$i]);

        array_push($visitors, $newDay);

        //increment
        $i++;
    }

    if ($changedDay){
        //do last
        $visitorsJSON = json_encode(array_values($visitors));
        file_put_contents(WIDGETS_DATA."visitors.json", $visitorsJSON);
    }

    $response = [
        "success" => true,
        "data" => $visitors
    ];

    echo json_encode($response);

} elseif (isset($_GET['save_visitors'])){
    $visitors = $_GET['visitors'];

    file_put_contents(WIDGETS_DATA."visitors.json", $visitors);

    $response = [
        "success" => true
    ];

    echo json_encode($response);
}
