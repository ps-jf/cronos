<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

use Carbon\Carbon;

if (isset($_GET['get'])){
    switch($_GET['get']){
        case "daily_count":
            $response = [
                'success' => false,
                'rows' => false,
            ];

            $records = json_decode(file_get_contents(WIDGETS_DATA."proreport_daily_count.json"), true);
            $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."proreport_daily_count.json"));

            $rows = [];

            foreach($records as $record){
                $rows[] = "<tr>
                        <td>" . $record['user_name'] . "</td>
                        <td>" . $record['reports'] . "</td>
                        <td>&pound;" . $record['invoiced_amount'] . "</td>
                    </tr>";
            }

            $response = [
                'success' => true,
                'rows' => $rows,
                'lastUpdate' => $lastUpdate
            ];

            echo json_encode($response);

            break;
    }
} else {
    if (isset($_GET["type"])) {
        $type = $_GET['type'];
        $repsonse = [
            'success' => false,
            'rows' => false
        ];

        switch ($type){
            case "single":
                $invoicesJSON = file_get_contents(WIDGETS_DATA."single_invoices.json");
                $classes = "";
                $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."single_invoices.json"));
                break;
            case "overdue":
                $invoicesJSON = file_get_contents(WIDGETS_DATA."overdue_invoices.json");
                $lastUpdate = date("H:i:s", filemtime(WIDGETS_DATA."overdue_invoices.json"));
                $classes = "table-danger";
                break;
            default:
                echo json_encode($response);
                exit(0);
                break;
        }

        $invoices = json_decode($invoicesJSON, true);

        $rows = [];

        foreach($invoices as $invoice){
            $dueDate = date("U", strtotime($invoice['due_date']));

            $amount = number_format((float)$invoice['amount'], 2, '.', ',');

            $dueDate = Carbon::createFromTimestamp($dueDate);
            $dueDate = $dueDate->toFormattedDateString();

            $rows[] = "<tr class='" . $classes . "'>
                <td>" . $invoice['invoice_number'] . "</td>
                <td>" . $invoice['po_number'] . "</td>
                <td>" . $dueDate . "</td>
                <td>£" . $amount . "</td>
            </tr>";
        }

        $response = [
            "success" => true,
            "rows" => $rows,
            "lastUpdate" => $lastUpdate
        ];

        echo json_encode($response);
    }
}
