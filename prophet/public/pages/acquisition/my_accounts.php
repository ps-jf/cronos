<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET["do"])) {
    switch ($_GET['do']) {
        case "list_prospects":

            $json = (object)[
                "data" => null,
                "status" => null
            ];
            
            $db = new mydb();
            $q = "SELECT * FROM pm_prospects
						WHERE ps_manager = " . $_GET['user'] . " AND partner_id IS NULL AND status not in (3, 4, 5)
						AND hidden != 1
						ORDER BY surname ASC";
            $db->query($q);

            $row_t = Template("acquisition/list_row_prospect.html");

            while ($prospect = $db->next(MYSQLI_ASSOC)) {
                $row_t->tag($prospect, "prospect");
                $prospects .= $row_t;
            }

            if (isset($prospects)) {
                $json->data = $prospects;
            }

            $json->status = true;

            echo json_encode($json);

            break;

        case "list_partners":
            $json = (object)[
                "data" => null,
                "status" => null,
                "pcomplete" => null
            ];

            $db = new mydb();
            $q = "SELECT * FROM tblpartner
						WHERE (tblpartner.ps_manager = " . $_GET['user'] . " AND bulkTransferComplete = 1 AND bulkpartner = -1)
						OR (tblpartner.ps_manager = " . $_GET['user'] . " AND bulkTransferComplete = 4 AND bulkpartner = -1)";
            $db->query($q);

            $row_t = Template("acquisition/list_row_partner.html");

            while ($partner = $db->next(MYSQLI_ASSOC)) {
                $row_t->tag($partner, "partner");
                $partners .= $row_t;
            }

            if (isset($partners)) {
                $json->data = $partners;
            }

            $json->status = true;

            echo json_encode($json);
            break;
    }
} else {
    $acqStaff = [];

    $s = new Search(new User);
    $s->eq("department", 4);
    $s->eq("active", 1);
    $s->add_order("username");
    while ($u = $s->next(MYSQLI_ASSOC)) {
        $acqStaff[] = $u;
    }

    echo new Template("acquisition/my_accounts.html", ["acqStaff" => $acqStaff]);
}

