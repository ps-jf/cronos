<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET["get"])) {
    switch ($_GET["get"]) {
        case "policies":
            // build a dropdown list for issuer
            $db = new mydb();
            try {
                $db->query(
                    "select issuerid,issuername from ".ISSUER_TBL." order by issuername"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }
  
            $drop = "<option id='0'>- -</option>";
            while ($iss = $db->next()) {
                $drop .= "<option id='".$iss['issuerid']."' value='".$iss['issuerid']."'>".$iss['issuername']."</option>";
            }

            $db = new mydb();
            try {
                $db->query(
                    "select id, issuername from ".POLICY_IMPORT_TBL." where issuer_id = 0 group by issuername order by issuername"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }

            $r = [];
            $i = 0;
            
            while ($iss = $db->next()) {
                $r["results"][$i]["name"] = $iss["issuername"];
                $r["results"][$i]["id"] = $iss["id"];
                $i++;
            }
             $r["drop"] = $drop;
            
            echo json_encode($r);

            break;
        
        case "poltype":
            $db = new mydb();
            try {
                $db->query(
                    "select id, policytype from ".POLICY_IMPORT_TBL." group by policytype order by policytype"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }

            $r = [];
            $i = 0;

            $rows = "";
            
            while ($pol = $db->next()) {
                //we must use ob to stop the suggest element from returning too early
                ob_start();

                $p = new Policy;

                el($p->type, $pol['id']);

                $polTypes = ob_get_contents();

                ob_end_clean();

                $rows .= "<tr><td>" . $pol['policytype'] . "</td><td>" . $polTypes . "</td></tr>";

            }
            
            echo json_encode($rows);

            break;
        
        case "agencies":
            $db = new mydb();
            try {
                $db->query(
                    "SELECT issuerName FROM ".POLICY_TBL." INNER JOIN ".CLIENT_TBL.
                            " ON ".CLIENT_TBL.".clientid = ".POLICY_TBL.".clientid".
                            " INNER JOIN ".ISSUER_TBL.
                            " ON ".POLICY_TBL.".issuerid = ".ISSUER_TBL.".issuerid".
                            " WHERE partnerid = ".$_GET["pid"]." AND bulkclient = 'x'".
                            " GROUP BY ".POLICY_TBL.".issuerid".
                            " ORDER BY issuername"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            if ($db-> next() !== false) {
                while ($i = $db->next()) {
                    echo "<tr><td>".$i["issuerName"]."</td></tr>";
                }
            } else {
                return false;
            }
        
            break;
        
        case "issuers":
            $db = new mydb();
            try {
                $db->query(
                    "SELECT ".ISSUER_TBL.".issuername FROM ".POLICY_IMPORT_TBL.
                        " INNER JOIN ".ISSUER_TBL.
                        " ON ".ISSUER_TBL.".issuerid = ".POLICY_IMPORT_TBL.".issuer_id".
                        " INNER JOIN ".CLIENT_IMPORT_TBL.
                        " ON ".CLIENT_IMPORT_TBL.".id = ".POLICY_IMPORT_TBL.".import_id".
                        " WHERE partner_id = ".$_GET["pid"].
                        " GROUP BY ".ISSUER_TBL.".issuername"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            echo "<thead><th>Imported Issuers</th></thead><tbody>";

            while ($i = $db->next()) {
                echo "<tr><td>".$i["issuername"]."</td></tr>";
            }
            echo "</tbody>";
        
            break;
    }
} elseif (isset($_GET["do"])) {
    switch ($_GET["do"]) {
        case "match":
            $db = new mydb();

            $errors = [];
            
            foreach ($_POST as $k => $v) {
                if ($v) {
                    //firstly take the select id to find the policy type
                    try {
                        $db->query(
                            "select policytype from ".POLICY_IMPORT_TBL." WHERE id = ".$k
                        );
                    } catch (Exception $e) {
                        $errors[] = $e->getMessage();
                    }
                    
                    $policy = $db -> next(MYSQLI_NUM);
                    
                    //then update the id of all the policy types with that name
                    try {
                        $db->query(
                            "UPDATE ".POLICY_IMPORT_TBL." SET policytype_id = ".$v." WHERE policytype = '".$policy[0]."'"
                        );
                    } catch (Exception $e) {
                        $errors[] = $e->getMessage();
                    }
                } else {
                    $errors[] = "Non numeric policytypeid(not possible)";
                }
            }
            

            // build a dropdown list for issuer
            $db = new mydb();
            try {
                $db->query(
                    "select issuerid,issuername from ".ISSUER_TBL." order by issuername"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }
  
            $drop = "<option id='0'>- -</option>";
            while ($iss = $db->next()) {
                $drop .= "<option id='".$iss['issuerid']."' value='".$iss['issuerid']."'>".$iss['issuername']."</option>";
            }

            $db = new mydb();
            try {
                $db->query(
                    "select id, issuername from ".POLICY_IMPORT_TBL." where issuer_id = 0 group by issuername order by issuername"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }

            $r = [];
            $i = 0;
            
            while ($iss = $db->next()) {
                $r["results"][$i]["name"] = $iss["issuername"];
                $r["results"][$i]["id"] = $iss["id"];
                $i++;
            }
             $r["drop"] = $drop;
            
            if (count($errors) > 0) {
                echo json_encode($errors);
            } else {
                echo json_encode($r);  //Changed 'true' to r
            }
        
            break;
        
        case "matchissuers":
            //this is quite a horrible way of doing this, but it replicates the behaviour(?) of access(!) quite well
            //here we loop through the assignments and update the table
            $db = new mydb();

            $errors = [];

            foreach ($_POST as $k => $v) {
                if (is_numeric($v)) {
                    //firstly take the select id to find the issuer name
                    try {
                        $db->query(
                            "select issuername from ".POLICY_IMPORT_TBL." WHERE id = ".$k
                        );
                    } catch (Exception $e) {
                        $errors[] = $e->getMessage();
                    }
                    
                    $issuer = $db -> next(MYSQLI_NUM);

                    //then update the id of all the issuers with that name
                    try {
                        $db->query(
                            "UPDATE ".POLICY_IMPORT_TBL." SET issuer_id = ".$v." WHERE issuername = '".$issuer[0]."'"
                        );
                    } catch (Exception $e) {
                        $errors[] = $e->getMessage();
                    }
                } else {
                    $errors[] = "Non numeric issuerid(not possible)";
                }
            }
            
            if (count($errors) > 0) {
                echo json_encode($errors);
            } else {
                echo json_encode('true');  //Changed 'true' to r
            }
        
            break;
        
        case "add_agencies":
            $r = [];
        
            // add any agencies automatically since the transfer is a bingle
            $db = new mydb();
            $db1 = new mydb();

            try {
                $db->query("SELECT clientID, clientSname1 FROM ".CLIENT_TBL." WHERE BulkClient = 'x' And partnerid = ".$_GET["pid"]);
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            $res = $db -> next(MYSQLI_ASSOC);
            
            $cli = $res["clientID"];
            $cliname = $res["clientSname1"];
            
            if ($cli == "" || is_null($cli)) {
                $r["error"] = true;
                $r["message"]  = "Could not add agencies: No bulk client";
                
                echo json_encode($r);
                die();
            }

            try {
                $db->query(
                    "select issuer_id from ".POLICY_IMPORT_TBL.
                            " inner join ".CLIENT_IMPORT_TBL.
                            " on ".CLIENT_IMPORT_TBL.".id = ".POLICY_IMPORT_TBL.".import_id".
                            " where partner_id = ".$_GET["pid"].
                            " group by issuer_id"
                );
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            while ($iss = $db -> next(MYSQLI_ASSOC)) {
                try {
                    $db1->query("INSERT INTO ".POLICY_TBL."(clientid,issuerid,policybatched,policytypeid,status,
                    policynum,policysubmitted,addedwhen,added_how,addedby) VALUES
                    (".$cli.", ".$iss["issuer_id"].", 1, 27, 16, '".$cliname."', '".date("Y-m-d")."', ".time().", 5,
                    ".$_COOKIE["uid"].")");

                    // Just stick to raw SQL for now
                    $pid = $db1->query("SELECT LAST_INSERT_ID()");

                    $p = new Policy($pid, true);

                    if ($p->addedhow() == 5 && $p->addedby() == $_COOKIE["uid"]){
                        //run the on_create method to catch anything we missed
                        $p->on_create();
                    }

//                    $p->client($cli);
//                    $p->issuer($iss["issuer_id"]);
//                    $p->bulk(1);
//                    $p->type(27);
//                    $p->status(16);
//                    $p->number($cliname);
//                    $p->submitted(date("Y-m-d"));
//                    $p->addedwhen(time());
//                    $p->addedhow(5);
//                    $p->addedby($_COOKIE["uid"]);
//
//                    if ($p->save()){
//                        $p->on_create();
//                    } else {
//                        throw new Exception("Failed to create new policy");
//                    }
                } catch (Exception $e) {
                    $r["error"] = true;
                    $r["message"]  = $e->getMessage();
                }
            }
            
            if (count($r) > 0) {
                echo json_encode($r);
            } else {
                echo json_encode(true);
            }
        
            break;
        
        case "import":
            $db = new mydb();
            $db2 = new mydb();
            $r = [];
            
            //add clients from temp table to real table
            try {
                $db->query("SELECT * FROM " . CLIENT_IMPORT_TBL . " WHERE partner_id = " . $_GET['pid']);

                $clicount = 0;

//                while($ci = $db->next(MYSQLI_ASSOC)){
//                    $client = new Client();
//
//                    if($ci['dob1'] == "0000-00-00"){
//                        $ci['dob1'] = null;
//                    }
//
//                    if($ci['dob2'] == "0000-00-00"){
//                        $ci['dob2'] = null;
//                    }
//
//                    $client->partner($ci['partner_id']);
//
//                    $client->one->title($ci['title1']);
//                    $client->one->forename($ci['fname1']);
//                    $client->one->surname($ci['sname1']);
//                    $client->one->dob($ci['dob1']);
//                    $client->one->email($ci['email1']);
//                    $client->one->nino($ci['nino1']);
//
//                    $client->two->title($ci['title2']);
//                    $client->two->forename($ci['fname2']);
//                    $client->two->surname($ci['sname2']);
//                    $client->two->dob($ci['dob2']);
//                    $client->two->email($ci['email2']);
//                    $client->two->nino($ci['nino2']);
//
//                    $client->address->line1($ci['add1']);
//                    $client->address->line2($ci['add2']);
//                    $client->address->line3($ci['add3']);
//                    $client->address->postcode($ci['postcode']);
//
//                    $client->phone($ci['telno']);
//                    $client->pure_protection($ci['pure_protection']);
//
//                    $client->import_id($ci['id']);
//                    $client->addedby(71);
//                    $client->addedhow(4);
//                    $client->addedwhen(date("U"));
//
//                    $client->salutation($client->generate_salutation($client));
//
//                    $client->save();
//
//                    $clicount++;
//                }

                // TODO check for client age, but watch out for trusts
                $db->query(
                    "INSERT INTO ".CLIENT_TBL." (partnerid,clientfname1,clientsname1,clientdob1,
                            clientfname2,clientsname2,clientdob2,
                            clientaddress1,clientaddress2,clientaddress3,clientaddresspostcode,
                            clienttelno,clientnino1,clientnino2,clienttitle1,clienttitle2,
                            email1,email2, pure_protection,
                            tempclientidforimport,addedby,added_how,addedwhen)
                            SELECT partner_id,fname1,sname1,dob1,
                            fname2,sname2,dob2,
                            add1,add2,add3,postcode,
                            telno,nino1,nino2,title1,title2,
                            email1,email2, pure_protection,
                            id,71,4,".time().
                            " FROM ".CLIENT_IMPORT_TBL." WHERE partner_id = ".$_GET["pid"]
                            #AND (dob1 is null or dob1 = "0000-00-00" or timestampdiff(YEAR, dob1, CURDATE()) > 13)
                            #AND (dob2 is null or dob2 = "0000-00-00" or timestampdiff(YEAR, dob2, CURDATE()) > 13)
                );

                $clicount = $db -> affected_rows;
                $r["clients_added"] = $clicount;
            } catch (Exception $e) {
                $r["clients_added"] = false;
                $r["message"] = $e->getMessage();
                echo json_encode($r);
                die();
            }
            
           // add all policies from temp table to real table
            try {
                $db->query("SELECT issuer_id, policynum, clientid, ".time().", policytype_id, 71, 1, 1, 4, adviser_charge, owner
                            FROM ".CLIENT_TBL." INNER JOIN ".POLICY_IMPORT_TBL." ON ".CLIENT_TBL.".tempclientidforimport = ".POLICY_IMPORT_TBL.".import_id");

                $blockedIssuers = [];
                $issuersBlocked = [];

                $i = 0;

                $blockedIssuers = array_merge($blockedIssuers, [233, 11612, 11866]); //Standard Life

                while ($ac = $db -> next(MYSQLI_ASSOC)) {
                    if (!in_array($ac['issuer_id'], $blockedIssuers)){
                        //removes everything but numbers, letters and periods
                        $ac['adviser_charge'] = preg_replace('/[^a-zA-Z0-9\.]/', '', $ac['adviser_charge']);
                        if (is_numeric($ac['adviser_charge'])) {
                            $charging = $ac['adviser_charge'];
                        } else {
                            $charging = null;
                        }

                        // newer imports should have a policy owner set by the script
                        if (empty($ac['owner'])){
                            // older imports might not have an owner set
                            $client1 = $client2 = $joint = $owner = false;

                            $c = new Client($ac['clientid'], true);

                            // check whether we have client 1/2/both on record
                            if (($c->one->forename() != "" && $c->one->forename() != null) || ($c->one->surname() != "" && $c->one->surname() != null)) {
                                $client1 = true;
                            }

                            if (($c->two->forename() != "" && $c->two->forename() != null) || ($c->two->surname() != "" && $c->two->surname() != null)) {
                                $client2 = true;
                            }

                            if ($client1 == true && $client2 == true) {
                                $joint = true;
                            }

                            // depending on which client details we have, set policy owner flag
                            if ($joint == true) {
                                $owner = 3;
                            } elseif ($client1 == true) {
                                $owner = 1;
                            } elseif ($client2 == true) {
                                $owner = 2;
                            } else {
                                $owner = 3;
                            }
                        } else {
                            $owner = $ac['owner'];
                        }

                        $db2->query("INSERT INTO ".POLICY_TBL." ( issuerID, policyNum, clientID, addedWhen, policyTypeID, addedby, Status, policyBatched,added_how,ac_percent,owner )
                                VALUES ( ".$ac['issuer_id'].", '".$ac['policynum']."', ".$ac['clientid'].", ".time().", ".$ac['policytype_id'].", 71, 1, 1, 4, '".$charging."','".$owner."')");

                        // Just stick to raw SQL for now
                        $pid = $db2->query("SELECT LAST_INSERT_ID()");

                        $p = new Policy($pid, true);

                        if ($p->addedhow() == 4 && $p->addedby() == 71){
                            //run the on_create method to catch anything we missed
                            $p->on_create();
                        }

//                        $p = new Policy();
//
//                        $p->issuer($ac['issuer_id']);
//                        $p->number($ac['policynum']);
//                        $p->client($ac['clientid']);
//                        $p->addedwhen(time());
//                        $p->type($ac['policytype_id']);
//                        $p->addedby(71);
//                        $p->status(1);
//                        $p->bulk(1);
//                        $p->addedhow(4);
//                        $p->ac_percent($charging);
//                        $p->owner($owner);
//
//                        if ($p->save()){
//                            $p->on_create();
//                        } else {
//                            throw new Exception("Failed to save new policy");
//                        }

                        $i++;
                    } else {
                        $issuer = new Issuer($ac['issuer_id'], true);

                        $issuersBlocked[] = $issuer->name;
                    }
                }

                $r["policies_added"] = $i;

                if (count($issuersBlocked)){
                    $r['blocked'] = "Policies from " . implode(", ", array_unique($issuersBlocked)) . " are currently blocked.";
                }
            } catch (Exception $e) {
                $r["policies_added"] = false;
                $r["message"] = $e->getMessage();
                echo json_encode($r);
                die();
            }
            
            
            // audit the addition of clients
            try {
                $db->query(
                    "INSERT INTO `".DATA_DB."`.tblimportaudit( partnerID, numberOfClients, numberOfPolicies, addedBy, addedWhen, filePath )VALUES(".
                            $_GET["pid"].",".$clicount.",".$r['policies_added'].",".$_COOKIE["uid"].",'".date("Y-m-d")."','SEE python script for a clue')"
                );
                            
                $r["audited"] = true;
            } catch (Exception $e) {
                $r["audited"] = false;
                $r["message"] = $e->getMessage();
                echo json_encode($r);
                exit();
            }
            
            
            // add client serviving level for csrd
            try {
                $db2 = new mydb(REMOTE, E_USER_WARNING);
                if (mysqli_connect_errno()) {
                    $r["message"] = "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno().", Servicing level not added.";
                    echo json_encode($r);
                    exit();
                }
                
                $db->query("SELECT feebase.tblclient.clientid, feebase.tblclient.partnerid, feebase.tblclient.addedwhen, feebase.client_import_holding.servlevel ".
                            "FROM feebase.tblclient ".
                            "INNER JOIN feebase.client_import_holding ".
                            "ON feebase.client_import_holding.id = feebase.tblclient.tempclientidforimport ".
                            "WHERE feebase.tblclient.partnerid = ".$_GET["pid"]." ".
                            "ORDER BY feebase.tblclient.clientid desc ".
                            "LIMIT ".$clicount);

                
                while ($sl = $db -> next(MYSQLI_ASSOC)) {
                    $cli_id = $sl['clientid'];
                    $partner_id = $sl['partnerid'];
                    $serv_level = $sl['servlevel'];

                    if (trim($serv_level) != '') {
                        $db2->query("INSERT INTO `".REMOTE_SYS_DB."`.client_servicing( client_id, servicing_level, level_by, level_when ) VALUES(".
                                    $cli_id.",'".trim($serv_level)."',".$partner_id.",".time().")");
                    }
                }
            } catch (Exception $e) {
                echo $e->getMessage();
                die();
            }
            
            
            
            echo json_encode($r);
        
            break;
    }
} else {
    $db = new mydb();
    try {
        $db->query(
            "select partner_id from ".CLIENT_IMPORT_TBL." limit 0,1"
        );
    } catch (Exception $e) {
        die($e->getMessage());
    }
    $p = $db -> next(MYSQLI_ASSOC);
    
    $p = new Partner($p["partner_id"], true);

    echo new Template("acquisition/import.html", ['partner'=>$p]);
}
