<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $agency = new Agency($_GET["id"], true);
    $policy = new Policy($_GET["id"], true);
    $issuer = new Issuer();

    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "save":
                # save changes to new business case + return status to browser
                $agency -> load($_POST);
                echo json_encode($agency -> save());

                break;
        }
    } elseif (isset($_GET["get"])) {
        switch ($_GET["get"]) {
            case "agencies":
                $db = new mydb();
                
                $db -> query("SELECT ".POLICY_TBL.".policyID, ".POLICY_TBL.".old_bulk_policy, ".POLICY_TBL.".agency_mandate, ".
                POLICY_TBL.".next_chase_date, ".POLICY_TBL.".short_bulk_policy, ".POLICY_STATUS_TBL.".Field1 AS Status".
                " FROM ".POLICY_STATUS_TBL." INNER JOIN ((".CLIENT_TBL." INNER JOIN ".POLICY_TBL." ON ".CLIENT_TBL.".clientID = ".POLICY_TBL.".clientID)".
                " INNER JOIN ".ISSUER_TBL." ON ".POLICY_TBL.".issuerID = ".ISSUER_TBL.".issuerID) ON ".POLICY_STATUS_TBL.".ID = ".POLICY_TBL.".Status".
                " WHERE (((".CLIENT_TBL.".partnerID)='".$_GET['partner']."') AND ((".POLICY_TBL.".issuerid)='".$_GET['issuer']."') AND ((".CLIENT_TBL.".BulkClient)='x'))".
                " ORDER BY ".ISSUER_TBL.".issuerName");
                
                while ($d=$db->next()) {
                    if ($d['agency_mandate']) {
                        $transfer_type = "Agency - Mandate";
                        $update_btn = "<button href='/pages/acquisition/agency_update.php?agency_mandate=true&id=".$d['policyID']."'>Update</button>";
                    } else {
                        $transfer_type = "Agency";
                        $update_btn = "<button href=\"/pages/acquisition/agency_update.php?id=".$d['policyID']."\">Update</button>";
                    }

//                    echo "<tr>
//					<td>".$policy->link($d['old_bulk_policy'], $policy->id($d['policyID']))."</td>
//					<td>".$d['Status']."</td>
//					<td>".$d['short_bulk_policy']."</td>
//					<td>".$transfer_type."</td>
//					<td>".$d['next_chase_date']."</td>
//					<td>".$update_btn."
//					</td>
//					<td><button href=\"/pages/acquisition/add_comm.php?id=".$d['policyID']."\">Add</button></td>
//					</tr>";

                    echo "<tr>
					<td>".$policy->link($d['old_bulk_policy'], $policy->id($d['policyID']))."</td>
					<td>".$d['Status']."</td>
					<td>".$d['short_bulk_policy']."</td>
					<td>".$transfer_type."</td>
					<td>".$d['next_chase_date']."</td>
					<td>".$update_btn."
					</td>
					</tr>";
                }

                break;
        
            case "corr":
                $db = new mydb();

                $db -> query("select subject, client, policy, issuername, ".ISSUER_TBL.".issuerid, datestamp, corrid, message".
                     " from ".CORR_TBL.
                     " inner join ".POLICY_TBL.
                     " on ".CORR_TBL.".policy = ".POLICY_TBL.".policyid".
                     " inner join ".ISSUER_TBL.
                     " on ".POLICY_TBL.".issuerid = ".ISSUER_TBL.".issuerid".
                     " where partner = '".$_GET['partner']."' and ".POLICY_TBL.".issuerid = '".$_GET['issuer']."' and ".POLICY_TBL.".policytypeid = 27".
                     " order by datestamp desc");

                    echo "<script type='text/css'> div#agency_tabs table.scrollable div.scroll { height: 30px; } </script>";
                    echo "<table name='corr_items' id='corr_items' class='scrollable'>";
                    echo "<thead><th>Subject</th><th>Client</th><th>Policy</th><th>Issuer</th><th>Date</th><th>ID</th><th>Scanned</th></thead>";
            
                    $corr = new Correspondence();

                while ($r=$db->next()) {
                     echo "<tr>
			     <td>".$corr->link($r['subject'], $corr->id($r['corrid']))."</td>
			     <td>".$r['client']."</td>
			     <td>".$policy->link($r['policy'], $policy->id($r['policyid']))."</td>
			     <td>".$issuer->link($r['issuername'], $issuer->id($r['issuerid']))."</td>
			     <td>".$r['datestamp']."</td>
			     <td>".$r['corrid']."</td>
			     <td><img src=".el::image(($corr->scan_exists()) ? 'tick.png' : 'cross.png'). "></td>
			     </tr>".
                     "<tr><td colspan='6'>".$r['message']."</td></tr>";
                }

                    echo "</table>";

                break;
        }
    } else {
        echo new Template("acquisition/agency.html", ["agency"=>$agency]);
    }
}
