<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    try {
        $b = new BulkDetails($_GET["id"], true);

        $b->load($_POST['bulkdetails']);
        $b->save();

        echo("Details Updated Successfully");
    } catch (Exception $e) {
        echo($e->getMessage());
    }
}
