<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    if (isset($_GET["get"])) {
        switch ($_GET["get"]) {
            case "profile":
                $i = new Issuer($_GET['id'], true);

                $t = new Template('acquisition/profile_byissuer.html');
                $t -> tag($i, 'issuer');
                
                echo $t;
                
                break;
    
    
            case "agencies":
                $policy = new Policy();
                $partner = new Partner();
                
                $db = new mydb();
            
                $db -> query("SELECT ".POLICY_TBL.".policyID, ".PARTNER_TBL.".partnerid, ".ISSUER_TBL.".issuerName,".ISSUER_TBL.".issuerCode, ".POLICY_TBL.".old_bulk_policy, ".PARTNER_TBL.".partnersname,".
                POLICY_TBL.".dateservtrfd, ".PARTNER_TBL.".partnerfname,".POLICY_TBL.".next_chase_date, ".POLICY_TBL.".short_bulk_policy, ".POLICY_STATUS_TBL.".Field1 AS Status".
                " FROM ".POLICY_STATUS_TBL." INNER JOIN ((".CLIENT_TBL." INNER JOIN ".POLICY_TBL." ON ".CLIENT_TBL.".clientID = ".POLICY_TBL.".clientID)".
                " INNER JOIN ".ISSUER_TBL." ON ".POLICY_TBL.".issuerID = ".ISSUER_TBL.".issuerID INNER JOIN ".PARTNER_TBL." ON ".CLIENT_TBL.".partnerid = ".PARTNER_TBL.".partnerid) ON ".POLICY_STATUS_TBL.".ID = ".POLICY_TBL.".Status".
                " WHERE (((".POLICY_TBL.".issuerid)='".$_GET['id']."') AND ((".CLIENT_TBL.".BulkClient)='x'))".
                " ORDER BY ".ISSUER_TBL.".issuerName");
             
                while ($d=$db->next()) {
                    //re-format date to allow column sorting (yyyy-mm-dd to dd/mm/yyyy)
                    $newDate = preg_replace("/(\d{4})-(\d{2})-(\d{2})/", "$3/$2/$1", $d['next_chase_date']);
                    //dont show irrelevant default date
                    if ($newDate == "00/00/0000") {
                        $newDate = "";
                    }
                    
                    echo "<tr>
					<td>".$policy->link($d['old_bulk_policy'], $policy->id($d['policyID']))."</td>
					<td>".$partner->link($d['partnerfname']. " " . $d['partnersname'], $partner->id($d['partnerid']))."</td>
					<td>".$d['Status']."</td>
					<td>".$d['short_bulk_policy']."</td>
					<td>".$d['issuerCode']."</td>
					<td>".$newDate."</td>
					<td><button href=\"/pages/acquisition/agency.php?id=".$d['policyID']."\">Details</button></td>
					</tr>";
                }
            
                break;
        }
    }
} else {

    function issuer_name_search(&$search, $str)
    {
        $search->eq($search->object->name, "%$str[text]%", true);
    }

    $search = new Search(new Issuer);
    
    $search
        -> flag("id", Search::DEFAULT_INT)
        -> flag("name", Search::DEFAULT_STRING, Search::CUSTOM_FUNC, "issuer_name_search");

    if (isset($_GET["search"])) {
        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);

        $matches = [];

        while ($match = $search->next($_GET["search"])) {
            // append to matches
            $matches[ "results" ][] = ['value'=> "$match->id", 'text'=>"$match->name"];
        }
        
        $matches["remaining"] = $search->remaining;
        
        echo json_encode($matches);
    } else {
        echo new Template("generic/search.html", ["help"=>$search->help_html(), "object"=>$search->get_object(), "url"=>$_SERVER["PHP_SELF"]]);
    }
}
