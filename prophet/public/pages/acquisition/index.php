<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $partner = new Partner;
    $partner->primary_key($_GET["id"]);

    // New objects to help create links to object popups
    $issuer = new Issuer;
    $policy = new Policy;

    if (isset($_GET["get"])) {
        switch ($_GET["get"]) {
            case "profile":
                $partner->get();
                echo Template('acquisition/profile.html', ["partner" => $partner]);

                break;

            case "status":
                $db = new mydb();
                $db->query("SELECT Count(" . POLICY_TBL . ".policyID) AS `count`, " . POLICY_STATUS_TBL . ".Field1 AS `field`, " . POLICY_TBL . ".status" .
                    " FROM " . POLICY_STATUS_TBL .
                    " INNER JOIN (" . POLICY_TBL . " INNER JOIN " . CLIENT_TBL . " ON " . POLICY_TBL . ".clientID = " . CLIENT_TBL . ".clientID) ON " . POLICY_STATUS_TBL . ".ID = " . POLICY_TBL . ".Status" .
                    " WHERE (((" . CLIENT_TBL . ".BulkClient)='x') AND ((" . CLIENT_TBL . ".partnerID)='" . $partner->id() . "'))" .
                    " GROUP BY " . POLICY_STATUS_TBL . ".Field1, " . POLICY_TBL . ".Status");

                $transcount = 0;
                $count = 0;
                $content = "";


                while ($row = $db->next(MYSQLI_ASSOC)) {
                    if ($row["status"] == 13 || $row["status"] == 22 || $row["status"] == 17 || $row["status"] == 14 || $row["status"] == 23 || $row["status"] == 28) {
                        $transcount = $transcount + $row["count"];
                    }

                    $content .= "<tr><td>" . $row["field"] . "</td><td>" . $row["count"] . "</td></tr>";
                    $count = $count + $row["count"];
                }

                echo json_encode([
                    'content' => $content,
                    'pccomplete' => ($transcount == 0 || $count == 0) ? "0%" : number_format((($transcount / $count) * 100), 0) . "%",
                    'count' => $count
                ]);

                break;

            case "agencies":
                $agencyissuerlist = [];
                $clientsbyissuer = [];

                $s = new Search(new Agency);
                $s->eq("client->partner", $partner->id());
                $s->eq("client->bulk", "x");
                $s->add_order("issuer->name");

                while ($d = $s->next()) {
                    $agencyissuerlist[] = $d->issuer->id;
                }

                //array of issuers which whould normally appear in the agency list
                $agencyissuerlist = array_unique($agencyissuerlist);

                //clients by issuer query for comparison
                // necessary objects
                $i = new Issuer;
                $c = new Client;
                $p = new Policy;
                $pa = new Partner($partner->id(), true);

                $db = new mydb();
                $q = "SELECT DISTINCT " . $i->id->get_field_name() . ", " . $i->name->get_field_name() . " " .
                    "FROM " . $c->get_table() . " " .
                    "LEFT JOIN " . $p->get_table() . " ON " . $c->id->get_field_name() . " = " . $p->client->get_field_name() . " " .
                    "INNER JOIN " . $i->get_table() . " ON " . $p->issuer->get_field_name() . " = " . $i->id->get_field_name() . " " .
                    "WHERE " . $c->partner->get_field_name() . " = " . $partner->id() . " 
				AND tblissuer.issuerID != 1197 AND tblissuer.issuerID != 1045 
				AND tblissuer.issuerID != 1254 AND tblissuer.issuerID != 11391
				GROUP BY " . $i->id->get_field_name() . " " .
                    "ORDER BY " . $i->name->get_field_name();

                $db->query($q);

                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $clientsbyissuer[] = $row['issuerID'];
                }

                //automatic adding of agency
                if ($pa->transtype() == 4) {
                    //compare original agency issuer list with the issuers in clients by issuer that would appear on parnter profile
                    $difference = array_diff($clientsbyissuer, $agencyissuerlist);

                    if ($difference || !$agencyissuerlist) {
                        if ($difference) {
                            $difference = array_values($difference);
                        } else {
                            $difference = $clientsbyissuer;
                        }

                        $i = 0;
                        foreach ($difference as $value) {
                            try {
                                $db = new mydb();

                                $db->query("select clientid from " . CLIENT_TBL . " where partnerid = " . $_GET["id"] . " and bulkclient = 'x'");

                                if (!$d = $db->next(MYSQLI_ASSOC)) {
                                    //if no bulk client, add so that we can add agencies
                                    $client = new Client();
                                    $client->one->surname("*Agency:");
                                    $client->partner($_GET["id"]);
                                    $client->bulk("x");
                                    $client->save();

                                    $q = "SELECT clientID FROM " . CLIENT_TBL . " WHERE partnerID = " . $_GET['id'] . " and bulkclient = 'x'";
                                    $db->query($q);

                                    while ($row = $db->next(MYSQLI_ASSOC)) {
                                        $clientid = $row['clientID'];
                                    }
                                } else {
                                    $clientid = $d["clientid"];
                                }

                                $policy = new Agency();
                                $policy->client($clientid);
                                $policy->number("*Agency: " . addslashes($agency[$i]));
                                $policy->type(27);
                                $policy->status(16);
                                $policy->issuer($difference[$i]);
                                $policy->bulk(true);
                                $policy->addedby(User::get_default_instance('id'));
                                $policy->agency_mandate(1);
                                $policy->save();

                                $i++;
                            } catch (Exception $e) {
                                var_dump($e->getMessage());
                            }
                        }
                    }
                }

                //output results, including any newly added agencies
                $s = new Search(new Agency);
                $s->eq("client->partner", $partner->id());
                $s->eq("client->bulk", "x");
                $s->add_order("issuer->name");

                while ($d = $s->next()) {
                    //hack We don't want an agency profile here, we want a policy
                    //simulate the link method
                    if ($d->old_bulk == "") {
                        $old_bulk = "???";
                    } else {
                        $old_bulk = $d->old_bulk;
                    }

                    echo "<tr>
				<td>" . $d->issuer->link() . "</td>
				<td><a class='profile Policy' target='_BLANK' href='/common/Policy/?id=$d->id'><i>" . $old_bulk . "</i></a></td>
				<td>" . $d->submitted . "</td>
				<td>" . $d->status . "</td>
				<td>" . $d->transferred . "</td>
				<td>";
                    if ($d->agency_mandate() == 0) {
                        echo 'Agency';
                    } else {
                        echo 'Agency - Mandate';
                    }
                    echo "</td>
				<td>" . $d->short_bulk . "</td>
				<td>" . $d->next_chase . "</td>
				<td>";
                    if ($d->agency_mandate() == 0) {
                        echo "<button id='$d->id' class='agencyup'>Update</button>";
                    } else {
                        echo "<button id='$d->id' class='agencyup agency_mandate'>Update</button>";
                    }
                    echo "</td><td>";
                    if ($d->agency_mandate() == 0) {
                        echo "<button href='/pages/acquisition/agency.php?id=" . $d->id . "'>Details</button>";
                    } else {
                        echo "<button href='/pages/acquisition/agency.php?agency_mandate=true&id=" . $d->id . "'>Details</button>";
                    }
                    echo "</td></tr>";
                }

                break;

            case "chaseups":
                $s = new Search(new BulkChaseAudit);
                $s->eq("partner_id", $partner->id());
                $s->add_order("week");

                while ($d = $s->next()) {
                    echo "<tr id='chaserow_$d->id' name='chaserow_$d->id'>
				<td>" . $d->week . "</td>
				<td>" . $d->chase_date . "</td>
				<td>" . $d->report_sent . "</td>
				<td>" . $d->pcomplete . "</td>
				<td>" . $d->helpdesk . "</td>
				<td><input type='submit' id='$d->id' name='$d->id' class='update' value='Update' /></td>
				<td><button id='$d->id' name='$d->id' class='delete' /></td>
				</tr>";
                }

                break;

            case "chase_update":
                $b = new BulkChaseAudit($_GET['id'], true);
                echo new Template("acquisition/chase_update.html", ["id" => $_GET['partnerID'], "chaseID" => $_GET['id'], "b" => $b]);

                break;

            case "sub_agencies":
                $json = [
                    "issuers" => null,
                    "data" => null,
                    "agencies" => null,
                ];

                $db = new mydb();

                $partner = new Partner($partner->id(), true);

                if ($partner->practice_bubble()) {
                    $q = "SELECT DISTINCT po.issuerID, i.issuerName FROM tblpolicy po
                        INNER JOIN tblclient c
                          ON po.clientID = c.clientID
                        INNER JOIN tblpartner pa
                          ON c.partnerID = pa.partnerID
                        INNER JOIN tblissuer i
                          ON po.issuerID = i.issuerID
                        INNER JOIN practice_staff ps
                            ON pa.partnerid = ps.partner_id
                        INNER JOIN practice p
                            ON ps.practice_id = p.id
                        WHERE p.id = " . $partner->get_practice()->id . "
                            AND policyTypeID = 27
                            AND po.issuerID != 11764
                            AND po.issuerID != 11391
                            AND po.issuerID != 1197
                            AND po.issuerID != 1254
                            AND po.agency_mandate = 1
                        ORDER BY i.issuerName ASC";
                } else {
                    $q = "SELECT DISTINCT po.issuerID, i.issuerName FROM tblpolicy po
                        INNER JOIN tblclient c
                          ON po.clientID = c.clientID
                        INNER JOIN tblpartner pa
                          ON c.partnerID = pa.partnerID
                        INNER JOIN tblissuer i
                          ON po.issuerID = i.issuerID
                        WHERE pa.partnerID = " . $partner->id() . "
                            AND policyTypeID = 27
                            AND po.issuerID != 11764
                            AND po.issuerID != 11391
                            AND po.issuerID != 1197
                            AND po.issuerID != 1254
                            AND po.agency_mandate = 1
                        ORDER BY i.issuerName ASC";
                }

                $db->query($q);

                $issuers_dropdown = "<select id='issuers' name='issuers'>";

                $issuers = [];
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $issuers[$row['issuerID']] = $row['issuerName'];
                    $issuers_dropdown .= "<option value='" . $row['issuerID'] . "'>" . $row['issuerName'] . "</option>";
                }

                reset($issuers);
                $key = key($issuers);

                $issuers_dropdown .= "</select>";

                $agencies = [];

                if ($key){
                    $q1 = "SELECT * FROM tblpolicy po
					INNER JOIN tblclient c
						ON po.clientID = c.clientID
					INNER JOIN tblpartner pa
						ON c.partnerID = pa.partnerID
					WHERE pa.partnerID = " . $partner->id() . "
					AND po.issuerID = " . $key . "
					AND policyTypeID != 27";

                    $db->query($q1);

                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        $c = new Client($row['clientID'], true);
                        $p = new Policy($row['policyID'], true);
                        $a = new Policy($p->agency_id, true);

                        $data .= "<tr><td><input id='" . $p->id() . "' type='checkbox'/></td><td>" . $c->link() . "</td><td>" . $p->link() . "</td><td>" . $p->status . "</td><td>" . $a->link() . "</td></tr>";
                    }

                    if (!isset($_GET['issuer'])) {
                        $issuerID = $key;
                    } else {
                        $issuerID = $_GET['issuer'];
                    }

                    // Check if practice link flag is set on partner account and return full agency list based on single account
                    // or all accounts within practice accordingly, for the chosen issuer
                    if ($partner->practice_bubble()) {
                        $q = "SELECT * FROM tblpolicy po
					INNER JOIN tblclient c
						ON po.clientID = c.clientID
					INNER JOIN tblpartner pa
						ON c.partnerID = pa.partnerID
					INNER JOIN practice_staff ps
					    ON pa.partnerid = ps.partner_id
					INNER JOIN practice p
					    ON ps.practice_id = p.id
					WHERE p.id = " . $partner->get_practice()->id . "
					AND policyTypeID = 27 AND issuerID = " . $issuerID . " AND po.agency_mandate = 1";
                    } else {
                        $q = "SELECT * FROM tblpolicy po
					INNER JOIN tblclient c
						ON po.clientID = c.clientID
					INNER JOIN tblpartner pa
						on c.partnerID = pa.partnerID
					WHERE pa.partnerID = " . $partner->id() . " AND policyTypeID = 27 AND issuerID = " . $issuerID . " AND po.agency_mandate = 1";
                    }

                    // Populate agency select
                    $db->query($q);

                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        if ($row['short_bulk_policy'] == "") {
                            $display_name = $row['policyNum'];
                        } else {
                            $display_name = $row['short_bulk_policy'];
                        }
                        $agencies[$row['policyID']] = $display_name;
                    }
                }

                if (!isset($data)) {
                    $data = "<tr><td>No policies available under this issuer</td></tr>";
                }


                $json['issuers'] = $issuers_dropdown;
                $json['agencies'] = $agencies;
                $json['data'] = $data;
                echo json_encode($json);

                break;

            case "change_issuer":
                $json = [
                    "data" => null
                ];

                $db = new mydb();

                $partner = new Partner($partner->id(), true);

                $q = "SELECT * FROM tblpolicy po
					INNER JOIN tblclient c
						ON po.clientID = c.clientID
					INNER JOIN tblpartner pa
						ON c.partnerID = pa.partnerID
					WHERE pa.partnerID = " . $partner->id() . "
					AND po.issuerID = " . $_GET['issuer'] . "
					AND policyTypeID != 27";

                $db->query($q);

                while ($row = $db->next(MYSQLI_ASSOC)) {
                    if ($row['agency_id'] == null) {
                        $row['agency_id'] = "--";
                    }

                    $c = new Client($row['clientID'], true);
                    $p = new Policy($row['policyID'], true);
                    $a = new Policy($p->agency_id, true);

                    $data .= "<tr><td><input id='" . $p->id() . "' type='checkbox'/></td><td>" . $c->link() . "</td><td>" . $p->link() . "</td><td>" . $p->status . "</td><td>" . $a->link() . "</td></tr>";
                }

                if ($data == null) {
                    $data = "<tr><td>No policies available under this issuer</td></tr>";
                }

                // Check if practice link flag is set on partner account and return full agency list based on single account
                // or all accounts within practice accordingly, for the chosen issuer
                if ($partner->practice_bubble()) {
                    $q = "SELECT * FROM tblpolicy po
					INNER JOIN tblclient c
						ON po.clientID = c.clientID
					INNER JOIN tblpartner pa
						ON c.partnerID = pa.partnerID
					INNER JOIN practice_staff ps
					    ON pa.partnerid = ps.partner_id
					INNER JOIN practice p
					    ON ps.practice_id = p.id
					WHERE p.id = " . $partner->get_practice()->id . "
					AND policyTypeID = 27 AND po.agency_mandate = 1 AND issuerID = " . $_GET['issuer'];
                } else {
                    $q = "SELECT * FROM tblpolicy po
					INNER JOIN tblclient c
						ON po.clientID = c.clientID
					INNER JOIN tblpartner pa
						on c.partnerID = pa.partnerID
					WHERE pa.partnerID = " . $partner->id() . " AND po.agency_mandate = 1 AND policyTypeID = 27 AND issuerID = " . $_GET['issuer'];
                }

                $db->query($q);

                $agencies = [];
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    if ($row['short_bulk_policy'] == "") {
                        $display_name = $row['policyNum'];
                    } else {
                        $display_name = $row['short_bulk_policy'];
                    }
                    $agencies[$row['policyID']] = $display_name;
                }

                $json['data'] = $data;
                $json['agencies'] = $agencies;

                echo json_encode($json);

                break;

            case "update_selected":
                $json = [
                    "error" => false,
                    "status" => null
                ];

                if (isset($_GET['policies']) && isset($_GET['agency'])) {
                    try {
                        $i = 0;

                        foreach ($_GET['policies'] as &$value) {
                            $p = new Policy($value, true);
                            $p->agency_id($_GET['agency']);
                            $p->save();

                            $i = $i + 1;
                        }

                        $json['status'] = "Agency for selected Policies have been successfully updated";
                    } catch (Exception $e) {
                        $json['error'] = true;
                        $json['status'] = $e;
                    }

                    echo json_encode($json);
                }

                break;

            case "satisfaction_call":
                $partner = $_GET['id'];
                $manager = $_GET['manager'];
                $u = new User($manager, true);

                // Check if call already exists for partner and echo relevant template
                $s = new Search(new BulkSatisfactionCall);
                $s->eq("partnerid", $partner);
                $s->add_order("id", "DESC");
                $s->limit(1);

                // If call already stored, update call
                if ($x = $s->next(MYSQLI_ASSOC)) {
                    echo new Template("acquisition/satisfaction_call_update.html", ["partner" => $partner, "manager" => $u, "id" => $x->id]);
                } // else create new call record
                else {
                    echo new Template("acquisition/satisfaction_call.html", ["partner" => $partner, "manager" => $u]);
                }

                break;

            case "letter_options":
                $p = new Partner($_GET['id'], true);
                $i = new Issuer($_GET['issuer'], true);
                echo new Template("acquisition/letter_options.html", ["partner" => $p, "issuer" => $i, "letter" => $_GET['letter']]);
                break;

            case "agency_mandate_chase":
                $json = [
                    "error" => null,
                    "status" => null
                ];

                function format_fax($fax, $efax = true)
                {
                    if ($fax == "") {
                        return $fax;
                    }
                    $fax = trim($fax);
                    $fax = str_replace(" ", "", $fax);

                    if (strpos($fax, '+', 0) == 0) {
                        //this is an international number
                        $returnfax = substr($fax, 1);
                    }
                    if (strpos($fax, '0', 0) == 0) {
                        //replace 0 with UK dialing code
                        $returnfax = "44" . substr($fax, 1);
                    }
                    if ($efax == true) {
                        //append the eFax domain to allow fax/email to send
                        return $returnfax . "@efaxsend.com";
                    } else {
                        return $returnfax;
                    }
                }

                //removes 'all' from array
                $_GET['issuer'] = array_diff($_GET['issuer'], ['all']);

                $partners = [];

                if ($_GET['level'] == "practice") {
                    $s = new Search(new PracticeStaffEntry());
                    $s->eq("practice", $_GET['practice']);
                    while ($r = $s->next(MYSQLI_ASSOC)) {
                        //dont add null as partnerID when a staff member is present
                        if ($r->partner() != null) {
                            $partners[] = $r->partner();
                        }
                    }
                } else {
                    $partners[] = $_GET['id'];
                }

                /*  check to ensure we are only adding entries for partners that have clients with
                policies under specified issuer(s) where servicing transferred and agency_id = null */
                $checked = [];
                $error = false;

                foreach ($partners as $p_id) {
                    foreach ($_GET['issuer'] as $issuer) {
                        $i = new Issuer((int)$issuer, true);

                        $db = new mydb();
                        $q = "SELECT DISTINCT " . CLIENT_TBL . ".partnerID, " . ISSUER_TBL . ".issuerID
                            FROM " . POLICY_TBL . "
                            INNER JOIN " . CLIENT_TBL . "
                                ON " . POLICY_TBL . ".clientID = " . CLIENT_TBL . ".clientID
                            INNER JOIN " . ISSUER_TBL . "
                                ON " . POLICY_TBL . ".issuerID = " . ISSUER_TBL . ".issuerID
                            WHERE " . CLIENT_TBL . ".partnerID = " . $p_id . "
                            AND " . POLICY_TBL . ".Status = 2
                            AND " . POLICY_TBL . ".agency_id IS NULL
                            AND " . ISSUER_TBL . ".issuerID = " . $i->id();
                        $db->query($q);

                        while ($row = $db->next(MYSQLI_ASSOC)) {
                            $checked[] = $row;
                        }
                    }
                }

                /* create empty array which is used to show issuer and partner that do not have an agency
                 that has  agency-mandate set to true*/
                $fail_array = [];

                if (!empty($checked)) {
                    foreach ($checked as $value) {
                        $part = (int)$value['partnerID'];
                        $iss = (int)$value['issuerID'];

                        $db = new mydb();
                        $q = "SELECT DISTINCT " . POLICY_TBL . ".policyID
                        FROM " . POLICY_TBL . "
                        INNER JOIN " . CLIENT_TBL . "
                            ON " . POLICY_TBL . ".clientID = " . CLIENT_TBL . ".clientID
                        INNER JOIN " . ISSUER_TBL . "
                            ON " . POLICY_TBL . ".issuerID = " . ISSUER_TBL . ".issuerID
                        WHERE " . CLIENT_TBL . ".partnerID = " . $part . "
                        AND " . POLICY_TBL . ".agency_mandate = 1
                        AND " . POLICY_TBL . ".issuerID = " . $iss;

                        $db->query($q);

                        $row = $db->next(MYSQLI_ASSOC);
                        if ($row['policyID'] != "") {
                            $amc = new AgencyMandateChase();
                            $i = new Issuer($issuer, true);

                            $amc->partner($part);
                            $amc->issuer($iss);
                            $amc->letter($_GET['letter']);

                            if ($_GET['fax_email'] == "fax") {
                                $send_to = format_fax($_GET['fax']);
                            } else if ($_GET['fax_email'] == "email") {
                                $send_to = $_GET['email'];
                            }

                            if (!isset($_GET['fax_email'])) {
                                //Get the default contact
                                $default_add = Issuer::get_contact($iss, "group_id", 4);
                                $send_to = $default_add->email();
                            }

                            $amc->send_to($send_to);

                            if ($_GET['letter'] == 1) {
                                $amc->subject("Set up Agency Request");
                                $amc->message("Please find attached documentation regarding set up of agency. If there are any problems please give Policy Services a call on 0345 450 7806.");
                            } else if ($_GET['letter'] == 2) {
                                $amc->subject("Transfer into Agency Request");
                                $amc->message("Please find attached documentation regarding transfer of agency. If there are any problems please give Policy Services a call on 0345 450 7806.");
                            }

                            if ($send_to != "") {
                                if ($amc->save()) {
                                    $json['error'] = false;
                                    $json['status'] = "Successfully added to to database.";
                                } else {
                                    exit("An error occurred when trying to write to the Database, contact IT");
                                }
                            }
                        } else {
                            $fail_array[$iss] = $part;
                            $error = true;
                            $json['status'] = "No Agency of type Agency-Mandate found under this issuer";
                            $json['error'] = true;
                        }
                    }
                } else {
                    $json['status'] = "There are no Agencies under this Issuer that are of type Agency-Mandate";
                    $json['error'] = true;
                }

                /* if an error has has taken place at any point we want the return status to show what issuer/partner
                caused the issue to occur  */
                if ($error == true) {
                    $json['error'] = true;
                    $json['status'] = "No Agency of type Agency-Mandate found under the following:\n\n";
                    while (list($key, $val) = each($fail_array)) {
                        $issuer_obj = new Issuer($key, true);
                        $partner_obj = new Partner($val, true);
                        $json['status'] .= "- Issuer: " . $issuer_obj->name() . ", Partner: " . $partner_obj->__toString() . "\n\n";
                    }
                    $json['status'] .= "\nPlease add a new Agency that is of type Agency-Mandate for the above Issuers / Partners then proceed with the letter creation at an individual Issuer & Partner level.";
                }

                echo json_encode($json);

                break;
        }
    }
} else {

    function search_on_name(&$search_obj, $text)
    {

        $x = parse_name($text["text"]);
        $partner = $search_obj->get_object();

        if ($x["wildcard"] == "forename") {
            $x["forename"] .= "%";
        } else {
            $x["surname"] .= "%";
        }

        $conditions = [];

        if (isset($x["forename"])) {
            $conditions[] = $search_obj->eq($partner->forename, $x["forename"], ($x["wildcard"] == "forename"), false);
        }

        $conditions[] = $search_obj->eq($partner->surname, $x["surname"], ($x["wildcard"] == "surname"), false);
        $search_obj->add_and($conditions);
    }

    $partner = new Partner;
    $search = new Search($partner);
    $search->set_flags([
        ["id", Search::DEFAULT_INT],
        ["name", Search::CUSTOM_FUNC, "search_on_name", Search::DEFAULT_STRING],
        ["sjp", "agencyCode", Search::PATTERN, "/^\d{6}[A-Z]+$/"]
    ]);

    if (isset($_GET["search"])) {
        $s = $_GET["search"];

        if (strlen($s) == 0) {
            exit();
        }

        $matches = [];

        $search->eq("bulk", -1);

        $search->set_limit(SEARCH_LIMIT);
        $search->set_offset((int)$_GET["skip"]);

        while ($match = $search->next($s)) {
            $matches["results"][] = ["text" => "$match", "value" => "$match->id"];
        }

        $matches["remaining"] = $search->remaining;

        echo json_encode($matches);
    } else {
        echo new Template("acquisition/search.html", ["help" => $search->help_html(), "url" => "pages/acquisition/?get=profile"]);
    }
}
