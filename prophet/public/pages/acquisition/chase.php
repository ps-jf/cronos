<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

function format_fax($fax, $efax = true)
{
    if ($fax == "") {
        return $fax;
    }
    $fax = trim($fax);
    $fax = str_replace(" ", "", $fax);
    
    if (strpos($fax, '+', 0)==0) {
        //this is an international number
        $returnfax = substr($fax, 1);
    }
    if (strpos($fax, '0', 0)==0) {
        //replace 0 with UK dialing code
        $returnfax = "44" . substr($fax, 1);
    }
    if ($efax==true) {
        //append the eFax domain to allow fax/email to send
        return $returnfax . "@efaxsend.com";
    } else {
        return $returnfax;
    }
}


if (isset($_GET["id"])) {
    $agency = new Agency($_GET["id"], true);
    $partner = new Partner($agency->client->partner->id(), true);
    $issuer = new Issuer($agency -> issuer -> id(), true);
    
    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "submit":
                try {
                    $contact_name = $_POST['contact_name'];
                    $send_to = $_POST['send_to'];
                    $subject = $_POST['subject'];
                    $message = $_POST['message'];

                    $bingle = ($partner->transtype() == 2) ? 1 : 0;

                    $print = (isset($_GET['submit_type']) && $_GET['submit_type'] == 'print') ? 1 : 0;

                    $return = [
                        "id"     => false,
                        "status" => false,
                        "error"  => false,
                        "print"  => false,
                        "bingle" => false
                    ];

                    // Check to see if real submit or preview
                    if ($print === 1) {
                        $return['print'] = true;
                    }
                        
                    if (!strrpos($send_to, '@', 0)) {
                        $send_to = format_fax($send_to);
                    }

                    $db = new mydb();

                    if ($db -> query("insert into ".CHASE_TBL."(contact_name, send_to, subject, message, addedby, policyid, bingle, sent, addedwhen) "
                                      ."values('".addslashes($contact_name)."', '".addslashes($send_to)."', '".addslashes($subject)." to ".$issuer->name." for (".preg_replace("/[^a-zA-Z0-9]/", "",$partner->forename)." ".addslashes($partner->surname).")', '".addslashes($message)."', ".$_COOKIE["uid"].", ".$agency->id().", ".$bingle.", ".$print.", ".time().")")) {
                        $return["id"] = $db->insert_id;

                    //update all agencies for partner under this provider to awaiting confirmation
                        $db -> query("UPDATE ".POLICY_TBL." INNER JOIN ".CLIENT_TBL." ON ".CLIENT_TBL.".clientid = ".POLICY_TBL.".clientid SET status = 12,policySubmitted = '".date("Y-m-d H:i:s")."' WHERE partnerid = ".$partner->id()." AND ".POLICY_TBL.".policytypeid = 27 AND ".POLICY_TBL.".status = 16 AND issuerid = ".$agency->issuer->id());


                        $return["status"] = true;
                        $return["bingle"] = $bingle;
                    }


                    echo json_encode($return);
                } catch (Exception $e) {
                    die($e -> getMessage());
                }

                break;
        }
    } else {
        echo new Template("acquisition/chase.html", ["agency"=>$agency,"partner"=>$partner, "default_add"=>$issuer->get_contact($issuer->id(), "group_id", 4)]);
    }
} elseif (isset($_GET["partnerid"])) {
    try {
        $partner = new BulkPartner($_GET["partnerid"], true);
            
        $bingle = ($partner->transtype() == 2) ? 1 : 0;
        
        $default_msg = 'Please find attached documentation regarding transfer of agency. If there are any problems please give Policy Services a call on 0345 450 7806.';
            
        $return = [
            "number" => false,
            "total"  => false,
            "status" => false,
            "error"  => false,
            "orig"   => false
        ];
            
        $db = new mydb();
        $db2 = new mydb();

        $db -> query("SELECT policyid,issuerid, ".CLIENT_TBL.".clientid".
                    " from ".POLICY_TBL.
                    " inner join ".CLIENT_TBL.
                    " on ".POLICY_TBL.".clientid = ".CLIENT_TBL.".clientid".
                    " where ".CLIENT_TBL.".partnerid = ".$partner->id()." and ".POLICY_TBL.".policytypeid = 27".
                    " and ".POLICY_TBL.".status = 16 and ".CLIENT_TBL.".bulkclient = 'x' group by ".POLICY_TBL.".issuerid");
                            
        $counter = 0;
        $submitted = 0;

        while ($all = $db -> next(MYSQLI_ASSOC)) {
            $iss = new Issuer($all['issuerid'], true);

            $default_sub = "Agency Transfer Request to " . $iss->name . ", " . $partner->forename ." ". $partner->surname;

            //Get the contact
            $default_add = $iss->get_contact($iss->id(), "group_id", 4);

            $contact_name = $default_add->contact->value;
        

            //Use best means of contact details
            $send_to = ($default_add->email_preferred->value == 1) ? $default_add->email->value : format_fax($default_add->fax->value);

            if ($send_to == '' || $send_to == '@efaxsend.com') {
                $return['error'][] = $iss->name();
            } else {
                if ($default_add->originals_required() == true) {
                    $return['orig'][] = $iss->name();
                    $return['error'][] = $iss->name();
                } else {
                    if ($db2 -> query("INSERT into ".CHASE_TBL."(contact_name, subject, message, policyid, send_to, addedby, bingle, addedwhen, sent_time)".
                              " values ('".addslashes($contact_name)."', '".addslashes($default_sub)."', '".addslashes($default_msg)."', ".addslashes($all['policyid']).", '".addslashes($send_to)."', ".$_COOKIE["uid"].", ".$bingle.", ".time().", 0)")) {
                        //this agency has essentially been "submitted" change to awaiting confirmation
                        $db2 -> query("UPDATE ".POLICY_TBL." set status = 12,policySubmitted = '".date("Y-m-d H:i:s")."' where issuerid = ".$all['issuerid']." and clientid = ".$all['clientid']." and ".POLICY_TBL.".status = 16 and ".POLICY_TBL.".policytypeid = 27");
                        $submitted++;
                    }
                }
            }

            $counter++;
        }
            
        if ($counter > 0 && ($partner->bulkstartdate == "" || $partner->bulkstartdate == null || $partner->bulkstartdate == "0000-00-00")) {
            //this transfer has started, update it accordingly if there were submissions and it has never been filled out
            $db2 -> query("UPDATE ".PARTNER_TBL." set bulkTransferComplete = 1,bulkStartDate ='".date("Y-m-d")."' WHERE partnerid = ".$partner->id());
        }

        $return["number"] = $submitted;
        $return["total"] = $counter;
        $return["status"] = true;

        echo json_encode($return);
    } catch (Exception $e) {
        die($e -> getMessage());
    }
}
