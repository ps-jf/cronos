<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    switch ($_GET["do"]) {
        case "add":
            //Declare variables to hold data being passed through
            $week = $_POST['week'];
            $day = $_POST['bulkchaseaudit']['chase_date']['D'];
            $month = $_POST['bulkchaseaudit']['chase_date']['M'];
            $year = $_POST['bulkchaseaudit']['chase_date']['Y'];
            $date = $year."-".$month."-".$day;
            $report = $_GET['report_sent'];
            $pcomplete = $_POST['bulkchaseaudit']['pcomplete'];
            $partner = $_GET['id'];
            $helpdesk = $_POST['bulkchaseaudit']['helpdesk'];

            try {
                $json = [
                    "id"     => false,
                    "status" => false,
                    "error"  => false
                ];
            
                $db = new mydb();
            
                $db -> query("INSERT INTO ".DATA_DB.".bulk_chase_audit(week,chase_date,report_sent,pcomplete,added_by,added_when,partner_id,helpdesk) ".
                         "VALUES(".$week.",'".addslashes($date)."',".$report.",".addslashes($pcomplete).",".User::get_default_instance('id').",".time().",".$partner.",'".addslashes($helpdesk)."')");

                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e -> getMessage();
            }

            echo json_encode($json);
        
            break;
    
        case "delete":
            //Declare variables to hold data being passed through
            $partner = $_GET['id'];
            $chaseid = $_GET['chaseid'];

            try {
                $json = [
                    "id"     => false,
                    "status" => false,
                    "error"  => false
                ];
        
                $db = new mydb();
                $db -> query("DELETE FROM ".DATA_DB.".bulk_chase_audit WHERE id = ".$chaseid);

                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e -> getMessage();
            }
        
            echo json_encode($json);
    
            break;
    
        case "update":
            //Declare variables to hold data being passed through
            $week = $_GET['week'];
            $partner = $_GET['id'];
            $chaseid = $_GET['chaseid'];
            $report = $_GET['report_sent'];
            $pcomplete = $_GET['pcomplete'];
            $helpdesk = $_GET['helpdesk'];
            $date = $_GET['date'];

            try {
                $json = [
                    "id"     => false,
                    "status" => false,
                    "error"  => false
                ];
        
                $db = new mydb();

                $db -> query("UPDATE ".DATA_DB.".bulk_chase_audit ".
                         "SET week = ".$week.", chase_date = ". $date. ", report_sent = " .$report. ", pcomplete = " .addslashes($pcomplete). ", updated_by = " .User::get_default_instance('id'). ", updated_when = " .time().", helpdesk = '".addslashes($helpdesk)."' ".
                         "WHERE id = ".$chaseid);
                         
                $json["status"] = true;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = $e -> getMessage();
            }
        
            echo json_encode($json);
    
            break;
    }
} else {
    echo Template("acquisition/add_weekly.html", ["partnerID"=>$_GET['partner']]);
}
