<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $agency = new Policy($_GET["id"], true);
    
    $message = $_POST['text'];
    $day = $_POST['day'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $date = addslashes($year)."-".addslashes($month)."-".addslashes($day);
    
    // strtotime for workflow add to allow timestamp entry for table format
    $date2 = strtotime($date);

    $db = new mydb();
    
    $db -> query("select ".CLIENT_TBL.".clientid as clientid, ".PARTNER_TBL.".partnerid as partnerid from ".POLICY_TBL.
    " inner join ".CLIENT_TBL.
    " on ".POLICY_TBL.".clientid = ".CLIENT_TBL.".clientid".
    " inner join ".PARTNER_TBL.
    " on ".CLIENT_TBL.".partnerid = ".PARTNER_TBL.".partnerid".
    " where ".POLICY_TBL.".policyid = ".$agency -> id);

    if (!$d = $db -> next(MYSQLI_ASSOC)) {
        die("No data");
    } else {
        $clientid = $d["clientid"];
        $partnerid = $d["partnerid"];
    }
    
    
    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "save":
                try {
                    $return = [
                        "id"     => false,
                        "status" => false,
                        "error"  => false
                    ];

                        $corr = new Correspondence();
                        $corr -> policy($agency -> id);
                        $corr -> client($clientid);
                        $corr -> partner($partnerid);
                        $corr -> message(addslashes($message));
                        $corr -> subject('Bulk Transfer System');
                        $corr -> sender($_GET["uid"]);
                        $corr -> corrnotsend(1);
                        $corr -> save();
                
                    if ($date != '--' && $date != 'yyyy-mm-dd') {
                        $agency -> next_chase($date);
                        $agency -> save();
                    }
                
                        // Workflow ticket reminder to replace 'Schedule' on original Agency Transfer System if 'next chase' date filled in
                    if ($date != '--' && $date != 'yyyy-mm-dd') {
                        $wf = new Workflow;

                        $array = [
                            'step' => '10',
                            'priority' => '2',
                            'due' => $date2,
                            'desc' => 'Chase agency: ' . $agency->id,
                            '_object' => 'Policy',
                            'index' => $agency->id()
                        ];
                    
                            $array2 = ['id'=>$_COOKIE["uid"]];
                
                            ob_start();
                            $wf->workflow_add($array, $array2, false);
                            ob_end_clean();
                    }

                        echo json_encode($return);
                } catch (Exception $e) {
                    die($e -> getMessage());
                }

                break;
        }
    } else {
        echo new Template("acquisition/add_comm.html", ["agency"=>$agency]);
    }
}
