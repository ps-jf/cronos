<?php

if (isset($_GET["id"])) {
    # Paths to both ifa and network letter used in bulk transfer reports
    $details = $_POST['bulkdetails'];
    
    $ifaletter = $details['ifa'];
    $netletter = $details['net'];
    
    // If ifa_auth or network_auth are empty, create random string to append to path in order to prevent
    // file_exists checking the directory exists and returning true
    if ($netletter == '') {
        $netletter = $netletter . uniqid();
    }
    
    if ($ifaletter == '') {
        $ifaletter = $ifaletter . uniqid();
    }

    $ifaletter = str_replace('\\', '/', $ifaletter);
    $netletter = str_replace('\\', '/', $netletter);

    $ifaletter = '/mnt/acq_terry2/' . $ifaletter;
    $netletter = '/mnt/acq_terry2/' . $netletter;
    
    
    # Check if both files exist in order to be used in reports
    if (file_exists($ifaletter) && file_exists($netletter)) {
        echo "IFA letter and Network letter both exist! Please check to ensure the partner has their old network recorded.";
    } else if (!file_exists($ifaletter) && file_exists($netletter)) {
        echo "Only network letter exists! Please check that the partner has their old network recorded";
    } else if (file_exists($ifaletter) && !file_exists($netletter)) {
        echo "Only IFA letter exists!";
    } else if (!file_exists($ifaletter) && !file_exists($netletter)) {
        echo "BOTH letters DO NOT exist!";
    }
}
