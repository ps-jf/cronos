<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 14/12/2018
 * Time: 13:20
 */

require_once $_SERVER["DOCUMENT_ROOT"]."/../bin/_main.php";

if(isset($_GET['id']) && isset($_GET['export'])){
    $partner = new Partner;
    $partner->primary_key($_GET["id"]);

    // New objects to help create links to object popups
    $issuer = new Issuer;
    $policy = new Policy;

    try{
        //export the data as a csv
        $filename = $partner->id()."_agencies";

        //set the headers
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=".$filename.".csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        //create the file
        $outstream = fopen("php://output", "w");

        //create table columns
        fputcsv($outstream, ["Issuer", "Issuer Address 1", "Issuer Address 2", "Issuer Address 3",
            "Issuer Postcode", "Old Agency", "Instructed", "Status", "Transferred", "Transfer Type", "New Agency",
            "Next Chase"]);

        $agencyissuerlist = [];
        $clientsbyissuer = [];

        $s = new Search(new Agency);
        $s->eq("client->partner", $partner->id());
        $s->eq("client->bulk", "x");
        $s->add_order("issuer->name");

        while ($d = $s->next()) {
            $agencyissuerlist[] = $d->issuer->id;
        }

        //array of issuers which whould normally appear in the agency list
        $agencyissuerlist = array_unique($agencyissuerlist);

        //clients by issuer query for comparison
        // necessary objects
        $i = new Issuer;
        $c = new Client;
        $p = new Policy;
        $pa = new Partner($partner->id(), true);

        $db = new mydb();
        $q = "SELECT DISTINCT ".$i->id->get_field_name().", ".$i->name->get_field_name()." ".
            "FROM ".$c->get_table()." ".
            "LEFT JOIN ".$p->get_table()." ON ".$c->id->get_field_name()." = ".$p->client->get_field_name()." ".
            "INNER JOIN ".$i->get_table()." ON ".$p->issuer->get_field_name()." = ".$i->id->get_field_name()." ".
            "WHERE ".$c->partner->get_field_name()." = ".$partner->id()." 
				AND tblissuer.issuerID != 1197 AND tblissuer.issuerID != 1045 
				AND tblissuer.issuerID != 1254 AND tblissuer.issuerID != 11391
				GROUP BY ".$i->id->get_field_name()." ".
            "ORDER BY ".$i->name->get_field_name();

        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            $clientsbyissuer[] = $row['issuerID'];
        }

        //automatic adding of agency
        if ($pa->transtype() == 4) {
            //compare original agency issuer list with the issuers in clients by issuer that would appear on parnter profile
            $difference = array_diff($clientsbyissuer, $agencyissuerlist);

            if ($difference || !$agencyissuerlist) {
                if ($difference) {
                    $difference = array_values($difference);
                } else {
                    $difference = $clientsbyissuer;
                }

                $i=0;
                foreach ($difference as $value) {
                    try {
                        $db = new mydb();

                        $db -> query("select clientid from ".CLIENT_TBL." where partnerid = ".$_GET["id"]." and bulkclient = 'x'");

                        if (!$d = $db -> next(MYSQLI_ASSOC)) {
                            //if no bulk client, add so that we can add agencies
                            $client = new Client();
                            $client -> one -> surname("*Agency:");
                            $client -> partner($_GET["id"]);
                            $client -> bulk("x");
                            $client -> save();

                            $q = "SELECT clientID FROM ".CLIENT_TBL." WHERE partnerID = ".$_GET['id']." and bulkclient = 'x'";
                            $db->query($q);

                            while ($row = $db->next(MYSQLI_ASSOC)) {
                                $clientid = $row['clientID'];
                            }
                        } else {
                            $clientid = $d["clientid"];
                        }

                        $policy = new Agency();
                        $policy -> client($clientid);
                        $policy -> number("*Agency: ".addslashes($agency[$i]));
                        $policy -> type(27);
                        $policy -> status(16);
                        $policy -> issuer($difference[$i]);
                        $policy -> bulk(true);
                        $policy -> addedby(User::get_default_instance('id'));
                        $policy -> agency_mandate(1);
                        $policy -> save();

                        $i++;
                    } catch (Exception $e) {
                        var_dump($e -> getMessage());
                    }
                }
            }
        }

        //output results, including any newly added agencies
        $s = new Search(new Agency);
        $s->eq("client->partner", $partner->id());
        $s->eq("client->bulk", "x");
        $s->add_order("issuer->name");

        while ($d = $s->next()) {
            //hack We don't want an agency profile here, we want a policy
            //simulate the link method
            if ($d->old_bulk == "") {
                $old_bulk = "???";
            } else {
                $old_bulk = $d->old_bulk;
            }

            $transferType = "";

            if ($d->agency_mandate() == 0) {
                $transferType = 'Agency';
            } else {
                $transferType = 'Agency - Mandate';
            }

            $issuerQ = "SELECT distinct issuerID, issuerName, 
                IF(address1 != \"\", address1, issuerAdd1) as address1, 
                IF(address1 != \"\", address2, issuerAdd2) as address2, 
                IF(address1 != \"\", address3, issuerAdd3) as address3, 
                IF(address1 != \"\", postcode, issuerPostcode) as postcode
            FROM feebase.issuer_contact 
                JOIN feebase.tblissuer ON issuer_contact.issuer_id = tblissuer.issuerID
                WHERE (group_id = 4 OR is_main = 1)
                AND issuerID = ".$d->issuer()."
            ORDER BY is_main DESC";

            $db->query($issuerQ);

            $address1 = "";
            $address2 = "";
            $address3 = "";
            $postcode = "";

            while($address = $db->next(MYSQLI_ASSOC)) {
                $address1 = $address["address1"];
                $address2 = $address["address2"];
                $address3 = $address["address3"];
                $postcode = $address["postcode"];
            }

            $row = [$d->issuer, $address1, $address2, $address3, $postcode, $old_bulk, $d->submitted, $d->status, $d->transferred, $transferType, $d->short_bulk, $d->next_chase];

            //add each row to the file
            fputcsv($outstream, $row, ',', '"');
        }

        fclose($outstream);
    } catch (Exception $e){
        die("Error creating file, contact IT");
    }
}