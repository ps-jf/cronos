<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    $db = new mydb();

    $db->query("select clientid from " . CLIENT_TBL . " where partnerid = " . $_GET["id"] . " and bulkclient = 'x'");

    if (!$d = $db->next(MYSQLI_ASSOC)) {
        $json["failure"] = 1;
        $json["error"] = "There is no bulk client for this partner";
        echo json_encode($json);
        exit();
    } else {
        $clientid = $d["clientid"];
    }

    //Declare variables to hold data being passed through
    $agency = $_POST['agency'];
    $issuer_id = $_POST['issuer'];
    $status = $_POST['status'];
    $agency_mandate = $_POST['agency_mandate'];

    $day = $_POST['day'];
    $month = $_POST['month'];
    $year = $_POST['year'];

    //Loop through both arrays and print agency code and corresponding issuer id
    $i = 0;
    $j = 0;
    $flag = false;
    $json = [];
    $failed_rows = [];

    foreach ($agency as $value) {
        try {
            $db = new mydb();

            $db->query("select * from " . ISSUER_TBL . " where issuerID = " . $issuer_id[$i] . " and issuerCode = '" . $agency[$i] . "'");

            #If database already has main agency with this code
            if ($d = $db->next(MYSQLI_ASSOC)) {
                $flag = true;
                $json["failure"] = 1;
                array_push($failed_rows, $i + 1);
            }
            $i++;
        } catch (Exception $e) {
            $json["error"] = $e->getMessage();
            $j++;
        }
        $json["failure_rows"] = $failed_rows;
    }
    if ($flag != true) {
        $i = 0;
        foreach ($agency as $value) {
            try {
                $policy = new Agency();
                $policy->client($clientid);
                $policy->number("*Agency: " . addslashes($agency[$i]));
                $policy->type(27);
                $policy->status($status[$i]);
                $policy->transferred(addslashes($day[$i]) . '-' . addslashes($month[$i]) . '-' . addslashes($year[$i]));
                $policy->issuer($issuer_id[$i]);
                $policy->old_bulk($agency[$i]);
                if ($status[$i] == 12) {
                    $policy->short_bulk($agency[$i]);
                }
                $policy->bulk(true);
                $policy->addedhow(5);
                $policy->addedby(User::get_default_instance('id'));
                $policy->agency_mandate($agency_mandate[$i]);
                $policy->owner(1);
                $policy->save();

                $i++;
            } catch (Exception $e) {
                $json["error"] = $e->getMessage();
                $j++;
            }
        }

        $json["failure"] = $j;
        $json["count"] = $i;
    }
    echo json_encode($json);
} else {
    echo Template("acquisition/add_agency.html", ["partnerID" => $_GET['partner']]);
}
