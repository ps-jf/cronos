<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $agency = new Policy($_GET["id"], true);
    $partner = $agency -> client -> partner -> id;
    $issuer = $agency -> issuer -> id;

    $message = $_POST['text'];
    $day = $_POST['day'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $date = addslashes($year)."-".addslashes($month)."-".addslashes($day);
    
    // strtotime for workflow add to allow timestamp entry for table format
    $date2 = strtotime($date);
    
    $db = new mydb();
    
    $d = $db -> query("select ".CLIENT_TBL.".clientid as clientid, policyid as policyid from ".POLICY_TBL.
    " inner join ".CLIENT_TBL.
    " on ".POLICY_TBL.".clientid = ".CLIENT_TBL.".clientid".
    " where ".CLIENT_TBL.".partnerid = ".$partner." and issuerid = ".$issuer." and policytypeid = 27");

    // Arrays to hold necessary IDs
    $cli = [];
    $pol = [];

    if (!$d = $db -> next(MYSQLI_ASSOC)) {
        die("No data");
    } else {
        // Add IDs into arrays for processing later on
        while ($d) {
            array_push($cli, $d['clientid']);
            array_push($pol, $d['policyid']);
            $d = $db -> next(MYSQLI_ASSOC);
        }
    }

    
    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "save":
                for ($c=0; $c<sizeof($cli);) {
                    try {
                        $return = [
                            "id"     => false,
                            "status" => false,
                            "error"  => false
                        ];

                        $corr = new Correspondence();
                        $corr -> policy($pol[$c]);
                        $corr -> client($cli[$c]);
                        $corr -> partner($partner);
                        $corr -> message(addslashes($message));
                        $corr -> subject('Bulk Transfer System');
                        $corr -> sender($_GET["uid"]);
                        $corr -> corrnotsend(1);
                        $corr -> save();
                            
                        $agency -> next_chase($date);
                        $agency -> save();
                            
                        $c++;
                    } catch (Exception $e) {
                        die($e -> getMessage());
                    }
                }


                // Workflow ticket reminder to replace 'Schedule' on original Agency Transfer System if 'next chase' date filled in
                if ($date != '--' && $date != 'yyyy-mm-dd') {
                    $wf = new Workflow;

                    $array = ['step' => '10',
                        'priority' => '2',
                        'due' => $date2,
                        'desc' => 'Chase agency: ' . $agency->id,
                        '_object' => 'Policy',
                        'index' => $agency->id()
                    ];
                        
                    $array2 = ['id'=>$_COOKIE["uid"]];
                        
                    ob_start();
                    $wf->workflow_add($array, $array2, false);
                    ob_end_clean();
                }


                echo json_encode($return);
                
                break;
        }
    } else {
        echo new Template("acquisition/add_comm_all.html", ["agency"=>$agency]);
    }
}
