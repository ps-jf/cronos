<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    $agency = new Agency($_GET["id"], true);
    echo new Template("acquisition/agency_update.html", ["agency" => $agency]);
} else {
    die("Profile not found");
}
