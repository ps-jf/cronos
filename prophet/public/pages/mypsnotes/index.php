<?php
require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

use Carbon\Carbon;

$db = new mydb(REMOTE, E_USER_WARNING);
// establish remote db conn
if (mysqli_connect_errno()) {
    echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
    exit();
}

if (isset($_GET["do"])) {
    switch ($_GET['do']){
        case "load_notes":
            $response = [
                "success" => true,
                "data" => null
            ];

            $object = $_GET['object'];
            $objectID = $_GET['object_id'];

            $ns = new Search(new MypsNotes);

            $ns->eq("object", $object);
            $ns->eq("object_id", $objectID);
            $ns->eq("is_visible", 1);

            while($note = $ns->next(MYSQLI_ASSOC)){
                $response["success"] = true;

                $addedWhen = Carbon::parse($note->added_when());
                $addedWhen = $addedWhen->toDayDateTimeString();

                $rows .= "<tr>
                    <td>" . $note->added_by->first_name() . " " . $note->added_by->last_name() . " (" . $note->added_by->id() . ")</td>
                    <td>" . $addedWhen . "</td>
                    <td>" . $note->subject() . "</td>
                    <td>" . $note->note_type() . "</td>
                    <td>" . $note->text() . "</td>
                </tr>";
            }

            $response['data'] = $rows;

            echo json_encode($response);
            break;

        case "save_note":
            $response = [
                "success" => true,
                "data" => null,
            ];

            $note = new MypsNotes();

            $note->object(ucfirst($_GET['object']));
            $note->object_id($_GET['object_id']);
            $note->subject($_GET['subject']);
            $note->text($_GET['text']);

            if (!$note->save()){
                $response['success'] = false;
                $response['data'] = "Failed to save note";
            }

            echo json_encode($response);

            break;

        case "convert_note":
            $response = [
                "success" => true,
                "data" => null,
            ];

            $pNote = new Notes($_GET['note'], true);

            $note = new MypsNotes();

            $note->object($pNote->object_type());
            $note->object_id($pNote->object_id());
            $note->subject($pNote->subject());
            $note->text($pNote->text());

            if (!$note->save()){
                $response['success'] = false;
                $response['data'] = "Failed to convert note to myPSnote";
            }

            echo json_encode($response);

            break;
    }
}
