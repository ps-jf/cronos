<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if(isset($_GET['do'])){
    switch($_GET['do']){
        case "run_dsar":
            $client = new Client($_GET['client'], true);

            $response = [
                "success" => true,
                "data" => null,
            ];

            try{
                $response['data'] = $client->dsar($_GET['checked']);

                $response['data'] .= "Client DSAR was successful. All files have been placed in " . DSAR_DIR . $client->id();
            } catch (Exception $e) {
                $response['success'] = false;
                $response['data'] = $e->getMessage();
            }

            echo json_encode($response);

            break;
    }
} else {
    $client = new Client($_GET['client_id'], true);

    echo new Template("dsar/setup.html", compact([
        "client"
    ]));
}