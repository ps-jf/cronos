<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "update":
                $entry = new PipelineEntry;
                $entry->group_name("entry");

                $entry->batch($_GET["batch_id"]);
                    
                // get percentage shares for different commission types as well as vat rate
                $type_search = new Search($entry->type->get_object());

                $x = $type_search->get_response_array(
                    false,
                    function ($object) {
                        return $object->id();
                    },
                    function ($object) {
                        return [
                         "partner_percent" => $object->partner_percent(),
                         "ps_percent"      => $object->ps_percent(),
                         "locked"          => $object->locked(),
                         "vat"             => $object->vat(),
                         "currency_type"   => $object->currency_type()
                        ];
                    }
                );

                $percents = [];
                foreach ($x["results"] as $k => $v) {
                    $percents["$v[text]"] = $v["value"];
                }

                $percents = addslashes(json_encode($percents));

                // manually register batch as it's used as a hidden field
                $entry->register_field($entry->batch);

                echo PipelineEntry::get_template("update.html", ["id" => $_GET['id'], "pipelineentry"=>$entry, "percents"=>$percents]);
                    
                break;
        }
    }
} else {
    function list_issuers()
    {
        // code commented out due to legacy table no longer existing. No behaviour affected by this comment out.

        $i = new Issuer;
        $p = new Policy;
        //$pp = new PendingPipelineEntry;

        /* $db = new mydb();
        $db->query(
            "select ".$i->id->get_field_name().", ".$i->name->get_field_name().", COUNT(1) ".
            "from $pp->table ".
            "inner join ".$p->get_table()." on ".$pp->policy->get_field_name()." = ".$p->id->get_field_name()." ".
            "inner join ".$i->get_table()." on ".$p->issuer->get_field_name()." = ".$i->id->get_field_name()." ".
            "group by ".$i->id->get_field_name()." ".
            "order by ".$i->name->get_field_name()
        );*/

        $results = [];
        /* while ($row = $db->next(MYSQLI_NUM)) {
            $results[ $row[0] ] = "$row[1] ($row[2])";
        }*/

        return $results;
    }

    echo Template(PipelineEntry::get_template_path("process.html"), ["issuers"=>el::dropdown("issuer", list_issuers())]);
}
