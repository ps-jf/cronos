<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET['archived'])) {
    //parse out serialized string
    $params = [];
    parse_str($_GET['entry'], $params);

    //assign values to variables
    $policy_id = $_GET['policyid'];
    $batch_id = $_GET['batch_id'];
    $type = $params['pipelineentry']['type'];
    $description = $params['pipelineentry']['description'];
    $amount = $params['pipelineentry']['amount'];
    $vat_rate = $params['pipelineentry']['vat_rate'];
    $currency_type = $params['pipelineentry']['currency_type'];
    $partner_percent = $params['pipelineentry']['partner_percent'];
    $ps_percent = $params['pipelineentry']['ps_percent'];

    $json = [];

    // Check if any values being submitted are 0 or null, if so do not proceed with new entry
    if (empty($vat_rate) || empty($amount) || empty($policy_id) || empty($batch_id)) {
        $json["error"] = "Not all data is valid, please check values and re-submit";
        echo json_encode($json);
        die();
    }

    $shares_locked = [0,8,10,12];

    if (!in_array($type, $shares_locked)) {
        // Check percentage splits are present
        if (empty($partner_percent) || empty($ps_percent)) {
            $json["error"] = "Percentage splits missing, please check values and re-submit";
            echo json_encode($json);
            die();
        }

        // Check percentage splits equal 100%
        if (( floatval($partner_percent) + floatval($ps_percent) ) != 100) {
            $json["error"] = "Percentage splits do not equal 100%, please check values and re-submit";
            echo json_encode($json);
            die();
        }
    }

    // Check that clawback is a negative amount, adjustments (5) can be positive or negative
    if ($type != 5){
        if (($type == 2 || $type == 13) && $amount >= 0) {
            $json["error"] = "Clawback must be less than zero";
            echo json_encode($json);
            die();
        } elseif ($type != 2 && $type != 13 && $amount < 0) {
            $json["error"] = "Only clawbacks can be less than zero";
            echo json_encode($json);
            die();
        }
    }

    //// New PipelineEntry submitted for creation
    $entry = new PipelineEntry();
    $entry->policy($policy_id);
    $entry->amount($amount);
    $entry->batch($batch_id);
    $entry->type($type);
    $entry->partner_percent($partner_percent);
    $entry->ps_percent($ps_percent);
    $entry->description($description);
    $entry->vat_rate($vat_rate);
    $entry->currency_type($currency_type);

    //check owning client of policy to see if on hold


    if (!$entry->save()) {
        $json["error"] = $entry->errors;
        $json["status"] = false;
    } else {
        // Set policy to vatable if entry being processed with 'reduced' or 'current' vat rates
        // and policy isn't already vatable
        if (isset($policy_id) && ( $vat_rate == 3 || $vat_rate == 4 )) {
            try {
                $policy = new Policy($policy_id, true);
                $client = new Client($policy->client->id(), true);

                if ($type == 8 || $type == 10) {
                    // check if client income on hold, if so hold entry
                    if ($client->income_on_hold($client)) {
                        $entry->on_hold(1);
                        $entry->save();
                    }
                }

                if (!$policy -> vatable()) {
                    $policy -> vatable(1);
                    $policy -> updatedwhen(time());
                    $policy -> updatedby(User::get_default_instance('id'));
                    $policy -> save();

                    $json["id"] = $entry->id();
                    $json["status"] = true;
                }
            } catch (Exception $e) {
                $json["error"] = $e -> getMessage();
                $json["status"] = false;
            }
        }

        echo json_encode($json);

        return;
    }
}

if (isset($_GET["batch_id"]) && intval($_GET["batch_id"])) {
    if (isset($_POST["entry"])) {

        $json = [];

        //// New PipelineEntry submitted for creation
        $entry = new PipelineEntry;
        $entry->editedBy($_USER->id());


        // Check if any values being submitted are 0 or null, if so do not proceed with new entry
        if (empty($_POST["entry"]["vat_rate"]) || empty($_POST["entry"]["amount"])
            || empty($_POST["entry"]["policy"]) || empty($_POST["entry"]["batch"])   ) {
            $json["error"] = "Not all data is valid, please check values and re-submit";
            echo json_encode($json);
            die();
        }

        $shares_locked = [0,8,10,12];

        if (!in_array($_POST["entry"]["type"], $shares_locked)) {
            // Check percentage splits are present
            if (empty($_POST["entry"]["partner_percent"]) || empty($_POST["entry"]["ps_percent"])) {
                $json["error"] = "Percentage splits missing, please check values and re-submit";
                echo json_encode($json);
                die();
            }

            // Check percentage splits equal 100%
            if (( floatval($_POST["entry"]["partner_percent"]) + floatval($_POST["entry"]["ps_percent"]) ) != 100) {
                $json["error"] = "Percentage splits do not equal 100%, please check values and re-submit";
                echo json_encode($json);
                die();
            }
        }

        // Check that clawback is a negative amount, adjustments can be positive or negative
        if ($_POST['entry']['type'] != 5){
            if (($_POST["entry"]["type"] == 2 || $_POST["entry"]["type"] == 13) &&  $_POST["entry"]["amount"] >= 0) {
                $json["error"] = "Clawback must be less than zero";
                echo json_encode($json);
                die();
            } elseif ($_POST['entry']['type'] != 2 && $_POST['entry']['type'] != 13 &&  $_POST["entry"]["amount"] < 0) {
                $json["error"] = "Only clawbacks can be less than zero";
                echo json_encode($json);
                die();
            }
        }

        $entry->load($_POST["entry"]);

        $client = new Client($entry->policy->client->id(), true);

        // check if client income on hold, if so hold entry
        if ($entry->type() == 8 || $entry->type() == 10) {
            if ($client->income_on_hold($client)) {
                $entry->on_hold(1);
                //$entry->save();
            }
        }


        if (!$entry->save()) {
            $json["error"] = $entry->errors;
            $json["status"] = false;
        } else {
            // If user processing entry from EDI system, delete entry from tbl_source on successful save
            if (isset($_GET["source_id"])) {
                try {
                    $db = new mydb();
                    $db -> query("DELETE FROM feebase.tbl_source WHERE id = " . $_GET["source_id"]);
                    $json["id"] = $entry->id();
                    $json["status"] = true;
                } catch (Exception $e) {
                    $json["error"] = $e -> getMessage();
                    $json["status"] = false;
                }
            } // Non-EDI system entry being added
            else {
                $json["id"] = $entry->id();
                $json["status"] = true;
            }
        }

        // Set policy to vatable if entry being processed with 'reduced' or 'current' vat rates
        // and policy isn't already vatable
        if (isset($_POST["entry"]["policy"]) && ( $_POST["entry"]["vat_rate"] == 3 || $_POST["entry"]["vat_rate"] == 4 )) {
            try {
                $policy = new Policy($_POST["entry"]["policy"], true);

                if (!$policy -> vatable()) {
                    $policy -> vatable(1);
                    $policy -> updatedwhen(time());
                    $policy -> updatedby(User::get_default_instance('id'));
                    $policy -> save();

                    $json["id"] = $entry->id();
                    $json["status"] = true;
                }
            } catch (Exception $e) {
                $json["error"] = $e -> getMessage();
                $json["status"] = false;
            }
        }

        echo json_encode($json);

        return;
    } else {
        $entry = new PipelineEntry;
        $entry->group_name("entry");

        $entry->batch($_GET["batch_id"]);

        // get percentage shares for different commission types as well as vat rate
        $type_search = new Search($entry->type->get_object());

        $x = $type_search->get_response_array(
            false,
            function ($object) {
                return $object->id();
            },
            function ($object) {
                return [
                    "partner_percent" => $object->partner_percent(),
                    "ps_percent" => $object->ps_percent(),
                    "locked" => $object->locked(),
                    "vat" => $object->vat()
                ];
            }
        );


        $percents = [];
        foreach ($x["results"] as $k => $v) {
            $percents["$v[text]"] = $v["value"];
        }

        $percents = addslashes(json_encode($percents));

        // manually register batch as it's used as a hidden field
        $entry->register_field($entry->batch);


        if ($_GET['newbusiness'] == "has_nb") {
            $newbusiness = $_GET['newbusiness'];
        } else {
            $newbusiness = false;
        }

        // check is entry being added via EDI system, pre-determine entry type and pass relevant data to template
        if (isset($_GET["edi"])) {
            $edi = "true";
            if (isset($_GET["type"])) {
                if (( $_GET["type"] ) == "clawback") {
                    $type = "2";  // Clawback entry
                }
                if (( $_GET["type"] ) == "initial") {
                    $type = "1";  // New Business/Initial entry
                }
                if (( $_GET["type"] ) == "aco") {
                    $type = "8";  // Adviser Charging Ongoing entry
                }
                echo new Template("pipelineentry/new.html", ["pipelineentry"=>$entry, "percents"=>$percents, "edi"=>$edi, "type"=>$type, "newbusiness"=>$newbusiness]);
            }
        } // manual entry being added to batch
        else {
            echo new Template("pipelineentry/new.html", ["pipelineentry"=>$entry, "percents"=>$percents, "newbusiness"=>$newbusiness]);
        }
    }
} else if (isset($_GET["policyid"])) {
    switch ($_GET["do"]) {
        case "check_vatable":
            $json = [];

            try {
                $json["id"] = $_GET["policyid"];
                $json["vatable"] = Policy::is_vatable($_GET["policyid"]);
                $json["status"] = true;
                $json["error"] = false;
            } catch (Exception $e) {
                $json["status"] = false;
                $json["error"] = "Failed to check if policy is vatable";
            }

            echo json_encode($json);

            break;

        case "add_to_archived":
            $policy = new Policy($_GET['policyid'], true);
            $entry = new PipelineEntry;

            // get percentage shares for different commission types as well as vat rate
            $type_search = new Search($entry->type->get_object());

            $x = $type_search->get_response_array(
                false,
                function ($object) {
                    return $object->id();
                },
                function ($object) {
                    return [
                        "partner_percent" => $object->partner_percent(),
                        "ps_percent" => $object->ps_percent(),
                        "locked" => $object->locked(),
                        "vat" => $object->vat()
                    ];
                }
            );


            $percents = [];
            foreach ($x["results"] as $k => $v) {
                $percents["$v[text]"] = $v["value"];
            }

            $percents = addslashes(json_encode($percents));

            echo new Template("pipelineentry/new_archived.html", ["pipelineentry"=>$entry, "percents"=>$percents, "policy"=>$policy]);

            break;
    }
}
