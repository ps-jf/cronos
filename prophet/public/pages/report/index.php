<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET["id"])) {
    if (!$id = intval($_GET["id"])) {
        trigger_error("Invalid report id '$_GET[id]'", E_USER_ERROR);
    }

    $report = new Report($id);

    if (!$report->get()) {
        trigger_error("Could not find report id #$id", E_USER_ERROR);
    }

    if (isset($_GET["action"])) {
        $action = $_GET["action"];

        if ($action == "form") {
            // Return parameter form for this Report wrapped by base template
            // which includes scripting etc.
            echo Template(Report::get_template_path("form_base.html"), ["form_body"=>$report->get_form_path(), "report"=>$report]);
        }
    }
} else if (isset($_GET["do"])) {
    if ($_GET["do"] = 'runReport') {
        try {
            if (isset($_GET['report_id'])) {
                // get report object based on ID
                $report_obj = new Report($_GET['report_id'], true);
            }

            $c = new Jaspersoft\Client\Client(
                JASPER_HOST,
                JASPER_USER,
                JASPER_PASSWD,
                ""
            );

            //this gives us a stdClass object
            $stdObj = json_decode($_GET['data']);

            // gives us one array..
            function objToarray($obj)
            {
                if (!is_array($obj) && !is_object($obj)) {
                    return $obj;
                }
                if (is_object($obj)) {
                    $obj = get_object_vars($obj);
                }
                return array_map(__FUNCTION__, $obj);
            }
            $arr = objToarray($stdObj);

            // create multidimensional array for passing values through in expected format
            $form_values = [];
            if (isset($arr) && !empty($arr)) {
                foreach ($arr as $key => $value) {
                    $form_values[$key][] = $value;
                }
            }

            // if frontend scanning report is ran
            if (isset($form_values['PROCESSED_FROM_DATE'][0])) {
                $form_values['PROCESSED_FROM'][0] = $form_values['PROCESSED_FROM_DATE'][0]." ".$form_values['PROCESSED_FROM_HOUR'][0].":".$form_values['PROCESSED_FROM_MINUTE'][0].":00";
                $form_values['PROCESSED_TO'][0] = $form_values['PROCESSED_TO_DATE'][0]." ".$form_values['PROCESSED_TO_HOUR'][0].":".$form_values['PROCESSED_TO_MINUTE'][0].":59";

                // remove all un-necessary array items
                unset($form_values['PROCESSED_FROM_DATE']);
                unset($form_values['PROCESSED_FROM_HOUR']);
                unset($form_values['PROCESSED_FROM_MINUTE']);
                unset($form_values['PROCESSED_TO_DATE']);
                unset($form_values['PROCESSED_TO_HOUR']);
                unset($form_values['PROCESSED_TO_MINUTE']);
            }

            // if corr_count_man report is ran
            if (isset($form_values['date1_hour'][0])) {
                $form_values['date1'][0] = $form_values['date1'][0]." ".$form_values['date1_hour'][0].":".$form_values['date1_minute'][0].":".$form_values['date1_second'][0];
                $form_values['date2'][0] = $form_values['date2'][0]." ".$form_values['date2_hour'][0].":".$form_values['date2_minute'][0].":".$form_values['date2_second'][0];

                // remove all un-necessary array items
                unset($form_values['date1_hour']);
                unset($form_values['date1_minute']);
                unset($form_values['date1_second']);
                unset($form_values['date2_hour']);
                unset($form_values['date2_minute']);
                unset($form_values['date2_second']);
            }

            // specific code to execute if running an agency mandate report
            if (isset($_GET['agency_mandate']) && $_GET['agency_mandate'] == true) {
                //remove un-needed array items
                unset($form_values['partner_level']);
                unset($form_values['practice_level']);
                unset($form_values['get']);
                unset($form_values['undefined']);
                unset($form_values['issuer'][0][0]);

                $partners = [];
                if ($form_values['level'][0] == 'practice_level') {
                    $db = new mydb();
                    $q = "SELECT partner_id from feebase.practice_staff where partner_id is not null and practice_id = " . $form_values['practice'][0];
                    $db->query($q);

                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        $partners[] = $row['partner_id'];
                    }
                } else {
                    if ($form_values['level'][0] == "partner_level") {
                        $partners[] = $form_values['partner'][0];
                    }
                }

                $form_values['partner'] = $partners;
                $form_values['partner'] = "(" . implode(', ', $form_values['partner']) . ")";
                $form_values['issuer'] = "(" . implode(', ', $form_values['issuer'][0]) . ")";
            }


            $data_array = json_decode($_GET['data']);
            if (isset($data_array->gdpr)) {
                // point to GDPR folder on rosie
                $report_url = JASPER_GDPR_REPORTS . $report_obj->report_file();
            } else {
                if (isset($_GET['report_id'])) {
                    // point to prophet_reports dir on rosie
                    $report_url = JASPER_PROPHET_REPORTS . $report_obj->report_file();
                } elseif (isset($_GET['report_name'])) {
                    // report by name
                    $report_url = JASPER_PROPHET_REPORTS . $_GET['report_name'];
                }
            }

            // check if spreadsheet required, if not generate pdf
            if (isset($stdObj) && $stdObj->required_format == "xls") {
                if (!empty($form_values)) {
                    $report = $c->reportService()->runReport($report_url, 'xls', null, null, $form_values);
                } else {
                    $report = $c->reportService()->runReport($report_url, 'xls');
                }
            } else {
                if (!empty($form_values)) {
                    $report = $c->reportService()->runReport($report_url, 'pdf', null, null, $form_values);
                } else {
                    $report = $c->reportService()->runReport($report_url, 'pdf');
                }
            }

            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Description: File Transfer');

            // check if spreadsheet required, if not return pdf
            if (isset($stdObj) && $stdObj->required_format == "xls") {
                // if report object is available
                header('Content-Disposition: attachment; filename='.$report_obj->report_file().'.xls');
                header('Content-Type: application/vnd.ms-excel');
            } else {
                if (isset($_GET['report_id'])) {

                    if ($_GET['report_id'] == 131 || $_GET['report_id'] == 132) {

                        $fileArray = [];
                        $time = time();

                        $report_file_front = "/reports/prophet_reports/pa_progress_report_front";
                        $report_front = $c->reportService()
                            ->runReport($report_file_front, 'pdf', null, null, null);

                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Description: File Transfer');
                        header('Content-Disposition: attachment; filename=progress-front.pdf');
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Content-Length: ' . strlen($report_front));
                        header('Content-Type: application/pdf');

                        // create front cover
                        $front_cover = PROREPORT_PENDING_SCAN_DIR."progress-front.pdf";
                        file_put_contents($front_cover, $report_front);

                        // check that temporary coversheet has been created, if so continue..
                        if (!file_exists($front_cover)) {
                            $json['error'] = true;
                            $json['status'] = "An error has occurred creating front cover, please contact IT.";
                            echo json_encode($json);
                            exit();
                        } else {
                            $fileArray[] = $front_cover;
                            // front cover created, now lets create main report
                            ($_GET['report_id'] == 131) ?
                                $report_file_main = "/reports/prophet_reports/pa_progress_reports" :
                                $report_file_main = "/reports/prophet_reports/pa_progress_reports_2";

                            $report_main = $c->reportService()
                                ->runReport($report_file_main, 'pdf', null, null, $form_values);

                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Description: File Transfer');
                            header('Content-Disposition: attachment; filename=progress-main.pdf');
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Content-Length: ' . strlen($report_main));
                            header('Content-Type: application/pdf');

                            // create main
                            $main = PROREPORT_PENDING_SCAN_DIR."progress-main.pdf";
                            file_put_contents($main, $report_main);

                            // check that temporary coversheet has been created, if so continue..
                            if (!file_exists($main)) {
                                $json['error'] = true;
                                $json['status'] = "An error has occurred creating front cover, please contact IT.";
                                echo json_encode($json);
                                exit();
                            } else {
                                $fileArray[] = $main;
                                // front cover and main created, now lets create back cover
                                $report_file_back = "/reports/prophet_reports/pa_progress_reports_back";
                                $report_back = $c->reportService()
                                    ->runReport($report_file_back, 'pdf', null, null, $form_values);

                                header('Cache-Control: must-revalidate');
                                header('Pragma: public');
                                header('Content-Description: File Transfer');
                                header('Content-Disposition: attachment; filename=progress-back.pdf');
                                header('Content-Transfer-Encoding: binary');
                                header('Expires: 0');
                                header('Content-Length: ' . strlen($report_back));
                                header('Content-Type: application/pdf');

                                // create back cover
                                $back_cover = PROREPORT_PENDING_SCAN_DIR."progress-back.pdf";
                                file_put_contents($back_cover, $report_back);

                                if (!file_exists($main)) {
                                    $json['error'] = true;
                                    $json['status'] = "An error has occurred creating front cover, please contact IT.";
                                    echo json_encode($json);
                                    exit();
                                } else {
                                    $fileArray[] = $back_cover;
                                }
                            }
                        }

                        // let ghost script works its magic
                        $final_file = PROREPORT_PENDING_SCAN_DIR."progress-report.pdf";
                        $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH  -sOutputFile=".$final_file." ";

                        //Add each pdf file to the end of the command
                        foreach ($fileArray as $file) {
                            $cmd .= $file . " ";
                        }

                        //merge and create final pdf
                        $result = exec($cmd);

                        if (file_exists($final_file)) {
                            unlink(PROREPORT_PENDING_SCAN_DIR."progress-front.pdf");
                            unlink(PROREPORT_PENDING_SCAN_DIR."progress-main.pdf");
                            unlink(PROREPORT_PENDING_SCAN_DIR."progress-back.pdf");

                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            header('Content-Description: File Transfer');
                            header('Content-Disposition: inline; filename='.basename($final_file).'"');
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Content-Length: ' . filesize($final_file));
                            header('Content-Type: application/pdf');
                            $final_2 = file_get_contents($final_file);
                            readfile($final_file);
                            exit();
                        } else {
                            $json['error'] = true;
                            $json['status'] = "Final progress report does not exist, please contact IT.";
                            echo json_encode($json);
                            exit();
                        }

                    } else {
                        // if report object is available
                        header('Content-Disposition: inline; filename='.$report_obj->report_file().'.pdf');
                    }

                } else if (isset($_GET['report_name'])) {
                    // report by name
                    header('Content-Disposition: inline; filename='.$_GET['report_name'].'.pdf');
                }
                header('Content-Type: application/pdf');
            }

            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Content-Length: ' . strlen($report));


            echo $report;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
} else {
    echo Template(Report::get_template_path("home.html"), ["report"=>new Report]);
}
