<?php

require($_SERVER['DOCUMENT_ROOT'] . "/../bin/_main.php");

if (isset($_GET["id"])) {
    if (!$id = intval($_GET["id"])) {
        trigger_error("Invalid Letter id '$_GET[id]'", E_USER_ERROR);
    }

    $letter = new Letter;

    if (!$letter->get($id)) {
        trigger_error("Letter id '$id' not found", E_USER_ERROR);
    }

    if (isset($_GET["get"])) {
        if ($_GET["get"] == "form") {
            // Display the customisation form for this Letter

            if (!isset($_GET["policy"]) || !$id = intval($_GET["policy"])) {
                trigger_error("Invalid/missing Policy ID", E_USER_ERROR);
            }

            $policy = new Policy;
            if (!$policy->get($id)) {
                trigger_error("Policy ID $id does not exist", E_USER_ERROR);
            }

            $item = new Correspondence;
            $item->policy($policy->id());
            $item->client($policy->client());
            $item->partner($item->client->get_object()->partner());

            echo Template(Letter::get_template_path($letter->form()), ["item" => $item, "client" => new Client($policy->client(), true)]);
        }
    } else {
        if (isset($_GET["do"])) {
            $do = $_GET["do"];
        }
    }
} else {
    if ($_GET["do"] == 'runReport') {
        try {
            // use $_GET['data'] from regular std_letter, $_GET from FES std_letter
            if ($_GET['data']) {
                $data = $_GET['data'];

                // this gives us a stdClass object
                $stdObj = json_decode($data);

                // gives us one array..
                function objToarray($obj)
                {
                    if (!is_array($obj) && !is_object($obj)) {
                        return $obj;
                    }
                    if (is_object($obj)) {
                        $obj = get_object_vars($obj);
                    }
                    return array_map(__FUNCTION__, $obj);
                }

                $arr = objToarray($stdObj);
            }

            /*
             * arr['correspondence[letter]'] = std_letter type
             * else default to coversheet
             */
            if (isset($arr['correspondence[letter]'])) {
                $letter_id = $arr['correspondence[letter]'];
            } else {
                $letter_id = 6;
            }

            // get letter object based on ID
            $letter_obj = new Letter($letter_id, true);

            // we must declare any objects we wish to use later on in this code block before we require jasper client
            $cr = new Correspondence();
            $policy = new Policy();

            $c = new \Jaspersoft\Client\Client(
                JASPER_HOST,
                JASPER_USER,
                JASPER_PASSWD,
                ""
            );

            // create multidimensional array for passing values through in expected format
            $form_values = [];
            foreach ($arr as $key => $value) {
                $form_values[$key][] = $value;
            }

            // get constant values for standard letter footer
            $std_letter_foot = unserialize(STANDARD_LETTER_FOOTER);
            foreach ($std_letter_foot as $key => $value) {
                $form_values[$key][] = $value;
            }

            // point to prophet_reports dir on rosie
            $report_url = JASPER_STANDARD_LETTERS . $letter_obj->rosie_file();

            $report_url = Letter::url_path_corrections($report_url);

            // check if we have any form data to pass through
            if (!empty($form_values)) {
                try {
                    $report = $c->reportService()->runReport($report_url, 'pdf', null, null, $form_values);
                } catch (Exception $e) {
                    exit('Caught exception: ' . $e->getMessage() . "\n");
                }
            } else {
                exit("Report parameters missing, please re-run report.");
            }

            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Description: File Transfer');
            header('Content-Disposition: inline; filename=' . $letter_obj->rosie_file() . '.pdf');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Content-Length: ' . strlen($report));
            header('Content-Type: application/pdf');


            // has standard letter been selected via front end scanning..?
            if (!isset($_GET['front_end']) || $_GET['front_end'] != 1) {
                echo $report;
            } else {
                $json = [
                    "error" => false,
                    "status" => null,
                    "id" => null,
                    "corr_count" => null
                ];

                $corr = $arr['ITEM_ID'];
                // temporary create std_letter for merging with correspondence
                $temp_std_letter = CORRESPONDENCE_PENDING_SCAN_DIR . $corr . "_temp_std_letter.pdf";
                file_put_contents($temp_std_letter, $report);

                // check that temporary coversheet has been created, if so continue..
                if (!file_exists($temp_std_letter)) {
                    $json['error'] = true;
                    $json['status'] = "An error has occurred creating coversheet, please contact IT.";
                } else {
                    // now that we have the cover sheet we want to use ghost script to
                    // join this with our current item of correspondence

                    // Check to see if Live or Development server. exec("gs") for Linux (Live),
                    // exec("gswin64") for Windows (Development)
                    $whitelist = [
                        '127.0.0.1',
                        '127.0.0.1:8080',
                        '127.0.0.1:8088',
                        '::1',
                        'localhost:8080',
                        'localhost:8088',
                        '10.0.2.2'
                    ];

                    if (isset($_GET['filepath'])) {
                        $correspondence = str_replace('\/', '/', $_GET['filepath']);
                        $correspondence = str_replace('//', '/', $correspondence);
                    }

                    // merge files contained in this array
                    if (isset($correspondence)) {
                        $fileArray = [$temp_std_letter, $correspondence];
                    } else {
                        $fileArray = [$temp_std_letter];
                    }


                    // specify directory to place new file in
                    $corr_file = CORRESPONDENCE_PROCESSED_SCAN_DIR . $corr . ".pdf";

                    // let ghost script works its magic
                    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                        $cmd = "gswin64 -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH -sOutputFile=$corr_file ";
                    } else {
                        $cmd = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -q -dNOPAUSE -dBATCH -sOutputFile=$corr_file ";
                    }

                    //Add each pdf file to the end of the command
                    foreach ($fileArray as $file) {
                        $cmd .= $file . " ";
                    }

                    //merge and create final pdf
                    $result = exec($cmd);

                    // check final pdf has ben created
                    if (file_exists($corr_file)) {
                        $json['url'] = $corr_file;

                        /*** Automatically update Item's Policy to 'Servicing Transferred to PS' status if
                         * it is still waiting for confirmation etc. ***/
                        $cr = new Correspondence($corr, true);
                        $policy = new Policy($cr->policy(), true);
                        $omission_subjects = unserialize(CORR_STATUS_OMISSION);

                        if (($policy->status() < 2) && (!in_array($cr->subject(), $omission_subjects))) {
                            $policy->transferred(time());
                            $policy->status(2);
                            if ($policy->save()) {
                                $json['status'] = "Correspondence item successfully created.";
                            } else {
                                $json['status'] = "File was successfully uploaded, error updating status.";
                            }
                        } else {
                            $json['status'] = "Correspondence item successfully created.";
                        }

                        $json['id'] = $corr;
                        $json['url'] = $temp_std_letter;

                        $json["corr_count"] = Correspondence::correspondence_count(User::get_default_instance('id'));
                    } else {
                        $json['error'] = true;
                        $json['status'] = "An error has occurred, please try again or contact IT if error persists.";
                    }
                }

                echo json_encode($json);
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } elseif ($_GET['do'] == "tidy_up") {
        // delete temp std_letter and original correspondence now that merged pdf has been created
        // and the preview of the letter has been supplied
        $std_letter = $_GET['file_url'];
        $orig_corr = $_GET['file_path'];

        if (file_exists($std_letter)) {
            unlink($std_letter);
        }

        if (file_exists($orig_corr)) {
            unlink($orig_corr);
        }

        exit();
    }
}
