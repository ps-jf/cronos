<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

// Reviewable contract - 2009 Tariff
$tariff_2009 = [
    ['min' => -99999, 'max' => 1000, 'percentage' => 0, 'band' => 0],
    ['min' => 1001, 'max' => 5000, 'percentage' => 60, 'band' => 1],
    ['min' => 5001, 'max' => 10000, 'percentage' => 65, 'band' => 2],
    ['min' => 10001, 'max' => 15000, 'percentage' => 70, 'band' => 3],
    ['min' => 15001, 'max' => 25000, 'percentage' => 75, 'band' => 4],
    ['min' => 25001, 'max' => 50000, 'percentage' => 80, 'band' => 5],
    ['min' => 50001, 'max' => 75000, 'percentage' => 82, 'band' => 6],
    ['min' => 75001, 'max' => 100000, 'percentage' => 84, 'band' => 7],
    ['min' => 100001, 'max' => 150000, 'percentage' => 86, 'band' => 8],
    ['min' => 150001, 'max' => 200000, 'percentage' => 88, 'band' => 9],
    ['min' => 200001, 'max' => 250000, 'percentage' => 90, 'band' => 10],
    ['min' => 250001, 'max' => 300000, 'percentage' => 92, 'band' => 11],
    ['min' => 300001, 'max' => 999999999, 'percentage' => 94, 'band' => 12]
];

// Reviewable contract - 2013 Tariff
$tariff_2013 = [
    ['min' => -99999, 'max' => 1000, 'percentage' => 30, 'band' => 0],
    ['min' => 1001, 'max' => 10000, 'percentage' => 60, 'band' => 1],
    ['min' => 10001, 'max' => 20000, 'percentage' => 65, 'band' => 2],
    ['min' => 20001, 'max' => 30000, 'percentage' => 70, 'band' => 3],
    ['min' => 30001, 'max' => 50000, 'percentage' => 75, 'band' => 4],
    ['min' => 50001, 'max' => 75000, 'percentage' => 80, 'band' => 5],
    ['min' => 75001, 'max' => 100000, 'percentage' => 82, 'band' => 6],
    ['min' => 100001, 'max' => 150000, 'percentage' => 84, 'band' => 7],
    ['min' => 150001, 'max' => 200000, 'percentage' => 86, 'band' => 8],
    ['min' => 200001, 'max' => 250000, 'percentage' => 88, 'band' => 9],
    ['min' => 250001, 'max' => 300000, 'percentage' => 90, 'band' => 10],
    ['min' => 300001, 'max' => 400000, 'percentage' => 92, 'band' => 11],
    ['min' => 400001, 'max' => 999999999, 'percentage' => 94, 'band' => 12]
];

$whitelist = [
    '127.0.0.1',
    '127.0.0.1:8080',
    '127.0.0.1:8088',
    '::1',
    'localhost:8080',
    'localhost:8088',
    '10.0.2.2'
];

if (isset($_GET["do"])) {
    switch ($_GET["do"]) {

        case "auto_proess_same_show":
            // get date plus 1 year for next review date
            $date = new DateTime();
            $date->modify('+1 year');

            echo new Template("accountreviews/review_same.html", compact('date'));
            break;

        case "auto_process_same_new":
            $json = [
                'acc' => false,
                'practice_counter' => 0
            ];

            $db = new mydb();
            $date = new DateTime();

            $q = "SELECT practice.name, practice.id as `practice_id`, MIN(accountreviewdate), reviewable_contract,
                group_concat(distinct a.partnerID) as `partners`, group_concat(distinct a.partnerBulkRate) as `rates`, min(partnerBulkRate), max(partnerBulkRate), 
                group_concat(distinct reviewable_contract) as `tariff`
            FROM tblpartner a
            INNER JOIN practice_staff ON a.partnerID = practice_staff.partner_id
            INNER JOIN practice ON practice_staff.practice_id = practice.id
            WHERE practice_id in (
                SELECT
                    practice.id
                FROM tblpartner a
                INNER JOIN practice_staff ON a.partnerID = practice_staff.partner_id
                INNER JOIN practice ON practice_staff.practice_id = practice.id
                INNER JOIN tblcommissionauditsql ON a.partnerID = tblcommissionauditsql.partnerid
                WHERE (accountreviewdate <= '" . $date->format('Y-m-d') . "' OR accountreviewdate like concat(year(now()), '-', LPAD(month(now()), 2, '0'), '%' ))
                    AND reviewable_contract IN (1,2)
                    AND commntype IN (0,2,8,10)
                    AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)
                    AND practice.id != 2894
                    AND a.is_closed != 1
                GROUP BY a.partnerid
                ORDER BY MIN(accountreviewdate)
            )    
                AND reviewable_contract IN (1,2)
                AND practice.id != 2894
                AND a.is_closed != 1                
            GROUP BY practice_id
            having (tariff not like '%,%' and rates not like '%,%')
            ORDER BY name, MIN(accountreviewdate)";

            $db->query($q);

            $same_array = [];
            // Loop through all partners that are due a review
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $add = false;
                // Show total amount as GRI, When no VAT available SUM() as GRI fails on SQL
                $practice_gri = str_replace('&pound;', '', Practice::practice_gri($row['partners']));
                $suggested_rate = Practice::suggestedReviewRate($row['tariff'], $practice_gri);

                // Get a suggested rate dependant upon which tariff and percentage the partner's gri falls within
                if (intval($row['rates']) == intval($suggested_rate)) {
                    // rate is remaining the same
                    foreach (explode(",", $row['partners']) as $p) {
                        $partner = new Partner($p, true);
                        $same_array[$row['practice_id']][$partner->id()] = $partner;
                        $add = true;
                    }
                } else {
                    // check if suggested rate of tariff going from lowest rate to zero, if so we want to keep it the same.
                    if (
                        (intval($row['tariff']) == 1) &&
                        (
                            (intval($row['rates']) == intval(60)) && (intval($suggested_rate) == intval(0)) ||
                            (intval($row['rates']) == intval(30)) && (intval($suggested_rate) == intval(0))
                        )
                    ) {
                        foreach (explode(",", $row['partners']) as $p) {
                            $partner = new Partner($p, true);
                            $same_array[$row['practice_id']][$partner->id()] = $partner;
                            $add = true;
                        }
                    } elseif (
                        (intval($row['tariff']) == 2) &&
                        (
                            (intval($row['rates']) == intval(30) && intval($suggested_rate) == intval(0)) ||
                            (intval($row['rates']) == intval(60) && (intval($suggested_rate) == intval(0) || intval($suggested_rate) == intval(30)))
                        )
                    ) {
                        foreach (explode(",", $row['partners']) as $p) {
                            $partner = new Partner($p, true);
                            $same_array[$row['practice_id']][$partner->id()] = $partner;
                            $add = true;
                        }
                    } else {
                        $add = false;
                    }
                }
            }

            // show full practices that should remain the same
            foreach ($same_array as $practice) {
                foreach ($practice as $practice_partner) {
                    $gri = Partner::partner_gri($practice_partner->id());
                    // Build up json return to pass back to template and display results to user
                    $json['acc'] .= "<tr id='" . $practice_partner->id() . "'>
                                            <td>" . $practice_partner->get_practice()->link() . "</td>
                                            <td>" . $practice_partner->link() . "</td>
                                            <td>" . $practice_partner->acctype . "</td>
                                            <td>" . $practice_partner->reviewable . "</td>
                                            <td>" . $practice_partner->review . "</td>
                                            <td>" . $practice_partner->single_rate . "</td>
                                            <td>" . $practice_partner->bulk_rate . "</td>
                                            <td>" . Partner::suggestedReviewRate($practice_partner, $gri) . "</td>
                                            <td class='gri'>" . $gri . "</td>
                                        </tr>";
                }
            }

            $json['practice_counter'] = sizeof($same_array);

            echo json_encode($json);

            break;

        case "update_all_auto":

            $json = [
                "error" => "",
                "status" => "",
                "feedback" => ""
            ];

            $date = new DateTime();
            $new_date = new DateTime();
            $new_date->modify('+1 year');

            $yes_i = 0;
            $no_i = 0;
            foreach ($_GET['partner_id'] as $pid) {
                $p = new Partner($pid, true);
                $p->review($new_date->format('Y-m-d'));
                if ($p->save()) {
                    //format gri for inserting into db
                    $gri = str_replace([",", "£", " "], "", $_GET['gri']);

                    //audit rate changed
                    $pra = new PartnerRateAudit();
                    $pra->partner($p->id());
                    $pra->monthsum_12($gri);
                    $pra->oldrate($p->bulk_rate());
                    $pra->newrate($p->bulk_rate());
                    $pra->date($date->format('Y-m-d'));
                    if ($pra->save()) {
                        //add note explaining partner account review and rate change
                        $n = new Notes();
                        $n->object_type("Partner");
                        $n->object_id($p->id());
                        $n->subject("Account Review");
                        $n->added_when(time());
                        $n->text("Account review date automatically updated to " . $new_date->format('d/m/Y') . ". Rate remained the same.");
                        $n->note_type("email");
                        if ($n->save()) {
                            $recipient = $p->email();

                            $oldrate = $suggested = $p->bulk_rate();

                            if ($suggested == null) {
                                $suggested = 0;
                            }

                            if ($recipient) {
                                // Email partner with new details
                                $next_review = $new_date->format('F Y');

                                $type = "SAME";

                                $gri_partner = str_replace("&pound; ", "", Partner::partner_gri($p->id()));
                                $gri_partner_life = str_replace("&pound; ", "", Partner::partner_gri($p->id(), true));
                                $mail_html_table = "";
                                $mail_text_table = "";

                                if ($p->reviewable() == 1) {
                                    $tariff = $tariff_2009;
                                    $tariff_description = $p->reviewable;
                                    $t1 = true;
                                } else if ($p->reviewable() == 2) {
                                    $tariff = $tariff_2013;
                                    $tariff_description = $p->reviewable;
                                    $t2 = true;
                                } else if ($p->reviewable() == 3) {
                                    $tariff = false;
                                    $tariff_description = $p->reviewable;
                                } else {
                                    $tariff = false;
                                }

                                if ($tariff) {
                                    //get rid of any dot delimiter
                                    $clean_gri = str_replace('.', '', $gri_partner);

                                    // Determine which which band the partners percentage falls within
                                    // Determine which which band the partners percentage falls within
                                    $band = array_search($p->bulk_rate(), array_column($tariff, 'percentage'));
                                    if (is_null($band)) {
                                        $band = array_search($p->bulk_rate() + 1, array_column($tariff, 'percentage'));
                                        if (is_null($band)) {
                                            $band = array_search($p->bulk_rate() - 1, array_column($tariff, 'percentage'));
                                        }
                                    }

                                    //build up email tables for
                                    $mail_html_table .= "<table align='center' width='600' border='0' cellspacing='0' cellpadding='0' style='border:1px solid #ccc; color:black !important; text-align: center; vertical-align: middle;'><tr  style='color:#3d3d3d'><th></th><th>Min</th><th>Max</th><th>(%)</th><th>Band</th></tr>";
                                    $mail_text_table = [];

                                    if (isset($band) && $band == 12) {
                                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 3]['min']) . "          £" . number_format($tariff[$band - 3]['max']) . "           " . number_format($tariff[$band - 3]['percentage']) . "        " . number_format($tariff[$band - 3]['band']));
                                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 2]['min']) . "        £" . number_format($tariff[$band - 2]['max']) . "           " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                                        array_push($mail_text_table, "      £" . number_format($tariff[$band - 1]['min']) . "         £" . number_format($tariff[$band - 1]['max']) . "             " . number_format($tariff[$band - 1]['percentage']) . "     " . number_format($tariff[$band - 1]['band']));
                                        array_push($mail_text_table, "     **£ " . number_format($tariff[$band]['min']) . "          £" . number_format($tariff[$band]['max']) . "               " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");

                                        $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 3]['min']) . "</td><td>£ " . number_format($tariff[$band - 3]['max']) . "</td><td>" . number_format($tariff[$band - 3]['percentage']) . "</td><td>" . number_format($tariff[$band - 3]['band']) . "</td></tr>";
                                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                                        $row3 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                                        $row4 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                                        $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                                    } elseif (isset($band) && $band == 11) {
                                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 2]['min']) . "            £ " . number_format($tariff[$band - 2]['max']) . "          " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 1]['min']) . "            £ " . number_format($tariff[$band - 1]['max']) . "         " . number_format($tariff[$band - 1]['percentage']) . "       " . number_format($tariff[$band - 1]['band']));
                                        array_push($mail_text_table, "     *£" . number_format($tariff[$band]['min']) . "            £ " . number_format($tariff[$band]['max']) . "         " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");
                                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "          £ " . number_format($tariff[$band + 1]['max']) . "           " . number_format($tariff[$band + 1]['percentage']) . "     " . number_format($tariff[$band + 1]['band']));

                                        $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                                        $row3 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                                        $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                                        $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                                    } elseif ($band < 5) {
                                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                                        array_push($mail_text_table, "     **£" . number_format($tariff[$band]['min']) . "           £ " . number_format($tariff[$band]['max']) . "          " . number_format($tariff[$band]['percentage']) . "        " . number_format($tariff[$band]['band']) . "**");
                                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "         £ " . number_format($tariff[$band + 1]['max']) . "        " . number_format($tariff[$band + 1]['percentage']) . "          " . number_format($tariff[$band + 1]['band']));
                                        array_push($mail_text_table, "       £" . number_format($tariff[$band + 2]['min']) . "        £ " . number_format($tariff[$band + 2]['max']) . "      " . number_format($tariff[$band + 2]['percentage']) . "        " . number_format($tariff[$band + 2]['band']));
                                        array_push($mail_text_table, "       £" . number_format($tariff[$band + 3]['min']) . "       £ " . number_format($tariff[$band + 3]['max']) . "      " . number_format($tariff[$band + 3]['percentage']) . "        " . number_format($tariff[$band + 3]['band']));

                                        $row1 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                                        $row3 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 2]['min']) . "</td><td>£ " . number_format($tariff[$band + 2]['max']) . "</td><td>" . number_format($tariff[$band + 2]['percentage']) . "</td><td>" . number_format($tariff[$band + 2]['band']) . "</td></tr>";
                                        $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 3]['min']) . "</td><td>£ " . number_format($tariff[$band + 3]['max']) . "</td><td>" . number_format($tariff[$band + 3]['percentage']) . "</td><td>" . number_format($tariff[$band + 3]['band']) . "</td></tr>";
                                        $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                                    } else {
                                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                                        array_push($mail_text_table, "      £" . number_format($tariff[$band - 2]['min']) . "           £" . number_format($tariff[$band - 2]['max']) . "         " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                                        array_push($mail_text_table, "      £" . number_format($tariff[$band - 1]['min']) . "          £" . number_format($tariff[$band - 1]['max']) . "         " . number_format($tariff[$band - 1]['percentage']) . "       " . number_format($tariff[$band - 1]['band']));
                                        array_push($mail_text_table, "     *£" . number_format($tariff[$band]['min']) . "           £" . number_format($tariff[$band]['max']) . "          " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");
                                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "        £" . number_format($tariff[$band + 1]['max']) . "           " . number_format($tariff[$band + 1]['percentage']) . "       " . number_format($tariff[$band + 1]['band']));
                                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 2]['min']) . "         £" . number_format($tariff[$band + 2]['max']) . "          " . number_format($tariff[$band + 2]['percentage']) . "       " . number_format($tariff[$band + 2]['band']));

                                        $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                                        $row3 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                                        $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                                        $row5 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 2]['min']) . "</td><td>£ " . number_format($tariff[$band + 2]['max']) . "</td><td>" . number_format($tariff[$band + 2]['percentage']) . "</td><td>" . number_format($tariff[$band + 2]['band']) . "</td></tr>";
                                        $mail_html_table .= $row1 . $row2 . $row3 . $row4 . $row5;
                                    }
                                    $mail_html_table .= "</table>";
                                }

                                $body = json_encode(["PARTNERNAME" => strval($p),
                                    "PARTNER_ID" => strval($p) . ' (' . $p->id() . ')',
                                    "TYPE" => $type,
                                    "OLD_RATE" => $oldrate,
                                    "SUGGESTED" => $suggested,
                                    "RATE" => $p->bulk_rate(),
                                    "LASTYEAR_GRI" => $gri_partner,
                                    "LIFETIME_GRI" => $gri_partner_life,
                                    "NEXT_REVIEW" => $next_review,
                                    "YEAR" => date("Y"),
                                    "dynamic_html" => ["CUSTOM_HTML" => $mail_html_table, "TEXT_HTML" => $mail_text_table]
                                ]);

                                //check if account number = SJP's (to determine if the account has an RBO)
                                if ($p->account_num == "06982440") {
                                    //check if development or live server
                                    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                                        $blind_copy = "jonathan.foley@policyservices.co.uk";
                                    } else {
                                        $blind_copy = "paul.fleckney@sjp.co.uk";
                                    }
                                } else {
                                    $blind_copy = null;
                                }

                                if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                                    $send_to = 'jonathan.foley@policyservices.co.uk';
                                } else {
                                    $send_to = $recipient;
                                }

                                $receiver = ['name' => strval($p), 'email' => $send_to];
                                $sender = ['name' => "Agency Support", 'email' => "agencysupport@policyservices.co.uk"];

                                if (sendSparkEmail($receiver, "Policy Services Account Review", 'account-review-backup', $body, $sender)) {
                                    $json['error'] = false;
                                    //$json['status'] = " An email has been sent to " . $recipient . " with regards to account: " . strval($p);

                                    $audit = new AccountReviewAudit();
                                    $audit->partner($p->id());
                                    $audit->recipient($send_to);
                                    $audit->email($body);
                                    if ($audit->save()) {
                                        $yes_i++;
                                    }
                                } else {
                                    $json['error'] = true;
                                    $json['status'] .= "Could not email new details to " . strval($p) . "<br />";
                                }
                            } else {
                                $json['error'] = false;
                                $json['status'] .= " Account updated for " . strval($p) . " No e-mail address on record.<br />";
                                $yes_i++;
                            }
                        } else {
                            $json['error'] = true;
                            $json['status'] .= "An error occurred while adding a confirmation note to " . strval($p) . "'s' profile, please contact IT<br />";
                            $no_i++;
                        }
                    } else {
                        $json['error'] = true;
                        $json['status'] .= "An error occurred whilst auditing rate change for " . strval($p) . ", Please contact IT<br />";
                        $no_i++;
                    }
                } else {
                    $json['error'] = true;
                    $json['status'] .= "An error occurred while updating partner account " . strval($p) . ", contact IT<br />";
                    $no_i++;
                }
            }

            $json['feedback'] = $yes_i . " acoounts updated, " . $no_i . " errors.";

            echo json_encode($json);

            break;

        case "load_accounts_new":
               $json = [
                   "status" => null,
                   "error" => null,
                   "accounts" => null
               ];

            try {
                $date = new DateTime();

                $db = new mydb();
                /* Obtain practice details with partner account reviews due in the current month or
                 any partner accounts with a historic date */
                $q = "SELECT practice.name, practice.id as `practice_id`, MIN(accountreviewdate), reviewable_contract,
                group_concat(distinct a.partnerID) as `partners`, group_concat(distinct a.partnerBulkRate) as `rates`,
                min(partnerBulkRate), max(partnerBulkRate)
                FROM tblpartner a
                INNER JOIN practice_staff ON a.partnerID = practice_staff.partner_id
                INNER JOIN practice ON practice_staff.practice_id = practice.id
                INNER JOIN tblcommissionauditsql ON a.partnerID = tblcommissionauditsql.partnerid
                WHERE practice_id in (
                    SELECT
                        practice.id
                    FROM tblpartner a
                    INNER JOIN practice_staff ON a.partnerID = practice_staff.partner_id
                    INNER JOIN practice ON practice_staff.practice_id = practice.id
                    INNER JOIN tblcommissionauditsql ON a.partnerID = tblcommissionauditsql.partnerid
                    WHERE (accountreviewdate <= '" . $date->format('Y-m-d') . "' OR accountreviewdate like concat(year(now()), '-', LPAD(month(now()), 2, '0'), '%' ))
                        AND reviewable_contract IN (1,2)
                        AND commntype IN (0,2,8,10)
                        AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)
                        AND practice.id != 2894
                        AND a.is_closed != 1
                    GROUP BY a.partnerid
                    ORDER BY MIN(accountreviewdate)
                )    
                    AND reviewable_contract IN (1,2)
                    AND practice.id != 2894
                    AND a.is_closed != 1
                GROUP BY practice_id
                ORDER BY MIN(accountreviewdate)";

                $db->query($q);

                // Gather query output and build up json return with practice information to be displayed back to user
                $practice_counter = 0;
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $p = new Practice($row['practice_id'], true);
                    $json['accounts'] .= "<tr id='" . $p->id() . "'>
                        <td>" . $p->link() . "</td>
                        <td>" . date("d/m/Y", strtotime($row['MIN(accountreviewdate)'])) . "</td>
                        <td><button id='" . $p->id() . "' class='review'>Review</button></td>
                    </tr>";
                    $practice_counter++;
                }

                if ($json['accounts'] == null) {
                    $json['accounts'] = "<tr><td>There are no accounts currently up for review.</td></tr>";
                }

                $json['status'] = true;
                $json['error'] = false;
                $json['practice_counter'] = $practice_counter;
            } catch (Exception $e) {
                $json['status'] = false;
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);
            break;

        case "review_account":
            if (isset($_GET['action'])) {
                $json = [
                    "status" => null,
                    "error" => null,
                    "acc" => null,
                    "total_gri" => null,
                    "tariff_diff" => null,
                    "practice_rate" => null,
                    "all" => null,
                    "band" => null
                ];

                try {
                    $db = new mydb();

                    // Obtain practice details with partner accounts in the affected practice.
                    $q = "SELECT practice.name, practice.id, tblpartner.partnerID, partnerbulkrate, accountreviewdate, reviewable_contract
								FROM tblpartner
								INNER JOIN practice_staff
								  ON tblpartner.partnerID = practice_staff.partner_id
								INNER JOIN practice
								  ON practice_staff.practice_id = practice.id
								WHERE reviewable_contract IN (1,2)
								  AND practice_staff.practice_id = " . $_GET['practice_id'] . "
								  AND tblpartner.is_closed != 1
								GROUP BY tblpartner.partnerid
								ORDER BY practice.name
                                ";

                    $db->query($q);

                    // $t1, $t2 - booleans to store contract type.  Used later to determine whether or not we can
                    // go on to suggest the recommended rate for each practice.
                    $t1 = false;
                    $t2 = false;
                    $band = false;
                    $suggested_practice_rate = false;
                    $total_gri = 0;

                    // Counter to loop through practices with outstanding review dates
                    $i = 0;

                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        $p = new Partner($row['partnerID'], true);

                        // Show total amount as GRI, When no VAT available SUM() as GRI fails on SQL
                        if ($row['SUM(vat)'] == null) {
                            // $partner_gri =  number_format($row['SUM(amount)'], 2);
                            $partner_gri = str_replace('&pound;', '', Partner::partner_gri($row['partnerID']));
                        } else {
                            //  $partner_gri = number_format($row['gri'], 2);
                            $partner_gri = str_replace('&pound;', '', Partner::partner_gri($row['partnerID']));
                        }

                        if ($row['reviewable_contract'] == 1) {
                            $tariff = $tariff_2009;
                            $tariff_description = $p->reviewable;
                            $t1 = true;
                        } else if ($row['reviewable_contract'] == 2) {
                            $tariff = $tariff_2013;
                            $tariff_description = $p->reviewable;
                            $t2 = true;
                        }

                        // If both tariffs are present in partner accounts within this practice inform user that
                        // there are diffrent tariffs between the partners
                        if (($t1 == true) && ($t2 == true)) {
                            $json['tariff_diff'] = true;
                        }

                        // Get a suggested rate dependant upon which tariff and percentage the partner's gri falls within
                        $precision = 0.01;
                        foreach ($tariff as $current) {
                            if ((str_replace(',', '', $partner_gri) - $current['min']) > $precision and
                                (str_replace(',', '', $partner_gri) - $current['max']) <= $precision) {
                                $suggested_rate = $current['percentage'];
                                break;
                            }
                        }

                        // Build up json return to pass back to template and display results to user
                        $json['acc'] .= "<tr id='" . $p->id() . "'>
												<td>" . $p->link() . "</td>
												<td>" . $p->acctype . "</td>
												<td>" . el($p->review) . "</td>
												<td>" . el($p->single_rate) . "</td>
												<td>" . el($p->bulk_rate) . "</td>
												<td class='gri'>&pound;" . $partner_gri . "</td>
												<td class='suggested'><span id='practice_rate'>&nbsp;</span></td>
												<td>" . $tariff_description . "</td>
												<td><input type='checkbox' id='" . $p->id() . "' class='email_partner'/></td>
												<td>
													<input class='band' type='hidden' value=''/>
													<button id='" . $p->id() . "' class='update'>Update</button>
												</td>

											</tr>";

                        // Replace comma from any GRI values to allow total gri for practice to be calculated
                        $total_gri = $total_gri + str_replace(',', '', $partner_gri);
                        $i = $i + 1;
                    }

                    if ($json['tariff_diff'] != true) {
                        foreach ($tariff as $current) {
                            if ((str_replace(',', '', $total_gri) - $current['min']) > $precision and
                                (str_replace(',', '', $total_gri) - $current['max']) <= $precision) {
                                $suggested_practice_rate = $current['percentage'];
                                $band = $current['band'];
                                break;
                            }
                        }
                    }

                    if ($i >= 2) {
                        $json['all'] = true;
                    }

                    $json['band'] = $band;
                    $json['practice_rate'] = $suggested_practice_rate;
                    $json['total_gri'] = number_format($total_gri, 2);
                    $json['status'] = true;
                    $json['error'] = false;
                } catch (Exception $e) {
                    $json['status'] = false;
                    $json["error"] = $e->getMessage();
                }

                echo json_encode($json);
            } else {
                $pr = new Practice($_GET['practice_id'], true);
                echo new Template("accountreviews/review.html", ["practice" => $pr]);
            }

            break;

        case "update_single_row":
            $json = [
                "status" => null,
                "error" => null
            ];

            try {
                $sjp_single = (100 - $_GET['single_rate']) / 10;
                $sjp_bulk = (100 - $_GET['bulk_rate']) / 10;

                $p = new Partner($_GET['partner_id'], true);

                $old_rate = $p->bulk_rate();

                $p->band($_GET['band']);
                $p->review($_GET['review_date']);
                $p->single_rate($_GET['single_rate']);
                $p->bulk_rate($_GET['bulk_rate']);
                $p->sjpsinglerate($sjp_single);
                $p->sjpbulkrate($sjp_bulk);

                if ($p->save()) {
                    $json['status'] = "Account details for " . $p . " have been successfully updated";
                    $json['error'] = false;

                    //format gri for inserting into db
                    $gri = str_replace(",", "", $_GET['gri']);
                    $gri = str_replace("£", "", $gri);
                    $gri = str_replace(" ", "", $gri);

                    //audit rate changed
                    $pra = new PartnerRateAudit();
                    $pra->partner($_GET['partner_id']);
                    $pra->monthsum_12($gri);
                    $pra->oldrate($old_rate);
                    $pra->newrate($_GET['bulk_rate']);
                    $pra->date(date("Y-m-d", time()));
                    if ($pra->save()) {
                        //add note explaining partner account review and rate change
                        $n = new Notes();
                        $n->object_type("Partner");
                        $n->object_id($_GET['partner_id']);
                        $n->subject("Account Review");
                        $n->added_when(time());
                        $n->text("Account review date updated to " . date("d/m/Y", strtotime($_GET['review_date'])) . " and a new rate of " . $_GET['bulk_rate'] . "% has been applied. Previous rate was " . $old_rate . "%");
                        $n->note_type("email");
                        if ($n->save()) {
                            $json['status'] = "Account details for " . $p . " have been successfully updated";
                            $json['error'] = false;
                        } else {
                            $json['status'] = "An error occurred while adding a confirmation note to partner profile, please contact IT";
                            $json['error'] = true;
                        }
                    } else {
                        $json['status'] = "An error occurred whilst auditing partner rate change, Please contact IT";
                        $json['error'] = true;
                    }
                } else {
                    $json['status'] = "An error occurred while updating partner account, contact IT";
                    $json['error'] = true;
                }
            } catch (Exception $e) {
                $json['status'] = false;
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "update_all":
            $json = [
                "status" => null,
                "error" => null,
            ];

            try {
                $i = 0;
                foreach ($_GET['partner_id'] as $item) {
                    $sjp_single = (100 - $_GET['single_rate'][$i]) / 10;
                    $sjp_bulk = (100 - $_GET['bulk_rate'][$i]) / 10;

                    $p = new Partner($_GET['partner_id'][$i], true);

                    $old_rate = $p->bulk_rate();

                    $p->band($_GET['band'][$i]);
                    $p->review($_GET['review_date'][$i]);
                    $p->single_rate($_GET['single_rate'][$i]);
                    $p->bulk_rate($_GET['bulk_rate'][$i]);
                    $p->sjpsinglerate($sjp_single);
                    $p->sjpbulkrate($sjp_bulk);

                    $next_review = $_GET['review_date'][$i];
                    $rate = $_GET['bulk_rate'][$i];

                    if ($p->save()) {
                        //format gri for inserting into db
                        $gri = str_replace(",", "", $_GET['gri'][$i]);
                        $gri = str_replace("£", "", $gri);
                        $gri = str_replace(" ", "", $gri);

                        //audit rate changed
                        $pra = new PartnerRateAudit();
                        $pra->partner($_GET['partner_id'][$i]);
                        $pra->monthsum_12($gri);
                        $pra->oldrate($old_rate);
                        $pra->newrate($_GET['bulk_rate'][$i]);
                        $pra->date(date("Y-m-d", time()));
                        if ($pra->save()) {
                            $n = new Notes();
                            $n->object_type("Partner");
                            $n->object_id($p->id());
                            $n->subject("Account Review");
                            $n->added_when(time());
                            $n->text("Account review date updated to " . date("d/m/Y", strtotime($_GET['review_date'][$i])) . " and a new rate of " . $rate . "% has been applied. Previous rate was " . $old_rate . "%");
                            $n->note_type("email");
                            if ($n->save()) {
                                $json['status'] = "Account details for all Partners have been successfully updated.";
                                $json['error'] = false;
                            } else {
                                $json['status'] = "An error occurred whilst adding reference note to partner account " . $item . ", please contact IT.";
                                $json['error'] = true;
                                exit();
                            }
                        } else {
                            $json['status'] = "An error occurred whilst auditing partner rate change for partner account " . $item . ", please contact IT.";
                            $json['error'] = true;
                            exit();
                        }
                    } else {
                        $json['status'] = "An error occurred whilst updating partner account " . $item . ", please contact IT.";
                        $json['error'] = true;
                        exit();
                    }
                    $i = $i + 1;
                }
            } catch (Exception $e) {
                $json['status'] = false;
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "queries":
            echo new Template("accountreviews/queries_index.html");
            break;

        case "load_queries":
            $json = [
                "status" => null,
                "error" => null,
                "accounts" => null
            ];

            try {
                $db = new mydb();

                // Obtain any accounts that have are on a reviewable contract but do not have a review date
                $q = "SELECT practice.name, practice.id, tblpartner.partnerID, partnerbulkrate, SUM(amount), SUM(vat),
                              SUM(amount)-SUM(vat) AS gri, MIN(accountreviewdate),reviewable_contract
                            FROM tblpartner
                            INNER JOIN practice_staff
                              ON tblpartner.partnerID = practice_staff.partner_id
                            INNER JOIN practice
                              ON practice_staff.practice_id = practice.id
                            INNER JOIN tblcommissionauditsql
                              ON tblpartner.partnerID = tblcommissionauditsql.partnerid
                            WHERE accountreviewdate IS NULL
                            AND reviewable_contract IN (1,2)
                            AND commntype IN (0,2,8,10)
                            AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)
                            AND practice.id != 2894
                            AND tblpartner.is_closed != 1
                            GROUP BY tblpartner.partnerid
                            ORDER BY MIN(accountreviewdate)";

                $db->query($q);

                $array = [];

                // Build up json return to pass back to template and display results to user
                while ($row = $db->next(MYSQLI_ASSOC)) {
                    $p = new Practice($row['id'], true);

                    if (!in_array($p->id(), $array)) {
                        $array[] = $p->id();

                        if ($row['MIN(accountreviewdate)'] != null) {
                            $review_date = date("d/m/Y", strtotime($row['MIN(accountreviewdate)']));
                        } else {
                            $review_date = "No Review Date";
                        }

                        $json['accounts'] .= "<tr id='" . $row['id'] . "'>
                                                    <td>" . $p->link() . "</td>
                                                    <td>" . $review_date . "</td>
                                                    <td><button id='" . $row['id'] . "' class='review'>Review</button></td>
                                                  </tr>";
                    }
                }

                if ($json['accounts'] == null) {
                    $json['accounts'] = "<tr><td>There are no accounts currently up for review.</td></tr>";
                }

                $json['status'] = true;
                $json['error'] = false;
            } catch (Exception $e) {
                $json['status'] = false;
                $json["error"] = $e->getMessage();
            }

            echo json_encode($json);

            break;

        case "review_queries":
            if (isset($_GET['action'])) {
                $json = [
                    "status" => null,
                    "error" => null,
                    "acc" => null,
                    "total_gri" => null,
                    "tariff_diff" => null,
                    "practice_rate" => null,
                    "band" => null
                ];

                try {
                    $db = new mydb();

                    // Obtain any accounts that have are on a reviewable contract but do not have a review date
                    $q = "SELECT practice.name, practice.id, tblpartner.partnerID, partnerbulkrate, SUM(amount), SUM(vat),
						        SUM(amount)-SUM(vat) AS gri, accountreviewdate,reviewable_contract
							  FROM tblpartner
							  INNER JOIN practice_staff
							    ON tblpartner.partnerID = practice_staff.partner_id
                              INNER JOIN practice
                                ON practice_staff.practice_id = practice.id
                              INNER JOIN tblcommissionauditsql
                                ON tblpartner.partnerID = tblcommissionauditsql.partnerid
                              WHERE reviewable_contract IN (1,2)
                              AND accountreviewdate IS NULL
                              AND commntype IN (0,2,8,10)
                              AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)
                              AND practice_staff.practice_id = " . $_GET['practice_id'] . "
                              AND tblpartner.is_closed != 1
                              GROUP BY tblpartner.partnerid";

                    $db->query($q);

                    $t1 = false;
                    $t2 = false;
                    $suggested_practice_rate = false;
                    $band = false;
                    $total_gri = 0;
                    $i = 0;

                    while ($row = $db->next(MYSQLI_ASSOC)) {
                        $p = new Partner($row['partnerID'], true);

                        // Show total amount as GRI, When no VAT available SUM() as GRI fails on SQL
                        if ($row['SUM(vat)'] == null) {
                            $partner_gri = number_format($row['SUM(amount)'], 2);
                        } else {
                            $partner_gri = number_format($row['gri'], 2);
                        }

                        if ($row['reviewable_contract'] == 1) {
                            $tariff = $tariff_2009;
                            $tariff_description = $p->reviewable;
                            $t1 = true;
                        } else if ($row['reviewable_contract'] == 2) {
                            $tariff = $tariff_2013;
                            $tariff_description = $p->reviewable;
                            $t2 = true;
                        } else if ($row['reviewable_contract'] == 3) {
                            $tariff = false;
                            $tariff_description = $p->reviewable;
                        }

                        /* If both tariffs are present in partners within this practice inform user that there are
                        different tariffs between the partners */
                        if (($t1 == true) && ($t2 == true)) {
                            $json['tariff_diff'] = true;
                        }

                        // Get a suggested rate dependant upon which tariff and percentage the partners gri falls within
                        $precision = 0.01;
                        foreach ($tariff as $current) {
                            if ((str_replace(',', '', $partner_gri) - $current['min']) > $precision and (str_replace(',', '', $partner_gri) - $current['max']) <= $precision) {
                                $suggested_rate = $current['percentage'];
                                $band = $current['band'];
                                break;
                            }
                        }

                        // Build up json return to pass back to template and display results to user
                        $json['acc'] .= "<tr id='" . $p->id() . "'>
												<td>" . $p->link() . "</td>
												<td>" . $p->acctype . "</td>
												<td>" . el($p->review) . "</td>
												<td>" . el($p->single_rate) . "</td>
												<td>" . el($p->bulk_rate) . "</td>
												<td class='gri'>&pound;" . $partner_gri . "</td>
												<td class='suggested'><span id='practice_rate'>&nbsp;</span></td>
												<td>" . $tariff_description . "</td>
												<td><input type='checkbox' id='" . $p->id() . "' class='email_partner'/></td>
												<td>
													<input class='band' type='hidden' value=''/>
													<button id='" . $p->id() . "' class='update'>Update</button>
												</td>

											</tr>";

                        $total_gri = $total_gri + str_replace(',', '', $partner_gri);
                        $i = $i + 1;
                    }

                    foreach ($tariff as $current) {
                        if ((str_replace(',', '', $total_gri) - $current['min']) > $precision and
                            (str_replace(',', '', $total_gri) - $current['max']) <= $precision) {
                            $suggested_practice_rate = $current['percentage'];
                            $band = $current['band'];
                            //break;
                        }
                    }

                    if ($i >= 2) {
                        $json['all'] = true;
                    }

                    $json['band'] = $band;
                    $json['practice_rate'] = $suggested_practice_rate;
                    $json['total_gri'] = number_format($total_gri, 2);
                    $json['status'] = true;
                    $json['error'] = false;
                } catch (Exception $e) {
                    $json['status'] = false;
                    $json["error"] = $e->getMessage();
                }

                echo json_encode($json);
            } else {
                $pr = new Practice($_GET['practice_id'], true);
                echo new Template("accountreviews/review_queries.html", ["practice" => $pr]);
            }

            break;

        case "prompt_email":
            $p = new Partner($_GET['partner_id'], true);
            $practice = new Practice($_GET['practice'], true);
            $gri = str_replace('&pound;', '', Partner::partner_gri($_GET['partner_id']));

            echo new Template("accountreviews/prompt_email.html", ["p" => $p, "practice" => $practice, "gri" => $gri, "suggested" => $_GET['suggested']]);
            break;

        case "single_email":

            $p = new Partner($_GET['partner_id'], true);
            $practice = new Practice($_GET['practice'], true);

            $db = new mydb();
            // Obtain practice gri
            $q = "SELECT SUM(amount)-SUM(vat) AS gri FROM tblpartner
                        INNER JOIN practice_staff
                          ON tblpartner.partnerID = practice_staff.partner_id
                        INNER JOIN practice
                          ON practice_staff.practice_id = practice.id
                        INNER JOIN tblcommissionauditsql
                          ON tblpartner.partnerID = tblcommissionauditsql.partnerid
                        WHERE practice.id =  " . $practice->id() . "
                          AND commntype IN " . CommissionType::griTypes() . "
                          AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)
                          AND practice.id != 2894
                          AND tblpartner.is_closed != 1";
            $db->query($q);

            while ($result = $db->next(MYSQLI_ASSOC)) {
                $practice_gri = $result['gri'];
            }
            $practice_gri = number_format($practice_gri, 2, '.', ',');

            $recipient = $_GET['email'];

            $gri = $_GET['gri'];
            $suggested = $_GET['suggested'];
            if ($suggested == null) {
                $suggested = 0;
            }

            // Email partner with new details
            $month = date('F');
            $year = date('Y');
            $next_month = date('F', strtotime($month . ' + 1 month'));
            $next_year = date('Y', strtotime($year . ' + 1 year'));
            $next_review = date('F ') . $next_year;
            $partner_name = $p->title() . " " . $p->forename() . " " . $p->surname();

            //get old rate
            $pra = new Search(new PartnerRateAudit);
            $pra->eq("partner", $_GET['partner_id']);
            $pra->add_order("id", Search::ORDER_DESCEND);
            $pra->limit(1);

            if ($audit = $pra->next()) {
                $oldrate = $audit->oldrate();
            }

            // wording dependant upon increase/decrease/same rate percent
            if ($p->bulk_rate() > $oldrate) {
                //increase in percentage
                $increase = $p->bulk_rate() - $oldrate;
                $type = "INCREASE";
                $json['type'] = "INCREASE";
            } elseif ($p->bulk_rate() == $oldrate && $p->bulk_rate() > $suggested) {
                //remain the same although we should be decreasing
                $decrease = $oldrate - $suggested;
                $type = "CONCESSIONARY";
                $json['type'] = "CONCESSIONARY1";
            } elseif ($p->bulk_rate() == $oldrate) {
                //remain the same
                $type = "SAME";
                $json['type'] = "SAME";
            } elseif ($p->bulk_rate() < $oldrate && $p->bulk_rate() > $suggested) {
                // decrease but not by as much as we should have decreased by
                $decrease = $oldrate - $suggested;
                $type = "CONCESSIONARY";
                $json['type'] = "CONCESSIONARY2";
            } elseif ($p->bulk_rate() < $oldrate) {
                // decrease
                $decrease = $oldrate - $p->bulk_rate();
                $type = "DECREASE";
                $json['type'] = "DECREASE";
            } else {
                //catch any other circumstances here..
                $json['status'] = false;
                $json['error'] = "Could not send preview email, rate error, contact IT.";
                echo json_encode($json);
                exit();
            }

            $gri_partner = str_replace("&pound; ", "", Partner::partner_gri($p->id()));
            $gri_partner_life = str_replace("&pound; ", "", Partner::partner_gri($p->id(), true));
            $mail_html_table .= "";
            $mail_text_table .= "";

            if ($p->reviewable() == 1) {
                $tariff = $tariff_2009;
                $tariff_description = $p->reviewable;
                $t1 = true;
            } else if ($p->reviewable() == 2) {
                $tariff = $tariff_2013;
                $tariff_description = $p->reviewable;
                $t2 = true;
            } else if ($p->reviewable() == 3) {
                $tariff = false;
                $tariff_description = $p->reviewable;
            }

            if ($tariff != false) {
                //get rid of any dot delimiter
                $clean_gri = str_replace('.', '', $gri_partner);

                // Determine which which band the partners percentage falls within
                $band = array_search($p->bulk_rate(), array_column($tariff, 'percentage'));
                if (is_null($band)) {
                    $band = array_search($p->bulk_rate() + 1, array_column($tariff, 'percentage'));
                    if (is_null($band)) {
                        $band = array_search($p->bulk_rate() - 1, array_column($tariff, 'percentage'));
                    }
                }

                $json['percent'] = $p->bulk_rate();
                $json['band'] = $band;

                //build up email tables for
                $mail_html_table .= "<table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:1px solid #ccc; color:black !important; text-align: center; vertical-align: middle;\"><tr  style='color:#3d3d3d'><th></th><th>Min</th><th>Max</th><th>(%)</th><th>Band</th></tr>";
                $mail_text_table = [];

                if (isset($band) && $band == 12) {
                    array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                    array_push($mail_text_table, "       £" . number_format($tariff[$band - 3]['min']) . "          £" . number_format($tariff[$band - 3]['max']) . "           " . number_format($tariff[$band - 3]['percentage']) . "        " . number_format($tariff[$band - 3]['band']));
                    array_push($mail_text_table, "       £" . number_format($tariff[$band - 2]['min']) . "        £" . number_format($tariff[$band - 2]['max']) . "           " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                    array_push($mail_text_table, "      £" . number_format($tariff[$band - 1]['min']) . "         £" . number_format($tariff[$band - 1]['max']) . "             " . number_format($tariff[$band - 1]['percentage']) . "     " . number_format($tariff[$band - 1]['band']));
                    array_push($mail_text_table, "     **£ " . number_format($tariff[$band]['min']) . "          £" . number_format($tariff[$band]['max']) . "               " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");

                    $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 3]['min']) . "</td><td>£ " . number_format($tariff[$band - 3]['max']) . "</td><td>" . number_format($tariff[$band - 3]['percentage']) . "</td><td>" . number_format($tariff[$band - 3]['band']) . "</td></tr>";
                    $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                    $row3 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                    $row4 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                    $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                } elseif (isset($band) && $band == 11) {
                    array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                    array_push($mail_text_table, "       £" . number_format($tariff[$band - 2]['min']) . "            £ " . number_format($tariff[$band - 2]['max']) . "          " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                    array_push($mail_text_table, "       £" . number_format($tariff[$band - 1]['min']) . "            £ " . number_format($tariff[$band - 1]['max']) . "         " . number_format($tariff[$band - 1]['percentage']) . "       " . number_format($tariff[$band - 1]['band']));
                    array_push($mail_text_table, "     *£" . number_format($tariff[$band]['min']) . "            £ " . number_format($tariff[$band]['max']) . "         " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");
                    array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "          £ " . number_format($tariff[$band + 1]['max']) . "           " . number_format($tariff[$band + 1]['percentage']) . "     " . number_format($tariff[$band + 1]['band']));

                    $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                    $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                    $row3 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                    $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                    $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                } elseif ($band < 5) {
                    array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                    array_push($mail_text_table, "     **£" . number_format($tariff[$band]['min']) . "           £ " . number_format($tariff[$band]['max']) . "          " . number_format($tariff[$band]['percentage']) . "        " . number_format($tariff[$band]['band']) . "**");
                    array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "         £ " . number_format($tariff[$band + 1]['max']) . "        " . number_format($tariff[$band + 1]['percentage']) . "          " . number_format($tariff[$band + 1]['band']));
                    array_push($mail_text_table, "       £" . number_format($tariff[$band + 2]['min']) . "        £ " . number_format($tariff[$band + 2]['max']) . "      " . number_format($tariff[$band + 2]['percentage']) . "        " . number_format($tariff[$band + 2]['band']));
                    array_push($mail_text_table, "       £" . number_format($tariff[$band + 3]['min']) . "       £ " . number_format($tariff[$band + 3]['max']) . "      " . number_format($tariff[$band + 3]['percentage']) . "        " . number_format($tariff[$band + 3]['band']));

                    $row1 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                    $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                    $row3 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 2]['min']) . "</td><td>£ " . number_format($tariff[$band + 2]['max']) . "</td><td>" . number_format($tariff[$band + 2]['percentage']) . "</td><td>" . number_format($tariff[$band + 2]['band']) . "</td></tr>";
                    $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 3]['min']) . "</td><td>£ " . number_format($tariff[$band + 3]['max']) . "</td><td>" . number_format($tariff[$band + 3]['percentage']) . "</td><td>" . number_format($tariff[$band + 3]['band']) . "</td></tr>";
                    $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                } else {
                    array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                    array_push($mail_text_table, "      £" . number_format($tariff[$band - 2]['min']) . "           £" . number_format($tariff[$band - 2]['max']) . "         " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                    array_push($mail_text_table, "      £" . number_format($tariff[$band - 1]['min']) . "          £" . number_format($tariff[$band - 1]['max']) . "         " . number_format($tariff[$band - 1]['percentage']) . "       " . number_format($tariff[$band - 1]['band']));
                    array_push($mail_text_table, "     *£" . number_format($tariff[$band]['min']) . "           £" . number_format($tariff[$band]['max']) . "          " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");
                    array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "        £" . number_format($tariff[$band + 1]['max']) . "           " . number_format($tariff[$band + 1]['percentage']) . "       " . number_format($tariff[$band + 1]['band']));
                    array_push($mail_text_table, "      £" . number_format($tariff[$band + 2]['min']) . "         £" . number_format($tariff[$band + 2]['max']) . "          " . number_format($tariff[$band + 2]['percentage']) . "       " . number_format($tariff[$band + 2]['band']));

                    $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                    $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                    $row3 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                    $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                    $row5 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 2]['min']) . "</td><td>£ " . number_format($tariff[$band + 2]['max']) . "</td><td>" . number_format($tariff[$band + 2]['percentage']) . "</td><td>" . number_format($tariff[$band + 2]['band']) . "</td></tr>";
                    $mail_html_table .= $row1 . $row2 . $row3 . $row4 . $row5;
                }
                $mail_html_table .= "</table>";
            }

            $body = json_encode(["PARTNERNAME" => $partner_name,
                "PARTNER_ID" => $partner_name . ' (' . $p->id() . ')',
                "TYPE" => $type,
                "OLD_RATE" => $oldrate,
                "SUGGESTED" => $suggested,
                "RATE" => $p->bulk_rate(),
                "LASTYEAR_GRI" => $gri_partner,
                "LIFETIME_GRI" => $gri_partner_life,
                "NEXT_REVIEW" => $next_review,
                "YEAR" => date("Y"),
                "dynamic_html" => ["CUSTOM_HTML" => $mail_html_table, "TEXT_HTML" => $mail_text_table]
            ]);

            //check if account number = SJP's (to determine if the account has an RBO)
            if ($p->account_num == "06982440") {
                //check if development or live server
                if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                    $blind_copy = "jonathan.foley@policyservices.co.uk";
                } else {
                    $blind_copy = "paul.fleckney@sjp.co.uk";
                }
            }

            if ($_GET['preview'] == "true") {
                $u = new User(User::get_default_instance('id'), true);
                $send_to = $u->email();
                $receiver = ['name' => $partner_name, 'email' => $send_to];
                $sender = ['name' => "agencysupport@policyservices.co.uk", 'email' => "agencysupport@policyservices.co.uk"];
                if (sendSparkEmail($receiver, "Policy Services Account Review", 'account-review-backup', $body, $sender)) {
                    $json['status'] = " A preview email has been sent to " . $send_to . " with regards to account: " . $p;
                    $json['error'] = false;
                } else {
                    $json['status'] = false;
                    $json['error'] = "Could not send preview email";
                }
            } else {
                if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                    $send_to = "jonathan.foley@policyservices.co.uk";
                } else {
                    $send_to = $recipient;
                    $receiver = ['name' => $partner_name, 'email' => $send_to];
                    $sender = ['name' => 'Agency Support', 'email' => 'agencysupport@policyservices.co.uk'];
                }

                if (sendSparkEmail($receiver, 'Policy Services Account Review', 'account-review-backup', $body, $sender)) {
                    $json['status'] = " An email has been sent to " . $recipient . " with regards to account: " . $p;
                    $json['error'] = false;

                    $audit = new AccountReviewAudit();
                    $audit->partner($_GET['partner_id']);
                    $audit->recipient($send_to);
                    $audit->email($body);
                    $audit->save();
                } else {
                    $json['status'] = false;
                    $json['error'] = "Could not email new details to Partner";
                }
            }

            echo json_encode($json);

            break;

        case "email_multiple_prompt":
            $_GET['partner_id'] = explode(',', $_GET['partner_ids']);
            $_GET['suggested'] = explode(',', $_GET['suggested']);
            $_GET['gri'] = explode(',', $_GET['gri']);
            $gri = [];
            $practice = new Practice($_GET['practice'], true);
            $suggested = array_shift(array_values($_GET['suggested']));

            $i = 0;
            foreach ($_GET['partner_id'] as $item) {
                $p = new Partner($item, true);
                array_push($gri, str_replace('&pound;', '', Partner::partner_gri($item)));
                if ($p->acctype() == "Sole Trader") {
                    $default_email = $p->email();
                } else {
                    $default_email = "";
                }
                $tr .= "<tr id='" . $p->id() . "'>
                            <td>" . $p->link() . "</td>
                            <td>" . $p->acctype() . "</td>
                            <td>
                                <input type='hidden' id='gri_" . $p->id() . "' size='45' value='" . $gri[$i] . "'/>
                                <input type='hidden' id='default_email_" . $p->id() . "' size='45' value='" . $p->email() . "'/>
                                <input type='text' id='recipient_email_" . $p->id() . "' data-pattern='^[-A-Z0-9._%+&amp;]+@[A-Z0-9.&amp;-]+\.[A-Z]{2,4}$' data-pattern_mods='i' size='45' value='" . $default_email . "'/>
                            </td>
                            <td><input type='checkbox' id='" . $p->id() . "' class='default'/></td>
                        </tr>";
                $i += 1;
            }

            echo new Template("accountreviews/prompt_email_multiple.html", ["p" => $p, "practice" => $practice, "gri" => $gri, "suggested" => $suggested, "tr" => $tr]);

            break;

        case "email_multiple":
            $i = 0;
            foreach ($_GET['partner_id'] as $item) {
                $p = new Partner($item, true);
                $practice = new Practice($_GET['practice'], true);

                $suggested = $_GET['suggested'][$i];
                if ($suggested == null) {
                    $suggested = 0;
                }

                $db = new mydb();
                // Obtain practice gri
                $q = "SELECT SUM(amount)-SUM(vat) AS gri FROM tblpartner
                        INNER JOIN practice_staff
                          ON tblpartner.partnerID = practice_staff.partner_id
                        INNER JOIN practice
                          ON practice_staff.practice_id = practice.id
                        INNER JOIN tblcommissionauditsql
                          ON tblpartner.partnerID = tblcommissionauditsql.partnerid
                        WHERE practice.id =  " . $practice->id() . "
                          AND commntype IN " . CommissionType::griTypes() . "
                          AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)
                          AND practice.id != 2894
                          AND tblpartner.is_closed != 1";
                $db->query($q);

                while ($result = $db->next(MYSQLI_ASSOC)) {
                    $practice_gri = $result['gri'];
                }
                $practice_gri = "£" . number_format($practice_gri, 2, '.', ',');

                $recipient = $_GET['email'][$i];


                // Email partner with new details
                $month = date('F');
                $year = date('Y');
                $next_month = date('F', strtotime($month . ' + 1 month'));
                $next_year = date('Y', strtotime($year . ' + 1 year'));
                $next_review = date('F ') . $next_year;
                $partner_name = $p->title() . " " . $p->forename() . " " . $p->surname();


                //get old rate
                $pra = new Search(new PartnerRateAudit);
                $pra->eq("partner", $_GET['partner_id'][$i]);
                $pra->add_order("id", Search::ORDER_DESCEND);
                $pra->limit(1);

                if ($audit = $pra->next()) {
                    $oldrate = $audit->oldrate();
                }

                //wording dependant upon increase/decrease/same rate percent
                if ($p->bulk_rate() > $oldrate) {
                    //increase in percentage
                    $increase = $p->bulk_rate() - $oldrate;
                    $type = "INCREASE";
                    $json['type'] = "INCREASE";
                } elseif ($p->bulk_rate() == $oldrate && $p->bulk_rate() > $suggested) {
                    //remain the same although we should be decreasing
                    $decrease = $oldrate - $suggested;
                    $type = "CONCESSIONARY";
                    $json['type'] = "CONCESSIONARY1";
                } elseif ($p->bulk_rate() == $oldrate) {
                    //remain the same
                    $type = "SAME";
                    $json['type'] = "SAME";
                } elseif ($p->bulk_rate() < $oldrate && $p->bulk_rate() > $suggested) {
                    // decrease but not by as much as we should have decreased by
                    $decrease = $oldrate - $suggested;
                    $type = "CONCESSIONARY";
                    $json['type'] = "CONCESSIONARY2";
                } elseif ($p->bulk_rate() < $oldrate) {
                    // decrease
                    $decrease = $oldrate - $p->bulk_rate();
                    $type = "DECREASE";
                    $json['type'] = "DECREASE";
                } else {
                    //catch any other circumstances here..
                    $json['status'] = false;
                    $json['error'] = "Could not send preview email, rate error, contact IT.";
                    echo json_encode($json);
                    exit();
                }

                $gri_partner = str_replace("&pound; ", "", Partner::partner_gri($p->id()));
                $gri_partner_life = str_replace("&pound; ", "", Partner::partner_gri($p->id(), true));
                $mail_html_table .= "";
                $mail_text_table .= "";

                if ($p->reviewable() == 1) {
                    $tariff = $tariff_2009;
                    $tariff_description = $p->reviewable;
                    $t1 = true;
                } else if ($p->reviewable() == 2) {
                    $tariff = $tariff_2013;
                    $tariff_description = $p->reviewable;
                    $t2 = true;
                } else if ($p->reviewable() == 3) {
                    $tariff = false;
                    $tariff_description = $p->reviewable;
                }

                if ($tariff != false) {
                    //get rid of any dot delimeter
                    $clean_gri = str_replace('.', '', $gri_partner);

                    // Determine which which band the partners percentage falls within
                    // Determine which which band the partners percentage falls within
                    $band = array_search($p->bulk_rate(), array_column($tariff, 'percentage'));
                    if (is_null($band)) {
                        $band = array_search($p->bulk_rate() + 1, array_column($tariff, 'percentage'));
                        if (is_null($band)) {
                            $band = array_search($p->bulk_rate() - 1, array_column($tariff, 'percentage'));
                        }
                    }

                    $json['percent'] = $p->bulk_rate();
                    $json['band'] = $band;

                    //build up email tables for
                    $mail_html_table .= "<table align=\"center\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:1px solid #ccc; color:black !important; text-align: center; vertical-align: middle;\"><tr  style='color:#3d3d3d'><th></th><th>Min</th><th>Max</th><th>(%)</th><th>Band</th></tr>";
                    $mail_text_table = [];

                    if (isset($band) && $band == 12) {
                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 3]['min']) . "          £" . number_format($tariff[$band - 3]['max']) . "           " . number_format($tariff[$band - 3]['percentage']) . "        " . number_format($tariff[$band - 3]['band']));
                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 2]['min']) . "        £" . number_format($tariff[$band - 2]['max']) . "           " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                        array_push($mail_text_table, "      £" . number_format($tariff[$band - 1]['min']) . "         £" . number_format($tariff[$band - 1]['max']) . "             " . number_format($tariff[$band - 1]['percentage']) . "     " . number_format($tariff[$band - 1]['band']));
                        array_push($mail_text_table, "     **£ " . number_format($tariff[$band]['min']) . "          £" . number_format($tariff[$band]['max']) . "               " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");

                        $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 3]['min']) . "</td><td>£ " . number_format($tariff[$band - 3]['max']) . "</td><td>" . number_format($tariff[$band - 3]['percentage']) . "</td><td>" . number_format($tariff[$band - 3]['band']) . "</td></tr>";
                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                        $row3 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                        $row4 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                        $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                    } elseif (isset($band) && $band == 11) {
                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 2]['min']) . "            £ " . number_format($tariff[$band - 2]['max']) . "          " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                        array_push($mail_text_table, "       £" . number_format($tariff[$band - 1]['min']) . "            £ " . number_format($tariff[$band - 1]['max']) . "         " . number_format($tariff[$band - 1]['percentage']) . "       " . number_format($tariff[$band - 1]['band']));
                        array_push($mail_text_table, "     *£" . number_format($tariff[$band]['min']) . "            £ " . number_format($tariff[$band]['max']) . "         " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");
                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "          £ " . number_format($tariff[$band + 1]['max']) . "           " . number_format($tariff[$band + 1]['percentage']) . "     " . number_format($tariff[$band + 1]['band']));

                        $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                        $row3 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                        $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                        $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                    } elseif ($band < 5) {
                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                        array_push($mail_text_table, "     **£" . number_format($tariff[$band]['min']) . "           £ " . number_format($tariff[$band]['max']) . "          " . number_format($tariff[$band]['percentage']) . "        " . number_format($tariff[$band]['band']) . "**");
                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "         £ " . number_format($tariff[$band + 1]['max']) . "        " . number_format($tariff[$band + 1]['percentage']) . "          " . number_format($tariff[$band + 1]['band']));
                        array_push($mail_text_table, "       £" . number_format($tariff[$band + 2]['min']) . "        £ " . number_format($tariff[$band + 2]['max']) . "      " . number_format($tariff[$band + 2]['percentage']) . "        " . number_format($tariff[$band + 2]['band']));
                        array_push($mail_text_table, "       £" . number_format($tariff[$band + 3]['min']) . "       £ " . number_format($tariff[$band + 3]['max']) . "      " . number_format($tariff[$band + 3]['percentage']) . "        " . number_format($tariff[$band + 3]['band']));

                        $row1 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                        $row3 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 2]['min']) . "</td><td>£ " . number_format($tariff[$band + 2]['max']) . "</td><td>" . number_format($tariff[$band + 2]['percentage']) . "</td><td>" . number_format($tariff[$band + 2]['band']) . "</td></tr>";
                        $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 3]['min']) . "</td><td>£ " . number_format($tariff[$band + 3]['max']) . "</td><td>" . number_format($tariff[$band + 3]['percentage']) . "</td><td>" . number_format($tariff[$band + 3]['band']) . "</td></tr>";
                        $mail_html_table .= $row1 . $row2 . $row3 . $row4;
                    } else {
                        array_push($mail_text_table, "      MIN             MAX                 (%)         BAND ");
                        array_push($mail_text_table, "      £" . number_format($tariff[$band - 2]['min']) . "           £" . number_format($tariff[$band - 2]['max']) . "         " . number_format($tariff[$band - 2]['percentage']) . "       " . number_format($tariff[$band - 2]['band']));
                        array_push($mail_text_table, "      £" . number_format($tariff[$band - 1]['min']) . "          £" . number_format($tariff[$band - 1]['max']) . "         " . number_format($tariff[$band - 1]['percentage']) . "       " . number_format($tariff[$band - 1]['band']));
                        array_push($mail_text_table, "     *£" . number_format($tariff[$band]['min']) . "           £" . number_format($tariff[$band]['max']) . "          " . number_format($tariff[$band]['percentage']) . "       " . number_format($tariff[$band]['band']) . "**");
                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 1]['min']) . "        £" . number_format($tariff[$band + 1]['max']) . "           " . number_format($tariff[$band + 1]['percentage']) . "       " . number_format($tariff[$band + 1]['band']));
                        array_push($mail_text_table, "      £" . number_format($tariff[$band + 2]['min']) . "         £" . number_format($tariff[$band + 2]['max']) . "          " . number_format($tariff[$band + 2]['percentage']) . "       " . number_format($tariff[$band + 2]['band']));

                        $row1 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 2]['min']) . "</td><td>£ " . number_format($tariff[$band - 2]['max']) . "</td><td>" . number_format($tariff[$band - 2]['percentage']) . "</td><td>" . number_format($tariff[$band - 2]['band']) . "</td></tr>";
                        $row2 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band - 1]['min']) . "</td><td>£ " . number_format($tariff[$band - 1]['max']) . "</td><td>" . number_format($tariff[$band - 1]['percentage']) . "</td><td>" . number_format($tariff[$band - 1]['band']) . "</td></tr>";
                        $row3 = "<tr style='color:green'><td>*</td><td>£" . number_format($tariff[$band]['min']) . "</td><td>£ " . number_format($tariff[$band]['max']) . "</td><td>" . number_format($tariff[$band]['percentage']) . "</td><td>" . number_format($tariff[$band]['band']) . "</td></tr>";
                        $row4 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 1]['min']) . "</td><td>£ " . number_format($tariff[$band + 1]['max']) . "</td><td>" . number_format($tariff[$band + 1]['percentage']) . "</td><td>" . number_format($tariff[$band + 1]['band']) . "</td></tr>";
                        $row5 = "<tr style='color:#3d3d3d'><td></td><td>£" . number_format($tariff[$band + 2]['min']) . "</td><td>£ " . number_format($tariff[$band + 2]['max']) . "</td><td>" . number_format($tariff[$band + 2]['percentage']) . "</td><td>" . number_format($tariff[$band + 2]['band']) . "</td></tr>";
                        $mail_html_table .= $row1 . $row2 . $row3 . $row4 . $row5;
                    }
                    $mail_html_table .= "</table>";
                }


                $body = json_encode(["PARTNERNAME" => $partner_name,
                    "PARTNER_ID" => $partner_name . ' (' . $p->id() . ')',
                    "TYPE" => $type,
                    "OLD_RATE" => $oldrate,
                    "SUGGESTED" => $suggested,
                    "RATE" => $p->bulk_rate(),
                    "LASTYEAR_GRI" => $gri_partner,
                    "LIFETIME_GRI" => $gri_partner_life,
                    "NEXT_REVIEW" => $next_review,
                    "YEAR" => date("Y"),
                    "dynamic_html" => ["CUSTOM_HTML" => $mail_html_table, "TEXT_HTML" => $mail_text_table]
                ]);

                //check if account number = SJP's (to determine if the account has an RBO)
                if ($p->account_num == "06982440") {
                    //check if development or live server
                    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                        $blind_copy = "jonathan.foley@policyservices.co.uk";
                    } else {
                        $blind_copy = "paul.fleckney@sjp.co.uk";
                    }
                } else {
                    $blind_copy = null;
                }

                if ($_GET['preview'] == "true") {
                    $u = new User(User::get_default_instance('id'), true);
                    $send_to = $u->email();
                    $receiver = ['name' => $partner_name, 'email' => $send_to];
                    $sender = ['name' => 'Agency Support', 'email' => "agencysupport@policyservices.co.uk"];
                    if (sendSparkEmail($receiver, "Policy Services Account Review", 'account-review-backup', $body, $sender)) {
                        $json['status'] = " A preview email has been sent to " . $send_to . " with regards to account: " . $p;
                        $json['error'] = false;
                    } else {
                        $json['status'] = false;
                        $json['error'] = "Could not send preview email";
                    }

                } else {
                    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                        $send_to = 'jonathan.foley@policyservices.co.uk';
                    } else {
                        $send_to = $recipient;
                    }

                    $receiver = ['name' => $partner_name, 'email' => $send_to];
                    $sender = ['name' => "Agency Support", 'email' => "agencysupport@policyservices.co.uk"];

                    if (sendSparkEmail($receiver, "Policy Services Account Review", 'account-review-backup', $body, $sender)) {
                        $json['status'] = " An email has been sent to " . $recipient . " with regards to account: " . $p;
                        $json['error'] = false;

                        $audit = new AccountReviewAudit();
                        $audit->partner($item);
                        $audit->recipient($send_to);
                        $audit->email($body);
                        $audit->save();
                    } else {
                        $json['status'] = false;
                        $json['error'] = "Could not email new details to Partner";
                    }
                }
                $i += 1;
            }

            echo json_encode($json);

            break;
    }
} else {
    echo new Template("accountreviews/index.html");
}
