<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if(isset($_GET['id'])){
    $bsp = new BSP($_GET['id'], true);

    if(isset($_GET['do'])){
        switch($_GET['do']){
            case "save":
                $json = (object)[
                    "id" => false,
                    "status" => false,
                    "error" => false,
                    "data" => null
                ];

                try{
                    $bsp->load($_GET['bsp']);

                    $bsp->notes(json_encode($_GET['bsp']['notes']));

                    $bsp->save();

                    foreach($_GET['bspchecklist'] as $key => $value){
                        if($key === "client_list"){
                            if(!empty($_GET['clientlist_na'])){
                                $bsp->checklistValue($key, -1);
                            } else {
                                $bsp->checklistValue($key, $value);
                            }
                        } else {
                            $bsp->checklistValue($key, $value);
                        }
                    }

                    $json->data = $bsp->on_update($_REQUEST['bsp']);
                    $json->id = $bsp->id();
                    $json->status = true;
                } catch (Exception $e) {
                    trigger_error($e->getMessage());
                    $json->error = $e->getMessage();
                }

                echo json_encode($json);

                break;
        }
    }
}