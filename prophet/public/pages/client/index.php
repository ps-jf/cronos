<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

use Aws\S3\S3Client;

define("DEFAULT_CLIENT_SUGGEST_LIMIT", 15);

$whitelist = [
    '127.0.0.1',
    '127.0.0.1:8080',
    '127.0.0.1:8088',
    '::1',
    'localhost:8080',
    'localhost:8088',
    '10.0.2.2'
];

// Define our AWS credentials
$bucket = AWS_BUCKET;
$s3StorageKey = AWS_STORAGE_KEY;
$s3Secret = AWS_SECRET;

// Instantiate the S3 client with AWS credentials
$s3Client = S3Client::factory([
    'credentials' => [
        'key'    => $s3StorageKey,
        'secret' => $s3Secret
    ],
    'region'  => 'eu-west-2',
    'version'  => '2006-03-01'
]);

$redisClient = RedisConnection::connect();

if (isset($_GET["id"])) {
    $client = new Client($_GET["id"], true);

    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {

            case "update_proposition_level":
                if (isset($_GET['level'])) {
                    $json = [
                        'error' => false,
                        'feedback' => ""
                    ];

                    $percent = [
                        1 => 1,
                        2 => '0.75',
                        3 => '0.5',
                        'T' => 0,
                        'P' => 0
                    ];

                    // check if we already have a new servicing level, is so update, else add new entry. Audit in log_actions
                    $s = new Search(new ServicingPropositionLevel());
                    $s->eq("client_id", $client->id());
                    $s->eq("partner_id", $client->partner->id());
                    if ($sl = $s->next(MYSQLI_ASSOC)) {
                        $new_sl = new ServicingPropositionLevel($sl->id(), true);
                        $new_sl->updated_by(User::get_default_instance('id'));
                        $new_sl->updated_when($_GET['level_when']);
                    } else {
                        $new_sl = new ServicingPropositionLevel();
                        $new_sl->client_id($client->id());
                        $new_sl->partner_id($client->partner->id());
                        $new_sl->level_when($_GET['level_when']);
                    }

                    $new_sl->level($_GET['level']);
                    $new_sl->percent($percent[$_GET['level']]);
                    $new_sl->method('client_merge');

                    if (!$new_sl->save()) {
                        $json['error'] = true;
                        $json['feedback'] = "An error occurred, try again or update manually";
                    } else {
                        $json['feedback'] = "Servicing level updated";
                    }
                }

                echo json_encode($json);

                break;

            case "invoice_object_template":
                echo new Template("client/invoice.html");
                break;

            case "hold_income_form":
                $ih = new IncomeHold();
                echo new Template("client/hold_income_form.html", compact('client', 'ih'));
                break;

            case "hold_income":

                $json = [
                    'error' => false,
                    'feedback' => ""
                ];

                $json['feedback'] = IncomeHold::holdIncome($client->id());

                echo json_encode($json);
                break;

            case "release_income_form":
                $amount = 0;
                $ih = false;

                $search = new Search(new IncomeHold());
                $search->eq('object_type', 'Client');
                $search->eq('object_index', $client->id());
                $search->eq('release_when', NULL);

                if ($x = $search->next(MYSQLI_OBJECT)) {
                    $ih = $x;
                    $amount = IncomeHold::totalIncomeHeld($client->id());
                    $amount = "&pound;".number_format((float)$amount, 2);
                }

                echo new Template("client/release_income_form.html", compact('client', 'ih', 'amount'));
                break;

            case "release_income":

                $json = [
                    'error' => false,
                    'feedback' => ""
                ];

                $json['feedback'] = IncomeHold::releaseIncome($client->id());

                echo json_encode($json);
                break;

            case "turn_off_fees":
                $response = [
                    "success" => true,
                    "data" => null
                ];

                $i = 0;

                $s = new Search(new Policy());

                $s->eq("client", $client->id());

                function addWorkflow($policyID){
                    ob_start();

                    $userID = User::get_default_instance('id');

                    $reminderTime = time() + (60*60*24);

                    $arr_args = array(
                        'process' => '48',
                        'step' => '180',
                        'priority' => '2',
                        'due' => $reminderTime,
                        'desc' => "Switch fees to null on the back of Action Required #" . $_GET['ar_id'] . "\r\n\r\n" . $_GET['reason'],
                        '_object' => 'Policy',
                        'index' => $policyID
                    );

                    $workflow = new Workflow();
                    $workflow->workflow_add($arr_args, $userID, 0, 1);

                    ob_end_clean();
                }

                while($p = $s->next(MYSQLI_ASSOC)){
                    // check if the policy is paying fees
                    $sp = new Search(new PipelineEntry());

                    $sp->eq("policy", $p->id());
                    $sp->eq("type", [8, 10]);

                    if ($pipeline = $sp->next(MYSQLI_ASSOC)){
                        addWorkflow($p->id());
                        $i++;
                    } else {
                        // search old commission paid

                        $sc = new Search(new Commission());

                        $sc->eq("policy", $p->id());
                        $sc->eq("type", [8, 10]);

                        if ($comm = $sc->next(MYSQLI_ASSOC)){
                            addWorkflow($p->id());
                            $i++;
                        }
                    }
                }

                // close off action required ticket
                $ar = new ActionRequired($_GET['ar_id'], true);

                $ar->completed_when(time());
                $ar->completed_by(User::get_default_instance("myps_user_id"));

                if ($ar->save()){
                    // add a new message to the ticket
                    $arm = new ActionRequiredMessage();

                    // am/pm check
                    if(date("H") >= 18){
                        $ampm = "Evening";
                    } elseif(date("H") >= 12){
                        $ampm = "Afternoon";
                    } else {
                        $ampm = "Morning";
                    }

                    $arm->action_required_id($ar->id());
                    $arm->message("Good " . $ampm . ".

Many thanks for confirming how you wish to proceed in line with our earlier message.

I can confirm we will now action the request to switch the fees to nil for all fee paying plans under this client.

There may be further payments paid to you following this action however in most cases this will be the final payment as most providers pay any form of remuneration on a pro-rata basis.

If you have any further enquiries regarding the fees please speak to our Finance team on finance@policyservices.co.uk

Kind regards,
Policy Services");

                    $arm->save();

                    // return how many policies have been affected
                    $response['data'] = "Added workflow for " . $i . " " . pluralize($i, "policy");
                } else {
                    $response['success'] = false;
                    $response['data'] = "Added workflow but failed to close this ticket. Please close manually";
                }

                echo json_encode($response);

                break;

            case "mark_vulnerable":
                $response = [
                    'success' => true,
                    'error' => null
                ];

                try{
                    $vc = new VulnerableClient();

                    $vc->client($client->id());
                    $vc->client_index($_GET['index']);
                    $vc->reason($_GET['reason']);

                    $vc->save();
                } catch (Exception $e){
                    $response['success'] = false;
                    $response['error'] = $e->getMessage();
                }

                echo json_encode($response);

                break;

            case "unmark_vulnerable":
                $response = [
                    'success' => true,
                    'error' => null,
                ];

                try{
                    $vc = new VulnerableClient($_GET['vc'], true);

                    $vc->archived(1);

                    $vc->updated_when(time());
                    $vc->updated_by(User::get_default_instance("id"));

                    $vc->save();
                } catch (Exception $e){
                    $response['success'] = false;
                    $response['error'] = $e->getMessage();
                }

                echo json_encode($response);

                break;

            case "set_partner":
                /** Transfer client from one Partner to another. ***/

                $json = (object)["status" => false, "error" => false];

                if (isset($_POST["partner"], $_POST["current_partner"])) {
                    try {
                        if ($redisClient->get("incomerun")){
                            throw new Exception("Owning partner cannot be changed whilst the income run is being processed.");
                        }

                        $finance_leaders = [
                            29, 99, 203
                        ];

                        // check if we are attempting to move client to Untraced income account
                        if ($_POST["partner"] == 5130) {
                            if (!in_array(User::get_default_instance('id'), $finance_leaders) && User::get_default_instance('department') != 7 && User::get_default_instance('department') != 6) {
                                throw new Exception("Only Finance team leaders can transfer client to this partner account");
                            } else {
                                if (User::get_default_instance('department') == 7 || User::get_default_instance('department') == 6) {
                                    // send email to Finance to inform them that a client has been moved to untraced income account
                                    $recipient = [
                                        0 => [
                                            'address' => [
                                                'name' => "Finance",
                                                'email' => "finance@policyservices.co.uk"
                                            ]
                                        ]
                                    ];
                                    $from = [
                                        'name' => User::get_default_instance('email'),
                                        'email' => User::get_default_instance('email')
                                    ];
                                    $local_template['html'] = $local_template['text'] = new Template("sparkpost/emailTemplates/clientMove/untracedFinanceNotify.txt", [
                                        "current_partner" => $_POST["current_partner"],
                                        "new_partner" => $_POST["partner"],
                                        "client" => $client,
                                        "reason" => $_POST['transfer_reason'],
                                        "email" => User::get_default_instance('email')
                                    ]);
                                    if (sendSparkEmail($recipient, "Client Moved to UNTRACED (INCOME) (5130)", '', false, $from, [], $local_template)) {
                                        $json->email = true;
                                    } else {
                                        $json->email = false;
                                    }
                                }
                            }
                        }

                        if ($_POST["partner"] == 394)  {
                            if (User::get_default_instance('team_leader') == 1 || User::get_default_instance('department') == 7 || User::get_default_instance('department') == 6)  {
                               $do_change = true;
                            } else {
                                throw new Exception("Only Finance team leaders can transfer client to this partner account");
                            }
                        }

                        if ($_POST["current_partner"] != $client->partner()) {
                            throw new Exception("Client #$client->id belongs to " . $client->partner() . " not " . $_POST["current_partner"]);
                        }

                        if (isset($_POST["partner"])) {
                            $json->status = $client->changeOwningPartner($_POST['partner'], $_POST['transfer_reason']);
                        } else {
                            throw new Exception("Failed to transfer client to new partner");
                        }
                    } catch (Exception $e) {
                        //rollback function doesnt seem to exist and is causing errors
//                        $db->rollback();
                        $json->error = $e->getMessage();
                    }

                    echo json_encode($json);
                } else {
                    echo Client::get_template("set_partner.html", ["client" => $client, "enc_partner" => $enc_partner]);
                }

                break;

            case "archive":
                $json = [
                    "client_status" => false,
                    "policy_status" => false,
                    "status" => false,
                    "error" => false,
                    "timestamp" => false,
                ];

                //If reason isnt 20 characters wrong - return error
                if (strlen($_GET['reason']) >= 1) {
                    //create new database instance and then add reasoning to tbl_action_reason
                    $db = new mydb();
                    $q = "INSERT INTO
                         `feebase`.`tbl_action_reason` (`object`, `object_id`, `reason`, `created_by`, `created_when`,`action` )
                         VALUES ('Client', '". $_GET['id']."', '". addslashes($_GET['reason'])."', '". $_COOKIE['uid']."', '".time()."', 'Archived')";

                    //If reason is added to the database - then run the rest of the archive
                    if ($db->query($q)) {
                        try {
                            $timestamp = time();

                            //archive client and all policies under said client
                            $client = new Client($_GET['id'], true);
                            $client->archived($timestamp);
                            if ($client->save()) {
                                $json['client_status'] = true;
                            }

                            $s = new Search(new Policy());
                            $s->eq('client', $_GET['id']);

                            //if client has no policies set policy_status flag as true otherwise archive policies belonging to this client
                            if ($r = $s->next(MYSQLI_ASSOC) == false) {
                                $json['policy_status'] = true;
                            } else {
                                $s = new Search(new Policy());
                                $s->eq('client', $_GET['id']);
                                while ($r = $s->next(MYSQLI_ASSOC)) {
                                    $p = new Policy($r->id, true);
                                    $p->archived($timestamp);
                                    if ($p->save()) {
                                        $json['policy_status'] = true;
                                    }
                                }
                            }

                            if (($json['client_status']) && ($json['policy_status'])) {
                                $json['status'] = "This Client and all Policies belonging to this Client have been archived.";
                                $json['timestamp'] = date("d/m/Y");
                            } else {
                                $json['error'] = true;
                                $json['status'] = "An error occurred please contact IT";
                            }
                        } catch (Exception $e) {
                            $json['error'] = $e->getMessage();
                        }
                    } else {
                            $json['error'] = true;
                            $json['status'] = "Reason did not add to database. Please contact IT.";
                    }
                } else {
                    $json['error'] = true;
                    $json['status'] = "Please fill in the reason box.";
                }
                echo json_encode($json);


                break;

            case "get_archive_reason":
                $json = [
                    "status" => false,
                    "error" => false,
                    "timestamp" => false,
                ];

                //create new database instance and then add reasoning to tbl_action_reason
                $db = new mydb();
                $q = "Select * from feebase.tbl_action_reason where action = 'Archived' and object = 'Client' and object_id = ". $_GET['id'];

                //If reason is added to the database - then run the rest of the archive
                $db->query($q);

                while ($archive = $db -> next(MYSQLI_ASSOC)) {
                    $json['status'] = 'Archive reason: '. $archive['reason'];
                }

                echo json_encode($json);
                break;

            case "unarchive":
                $json = [
                    "client_status" => false,
                    "policy_status" => false,
                    "status" => false,
                    "error" => false
                ];

                //create new database instance and then add reasoning to tbl_action_reason
                $db = new mydb();
                $q = "DELETE FROM `feebase`.`tbl_action_reason` WHERE action = 'Archived' and `object_id`='". $_GET['id']."' and object = 'Client' ";

                //If reason is added to the database - then run the rest of the archive
                $db->query($q);

                try {
                    //archive client and all policies under said client
                    $client = new Client($_GET['id'], true);
                    $client->archived('null');
                    if ($client->save()) {
                        $json['client_status'] = true;
                    }

                    $s = new Search(new Policy());
                    $s->eq('client', $_GET['id']);

                    //if client has no policies set policy_status flag as true otherwise archive policies belonging to this client
                    if ($r = $s->next(MYSQLI_ASSOC) == false) {
                        $json['policy_status'] = true;
                    } else {
                        $s = new Search(new Policy());
                        $s->eq('client', $_GET['id']);
                        while ($r = $s->next(MYSQLI_ASSOC)) {
                            $p = new Policy($r->id, true);
                            $p->archived('null');
                            if ($p->save()) {
                                $json['policy_status'] = true;
                            }
                        }
                    }

                    if (($json['client_status']) && ($json['policy_status'])) {
                        $json['status'] = "This Client and all Policies belonging to this Client have been unarchived.";
                    } else {
                        $json['error'] = true;
                        $json['status'] = "An error occurred please contact IT";
                    }
                } catch (Exception $e) {
                    $json['error'] = $e->getMessage();
                }


                echo json_encode($json);


                break;

            case "get_signature_corr":
                $json = [
                    "objectid" => false,
                    "exists" => false,
                    "error" => false,
                ];

                $objectId = [];

                try {
                    $search = new Search(new Correspondence());
                    $search -> eq("Client", $_GET['id']);
                    $search -> add_or([
                        $search->eq("Subject", 'Client Signature Verification', true, 0),
                        $search->eq("Subject", 'Client ID', true, 0)
                    ]);
                    $search -> order("corrID", "desc");

                    while ($x = $search -> next(MYSQLI_ASSOC)) {
                        $s = new Search(new ClientIDDocument());
                        $s->eq("correspondence", $x->id());

                        if($iddoc = $s->next(MYSQLI_ASSOC)){
                            switch($iddoc->index()){
                                case 1:
                                    $belongs = $iddoc->client->one->name();

                                    break;
                                case 2:
                                    $belongs = $iddoc->client->two->name();

                                    break;

                                default:
                                    $belongs = strval($iddoc->client);

                                    break;
                            }
                        } else {
                            $belongs = "<i>Unknown</i>";
                        }

                        array_push($objectId,
                            [$x->id(),$x->subject(), $x->message(), strval($x->date), strval($x->policy->owner), $x->policy->link(), $belongs ]
                        );
                    }

                    //check array is empty
                    if (empty($objectId)) {
                        $json['exists'] = false;
                    } else {
                        $json['exists'] = true;
                    }

                    $json['objectid'] = $objectId;
                } catch (Exception $e) {
                    $json['error'] = "An error has occurred";
                }

                echo json_encode($json);

                break;

            case "get_consent_corr":

            $json = [
                "objectid" => false,
                "exists" => false,
                "error" => false,
            ];

            $objectId = [];

            try{

                $search = new Search( new Correspondence() );
                $search -> eq ("Client", $_GET['id']);
                $search -> eq ("Subject",'Data Subject Consent Form');
                $search -> order("corrID", "desc");

                while( $x = $search -> next(MYSQLI_ASSOC ))
                {
                    array_push($objectId, [$x->id(),$x->subject(), $x->date()]);
                }

                //check array is empty
                if (empty($objectId)) {
                    $json['exists'] = false;
                } else {
                    $json['exists'] = true;
                }

                $json['objectid'] = $objectId;

            }catch( Exception $e ) {
                $json['error'] = "An error has occurred";
            }

            echo json_encode($json);

            break;

            case "check_client_removal":
                $json = [
                    "status" => false,
                    "exists" => false,
                    "error" => false,
                ];

                //get client object with client id
                $client = new Client($_GET['id'], true);

                //create a new client handback removal object ready for populating
                $search = new Search(new HandbackRemoval());
                $search -> eq("clientID", $client->id());

                // get entry information if exists
                if ($x = $search -> next(MYSQLI_ASSOC)) {
                    $website_user = new WebsiteUser($x->submitted_by(), true);
                    if ($website_user) {

                        $extraText = " ";

                        if($x->removal_prompt() == 2){
                            $extraText = " from the Client Servicing proposition exercise ";
                        }

                        $json['status'] =  "<div class='alert alert-danger mb-1' role='alert'>
                            <i class='fa fa-exclamation-circle mr-1'></i>
                                            This client has been marked for removal" . $extraText . "on ". date('d/m/Y', $x->submitted_when())." by
                                             ".$website_user->link() ." <a class='float-right' href='#' id='show-reason'>Show Details</a>
                                 <div class='removal-reason'>
                                    Reason: " . $x->removal_reason . "
                                </div>
                            </div>";
                        $json['exists'] = true;
                    }
                }

                if (!$json['exists']){
                    $db  = new mydb(REMOTE);

                    $q = "SELECT * FROM myps.client_servicing WHERE client_id = " . $client->id();

                    $db->query($q);

                    if ($l = $db->next(MYSQLI_ASSOC)){
                        if ($l['servicing_level'] == "R"){
                            $website_user = new WebsiteUser($l['level_by'], true);
                            if ($website_user){
                                $json['status'] =  "<div class='alert alert-danger mb-1' role='alert'>
                            <i class='fa fa-exclamation-circle mr-1'></i>
                                            This client has been marked for removal on ". date('d/m/Y', $l['level_when'])." by
                                             ".$website_user->link() ." <a class='float-right' href='#' id='show-reason'>Show Details</a>
                                 <div class='removal-reason'>
                                    Reason: CSRD Removal
                                </div>
                            </div>";
                                $json['exists'] = true;
                            }
                        }
                    }
                }

                echo json_encode($json);

                break;

            case "mark_client_removal":
                $json = [
                    "exists" => false,
                    "error" => false,
                    "status" => false,
                ];

                try{
                    // create a new client handback removal object ready for populating
                    $handback = new HandbackRemoval();

                    $handback->client($client->id());
                    $handback->partner($client->partner->id());
                    $handback->removal_reason($_GET['reason']);
                    $handback->removal_prompt($_GET['prompt']);

                    if ($handback->save()) {
                        $message = "This client has now been unsegmented from a servicing level due to the client being marked for removal therefore this ticket has now been been closed.";

                        $client->unsegment($message);
                        $client->endWorkflow($message, [41,188,189,193], true);
                        $client->endActionRequired($message);

                        if (!empty($client->policies())){
                            foreach($client->policies() as $policy){
                                $policy->endActionRequired("This ticket can now be closed as the client has been marked for removal");
                            }
                        }

                        $json['exists'] = true;
                        $json['status'] .= ' Client was successfully marked for removal.';
                    } else {
                        $json['error'] = true;
                        $json['status'] .= ' Client has NOT been marked for removal.';
                    }
                } catch (Exception $e) {
                    $json['error'] = true;
                    $json['status'] = $e->getMessage();
                }

                echo json_encode($json);

                break;

            case "restore_client_removal":
                $response = [
                    "success" => true,
                    "data" => null,
                ];

                $search = new Search(new HandbackRemoval());
                $search->eq("clientID", $client->id());

                $rows = $deleted = 0;

                while($hr = $search->next(MYSQLI_ASSOC)){
                    $rows++;

                    if ($hr->delete()){
                        $deleted++;
                    }
                }

                if ($deleted != $rows){
                    $response['success'] = false;
                    $response['data'] = "Number of rows found does not matched rows deleted!";
                } else {
                    // log the reason for restoring the client
                    $ar = new ActionReason();

                    $ar->object("Client");
                    $ar->object_id($client->id());
                    $ar->reason($_GET['reason']);
                    $ar->action("Restore");

                    $ar->save();
                }

                echo json_encode($response);

                break;

            case "at_risk":
                $json = [
                    "status" => false,
                    "error" => false,
                    "at_risk" => false,
                ];

                try {
                    $c = new Client($_GET['id'], true);

                    $renewal_12 = Client::client_gri_atrisk($_GET['id'], $life = false);
                    $renewal_lifetime = Client::client_gri_atrisk($_GET['id'], $life = true);

                    if ($renewal_12 != "&pound;0.00;" and $renewal_lifetime != "&pound;0.00") {
                        $notification = "<div class='notification warning static' style='display:block;'>".$c->__toString()." has Income at Risk. This is viewable via the at risk page on mypsaccount.
                        <br />Total Income at risk (12 Months): ".$renewal_12.", Lifetime Renewal: ".$renewal_lifetime."</div>";
                        $json['at_risk'] = $notification;
                    }
                } catch (Exception $e) {
                    $json['error'] = $e->getMessage();
                }

                echo json_encode($json);

                break;

            case "servicing_level":
                $json = [
                    "new_level" => null,
                    "new_percent" => null,
                    "old_level" => null,
                    "added_when" => null
                ];

                $json['new_level'] = $client->servicingPropositionLevel();
                $json['new_percent'] = $client->splPercent();

                $json['old_level'] = $client->servicing_level();
                $json['added_when'] = $client->addedwhen();

                echo json_encode($json);

                break;

            case "statistics":

                /* Graph monthly commission since last year */
                $json = [
                    "cGraph" => []
                ];

                // Compile a list of the last 12 months
                $months = [];
                for ($i = 12; $i >= 0; $i--) {
                    $months[] = date('M Y', mktime(0, 0, 0, date('n') - $i));
                }

                $json["cGraph"]["months"] = $months;

                $db = new mydb();
                $c = new Commission;

                if ($redisClient->exists("income.client.".$client->id().".statistics.amounts")){
                    $amounts = json_decode($redisClient->get("income.client.".$client->id().".statistics.amounts"), true);
                } else {
                    // Commission since this time last year
                    $db->query(
                        "SELECT DATE_FORMAT(" . $c->date->get_field_name() . ", '%b %Y'), " .
                        "ROUND(SUM(" . $c->paid->get_field_name() . "),2) " .
                        "FROM " . $c->get_table() . " " .
                        "WHERE " . $c->client->to_sql($client->id()) . " " .
                        "AND " . $c->date->get_field_name() . " BETWEEN '" . (date("Y") - 1) . "-" . date("m") . "-01 00:00:00' AND '" . date("Y") . "-" . date("m") . "-" . date("d") . " " . date('G:i:s') . "' " .
                        "GROUP BY DATE_FORMAT(" . $c->date->get_field_name() . ", '%b %Y')"
                    );


                    $a = [];
                    while ($row = $db->next(MYSQLI_NUM)) {
                        $a[$row[0]] = $row[1];
                    }

                    $amounts = [];
                    foreach ($months as $m) {
                        if (!array_key_exists($m, $a)) {
                            $amounts[$m] = 0;
                            continue;
                        }

                        $amounts[$m] = $a[$m];
                    }

                    $redisClient->set("income.client.".$client->id().".statistics.amounts", json_encode($amounts));
                }


                $json["cGraph"]["data"] = array_values($amounts);

                /* Various figures */
                $figures = [];

                // number of policies
                $s = new Search(new Policy);
                $s->eq("clientID", $client->id());
                $figures["Policies"] = $s->count();

                // number of outstanding policies
                $s->eq("status", 1);
                $s->eq("addedhow", 2);
                $figures["Outstanding Mandate Policies"] = $s->count();

                // number of new business cases
                $s = new Search(new NewBusiness);
                $s->eq("policy->client", $client->id());
                $figures["New Business"] = $s->count();

                // number of unique paying policies
                $s = new Search(new Commission);
                $s->eq("client", $client->id());
                $figures["Paying Policies"] = $s->count("policy");

                // total commission for partner
                if ($redisClient->exists("income.client.".$client->id().".statistics.total_paid_to_client")){
                    $figures["Total Income Paid To Client"] = $redisClient->get("income.client.".$client->id().".statistics.total_paid_to_client");
                } else {
                    $ps = new Search(new Commission);
                    $ps->eq("client", $client->id());

                    $tiptc = "&pound; " . number_format($ps->sum("paid"), 2);
                    $figures["Total Income Paid To Client"] = $tiptc;;

                    $redisClient->set("income.client.".$client->id().".statistics.total_paid_to_client", $tiptc);
                }


               /* GRI calculated as (renewal(0) + clawback(2) + adviser_charging_ongoing(8) + client_direct_ongoing(10) + dfm_fees(12) +
                OAS Income(14) + Trail(15) + PPB Income(16)) - VAT */
                $figures["12 Month Rolling GRI"] =  Client::client_gri($client->id());
                $figures["Lifetime GRI"] = Client::client_gri($client->id(), true);

                $figures["12 Month Client Worth"] = $client->clientWorth();
                $figures["Lifetime Client Worth"] = $client->clientWorth(true);

                // number of correspondence items
                $s = new Search(new Correspondence);
                $s->eq("client", $client->id());
                $figures["Correspondence Items"] = $s->count();

                foreach ($figures as $key => $value) {
                    $figures[$key] = "<tr><th>$key</th><td>$value</td></tr>";
                }

                $json["figures"] = implode("\n", $figures);

                echo json_encode($json);

                break;

            case "policy_merge_request":
                $json = [
                    "error" => null,
                    "status" => null
                ];

                $blockIssuers = [
                    11602, // Axa Elevate
                ];

                # we need to ensure that the merge_time is always the same for policy merges
                $merge_time = time();

                //add policies to array
                $policy_array = explode(",", $_GET['policies']);

                //safe guard, ensure all policies belong to the same client
                foreach ($policy_array as $policy) {
                    $p = new Policy($policy, true);
                    if ($p->client() != $_GET['id']) {
                        $json['status'] = "Please only select policies from the same client to merge";
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }

                    if (in_array($p->issuer(), $blockIssuers)){
                        $json['status'] = "There is currently a block on merges for this issuer, contact IT";
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }
                }

                $policy_merge_requested = 0;

                //add entry for merge for each policy
                foreach ($policy_array as $policy) {

                    $dc = new DataChange;
                    $dc->load([
                        'data'=> $_GET['policies'],
                        'comments' => DataChange::COMMENT_POLICY_MERGE,
                        'object_type' => "Policy",
                        'object_id'=> $policy,
                        'proposed_time'=> $merge_time
                    ]);

                    if ($dc->proposed_by() != null) {
                        if ($dc->save()) {
                            $policy_merge_requested++;
                        }
                    } else {
                        $json['status'] = "Policy merge failed, no myps id, Please contact IT.";
                        $json['error'] = true;
                        echo json_encode($json);
                        exit();
                    }
                }

                //check that 'merge entries added' match number of 'policy requested to merge'
                if ($policy_merge_requested == sizeof($policy_array)) {
                    $json['status'] = "Policy merge successfully requested";
                    $json['error'] = false;
                } else {
                    $json['status'] = "Policy merge failed, Please contact IT.";
                    $json['error'] = true;
                }

                echo json_encode($json);

                break;

            case "nb_folder":
                if (isset($_GET['load_contents'])) {
                    $json = [
                        "error" => false,
                        "status" => null,
                        "data" => null,
                        "dir" => null
                    ];

                    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                        $nb_directory = '\\\terry.internal.policyservices.com\\share\\psfiles\\newbusinessdocs\\'.$_GET['id'].'\\';
                    } else {
                        $nb_directory = '/mnt/newbusinessdocs/'.$_GET['id'].'/';
                    }

                    $nb_dir_print = '\\\terry\share\\psfiles\\newbusinessdocs\\'.$_GET['id'].'\\';

                    if (file_exists($nb_directory)) {
                        if ($handle = opendir($nb_directory)) {
                            while (false !== ($entry = readdir($handle))) {
                                if ($entry != "." && $entry != "..") {
                                    $filepath = $nb_directory.$entry;
                                    $filepath_print = $nb_dir_print.$entry;

                                    if (file_exists($filepath)) {
                                        $last_modified = date("d/m/Y", filemtime($nb_directory.$entry));
                                    } else {
                                        $last_modified = "--";
                                    }


                                    $json['data'] .= "<tr><td>".$entry."</td><td>".$filepath_print."</td><td>".$last_modified."</td></tr>";
                                }
                            }

                            $json['dir'] = $nb_dir_print;

                            closedir($handle);
                        } else {
                            $json['data'] = "<tr><td colspan='2'>Could not open New Business folder for this client.</td></tr>";
                            $json['dir'] = false;
                        }
                    } else {
                        $json['data'] = "<tr><td colspan='2'>No New Business folder in relation to this client.</td></tr>";
                        $json['dir'] = false;
                    }

                    if ($json['data'] == null) {
                        $json['data'] = "<tr><td colspan='2'>New Business folder for this client is empty.</td></tr>";
                    }

                    echo json_encode($json);
                } else if (isset($_GET['create'])) {
                    $json = [
                        "error" => false,
                        "status" => null
                    ];

                    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                        $nb_directory = '\\\terry.internal.policyservices.com\\share\\psfiles\\newbusinessdocs\\'.$_GET['id'].'\\';
                    } else {
                        $nb_directory = '/mnt/newbusinessdocs/'.$_GET['id'].'/';
                    }

                    if (!file_exists($nb_directory)) {
                        if (!mkdir($nb_directory, 0777, true)) {
                            $json['error'] = true;
                            $json['status'] = "<tr><td>An error occurred while attempting to create folder, please contact IT.</td></tr>";
                        }
                    }

                    echo json_encode($json);
                } else {
                    echo new Template("client/new_business_dir.html", ["client"=>$client]);
                }

                break;

            case "newLifeEvent":
                $event_type = LifeEventType::all('desc');
                $issuer_array = [];

                $s = new Search(new Policy());
                $s->eq("client", $client->id());
                while ($pol = $s->next(MYSQLI_ASSOC)) {
                    // exclude star policies
                    if (substr(strval($pol->issuer), 0, 1) !== '*') {
                        // build up array of issuers & policies
                        if (!array_key_exists(strval($pol->issuer)."_".$pol->issuer->id(), $issuer_array)) {
                            // add issuer name to start of issuer id, used to order array alphabetically and stripped off in JS
                            $issuer_array[strval($pol->issuer)."_".$pol->issuer->id()] = [];
                            $issuer_array[strval($pol->issuer)."_".$pol->issuer->id()]['issuer'] = strval($pol->issuer);
                            $issuer_array[strval($pol->issuer)."_".$pol->issuer->id()]['policies'][$pol->id()] = strval($pol);
                        } else {
                            $issuer_array[strval($pol->issuer)."_".$pol->issuer->id()]['policies'][$pol->id()] = strval($pol);
                        }
                    }
                }

                asort($issuer_array);

                echo json_encode([
                    "issuer_array" => $issuer_array,
                    "event_type" => $event_type
                    ]);

                break;

            case "createLifeEvent":
                $return = [
                    'id' => false,
                    'error' => true,
                    'feedback' => "An error has occurred, please try again or contact IT if this message persists."
                ];

                $event_type = $_POST['event_id'];
                $affected_person = $_POST['affected_person'];
                $affected_issuers = $_POST['affected_issuers'];

                $life_event = new LifeEvent();

                if (is_null($affected_issuers)) {
                    // search for a client agreement/life event policy, if none, add new policy
                    $search = new Search(new Policy());
                    $search->eq('client', $client->id());
                    $search->eq('issuer', 11764);
                    if ($ca = $search->next()) {
                        // we already have a Client Agreement policy, this will be used later
                        $affected_issuers[$ca->issuer->id()] = $ca->id();
                    } else {
                        // search for a life event policy
                        $search_le = new Search(new Policy());
                        $search_le->eq('client', $client->id());
                        $search_le->eq('issuer', 1197);
                        $search_le->eq('number', 'Life Event');
                        if ($le = $search->next()) {
                            // we already have a life event policy, this will be used later
                            $affected_issuers[$le->issuer->id()] = $le->id();
                        } else {
                            // add new life event policy for later use
                            $p = new Policy();
                            $p->client($client->id());
                            $p->number("Life Event");
                            $p->status(17); // No Business
                            $p->addedhow(1); // Prophet
                            $p->issuer(1197); // ***Partner Account***
                            $p->type(32); // Client Agreement
                            $p->owner($_POST['policy'][0]['owner']);

                            if ($p->save()){
                                $affected_issuers[$p->issuer->id()] = $p->id();
                            }
                        }
                    }
                } else {
                    // use policy information passed from form
                    $life_event->affected_issuers(json_encode($affected_issuers));
                }

                $life_event->client($client->id());
                $life_event->document_for($affected_person);
                $life_event->event($event_type);
                $life_event->status('pending');
                if ($life_event->save()) {
                    $return['id'] = $life_event->id();
                    $return['error'] = false;
                    $return['feedback'] = "Life event successfully added.";
                }

                echo json_encode($return);

                break;

            case "listLifeEvents":
                $events = [
                    'data' => ""
                ];
                $issuers = [];

                $s = new Search(new LifeEvent());
                $s->eq('clientID', $client->id());
                $s->add_order('status', 'DESC');

                while ($event = $s->next(MYSQLI_ASSOC)) {

                    if (!is_null(json_decode($event->affected_issuers()))) {
                        foreach (json_decode($event->affected_issuers()) as $key => $value) {
                            $iss = new Issuer($key, true);
                            if (!in_array(strval($iss), $issuers)) {
                                $issuers[] = strval($iss);
                            }
                        }
                        asort($issuers);
                        $issuer_desc = (sizeof($issuers) <= 3) ? implode(", ", $issuers) : sizeof($issuers)." Issuers..";
                    } else {
                        $issuer_desc = "Partner Only";
                    }

                    $events['data'] .= "<tr data-eventid='".$event->id()."' class='".str_replace('&amp;', '', str_replace(' ', '', $event->document_for()))." ".lcfirst($event->status())."'>
<td class='event-status'>".ucfirst($event->status())."</td>
<td>".$event->event."</td>
<td>".$event->document_for()."</td>
<td title='".implode(", ", $issuers)."'>".$issuer_desc."</td>
<td>
    <button class='life-event-details' data-id='".$event->id()."' type='button'>Details</button>
    <button class='life-event-delete btn-danger' data-id='".$event->id()."' type='button'>Delete</button>
</td>
</tr>";
                }

                echo json_encode($events);
                break;

            case "showLifeEvent":
                $policy_nums = $tracking = $issuers = $issuer_names = [];
                $event = new LifeEvent($_GET['event_id'], true);

                $partner_issuer_id = $pending_count = 0;

                if ($event) {
                    if (!empty($event->affected_issuers())) {
                        $affected_issuers = json_decode($event->affected_issuers(), true);
                    } else {
                        // search for a life event policy
                        $search_le = new Search(new Policy());
                        $search_le->eq('client', (int)$client->id());
                        $search_le->eq('issuer', 1197);
                        $search_le->eq('number', 'Life Event');

                        if ($le = $search_le->next()) {
                            // we already have a life event policy, this will be used later
                            $affected_issuers[$le->issuer->id()][] = $le->id();
                            $partner_issuer_id = $le->issuer->id();
                        } else {
                            $p = new Policy();

                            $p->number("Life Event");
                            $p->issuer(1197);
                            $p->client($client->id());
                            $p->type(32);
                            $p->status(17);

                            $p->owner($p->guessOwner());

                            $p->save();

                            // new life event policy, this will be used later
                            $affected_issuers[$p->issuer->id()][] = $p->id();
                            $partner_issuer_id = $p->issuer->id();
                        }
                    }

                    foreach ($affected_issuers as $key => $value) {
                        if ($key) {
                            $iss = new Issuer($key, true);
                            $issuers[$iss ."_0"] = $iss;
                            $issuers[$iss ."_1"] = $iss;

                            foreach ($value as $policy) {
                                $pol = new Policy($policy, true);
                                $policy_nums[$iss->id()][] = $pol->link();
                            }
                        }
                    }
                    ksort($issuers);

                    $s = new Search(new LifeEventTracking());
                    $s->eq('life_event', $event->id());
                    while ($track = $s->next(MYSQLI_ASSOC)) {
                        if (is_null($track->date())) {
                            $pending_count++;
                        }
                        $tracking[] = $track;
                    }

                    // no tracking has been created, lets build this up
                    if (empty($tracking)) {
                        $i = 0;
                        foreach ($issuers as $iss) {
                            $direction = ($i % 2 !== 0) ? "Received" : "Sent Out";
                            $eventTracking = new LifeEventTracking();
                            $eventTracking->life_event($event->id());
                            $eventTracking->issuer($iss->id());
                            $eventTracking->direction($direction);
                            // save required tracking, user will fill in the blanks as and when
                            if ($eventTracking->save()) {
                                $tracking[] = $eventTracking;
                            }
                            $i++;
                            $pending_count++;
                        }

                        // additional row for returning form to partner
                        $eventTracking = new LifeEventTracking();
                        $eventTracking->life_event($event->id());
                        $eventTracking->issuer(0);
                        $eventTracking->direction("Returned");
                        if ($eventTracking->save()) {
                            $tracking[] = $eventTracking;
                        }
                        $i++;
                        $pending_count++;
                    }
                }

                echo new Template("client/life_event_details.html", [
                    'client' => $client,
                    'event' => $event,
                    'issuers' => $issuers,
                    'policy_nums' => $policy_nums,
                    'tracking' => $tracking,
                    'pending_count' => $pending_count,
                    'partner_issuer_id' => $partner_issuer_id
                ]);

                break;

            case "updateLifeEventTracking":
                $json = [
                  'error' => true,
                  'feedback' => "An error has occurred while updating tracking, try again or contact IT."
                ];

                $tracking = new LifeEventTracking($_POST['trackid'], true);
                $tracking->date($_POST['date']);

                if (isset($_POST['tracking'])) {
                    // only used for sent out
                    $tracking->tracker($_POST['tracking']);
                }

                $tracking->updated_by(User::get_default_instance('id'));
                $tracking->updated_when(\Carbon\Carbon::now());
                if ($tracking->save()) {
                    $json['error'] = false;
                    $json['feedback'] = "Tracking successfully updated";

                    // close provider chase workflow
                    $s = new Search(new Workflow());
                    $s->eq('_object', "LifeEventTracking");
                    $s->eq("index", ($tracking->id() - 1));
                    if ($wf = $s->next(MYSQLI_ASSOC)) {
                        $wf->workflow_next();
                    }

                    $json['pending_count'] = $_POST['pending_count'] - 1;

                    // final step completed
                    if (!$json['pending_count']) {
                        $event = new LifeEvent($tracking->life_event->id(), true);
                        $event->status("Complete");
                        if ($event->save()) {
                            $json['feedback'] = "Tracking successfully updated, Life event complete";
                        }
                    }
                }
                echo json_encode($json);
                break;

            case "lifeEventWorkflow":

                $json = [
                    'error' => false,
                    'feedback' => "Workflow added."
                ];

                $tracking = new LifeEventTracking($_POST['trackid'], true);

                $arr_args = [
                    'process' => 50,
                    'step' => 187,
                    'priority' => 2,
                    'due' => \Carbon\Carbon::now()->addDays(14)->timestamp,
                    'desc' => $_POST['desc'],
                    '_object' => 'LifeEventTracking',
                    'index' => $tracking->id()
                ];

                ob_clean();
                $wf = new Workflow();
                $wf = $wf->workflow_add($arr_args, false, 6, false);
                ob_end_clean();

                echo json_encode($json);

                break;

            case "destroyLifeEvent":
                $json = [
                    'error' => true,
                    'feedback' => "An error has occurred while deleting life event, try again or contact IT."
                ];

                $event = new LifeEvent($_POST['event_id'], true);

                $s = new Search(new LifeEventTracking());
                $s->eq('life_event', $event->id());
                while ($track = $s->next(MYSQLI_OBJECT)) {
                    $track->delete();
                }

                if ($event->delete()) {
                    $json['feedback'] = "Life event successfully deleted";
                    $json['error'] = false;
                }

                echo json_encode($json);
                break;

            case "death_certificate_list":
                $data = false;
                // search for all policies for current client matching criteria set out below
                $s = new Search(new Policy);
                $s -> eq("client", $client->id());
                $s->add_and([
                    $s->nt("issuer", 1045, true, 0),
                    $s->nt("issuer", 1197, true, 0),
                    $s->nt("issuer", 1254, true, 0),
                    $s->nt("issuer", 11391, true, 0),
                    $s->nt("issuer", 11764, true, 0)
                ]);
                $s -> nt("type", 27);
                $s -> nt("archived", null);
                $s -> eq("dc_exempt", 0);


                // for each policy returned format rows and fill variables with relevant information
                while ($p = $s->next(MYSQLI_ASSOC)) {
                    $date1 = $date2 = $date3 = '';
                    $track1 = $track2 = $track3 = '';
                    $id = "";
                    $form = "";

                    $search = new Search(new DeathCertificate());
                    $search->eq("policy", $p->id());

                    while ($x = $search->next(MYSQLI_ASSOC)) {
                        if ($x->form_type) {
                            $form = $x->form_type;
                        }

                        if ($x->date_sent_to_provider) {
                            $date1 = $x->date_sent_to_provider;
                        }

                        if ($x->date_returned_from_provider) {
                            $date2 = $x->date_returned_from_provider;
                            if ($x->date_returned_to_partner) {
                                $date3 = $x->date_returned_to_partner;
                            }
                            if ($x->track_sent_to_provider) {
                                $track1 = $x->track_sent_to_provider;
                            }
                            if ($x->track_returned_from_provider) {
                                $track2 = $x->track_returned_from_provider;
                            }
                            if ($x->track_returned_to_partner) {
                                $track3 = $x->track_returned_to_partner;
                            }

                            $add_or_edit = "Edit";
                            $delete = "<button id='" . $x->id() . "' class='delete'></button>";

                            // display row with relevant information and appropriate buttons
                            $data .= "<tr id='" . $p->id . "'>
						        <td>" . $p->link() . "</td>
						        <td>" . $p->issuer->link() . "</td>
						        <td>" . $form . "</td>
						        <td><p>" . $date1 . "<br />" . $track1 . "</td>
						        <td>" . $date2 . "<br />" . $track2 . "</td>
						        <td>" . $date3 . "<br />" . $track3 . "</td>
                                <td><button id='" . $x->id() . "' class='edit_old'>" . $add_or_edit . "</button>
                                " . $delete . "
                                </td>
					        </tr>";
                        }
                    }
                }

                // now lets search new behaviour that check our policy/death mapping table
                $search_1 = new Search(new DeathCertificate);
                $search_1->eq("client", $client->id());
                while ($x = $search_1->next(MYSQLI_ASSOC)) {

                    $search_2 = new Search(new DeathCertificateMap);
                    $search_2->eq("death_cert", $x->id());
                    $policy_links = [];
                    while ($y = $search_2->next(MYSQLI_ASSOC)) {
                        $policy_links[] = $y->policy->link();
                    }

                    if ($x->form_type) {
                        $form = $x->form_type;
                    }

                    if ($x->date_sent_to_provider) {
                        $date1 = $x->date_sent_to_provider;
                    }

                    if ($x->date_returned_from_provider) {
                        $date2 = $x->date_returned_from_provider;
                        if ($x->date_returned_to_partner) {
                            $date3 = $x->date_returned_to_partner;
                        }
                        if ($x->track_sent_to_provider) {
                            $track1 = $x->track_sent_to_provider;
                        }
                        if ($x->track_returned_from_provider) {
                            $track2 = $x->track_returned_from_provider;
                        }
                        if ($x->track_returned_to_partner) {
                            $track3 = $x->track_returned_to_partner;
                        }

                        $add_or_edit = "Edit";
                        $delete = "<button id='" . $x->id() . "' class='delete'></button>";

                        // display row with relevant information and appropriate buttons
                        $data = "<tr id='" . $x->id . "'>
						        <td>" . implode (", ", $policy_links) . "</td>
						        <td>" . $x->issuer->link() . "</td>
						        <td>" . $form . "</td>
						        <td><p>" . $date1 . "<br />" . $track1 . "</td>
						        <td>" . $date2 . "<br />" . $track2 . "</td>
						        <td>" . $date3 . "<br />" . $track3 . "</td>
                                <td><button id='" . $x->id() . "' class='edit_new'>" . $add_or_edit . "</button>
                                " . $delete . "
                                </td>
					        </tr>".$data;
                    }
                }

                echo $data;

                break;

            case "death_certificate_entry_old":
                $obj = new DeathCertificate;

                $issuer_array = [];
                // we want to get a list of issuer that this client has policies for and place into a select
                $s = new Search(new Policy());
                $s->eq("client", $client->id());
                while ($pol = $s->next(MYSQLI_ASSOC)) {
                    if (substr(strval($pol->issuer), 0, 1) !== '*') {
                        if (!in_array(strval($pol->issuer), $issuer_array)) {
                            $issuer_array[$pol->issuer->id()] = strval($pol->issuer);
                        }
                    }
                }
                asort($issuer_array);

                if (isset($_GET['policy'])) {
                    // search to see if there is an entry in feebase.death_certificates_poa for selected policy
                    $search = new Search(new DeathCertificate);
                    $search->eq("id", $_GET['policy']);

                    // get entry information if exists else get a new object
                    if ($x = $search->next(MYSQLI_ASSOC)) {
                        $obj = $x;
                    }
                }

                echo new Template("client/death_poa_old.html", [
                    "client" => $client->id,
                    "policy" => $_GET['policy'],
                    "obj" => $obj,
                    "issuer_array" => $issuer_array
                ]);

                break;

            case "death_certificate_entry_new":

                if (isset($_GET['death_id'])) {
                    $obj = new DeathCertificate($_GET['death_id'], true);
                } else {
                    $obj = new DeathCertificate();
                }

                $issuer_array = [];
                // we want to get a list of issuer that this client has policies for and place into a select
                $s = new Search(new Policy());
                $s->eq("client", $client->id());
                while ($pol = $s->next(MYSQLI_ASSOC)) {
                    if (substr(strval($pol->issuer), 0, 1) !== '*') {
                        if (!in_array(strval($pol->issuer), $issuer_array)) {
                            $issuer_array[$pol->issuer->id()] = strval($pol->issuer);
                        }
                    }
                }
                asort($issuer_array);

                $search = new Search(new DeathCertificateMap);
                $search->eq("death_cert", $obj->id());
                $policy_array = [];
                while ($x = $search->next(MYSQLI_ASSOC)) {
                    $policy_array[] = $x->policy->link();
                }

                echo new Template("client/death_poa.html", [
                    "client" => $client->id,
                    "policy_array" => $policy_array,
                    "obj" => $obj,
                    "issuer_array" => $issuer_array
                ]);

                break;

            case "client_policies_by_issuer":

                if (isset($_GET['issuer_id']) && isset($_GET['id'])) {
                    $policy_array = [];
                    // we want to get a list of policy based on issuer and client
                    $s = new Search(new Policy());
                    $s->eq("client", $_GET['id']);
                    $s->eq("issuer", $_GET['issuer_id']);
                    while ($pol = $s->next(MYSQLI_ASSOC)) {
                        if (!in_array(strval($pol), $policy_array)) {
                            $policy_array[$pol->id()] = strval($pol);
                        }
                    }
                    asort($policy_array);

                    echo json_encode($policy_array);
                    return;
                }
                break;

            case "death_create":

                $json = [
                  'error' => false,
                  'feedback' => false
                ];

                if (isset($_POST['number_string']) && $_POST['number_string'] != "") {
                    $policy_array = explode(',', $_POST['number_string']); // policyID list
                }

                $params = [];
                parse_str($_POST['form_data'], $params);

                if (isset($params['death_id'])) {
                    $dc = new DeathCertificate($params['death_id'], true);
                } else {
                    $dc = new DeathCertificate();
                }

                if (!isset($params['death_id'])) {
                    $dc->client($_GET['id']);
                    $dc->issuer($params['issuer-selection']);
                    $dc->form_type($params['deathcertificate']['form_type']);
                }

                $dt_1 = $params['deathcertificate']['date_sent_to_provider']['Y'].'-'.$params['deathcertificate']['date_sent_to_provider']['M'].'-'.$params['deathcertificate']['date_sent_to_provider']['D'];
                if ($dt_1 != "--") {
                    $date_1 = new DateTime($dt_1);
                    $dc->date_sent_to_provider($date_1->getTimestamp());
                }
                $dc->track_sent_to_provider($params['deathcertificate']['track_sent_to_provider']);

                $dt_2 = $params['deathcertificate']['date_returned_from_provider']['Y'].'-'.$params['deathcertificate']['date_returned_from_provider']['M'].'-'.$params['deathcertificate']['date_returned_from_provider']['D'];
                if ($dt_2 != "--") {
                    $date_2 = new DateTime($dt_2);
                    $dc->date_returned_from_provider($date_2->getTimestamp());
                }
                $dc->track_returned_from_provider($params['deathcertificate']['track_returned_from_provider']);

                $dt_3 = $params['deathcertificate']['date_returned_to_partner']['Y'].'-'.$params['deathcertificate']['date_returned_to_partner']['M'].'-'.$params['deathcertificate']['date_returned_to_partner']['D'];
                if ($dt_3 != "--") {
                    $date_3 = new DateTime($dt_3);
                    $dc->date_returned_to_partner($date_3->getTimestamp());
                }
                $dc->track_returned_to_partner($params['deathcertificate']['track_returned_to_partner']);

                if ($dc->save()) {
                    if (!isset($params['death_id'])) {
                        $i = 0;
                        foreach ($policy_array as $policy) {
                            $dcm = new DeathCertificateMap();
                            $dcm->death_cert($dc->id());
                            $dcm->policy($policy);
                            if ($dcm->save()) {
                                $i++;
                            }
                        }

                        if ($i == count($policy_array)) {
                            $json['feedback'] = "Death certificate entry successfully recorded";
                        } else {
                            $json['feedback'] = "Death certificate entry successfully recorded, policies not mapped - Contact IT.";
                            $json['error'] = true;
                        }
                    } else {
                        $json['feedback'] = "Death certificate entry successfully updated";
                    }
                } else {
                    $json['feedback'] = "Death certificate failed to save - Contact IT.";
                    $json['error'] = true;
                }

                echo json_encode($json);
                exit();
                break;

            case "death_certificate_corr":
                echo new Template("client/death_poa_corr.html", ["client"=>$client->id, "policy"=>$_GET['policy'], "obj"=>$obj]);
                break;

            case "io_get_table":
                $return = false;

                if (isset($_GET["id"]) && isset($_GET["object_type"])) {
                    // Get due date of next due pro report
                    $m = new Search(new Workflow);
                    $m->eq('step', [41,193]);
                    $m->eq('object_index', (int)$_GET["id"]);
                    $m->eq('object', $_GET["object_type"]);
                    $m->add_order('addedwhen', 'DESC');
                    while ($v = $m->next(MYSQLI_ASSOC)) {
                        $return .= "<tr><td>" . date("d-m-Y ", $v->due()) . "</td><td>" . $v->link() . "</td><td>&nbsp;</td><td>&nbsp;</td>";
                    }

                    // get all relevant corr items under policy
                    $s = new Search(new Correspondence);
                    $s->eq('client', (int)$_GET["id"]);
                    $s->add_or([
                        $s->eq("subject", "Portfolio Valuation", true, 0),
                        $s->eq("subject", "Portfolio Valuation - Elevate Only", true, 0),
                        $s->eq("subject", "ProReport", true, 0),
                        $s->eq("subject", "ProReport Amendment", true, 0),
                        $s->eq("subject", "Portfolio Valuation Amendment - PS", true, 0),
                        $s->eq("subject", "Portfolio Valuation Amendment - Partner", true, 0),
                        $s->eq("subject", "Portfolio Valuation Amendment - Provider", true, 0)
                    ]);
                    $s->add_order("date", "DESC");

                    while ($v = $s->next(MYSQLI_ASSOC)) {
                        $return .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>" . date("d-m-Y", strtotime($v->date())) . "</td><td>   " . $v->link() . "</td></tr>";
                    }

                    echo $return;
                }

                break;

            case "get_merge_history":

                if (isset($_GET["list"])){
                    $return = [
                        "error" => false,
                        "data" => false
                    ];

                    $rows = "";

                    $s = new Search(new DedupeClient());

                    $s->eq("movedtoClientID", $client->id());

                    while($oldClient = $s->next()){
                        $dateString = "NULL";

                        if ($oldClient->addedWhen() != ""){
                            $dateString = date("d/m/Y", $oldClient->addedWhen());
                        }

                        $db = new mydb(REMOTE);

                        $q = "SELECT proposed_by, proposed_time, email 
                              FROM myps.data_change JOIN myps.user_info ON proposed_by = user_info.id 
                              where object_id = " . $oldClient->id() . " and object_type = \"Client\"";

                        $db->query($q);

                        $proposedBy = "NULL";
                        $proposedWhen = "NULL";

                        if ($prop = $db->next()){
                            $proposedBy = $prop['email'];
                            $proposedWhen = date("d/m/Y", $prop['proposed_time']);
                        }

                        $rows .= "<tr>
                            <td width>" . $oldClient->id() . "</td>
                            <td>" . $oldClient->fullName() . "</td>
                            <td>" . $oldClient->address . "</td>
                            <td>" . $oldClient->addedBy->link() . "</td>
                            <td>" . $dateString . "</td>
                            <td>" . $proposedBy . "</td>
                            <td>" . $proposedWhen . "</td>
                        </tr>";
                    }

                    $return["data"] = $rows;

                    echo json_encode($return);
                } else {
                    echo Client::get_template('merge_history.html', array("id" => $client->id));
                }

            break;

            case "new_salutation":
                if (isset($_GET['save'])){
                    $response = [
                        "success" => true,
                        "data" => null,
                    ];

                    $client->salutation($_GET['salutation']);

                    if (!$client->save()){
                        $response['success'] = false;
                        $response['data'] = "Failed to update client salutation";
                    }

                    echo json_encode($response);
                } else {
                    echo new Template("client/new_salutation.html", ['client' => $client, 'salutation' => $_GET['salutation'], 'callbackForm' => $_GET['callbackForm']]);
                }

                break;

            case "load_trustees":
                $response = [
                    'success' => true,
                    'trustees' => []
                ];

                $s = new Search(new Trustee());

                $s->eq("client", $client->id());

                while($t = $s->next(MYSQLI_ASSOC)) {
                    $response['trustees'][] = [
                        "id" => $t->id(),
                        "name" => strval($t),
                        "address" => $t->address->line1() . " (" . $t->address->postcode() . ")",
                    ];
                }

                echo json_encode($response);

                break;

            case "get_link":
                $response = [
                    'success' => true,
                    'link' => null
                ];

                if ($client->id()){
                    $response['link'] = $client->link();
                } else {
                    $response['success'] = false;
                }

                echo json_encode($response);

                break;

            case "partner_check":
                $response['correct'] = false;

                if ($client->partner() == $_GET['partner']){
                    $response['correct'] = true;
                }

                echo json_encode($response);

                break;

            case "get_client_fees":
                $response = [
                    'success' => true,
                    'fees' => null,
                    'data' => null
                ];

                if ($client->get_fees()){
                    $response['fees'] = true;
                    $response['data'] = "This client has paid fees in the last 12 months.";
                } else {
                    $response['fees'] = false;
                    $response['data'] = "This client has not paid any fees in the last 12 months.";
                }

                echo json_encode($response);

                break;

            case "get_ar_ticket_links":
                $response = [
                    'success' => true,
                    'has_ar' => false,
                    'links' => "",
                ];

                $links = $client->ar_links();

                if ($links){
                    $response['has_ar'] = true;
                    $response['links'] = $links;
                }

                echo json_encode($response);

                break;

            case "get_ioh_ticket_links":
                $response = [
                    'success' => true,
                    'has_ioh' => false,
                    'historic_ioh' => false,
                    'links' => "",
                    'historic_links' => ""
                ];

                $s = new Search(new WorkflowStep());

                $s->eq("process", 48);
                $s->eq("active", 1);

                $steps = [];

                while($st = $s->next(MYSQLI_ASSOC)){
                    $steps[] = $st->id();
                }

                $links = $client->wf_links($steps);

                if ($links){
                    $response['has_ioh'] = true;
                    $response['links'] = $links;
                }

                $hLinks = $client->historic_wf_links($steps);

                if ($hLinks){
                     $response['historic_ioh'] = true;
                     $response['historic_links'] = $hLinks;
                }

                echo json_encode($response);

                break;

            case "ongoing_service":
                $response = [
                    'success' => true,
                    'data' => ""
                ];

                $s = new Search(new OngoingService);

                $s->eq("client_id", $client->id());

                $s->add_order("id", "DESC");

                while($os = $s->next(MYSQLI_ASSOC)){
                    $response['data'] .= "<tr id='".$os->id."'>
                        <td>" . $os->id . "</td>                        
                        <td>" . $os->link('View Full Servicing Entry') . "</td>                        
                        <td>" . $os->added_when . "</td>
                        <td>" . $os->review_accepted . "</td>
                        <td>" . $os->review_type . "</td>
                        <td>" . $os->proposed_review_date . "</td>
                        <td>" . $os->review_done . "</td>
                        <td>" . $os->archived_when . "</td>
                    </tr>";
                }

                echo json_encode($response);

                break;

            case "backdated_service":
                if(!empty($_GET['ongoingservice'])){
                    $json = [
                        "status" => true,
                        "data" => false,
                        "error" => "",
                    ];

                    try{
                        $os = new OngoingService();

                        $os->load($_GET['ongoingservice']);
                        $os->client_id($client->id());
                        $os->archived_by(User::get_default_instance("myps_user_id"));
                        $os->archived_when(date("Y-m-d"));

                        $reviewDate = \Carbon\Carbon::parse($os->proposed_review_date());

                        if ($reviewDate->isPast()){
                            $os->archived_when($reviewDate->format("Y-m-d"));
                        } else {
                            throw new Exception("Only past review dates can be entered here!");
                        }

                        if(!$os->save()){
                           throw new Exception("Failed to save ongoing service object");
                        }

                        // close action required tickets relating to review needed
                        $client->endActionRequired("A backdated review date has now been added.", [26,31]);

                    } catch (Exception $e) {
                        $json['status'] = false;
                        $json['error'] = $e->getMessage();
                    }

                    echo json_encode($json);
                } else {
                    echo new Template("client/backdated_service.html", compact("client"));
                }

                break;

            case "add_io":
                $response = [
                    'success' => true,
                    'data' => null
                ];

                try{
                    if ($client->group_scheme()){
                        throw new Exception("Schemes must be added manually");
                    }

                    // check valid email address format
                    if (!empty($client->one->email() && !filter_var($client->one->email(), FILTER_VALIDATE_EMAIL))){
                        throw new Exception("Invalid email format for " . strval($client->one));
                    }

                    if (!empty($client->two->surname()) && !empty($client->two->email() && !filter_var($client->two->email(), FILTER_VALIDATE_EMAIL))){
                        throw new Exception("Invalid email format for " . strval($client->two));
                    }

                    // check valid NI number format
                    $niRegex = "/^(?!BG)(?!GB)(?!NK)(?!KN)(?!TN)(?!NT)(?!ZZ)(?:[A-CEGHJ-PR-TW-Z][A-CEGHJ-NPR-TW-Z])(?:\s*\d\s*){6}([A-D]|\s)$/";

                    if (!empty($client->one->nino()) && !preg_match($niRegex, $client->one->nino())){
                        throw new Exception("Invalid National Insurance Number for " . strval($client->one));
                    }

                    if (!empty($client->two->surname()) && !empty($client->two->nino() && !preg_match($niRegex, $client->two->nino()))){
                        throw new Exception("Invalid National Insurance Number for " . strval($client->two));
                    }

                    // create curl resource
                    $ch = curl_init();

                    // set url
                    // *** CHANGE THIS FOR TESTING ***
                    curl_setopt($ch, CURLOPT_URL, HRZN_URL . "/api/client/" . $client->id());
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, 1);

                    // get stringified data/output. See CURLOPT_RETURNTRANSFER
                    $data = curl_exec($ch);
                    $error = curl_error($ch);

                    if ($error) {
                        throw new Exception("Client - " . $error);
                    } else if ($data) {
                        // We've created a job on HRZN to add the client, now we play the waiting game
                        $c = json_decode($data);

                        $response['success'] = $c->success;
                        $response['data'] = $c->data;

                        if ($c->success){
                            // set clients intelligent office value to pending
                            $client->intelligent_office(-1);

                            $client->save();
                        }
                    } else {
                        throw new Exception("Unknown error adding client");
                    }

                    // Just pass the client ID, HRZN will take care of the rest
//                    foreach(["one", "two"] as $i){
//                        if (!empty($client->$i->surname())){
//                            $client_data = [
//                                "client" => json_encode([
//                                    "migrationReference" => "Prophet",
//                                    "partyType" => "Person",
//                                    "secondaryReference" => $client->id(),
//                                    "name" => $client->salutation(),
//                                    "person" => [
//                                        "niNumber" => $client->$i->nino(),
//                                        "title" => str_replace(".", "", $client->$i->title()),
//                                        "firstName" => $client->$i->forename(),
//                                        "lastName" => $client->$i->surname(),
//                                        "dateOfBirth" => \Carbon\Carbon::parse($client->$i->dob)->toISOString(),
//                                        "salutation" => $client->$i->title() . " " . $client->$i->forename() . " " . $client->$i->surname(),
//                                        "isDeceased" => boolval($client->$i->client_deceased()),
//                                        "nationality" => $client->$i->nationality(),
//                                    ]
//                                ])
//                            ];
//
//                            // create curl resource
//                            $ch = curl_init();
//
//                            // set url
//                            curl_setopt($ch, CURLOPT_URL, "http://192.168.16.194:8000/client");
//                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                            curl_setopt($ch, CURLOPT_POST, 1);
//                            curl_setopt($ch, CURLOPT_POSTFIELDS, $client_data);
//
//                            // get stringified data/output. See CURLOPT_RETURNTRANSFER
//                            $data = curl_exec($ch);
//                            $error = curl_error($ch);
//
//                            if ($error) {
//                                throw new Exception("Client - " . $error);
//                            } else if ($data) {
//                                $c = json_decode($data);
//                                // we'll add this to a new schema mapping prophet entities to their IO counterparts
//                                $ioc = new IOClient();
//
//                                $ioc->id($c->id);
//                                $ioc->prophet_id($client->id());
//
//                                $ioc->save();
//
//                                // set clients intelligent office value to true
//                                $client->intelligent_office(1);
//
//                                $client->save();
//
//                                $ids[] = $c->id;
//                            } else {
//                                throw new Exception("Unknown error adding client");
//                            }
//
//                            // get info about the request
//                            $info = curl_getinfo($ch);
//
//                            // close curl resource to free up system resources
//                            curl_close($ch);
//                        }
//                    }
//
//                    // if client two exists, add a relationship between them
//                    if (count($ids) == 2){
//                        // we have two clients, lets add a relationship between them
//                        // create curl resource
//                        $ch = curl_init();
//
//                        $relation = [
//                            "relation" => json_encode([
//                                "id" => $ids[1]
//                            ])
//                        ];
//
//                        // set url
//                        curl_setopt($ch, CURLOPT_URL, "http://192.168.16.194:8000/client/relationship/" . $ids[0]);
//                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                        curl_setopt($ch, CURLOPT_POST, 1);
//                        curl_setopt($ch, CURLOPT_POSTFIELDS, $relation);
//
//                        // get stringified data/output. See CURLOPT_RETURNTRANSFER
//                        $data = curl_exec($ch);
//                        $error = curl_error($ch);
//
//                        if ($error) {
//                            throw new Exception("Relationship - " . $error);
//                        } else if ($data) {
//                            // client relationship added
//                        } else {
//                            throw new Exception("Unknown error adding relationship");
//                        }
//
//                        // get info about the request
//                        $info = curl_getinfo($ch);
//
//                        // close curl resource to free up system resources
//                        curl_close($ch);
//                    }
//
//                    // add policies for this/these client(s) with the correct policy owner
//                    $s = new Search(new Policy());
//
//                    $star_issuers = [
//                        1045, // ***Unassigned***
//                        1197, // ***Partner Account***
//                        1254, // **Contract**
//                        11391, // ***Client Letter***
//                        11764, // ***Client Agreement***
//                    ];
//
//                    // policy type check
//                    $dont_import_types = [
//                        27, // Agency
//                        31, // Fee Agreements
//                        32, // Client Agreement
//                        35, // Direct Debit
//                        36 // Client Letter
//                    ];
//
//                    $s->eq("client", $client->id());
//                    $s->eq("archived", null);
//                    $s->eq("status", LIVE_POLICY_STATUS);
//                    $s->nt("issuer", $star_issuers);
//                    $s->nt("type", $dont_import_types);
//
//                    // array to check which policies have not gone up
//                    $failedPolicies = [];
//
//                    while($p = $s->next(MYSQLI_ASSOC)){
//                        $policyOwner = [];
//                        $addPolicy = false;
//
//                        // TODO deceased check
//                        switch(intval($p->owner())){
//                            case 1:
//                                $policyOwner[] = [
//                                    "id" => $ids[0] // client 1
//                                ];
//
//                                $addPolicy = true;
//
//                                break;
//
//                            case 2:
//                                if (array_key_exists(1, $ids)){
//                                    $policyOwner[] = [
//                                        "id" => $ids[1] // client 2
//                                    ];
//
//                                    $addPolicy = true;
//                                }
//
//                                break;
//
//                            case 3:
//                                $policyOwner[] = [
//                                    "id" => $ids[0]
//                                ];
//
//                                if (array_key_exists(1, $ids)){
//                                    $policyOwner[] = [
//                                        "id" => $ids[1] // client 2
//                                    ];
//                                }
//
//                                $addPolicy = true;
//
//                                break;
//
//                            default:
//                                // TODO add cases for all other owner scenarios
//
//                                break;
//                        }
//
//                        // TODO find the issuer ID for IO
//                        // get out if this issuer hasnt been added yet
//                        $s = new Search(new IOIssuer());
//
//                        $s->eq("prophet_id", $p->issuer());
//
//                        if ($i = $s->next(MYSQLI_ASSOC)){
//                            // this issuer is mapped from PS to IO
//
//                            // dont add unknown policy types, return these to the user
//                            if ($p->type() == 17){
//                                $addPolicy = false;
//                            }
//
//                            $pt = new IOPolicyType($p->type(), true);
//
//                            if ($addPolicy) {
//                                $policy_data = [
//                                    "policy" => json_encode([
//                                        "productProvider" => [
//                                            "id" => $i->id() // this is the ID from IO
//                                        ],
//                                        "planType" => [
//                                            "name" => $pt->io_name() // load IO name from database mapping table
//                                        ],
//                                        "lifecycle" => [
//                                            "id" => 16939 // Pre-Existing
//                                        ],
//                                        "startsOn" => \Carbon\Carbon::parse($p->start_date())->toISOString(),
//                                        "otherReferences" => [
//                                            "migrationReference" => "Prophet",
//                                        ],
//                                        "clientCategory" => "Retail",
//                                        "owners" => $policyOwner,
//                                        "isClientSuitableForTargetMarket" => false,
//                                        "policyNumber" => $p->number(),
//                                        "endsOn" => \Carbon\Carbon::parse($p->maturity())->toISOString(),
//                                        "interestRate" => 0
//                                    ])
//                                ];
//
//                                // create curl resource
//                                $ch = curl_init();
//
//                                // set url
//                                curl_setopt($ch, CURLOPT_URL, "http://192.168.16.194:8000/policy");
//                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                                curl_setopt($ch, CURLOPT_POST, 1);
//                                curl_setopt($ch, CURLOPT_POSTFIELDS, $policy_data);
//
//                                // get stringified data/output. See CURLOPT_RETURNTRANSFER
//                                $data = curl_exec($ch);
//                                $error = curl_error($ch);
//
//                                if ($error) {
//                                    $failedPolicies[] = $p->number();
//
//                                    throw new Exception("Policy (" . $p->number() . ") - " . $error);
//                                } else if ($data) {
//                                    // policy added successfully
//                                    $p2 = json_decode($data);
//                                    // save policy ID to bridge DB
//                                    $iop = new IOPolicy();
//
//                                    $iop->id($p2->id);
//                                    $iop->prophet_id($p->id());
//
//                                    $iop->save();
//                                } else {
//                                    $failedPolicies[] = $p->number();
//
//                                    throw new Exception("Unknown error adding policy (" . $p->number() . ")");
//                                }
//
//                                // get info about the request
//                                $info = curl_getinfo($ch);
//
//                                // close curl resource to free up system resources
//                                curl_close($ch);
//                            } else {
//                                $failedPolicies[] = $p->number();
//                            }
//                        }
//
//                    }
//
//                    $response['data'] = "Client added to IO.";
//
//                    if (count($failedPolicies)){
//                        $response['data'] .= " Failed to add the following policies: " . implode(", ", $failedPolicies) . ". Please add these policies manually.";
//                    }
                } catch (Exception $e) {
                    $response['success'] = false;
                    $response['data'] = "Failed to add details to IO. Error: " . $e->getMessage();
                }

                echo json_encode($response);

                break;

            case "unsegment_confirm":
                echo Client::get_template("unsegment.html", compact("client"));

                break;

            case "unsegment_client":
                $response = [
                    'success' => true,
                    'data' => null
                ];

                try {
                    $client->unsegment($_GET['reason']);

                    $message = "This client has now been unsegmented from a servicing level, therefore this ticket has now been been closed.";

                    $client->endWorkflow($message,[41,188,189,193], true);
                    $client->endActionRequired($message, [26,31,32,33,34]);
                } catch (Exception $e) {
                    $response['success'] = false;
                    $response['data'] = $e->getMessage();
                }

                echo json_encode($response);

                break;

            case "unsegment_reason":
                $response = [
                    "success" => false,
                    "data" => null,
                ];

                $s = new Search(new ActionReason());
                $s->eq("object", "Client");
                $s->eq("object_id", $client->id());
                $s->eq("action", "Unsegment Client");
                $s->add_order("id", "DESC");
                $s->limit(1);

                if ($ar = $s->next(MYSQLI_ASSOC)){
                    $response['success'] = true;
                    $response['data'] = "Client was unsegmented by " . strval($ar->created_by) . " on " . \Carbon\Carbon::createFromTimestamp($ar->created_when())->toDayDateTimeString() . ". Reason: " . $ar->reason();
                }

                echo json_encode($response);

                break;

            case "restore_service_level":
                if(isset($_POST['servicingpropositionlevel'])){
                    $json = [
                        "error" => null,
                        "status" => true,
                        "data" => null,
                    ];

                    try{
                        // log the reason
                        if(empty($_POST['reason'])){
                            throw new Exception("A reason is required");
                        }

                        $ar = new ActionReason();

                        $ar->load([
                            "object" => "Client",
                            "object_id" => $client->id(),
                            "reason" => $_POST['reason'],
                            "action" => "Restore Servicing Level",
                        ]);

                        $ar->save();

                        $spl = new ServicingPropositionLevel();

                        $spl->load($_POST['servicingpropositionlevel']);

                        $spl->method("manual_restore");

                        if(array_key_exists($spl->level(), ServicingPropositionLevel::PERCENT)){
                            $spl->percent(ServicingPropositionLevel::PERCENT[$spl->level()]);
                        }

                        if ($spl->save()) {
                            IncomeHold::releaseIncome($client->id());
                        }
                    } catch (Exception $e) {
                        $json['status'] = false;
                        $json['error'] = $e->getMessage();
                    }

                    echo json_encode($json);
                } else {
                    $spl = new ServicingPropositionLevel();

                    // find the last client agreement
                    $s = new Search(new Policy());

                    $s->eq("client", $client->id());
                    $s->eq("issuer", 11764);

                    $s->add_order("id", "DESC");
                    $s->limit(1);

                    if($p = $s->next(MYSQLI_ASSOC)){
                        $added = \Carbon\Carbon::createFromTimestamp($p->addedwhen());
                        $spl->level_when($added->format("Y-m-d"));
                    }

                    $spl->client_id($client->id());
                    $spl->partner_id($client->partner->id());

                    echo new Template("client/servicing_level.html", compact("spl"));
                }

                break;

            case "servicing_upload_form":
                $s2 = new Search(new OngoingService);
                $s2->eq("client_id", $client->id());
                $s2->eq("review_done", 0);
                $s2->eq("archived_when", null);

                if ($oss = $s2->next(MYSQLI_ASSOC)){
                    $os = $oss;
                } else {
                    $os = new OngoingService();

                    $os->client_id($client->id());
                }

                echo new Template("client/new_ongoing_service.html", ["os"=>$os]);

                break;

            case "upload_servicing_document":

                $response = [
                    'success' => true,
                    'data' => null
                ];

                $file = $_FILES['servicing_document'];

                $partnerID = $client->partner->id();
                $practiceID = $client->partner->get_practice()->id();

                try {
                    $s2 = new Search(new OngoingService);
                    $s2->eq("client_id", $client->id());
                    $s2->eq("review_done", 0);
                    $s2->eq("archived_when", null);

                    if ($os = $s2->next(MYSQLI_ASSOC)){
                        // found our open ongoing service object
                        $hash = md5(file_get_contents($file['tmp_name']));
                        $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

                        $unique_name = $hash.".".$fileExtension;
                        $directory = $practiceID . "/" . $partnerID . "/" . $unique_name;

                        $result = $s3Client->putObject([
                            'Bucket' => $bucket,
                            'Key'    => $directory,
                            'Body'   => file_get_contents($file['tmp_name']),
                            'ServerSideEncryption' => 'AES256'
                        ]);

                        if ($result['@metadata']['statusCode'] == '200') {
                            // file successfully uploaded, add entry to database
                            $ds = new DocumentStorage();
                            $ds->partnerID($partnerID);
                            $ds->object_id($client->id());
                            $ds->object("Client");
                            $ds->filepath($directory);
                            $ds->original_name($file['name']);
                            $ds->friendly_name($file['name']);
                            $ds->hash($unique_name);
                            $ds->mime_type($fileExtension);
                            $ds->file_size($file['size']);
                            $ds->servicing_document(1);
                        }

                        if ($ds->save()) {
                            // file uploaded and entry in DB, all good!

                            // we can now finalise our ongoing service object
                            $os->document_upload_ids($ds->id());
                            $os->review_done(1);
                            $os->archived_by(User::get_default_instance("myps_user_id"));
                            $os->archived_when(date("Y-m-d"));

                            if ($os->save()){
                                $json['data'] = "File successfully uploaded.";
                            } else {
                                // file failed to add to DB, inform user
                                $response['success'] = false;
                                $json['data'] = "File uploaded but failed to link to ongoing service object. Contact IT";
                            }
                        } else {
                            // file failed to add to DB, inform user
                            $response['success'] = false;
                            $json['data'] = "File not added to database.";
                        }
                    } else {
                        $response['success'] = false;
                        $json['data'] = "Failed to find open ongoing service object.";
                    }

                } catch( Exception $e ) {
                    $response['success'] = false;
                    $response['data'] = $e->getMessage();
                }

                echo json_encode($response);

                break;

            case "load_for_merging":
                $index = $_GET['index'];

                $response = [
                    "success" => true,
                    "data" => null
                ];

                foreach(get_object_vars($client->$index) as $key => $value){
                    $response['data'][$key] = strval($value);
                }

                echo json_encode($response);

                break;

            case "delete_records":
                if (!empty($_GET['reason'])){
                    $response = [
                        "success" => true,
                        "data" => null,
                    ];

                    try{
                        // add an entry to action reason
                        $ar = new ActionReason();

                        $ar->object("Client");
                        $ar->object_id($client->id());
                        $ar->reason($_GET['reason']);
                        $ar->action("Delete");

                        if ($ar->save()){
                            $client->deleteRecords();
                        } else {
                            throw new Exception("Failed to audit delete reason");
                        }
                    } catch (Exception $e) {
                        $response['success'] = false;
                        $response['data'] = "Failed to delete client: " . $e->getMessage();
                    }

                    echo json_encode($response);
                } else {
                    echo new Template("client/delete_records.html", compact("client"));
                }

                break;

            case "extensions":
                $s = new Search(new ClientReviewExtension());
                $s->eq("client", $client->id());
                $s->add_order("id", "DESC");

                $extensions = [];

                $json = "";

                while($ext = $s->next(MYSQLI_ASSOC)){
                    $extensions[] = $ext;

                    $json .= "<tr>
                                <td>" . $ext->original_date . "</td>
                                <td>" . $ext->extra_months . "</td>
                                <td>" . $ext->reason_extended . "</td>
                                <td>" . $ext->added_by->link() . "</td>
                                <td>" . $ext->added_when . "</td>
                                <td>" . $ext->checkArchived() . "</td>
                                <td>" . $ext->reason_archived . "</td>
                                <td>" . $ext->archived_by->link() . "</td>
                                <td>" . $ext->archived_when . "</td>
                            </tr>";
                }

                if(!empty($_GET['reload'])){
                    $response = [
                        'success' => true,
                        'data' => $json
                    ];

                    echo json_encode($response);
                } else {
                    echo new Template("client/review_extensions/list.html", compact([
                        "extensions",
                        "client",
                    ]));
                }

                break;

            case "add_extension":
                $extension = new ClientReviewExtension();

                $extension->client($client->id());

                // check the current date a review is due by
                $reviewDates = $client->reviewCountdown();
                $extension->original_date($reviewDates['next']);

                echo new Template("client/review_extensions/new.html", compact([
                    "extension"
                ]));

                break;

            case "archive_extension":
                $extension = new ClientReviewExtension($_GET['extension'], true);

                if(!empty($_GET['archive_reason'])){
                    try{
                        $response = [
                            "success" => true,
                            "data" => null
                        ];

                        $extension->reason_archived($_GET['archive_reason']);
                        $extension->archived_by(User::get_default_instance("myps_user_id"));
                        $extension->archived_when(date("Y-m-d H:i:s"));

                        if($extension->save()){
                            $response['data'] = "Successfully archived client extension";
                        }
                    } catch (Exception $e){
                        $response['success'] = false;
                        $response['data'] = $e->getMessage();
                    }

                    echo json_encode($response);
                } else {
                    echo new Template("client/review_extensions/archive.html", compact([
                        "extension",
                    ]));
                }

                break;

            case "swap":
                $response = [
                    "success" => true,
                    "data" => null,
                ];

                try{
                    $dbo = new DatabaseObject();
                    $tempClientOne = $dbo->flat_array($client->one);

                    $client->one->load($dbo->flat_array($client->two));
                    $client->two->load($tempClientOne);

                    if($client->save()){
                        $s = new Search(new Policy());

                        $s->eq("client", $client->id());

                        $allPolicies = $savedPolicies = 0;

                        while($p = $s->next(MYSQLI_ASSOC)){
                            if($p->owner() == 1){
                                $allPolicies++;
                                $p->owner(2);

                                if($p->save()){
                                    $savedPolicies++;
                                }
                            } elseif($p->owner() == 2){
                                $allPolicies++;
                                $p->owner(1);

                                if($p->save()){
                                    $savedPolicies++;
                                }
                            }
                        }

                        if($savedPolicies != $allPolicies){
                            throw new Exception("Swapped clients but failed to update all policy owners");
                        }

                        $s = new Search(new ClientIDDocument());

                        $s->eq("client", $client->id());

                        $allDocuments = $savedDocuments = 0;

                        while($d = $s->next(MYSQLI_ASSOC)){
                            if($d->index() == 1){
                                $allDocuments++;
                                $d->index(2);

                                if($d->save()){
                                    $savedDocuments++;
                                }
                            } elseif($d->index() == 2){
                                $allDocuments++;
                                $d->index(1);

                                if($d->save()){
                                    $savedDocuments++;
                                }
                            }
                        }

                        if($savedDocuments != $allDocuments){
                            throw new Exception("Swapped clients and updated policy owners, but failed to update all client ID documents");
                        }

                        $s = new Search(new VulnerableClient());

                        $s->eq("client", $client->id());

                        $allVulnerable = $savedVulnerable = 0;

                        while($v = $s->next(MYSQLI_ASSOC)){
                            if($v->client_index() == 1){
                                $allVulnerable++;
                                $v->client_index(2);

                                if($v->save()){
                                    $savedVulnerable++;
                                }
                            } elseif($v->client_index() == 2){
                                $allVulnerable++;
                                $v->client_index(1);

                                if($v->save()){
                                    $savedVulnerable++;
                                }
                            }
                        }

                        if($savedVulnerable != $allVulnerable){
                            throw new Exception("Swapped clients, updated policy owners and client ID documents, but failed to update vulnerable clients");
                        }

                        // try to flip index's on IO bridge
                        $db = new mydb(IO);

                        $db->query("UPDATE clients SET prophet_index = NULL WHERE prophet_id = " . $client->id() . " AND prophet_index = 1");
                        $db->query("UPDATE clients SET prophet_index = 1 WHERE prophet_id = " . $client->id() . " AND prophet_index = 2");
                        $db->query("UPDATE clients SET prophet_index = 2 WHERE prophet_id = " . $client->id() . " AND prophet_index = NULL");
                    } else {
                        $response['success'] = false;
                        $response['data'] = "Failed to save client details after swapping";
                    }
                } catch (Exception $e) {
                    $response['success'] = false;
                    $response['data'] = $e->getMessage();
                }

                echo json_encode($response);

                break;
        }
    }
    
    if (!isset($_GET["do"])) {
        switch ($_GET['export']) {
            case "partner_clients":
                $db = new mydb();
                $c = new Client;

                $q= "SELECT clientID, clientTitle1, clientFname1, clientSname1, clientDOB1, clientTitle2, clientFname2, clientSname2, clientDOB2 ,partnerID,
				clientaddress1, clientaddress2, clientaddress3, clientaddresspostcode, clienttelno
				FROM tblclient where partnerID = ".$_GET['id'];

                $db->query($q);
                echo "<tr>
						<td>Partner ID</td>
						<td>Client ID</td>
						<td>Client 1 Title</td>
						<td>Client 1 Forename</td>
						<td>Client 1 Surname</td>
						<td>Client 1 DOB</td>
						<td>Client 2 Title</td>
						<td>Client 2 Forename</td>
						<td>Client 2 Surname</td>
						<td>Client 2 DOB</td>
						<td>Client Address Line 1</td>
						<td>Client Address Line 2</td>
						<td>Client Address Line 3</td>
						<td>Client Postcode</td>
						<td>Client Contact No.</td>
					</tr>";

                while ($records = $db -> next(MYSQLI_ASSOC)) {
                    echo "<tr>
						<td>".$records['partnerID']."</td>
						<td>".$records['clientID']."</td>
						<td>".$records['clientTitle1']."</td>
						<td>".$records['clientFname1']."</td>
						<td>".$records['clientSname1']."</td>
						<td>".$records['clientDOB1']."</td>
						<td>".$records['clientTitle2']."</td>
						<td>".$records['clientFname2']."</td>
						<td>".$records['clientSname2']."</td>
						<td>".$records['clientDOB2']."</td>
						<td>".$records['clientaddress1']."</td>
						<td>".$records['clientaddress2']."</td>
						<td>".$records['clientaddress3']."</td>
						<td>".$records['clientaddresspostcode']."</td>
						<td>".$records['clienttelno']."</td>
					</tr>";
                }
            
                echo json_encode($json);
            
                break;
            
            case "practice_clients":
                $db = new mydb();
                $c = new Client;

                $q= "SELECT practice_id, clientID, clientTitle1, clientFname1, clientSname1, clientDOB1, clientTitle2, clientFname2, clientSname2, clientDOB2 ,tblclient.partnerID,
				clientaddress1, clientaddress2, clientaddress3, clientaddresspostcode, clienttelno
				FROM tblclient
				INNER JOIN tblpartner
				  ON tblclient.partnerID = tblpartner.partnerID
				INNER JOIN practice_staff
				  ON tblpartner.partnerID = practice_staff.partner_id
				WHERE practice_id = ".$_GET['id'];

                $db->query($q);
                echo "<tr>
						<td>Practice ID</td>
						<td>Partner ID</td>
						<td>Client ID</td>
						<td>Client 1 Title</td>
						<td>Client 1 Forename</td>
						<td>Client 1 Surname</td>
						<td>Client 1 DOB</td>
						<td>Client 2 Title</td>
						<td>Client 2 Forename</td>
						<td>Client 2 Surname</td>
						<td>Client 2 DOB</td>
						<td>Client Address Line 1</td>
						<td>Client Address Line 2</td>
						<td>Client Address Line 3</td>
						<td>Client Postcode</td>
						<td>Client Contact No.</td>
					</tr>";
                while ($records = $db -> next(MYSQLI_ASSOC)) {
                    echo "<tr>
						<td>".$records['practice_id']."</td>
						<td>".$records['partnerID']."</td>
						<td>".$records['clientID']."</td>
						<td>".$records['clientTitle1']."</td>
						<td>".$records['clientFname1']."</td>
						<td>".$records['clientSname1']."</td>
						<td>".$records['clientDOB1']."</td>
						<td>".$records['clientTitle2']."</td>
						<td>".$records['clientFname2']."</td>
						<td>".$records['clientSname2']."</td>
						<td>".$records['clientDOB2']."</td>
						<td>".$records['clientaddress1']."</td>
						<td>".$records['clientaddress2']."</td>
						<td>".$records['clientaddress3']."</td>
						<td>".$records['clientaddresspostcode']."</td>
						<td>".$records['clienttelno']."</td>
					</tr>";
                }
            
                echo json_encode($json);
            
                break;
        }
    }
} else {
    if (isset($_GET['do']) && $_GET['do'] != "searchScheme"){
        switch ($_GET['do']){
            case "dupe_check":
                $response = [
                    'success' => true,
                    'data' => "",
                ];

                if ($_GET['clientsname1'] == "") {
                    $_GET['clientsname1'] = null;
                }

                if ($_GET['clientsname2'] == "") {
                    $_GET['clientsname2'] = null;
                }

                $s = new Search(new Client());

                $s->add_or([
                    $s->eq("one->surname", $_GET['clientsname1'], false, 0),
                    $s->eq("two->surname", $_GET['clientsname1'], false, 0),
                    $s->eq("one->surname", $_GET['clientsname2'], false, 0),
                    $s->eq("two->surname", $_GET['clientsname2'], false, 0),
                ]);
                $s->add_or([
                    $s->eq("one->dob", $_GET['client1dob'], false, 0),
                    $s->eq("two->dob", $_GET['client1dob'], false, 0),
                    $s->eq("one->dob", $_GET['client2dob'], false, 0),
                    $s->eq("two->dob", $_GET['client2dob'], false, 0),
                ]);
                // cover a couple different possibilities for postcode formatting
                $s->add_or([
                    $s->eq('clientaddresspostcode', $_GET['postcode'], false, 0),
                    $s->eq('clientaddresspostcode', substr($_GET['postcode'], 0, 3) . ' ' . substr($_GET['postcode'], 3), false, 0),
                    $s->eq('clientaddresspostcode', substr($_GET['postcode'], 0, 4) . ' ' . substr($_GET['postcode'], 4), false, 0),
                    $s->eq('clientaddresspostcode', str_replace(" ", "", $_GET['postcode']), false, 0)
                ]);
                $s->eq("archived", null);
                $s->nt("id", $_GET['clientID']);

                while ($c = $s->next(MYSQLI_ASSOC)) {
                    $response['data'] .= "<tr data-id='" . $c->id() . "'>
                        <td>" . $c->link() . "</td>
                        <td>" . $c->partner->link() . "</td>
                        <td>" . $c->address . "</td>
                        <td><input type='checkbox' class='client_option' data-id='" . $c->id() . "' /></td>
                    </tr>";
                }

                if ($response['data'] == ""){
                    $response['success'] = false;
                }

                echo json_encode($response);

                break;

            case "merge_request":
                $clientIDs = json_decode($_GET['clients']);

                $clients = [];

                foreach ($clientIDs as $id){
                    $clients[] = new Client($id, true);
                }

                echo new Template("client/merge.html", ["clients" => $clients]);

                break;
        }
    } else {
        function client_search_scheme(&$search, $str)
        {
            $search->add_and([
                $search->eq("group_scheme", 1, false, 0),
                $search->eq("scheme_name", "%".$str["text"]."%", true, 0),
            ]);
        }

        function client_search_dob(&$search, $str)
        {
            list($d,$m,$y) = explode("/", $str["text"]);
            $dob = "$y-$m-$d";
            $search->add_or([
                $search->eq("one->dob", $dob, false, 0),
                $search->eq("two->dob", $dob, false, 0)
            ]);
        }

        function client_search_nino(&$search, $str)
        {
            $search->add_or([
                $search->eq("one->nino", $str["text"], true, 0),
                $search->eq("two->nino", $str["text"], true, 0)
            ]);
        }

        function client_search_name(&$search, $str)
        {
            if (preg_match('/([\d\w\s]+) [&+] ([\d\w\s]+)/', $str["text"], $match)) {
                $str["text"] = $match[1];
                client_search_name($search, $str);

                $str["text"] = $match[2];
                client_search_name($search, $str);

                return;
            }

            if (isset($_GET['do']) && $_GET['do'] == 'searchScheme') {
                // Group scheme 'client' search
                $sname_wildcard = true;
                $x["surname"] = "%".$str["text"]."%";

            } else {
                // Client search
                $x = parse_name($str["text"]);
                $sname_wildcard = ($x["wildcard"]=="surname");
                ($sname_wildcard) ? $x["surname"] .= "%" : $x["forename"] .= "%";
            }

            $conditions = [];

            if (isset($x["forename"])) {
                //we always require a wildcard here due to middle names
                $search->add_or([
                    $search->eq("one->forename", $x["forename"]."%", true, 0),
                    $search->eq("two->forename", $x["forename"]."%", true, 0)
                ]);
            }

            $search->add_or([
                $search->eq("one->surname", $x["surname"], $sname_wildcard, 0),
                $search->eq("two->surname", $x["surname"], $sname_wildcard, 0)
            ]);
        }

        $client = new Client;
        $search = new Search($client);
        $search->calc_rows = true;

        $search
            -> flag("id", Search::DEFAULT_INT)
            -> flag("name", Search::CUSTOM_FUNC, "client_search_name", Search::DEFAULT_STRING)
            -> flag("postcode", "address->postcode", Search::PATTERN, "/".Patterns::UK_POSTCODE."/")
            -> flag("nino", Search::CUSTOM_FUNC, "client_search_nino", Search::PATTERN, "/".Patterns::NINO."/")
            -> flag("dob", Search::CUSTOM_FUNC, "client_search_dob")
            -> flag("partner")
            -> flag("s", Search::CUSTOM_FUNC, "client_search_scheme");

        if (isset($_GET["search"])) {
            if (isset($_GET["filter"]) && is_array($_GET["filter"])) {
                $search->apply_filters($_GET["filter"]);
            }

            if (isset($_GET["limit"]) && (int)$_GET["limit"] > 0) {
                $search->set_limit((int) $_GET["limit"]);
            } else {
                $search->set_limit(15);
            }

            if (isset($_GET["skip"]) && (int)$_GET["skip"] > 0) {
                $search->set_offset((int) $_GET["skip"]);
            }

            $matches = [];

            if (!empty($_GET['partner_order'])){
                $search->add_order("partnerID = " . $_GET['partner_order'], "DESC");
            }

            $search->order($client->NAME_ORDER);

            // Additional options
            if (isset($_REQUEST[ "partner" ])) {
                $search->add_and($search->object->partner->to_sql((int)$_REQUEST[ "partner" ]));
            }

            if (isset($_GET['archive'])) {
                if ($_GET['archive'] == 'hide') {
                    $search->eq("archived", null);
                }
            }

            while ($m = $search->next(addslashes($_GET["search"]))) {
                $postcode = (strlen($m->address->postcode)) ? "(".$m->address->postcode.")" : false;

                if (isset($_GET['mandate'])) {
                    $matches["results"][] = ["text"=>"$m $postcode - $m->partner", "link"=>$m->link("$m $postcode"), "value"=>"$m->id"];
                } else {
                    $matches["results"][] = ["text"=>"$m $postcode", "link"=>$m->link("$m $postcode"), "value"=>"$m->id"];
                }
            }

            $matches["remaining"] = $search->remaining;

            echo json_encode($matches);
        } else {
            echo new Template("generic/search.html", ["help"=>$search->help_html(), "object"=>$client]);
        }
    }
}
