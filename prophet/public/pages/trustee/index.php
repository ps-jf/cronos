<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET["client"])) {
    $client = new Client($_GET["client"], true);

    if (isset($_GET["do"])) {
        switch ($_GET["do"]) {
            case "list":
                $data = false;

                $s = new Search(new Trustee);
                $s->eq("client", $_GET['client']);

                while ($x = $s->next(MYSQLI_ASSOC)) {
                    $entry = (array)$x;
                    $t = new Trustee($entry['id'], true);

                    isset($t->company) && $t->company != "" ? $tName = $t->company : $tName = $t->__toString();

                    $data .= "<tr id='" . $t->id . "'>
						        <td>" . $tName . "</td>
                                <td>" . $t->dob . "</td>
                                <td>" . $t->nino . "</td>
                                <td>" . $t->address . "</td>
                                <td>" . $t->servicing_level() . "</td>
                                <td><button id='" . $t->id . "' class='edit'>Edit</button>
                                    <button id='" . $t->id . "' class='delete'></button>
                                </td>
					        </tr>";
                }

                echo $data;

                break;

            case "list_trustee_addresses":
                echo new Template("trustee/address_options.html", ["client" => $client->id()]);
                break;

            case "list_trustee_address_data":

                $json = [
                    "data" => ""
                ];

                $s = new Search(new Trustee);
                $s->eq("client", $_GET['client']);

                while ($x = $s->next(MYSQLI_ASSOC)) {
                    $entry = (array)$x;
                    $t = new Trustee($entry['id'], true);

                    $json['data'] .= "<tr>
                                    <td><input type='checkbox' class='trustee' value='".$t->id()."'/></td>
                                    <td class='trustee-name'>".$t->title. " ". $t->firstname ." ". $t->surname." </td>                                   
                                   <td class='trustee-address'> " . $t->address . "</td>
                               </tr>
                              ";
                }

                echo json_encode($json);

                break;

            case "list_trustee_addressee":

                $json = [
                    "data" => ""
                ];

                $s = new Search(new Trustee);
                $s->eq("client", $_GET['client']);

                while ($x = $s->next(MYSQLI_ASSOC)) {
                    $entry = (array)$x;
                    $t = new Trustee($entry['id'], true);

                    $json['data'] .= "<tr class='trustee_choice_tr'>
                                    <td></td>
                                    <td class='trustee'>
                                    <input type='checkbox' class='trustee' id='include_one' name='include_one' >" .
                        el($t->title, 'title') .
                        el($t->firstname, 'forename') .
                        el($t->surname, 'surname') . "
                                   </td>
                               </tr>
                               <tr class=' trustee_choice_tr trustee_choice_address'>
                                   <td></td>
                                   <td> 
                                   " . el($t->address->line1) . "<br>" .
                        el($t->address->line2) . "<br>" .
                        el($t->address->line3) . "<br>" .
                        el($t->address->postcode) . "
                                   </td>
                               </tr>
                              ";
                }

                echo json_encode($json);

                break;

            case "create":
                echo new Template("trustee/new.html", ["client" => $client->id]);
                break;

            case "edit":
                $trustee = new Trustee(($_GET['id']), true);
                echo new Template("trustee/edit.html", ["t" => $trustee]);
                break;

            case "policySearch":
                $json = [
                    'policies' => false
                ];

                $s = new Search(new Policy);
                $s->eq('client', $_GET['client']);
                while ($policy = $s->next(MYSQLI_ASSOC)) {
                    $json['policies'][$policy->id()] = "<td>".$policy->link()."</td><td>".$policy->issuer->link()."</td>";
                }

                echo json_encode($json);
                break;

            case "select":
                $data = null;

                $s = new Search(new Trustee);
                $s->eq("client", $_GET['client']);

                while ($x = $s->next(MYSQLI_ASSOC)) {
                    $entry = (array)$x;
                    $t = new Trustee($entry['id'], true);

                    isset($t->company) && $t->company != "" ? $tName = $t->company : $tName = $t->__toString();

                    $data .= "<tr id='" . $t->id . "'>
						        <td>" . $tName . "</td>
                                <td>" . $t->address . "</td>
                                <td>
                                    <input type=\"radio\" name=\"trustee[id]\" value=\"" . $t->id . "\">
                                </td>
					        </tr>";
                }

                if (!isset($data)){
                    $data = "<tr><td colspan='3'>This Client Currently has no Trustees</td></tr>";
                }

                echo $data;
                break;
        }
    }
} elseif (isset($_POST['do']) && $_POST['do'] == "new") {
    $json = [
        'error' => false,
        'feedback' => false,
        'id' => false
    ];

    parse_str ($_POST['data'], $output);

    $trustee = new Trustee();
    $trustee->client($output['trustee']['client']);
    $trustee->company($output['trustee']["company"] ?? '');
    $trustee->title($output['trustee']['title']);
    $trustee->firstname($output['trustee']['firstname']);
    $trustee->surname($output['trustee']['surname']);
    $trustee->dob($output['trustee']['dob']['Y']."-".$output['trustee']['dob']['M']."-".$output['trustee']['dob']['D']);
    $trustee->nino($output['trustee']['nino']);
    $trustee->nationality($output['trustee']['nationality']);
    $trustee->residency($output['trustee']['residency']);
    $trustee->address->line1($output['trustee']['address']['line1']);
    $trustee->address->line2($output['trustee']['address']['line2']);
    $trustee->address->line3($output['trustee']['address']['line3']);
    $trustee->address->postcode($output['trustee']['address']['postcode']);


    if ($trustee->save()) {
        $json['feedback'] = "Trustee Saved.";
        $json['id'] = $trustee->id();
    } else {
        $json['data'] = true;
        $json['feedback'] = "Trustee failed to save, try again or contact IT.";
    }

    echo json_encode($json);
} elseif (isset($_GET['do']) && $_GET['do'] == "view_mandates") {

    $mandates = Trustee::mandateCheck($_GET['policyID']);

    echo Trustee::get_template("mandates.html", ["mandates" => $mandates]);
} elseif (isset($_GET['do']) && $_GET['do'] == "countMandates") {
    $id = $_GET['trustee'];

    $s = new Search(new MandateTrustee());
    $s->eq("trustee", $id);

    echo $s->count();
}
