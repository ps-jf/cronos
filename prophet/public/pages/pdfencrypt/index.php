<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

if (isset($_GET['do'])){
    switch($_GET['do']){
        case "encrypt":

            $file = $_FILES["pdf"];
            $password = $_POST['password'];

            // Set paths to the python script to run
            $python = '/usr/bin/python3.5';
            $py_script = '/usr/local/bin/pdf_encrypt.py';

            if (!file_exists($python)) {
                die("The python executable '$python' does not exist!");
            }
            if (!is_executable($python)) {
                die("The python executable '$python' is not executable!");
            }
            if (!file_exists($py_script)) {
                die("The python script file '$py_script' does not exist!");
            }
            if (!is_executable($py_script)) {
                die("The python script '$py_script' is not executable!");
            }

            // Build up the command for the cmd line
            $cmd = "$python $py_script";
            exec($cmd . " " . $file['tmp_name'] . " " . $password . " 2>&1", $output, $return_code);

            //	If return code returns 0, no problems have arisen
            if ($return_code == 0) {
                $pdf = end($output);

                header("Content-type:application/pdf");
                header("Content-Disposition:attachment;filename=" . $file['name']);
                readfile($pdf);

                unlink($pdf);
            } else {
                $errormessage = "";
                // Loop through the return array and create a string of errors.
                foreach ($output as $r) {
                    $errormessage .= $r . " ";
                }

                // If array returned with data in it, then json return the data
                die($errormessage);
            }

            break;
    }
} else {
    echo new Template("pdfencrypt/index.html");
}