/*
 * 
 *
 */

var moz = ( document.getElementById && !document.all ) ? true : false;

function json_decode( what ){ return eval( "(" + what + ")" ); } // prefer this name

function date_element ( id ) {

    /*
     * Initiate a date input widget, by implementing some simple 
     * validation checks ( range, type ), and ensuring a consistent
     * format by specifying 
     *
     */

    var day, month, year, hidden;

    // Stores an ISO 8601 representation of the collected input values
    hidden = $("input.date:hidden#"+id+":last")[0];

    // Define permitted values for each input element

    day = $("input.date.day#"+id+"_day:last")[0];
    day.lastValue = $(day).val();
    day.max = 31;
    day.min = 1;

    month = $("input.date.month#"+id+"_month:last")[0];
    month.lastValue = $(month).val();
    month.max = 12;
    month.min = 1;

    year = $("input.date.year#"+id+"_year:last")[0];
    year.lastValue = $(year).val();
    year.max = new Date( ).getFullYear( ) + 125;
    year.min = new Date( ).getFullYear( ) - 125;

    $( [day,month,year] ).change(
        function(){ 

            var input = this;
            var value = parseFloat(input.value);


            // if input value is valid number within the input's permitted range
            if( !isNaN(value) && value >= input.min && value <= input.max ){
                input.lastValue = value;
            } else {
                value = input.lastValue;
            }

            // for consistency, prepend a leading zero to any single digit number
            if ( value < 10 ) {
                value = "0" + value;
            }
            
            input.value = value;

            $( hidden ).val( [ year.value, month.value, day.value ].join("-") );
        }
    );

}

function clicked( event ) {
    
    // Which button was pressed?

    var buttons = { 'left':false, 'right':false };

    if( event.button === 0 ){
        buttons.left = true;
    } else {
        buttons.right = true;
    }

    return buttons;
}

function scrolled( ) {
    // How far has user scrolled?
    return ( moz ) ? { 'x': window.pageXOffset, 'y': window.pageYOffset } : { 'x': document.body.scrollLeft, 'y': document.body.scrollTop };
}

function random_int( limit, start ) {

    var n = Math.floor( Math.random( ) * ( ++limit ) );

    if( start ) {
        n += start;
    }

    return n;
}

function mapForm( form ) { 

    var fields = { _self:form };

    fields.get = function( name ) { 

        if( !fields[ name ] ) {
                
            var f = $( form ).find( "#" + name )[ 0 ];

            if( f  ) {
                fields[ name ] = f;
            } else {
                return false;
            }
        }

        return fields[ name ];
    };

    //  Create an associative array of form elements
    $( form ).find( "input, select, button, textarea, checkbox" ).each( function() { fields[ this.id ] = this; } );

    return fields;
}

function random_string( n )
{
    var LETTER = 1;
    var NUMBER = 0;
    var ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    var string = "";

    /* Create an alphanumeric string n characters long */

    // generate n characters
    var c;
    for( var i = 0; i < n; i++ ) {

        // letter or number?
        if( random_int( 1 ) == LETTER ) {

            c = ALPHABET.charAt( random_int( 25 ) );

            // lower or uppercase?
            if( random_int( 1 ) == 1 ) {
                c = c.toUpperCase( );
            }
        }
        else{
            c = random_int( 9 ).toString( ); // single digits only
        }
        string += c;
    }

    return string;
}


function Proc( )
{
    /* a self-managed ajax process */

    var self = this;
    
    this.kill = function( )
    {
        self.phys.innerHTML = ""; // workaround for jquery/java clash when applet on page
        $( self.phys ).remove( );     
    };

    /** Create process container **/
    self.phys = document.createElement("DIV");
    $( self.phys ).addClass( "_process" );
    $( self.phys ).bind( 'init_kill',function( ){ self.kill( ); } );


    /** Generate a unique element ID **/
    do
    {
        var id = random_string( 15 );

        if( $( "#"+id ).length < 1 )
        {
            self.phys.id = id;
            break;
        }    

    }
    while( 1 ); // ... loop until unique

    
    /** Append to process container **/
    $( "div#_proc" ).append( self.phys );
}


function get_popup( id ) { return $( "table.popup#" + id )[ 0 ]; }


