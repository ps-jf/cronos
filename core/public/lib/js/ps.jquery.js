$(function( ){

	$.fn.extend({

        "mySerialize": function( ){
            
            /* my custom serialization method for including unorthodox form elements */

            if( !this.elements ){
                this.elements = $(":input",this);
            }

            var formData = $(":input",this).serializeArray();

            var buttonData = $("button",this).map(function( i, elem ){

                if( !elem.id ){
                    return null;
                }

		        return { name: elem.name, value: $(elem).val().replace( /r?\n/g, "\r\n" ) };
	        }).get();
            
            formData = formData.concat(buttonData);
            return jQuery.param(formData);
        },

		"popup_config": function( settings ) {

            /* Configure "this"'s parent popup */

            // get holding popup
            var my_popup = $( this ).closest( ".popup" );

            if( !my_popup.length ) {
                var title = settings['title'];

                if( "helpGuide" in settings){
                    title = "<button class='btn btn-link text-white help_guide' data-help='" + settings['helpGuide'] + "'><i class='far fa-question-circle'></i></button>" + title;
                }
                $(".nav-bc").html(title);

                return false;
            }

            my_popup = $(my_popup)[0];
          
            // work through supplied settings
            var x;
            for ( x in settings ) {

                switch( x ) {

                    case "title":
                    my_popup.setTitle( settings[x] );
                    break;

                    case "center":
                      $(my_popup).center( );
                    break;

					// Custom funcion to allow custom class to be assigned to popup
					case "popupClass":
						$(my_popup).addClass( settings[x] );
					break;

                    case "triggers":

                    for( event in settings.triggers ){
                        $( my_popup ).bind( event, settings.triggers[event] );
                    }

                    break;

                    case "canMaximize":
                        $("button.maximize", my_popup).show();
                        break;

                    case "helpGuide":
                        $("button.help_guide", my_popup).show().data("help", settings[x]);
                        break;

                }
            }

            return my_popup;
		},

		'export': function() {

			var src = $(this).clone();

			$( "span.truncated", src ).each(
				function(){
					$( this ).html( $( this ).attr("title") );
				}
			);
			
			var n = new ProphetNotification({
				type: "info",
				text: " Exporting...",
				timeout: 2000
			});

			var f = $("<form action=\"/core/lib/tools/export.php\" method=\"POST\"/>")
				.appendTo( n );

			$("<input id=\"src\" name=\"src\" type=\"hidden\" />")
				.val( $(src).html() )
				.appendTo( f );

			f[0].submit();
		
		},

		'exportall': function( $x ) {

            /* - Used to export everything regardless of table contents at time of export.
               - Use $(function(){ $('#commission').export('/pages/commission/?do=list&filter[<?=$property?>]=<?=$_GET['id']?>'); } on commission/plugin.html
            */
		
			$("<form id=\'resform\' name=\'resform\' action='"+$x+"'></form>").appendTo('body');
				
			$.post($x, $("#resform").serialize(),
			
				function(data){
				 
					var src = data;
            
					var n = new ProphetNotification({
						type: "info",
						text: " Exporting...",
						timeout: 2000
					});

					var f = $("<form action=\"/core/lib/tools/export.php\" method=\"POST\"/>")
						.appendTo( n );

					$("<input id=\"src\" name=\"src\" type=\"hidden\" />")
						.val( src )
						.appendTo( f );

					f[0].submit();

				});  	
		
        },

		'print': function( ) {

			var self = this[0];
            var el;
            
			if( self.tagName == "TABLE" ) {
                
				// Create a duplicate table
				el = document.createElement( "TABLE" );
                
				$( [ "thead","tbody:last","tfoot" ] ).each( function( ){ 
					$( el ).append( 
						$( $( self ).find( this+"" )[ 0 ] ).clone( ) 
					); 
				}); 
                
				// Remove unprintable cells
				var i = 0;
				$( el ).find( "thead th" ).each(
					function( ) {
                        
						if( $( this ).is( ".no-print" ) ){
                            
							$( el ).find( "tbody > tr > td:eq("+i+")" ).remove( );
							$( this ).remove( );
                            
						} else {
							++i;
                        }
					});
				
			}

			var win = window.open( "core/lib/tools/print_table.php", "<?=random_string(10)?>", "width=500,height=500,scrollbars=yes" );

			win.init = function( ) {
				if( win.loaded ){
					$( win.document.getElementById("content") ).html( el );
                } else {
					setTimeout( win.init, 100 );									 
				}
            };

			win.init( );

			// untruncate any profile links
			$( el ).find( "span" ).each(
				function( )	{
					if( $( this ).attr( "title" ).length > 0 ){
						$( this ).text( $( this ).attr( "title" ) );
                    }
				}
			);

		},

        'onchange': function( callback ) {
            /* Forms respond to onchange events whenever it's child elements are changed */

            var self = $(this)[0];

            if( self.tagName == "FORM" )
            {
                // attach callback function to form
                $(this).bind( "onchange", callback );

                // trigger event via behaviours particular to element

                // keyup for textfields
                $(this).find("input[type=text]").keyup( function(){ setTimeout( callback, 1500 ); } );

                // keyup for textareas
                $(this).find("textarea").keyup( function(){ setTimeout( callback, 1500 ); } );

                // option choice for dropdowns
                $(this).find("select").change(function(){ $(self).trigger('onchange'); });

                // click of checkboxes / radio buttons
                $(this).find("input[type=checkbox],input[type=radio]").click(function(){ $( self ).trigger('onchange'); });

            }

        },

        'send': function( callback ) {

            var self = this[0];

            if( self.tagName == "FORM" ) {

                /* Submit form via ajax */

                // check required attributes
                if( !self.method || !self.action ) {
                    return false;
                }

                // write to target attr or perform custom callback?
                var c = ( callback !== undefined )
                    ? callback
                    : function( r ) {
                        var t = self.target;

                        if ( !t ){
                            return false;
                        }

                        $( t ).html( r );
                    };


                // interrupt ongoing XHRs
                if ( self.xhr !== undefined && self.xhr.readyState != 4 ) {
                    self.xhr.abort( );
                }

                // send request + execute callback
                self.xhr = $.ajax({
                    beforeSend: function(){
                        $(":submit",self).attr("disabled",true);
                    },
                    type: $( self ).attr("method"),
                    url: $( self ).attr("action"),
                    data: $( self ).mySerialize( ),
                    success: c,
                    complete: function(){
                        $(":submit",self).attr("disabled",false);
                    }
                });
            }

        },

        'center': function( ){

            /*	Align 'this' in the center of the screen */

            var width;
            var height;

            height = width = x = y = 0;

            if (moz) {
                height = window.innerHeight;
                width = window.innerWidth;
            }
            else {
                height = document.body.clientHeight;
                width = document.body.clientWidth;
            }

            y = (  (height / 2) - ( $(this).height()  / 2 ) ) + scrolled().y;

            if( y < scrolled().y ){ // prevent popup from disappearing centering off the top of the screen
                y = scrolled().y + 10;
            }

            x = (  (width / 2) - ( $(this).width() / 2 ) ) + scrolled().x;

            $( this ).css( 'top' , y + "px" );
            $( this ).css( 'left', x + "px" );

        },
	
        'flash': function( text, timeout ) {
            /* temporarily display text in $(this) */

            var self = $(this);

            $(self).hide(); // ensure hidden to start
            $(self).html(text); // populate with text

            // give default timeout if necessary
            if( timeout ) {
                timeout = 1500;
            }

            // flash message
            $(self).fadeIn('fast',
                function( )	{
                    setTimeout( "$(\""+self.selector+"\").fadeOut('slow');", parseInt(timeout,10) );
                }
            );

        },

        'get_popup' : function( ){ return $( this ).closest( "table.popup" )[0]; },

        'scrollable' : function( callback ){

            var st = new ScrollableTbody( this[ 0 ] );
            if( callback != null ){
                callback( );
            }

        },

        "style" : function( e ){

            $( this ).children( e ).removeClass("odd even");
            $( this ).children( e + ":even").addClass("even");
            $( this ).children( e + ":odd").addClass("odd");

        },

        "profile_refresh": function(object, object_id, popup = false){
            var url = "/common/"+object+"/";

            $.ajax({
                url: url,
                data: {
                    get: "profile",
                    id: object_id,
                },
                dataType: "html",
                success: function(r){
                    if (popup){
                        $(".content", popup).html(r);
                    } else {
                        $("#content_window").html(r);
                    }
                }
            });
        }
	});

    $("button.bool").live("click", function( ){
            var x = parseInt($(this).val());

            (x == 1) ? $(this).val(0) : $(this).val(1);

            $(this).trigger("change");
            $(this).closest("form").trigger("change");

            return false;
        }
    );
    
    /*
    purely experimental/might work out horribly,double clicking year input box
    will automatically insert todays date into the fields
    */
    $("input.date.year").live("dblclick", function( ){
        if($(this).val() == "" && !$(this).attr("readonly")){
            var d = new Date();
            var yr = this;
            var mnth = $(this).prev();
            var day = $(mnth).prev();

            var rmnth = d.getMonth()+1;
            var rday = d.getDate();

            if(rmnth < 10){
                rmnth = '0' + rmnth;
		    }

            if(rday < 10){
                rday = '0' + rday;
            }

            $(yr).val(d.getFullYear());
            $(mnth).val(rmnth);
            $(day).val(rday);

        }
        $(this).change(); //trigger save notification span
        return false;
    });

    $("input.datetime.year").live("dblclick", function( ){
        if($(this).val() == "" && !$(this).attr("readonly")){
            var d = new Date();
            var yr = this;
            var mnth = $(this).prev();
            var day = $(mnth).prev();
            var hr = $(this).next();
            var min = $(hr).next();

            var rmnth = d.getMonth()+1;
            var rday = d.getDate();

            var rHour = d.getHours();
            var rMins = d.getMinutes();

            if(rmnth < 10){
                rmnth = '0' + rmnth;
            }

            if(rday < 10){
                rday = '0' + rday;
            }

            if (rHour < 10){
                rHour = '0' + rHour;
            }

            if (rMins < 10){
                rMins = '0' + rMins;
            }

            $(yr).val(d.getFullYear());
            $(mnth).val(rmnth);
            $(day).val(rday);
            $(hr).val(rHour);
            $(min).val(rMins);

        }
        $(this).change(); //trigger save notification span
        return false;
    });

    /*
    purely experimental/might work out horribly,double clicking day input box
    will automatically clear date fields
    */
    $("input.date.day").live("dblclick", function( ){
        if($(this).val() != "" && !$(this).attr("readonly")){
            var day = this;
            var mnth = $(day).next();
            var yr = $(mnth).next();

            $(day).val("");
            $(mnth).val("");
            $(yr).val("");
        }
        return false;
    });



    $(".profile").live("click",function(event){

        // we want to disable the popup functionality for the staff training context menu
        var string = String(this);
        if (string.indexOf("StaffTraining") >= 0) {
            return false;
        }

        if( clicked(event).left ){

            /*
              Open a popup (overlay) with the respective DatabaseObject's profile.
              If this profile has already been opened then bring it to the foreground
              rather than creating a new one.
              */

            var url = this.href + "&get=profile";

            var openPopup;
            $( ".popup" ).each(
                function( ){
                    if( this.url == url ){
                        openPopup = this;
                    }
                }
            );

            if( openPopup !== undefined ){

                if( $(openPopup.content).is(":hidden") ){
                    openPopup.buttons.mini.click();
                } else {
                    $(openPopup).click();
                }


            } else {
                var p = new popup({'url':url});
                p.url = url;
            }

            return false;
        }
    });


	// Provide context menus for Clients, Policies etc.
	$(".profile").live('contextmenu', function( event ){
            
        $.get(this.href, { 'get': "context", 'front_end' : +window.front_end, 'ppp_predicted' : +window.ppp_predicted},
            function( r ) {
                r = json_decode( r );
                if (!r) {
                    return true;
                }
                Context( r, event );
            }
        );

        return false;
    });

	// By default, form submit returns false
	$( "form" ).live( 
        "submit", function( ){ return false; } 
    );

	// Automate overlay-initiating button events
	$( "button[href],:button[href]" ).live( 'click', function() {
        var href = $(this).attr("href");
        var title = $(this).attr("title");

        if( href !== undefined ) {
            new popup({ 'title': title, 'url': href });
        }
    });

	$(".note_subject_search_btn").live("click", function(){
	    var input = $(this).next(".note_subject_search");

	    $(this).remove();
	    input.removeClass("collapse");
	    input.focus();
    });

	$(".note_subject_search").live("keyup", function(){
	    var q = $(this).val();
        var note = ".modal-note-main";
        var subject = ".note-subject-modal";
        var text = ".note-body-modal";

        $(note).each(function(){
            if ($(subject, this).text().toLowerCase().indexOf(q.toLowerCase()) < 0 && $(text, this).text().toLowerCase().indexOf(q.toLowerCase()) < 0){
                $(this).closest("ul").hide();
            } else {
                $(this).closest("ul").show();
            }
        });

    });

	$("button.add-note").live('click', function() {

        var parent_level = false;

	    if ($(this).text() == "Add Note" || $(this).text() == "NB Additional Note" || $(this).hasClass('std-note')) {
            // original note behaviour

            var ul = $(this).closest("ul.notes");
            var li = $(this).closest("li");
            var div = $(li).children("#bottom");

            if (li.length < 1) {
                parent_level = true;
                ul = $(this).parent().parent().parent().children("ul.notes");
                li = document.createElement("LI");
                div = document.createElement("DIV");
                $(li).prepend(div);
                $(ul).prepend(li);
            }

            var note_type = $(this).attr("note_type");
            var object = $(ul).attr("object");
            var object_id = $(ul).attr("object_id");

            var subject_ta = document.createElement("input");
            $(subject_ta).attr("class", "note_subject_ta");
            $(subject_ta).attr("placeholder", "Subject");
            $(subject_ta).attr("maxlength", "200");
            $(subject_ta).css("text-overflow", "ellipsis");
            $(div).append(subject_ta);
            $(subject_ta).after("<span class='text-danger'>*</span>");

            var ta = document.createElement("textarea");
            $(ta).attr("class", "note_ta");
            $(ta).attr("maxlength", "20000");
            $(div).append(ta);

            var ta_count = document.createElement("p");
            $(ta_count).attr("class", "note_count");
            $(ta_count).html("Remaining characters: " + 20000);
            $(div).append(ta_count);

            var sticky_bt = document.createElement("button");
            $(sticky_bt).val(this.value);
            $(sticky_bt).addClass("sticky");
            $(sticky_bt).attr("title", "Make note sticky");

            var sticky = 0;

            $(sticky_bt).click(function () {
                sticky = 1;
                $("sticky_bt").remove();
                $(this).removeClass('sticky');
                $(this).after("&nbsp; <b> Now a sticky note </b>");
                $(this).hide();
            });

            var parent_id = $(this).attr("value");

            $(".note_ta").css("display", "block");
        } else {
	        // modal note
            var ul = $(this).closest('#notes_box_modal').find('ul.notes-modal');

            var li = $(this).closest("li");
            var div = $(li).children("#bottom");

            if (ul.length < 1) {
                ul = $(this).closest("#notes_box").find('ul.notes');
            }

            if (li.length < 1) {
                parent_level = true;
                li = document.createElement("LI");
                div = document.createElement("DIV");
                $(li).prepend(div);
                $(ul).prepend(li);
            }

            var note_type = $(this).attr("note_type");
            var object = $(ul).attr("object");
            var object_id = $(ul).attr("object_id");

            var subject_ta = document.createElement("input");
            $(subject_ta).attr("class", "note_subject_ta");
            $(subject_ta).attr("placeholder", "Subject");
            $(subject_ta).attr("maxlength", "200");
            $(subject_ta).css("text-overflow", "ellipsis");
            $(div).append(subject_ta);
            $(subject_ta).after("<span class='text-danger'>*</span>");

            var ta = document.createElement("textarea");
            $(ta).attr("class", "note_ta");
            $(ta).attr("maxlength", "20000");
            $(div).append(ta);

            var ta_count = document.createElement("p");
            $(ta_count).attr("class", "note_count");
            $(ta_count).html("Remaining characters: " + 20000);
            $(div).append(ta_count);

            var sticky_bt = document.createElement("button");
            $(sticky_bt).val(this.value);
            $(sticky_bt).addClass("stick-note-modal");
            $(sticky_bt).attr("title", "Make note sticky");

            var sticky = 0;

            $(sticky_bt).click(function () {
                sticky = 1;
                $("sticky_bt").remove();
                $(this).removeClass('stick-note-modal');
                $(this).after("&nbsp; <b> Now a sticky note </b>");
                $(this).hide();
            });

            var parent_id = $(this).attr("value");

            $(".note_ta").css("display", "block");

        }

	    function saveNote(btn){
	        var txt = $(ta).val();

	        if (txt.length > 20 && !(/^\d+$/.test(txt))) {
	            $(".note_warning", ta_count).remove();

                if (btn.text() == "Add Note") {
                    // original note behaviour
                    var modal = false;
                } else {
                    // modal note
                    var modal = true;
                }

                $.post(
                    '/pages/tools/notes/?'+
                    'do=new&'+
                    'object='+ object + '&' +
                    'sticky='+ sticky + '&' +
                    'object_id=' + object_id + '&' +
                    'note_type=' + note_type + '&' +
                    'parent=' + parent_id,
                    { 'text': $( ta ).val( ), 'subject': $(subject_ta).val() },
                    function( r ) {
                        var id = json_decode( r );

                        if (sticky == 1) {
                            $.get('/pages/tools/notes/', { 'note': id, 'do': "get", 'modal' : modal},
                                function(r) {
                                    if (parent_level) {
                                        $(li).remove();
                                        $(ul).prepend(json_decode(r));
                                    } else {
                                        $(div).html(json_decode(r));
                                    }
                                }
                            );
                        } else if (id !== false) {

                            $.get( '/pages/tools/notes/',  { 'note': id, 'do': "get",'modal' : modal }, function( r ){
                                if (parent_level) {
                                    $(li).remove();
                                    $(ul).prepend(json_decode(r));
                                } else {
                                    $(div).closest('#bottom').html(json_decode(r));
                                }
                            });
                        }
                    }
                );
            } else {
	            $(ta_count).append("<span class='note_warning text-danger'><br>Notes must be a minimum of 20 characters long and contain at least 1 letter! Please do not use helpdesk ID's!</span>");
            }
        }

        $(subject_ta,li).focusout(function() {

            if ($(ta,li).val().length) {
                if ($(subject_ta, li).val().length) {
                    saveNote($(this));
                }
            }
        });

        $(ta,li).focusout(function() {

            if ($(ta,li).val() != 0) {

                if ($(subject_ta, li).val().length){
                    saveNote($(this));
                } else {
                    alert("Please provide a subject before saving the note.");
                }
            }
        });
    });


    //Wait until leaving the edit box before adding to database
    $( ".note-body, .note-subject" ).live( 'focusout' , function() {

        var noteID = this.id;
        var subject = $("#" + noteID + "  .note-subject[data-id='" + noteID + "']").text();
        var body = $("#" + noteID + "  .note-body[data-id='" + noteID + "']").text();

		$.ajax({
			url: '/pages/tools/notes/?do=edit',
			type: "GET",
			data: {
				type: "json",
				id: noteID,
				subject: subject,
				text: body
			},
			dataType: "json",
			success: function (r) {

			}
		});
    });

	//show users remaining available characters for notes, limited to 20000 in DB
	$(".note_ta").live("keyup", function(){
		var max_char = 20000;
		var length = $(".note_ta").val().length;
		var reminaing_chars = max_char - length;
		$(".note_count").html("Remaining characters: "+reminaing_chars);

	});

    $("button.alter-note").live('click', function (event) {
        // display the hidden list types
        $(this).next("ul#note_types").slideDown("fast", function () {
            // another click is fired
            $("body").click(function () {
                $("ul#note_types").fadeOut("fast")
            });
        });
    });

    //the various different note types
    $("ul#note_types li").live('click', function () {
            var add = $(this).parent().siblings("button.add-note");
            var nt = $(this).attr("id");
            $(add).attr("note_type", nt);
            $(add).click();
        }
    );

    $("ul.notes button.lock-note").live('click', function () {
        var li = $(this).closest("li");
        var ul = $(li).closest("ul.notes");

        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "lock"}, function (r) {
            var locked = json_decode(r);

            if (locked) {
                $("div#<?=_LINK?> div#notes_box")
                    .load('/pages/tools/notes/?do=list&object=' + object + '&object_id=' + object_id);
            }
        });
    });

    $("ul.notes button.unlock-note").live('click', function () {
        var li = $(this).closest("li");
        var ul = $(li).closest("ul.notes");

        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "unlock"}, function (r) {
            var unlocked = json_decode(r);

            if (unlocked) {
                $("div#<?=_LINK?> div#notes_box")
                    .load('/pages/tools/notes/?do=list_modal&object=' + object + '&object_id=' + object_id);
            }
        });
    });

    $("ul.notes button.delete-note").live('click', function () {
        if (confirm("Delete this note?")){
            var li = $(this).closest("li");

            $.get('/pages/tools/notes/', {'note': this.value, 'do': "delete"}, function (r) {
                var deleted = json_decode(r);
                if (deleted) {
                    $(li).fadeOut();
                }
            });
        }
    });

    $("ul.notes button.unstick-note").live('click', function () {
        var li = $(this).closest("li");
        var ul = $(li).closest("ul.notes");

        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "unstick"}, function (r) {
            var unstuck = json_decode(r);

            if (unstuck) {
                $("div#<?=_LINK?> div#notes_box")
                    .load('/pages/tools/notes/?do=list&object=' + object + '&object_id=' + object_id);
            }
        });
    });

    $("ul.notes button.stick-note").live('click', function () {
        var li = $(this).closest("li");
        var ul = $(li).closest("ul.notes");

        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "stick"}, function (r) {
            var stuck = json_decode(r);

            if (stuck) {
                $("div#<?=_LINK?> div#notes_box")
                    .load('/pages/tools/notes/?do=list&object=' + object + '&object_id=' + object_id);
            }
        });
    });

    // modal notes behaviour below

    // Wait until leaving the edit box before adding to database (Notes Modal)
    $(".note-body-modal, .note-subject-modal").live( 'focusout' , function() {
        var noteID = $(this).data('id');
        var subject = $("li#" + noteID + " .note-subject-modal[data-id='" + noteID + "']").text();
        var body = $("li#" + noteID + " .note-body-modal[data-id='" + noteID + "']").text();
        $.ajax({
            url: '/pages/tools/notes/?do=edit',
            type: "GET",
            data: {
                type: "json",
                id: noteID,
                subject: subject,
                text: body
            },
            dataType: "json",
            success: function (r) {

            }
        });
    });

    $("ul.notes-modal button.lock-note-modal").live('click', function () {
        var li = $(this).closest("li");

        var ul = $(this).closest('#notes_box_modal').find('ul.notes-modal');
        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "lock"}, function (r) {
            var locked = json_decode(r);

            if (locked) {
                $("div#<?=_LINK?> div#notes_box_modal")
                    .load('/pages/tools/notes/?do=list_modal&object=' + object + '&object_id=' + object_id);
            }
        });
    });

    $("ul.notes-modal button.unlock-note-modal").live('click', function () {
        var li = $(this).closest("li");
        var ul = $(this).closest('#notes_box_modal').find('ul.notes-modal');

        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "unlock"}, function (r) {
            var locked = json_decode(r);

            if (locked) {
                $("div#<?=_LINK?> div#notes_box_modal")
                    .load('/pages/tools/notes/?do=list_modal&object=' + object + '&object_id=' + object_id);
            }
        });
    });

    $("ul.notes-modal button.delete-note").live('click', function () {
        if (confirm("Delete this note?")){
            var li = $(this).closest("li");
            $.get('/pages/tools/notes/', {'note': this.value, 'do': "delete"}, function (r) {
                var deleted = json_decode(r);
                if (deleted) {
                    $(li).fadeOut();
                }
            });
        }
    });

    $("ul.notes-modal button.unstick-note-modal").live('click', function () {
        var li = $(this).closest("li");
        var ul = $(this).closest('#notes_box_modal').find('ul.notes-modal');

        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "unstick"}, function (r) {
            var unstuck = json_decode(r);

            if (unstuck) {
                $("div#<?=_LINK?> div#notes_box_modal")
                    .load('/pages/tools/notes/?do=list_modal&object=' + object + '&object_id=' + object_id);
            }
        });
    });

    $("ul.notes-modal button.stick-note-modal").live('click', function () {
        var li = $(this).closest("li");
        var ul = $(this).closest('#notes_box_modal').find('ul.notes-modal');

        var object = $(ul).attr("object");
        var object_id = $(ul).attr("object_id");

        $.get('/pages/tools/notes/', {'note': this.value, 'do': "stick"}, function (r) {
            var stuck = json_decode(r);

            if (stuck) {
                $("div#<?=_LINK?> div#notes_box_modal")
                    .load('/pages/tools/notes/?do=list_modal&object=' + object + '&object_id=' + object_id);
            }
        });
    });


	$( "#content_window" ).ajaxComplete(
		function( r,e ){ 

            tabs( );

            // automatically convert tbody's within scrollable tables to... well scrollable
            $( "table.scrollable:not(.has_onshow)" ).each(
                function( ){
                    $( this ).scrollable( );
                });

            // init suggestion boxes
            $( "div.suggest:not(.init)" ).each(
                function(){
                    SuggestionBox($(this));
                });
            

            $("input[data-pattern]").trigger("blur");
		}
	);

    $("input[data-pattern]").live(
        "focus",
        function( ){
            $(this).removeClass("invalid");
        }
    );

    $("input[data-pattern]").live(
        "load change blur",
        function( ){

            if( !$(this).val().length ){
                return false;
            }

            try { 
                
                var pattern = $(this).attr("data-pattern");
                var mods = $(this).attr("data-pattern_mods");
                var valid = RegExp( pattern, mods ).exec( $(this).val() );
                
                if( !valid ){
                    $(this).addClass("invalid");
                }

            } catch( e ) {
                return false;
            }
        });


    //// Uppercase postcodes
    $( ":text.address.postcode" ).live(
        "change",
        function( ){
            $(this).val( $(this).val().toUpperCase() );
        }
    );
    
    //// Set postcode field's bg as white following errors / unrecognised postcodes etc.
    $( ":text.address.postcode" ).live( "focus", function(){ $(this).css("background","#fff"); } );

    //// Postcode-lookup service 
    /*$( ":button.address_search" ).live(
        'click',
        function( event ){

            var link = $(this).val();
            var postcode = $(".postcode."+link).val().replace(" ","+");
            var url = "/core/lib/modules/address_finder.php";
            var status = $("span.status."+link);

            $( status ).text("");

            var p = new popup;

            $.get(
                url,
                { 'postcode': postcode },
                function ( r ) {
                    try {

                        r = json_decode( r );

                        if( r.error.length ) {
                            $( p.content ).text( r.error );
                            $( "input.address.postcode" )
                                .addClass("bad_news");
                            return false;
                        }


                        var select = $("<select><option>-- Please select --</option></select>" );

                        for( key in r.options ){
                            $( select ).append( $("<option value='"+key+"'>"+r.options[key]+"</option>" ) );
                        }

                        $( select ).change(
                            function( ){

                                var lines = $( ".address."+link+":not(.postcode)" );
                                $( lines ).val(""); // reset current address
                                $( lines ).change(); // trigger save span

                                $.get(
                                    url,
                                    { 'address': $(this).val() },
                                    function( r ){
                                        try {
                                            r = json_decode( r );
                                            for( var i=1; i<=r.length; i++ ){
                                                $( lines )
                                                    .filter( ".line"+i )
                                                    .val( r[(i-1)] );
                                            }
                                        } catch( e ){
                                            throw(e);
                                        }
                                        p.close( );
                                    }
                                );

                            }
                        );

                        $( p.content ).html( select );

                    } catch( e ){
                        handleSystemError( e );
                        $( "input.address.postcode" )
                            .addClass( "bad_news" );
                    }
                });

            return false; // prevent potential form submission
        });*/

    // hide suggestion div by default
    $(".autocomplete-suggestions").hide();

    // time out function to simulate when user has stopped typing
    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

	// address lookup
	$('body').on('keyup', '.address', function( ){

        var search_term = $(this).val();
        var url = "/core/lib/modules/address_finder_new.php";

        // when user has enter 3+ characters
        if (search_term.length >= 3) {
            // add slight delay so request is sent when user stops typing rather than every key press
            delay(function() {

                $.get(
                    url,
                    { 'search_term': search_term },
                    function ( r ) {
                        try {
                            if ( r.length ) {
                                r = json_decode( r );

                                $(".autocomplete-suggestions").show();
                                $( ".autocomplete-suggestions" ).html( r );

                                $(document).on("click", function(){
                                    $(".autocomplete-suggestions").hide();
                                });
                                return false;
                            } else {
                                $(".autocomplete-suggestions").hide();
                            }

                        } catch( e ){
                            handleSystemError( e );
                            $("input.address.postcode").addClass("bad_news");
                        }
                    });

                return false; // prevent potential form submission

            }, 500 );
        }
	});



    // address retrieval from suggestion
    $("body").on("click", ".autocomplete-suggestion", function(){

        var id = this.id;
        var url = "/core/lib/modules/address_finder_new.php";

        $(".autocomplete-suggestions").hide();

        if($(this).data("type") === "Address"){
            // get full address from suggestion, Note these requests
            $.get(
                url,
                { 'id': id },
                function ( r ) {
                    try {

                        // clear fields before filling with new address
                        $(".line1").val("");
                        $(".line2").val("");
                        $(".line3").val("");
                        $(".postcode").val("");

                        r = json_decode(r);

                        var length = Object.keys(r).length;

                        // fill in address fields with relevant data
                        if (length >= 2) {
                            $(".line1").val(r['line1']);
                        }
                        if (length >= 3) {
                            $(".line2").val(r['line2']);
                        }
                        if (length >= 4) {
                            // If address contains 3 lines or less, use object value else use concatenated string
                            // value from address_finder_new.php to populate address->line3
                            $(".line3").val(r['line3']);

                        }
                        if (r['postcode'].length) {
                            $(".postcode").val(r['postcode']);
                        }

                    } catch( e ){
                        handleSystemError( e );
                        $("input.address.postcode").addClass("bad_news");
                    }
                }
            );
        } else {
            var search_term = $(this).data("search_term");
            var container = $(this).attr("id");

            $.get(
                url,
                {
                    'search_term': search_term,
                    'container': container,
                },
                function ( r ) {
                    try {
                        if ( r.length ) {
                            r = json_decode( r );

                            $(".autocomplete-suggestions").show();
                            $( ".autocomplete-suggestions" ).html( r );

                            $(document).on("click", function(){
                                $(".autocomplete-suggestions").hide();
                            });
                            return false;
                        } else {
                            $(".autocomplete-suggestions").hide();
                        }

                    } catch( e ){
                        handleSystemError( e );
                        $("input.address.postcode").addClass("bad_news");
                    }
                }
            );
        }
    });

    $("body").on("focus", "input", function() {

        var check_flag = false;

        var id = this.id;

        // email validation
        // check if we are on an email field
        if (id.indexOf("email") != -1) {

            var email_input = $(this);

            $(email_input).on('focusout', function( ){

                var email = $(email_input).val();
                var url = "/core/lib/modules/email_validator.php";

                // when user has entered 3+ characters
                if (email.length >= 3) {

                    delay(function(){
                        if(check_flag == false){
                            $.get(
                                url,
                                {'email': email},
                                function (r) {
                                    try {
                                        if(r.length){
                                            r = json_decode(r);

                                            if (r == "valid") {
                                                $(email_input).css("border-color", "#6ec30c");
                                            } else if (r == "invalid") {
                                                $(email_input).css("border-color", "#df0000");
                                            } else {
                                                $(email_input).css("border-color", "#d0d0bc");
                                            }
                                        }

                                    } catch (e) {
                                        handleSystemError(e);
                                    }
                                });
                            check_flag = true;
                        }


                        return false; // prevent potential form submission

                    }, 500 );
                }

            });
        }

        //mobile validation
        // check if we are on an phone number field
        if ( (id.indexOf("mobile") != -1) || (id.indexOf("phone") != -1) ) {

            var phone_input = $(this);

            $(phone_input).on('focusout',  function( ) {

                var number = $(this).val();
                var url = "/core/lib/modules/phone_no_validator.php";

                // when user has entered 3+ characters
                if (number.length >= 3) {

                    delay(function () {
                        if(check_flag == false){
                            $.get(
                                url,
                                {'number': number},
                                function (r) {
                                    try {
                                        if(r.length) {
                                            r = json_decode(r);

                                            if (r == "valid") {
                                                $(phone_input).css("border-color", "#6ec30c");
                                            } else if (r == "invalid") {
                                                $(phone_input).css("border-color", "#df0000");
                                            } else {
                                                $(phone_input).css("border-color", "#d0d0bc");
                                            }
                                        }
                                    } catch (e) {
                                        handleSystemError(e);
                                    }
                                });
                            check_flag = true;
                        }

                        return false; // prevent potential form submission

                    }, 100);

                }
            });
        }

    });


    // help guides
    $("body").on("click", "button.help_guide", function(e){
        e.preventDefault();
        e.stopPropagation();
        if ($(this).data("help")){
            var guide = $(this).data("help");

            new popup({
                url: "/pages/documentation/?do=read_file&help=" + encodeURIComponent(guide),
            });
        }
    });

});
