/*
 * -- Atlas core javascript library --
 * @author: Daniel ness
 *
 */

$(function(){

    $.fn.extend({
        send: function( callbacks ){

            /* Submits a form via AJAX */

            var form = this.get(0);

            if( form.tagName != "FORM" ){
                throw "Invalid element for XHRequest '" + form.tagName + "'";
            }
            
            if( form.xhr != null ){
                // cancel pending request
                form.xhr.abort();
            }
            
            if( !/^function Object\(/.test(callbacks.constructor.toString()) ){
                // make sure that callbacks arg is an Object
                callbacks = {};
            }

            form.xhr = $.ajax({
                url: this.attr("action"),
                type: this.attr("method"),
                data: this.serialize(),
                dataType: ( (callbacks.dataType) ? callbacks.dataType : "html" ),
                success: function( r ){
                    callbacks.success( r );
                }
            });

        }
    });


});


