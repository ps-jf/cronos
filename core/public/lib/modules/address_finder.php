<?php

include($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

$address = new Address("line1", "line2", "line3", "postcode");

if (isset($_GET["postcode"])) {
    $address->postcode->set($_GET["postcode"]);

    print ( $results = $address->search() )
        ? json_encode($results) // could be empty
        : "false"; // error
} elseif ($_GET["address"]) {
    $address->id = intval($_GET["address"]);
    $address->get();

    $out[] = (string) $address->line1;
    $out[] = (string) $address->line2;
    $out[] = (string) $address->line3;
    print json_encode($out);
}
