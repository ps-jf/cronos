<?php

include($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");


if (isset($_GET['number'])) {
    try{
        $key = "CB82-JC35-BJ14-DY84";

        $pa = new PhoneNoValidation($key, $_GET['number'], "GB");

        if ($pa){
            $pa->MakeRequest();

            if ($data = $pa->HasData()) {
                foreach ($data as $item) {
                    if ($item->IsValid == "No") {
                        echo json_encode("invalid");
                    } elseif ($item->IsValid == "Yes") {
                        echo json_encode("valid");
                    } else {
                        echo json_encode("maybe");
                    }

                    // all available information
                    /*echo $item["PhoneNumber"] . "<br/>";
                    echo $item["ValidationSucceeded"] . "<br/>";
                    echo $item["IsValid"] . "<br/>";
                    echo $item["NetworkCode"] . "<br/>";
                    echo $item["NetworkName"] . "<br/>";
                    echo $item["NetworkCountry"] . "<br/>";
                    echo $item["NationalFormat"] . "<br/>";
                    echo $item["CountryPrefix"] . "<br/>";
                    echo $item["NumberType"] . "<br/>";*/
                }
            } else {
                echo json_encode("No Data to display");
            }
        } else {
            echo json_encode("nothing happened...");
        }
    } catch (Exception $e) {
        echo json_encode($e->getMessage());
    }
} else {
    echo json_encode("no number given");
}
