<?php

include($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");



if (isset($_GET['email'])) {
    try{
        $key = "CB82-JC35-BJ14-DY84";

        $pa = new EmailValidation($key, $_GET['email'], "5000");

        if ($pa){
            $pa->MakeRequest();

            if ($data = $pa->HasData()) {

                foreach ($data as $item) {
                    if ($item->ResponseCode == "Invalid") {
                        echo json_encode("invalid");
                    } elseif ($item->ResponseCode == "Valid") {
                        echo json_encode("valid");
                    } elseif ($item->ResponseCode == "Valid_CatchAll") {
                        echo json_encode("valid");
                    } else {
                        echo json_encode("Email validation timed-out");
                    }

                    // all available information
                    /* echo $item["ResponseCode"] . "<br/>";
                      echo $item["ResponseMessage"] . "<br/>";
                      echo $item["EmailAddress"] . "<br/>";
                      echo $item["UserAccount"] . "<br/>";
                      echo $item["Domain"] . "<br/>";
                      echo $item["IsDisposableOrTemporary"] . "<br/>";
                      echo $item["IsComplainerOrFraudRisk"] . "<br/>";
                      echo $item["Duration"] . "<br/>";*/
                }
            } else {
                echo json_encode("No Data to display");
            }
        } else {
            echo json_encode("Failed to start validator");
        }
    } catch (Exception $e) {
        echo json_encode($e->getMessage());
    }
} else {
    echo json_encode("No email passed");
}
