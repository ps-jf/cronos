<?php

include($_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php");

if (isset($_GET['search_term'])) {
    try{
        $key = "CB82-JC35-BJ14-DY84";

        // params => ($Key, $SearchTerm, $LastId, $SearchFor, $Country, $LanguagePreference, $MaxSuggestions, $MaxResults)
        $pa = new AddressFinder($key, $_GET['search_term'], "GBR", "EN", $_GET['container'] ?? "");

        if ($pa){
            $pa->MakeRequest();

            if ($data = $pa->HasData()) {
                $out = "";

                $i = 1;
                foreach ($data as $item) {
                    // all available information
                    /* echo $item["Id"] . "<br/>";
                     echo $item["Text"] . "<br/>";
                     echo $item["Highlight"] . "<br/>";
                     echo $item["Cursor"] . "<br/>";
                     echo $item["Description"] . "<br/>";
                     echo $item["Next"] . "<br/>";*/

                    if ($i % 2 == 0) {
                        $class = "odd_row";
                    } else {
                        $class = "even_row";
                    }

                    $out .= "<div id='" . $item->Id . "' class='autocomplete-suggestion " . $class . "' data-type='" . $item->Type . "' data-search_term='" . $_GET['search_term'] . "'>" . $item->Text . " <span class='text-muted'>" . $item->Description . "</span></div>";
                    $i++;
                }

                echo json_encode($out);
            } else {
                echo json_encode("no more data available");
            }
        } else {
            echo json_encode("Failed to load address finder");
        }
    } catch (Exception $e) {
        echo json_encode($e->getMessage());
    }

} elseif (isset($_GET['id'])) {
    try{
        $key = "CB82-JC35-BJ14-DY84";

        $pa = new AddressRetrieval($key, $_GET['id']);

        if ($pa){
            $pa->MakeRequest();

            if ($data = $pa->HasData()) {
                // declare new object
                $address = new stdClass();

                foreach ($data as $item) {
                    $i = 1;
                    foreach (["Company", "Line1", "Line2", "Line3", "City"] as $line) {
                        if (!empty($item->$line)) {
                            if ($i < 4) { // it's a 3-line address
                                $address->{"line" . ($i++)} = $item->$line;
                            } else { // therefore, append extra lines to line3. (messy)
                                $address->line3 .= ", " . $item->$line;
                            }
                        }
                    }

                    $address->postcode = $item->PostalCode;

                    // all available information
                    /*echo $item["Id"] . "<br/>";
                    echo $item["DomesticId"] . "<br/>";
                    echo $item["Language"] . "<br/>";
                    echo $item["LanguageAlternatives"] . "<br/>";
                    echo $item["Department"] . "<br/>";
                    echo $item["Company"] . "<br/>";
                    echo $item["SubBuilding"] . "<br/>";
                    echo $item["BuildingNumber"] . "<br/>";
                    echo $item["BuildingName"] . "<br/>";
                    echo $item["SecondaryStreet"] . "<br/>";
                    echo $item["Street"] . "<br/>";
                    echo $item["Block"] . "<br/>";
                    echo $item["Neighbourhood"] . "<br/>";
                    echo $item["District"] . "<br/>";
                    echo $item["City"] . "<br/>";
                    echo $item["Line1"] . "11<br/>";
                    echo $item["Line2"] . "22<br/>";
                    echo $item["Line3"] . "33<br/>";
                    echo $item["Line4"] . "44<br/>";
                    echo $item["Line5"] . "55<br/>";
                    echo $item["AdminAreaName"] . "<br/>";
                    echo $item["AdminAreaCode"] . "<br/>";
                    echo $item["Province"] . "<br/>";
                    echo $item["ProvinceName"] . "<br/>";
                    echo $item["ProvinceCode"] . "<br/>";
                    echo $item["PostalCode"] . "<br/>";
                    echo $item["CountryName"] . "<br/>";
                    echo $item["CountryIso2"] . "<br/>";
                    echo $item["CountryIso3"] . "<br/>";
                    echo $item["CountryIsoNumber"] . "<br/>";
                    echo $item["SortingNumber1"] . "<br/>";
                    echo $item["SortingNumber2"] . "<br/>";
                    echo $item["Barcode"] . "<br/>";
                    echo $item["POBoxNumber"] . "<br/>";
                    echo $item["Label"] . "<br/>";
                    echo $item["Type"] . "<br/>";
                    echo $item["DataLevel"] . "<br/>";*/
                }

                echo json_encode($address);
            }
        } else {
            echo json_encode("Failed to load address finder");
        }
    } catch (Exception $e) {
        echo json_encode($e->getMessage());
    }

}
