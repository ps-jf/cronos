<html>

    <head>

        <style type="text/css">

        body{
        font: 10pt CoreSansC;
        }

        table{
        border: 1px solid;
        border-collapse: collapse; 
        width: 100%;
        font-size: 9pt;
        }

        table > thead th{ font-size: 10pt; }
        
        td,th{ border: 1px solid; padding: 3px; }

        tfoot{ font-weight: bold; }

        td.sorted{ font-weight: bold; }

        td.hidden, th.hidden{ 
	    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=25)";
	    filter: alpha(opacity=25);
        -moz-opacity:0.25;
        }

        .no-print{ display: none; }

        </style>

        <style type="text/css" media="print">

        p.controls{
        display: none;
        }

        td.hidden, th.hidden{ display: none; }

        </style>

        <script type="text/javascript" src="/lib/js/jquery.js"></script>

        <script type="text/javascript">
        var loaded = true;

        $(  
            function( )
            {

                $( "thead td, thead th" ).live( 
                    "click",
                    function( )
                    {

                        var cell = this;

                        // get cell index
                        var me_index;
                        var i = 0;
                                        
                        $( "thead" ).children("th").each( 
                            function( )
                            { 
                                ( cell == this )
                                ? me_index = i
                                : i++;
                            } 
                        );

                        // bah, something's up
                        if( me_index === false ){ return false; }

                        if( $( cell ).is( ".hidden" ) )
                        {
                            $( cell ).removeClass( "hidden" );
                            $( "tbody > tr > td:eq("+me_index+")" ).removeClass( "hidden" );
    
                        }
                        else
                        {
                            $( cell ).addClass( "hidden" );
                            $( "tbody > tr > td:eq("+me_index+")" ).addClass( "hidden" );
                        }

                    }
                );
            }
        );

        </script>

    </head>


    <body>

        <p class="controls"><button onclick="window.print();">Print</button></p>
        <div id="content" name="content"></div>

    </body>

</html>
