<?php

require $_SERVER["DOCUMENT_ROOT"] . "/../bin/_main.php";

// Reduce source to one line
$src = preg_replace("/ {2,}|[\t\n\r\f]/", "", urldecode($_POST["src"]));
$out = "";

// Grab all table rows
if (preg_match_all("/<tr.*?>(.*?)<\/tr>/", $src, $rows_source)) {
    foreach ($rows_source[ 1 ] as $row_source) {
        if (preg_match_all("/<(td|h).*?>(.*?)<\/\\1>/", $row_source, $cells)) {
            foreach ($cells[ 2 ] as $cell) {

                $cell = str_replace("&nbsp;", " ", $cell);
                $cell = preg_replace("/<.*?>/", "", $cell);
                
                if ($out <> '' && $out[ strlen($out) - 1 ] <> "\n") {
                    $out .= ",";
                }

                $out .= ( is_numeric($cell) )
                    ? $cell
                    : "\"".str_replace('"', '\\"', $cell)."\"";
            }

            $out .= "\n";
        }
    }
}

//Completely remove any mad foreign characters from the whole file
$out = str_replace(utf8_decode("&pound;"), "£", $out);
$out = str_replace(utf8_decode("&amp;"), "&", $out);
$out = str_replace(utf8_decode("Â"), "", $out);
header("Content-type: text/csv");
header("Content-disposition: attachment; filename=output.csv");
echo $out;
