<?php
function url($url, $get, $xml_compliant = false)
{

    // form a url
    // -- used in Templates to modify _SELF and existing get

    $get_in = [];
    if (strpos($url, "?")>-1) {
        // parse original GET query string
        parse_str(substr($url, strrpos($url, "?")+1), $get_in);
        $url = substr($url, 0, strrpos($url, "?"));
    }

    $get_out = []; //, noone wants you here, [];

    // Iterate through input array, adjusting and including elements
    // as necessary.
    if (is_array($get)) {
        foreach ($get as $key => $value) {
            if (gettype($key) != "string") { // treat array value as key

                if (isset(${$value})) {
                    $get_out[ $value ] = $$value
                };
                else if (array_key_exists($value, $_GET)) {
                    $get_out[ $value ] = $_GET[$value];
                }
            } else {
                // NULL nulifies inclusion of $key derived from any source
                if ($value == null) {
                    unset($get_in[$key]);
                    continue;
                }

                $get_out[ $key ] = $value;
            }
        }
    }

    // Any duplicates will be overwritten ( array1 <- array2 )
    foreach (array_merge($get_in, $get_out) as $key => $value) {
        $get_out[$key] = "$key=".urlencode($value);
    }

    $url .= "?". implode("&", $get_out);

    return ($xml_compliant) ? htmlentities($url) : $url;
}

function salty_md5($str_to_encrypt = false)
{

    // Returns MD5-encryption based on input string and a dynamic salt.

    // salt
    $k = (int)( str_replace(".", "", $_SERVER["HTTP_HOST"]) / (date('Y') - 2002) );
    $k .= substr("aeiouhklmnpw", date('n')-1, 1);
    return ( !$str_to_encrypt ) ? false : md5($k . $str_to_encrypt);
}

// random_string - LETTERS only for ID's
function random_string($length)
{

    $str = "rdm";
    $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";

    $size = strlen($chars);
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[ rand(0, $size - 1) ];
    }

    return $str;
}

function passwd()
{

    // generate a password

    $pwd = strtolower(random_string(8)).";";

    $swap = array( "a", "s", "x", "d" );
    $for  = array( "@", "$", "*", "?" );

    return str_replace($swap, $for, $pwd);
}


function sendEmail($recipient, $subject, $template, $blind_copy = false, $sender = PS_EMAIL_MAIN)
{

    if (isset($blind_copy)) {
        $headers = "From: <$sender>\r\n".
            "Reply-To: <$sender>\r\n".
            "Recipient: <$recipient>\r\n".
            "Return-Path: <$sender>\r\n".
            "BCC: <$blind_copy>\r\n";
    } else {
        $headers = "From: <$sender>\r\n".
            "Reply-To: <$sender>\r\n".
            "Recipient: <$recipient>\r\n".
            "Return-Path: <$sender>\r\n";
    }

    return mail($recipient, $subject, "$template", $headers, "-r partnersupport@policyservices.co.uk");
}

class mydb
{

    const NO_CACHE = "no-cache";

    private static $db_links = [];
    private $mysqli;
    private $result;

    public function __construct($server = false, $connection_err_level = E_USER_ERROR)
    {

        ( $server ) or $server = LOCAL;

        /*
          To cut connections down to one per User I've implemented a class array
          which will store mysqli instances indexed by server.
          Therefore, this constructor will check the array for an existing mysqli
          instance and, if found, assign instance variable $mysqli a reference to it.
          If a preexisting instance is not found, one will be initiated and stored.
        */

        if (!array_key_exists($server, self::$db_links)) {
            $link = mysqli_init();
            list( $host, $user, $password, $db_name ) = json_decode($server);

            // open persistent connection
            if (!$link->real_connect($host, $user, $password, $db_name)) {
                trigger_error($link->error, $connection_err_level);
            }

            self::$db_links[$server] = $link;
        }

        $this->mysqli = &self::$db_links[$server];
    }

    public function __get($name)
    {
        // Allow access to mysqli class properties
        return $this->mysqli->{$name};
    }

    public function __call($name, $args)
    {
        // Allow access to mysqli class methods
        return call_user_func_array(array( $this->mysqli, $name ), $args);
    }

    public function query($stmt)
    {

        $r = $this->mysqli->query($stmt);

        if (!$this->errno) {
            $this->result = $r;

            return ( strpos(strtolower($stmt), "insert") === 0 )
                ? $this->insert_id
                : $this->affected_rows;
        } else {
            trigger_error("$this->error ($stmt)");
            return false;
        }
    }

    public function get_result()
    {
        return $this->result;
    }

    public function set_result($result)
    {
        $this->result = $result;
    }

    public function next($type = MYSQLI_ASSOC, $result = false)
    {

        ( $result  ) or $result = $this->result;

        if (is_a($type, "DatabaseObject")) {
            $d = mysqli_fetch_array($result, MYSQLI_ASSOC);

            if (!$d) {
                return false;
            }

            $type->load($d);
            return $type;
        } else {
            $d = ( $type == MYSQLI_OBJECT ) ? mysqli_fetch_object($result) : mysqli_fetch_array($result, $type);
        }

        return ( $d !== false ) ? $d : false;
    }
}
