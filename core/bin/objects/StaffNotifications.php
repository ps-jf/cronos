<?php

class StaffNotifications extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "staff_notifications";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->recipient_id = Sub::factory("User", "recipient_id")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set_var(Field::REQUIRED, true);

        $this->sender_id = Sub::factory("User", "sender_id")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->caller_name = Field::factory("caller_name");

        $this->caller_company = Field::factory("caller_company");

        $this->contact_details = Field::factory("contact_details");

        $this->caller_action = Choice::factory("caller_action")
            ->push('1', "Left a message")
            ->push('2', "Called for you")
            ->push('3', "Returned your call");

        $this->recipient_action = Choice::factory("recipient_action")
            ->push('1', "Please call them back")
            ->push('2', "Caller will try again later, no action required")
            ->push('3', "Caller will email you, no action required");

        $this->message = Field::factory('message')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE)
            ->set_var(Field::REQUIRED, true);

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->archived = Date::factory("archived")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->read = Boolean::factory("read");

        $this->type = Choice::factory("type")
            ->push("message")
            ->push("tag")
            ->set("message");

        parent::__construct($id, $autoget);
    }


    public function __toString()
    {
        return "$this->id";
    }
}
