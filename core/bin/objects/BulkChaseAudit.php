<?php
/**
 * Bulk chase allows the acquisition department to log
 * when they chased an agency with the provider.
 *
 *
 * @package prophet.admin.acquisition
 * @author lewis.barbour
 */

class BulkChaseAudit extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "bulk_chase_audit";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("ID", Field::PRIMARY_KEY);

        $this->week = Field::factory("week");

        $this->chase_date = Date::factory("chase_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->report_sent = Boolean::factory("report_sent");

        $this->pcomplete = Field::factory("pcomplete")
            ->set_var(Field::MAX_LENGTH, 3);

        $this->partner = Field::factory("partner_id");

        $this->helpdesk = Field::factory("helpdesk");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
