<?php

class ActionRequired extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "action_required";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->subject_id = Choice::factory("subject_id")
            ->setSource("select id, subject from myps.action_required_subject WHERE published = 1 order by subject")
            ->set_var(Field::REQUIRED, true);

//        $this->subject_id = Sub::factory("ActionRequiredSubject", "subject")
//            ->set_var(Field::REQUIRED, true);

        $this->verification_code = Field::factory("verification_code")
            ->set(uniqid());

        $this->object = Field::factory("object")
            ->set_var(Field::REQUIRED, true);

        $this->object_id = Field::factory("object_id");

        $this->addedby = Sub::factory("WebsiteUser", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->priority = Choice::factory("priority")
            ->push(1, "Low")
            ->push(2, "Medium")
            ->push(3, "High");

        $this->completed_by = Sub::factory("WebsiteUser", "completed_by");

        $this->completed_when = Date::factory("completed_when")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->archived = Boolean::factory("archived")
            ->set_values(1, 0)
            ->set(0);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true);

        $this->date_due = Date::factory("date_due")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->client_label = Field::factory("client_label");

        $this->SNAPSHOT_LOG = true;
        $this->database = REMOTE;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->subject_id";
    }

    public function get_partner()
    {
        switch ($this->object){
            case "Partner":
                $partner = new Partner($this->object_id, true);

                break;

            case "Client":
                $client = new Client($this->object_id, true);

                $partner = $client->partner;

                break;

            case "Policy":
                $policy = new Policy($this->object_id, true);

                $partner = $policy->client->partner;

                break;

            default:
                $partner  = new Partner();

                break;
        }

        return $partner;
    }

    public function get_client()
    {
        switch ($this->object){
            case "Client":
                $client = new Client($this->object_id, true);

                break;

            case "Policy":
                $policy = new Policy($this->object_id, true);

                $client = $policy->client;

                break;

            default:
                $client = new Client();
        }

        return $client;
    }

    public function get_policy()
    {
        switch ($this->object){
            case "Policy":
                $policy = new Policy($this->object_id, true);

                break;

            default:
                $policy = new Policy();

                break;
        }

        return $policy;
    }

    public function on_create($post = false)
    {
        try{
            //create a new message with the chase date for this AR
            $arm = new ActionRequiredMessage();

            $arm->action_required_id($this->id());

            $message = "Chase Date set to " . \Carbon\Carbon::createFromTimestamp($this->date_due())->toFormattedDateString();

            $arm->message($message);
            $arm->alert(1);

            $arm->save();

            //update the action required key in redis
            $redisClient = RedisConnection::connect();

            switch($this->object()){
                case "Policy":
                    $p = new Policy($this->object_id(), true);

                    $redisClient->set("action_required.Policy." . $this->object_id(), 1);
                    $redisClient->set("action_required.Client." . $p->client(), 1);
                    $redisClient->set("action_required.Partner." . $this->partner(), 1);

                    break;

                case "Client":
                    $redisClient->set("action_required.Client." . $this->object_id(), 1);
                    $redisClient->set("action_required.Partner." . $this->partner(), 1);

                    break;

                case "Partner":
                    $redisClient->set("action_required.Partner." . $this->partner(), 1);

                    break;
            }


        } catch (Exception $e){
            return $e;
        }

    }

    public function on_update($post = false){
        try{
            // do a new lookup to set and unset all this objects keys and related keys in redis action required
            switch ($this->object()) {
                case "Policy":
                    $p = new Policy($this->object_id());

                    $policyID = $p->id();
                    $clientID = $p->client();
                    $partnerID = $this->partner();

                    break;

                case "Client":
                    $clientID = $this->object_id();
                    $partnerID = $this->partner();

                    break;

                case "Partner":
                    $partnerID = $this->partner();

                    break;
            }

            if (isset($policyID)){
                $this->updateOpenTickets("Policy", $policyID);
            }

            if (isset($clientID)){
                $this->updateOpenTickets("Client", $clientID);
            }

            if (isset($partnerID)){
                $this->updateOpenTickets("Partner", $partnerID);
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    public function get_allocated()
    {
        $s = new Search(new ActionRequiredAssigned());

        //get the last person this ticket was assigned to
        $s->eq("action_required", $this->id());
        $s->add_order("addedwhen", "DESC");
        $s->limit(1);

        if ($ara = $s->next(MYSQLI_ASSOC)){
            $assigned = $ara->assigned;
        } else {
            $assigned = $this->addedby;
        }

        return $assigned;
    }

    public function updateOpenTickets($object, $objectID){
        $redisClient = RedisConnection::connect();

        if ($redisClient->exists("action_required." . $object . "." . $objectID)){
            // check if the object has other tickets still open
            switch ($object) {
                case "Partner":
                    // use partner column in action required table to check against all partners clients and policies too
                    $s = new Search(new ActionRequired());
                    $s->eq("completed_when", null);
                    $s->eq("archived", 0);
                    $s->eq("partner", $objectID);

                    if (!$s->count()){
                        $redisClient->del("action_required." . $object . "." . $objectID);
                    }

                    break;

                case "Client":
                    // custom query to check against client and policy level tickets for this client
                    $db = new mydb(REMOTE);

                    $query = "SELECT id FROM " . REMOTE_SYS_DB . ".action_required WHERE completed_when is null AND action_required.archived = 0 AND object = 'Client' and object_id = " . $objectID .
                            " UNION
                            SELECT id FROM " . REMOTE_SYS_DB . ".action_required ar INNER JOIN " . DATA_DB . ".tblpolicy p on ar.object_id = p.policyID WHERE completed_when is null AND ar.archived = 0 AND object = 'Policy' and p.clientID = " . $objectID;

                    $db->query($query);

                    if (!$db->next(MYSQLI_ASSOC)){
                        $redisClient->del("action_required." . $object . "." . $objectID);
                    }

                case "Policy":
                    // nice easy one, just check against this policy ID for open tickets
                    $s = new Search(new ActionRequired());
                    $s->eq("completed_when", null);
                    $s->eq("archived", 0);
                    $s->eq("object", $object);
                    $s->eq("object_id", $objectID);

                    if (!$s->count()){
                        $redisClient->del("action_required." . $object . "." . $objectID);
                    }
            }
        }
    }

    public function progress_indicator()
    {
        $rc = RedisConnection::connect();

        $span = "";

        if ($rc->exists("ar_progress." . $this->id())){
            $progress = $rc->get("ar_progress." . $this->id());

            $pro_parts = explode(".", $progress);

            $uid = $pro_parts[0];
            $pro_when = $pro_parts[1];

            $user = new User($uid, true);

            $started = \Carbon\Carbon::createFromTimestamp($pro_when)->toDayDateTimeString();

            $span = "<div class='alert alert-success mb-1 ar_progress_indicator'><i class='fas fa-check'></i> Ticket is currently in progress by " . $user->staff_name() . " (Started: " . $started . ")";

            if ($uid == User::get_default_instance("id")){
                $span .= " <a class='alert-link' href='#' id='ar_end_progress'>Click here to end progress</a>";
            }

            $span .= "</div>";
        } else {
            $span = "<div class='alert alert-warning mb-1 ar_progress_indicator'><i class='fas fa-clock'></i> Ticket is not currently being worked on. <a class='alert-link' id='ar_start_progress' href='#'>Click Here to start progress</a></div>";
        }

        return $span;
    }

    public function add_message($text)
    {
        $m = new ActionRequiredMessage();

        $m->action_required_id($this->id());
        $m->message(addslashes($text));

        return $m->save();
    }

    public function close($user = false, $time = false)
    {
        if (!$user){
            $user = User::get_default_instance("id");
        }

        if (!$time){
            $time = time();
        }

        $this->completed_by($user);
        $this->completed_when($time);

        return $this->save();
    }
}
