<?php

class LifeEvent extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "life_events";

    public function __construct($id = false, $autoget = false)
    {
        $this->SNAPSHOT_LOG = true;

        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "clientID");

        $this->event = Sub::factory("LifeEventType", "event_type");

        $this->document_for = Field::factory("document_for");

        $this->affected_issuers = Field::factory('affected_issuers');

        $this->status = Choice::factory("status")
            ->push_array(["Pending", "Complete"]);

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(date('Y-m-d H:i:s'));

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        if (!$this->loaded) {
            $this->get();
        }

        if (!$this->id()) {
            return "";
        }

        return "$this->id";
    }

    public function getLinkedPolicies($policyID, $issuerID)
    {
        $policy_nums= [];
        $affected_issuers = json_decode($this->affected_issuers(), true);

        foreach ($affected_issuers as $key => $value) {
            if ($key && $key == $issuerID) {
                $iss = new Issuer($key, true);
                $issuers[$iss ."_0"] = $iss;
                $issuers[$iss ."_1"] = $iss;

                foreach ($value as $policy) {
                    if ($policy != $policyID) {
                        $pol = new Policy($policy, true);
                        $policy_nums[$iss->id()][] = $pol->link();
                    }
                }
            }
        }

        return $policy_nums;
    }
}
