<?php

class NewBusinessType extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblnewbusinesstype";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->name = Field::factory("description");

        $this->date_fields = Field::factory("form_date_fields");

        $this->date_title = Field::factory("form_date_name");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->name";
    }
}
