<?php

class Groups extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "groups";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->name = Field::factory('name');

        $this->parent_id = Field::factory('parent_id');

        $this->description = Field::factory('description');

        parent:: __construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->name";
    }

    public static function dept_groups()
    {
        /*
        * return an array containing department groups
        */
        $dept_array = [];

        // exclude call scoring, account reviews and FCC from search
        $s = new Search(new Groups);
        $s->nt("id", 9);
        $s->nt("id", 11);
        $s->nt("id", 12);
        $s->add_order("name");

        while ($dept = $s->next(MYSQLI_ASSOC)) {
            $dept_array[$dept->id()] = $dept->name();
        }

        return $dept_array;
    }

    public static function dept_users($dept)
    {
        $users = [];

        $s = new Search(new User);
        $s->eq("department", $dept);
        $s->eq("active", 1);

        while($u = $s->next(MYSQLI_ASSOC)){
            $users[] = $u;
        }

        return $users;
    }

    public function has_permission($permission_id)
    {
        $s = new Search(new ObjectPermission());

        $s->eq("object_type", "Group");
        $s->eq("object_id", $this->id());
        $s->eq("permission", $permission_id);

        return $s->next(MYSQLI_ASSOC);
    }
}
