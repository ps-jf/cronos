<?php

class AdviserChargingFile extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "adviser_charging_file";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->ac_id = Field::factory("ac_id");

        $this->filepath = Field::factory("filepath");

        $this->uploaded_by = Sub::factory("User", "uploaded_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->uploaded_when = Date::factory("uploaded_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    function __toString()
    {
        return "$this->filepath";
    }
}
