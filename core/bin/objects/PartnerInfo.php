<?php


class PartnerInfo extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "partner_info";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partner_id");

        $this->portfolio_reports = Field::factory("portfolio_reports")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->welcome_letters = Field::factory("welcome_letters")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->new_business_cases = Field::factory("new_business_cases")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->client_proposition = Field::factory("client_proposition")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->other = Field::factory("other")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return (string)$this->partner . " - Partner Info";
    }
}