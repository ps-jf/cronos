<?php

class CommissionType extends DatabaseObject
{
    //public function CommissionType()
    public function __construct($id = false, $auto_get = false)
    {
        $this->table = DATA_DB . ".commission_type";

        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->name = new Field("name");

        $this->partner_percent = new Field("partner_percent", Field::PERCENT);

        $this->ps_percent = new Field("ps_percent", Field::PERCENT);

        $this->locked = new Boolean("shares_locked");

        $this->vat = new Field("VAT");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        // example: Clawback
        return "$this->name";
    }


    // Lets only declare commission types to be included in GRI once instead of it being different on each page.  This
    // wee function is used in Client/Partner/Practive objects within object_gri() functions.
    /* GRI calculated as (renewal(0) + clawback(2) + adviser_charging_ongoing(8) + client_direct_ongoing(10) + dfm_fees(12) +
    OAS Income(14) + Trail(15) + PPB Income(16)) - VAT */
    public static function griTypes()
    {
        return "(0,2,8,10,12,14,15,16)";
    }
}
