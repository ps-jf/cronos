<?php

class PendingPipelineEntry extends DatabaseObject
{

    public function __construct()
    {
        $this->table = DATA_DB . ".pending_pipeline";

        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->batch = new Sub("Batch", "batch_id");

        $this->amount = new Field("amount", Field::CURRENCY);

        $this->type_text = new Field("type_text");

        $this->type = new Sub("CommissionType", "type_id");

        $this->policy_number = new Field("policy_number");

        $this->policy = new Sub("Policy", "policy_id");

        $this->forename = new Field("client_forename");

        $this->surname = new Field("client_surname");

        $this->agency_number = new Field("agency_number");

        call_user_func_array(array($this, "parent::__construct"), func_get_args());
    }

    public function transfer_to_pipeline()
    {
        /*
          Transfer this pending entry into the real pipeline table 
         */

        $pipeline = new PipelineEntry;

        $pipeline->load(array(
            "batch" => $this->batch(),
            "policy" => $this->policy(),
            "amount" => $this->amount(),
            "type" => $this->type()
        ));

        if (!$pipeline->save()) {
            throw new Exception("Cannot save pipeline entry");
        }
    }

    public function __toString()
    {
        return "$this->type $this->amount";
    }
}
