<?php

class WebNotificationViewer extends DatabaseObject
{
    var $table = WEB_NOTIFICATION_VIEWER_TBL;

    public function WebNotificationViewer()
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->notification = new Sub("WebNotification", "notification_id");

        $this->user = new Sub("WebsiteUser", "user_id");

        $this->partner = new Sub("Partner", "partner_id");

        call_user_func_array(array("parent", "__construct"), func_get_args());
    }
}
