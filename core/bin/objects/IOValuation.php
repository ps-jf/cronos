<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 31/10/2019
 * Time: 09:18
 */

class IOValuation extends DatabaseObject
{
    const DB_NAME = IO_DB;
    const TABLE = "valuations";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->io_policy = Sub::factory("IOPolicy", "io_policy_id");

        $this->valued_on = Date::factory("val_on")
            ->set_var(Date::FORMAT, DATE_ISO8601);

        $this->type = Field::factory("type");

        $this->amount = Field::factory("amount");

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->database = IO;

        parent::__construct($id, $auto_get);
    }
}