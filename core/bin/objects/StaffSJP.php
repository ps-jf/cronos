<?php

class StaffSJP extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblbusacqman";
    var $_profilePath = "/pages/staffSJP/";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->name = Field::factory("name")
            ->set_var(Field::REQUIRED, true);

        $this->office = Field::factory("office");

        $this->contactMobile = Field::factory("mobileNumber");

        $this->personalAssistant = Field::factory("personalAssistant");

        $this->personalAssistant_ext = Field::factory("personalAssistant_ext");

        parent::__construct($id, $autoget);
    }
}
