<?php

class Field
{

    /** A field holds a single item of information within an object. */

    const PRIMARY_KEY = "PRIMARY_KEY";
    const NO_TO_SQL = "NO_TO_SQL";
    const TYPE = "TYPE";
    const MAX_LENGTH = "MAX_LENGTH";
    const PATTERN = "PATTERN";
    const REQUIRED = "REQUIRED";
    const PARTNER_REF = "PARTNER_REF";
    const DISPLAY_NAME = "DISPLAY_NAME";

    // core settings
    const BLOCK_INSERT = "BLOCK_INSERT";
    const BLOCK_UPDATE = "BLOCK_UPDATE";
    const FORCE_ZERO = "FORCE_ZERO";
    const STRING = "string";
    const FLOAT = "float";
    const INTEGER = "integer";
    const CURRENCY = "currency";
    const DATE = "date";

    // SQL statement mode permissions
    const PASSWD = "passwd";
    const PERCENT = "percent";

    // @TODO essentially a hack
    //we have db values that can have zero as a primary key
    //we need this to pass validations checks and the sql to be created properly
    const HTML_ID_PLAIN = "plain";

    // Data types
    const HTML_ID_JQUERY = "jquery";
    const HTML_FIELD_TYPE = "HTML_FIELD_TYPE";
    const HTML_SINGLELINE = "singleline";
    const HTML_MULTILINE = "multiline";
    const HTML_HIDDEN = "hidden";

    var $value = false;
    var $db_object;
    var $is_set;
    var $name;
    var $edited = false;
    var $group;

    private $databaseName;
    private $settings = [];

    public function __construct($db_name = false, $type = false)
    {
        $this->databaseName = $db_name;

        if ($type == Field::PRIMARY_KEY) {
            $this->set_var(Field::PRIMARY_KEY, true);
            $this->set_var(Field::TYPE, Field::INTEGER);
            $this->set_var(Field::BLOCK_UPDATE, true);
        } elseif ($type !== false) {
            $this->set_var(Field::TYPE, $type);
        } else {
            $this->set_var(Field::TYPE, Field::STRING);
        }
    }

    public function set_var($var_name, $var_value)
    {

        if (!defined('static::' . $var_name)) {
            trigger_error("$var_name setting does not exist in " . get_called_class(), E_USER_ERROR);
        }

        if ($var_name == Field::TYPE) { // backwards compatible
            switch ($var_value) {
                case Field::PERCENT:
                    $this->set_var(Field::PATTERN, "/^(100|[1-9]?[0-9])(\.\d+)?$/");
                    $this->set_var(Field::MAX_LENGTH, 5); // need to allow 99.55%
                    break;

                case Field::CURRENCY:
                    $this->set_var(Field::PATTERN, "/^\-?\d+(\.\d{1,2})?$/");
                    break;

                case Field::PARTNER_REF:
                    $this->db_object->partner = &$this;
                    break;
            }
        }

        $this->settings["Field::$var_name"] = $var_value;
        return $this;
    }

    public static function factory()
    {
        $r = new ReflectionClass(get_called_class());
        return $r->newInstanceArgs(func_get_args());
    }

    function __invoke($value = null)
    {
        // Calling Field object as a function
        if ($value === null) {
            // if value not supplied, read value
            return $this->value;
        }
        // otherwise set Field
        $this->set($value);
        return $this;
    }

    public function set($value, $mark_as_edited = true)
    {
        //// Set the value of this Field
        $this->value = $value;
        $this->edited = $mark_as_edited;
        $this->is_set = true;
        return $this;
    }

    function __set($name, $value)
    {
        $this->{$name} = $value;

        if (is_a($value, "Field")) {
            $this->{$name}->name = $name;
        }
    }

    public function display_name()
    {
        /* Returns a user-friendly version of the property name, attempts to
         generate one if not specified using "->set_var( Field::DISPLAY_NAME, name )" */

        if (($name = $this->get_var(Field::DISPLAY_NAME)) == null) {
            $name = ucwords(str_replace('_', ' ', $this->name));
        }

        return $name;
    }

    public function get_var($var_name)
    {
        if (!defined('static::' . $var_name)) {
            trigger_error("$var_name getting does not exist in " . get_called_class(), E_USER_ERROR);
        }

        return (array_key_exists("Field::$var_name", $this->settings)) ? $this->settings["Field::$var_name"] : null;
    }

    public function is_valid()
    {
        $val = $this();

        if ($this->get_var(Field::REQUIRED) && empty($val)) {
            if ($this->get_var(Field::FORCE_ZERO) && $val === '0') {
                //do nothing, weird control flow, but meh
            } else {
                // ensure required fields are populated
                throw new Exception(" is required");
            }
        } elseif (!empty($val) && ($pattern = $this->get_var(Field::PATTERN)) !== null) {
            // if regex pattern defined, check value against it
            if (!preg_match($pattern, $val)) {
                throw new Exception(" value '$val' is invalid");
            }
        }
    }

    public function to_sql($value = null, $wildcard = false, $mode = false)
    {
        $value = ($value !== null) ? $value : $this();

        // If an array has been passed as $value in the function parameters, then prepare the
        // SQL as "field_name IN (1,2,3...)"
        // ** Wildcard is always ignored in this style. **

        $db = new mydb($this->db_object->get_database());

        if (is_array($value)) {
            for ($i = 0; $i < count($value); $i++) {
                $value[$i] = (is_numeric($value[$i])) ? $value[$i] : "'" . $db->real_escape_string($value[$i]) . "'";
            }
            return
                $this->get_field_name(true, true) . " IN ( " . implode(", ", $value) . " )";
        }

        $value = $db->real_escape_string(trim($value));
        $out = $this->get_field_name(true, true);

        if ($this->get_var(Field::TYPE) == Field::PASSWD) {
            $out .= " = PASSWORD('$value')";
        } elseif (in_array($this->get_var(Field::TYPE), array(Field::INTEGER, Field::FLOAT))) {
            // we need to know the mode to handle null properly. If updating/inserting we need equals, selecting needs is
            $mode = ($mode == DatabaseObject::SAVE_INSERT_MODE || $mode == DatabaseObject::SAVE_UPDATE_MODE) ? "=" : "is";

            if (!empty($value)) {
                $out .= " = $value";
            } else {
                if ($this->get_var(Field::FORCE_ZERO) && $value === '0') {
                    $out .= " = 0";
                } else {
                    $out .= $mode . " NULL";
                }
            }
        } else {
            if ($wildcard) {
                $out .= " LIKE ";
                $value .= "%";
            } else {
                $out .= " = ";
            }

            $out .= "\"$value\"";
        }

        return $out;
    }

    public function get_field_name($absolute = true, $with_ticks = false)
    {
        // return Field's database field name
        $field = ($with_ticks) ? "`$this->databaseName`" : $this->databaseName;
        return ($absolute) ? $this->db_object->get_table() . ".$field" : "$field";
    }

    function __toString()
    {
        // format string rep depending on the field type
        switch ($this->get_var(Field::TYPE)) {
            case Field::CURRENCY:
                return "&pound; " . number_format((float)$this->value, 2);
                break;

            case Field::PERCENT:
                $value = (float)$this->value;
                return (($value % 1) ? number_format($value, 2) : $value) . "%";
                break;

            default:
                return "$this->value";
                break;
        }
    }

    public function get()
    {
        return "$this";
    }

    public function friendly_name()
    {
        // convert example_property to "Example Property"
        return ucwords(str_replace("_", " ", $this->name));
    }

    public function toElement($id = false, $attr = false, $value = false)
    {
        /*** Outputs the Field in an appropriate form element ***/
        $id = $this->get_html_id($id);
        $value = ($value !== false) ? $value : $this->value;

        // apply any class styles / component links to element
        if (property_exists($this, "className")) {
            $attr .= " class=\"" . $this->className . "\" ";
        }

        // as well as any value restrictions
        if ($this->get_var(Field::MAX_LENGTH) !== null) {
            $length = $this->get_var(Field::MAX_LENGTH);
            $size = $length;
            if ($length > 25) {
                $size = 35;
            }
            $attr .= "maxlength='$length' size='$size' ";
        }

        if (($pattern = $this->get_var(Field::PATTERN)) !== null) {
            /* convert regex from PCRE to J RegExp-friendly */

            $delim = substr($pattern, 0, 1);

            $field = get_class($this->db_object) . "->" . $this->name;

            if (($last_delim_pos = strrpos($pattern, $delim)) == 0) {
                trigger_error("No closing pattern delimiter found in $field", E_USER_ERROR);
            }

            // extract + validate modifiers
            $mods = substr($pattern, $last_delim_pos + 1);
            if (strlen($mods) > 0) {
                foreach (str_split($mods) as $sym) {
                    if (strpos("imsx", $sym) == -1) {
                        trigger_error("Invalid RegExp modifier '$sym' found in $field modifiers", E_USER_ERROR);
                    }
                }
            }

            // remove delimiters
            $pattern = substr($pattern, 1, $last_delim_pos - 1);

            // include regex + mods as HTML5-friendly attributes
            $attr .= "data-pattern=\"$pattern\" data-pattern_mods=\"$mods\" ";
        }

        // wrapper classes
        $wrap_classes = array($this->get_var(Field::TYPE));

        // register Field as writable
        $this->db_object->register_field($this);


        if ($this->get_var(Field::HTML_FIELD_TYPE) == Field::HTML_HIDDEN) {
            return el::hide($id, $value, $attr);
        }

        // output element based on object type
        $content = ($this->get_var(Field::HTML_FIELD_TYPE) == Field::HTML_MULTILINE)
            ? "<textarea id='$id' name='$id' $attr>$value</textarea>"
            : el::txt($id, $value, $attr);

        $out = "<span class='input " . join(" ", $wrap_classes) . "'>$content</span>";
        if ($this->get_var(Field::REQUIRED)) {
            $out = "<span class='required'>$out</span>";
        }

        return $out;
    }

    public function get_html_id($custom_id = false, $format = Field::HTML_ID_PLAIN)
    {
        /* Get an HTML element ID for this Field.
         In prinicipal this is simply the Field->name property,
         however the new group_name method format may be encountered
         where object security is used. */

        $id = ($custom_id == false) ? $this->name : $custom_id;

        if ($this->group !== null) {
            return $this->db_object->group_name . "[" . $this->group->name . "][$id]";
        }

        if ($this->db_object !== null) {
            return $this->db_object->get_html_id($id, $format);
        }

        return $id;
    }
}
