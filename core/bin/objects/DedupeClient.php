<?php

use Carbon\Carbon;

class DedupeClient extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tbldedupe";

    var $NAME_ORDER = ["one->surname", "one->forename", "two->surname", "two->forename"];

    public function __construct()
    {
        $this->dedupeID = Field::factory("deDupeID", Field::PRIMARY_KEY);

        $this->id = Field::factory("oldClientID");

        $this->title_1 = Field::factory("oldclientTitle1");

        $this->first_name_1 = Field::factory("oldclientFname1");

        $this->surname_1 = Field::factory("oldclientSname1");

        $this->title_2 = Field::factory("oldclientTitle2");

        $this->first_name_2 = Field::factory("oldclientFname2");

        $this->surname_2 = Field::factory("oldclientSname2");

        $this->dob_1 = Field::factory("oldclientDOB1");

        $this->dob_2 = Field::factory("oldClientDOB2");

        $this->address = Address::factory(
            "oldclientaddress1",
            "oldclientaddress2",
            "oldclientaddress3",
            "oldclientaddresspostcode"
        );

        $this->phone = Field::factory("oldclienttelno")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->archived = Field::factory("archived");

        $this->movedToClient = Sub::factory("Client", "movedtoClientID");

        $this->partner = Sub::factory("Partner", "oldpartnerID");

        $this->bulk = Boolean::factory("oldbulkclient")
            ->set_values("x", "");

        $this->NINO1 = Field::factory("oldclientNINO1");

        $this->NINO2 = Field::factory("oldclientNINO2");

        $this->addedBy = Sub::factory("User", "oldaddedBy");

        $this->addedWhen = Date::factory("oldaddedWhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->group_scheme = Boolean::factory("oldgroup_scheme")
            ->set_values(1, 0)
            ->set(0);

        $this->mob_phone = Field::factory("oldclientmobno")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->default_num = Choice::factory("olddefault_num")
            ->push("1", "Home")
            ->push("2", "Mobile")
            ->set_var(Field::DISPLAY_NAME, "olddefault_num");

        $this->updatedwhen = Date::factory("oldupdated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "oldupdated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        // 1:Prophet 2:Mandate 3:Commission 4:Import 5: Bulk system 6:Client Split
        $this->addedhow = Field::factory('oldadded_how')
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(1);

        $this->servicing_level = Choice::factory("oldservicing_level")
            ->push_array(array("1", "3", "6", "12", "18", "24", "36", "48", "L", "T"));
        //->push_array( array(1=>"Monthly",2=>"Bimonthly",3=>"Quarterly",4=>"Biannually",5=>"Annually",6=>"Biennially") );

        $this->last_review_date = Date::factory("oldlast_review_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->next_review_date = Date::factory("oldnext_review_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->csrd = Boolean::factory("oldcsrd");

        $this->salutation = Field::factory("oldsalutation");

        $this->csrd_excluded = Choice::factory("oldcsrd_excluded")
            ->push("1", "Admin Client")
            ->push("2", "Family/Self")
            ->push("3", "Written to 2012")
            ->push("4", "Written to")
            ->push("5", "Marked for removal")
            ->push("6", "Policy Status")
            ->push("7", "Policy Type")
            ->push("8", "Zero Policies");

        $this->intelligent_office = Boolean::factory("oldintelligent_office")
            ->set_values(1, 0);

        $this->pv_mandatory_start = Date::factory("oldpv_mandatory_start")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->pv_comp = Boolean::factory("oldpv_comp");

        $this->pre_rdr = Boolean::factory("oldpre_rdr");

        $this->pure_protection = Boolean::factory("oldpure_protection");

        $this->welcome_pack = Date::factory("oldwelcome_pack")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->complaint_ongoing = Boolean::factory("complaint_ongoing");

        $this->import_id = Field::factory("tempclientidforimport");

        $this->scheme_name = Field::factory("scheme_name");
    }

    public function fullName()
    {
        $fullname = "";

        if ($this->surname_1 != "" && $this->surname_2 != ""){
            if ($this->surname_1 == $this->surname_2){
                $fullname = $this->first_name_1 . " & " . $this->first_name_2 . " " . $this->surname_1;
            } else {
                $fullname = $this->first_name_1 . " " . $this->surname_1 . " & " . $this->first_name_2 . " " . $this->surname_2;
            }
        } elseif ($this->surname_1 != ""){
            $fullname = $this->first_name_1 . " " . $this->surname_1;
        } else {
            $fullname = $this->first_name_2 . " " . $this->surname_2;
        }

        return $fullname;
    }
}
