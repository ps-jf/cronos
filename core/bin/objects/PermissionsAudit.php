<?php

class PermissionsAudit extends DatabaseObject
{
    /*
     * Provides an audit of viewer permissions
     */

    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "permissions_audit";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->viewer_entry = Sub::factory("ViewerEntry", "viewer_tbl_id");

        $this->user = Sub::factory("WebsiteUser", "user_id");

        $this->partner = Sub::factory("Partner", "partner_id");

        $this->permissions = JSON::factory("permissions_array");

        // The level of access assigned to this WebsiteUser
        $this->access_level = Choice::factory("access_level")
            ->push_array(["USER", "ADMIN", "CUSTOM"]);

        $this->email_notification = Boolean::factory("email_notification");

        $this->access_revoked = Boolean::factory("access_revoked");

        $this->updated_by = Sub::factory("WebsiteUser", "added_by")
            ->set(User::get_default_instance("myps_user_id"));

        $this->updated_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->user($this->access_level)";
    }
}