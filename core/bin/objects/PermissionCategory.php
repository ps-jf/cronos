<?php


class PermissionCategory extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "permission_categories";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->name = Field::factory("name");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return strval($this->name);
    }
}