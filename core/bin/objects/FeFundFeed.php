<?php

class FeFundFeed extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "fe_fund_feed";

    public function __construct($id = false, $auto_get = false)
    {
        $this->citicode = Field::factory("citicode", Field::PRIMARY_KEY);
        $this->mexcode = Date::factory("mexcode");
        $this->sedol = Date::factory("sedol");
        $this->isin = Field::factory("isin");
        $this->unitnameshort = Field::factory("unitnameshort");
        $this->currency = Field::factory("currency");
        $this->managementgroupname = Field::factory("managementgroupname");
        $this->legalstructure = Field::factory("legalstructure");
        $this->unittype = Field::factory("unittype");
        $this->xdflag = Field::factory("xdflag");
        $this->valuationdate = Field::factory("valuationdate");
        $this->bidprice = Field::factory("bidprice");
        $this->offerprice = Field::factory("offerprice");
        $this->midprice = Field::factory("midprice");
        $this->yield = Field::factory("yield");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->citicode";
    }
}
