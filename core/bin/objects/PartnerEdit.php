<?php

class PartnerEdit extends DatabaseObject
{
    /*
     * A PartnerEdit object contains a serialized instance of an edited Partner
     * object from mypsaccount.
     */

    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "partner_changes";

    public function __construct($id, $autoget)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->datetime = Date::factory("datetime")->set_var(Date::FORMAT, Date::UNIX);

        $this->partner = Sub::factory("Partner", "partner_id");

        $this->user = Sub::factory("WebsiteUser", "user_id");

        $this->object = Field::factory("object");

        $this->enacted = Boolean::factory("enacted");
    }

    public function __toString()
    {
        return "$this->partner $this->datetime";
    }
}
