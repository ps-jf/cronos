<?php
/**
 * Created by PhpStorm.
 * User: Sean
 * Date: 2020-04-09
 * Time: 10:21
 */

class PolicyNumberIssuerMap extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "policynum_issuer_map";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->issuer = Sub::factory("Issuer", "issuerID")
            ->set_var(Field::REQUIRED, true);

        $this->policy_number = Field::factory("policy_num")
            ->set_var(Field::REQUIRED, true);

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function mapIssuer($pn, $currentIssuer)
    {
        $db = new mydb();

        $db->query("SELECT issuerID FROM " . $this->get_table() . " WHERE '" . $pn . "' LIKE policy_num and issuerID != " . $currentIssuer);

        $result = $db->next(MYSQLI_ASSOC);

        if ($result){
            return $result['issuerID'];
        } else {
            return false;
        }
    }
}