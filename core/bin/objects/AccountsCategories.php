<?php

class AccountsCategories extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "accounts_categories";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->category = Field::factory('category');

        $this->sage_nc = Field::factory('sage_nc');

        $this->CAN_DELETE = true;

        parent:: __construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->category";
    }
}
