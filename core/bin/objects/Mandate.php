<?php

class Mandate extends DatabaseObject implements FileEntry
{
    const DB_NAME = DATA_DB;
    const TABLE = "mandate_tbl";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->file = Field::factory("filename");

        $this->partner = Sub::factory("Partner", "partner_id");

        $this->client = Sub::factory("Client", "client_id");

        $this->pending_client = Sub::factory("PendingClient", "pending_client_id");

        $this->issuer = Sub::factory("Issuer", "issuer_id");

        $this->track_id = Field::factory("track_id", Field::INTEGER);

        $this->prepop = Boolean::factory("prepop");

        $this->policy_count = Field::factory("policy_count", Field::INTEGER);

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->sent_by = Sub::factory("User", "sent_by");

        $this->sent_when = Date::factory("sent_when")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->original_copy = Choice::factory("original")
            ->push("1", "Copy")
            ->push("2", "Original")
            //->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Original/Copy");

        $this->jasper_include = Boolean::factory("jasper_include");

        $this->dropped = Boolean::factory("dropped");


        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __is_valid()
    {
        if (!$this->sent_when->edited && $this->sent_when()) {
            throw new Exception("Cannot update a sent Mandate");
        }
    }

    public function get_file_path()
    {
        if ($this->sent_when() !== false) {
            return MANDATE_PROCESSED_SCAN_DIR;
        } else {
            return MANDATE_PENDING_SCAN_DIR;
        }
    }

    public function get_file_stream()
    {
        //
    }

    public function on_update($post = false)
    {
        if ($this->pending_client() && $this->client->edited) {
            $db = new mydb();

            $db->query("UPDATE " . $this->get_table() . " " .
                "SET " . $this->client->to_sql() . " " .
                "WHERE " . $this->pending_client->to_sql() . " " .
                "AND " . $this->sent_when->get_field_name() . " IS NULL");
        }
    }

    public function rename_scan($file, $queue)
    {
        /* Obtain department for member of staff currently logged in obtain mandate list */
        // Acquisition Support department - Bulk Mandates
        $mandate_pending_scan_dir = MANDATE_PENDING_SCAN_DIR . "/" . $queue;

        if ($this->file->is_set) {
            if (is_file($mandate_pending_scan_dir . "/$file")){
                //check file exists before trying to rename it
                if (rename($mandate_pending_scan_dir . "/$file", $mandate_pending_scan_dir . "/" . $this->file())) {
                    return true;
                }
            }
        }
        return false;
    }

    public function __toString()
    {
        return "$this->file";
    }
}
