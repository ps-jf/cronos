<?php

class DeathCertificate extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "death_certificates_poa";

    public function __construct($id = false, $autoget = false)
    {
        $this->SNAPSHOT_LOG = true;

        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->policy = Sub::factory("Policy", "policyID");

        $this->issuer = Sub::factory("Issuer", "issuerID");

        $this->client = Sub::factory("Client", "clientID");

        $this->form_type = Choice::factory("form_type")
            ->push("1", "Death Certificate")
            ->push("2", "Grant Of Probate")
            ->push("3", "POA")
            ->push("4", "Marriage Certificate")
            ->push("5", "Birth Certificate")
            ->push("6", "Trust Forms")
            ->push("7", "Change of Beneficiaries")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Form");

        $this->date_sent_to_provider = Date::factory("date_sent_to_provider")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->track_sent_to_provider = Field::factory("track_sent_to_provider");

        $this->date_returned_from_provider = Date::factory("date_returned_from_provider")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->track_returned_from_provider = Field::factory("track_returned_from_provider");

        $this->date_returned_to_partner = Date::factory("date_returned_to_partner")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->track_returned_to_partner = Field::factory("track_returned_to_partner");

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        if (!$this->loaded) {
            $this->get();
        }

        if (!$this->id()) {
            return "";
        }

        return "$this->id";
    }
}
