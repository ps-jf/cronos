<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 22/07/2019
 * Time: 15:00
 */

class VulnerableClient extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "vulnerable_clients";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "client_id")
            ->set_var(Field::REQUIRED, true);

        $this->client_index = Field::factory("client_index");

        $this->added_when = Date::factory("addedWhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->added_by = Sub::factory("WebsiteUser", "addedBy")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->reason = Field::factory("reason")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->archived = Boolean::factory("archived");

        $this->updated_when = Date::factory("updatedWhen")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->updated_by = Sub::factory("WebsiteUser", "updatedBy")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("myps_user_id"));

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->reason - " . $this->added_by;
    }
}