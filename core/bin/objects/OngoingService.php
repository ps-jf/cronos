<?php

class OngoingService extends DatabaseObject
{
    const DB_NAME = "myps";
    const TABLE = "ongoing_service";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY)
            ->set_var(Field::DISPLAY_NAME, "ID");

        $this->client_id = Sub::factory("Client", "client_id")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Client");

        $this->added_by = Sub::factory("WebsiteUser", "added_by")
            ->set(User::get_default_instance("myps_user_id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(date("Y-m-d"));

        $this->review_offered = Boolean::factory("review_offered")
            ->set_var(Field::TRUE_VALUE, true)
            ->set_values(1, 0)
            ->set(0);

        $this->review_accepted = Boolean::factory("review_accepted")
            ->set_var(Field::TRUE_VALUE, true)
            ->set_var(Field::DISPLAY_NAME, "Offered & Accepted")
            ->set_values(1, 0)
            ->set(0);

        $this->reason_declined = Sub::factory("MypsNotes", "reason_declined_note_id");
        $this->post_meeting_notes = Sub::factory("MypsNotes", "post_meeting_notes_id");

        $this->review_type = Choice::factory("review_type")
            ->push_array([
                "Face to Face" => "Face to Face",
                "Telephone" => "Telephone",
                "Letter/Written" => "Letter/Written",
                "Virtual" => "Virtual",
                "PS Forced" => "PS Forced",
            ])
            ->set_var(Field::TRUE_VALUE, true);

        $this->proposed_review_date = Date::factory("proposed_review_date")
            ->set_var(Field::DISPLAY_NAME, "Review Date")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE)
            ->set_var(Field::TRUE_VALUE, true);

        $this->val_req_id = Sub::factory("ValuationRequest", "val_req_id")
        ->set_var(Field::DISPLAY_NAME, "Portfolio Report");

        $this->review_done = Boolean::factory("review_done")
            ->set_var(Field::TRUE_VALUE, true)
            ->set_var(Field::DISPLAY_NAME, "Did the review take place?")
            ->set_values(1, 0)
            ->set(0);

        $this->no_review_dropdown = Field::factory("no_review_dropdown")
            ->set_var(Field::DISPLAY_NAME, "Declined Reason");

        $this->archived_by = Sub::factory("WebsiteUser", "archived_by");

        $this->archived_when = Date::factory("archived_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->document_upload_ids = Field::factory("document_upload_ids");

        $this->review_location = Field::factory("review_location");

        $this->rearranged = Boolean::factory("rearranged")
            ->set_var(Field::TRUE_VALUE, true)
            ->set(0);

        $this->declined_review_type = Field::factory("declined_review_type");

        $this->declined_review_date = Date::factory("declined_review_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->date_log = Field::factory("date_log");

        $this->parent_id = Sub::factory("OngoingService", "id");

        $this->has_child = Boolean::factory("has_child")
            ->set_values(1, 0)
            ->set(0);

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->proposed_review_date";
    }

    public function link($text = false)
    {
        return parent::link($text);
    }

    public function getDocuments()
    {
        $db = new mydb(REMOTE);

        $db->query("select * from myps.document_storage where object = 'OngoingService' and object_id = " . $this->id() . "
                    union all
                    select document_storage.* from myps.document_storage
                    inner join myps.review_contact_attempts on document_storage.object_id = review_contact_attempts.id
                    where object = 'ReviewAttempt'
                    and os_id = " . $this->id());

        $files_array = [];

        while($file = $db->next(MYSQLI_ASSOC)){
            $file = new DocumentStorage($file['id'], true);

            $files_array[] = $file;
        }

        if (!empty($files_array)) {
            return $files_array;
        }

        return "No files have been uploaded against this review cycle";
    }

    public function getNotes()
    {
        if (! empty($this->reason_declined())) {
          return $this->reason_declined;
        }

        if (! empty($this->post_meeting_notes())) {
            return $this->post_meeting_notes;
        }

        return false;
    }

    public function getAttempts()
    {
        $s = new Search(new ReviewAttempt());

        $s->eq("ongoingService", $this->id());
        $s->eq("deletedwhen", null);

        $attempts_array = [];

        while($ra = $s->next(MYSQLI_ASSOC)){
            $attempts_array[] = $ra;
        }

        if(!empty($attempts_array)){
            return $attempts_array;
        }

        return "No attempts recorded against this review cycle";
    }

}