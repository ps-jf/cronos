<?php

/**
 * Maps a specific policy from tblpolicy.
 *
 *
 * @package prophet.objects.policy
 * @author daniel.ness
 *
 */

class DedupePolicy extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tbldedupepol";

    public function __construct($id = false, $auto_get = false)
    {
        $this->deDupeID = Field::factory("deDupeID", Field::PRIMARY_KEY);

        $this->newPolicyID = Sub::factory("Policy", "newPolicyID")
            ->set_var(Field::REQUIRED, true);

        $this->oldPolicyID = Field::factory('oldPolicyID');

        $this->number = Field::factory('oldPolicyNum')
            ->set_var(Field::REQUIRED, true);

        $this->issuer = Sub::factory("Issuer", "issuerID")
            ->set_var(Field::REQUIRED, true);

        $this->client = Sub::factory("Client", "clientID")
            ->set_var(Field::REQUIRED, true);

        $this->type = Sub::factory("PolicyType", "policyTypeID")
            ->set_var(Field::REQUIRED, true);

        $this->status = Choice::factory("Status")
            ->setSource(POLICY_STATUS_TBL, "ID", "Field1")
            ->set(2);

        $this->submitted = Date::factory("policySubmitted")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(date(Date::ISO_8601));

        $this->transferred = Date::factory("dateservtrfd")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->transaway = Date::factory("transferred_away")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->mandate_stored = Field::factory("policyMandateStored");

        $this->addedby = Sub::factory("User", "addedby")
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedWhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        // 1:Prophet 2:Mandate 3:Commission 4:import 5: Bulk transfer
        $this->addedhow = Field::factory("added_how")
            ->set_var(Field::TYPE, Field::INTEGER)
            ->set(1);

        $this->updatedwhen = Date::factory("update_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->updatedby = Sub::factory("User", "update_by")
            ->set(User::get_default_instance("id"));

        $this->bulk = Boolean::factory("policyBatched")
            ->set_values(1, 0)
            ->set(1);

        $this->maturity = Date::factory("policyMaturity")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->mandate = Sub::factory("Mandate", "mandate_id")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->next_chase = Date::factory("next_chase_date")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->agency_id = Field::factory('agency_id');

        $this->agency_mandate = Boolean::factory("agency_mandate")
            ->set_values(1, 0)
            ->set(0);

        $this->vatable = Boolean::factory("vatable")
            ->set_values(1, 0)
            ->set(0);

        $this->ac_applied = Boolean::factory("ac_applied")
            ->set_values(1, 0)
            ->set(0);

        $this->order = ["number"];

        $this->archived = Date::factory("archived")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->ac_percent = Field::factory("ac_percent");

        $this->ac_exempt = Boolean::factory("ac_exempt")
            ->set_values(1, 0)
            ->set(0);

        $this->owner = Choice::factory("owner")
            ->push("1", "Client One Only")
            ->push("2", "Client Two Only")
            ->push("3", "Joint Policy")
            ->push("4", "Trustee")
            ->push("5", "Scheme")
            ->push("6", "Unknown");

        $this->dc_exempt = Boolean::factory("dc_exempt")
            ->set_values(1, 0)
            ->set(0);

        $this->term = Field::factory('term');

        $this->rate = Field::factory("rate")
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->paysincome = Boolean::factory("pays_income");

        $this->original_investment = Field::factory('original_investment')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->premium = Field::factory('premium')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->pay_frequency = Choice::factory("frequency")
            ->push("1", "Monthly")
            ->push("2", "Quarterly")
            ->push("3", "Biannually")
            ->push("4", "Yearly")
            ->push("5", "Biennially");

        $this->start_date = Date::factory("start_date")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->ongoing_fee = Field::factory('ongoing_fee');

        // exempt from portfolio reports
        $this->pr_exempt = Boolean::factory("pr_exempt")
            ->set_values(1, 0)
            ->set(0);

        $this->selling_company = Field::factory("sellingCompany");

        $this->sum_assured = Field::factory("sum_assured")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->dfm = Boolean::factory("dfm_policy")
            ->set(0);

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $auto_get);
    }


    public function __toString()
    {
        return "$this->number";
    }
}
