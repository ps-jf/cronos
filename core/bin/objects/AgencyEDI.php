<?php
/**
 * Maps a specific agency used by the EDI system
 *
 * This object is not a normal agency, they are used only in the EDI system.
 * They are displayed via the issuer profile, but not used anywhere else throughout
 * the application
 *
 *
 * @package prophet.objects.agency
 * @author kevin.dorrian
 *
 */

class AgencyEDI extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblagency_codes";

    /**
     * Standard constructor - see long description.
     *
     *
     * Calls parent, but first includes a few extra fields pertinent to agencies
     * This object refers ONLY to the agencies in use in the EDI system
     * They are used to map unknown policies to partners
     *
     * @package prophet.objects.constructors
     * @author daniel.ness
     *
     */

    public function __construct()
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->code = Field::factory("agency");

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::PARTNER_REF, true);

        $this->issuer = Sub::factory("Issuer", "issuerID");

        $this->is_main = Boolean::factory("main")
            ->set_values("-1", "0");

        $this->added_by = Sub::factory("User", "addedBy")
            ->set(User::get_default_instance("id"));

        call_user_func_array(array("parent", "__construct"), func_get_args());
    }

    /**
     * Standard toString function - see long description
     *
     *
     * An agency is defined by its code, so we should return that when printing
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     */
    public function __toString()
    {
        return "$this->code";
    }
}
