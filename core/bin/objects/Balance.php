<?php
/**
 * Maps a partners account balance
 *
 * After every commssion run we update the partners account balance due
 * to rollovers, commission holds etc. This maps them from the database
 *
 * @package prophet.objects.finance
 * @author daniel.ness
 *
 */

class Balance extends DatabaseObject
{

    const DB_NAME = DATA_DB;
    const TABLE = "tblbalancessql";

    /**
     * Standard constructor - see long description.
     *
     *
     * Calls parent, but first includes a few extra fields pertinent to balances
     *
     * @package prophet.objects.constructors
     * @author daniel.ness
     *
     */

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "PartnerID");

        $this->date = Date::factory("DateStamp")
            ->set_var(Date::FORMAT, Date::ISO_8601);

        $this->opening_balance = Field::factory("OpenBalance")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->transferred_this_month = Field::factory("Trxthismnth")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->subtotal = Field::factory("SubTotal")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->paid = Field::factory("Paymntsthismnth")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->closing_balance = Field::factory("ClosingBalance")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->order = array(array("date", "DESC"));

        parent::__construct($id, $auto_get);
    }

    /**
     * Standard toString function - see long description
     *
     *
     * Blank for just now, what defines a balance if we want to print it magically??
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     */

    public function __toString()
    {
        return "";
    }
}
