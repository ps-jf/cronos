<?php

use Carbon\Carbon;

class IncomeHold extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "income_on_hold";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object_type = Field::factory("object_type");

        $this->object_index = Field::factory("object_index");

        $this->held_when = Date::factory('held_when')
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::BLOCK_UPDATE, true)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(date(Date::ISO_8601));

        $this->held_by = Sub::factory("User", "held_by")
            ->set_var(Sub::BLOCK_UPDATE, true)
            ->set(User::get_default_instance('id'));

        $this->reason = Choice::factory("reason")
            ->set_var(Field::DISPLAY_NAME, "reason")
            ->setSource("income_hold_reason", "id", "reason");

        $this->release_when = Date::factory('release_when')
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::BLOCK_INSERT, true)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(date(Date::ISO_8601));

        $this->release_by = Sub::factory("User", "release_by")
            ->set_var(Sub::BLOCK_INSERT, true)
            ->set(User::get_default_instance('id'));

        $this->release_amount = Field::factory("release_amount")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    function __toString()
    {
        return "".$this->holdReason()."";
    }

    public function holdIncome($client_id)
    {
        $client = new Client($client_id, true);

        $ih = new IncomeHold();
        $ih->object_type('Client');
        $ih->object_index($client_id);
        $ih->reason(intval($_GET['reason']));

        if ($ih->save()) {

            $policy_array = [];

            // get all policies belonging to client
            $search = new Search(new Policy());
            $search->eq('client', $client_id);
            while ($x = $search->next(MYSQLI_OBJECT)) {
                $policy_array[] = $x->id();
            }

            if (!empty($policy_array)) {
                $commission_rows = $save_yes = 0;
                // check if any of the policies have pending AC ongoing/Client Fees
                foreach ($policy_array as $policy_id) {
                    $search_p = new Search(new PipelineEntry());
                    $search_p->eq('policy', $policy_id);
                    $search_p->add_or([
                        $search_p->eq("type", 8, true, 0),
                        $search_p->eq("type", 10, true, 0)
                    ]);

                    // if so, place income on hold
                    while ($y = $search_p->next(MYSQLI_OBJECT)) {
                        $commission_rows++;
                        $y->on_hold(1);
                        if ($y->save()) {
                            $save_yes++;
                        }
                    }
                }
                ($save_yes === $commission_rows) ?
                    $feedback = "Income placed on hold" :
                    $feedback = "Income hold recorded, tblcommission not updated, please contact IT.";
            } else {
                $json['feedback'] = "Something went wrong, please try again or call IT.";
                $json['error'] = true;
            }

            $reason = new IncomeHoldReason($_GET['reason'], true);

            $n = new Notes();
            $n->object_type("Client");
            $n->object_id($client_id);
            $n->subject("Income Hold");
            $n->added_when(time());
            ($_GET['reason'] == 3 && $_GET['description'] != "" ) ?
                $reason_text = "Income for ".$client." placed on hold: ".$reason->reason(). ", ".$_GET['description'] :
                $reason_text = "Income for ".$client." placed on hold: ".$reason->reason();
            $n->text($reason_text);
            $n->note_type("note");
            $n->save();

            // check if client has income already in the process of being turned off
            $db = new mydb();

            $query = "select * 
                      from workflow
                      where
                        (object = 'Client' AND object_index = " . $client_id . ")
                        or
                        (object = 'Policy' AND object_index in (select policyID from tblpolicy where clientID = " . $client_id . "))
                      and step = 181";

            $db->query($query);

            if(empty($db->next(MYSQLI_ASSOC))){
                // no open wf tickets at "Ensure Fee has Stopped", lets proceed
                // add action required item
                $ar = new ActionRequired();
                $ar->subject_id(21); // Client Income on Hold
                $ar->object("Client");
                $ar->object_id($client_id);
                $ar->partner($client->partner->id());
                $ar->date_due(Carbon::now()->addWeek(2)->format('Y-m-d'));
                if ($ar->save()) {
                    $json['ar_id'] = $ar->id();
                    $arm = new ActionRequiredMessage();
                    $arm->action_required_id($ar->id());

                    $message = "Dear ".$client->partner.",

".$reason_text."
                
Kind regards,
Policy Services Ltd";
                    $arm->message($message);
                    $arm->save();
                }
            }

        } else {
            $feedback = "Income hold recorded.";
        }

        return $feedback;
    }

    public function releaseIncome($client_id) {

        $client = new Client($client_id, true);

        // get active income hold object
        $search = new Search(new IncomeHold());
        $search->eq('object_type', 'Client');
        $search->eq('object_index', $client_id);
        $search->eq('release_when', NULL);

        if ($x = $search->next(MYSQLI_OBJECT)) {
            $ih = $x;
            // get total amount of income currently held
            $amount = IncomeHold::totalIncomeHeld($client_id);

            // close off income hold object
            $ih->release_by(User::get_default_instance('id'));
            $ih->release_when(date(Date::ISO_8601));
            $ih->release_amount($amount);

            if ($ih->save()) {
                $policy_array = [];

                // get all policies belonging to client
                $search = new Search(new Policy());
                $search->eq('client', $client_id);
                while ($x = $search->next(MYSQLI_OBJECT)) {
                    $policy_array[] = $x->id();
                }

                if (!empty($policy_array)) {
                    $commission_rows = $save_yes = 0;
                    // check if any of the policies have pending AC ongoing/Client Fees
                    foreach ($policy_array as $policy_id) {
                        $search_p = new Search(new PipelineEntry());
                        $search_p->eq('policy', $policy_id);

                        // if so, release hold
                        while ($y = $search_p->next(MYSQLI_OBJECT)) {
                            $commission_rows++;
                            $y->on_hold(0);
                            if ($y->save()) {
                                $save_yes++;
                            }
                        }
                    }

                    ($save_yes === $commission_rows) ?
                        $feedback = "Income hold released" :
                        $feedback = "Income release recorded, tblcommission not updated, please contact IT.";
                } else {
                    $feedback = "Income release recorded.";
                }

                $n = new Notes();
                $n->object_type("Client");
                $n->object_id($client_id);
                $n->subject("Income hold - Released");
                $n->added_when(time());
                $n->text("Income was taken off hold. Amount: (". "&pound; " . number_format((float)$amount, 2).")");
                $n->note_type("note");
                $n->save();

                $search = new Search(new ActionRequired());
                $search->eq('subject_id', 21);
                $search->eq('object', 'Client');
                $search->eq('object_id', $client_id);
                $search->eq('partnerID', $client->partner->id());
                $search->eq('completed_by', NULL);

                while ($ar = $search->next(MYSQLI_OBJECT)) {

                    // add action required message

                        $arm = new ActionRequiredMessage();
                        $arm->action_required_id($ar->id());

                        $message = "Dear ".$client->partner.",

Income has now been release for ". $client.".
                
Kind regards,
Policy Services Ltd";
                        $arm->message($message);
                        $arm->save();

                    $ar->completed_when(time());
                    $ar->completed_by(User::get_default_instance('myps_user_id'));
                    $ar->save();
                }
            } else {
                $feedback = "Something went wrong, please try again or call IT.";
            }
        }
        return $feedback;
    }

    public function holdReason()
    {
        return new IncomeHoldReason($this->reason(), true);
    }

    public static function totalIncomeHeld($client_id)
    {
        $policy_array = [];
        $held_total = 0;

        // get all policies belonging to client
        $search = new Search(new Policy());
        $search->eq('client', $client_id);
        while ($x = $search->next(MYSQLI_OBJECT)) {
            $policy_array[] = $x->id();
        }

        if (!empty($policy_array)) {
            // check if any of the policies have pending AC ongoing/Client Fees
            foreach ($policy_array as $policy_id) {
                $search_p = new Search(new PipelineEntry());
                $search_p->eq('policy', $policy_id);
                $search_p->eq('on_hold', 1);

                // if so, sum total
                while ($y = $search_p->next(MYSQLI_OBJECT)) {
                    $held_total += $y->amount();
                }
            }
        }

        return $held_total;
    }

}
