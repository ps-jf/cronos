<?php

class Valuation extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "valuation";

    const SYMBOLS = [
        "GBP" => "£",
        "USD" => "$",
        "EUR" => "€",
    ];

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object_id = Field::factory("object_id")
            ->set_var(Field::REQUIRED, true);

        $this->value = Field::factory('value');

        $this->value_at = Date::factory('value_at')
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->added_by = Sub::factory("User", "added_by")
            ->set(User::get_default_instance("id"));

        $this->added_when = Date::factory('added_when')
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->comments = Field::factory('comments');

        $this->object_type = Field::factory("object_type")
            ->set_var(Field::REQUIRED, true);

        $this->currency = Choice::factory("currency")
            ->push("GBP", "GBP (£)")
            ->push("USD", "USD ($)")
            ->push("EUR", "EUR (€)")
            ->set("GBP");

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->policy ( " . $this->getValue() . " )";
    }

    public function getValue()
    {
        return Valuation::SYMBOLS[$this->currency()] . " " . $this->value;
    }
}
