<?php

class MandateTrustee extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "mandate_trustee";

    /**
     * MandateTrustee constructor.
     * @param bool $id
     * @param bool $auto_get
     */
    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->trustee = Sub::factory("Trustee", "trustee_id")
            ->set_var(Field::REQUIRED, true);

        $this->client = Sub::factory("Client", "client_id")
            ->set_var(Field::REQUIRED, true);

        $this->policy = Sub::factory("Policy", "policy_id")
            ->set_var(Field::REQUIRED, true);

        $this->mandate = Sub::factory("Mandate", "mandate_id")
            ->set_var(Field::REQUIRED, true);

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(date('Y-m-d H:i:s'));

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }

    public function link($text = false)
    {
        $link = "<a target='_blank' href='pages/mandate/?view=file&do=view&id=" . $this->mandate() . "&filename=M" . $this->mandate() . ".pdf'>
            M" . $this->mandate() . ".pdf
        </a>";

        return $link;
    }
}
