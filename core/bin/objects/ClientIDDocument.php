<?php


class ClientIDDocument extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "client_id_documents";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "client_id")
            ->set_var(Field::REQUIRED, true);

        $this->index = Field::factory("client_index")
            ->set_var(Field::REQUIRED, true);

        $this->correspondence = Sub::factory("Correspondence", "corr_id")
            ->set_var(Field::REQUIRED, true);

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }
}