<?php

class Staff extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "staff";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->firstname = Field::factory("firstname")
            ->set_var(Field::REQUIRED, true);

        $this->surname = Field::factory("surname")
            ->set_var(Field::REQUIRED, true);

        $this->phone = Field::factory("phone")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->email = Field::factory("email")
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS)
            ->set_var(Field::MAX_LENGTH, 320);

        $this->role = Field::factory("role")
            ->set_var(Field::REQUIRED, true);

        $this->active = Boolean::factory("active");

        $this->myps_id = Field::factory("myps_id")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->updatedwhen = Date::factory("update_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "update_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->unipass = Boolean::factory("unipass");

        parent::__construct($id, $autoget);
    }

    public function on_create($post = array())
    {
        // Add PracticeStaffEntry for this Staff + Practice

        if (array_key_exists("practice", $post)) {
            // verify that Practice exists
            $practice = new Practice($post["practice"]);
            if (!$practice->get()) {
                throw new Exception("Practice \"$post[practice]\" not found");
            }

            $pse = new PracticeStaffEntry;
            $pse->staff($this->id());
            $pse->practice($practice->id());
            $pse->save();
        }
    }

    public function __toString()
    {
        // PA McPAson
        return trim("$this->firstname $this->surname");
    }
}
