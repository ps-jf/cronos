<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 08/11/2019
 * Time: 14:32
 */

class GoodIdea extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "good_ideas";

    public function __construct($id = false, $auto_get = false)
    {
        $this->SNAPSHOT_LOG = true;

        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->subject = Field::factory("subject")
            ->set_var(Field::REQUIRED, true);

        $this->description = Field::factory("description")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->implement = Date::factory("implement")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->feedback = Field::factory("feedback")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->implemented = Date::factory("implemented")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->paid = Date::factory("paid_date")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->notes = Field::factory("notes")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->jira_id = Field::factory("jira_id");

        $this->allocated_to = Sub::factory("User", "allocated_to");

        $this->due_date = Date::factory("due_date")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->subject";
    }

    public function save($return_query = false)
    {
        if($this->id()){
            // object is being updated

            // get the old instance before the update
            $gi = new GoodIdea($this->id(), true);

            if($gi->allocated_to() != $this->allocated_to()){
                // allocated person has been updated, send them an email
                $subject = "Good Idea Assigned to You";
                $sender = [
                    'name' => "IT",
                    'email' => 'it@policyservices.co.uk',
                ];

                $recipient = [
                    [
                        'address' => [
                            'name' => $this->allocated_to->staff_name(),
                            'email' => $this->allocated_to->email()
                        ]
                    ]
                ];

                $content_string = "<p>
                                    Good Idea '" . $this->subject() . "' has been assigned to you to implement.
                                    </p>
                                    <p>
                                    Please visit the 'Assigned To Me' section of the good ideas system for more details.
                                    </p>";

                $body = json_encode([
                    "SUBJECT" => $subject,
                    "FROM_NAME" => "IT",
                    "SENDER" => "it@policyservices.co.uk",
                    "REPLY_TO" => "it@policyservices.co.uk",
                    "ADDRESSEE" => "Hi " . $this->allocated_to->staff_name(),
                    "CONTENT_STRING" => $content_string
                ]);

                sendSparkEmail($recipient, $subject, 'generic-staff', $body, $sender);
            }
        }

        return parent::save($return_query);
    }

    public function on_create($POST_data = false)
    {
        $recipients[0]['address']['email'] = "goodidea@policyservices.co.uk";
//        $recipients[0]['address']['email'] = "sean.ross@policyservices.co.uk";

        // team leader/heads of mapping
        $map = [
            29 => "kevin.dorrian@policyservices.co.uk",             // avril
            44 => "kayleigh.dorrian@policyservices.co.uk",          // blair
            45 => "kevin.dorrian@policyservices.co.uk",             // diane
            87 => "kevin.dorrian@policyservices.co.uk",             // lewis
            98 => "kayleigh.dorrian@policyservices.co.uk",          // louise gibson
            99 => "avril.braes@policyservices.co.uk",               // lisa
            116 => "louise.gibson@policyservices.co.uk",            // michelle
            141 => "michelle.fraser@policyservices.co.uk",          // catherine
            185 => "louise.gibson@policyservices.co.uk",            // louise mcgillivray
            192 => "louise.gibson@policyservices.co.uk",            // sarah ireland
            203 => "avril.braes@policyservices.co.uk",              // charmaine
        ];

        if (array_key_exists(User::get_default_instance("id"), $map)){
            $recipients[1]['address']['email'] = $map[User::get_default_instance("id")];
        } elseif (in_array(User::get_default_instance("department"), [14, 15])) {
            $recipients[1]['address']['email'] = "michelle.fraser@policyservices.co.uk";
        } elseif (user::get_default_instance("department") == 1){
            $recipients[1]['address']['email'] = "charmaine.quinn@policyservices.co.uk";
        } else {
            $s = new Search(new User());

            $s->eq("department", User::get_default_instance("department"));
            $s->eq("team_leader", 1);
            $s->eq("active", 1);

            if ($u = $s->next(MYSQLI_ASSOC)){
                $recipients[1]['address']['email'] = $u->email();
            }
        }

        $from = [
            'name' => 'Policy Services - IT',
            'email' => 'it@policyservices.co.uk',
        ];

        $local_template['html'] = new Template("sparkpost/emailTemplates/goodidea/submitted.html", ["gi" => $this]);

        sendSparkEmail($recipients, $this->subject(), '', false, $from, [], $local_template);

        parent::on_create($POST_data); // TODO: Change the autogenerated stub
    }

    public function jira()
    {
        try{
            if ($this->jira_id()){
                $base64auth = base64_encode(JIRA_USERNAME . ":" . JIRA_PASSWORD);

                $curl = curl_init();

                curl_setopt($curl, CURLOPT_URL, "https://policyservices.atlassian.net/rest/api/2/issue/" . $this->jira_id());
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_HTTPHEADER, [
                    "Content-Type: application/json",
                    "Authorization: Basic " . $base64auth
                ]);

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    throw new Exception("cURL Error #:" . $err);
                } else {
                    $response = json_decode($response);

                    return $response;
                }
            } else {
                return false;
            }
        } catch (Exception $e){
            return false;
        }
    }

    public static function assigned_counter()
    {
        $s = new Search(new GoodIdea());

        $s->eq("implemented", null);
        $s->eq("allocated_to", User::get_default_instance("id"));
        $s->nt("implement", -1);

        return $s->count();
    }
}