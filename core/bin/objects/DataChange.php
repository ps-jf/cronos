<?php

class DataChange extends DatabaseObject
{
    const DB_NAME = 'myps';
    const TABLE = "data_change";

    const COMMENT_CLIENT_EDIT = "Updated client details";
    const COMMENT_CLIENT_ADDRESS_INPUT = "Client Address Input";
    const COMMENT_CLIENT_TRANSFER = "Client Transfer";
    const COMMENT_POLICY_MERGE = "Policy Merge";
    const COMMENT_CLIENT_MERGE = "Client Merge";

    public function __construct($id = null, $auto_get = null)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object_type = Field::factory("object_type");

        $this->object_id = Field::factory("object_id")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->data = JSON::factory("data");

        $this->proposed_by = Sub::factory("WebsiteUser", "proposed_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->proposed_time = Date::factory("proposed_time")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->committed_by = Sub::factory("User", "committed_by");

        $this->committed_time = Date::factory("committed_time")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->comments = Field::factory("comments")
            ->set_var(Field::MAX_LENGTH, 255);

        $this->allocated = Field::factory("allocated");

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function problemEmail($object, $comment)
    {
        $recipient = [];

        $emailTo = $this->proposed_by->email_address();

        $recipient[] = [
            "address" => [
                "email" => $emailTo
            ]
        ];

        $sender = ['name' => 'Policy Services', 'email' => 'it@policyservices.co.uk'];
        $user = User::get_default_instance();

        $id = null;

        $s = new Search(new DataChange());
        $s->eq('object_type', $this->object_type());
        $s->eq('proposed_time', $this->proposed_time());
        $s->eq('proposed_by', $this->proposed_by());

        $object_type = ucfirst($object);
        $objects = "<ul>";
        while ($data_change = $s->next(MYSQLI_OBJECT)) {
            if (empty($id)){
                $id = $data_change->id();
            }

            $obj = new $object_type($data_change->object_id(), true);
            $objects .= "<li>" . $obj . " (" . $obj->id() . ")</li>";
        }
        $objects .= "</ul>";

        $object = pluralize(2, $object);

        $data = json_encode([
            "OBJECT" => $object,
            "X" => $objects,
            "REASON" => $comment,
            "SENDER" => $user->staff_name(),
            "DEPARTMENT" => "IT Department",
            "MERGE_ID" => $id,
        ]);

        return sendSparkEmail($recipient, 'Policy Services Merge Issue', 'merge-issue', $data, $sender);
    }

    public function successEmail($objects)
    {
        $object = ucfirst($this->object_type());
        $object_plural = pluralize(2, $object);

        $recipient = [];

        $emailTo = $this->proposed_by->email_address();

        $recipient[] = [
            "address" => [
                "email" => $emailTo
            ]
        ];

        $sender = ['name' => 'Policy Services', 'email' => 'it@policyservices.co.uk'];

        $objects_text = "<ul>";

        foreach($objects as $object){
            $objects_text .= "<li>$object (" . $object->id() . ")</li>";
        }

        $objects_text .= "</ul>";

        $content_string = "Your request to merge the following " . $object_plural . " is now complete:" . $objects_text;

        if(strtolower($this->object_type()) == "client"){
            $content_string .= "Please check the new client record for any policies which may also require merging.";
        }

        $data = json_encode([
            "SUBJECT" => "Policy Services Merge Success",
            "FROM_NAME" => $sender['name'],
            "SENDER" => $sender['email'],
            "REPLY_TO" => $sender['email'],
            "ADDRESSEE" => $this->proposed_by->user_name(),
            "CONTENT_STRING" => $content_string
        ]);

        return sendSparkEmail($recipient, 'Policy Services Merge Success', 'generic-staff', $data, $sender);
    }
}
