<?php

class AgencyMandateChase extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tbl_agency_mandate_chase";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("ID", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partner")
            ->set_var(Field::TYPE, Field::INTEGER)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->issuer = Sub::factory("Issuer", "issuer")
            ->set_var(Field::TYPE, Field::INTEGER)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->letter = Choice::factory("letter")
            ->push("1", "Letter One")
            ->push("2", "Letter Two")
            ->set_var(Field::DISPLAY_NAME, "Letter");

        $this->subject = Field::factory("subject")
            ->set_var(Field::MAX_LENGTH, 255);

        $this->message = Field::factory("message")
            ->set_var(Field::MAX_LENGTH, 255);

        $this->send_to = Field::factory("send_to")
            ->set_var(Field::MAX_LENGTH, 255);

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->sent = Boolean::factory("sent")
            ->set_values(1, 0)
            ->set(0);

        $this->sent_time = Date::factory("sent_time")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->send_to";
    }
}
