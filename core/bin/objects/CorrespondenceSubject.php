<?php

class CorrespondenceSubject extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblcorrespondencesubjects";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->subject = Field::factory("subject");

        $this->archived = Boolean::factory("archived");

        $this->search = Boolean::factory("search");

        $this->process = Field::factory("processing");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->subject";
    }
}