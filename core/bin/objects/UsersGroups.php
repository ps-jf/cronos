<?php

class UsersGroups extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "users_groups";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user = Sub::factory("User", "user_id");

        $this->user_group = Sub::factory("Groups", "group_id");

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->user $this->user_group";
    }

    public function check_user_group($group)
    {
        $db = new mydb();
        $db->query("SELECT * FROM prophet.users_groups WHERE group_id = " . $group . " AND user_id = " . User::get_default_instance('id'));
        ($d = $db->next(MYSQLI_ASSOC)) ? $group = true : $group = false;

        return $group;
    }
}
