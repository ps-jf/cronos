<?php

class LifeEventTracking extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "life_events_tracking";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->life_event = Sub::factory("LifeEvent", "life_event_id");

        $this->date = Date::factory("date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->tracker = Field::factory("tracker");

        $this->direction = Choice::factory("direction")
            ->push_array(["Sent Out", "Received"]);

        $this->entity = Field::factory("entity");

        $this->issuer = Sub::factory("Issuer", "issuer_id");

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(date('Y-m-d H:i:s'));

        $this->updated_by = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(date('Y-m-d H:i:s'));

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }

    public function has_workflow()
    {
        $s = new Search(new Workflow);
        $s->eq("_object", "LifeEventTracking");
        $s->eq("index", $this->id());

        if ($wf = $s->next(MYSQLI_ASSOC)) {
            return $wf->link('<img src="/lib/images/wf.png" alt="workf">');
        }

        return false;
    }
}
