<?php

class VirtueReview extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "virtue_review";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->policy = Sub::factory("policyID", "policyID")
            ->set_var(Field::REQUIRED, true);

        $this->partner = Sub::factory("partnerID", "partnerID")
            ->set_var(Field::REQUIRED, true);

        $this->review = Choice::factory("review")
            ->push("1", "Annual")
            ->push("2", "6 Monthly")
            ->push("3", "Quarterly")
            ->push("4", "Adhoc");

        $this->date_review = Date::factory("dateReview")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->reason_late = Field::factory("reasonLate")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE, "Reason_Late");

        $this->date_portfolio = Date::factory("datePortfolio")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->date_review_when = Date::factory("dateReviewWhen")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->location = Choice::factory("reviewLocation")
            ->push("1", "Home")
            ->push("2", "Office")
            ->push("3", "Other");

        $this->otherlocation = Field::factory("otherLocation")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE, "Other_Location");

        $this->pre_review_letter = Date::factory("preReviewLetter")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->post_review_letter = Date::factory("postReviewLetter")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->suitability_returned = Date::factory("suitabilityReturned")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->client_pre_review = Boolean::factory("clientPreReview")
            ->set_values(1, 0);

        $this->client_post_review = Boolean::factory("clientPostReview")
            ->set_values(1, 0);

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }

    public function on_create($post = false)
    {
        //On create - Determine whether partner ID is part of virtue group or not
        //On create - Assign ticket to appropriate group/person
        //On create =  Determine reminder time  due date - 28 days
        //On create - Build up array for sending through the workflow object "workflow_add"
        //On create - Empties buffer of json return as two returns were being returned!

        try {
            $dueDate = $this->date_review();
            $partnerID = $this->partner();

            $virtuePartners = [];
            $s = new Search(new PracticeStaffEntry);
            $s->eq("practice", 2894);

            while ($v = $s->next(MYSQLI_ASSOC)) {
                $virtuePartners[] = $v->partner();
            }

            if (in_array($partnerID, $virtuePartners)) {
                $assigned_group = "10";
                $assigned = false;
            } else {
                $assigned_group = false;
                if ($partnerID == 3513) {
                    $assigned = array(109);
                } elseif ($partnerID == 1300) {
                    $assigned = array(22);
                } elseif ($partnerID == 2540) {
                    $assigned = array(45);
                }
            }

            $reminderTime = date("Y-m-d", strtotime(date("Y-m-d", strtotime($dueDate)) . " -28 day"));

            $arr_args = array('process' => '7',
                'step' => '10',
                'priority' => '2',
                'due' => strtotime($reminderTime),
                'desc' => 'Reminder for virtue review',
                '_object' => 'Policy',
                'index' => $this->policy()
            );

            $workflow = new Workflow();
            $workflow->workflow_add($arr_args, $assigned, $assigned_group);


            //Drops the json return - Don't worry, we have the try catch to report errors
            ob_end_clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function on_update($post = false)
    {
        //On update - change the reminder time to the new due date - 28 days
        //Apply the updated when and updated by fields with appropriate data

        try {
            $dueDate = $this->date_review();

            $reminderTime = date("Y-m-d", strtotime(date("Y-m-d", strtotime($dueDate)) . " -28 day"));


            $updatedBy = User::get_default_instance('id');

            $workflowObj = new Search(new Workflow);
            $workflowObj->eq("_object", "Policy");
            $workflowObj->eq("step", "10");
            $workflowObj->eq("desc", "Reminder for virtue review");
            $workflowObj->eq("index", $this->policy());

            while ($a = $workflowObj->next()) {
                $obj = new Workflow($a->id(), true);
                $obj->due(strtotime($reminderTime));
                $obj->desc('Reminder for virtue review');
                $obj->updatedby($updatedBy);
                $obj->updatedwhen(time());

                $obj->save();
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
