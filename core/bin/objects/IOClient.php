<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 17/10/2019
 * Time: 15:15
 */

class IOClient extends DatabaseObject
{
    const DB_NAME = IO_DB;
    const TABLE = "clients";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("io_id", Field::PRIMARY_KEY);

        $this->prophet_id = Field::factory("prophet_id");

        $this->index = Field::factory("prophet_index");

        $this->database = IO;

        parent::__construct($id, $auto_get);
    }
}