<?php
/**
 * A variety of different settings used in bulk transfer submission.
 *
 * Here we hold paths to files, company names, variables that allow
 * the transfer request report to be built.
 *
 *
 * @package prophet.admin.acquisition
 * @author kevin.dorrian
 */

class BulkDetails extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "bulk_details";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("ID", Field::PRIMARY_KEY);
        $this->ifaauth = Field::factory("IFAauthat");
        $this->netauth = Field::factory("Networkauthorisationat");
        $this->sub_agency = Field::factory("Sub Agency To Be Called");
        $this->comm_paid = Field::factory("Income To Be Paid To");
        $this->comm_sent = Field::factory("Income Statements To Be Sent To");
        $this->corr_sent = Field::factory("Correspondence To Be Sent to");
        $this->old_name = Field::factory("oldbusname");
        $this->partnerid = Field::factory("PartnerID");
        $this->nov_away = Field::factory("novationAwayat");
        $this->ifa = Field::factory("ifa_auth");
        $this->net = Field::factory("network_auth");
        $this->subagency = Field::factory("agency_called");
        $this->novation_away = Field::factory("novation_away");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        if (!$this->loaded) {
            $this->get();
        }
        return "$this->id";
    }
}
