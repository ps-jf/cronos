<?php


class LogLogin extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "log_logins";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user = Sub::factory("User", "user_id")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->time = Date::factory("datime")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->direction = Choice::factory("direction")
            ->push("LOGIN", "LOGIN")
            ->push("LUNCH", "LUNCH")
            ->push("LUNCH", "LUNCH")
            ->push("BREAK", "BREAK");

        parent::__construct($id, $auto_get);
    }
}