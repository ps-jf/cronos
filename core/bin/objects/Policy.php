<?php

/**
 * Maps a specific policy from tblpolicy.
 *
 *
 * @package prophet.objects.policy
 * @author daniel.ness
 *
 */

class Policy extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblpolicy";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("policyID", Field::PRIMARY_KEY);

        $this->number = Field::factory('policyNum')
            ->set_var(Field::REQUIRED, true);

        $this->issuer = Sub::factory("Issuer", "issuerID")
            ->set_var(Field::REQUIRED, true);

        $this->client = Sub::factory("Client", "clientID")
            ->set_var(Field::REQUIRED, true);

        $this->type = Sub::factory("PolicyType", "policyTypeID")
            ->set_var(Field::REQUIRED, true);

        $this->status = Choice::factory("Status")
            ->setSource(POLICY_STATUS_TBL, "ID", "Field1")
            ->set(2);

        $this->submitted = Date::factory("policySubmitted")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(date(Date::ISO_8601));

        $this->transferred = Date::factory("dateservtrfd")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->transaway = Date::factory("transferred_away")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->mandate_stored = Field::factory("policyMandateStored");

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedWhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        // 1:Prophet 2:Mandate 3:Commission 4:import 5: Bulk transfer
        $this->addedhow = Field::factory("added_how")
            ->set_var(Field::TYPE, Field::INTEGER)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(1);

        $this->updatedwhen = Date::factory("update_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "update_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->bulk = Boolean::factory("policyBatched")
            ->set_values(1, 0)
            ->set(1);

        $this->maturity = Date::factory("policyMaturity")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->mandate = Sub::factory("Mandate", "mandate_id")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->next_chase = Date::factory("next_chase_date")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->agency_id = Field::factory('agency_id');

        $this->agency_mandate = Boolean::factory("agency_mandate")
            ->set_values(1, 0)
            ->set(0);

        $this->vatable = Boolean::factory("vatable")
            ->set_values(1, 0)
            ->set(0);

        $this->ac_applied = Boolean::factory("ac_applied")
            ->set_values(1, 0)
            ->set(0);

        $this->order = array("number");

        $this->archived = Date::factory("archived")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->ac_percent = Field::factory("ac_percent");

        $this->ac_exempt = Boolean::factory("ac_exempt")
            ->set_values(1, 0)
            ->set(0);

        $this->owner = Choice::factory("owner")
            ->push("1", "Client One Only")
            ->push("2", "Client Two Only")
            ->push("3", "Joint Policy")
            ->push("4", "Trustee")
            ->push("5", "Scheme")
            ->push("6", "Unknown");

        $this->dc_exempt = Boolean::factory("dc_exempt")
            ->set_values(1, 0)
            ->set(0);

        $this->term = Field::factory('term');

        $this->rate = Field::factory("rate")
            ->set_var(Field::TYPE, Field::PERCENT)
            ->set_var(Field::FORCE_ZERO, true);

        $this->paysincome = Boolean::factory("pays_income");

        $this->original_investment = Field::factory('original_investment')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->premium = Field::factory('premium')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->pay_frequency = Choice::factory("frequency")
            ->push("1", "Monthly")
            ->push("2", "Quarterly")
            ->push("3", "Biannually")
            ->push("4", "Yearly")
            ->push("5", "Biennially");

        $this->start_date = Date::factory("start_date")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->ongoing_fee = Field::factory('ongoing_fee');

        // exempt from portfolio reports
        $this->pr_exempt = Boolean::factory("pr_exempt")
            ->set_values(1, 0)
            ->set(0);

        $this->selling_company = Field::factory("sellingCompany");

        $this->sum_assured = Field::factory("sum_assured")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->dfm = Boolean::factory("dfm_policy")
            ->set(0);

        /**
         *
         * IMPORTANT: UPDATE DEDUPEPOLICY TABLE & OBJECT WHEN ADDING NEW FIELDS TO TBLPOLICY SO WE HAVE A FULL AUDIT
         *
         */

        $this->SNAPSHOT_LOG = true;

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public static function merge_requested($policy)
    {
        $merge_request = false;

        // outstanding merges on this policy?
        $s = new Search(new DataChange());
        $s->eq("object_type", "Policy");
        $s->eq("comments", DataChange::COMMENT_POLICY_MERGE);
        $s->eq("object_id", $policy);
        $s->eq_null("committed_by");
        $s->order_by(array("id", "DESC"));

        while ($c = $s->next(MYSQLI_ASSOC)) {
            $db = new mydb(REMOTE, E_USER_WARNING);
            if (mysqli_connect_errno()) {
                throw new Exception("Can't connect to the mypsaccount server: " . mysqli_connect_errno());
            }
            $q = "SELECT * FROM " . REMOTE_SYS_DB . ".user_info where id = " . $c->proposed_by();
            $db->query($q);

            $user_name = "";
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $user_name = $row['user_name'];
            }

            if ($user_name != "") {
                $by = "by " . ucwords(str_replace(".", " ", $user_name)) . ".";
            } else {
                $by = ".";
            }

            if ($c->committed_by() == "" && $c->committed_time() == "") {
                $merge_request = "<div class='notification static' style='display:block;'>
            This policy was marked for merging on " . date("d/m/Y H:i:s", $c->proposed_time()) . "
            " . $by . " These changes are pending and we try to approve and apply them within 5 days.</div>";
            }
        }
        return $merge_request;
    }

    public static function merge_history($policy)
    {
        $previous_id = false;
        $old_ids = [];

        $db = new mydb();
        $q = "SELECT * FROM tbldedupepol where newPolicyID = " . $policy;
        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            $old_ids[] = "<span class='history-link'>".$row['oldPolicyID']."</span>";
            $previous_id = true;
        }

        $ids = implode(", ", $old_ids);
        $text = "Previous Policy ID's that have been merged into viewing Policy: " . $ids;
        $notification = "<div class='alert alert-info' style='display:block;'>" . $text . "</div>";

        if ($previous_id == true) {
            return $notification;
        }
    }

    public function __toString()
    {
        return "$this->number";
    }

    public function has_workflow()
    {
        $s = new Search(new Workflow);
        $s->eq("_object", "policy");
        $s->eq("index", $this->id());
        $s->add_or(
            array(
                $s->eq("step", 16, true, 0),   // watch

                $s->eq("step", 90, true, 0),   // client address change - client services
                $s->eq("step", 91, true, 0),   // client address change - client services
                $s->eq("step", 92, true, 0),   // client address change - client services

                $s->eq("step", 93, true, 0),   // client address change - acq support
                $s->eq("step", 94, true, 0),   // client address change - acq support
                $s->eq("step", 95, true, 0),   // client address change - acq support

                $s->eq("step", 96, true, 0),   // client address change - new business
                $s->eq("step", 97, true, 0),   // client address change - new business
                $s->eq("step", 98, true, 0),   // client address change - new business

                $s->eq("step", 106, true, 0),  // client address change - IO
                $s->eq("step", 107, true, 0),  // client address change - IO
                $s->eq("step", 108, true, 0),  // client address change - IO

                $s->eq("step", 109, true, 0),  // client address change - Client Relations
                $s->eq("step", 110, true, 0),  // client address change - Client Relations
                $s->eq("step", 111, true, 0),  // client address change - Client Relations

                $s->eq("step", 40, true, 0),   // helpdesk chase - client services
                $s->eq("step", 42, true, 0),   // helpdesk chase - new business
                $s->eq("step", 43, true, 0)    // helpdesk chase - acq support
            )
        );

        $alerts = "";

        while($wf = $s->next(MYSQLI_ASSOC)){
            $alerts .= $wf->link("<div class='alert alert-warning mb-2 block-submit'><i class='fas fa-exclamation-circle'></i> This policy currently has a \"" . $wf->step . "\" ticket open. Please see " . $wf->addedby . " for more information. (Click here to open & acknowledge)</div>");
        }

        echo $alerts;
    }

//    public function outstanding_valuation()
//    {
//        //// Get outstanding Valuation for this Policy, or return false
//        $s = new Search(new Valuation);
//        $s->eq("policy", $this->id());
//        $s->lt("stage", ValuationStage::COMPLETE);
//        $s->limit(1);
//
//        if (($v = $s->next()) !== false) {
//            return $v;
//        }
//
//        return false;
//    }

    public function ioPolicy()
    {
        try{
            $s = new Search(new IOPolicy());

            $s->eq("prophet_id", $this->id());

            if ($s->count() > 1) {
                throw new Exception("More than 1 policy exists on IO for this prophet ID");
            } else {
                if ($p = $s->next(MYSQLI_ASSOC)){
                    return $p;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function has_newbusiness()
    {

        /*
         * -- Get the pending NewBusiness entry for this Policy.
         * @return NewBusiness instance or FALSE
         */


        $s = new Search(new NewBusiness);
        $s->eq("policy", $this->id())
            ->nt("status", 2)
            ->nt("status", 3)
            ->nt("status", 4)
            ->nt("status", 5);

        return (($nb = $s->next()) !== false) ? $nb : false;
    }

    public function has_reviewTicket()
    {
        /*
         * -- Get the pending Virtue review entry for this Policy.
         * @return VirtueReview instance or FALSE
         */
        $s = new Search(new VirtueReview);
        $s->eq("policy", $this->id());

        return (($vr = $s->next()) !== false) ? $vr : false;
    }

    public function on_create($post = false)
    {
        $dfm_providers = [
            1155, // Brewin Dolphin
            11881, // Brooks MacDonald Asset Management
            48, // Cazenove Capital Management Ltd
            11188, // Charles Stanley & Co. Limited
            11765, // City Asset Management PLC
            11816, // Close Brothers
            11144, // GHC Capital Markets Ltd
            11895, // Hawksmoor Investment Management
            1084, // Investec Wealth & Investment
            11581, // Quilter Cheviot Limited
            11760, // Rowan Dartington
            11459, // Seven Investment Management
            228, // Smith & Williamson Unit Trust Managers Limited
            11472, // Tilney Asset Management
            1162, // W H Ireland
            11677, // Whitechurch Securities Ltd
        ];

        // check if dfm providers and set vatable
        if (!$this->vatable() && in_array($this->issuer(), $dfm_providers)){
            $this->vatable(1);
            $this->save();
        }

        if (isset($post["reason"])) {
            try {
                //create new database instance and then add reasoning to tbl_action_reason
                $db = new mydb();
                $q = "INSERT INTO
                         `feebase`.`tbl_action_reason` (`object`, `object_id`, `reason`, `created_by`, `created_when`,`action` )
                         VALUES ('Policy', '" . $this->id() . "', '" . addslashes($post['reason']) . "', '" . $_COOKIE['uid'] . "', '" . time() . "', 'Add')";
                $db->query($q);
            } catch (Exception $e) {
                throw $e;
            }
        }
    }

    public function save($return_query = false)
    {
        if ($this->id()){
            // object is being updated
            $p = new Policy($this->id(), true);

            // p is the unedited version of the policy
            // check for updates
            if (isset($_REQUEST['policy']['issuer'])){
                if ($p->issuer() != $_REQUEST["policy"]["issuer"]){
                    // issuer has changed, check if policy has pending income
                    $s = new Search(new PipelineEntry());
                    $s->eq("policy", $p->id());

                    if ($s->next(MYSQLI_ASSOC)){
                        // policy has pending income, block update
                        throw new Exception("Policy issuer cannot be updated while policy has pipeline income pending");
                    }
                }
            }
        }

        if($this->owner != 5 && $this->client->group_scheme()){
            $this->owner(5);
        }

        return parent::save($return_query);
    }

    public function on_update($post = false)
    {
        $response = [
            "trigger" => null,
            "data" => null,
        ];

        if ($post["status"] == 10 && $this->transaway() == "") {
            try {
                $db = new mydb();
                $q = ("UPDATE " . POLICY_TBL . " set transferred_away = '" . time() . "' WHERE policyid = " . $this->id());
                $db->query($q);
            } catch (Exception $e) {
                throw $e;
            }
        } elseif ($post["status"] == 2 && $this->transferred == "") {
            try {
                $db = new mydb();
                $q = ("UPDATE " . POLICY_TBL . " set dateservtrfd = '" . date("Y-m-d") . "' WHERE policyid = " . $this->id());
                $db->query($q);
            } catch (Exception $e) {
                throw $e;
            }
        }

        // check if client still has live policies
        $s = new Search(new Policy());

        $s->eq("client", $this->client->id());
        $s->eq("status", LIVE_POLICY_STATUS);
        $s->eq("archived", null);

        if($s->count() < 1){
            // check if any policies are at awaiting confirmation (1), mandates required (22), unable to transfer (25)
            // futureproof: this check is kept separate as we may need a policy added date check in the future.
            $s = new Search(new Policy());
            $s->eq("client", $this->client->id());
            $s->eq("status", [1,22,25]);
            $s->eq("archived", null);

            if($s->count() < 1){
                $message = "This client has now been automatically unsegmented from a servicing level due to the client no longer holding any active fee-paying plans.";

                // client has no live policies
                $this->client->unsegment($message);

                $message .= " Therefore this ticket has now been closed.";

                // check if client has open action required tickets for "Urgent: New Client Agreement Required"
                $this->client->endActionRequired($message, [26,29,31,32,33,34]);
                $this->client->endWorkflow($message, [41,188,189,193], true);
            }
        }

        $p = new Policy($this->id(), true);

        if($p->rate() != $post['rate'] && $post['rate'] != $this->client->splPercent()){
            $filename = RATES_DIR . date("Y-m-d") . ".csv";

            if(file_exists($filename)){
                $fp = fopen($filename, 'a');
            } else {
                $fp = fopen($filename, 'w');
                fputcsv($fp, ["Client ID", "Client", "Policy ID", "Policy Num", "Issuer", "Servicing Level", "Servicing Percent", "Policy Rate"]);
            }

            fputcsv($fp, [$this->client->id(), strval($this->client), $this->id(), strval($this), strval($this->issuer), strval($this->client->servicingPropositionLevel(true)), $this->client->splPercent(), $this->rate()]);

            fclose($fp);
        }

        if($p->status() != $post['status'] && in_array($post['status'], [8 ,32])){
            // policy status changed to "8 - Out of Force' or "32 - Surrendered"

            $ar = new ActionReason();

            $ar->object("Policy");
            $ar->object_id($this->id());
            $ar->reason($_REQUEST["status_change_reason"]);
            $ar->action("status");

            $ar->save();

            // check for open action required tickets and alert the user
            $s = new Search(new ActionRequired());

            $s->eq("object", "Policy");
            $s->eq("object_id", $this->id());
            $s->eq("completed_when", null);
            $s->eq("archived", 0);

            if($arq = $s->next(MYSQLI_ASSOC)){
                $response['trigger'] = "alert";
                $response['data'] = "There are open action required tickets against this policy, please check these tickets before leaving the profile.";
            }
        }

        return (object)$response;
    }

    public static function is_vatable($policyid)
    {
        $p = new Policy($policyid, true);

        $vat = $p->vatable();
        return $vat;
    }

    public function has_charging()
    {

        //// Get most recent Adviser Charing rate for this Policy, or return false
        $s = new Search(new AdviserCharging);
        $s->eq("policy", $this->id());
        $s->add_order("id", "DESC");
        $s->limit(1);


        if (($ac = $s->next()) !== false) {
            return $ac->ac_requested();
        }

        return false;
    }

    public function link($text = false)
    {
        $link = parent::link($text);

        //highlight policies that are Archived
        if ($this->archived() != null) {
            $link = preg_replace("/(class='.*?)(')/", "$1 Archived$2", $link);
        }

        return $link;
    }

    public function determine_policy_owner($policy)
    {
        // if we do not have a policy owner set we need to determine one
        if ($policy->owner() == 0) {
            // initialise variables
            $scheme = false;
            $client1 = false;
            $client2 = false;

            // check if policy belongs to a scheme, else try predict owning client
            if ($policy->client->group_scheme() != 0) {
                $scheme = true;
            } else {
                // determine whether or not we have client one
                if (($policy->client->one->forename() and $policy->client->one->forename() != "")
                    or ($policy->client->one->surname() and $policy->client->one->surname() != "")) {
                    $client1 = true;
                }
                // determine whether or not we have client two
                if (($policy->client->two->forename() and $policy->client->two->forename() != "")
                    or ($policy->client->two->surname() and $policy->client->two->surname() != "")) {
                    $client2 = true;
                }
            }

            // depending on client information, set policy owner
            if ($scheme) {
                $owner = 5; // scheme
            } elseif ($client1 && !$client2) {
                $owner = 1; // client 1 only
            } elseif ($client2 && !$client1) {
                $owner = 2; // client 2 only
            } else {
                $owner = 6; // unknown
            }

            return $owner;
        } else {
            // policy owner already set
            return $policy->owner();
        }
    }

    public function hasIncomeHold($id)
    {
        $s = new Search(new PipelineEntry());

        $s->eq('policy', $id);
        $s->eq('on_hold', 1);

        if ($s->next(MYSQLI_ASSOC)){
            return true;
        } else {
            return false;
        }
    }

    public function ar_count(){
        $redisClient = RedisConnection::connect();
        return $redisClient->exists("action_required.Policy." . $this->id());
    }

    public function ar_links(){
        $s = new Search(new ActionRequired());

        $s->eq("object", "Policy");
        $s->eq("object_id", $this->id());
        $s->eq("completed_when", null);
        $s->eq("archived", 0);

        $links = [];

        while($ar = $s->next(MYSQLI_ASSOC)){
            $links[] = $ar->link();
        }

        if (count($links)){
            return implode(", ", $links);
        } else {
            return false;
        }
    }

    public function getOwnerAttribute($value)
    {
        $owner_options = [
            null => "--",
            0 => "--",
            1 => $this->client->one->name(),
            2 => $this->client->two->name(),
            3 => "Joint Policy",
            4 => "Trustee",
            5 => "Scheme",
            6 => "Unknown"
        ];

        return $owner_options[$value];
    }

    public function getOwner($value = 0, $id = "policy[owner]", $responseType = false){
        $html = "<select id='" . $id . "' name='" . $id . "'>";

        $html .= "<option value='0'>--</option>";

        if (isset($this->client)){
            $html .= "<option value='1' " . ($value == 1 ? 'selected' : '') . ">" . $this->client->one->name() . "</option>";
            if ($this->client->two->name() != ""){
                $html .= "<option value='2' " . ($value == 2 ? 'selected' : '') . ">" . $this->client->two->name() . "</option>";
                $html .= "<option value='3' " . ($value == 3 ? 'selected' : '') . ">Joint Policy</option>";
            }
        } else {
            $html .= "<option value='1' " . ($value == 1 ? 'selected' : '') . ">Client One Only</option>";
            $html .= "<option value='2' " . ($value == 2 ? 'selected' : '') . ">Client Two Only</option>";
            $html .= "<option value='3' " . ($value == 3 ? 'selected' : '') . ">Joint Policy</option>";
        }

        $html .= "<option value='4' " . ($value == 4 ? 'selected' : '') . ">Trustee</option>";
        $html .= "<option value='5' " . ($value == 5 ? 'selected' : '') . ">Scheme</option>";
        $html .= "<option value='6' " . ($value == 6 ? 'selected' : '') . ">Unknown</option>";

        $html .= "</select>";

        if ($responseType === "JSON"){
            return $html;
        } else {
            echo $html;
        }
    }

    public function wf_links($steps = []){
        $s = new Search(new Workflow());

        $s->eq("_object", "Policy");
        $s->eq("index", $this->id());

        if (count($steps)){
            $s->eq("step", $steps);
        }
        $links = [];

        while($wf = $s->next(MYSQLI_ASSOC)){
            $links[] = $wf->link();
        }

        if (count($links)){
            return implode(", ", $links);
        } else {
            return false;
        }
    }

    public function historic_wf_links($steps = []){
        $s = new Search(new WorkflowAudit());

        $s->eq("_object", "Policy");
        $s->eq("index", $this->id());

        if (count($steps)){
            $s->eq("step", $steps);
        }
        $links = [];

        while($wf = $s->next(MYSQLI_ASSOC)){
            $links[] = $wf->link();
        }

        if (count($links)){
            return implode(", ", $links);
        } else {
            return false;
        }
    }

    public function sourceIncome()
    {
        $sources = [];

        $s = new Search(new Source());
        $s->eq("policy", $this->id());

        while($p = $s->next(MYSQLI_ASSOC)){
            $sources[] = $p;
        }

        return $sources;
    }

    public function pipelineEntries()
    {
        $pipelineEntries = [];

        $s = new Search(new PipelineEntry());
        $s->eq("policy", $this->id());

        while($p = $s->next(MYSQLI_ASSOC)){
            $pipelineEntries[] = $p;
        }

        return $pipelineEntries;
    }

    public function commission()
    {
        $commission = [];

        $s = new Search(new Commission());

        $s->eq("policy", $this->id());

        while($p = $s->next(MYSQLI_ASSOC)){
            $commission[] = $p;
        }

        return $commission;
    }

    public function deleteRecords($runChecks = true)
    {
        $canDelete = true;

        if($runChecks){
            if ($canDelete && !empty($this->sourceIncome())){
                $canDelete = false;

                throw new Exception("Client has income currently being processed");
            }

            if ($canDelete && !empty($this->pipelineEntries())){
                $canDelete = false;

                throw new Exception("Client has pending income");
            }

            if ($canDelete && !empty($this->commission())){
                $canDelete = false;

                throw new Exception("Client has historic commission entries");
            }
        }

        if($canDelete){
            // Delete notes under any policies we have
            $sPolicyNotes = new Search(new Notes());
            $sPolicyNotes->eq("object_type", "Policy");
            $sPolicyNotes->eq("object_id", $this->id());

            while($note = $sPolicyNotes->next(MYSQLI_ASSOC)){
                $note->delete();
            }

            // Delete all adviser charging entries
            $sAC = new Search(new AdviserCharging());
            $sAC->eq("policy", $this->id());

            while($ac = $sAC->next(MYSQLI_ASSOC)){
                $ac->delete();
            }

            // Delete action required
            $sAR = new Search(new ActionRequired());
            $sAR->eq("object", "Policy");
            $sAR->eq("object_id", $this->id());

            while($ar = $sAR->next(MYSQLI_ASSOC)){
                $ar->delete();
            }

            // check if we can delete the mandate
            $sMan = new Search(new Policy());
            $sMan->eq("mandate", $this->mandate());
            $sMan->nt("id", $this->id());

            if ($sMan->count() == 0){
                // only this policy uses the mandate iD, delete the mandate
                $mandate = new Mandate($this->mandate(), true);

                $mandate->delete();
            }

            // delete all policies
            $this->delete();
        }

        return $canDelete;
    }

    public function statusCheck()
    {
        if($this->type() == 17){
            // unknown
            return "<div class='alert alert-warning mb-2'><i class='fas fa-info-circle'></i> The policy type is unknown. Please update this if possible.</div>";
        }
    }

    public function statusReason()
    {
        if(in_array($this->status(), [8, 32])){
            // policy status is either "8 - Surrendered" or "32 - Out of Force"
            $s = new Search(new ActionReason());
            $s->eq("object", "Policy");
            $s->eq("object_id", $this->id());
            $s->eq("action", "status");
            $s->add_order("id", "DESC");
            $s->limit(1);

            if($r = $s->next(MYSQLI_ASSOC)){
                return "<div class='alert alert-warning mb-2'><i class='fas fa-info-circle'></i> This policy was marked as '" . $this->status . "' by " . $r->created_by . " on " . $r->created_when . ". Reason: " . $r->reason . "</div>";
            }
        }
    }

    public function guessOwner()
    {
        if(!empty($this->client())){
            if($this->client->group_scheme()){
                // if its a group scheme
                return 5;
            }

            if(!empty($this->client->two->surname())){
                // if its a joint client
                return 3;
            }

            // no terms met but client exists
            return 1;
        }

        // no client ID
        return 6;
    }
}
