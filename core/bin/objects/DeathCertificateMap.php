<?php

class DeathCertificateMap extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "death_cert_map";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->death_cert = Sub::factory("DeathCertificate", "death_cert_id")
            ->set_var(Field::REQUIRED, true);

        $this->policy = Sub::factory("Policy", "policy_id")
            ->set_var(Field::REQUIRED, true);

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
