<?php

class WorkflowAudit extends Workflow
{
    const DB_NAME = DATA_DB;
    const TABLE = "workflow_audit";

    public function __construct($id = false, $autoget = false)
    {
        $this->SNAPSHOT_LOG = true;

        $this->id = new Field("ID", Field::PRIMARY_KEY);

        $this->workid = Sub::factory("Workflow", "workID")
            ->set_var(Field::REQUIRED, true);

        $this->due = Date::factory("due")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->step = Sub::factory("WorkflowStep", "stepID");

        $this->_object = Field::factory("object");

        $this->index = Field::factory("object_index");

        $this->completed = Date::factory("completed")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->completedby = Sub::factory("User", "completed_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->early_end = Field::factory("early_end");

        $this->end_reason = Field::factory("end_reason");

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        /* Output...something */
        return $this->id;
    }

    public function link($text = false)
    {
        $class = '';
        return "<a class='profile $class' href=\"" . $this->workid->index_url(array("id" => $this->id())) . "\" target='_BLANK'> " . preg_replace("/[^0-9]/", "", $this->workid) . "</a>";
    }
}
