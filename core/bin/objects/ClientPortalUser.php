<?php

class ClientPortalUser extends DatabaseObject
{
    const DB_NAME = PORTAL_DB;
    const TABLE = "users";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->email = Field::factory('email');

        $this->activation_code = Field::factory('activation_code');
        $this->password = Field::factory('password');

        $this->client_id = Field::factory('client_id')
            ->set_var(Field::REQUIRED, true);

        $this->index = Field::factory('index')
            ->set_var(Field::REQUIRED, true);

        $this->created_at = Date::factory("created_at")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->SNAPSHOT_LOG = true;

        $this->database = PORTAL;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->name";
    }
}
