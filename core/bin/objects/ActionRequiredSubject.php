<?php

class ActionRequiredSubject extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "action_required_subject";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->subject = Field::factory("subject");

        $this->published = Field::factory("published");

        $this->published_when = Date::factory("published_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->subject";
    }
}
