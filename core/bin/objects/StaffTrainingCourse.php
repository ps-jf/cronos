<?php

class StaffTrainingCourse extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "staff_training_courses";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->course_name = Field::factory("course_name")
            ->set_var(Field::REQUIRED, true);

        $this->category = Choice::factory("category")
            ->push('1', "Induction")
            ->push('2', "Department")
            ->push('3', "Company")
            ->set_var(Field::REQUIRED, true);

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->course_name";
    }
}
