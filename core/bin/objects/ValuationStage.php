<?php

class ValuationStage extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "info_prog";
    const COMPLETE = 5;

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->title = Field::factory("progress");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->title";
    }
}
