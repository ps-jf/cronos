<?php

class WebNotification extends DatabaseObject
{
    var $table = WEB_NOTIFICATION_TBL;

    public function WebNotification()
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->date = new Date("date");

        $this->subject = new Field("subject");

        $this->subject->set_var(Field::MAX_LENGTH, 50);

        $this->body = new Field("body");

        call_user_func_array(array("parent", "__construct"), func_get_args());
    }

    public function __toString()
    {
        return "$this->subject";
    }
}
