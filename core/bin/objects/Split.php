<?php

class Split extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "data_change";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object_type = Field::factory("object_type");

        $this->object_id = Field::factory("object_id");

        $this->data = Field::factory("data");

        $this->proposed_by = Field::factory("proposed_by");
        $this->proposed_time = Field::factory("proposed_time");

        $this->committed_by = Field::factory("committed_by");
        $this->committed_time = Field::factory("committed_time");

        $this->comments = Field::factory("comments");

        $this->database = REMOTE;
        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
