<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 22/10/2019
 * Time: 16:01
 */

class IOPolicyType extends DatabaseObject
{
    const DB_NAME = IO_DB;
    const TABLE = "policy_types";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("prophet_id", Field::PRIMARY_KEY);

        $this->io_name = Field::factory("io_name");

        $this->database = IO;

        parent::__construct($id, $auto_get);
    }
}