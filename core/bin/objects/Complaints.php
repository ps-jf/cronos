<?php

class Complaints extends DatabaseObject
{

    const DB_NAME = DATA_DB;
    const TABLE = "complaints_register";

    public function __construct($id = false, $autoget = false)
    {

        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true);

        $this->client = Sub::factory("Client", "clientID")
            ->set_var(Field::REQUIRED, true);

        $this->status = Choice::factory("Status")
            ->setSource("select stepID, stepDesc from feebase.workflow_step where processID = 26 ")
            ->set_var(Field::REQUIRED, true);

        $this->complaint_received = Date::factory("complaint_received")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::REQUIRED, true);

        $this->category = Choice::factory("category")
            ->push("1", "Overcharging or incorrect charges")
            ->push("2", "Delays")
            ->push("3", "Other Administration errors")
            ->push("4", "Misleading Advice")
            ->push("5", "Failure to carry out instructions")
            ->push("6", "Poor Customer Services")
            ->push("7", "Misleading advisertising or product information")
            ->push("8", "Disputes over sums or amounts payable")
            ->push("9", "Switching / Churning")
            ->push("10", "Breach of customer agreement or contract")
            ->push("11", "Arrears handling")
            ->push("12", "Other  ")
            ->set_var(Field::DISPLAY_NAME, "Complaint Category");

        $this->ps_adviser = Choice::factory("ps_adviser")
            ->push("0", "None")
            ->setSource("select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "where active = '1' and id in (22, 45, 60, 109 , 192) " .
                "order by first_name, last_name");

        $this->admin_complaint = Choice::factory("admin_complaint")
            ->push("1", "Admin")
            ->push("2", "Advice")
            ->push("3", "Income / Fees")
            ->set_var(Field::DISPLAY_NAME, "Admin Complaint");

        $this->outcome = Choice::factory("outcome")
            ->push("1", "Closed by Client")
            ->push("2", "Complaint Upheld")
            ->push("3", "Not Upheld")
            ->set_var(Field::DISPLAY_NAME, "Outcome");

        $this->compensation = Boolean::factory("client_compensation")
            ->set_values(1, 0);

        $this->raised_fos = Boolean::factory("raised_with_fos")
            ->set_values(1, 0);

        $this->fos_outcome = Choice::factory("fos_outcome")
            ->push("1", "PS Decision Upheld")
            ->push("2", "Decision in favour of Client")
            ->set_var(Field::DISPLAY_NAME, "FOS Outcome");

        $this->notes = Field::factory("notes")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE, "Notes");

        $this->type_of_product = Choice::factory("type_of_product")
            ->setSource(POLICY_TYPE_TBL, "policyTypeId", "policyTypeDesc");

        $this->related_to_advice = Choice::factory("related_to_advice")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "Yes (Not PS)")
            ->set_var(Field::DISPLAY_NAME, "Related to Advice");

        $this->final_response = Date::factory("final_response_issued")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->fos_deadline = Date::factory("deadline_for_fos_complaints")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->record_closed = Boolean::factory("record_closed")
            ->set_values(1, 0);

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->date_acknowledged = Date::factory("date_acknowledged")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::REQUIRED, true);

        $this->date_acknowledged_four = Date::factory("date_acknowledged_four")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->date_acknowledged_eight = Date::factory("date_acknowledged_eight")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->recorded_delivery_num = Field::factory("recorded_delivery_num");

        $this->insurers_approved_returned = Date::factory("insurers_approved_returned")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->insurers_response_draft = Date::factory("insurers_response_draft")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->date_resolved = Date::factory("date_resolved")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->final_response_drafted = Date::factory("final_response_drafted")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->investigation_completed = Date::factory("investigation_completed")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->mifid_category = Choice::factory("mifid_category")
            ->push("1", "Retail")
            ->push("2", "Professional")
            ->push("3", "Eligible Counterparty")
            ->set_var(Field::DISPLAY_NAME, "MiFID Category");

        $this->mifid = Boolean::factory("mifid")
            ->set_values(1, 0);

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }

    public function link($text = false)
    {
        return $this->client->link();
    }

    public function complaint_existing($client = false)
    {
        $merge_request = "";
        $data = "";

        // outstanding complaints on this policy/client
        $comp = new Search(new Complaints());
        $comp->eq("clientID", $client->id());
        $comp->eq("record_closed", 0);

        while ($c = $comp->next(MYSQLI_ASSOC)) {
            if ($c->status() < 59 || $c->status() != 59) {
                // outstanding complaints on this policy/client
                $s = new Search(new Workflow());
                $s->eq("object", "Complaints");
                $s->eq("object_index", $c->id());

                while ($x = $s->next(MYSQLI_ASSOC)) {
                    $user = User::get_default_instance();
                    if (($user->department() == 7 || $user->department() == 2)) {
                        $data .= " " . $x->link($x->id()) . " ";
                    } else {
                        $data .= " " . $x->id() . " ";
                    }
                }
            }
        }

        if ($data != "") {
            $merge_request = "<div class='notification static' style='display:block;'>
              This client has workflow open on an ongoing complaint " . $data . "</div>";
        }

        return $merge_request;
    }

    public function on_create($post = false)
    {

        //On create - Get user id from cookies
        //On create - Assign ticket to appropriate group/person
        //On create =  Determine reminder time
        //On create - Build up array for sending through the workflow object "workflow_add"
        //On create - Empties buffer of json return as two returns were being returned!

        try {
            $date1 = $this->date_acknowledged();

            $this->date_acknowledged_four($date1 + 2629743);
            $this->date_acknowledged_eight($date1 + 5259486);
            $this->save();

            $userID = User::get_default_instance('id');

            $reminderTime = time() + 0;
            $desc = "Complaint Received";
            $arr_args = array(
                'process' => '26',
                'step' => $this->status(),
                'priority' => '2',
                'due' => $reminderTime,
                'desc' => 'Complaint - ' . $desc,
                '_object' => 'Complaints',
                'index' => $this->id()
            );

            $workflow = new Workflow();
            ob_start();
            $workflow->workflow_add($arr_args, $userID, 2, 0);
            ob_end_clean();
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function on_update($post = false)
    {

        //On update - change the reminder time to the new due date
        //Apply the updated when and updated by fields with appropriate data

        try {
            $updatedBy = User::get_default_instance('id');
            $status = $this->status();
            $desc = "";
            $workflowObj = new Search(new Workflow);
            $workflowObj->eq("object", "Complaints");
            $workflowObj->eq("index", $this->id());

            if ($status == 48) {
                $reminderTime = time($this->complaint_received()) + 0;
                $desc = "Complaint Received";
            } elseif ($status == 49) {
                $reminderTime = time($this->addedwhen()) + 259200;
                $desc = "Complaint Acknowledged/Resolved";
            } elseif ($status == 50) {
                $reminderTime = time($this->addedwhen()) + 2419200;
                $desc = "Investigation Ongoing - Holding Letter issued";
            } elseif ($status == 51) {
                $reminderTime = time() + 4838400;
                $desc = "Investigation Ongoing - 2nd Holding letter is";
            } elseif ($status == 52) {
                $reminderTime = time() + 0;
                $desc = "Investigation Ongoing";
            } elseif ($status == 53) {
                $reminderTime = time() + 0;
                $desc = "Investigation Complete";
            } elseif ($status == 54) {
                $reminderTime = time() + 86400;
                $desc = "Final Response Drafted";
            } elseif ($status == 55) {
                $reminderTime = time() + 86400;
                $desc = "Draft Final Response to PI Insurers";
            } elseif ($status == 56) {
                $reminderTime = time() + 259200;
                $desc = "Draft Final Response Returned from PI Insurer";
            } elseif ($status == 57) {
                $reminderTime = time() + 432000;
                $desc = "Final response Issued";
            } elseif ($status == 58) {
                $reminderTime = time() + 15778463;
                $desc = "FOS Deadline";
            } elseif ($status == 59) {
                $reminderTime = time() + 15778463;
                $desc = "Record Closed";
            }


            while ($a = $workflowObj->next()) {
                $obj = new Workflow($a->id(), true);
                $obj->due($reminderTime);
                $obj->step($status);
                $obj->desc('Complaint Updated - ' . $desc);
                $obj->updatedby($updatedBy);
                $obj->updatedwhen(time());
                $obj->save();
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
