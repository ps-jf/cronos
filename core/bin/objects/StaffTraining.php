<?php

class StaffTraining extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "staff_training";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user = Sub::factory("User", "user_id")
            ->set(User::get_default_instance("id"));

        $this->course = Sub::factory("StaffTrainingCourse", "course_id")
            ->set_var(Field::REQUIRED, true);

        $this->start = Date::factory("start_date")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->completed = Date::factory("completed_date")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->next = Date::factory("next_date")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }


    public function __toString()
    {
        return "$this->id";
    }

    public function link($text = false)
    {

        return parent::link($this->course->course_name());
    }

    public function training_admin()
    {
        //allow avril, louise, diane & IT to re-assign correspondence from 1 queue to another
        $admin_ids = array(29, 98, 45);

        $db = new mydb();
        $q = "SELECT id FROM prophet.users WHERE department = 7 AND active = 1";

        $db->query($q);
        while ($users = $db->next(MYSQLI_ASSOC)) {
            $admin_ids[] = $users['id'];
        }

        if (in_array(User::get_default_instance("id"), $admin_ids)) {
            $correspondence_admin = true;
        } else {
            $correspondence_admin = false;
        }

        return $correspondence_admin;
    }
}
