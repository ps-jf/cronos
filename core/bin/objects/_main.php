<?php

/**
 * This is the basic template for how all of our objects get their structure.
 *
 *
 * The database object defines the data in the database(funnily enough)
 * It defines the behaviour and methods available to all database objects
 * before and after they have been pulled into the application layer
 *
 *
 * @package prophet.core
 * @author daniel.ness
 */

class DatabaseObject
{
    const MARK_AS_LOADED = -20;
    const ON_SAVE_NO_LOG = -21;
    const ELEMENT_SUGGEST = "suggest";
    const ELEMENT_DROPDOWN = "dropdown";
    const ELEMENT_HIDDEN = "hidden";
    const NO_CACHE = "no-cache";
    const DEBUG = "debug";
    const SAVE_INSERT_MODE = "INSERT";
    const SAVE_UPDATE_MODE = "UPDATE";
    public static $cache = [];
    protected static $fields;
    var $primary_key;
    var $table;
    var $loaded = false;
    var $_exists = false;
    var $_is_deleted = false;
    var $can_get = true;
    var $database = LOCAL;
    var $group_name; // Disallow deletion of records by default
    public $order = [];  // log insert/update events
    protected $CAN_DELETE = false; // store serialized value array of instance at insert/update
    protected $ACTION_LOG = true;
    protected $SNAPSHOT_LOG = false;
    protected $errors = [];
    protected $registered_properties = [];
    protected $db_link;
    protected $field_name_map = [];

    public function __construct($id = false, $auto_get = false)
    {
        $id = intval((string)$id);
        if ($id) {
            $this->primary_key($id);

            if ($auto_get) {
                $this->get();
            }
        }

        $this->group_name = $this->get_default_group_name();
    }

    /**
     * Returns one object based on the primary key, with the
     * logical assumption that there should only be one.
     *
     * @param bool $id
     * @return $this|bool
     */
    public function get($id = false)
    {
        if ($id) {
            $this->primary_key($id);
        }

        if ($this->primary_key() === false) {
            return false;
        }

        $classname = get_called_class();

        if (!array_key_exists($classname, self::$cache)) {
            self::$cache[$classname] = [];
        }

        if (is_int($this->primary_key()) || is_string($this->primary_key())) {
            if (array_key_exists($this->primary_key(), self::$cache[$classname])) {
                // object has been used this session, retrieve it from the local cache
                $cacheObj = unserialize(self::$cache[$classname][$this->primary_key()]);
                $this->load($cacheObj->flat_array(), false);
                $this->loaded = true;
                return $this;
            } else {
                if(empty($this->primary_key())){
                    return false;
                }

                // object not found in local/remote cache, so get it from the database
                $q = "SELECT * FROM " . $this->get_table() . " WHERE " . $this->primary_key->get_field_name() . " = " . $this->primary_key();

                $db = new mydb($this->database);

                if (!$db->query($q)) {
                    return false;
                }

                if ($d = $db->next()) {
                    $this->load($d, false);
                    $this->loaded = true;
                    // add this object to local cache + remote memcache
                    $cache_data = serialize($this);
                    self::$cache[$classname][$this->id()] = $cache_data;
                    return $this;
                }
            }
        }

        return false;
    }

    /**
     *  Uses $array's keys to attempt to import data into
     *  a DatabaseObject instance.
     *
     * @param bool $array
     * @param bool $mark_as_edited
     *
     * @return bool
     */
    public function load($array = false, $mark_as_edited = true)
    {
        if (!$array) {
            return false;
        }

        // *** Security ***
        $allowed_fields = false;
        if (isset($array["__fields__"], $array["__enc__"])) {
            if (salty_md5($array["__fields__"]) != $array["__enc__"]) {
                // __fields__ has been tampered with, do not proceed
                trigger_error("DatabaseObject::load($array) cannot be authorised", E_USER_ERROR);
                return false;
            } else {
                $allowed_fields = explode(";", $array["__fields__"]);
            }
        }

        // Cycle through each element in the input array, looking for the correct property to apply value to.
        foreach ($array as $key => $value) {
            if (property_exists($this, $key)) {
                // Element is indexed by property name
                if (is_a($this->{$key}, "Group")) {
                    $this->{$key}->load($value);
                } elseif (is_a($this->{$key}, "Field")) {
                    if(is_array($value)){
                        $this->{$key}->set($value);
                    } else {
                        $this->{$key}->set(trim($value));
                    }
                }

                $this->{$key}->edited = $mark_as_edited;
            } else {
                // Catch elements indexed by database field name
                if ($field = $this->get_field_by_dbname($key)) {
                    if(is_array($value)){
                        $field->set($value);
                    } else {
                        $field->set(trim($value));
                    }
                    $field->edited = $mark_as_edited;
                }
            }
        }

        // apply flags
        $args = func_get_args();
        array_shift($args);
        foreach ($args as $a) {
            if ($a == DatabaseObject::MARK_AS_LOADED) {
                $this->loaded = true;
            }
        }
        $this->on_get();
    }

    public function get_field_by_dbname($key)
    {

        /* Get the Field whose database name is $key */

        foreach ($this->field_name_map as $pname => $pvalue) {
            if ($key == $pvalue) {
                return $this->{$pname};
            } elseif (is_array($pvalue)) {
                foreach ($pvalue as $gpname => $gpvalue) {
                    if ($key == $gpvalue) {
                        return $this->{$pname}->{$gpname};
                    }
                }
            }
        }
    }

    public function on_get()
    {
        /* Perform specials taks when object first loaded */
    }

    public function get_table()
    {

        if (!empty($this->table)) {
            return $this->table;
        } elseif (defined("static::TABLE") && defined("static::DB_NAME")) {
            return static::DB_NAME . "." . static::TABLE;
        } else {
            trigger_error(get_called_class() . " has no \$table or ( ::TABLE, ::DB_NAM ) properties", E_USER_ERROR);
        }
    }

    static function get_default_group_name()
    {
        return strtolower(get_called_class());
    }

    /**
     *
     * Generate a URL pointing to the public index.php file in the appropriate object's directory,
     * appending an encoded GET string from (array) $get
     *
     * @param bool $get
     * @param bool $xml_compliant
     * @return string
     */
    public static function index_url($get = false, $xml_compliant = false)
    {

        $base = __APP_CONTENT_DIR__ . strtolower(get_called_class()) . "/";

        if (is_array($get)) {
            foreach ($get as $k => $v) {
                $get[$k] = "$k=" . urlencode($v);
            }
            $get = "?" . implode("&", $get);
        }

        return ($xml_compliant) ? htmlentities("$base$get") : "$base$get";
    }

    public static function get_template_dir()
    {
        return __APP_BIN__ . "templates/" . strtolower(get_called_class()) . "/";
    }

    public static function get_template_path($file)
    {
        return __APP_BIN__ . "templates/" . strtolower(get_called_class()) . "/$file";
    }

    public static function get_template($file, $vars = false)
    {

        // Ensure that an array (even if empty) is passed to template
        (is_array($vars)) or $vars = [];


        $oname = get_called_class();
        $object = new $oname;

        return Template($object->get_template_path($file), $vars);
    }

    public static function controller_path()
    {
        return __APP_BIN__ . "controllers/" . strtolower(get_called_class()) . ".php";
    }

    public static function url($action = "home", $get = array())
    {

        // Return a URL for the main controller script.
        $get["object"] = get_called_class();
        $get["action"] = $action;

        // prepare the GET string
        foreach ($get as $k => $v) {
            $get[$k] = "$k=" . urlencode($v);
        }

        return "/?" . implode("&", $get);
    }

    public static function get_search()
    {
        $class = get_called_class();
        return new Search(new $class);
    }

    public function get_field_name_map()
    {
        return $this->field_name_map;
    }

    public function __clone()
    {
        foreach ($this as $key => $val) {
            if (is_object($val) || (is_array($val))) {
                $this->{$key} = unserialize(serialize($val));
            }
        }
    }

    public function __call($name, $args)
    {
        //echo get_class($this) . "::$name()\n";
        if (property_exists($this, $name)) {
            return call_user_func_array(array($this->{$name}, "__invoke"), $args);
        }
    }

    public function get_errors()
    {
        return $this->errors;
    }

    public function group_name($group)
    {
        /*
         * Use this to set a group name by which form elements
         * can be grouped into an array. This allows the load
         * method to be passed a smaller slice of the $_REQUEST
         * array, and also prevent Field properties from overwriting
         * one another when the names match.
         */
        $this->group_name = (string)$group;
    }

    public function get_html_id($name, $format = Field::HTML_ID_PLAIN)
    {
        // Use this whenever you want to include a custom element in the POST
        // array group for object.
        $html_id = $this->group_name . "[$name]";
        return ($format == Field::HTML_ID_PLAIN) ? $html_id : "#" . str_replace(array('[', ']'), array('\\\[', '\\\]'), $html_id);
    }

    public function register_field(&$property)
    {

        // Register a Field property as writable.

        if (is_a($property, "Field") && !in_array($property->name, $this->registered_properties)) {
            $this->registered_properties[] = $property->name;
        }
    }

    public function flat_array($object = false)
    {

        /*
         * Produce a flat(ish) array of the DatabaseObject's Fields,
         * when a Group instance is encountered produce an array based
         * on all the Fields within it.
         */

        if (!$this->loaded) {
            $this->get();
        }

        $array = [];              // output
        ($object) or $object = $this;

        foreach ($object as $name => $field) {
            if ((is_a($field, "Field") || is_a($field, "Group")) && $name != "primary_key") {
                $array[$name] = (is_a($field, "Group"))
                    ? $this->flat_array($field)
                    : $field();
            }
        }

        // add a profile link
        if (is_a($object, "DatabaseObject")) {
            $array["@link"] = $object->link();
        }

        return $array;
    }

    public function link($text = false)
    {

        //// Returns a .profile element, used to provide (unsurprisingly) a hyperlink to launch
        //// DatabaseObject's profile in a new window, but also to offer object-specific context
        //// menus.

        // @input - $text - string : support custom output format

        // prepare link body
        if ($text) {
            // example. 'number \provider' would output '1234 provider'
            preg_match_all('/\\$([a-z0-9_]+(->[a-z0-9_]+)*)/', $text, $tags);

            foreach ($tags[1] as $tag) {
                $object = $this;

                foreach (explode('->', $tag) as $prop) {
                    if (!property_exists($object, $prop)) {
                        trigger_error("Property '$tag' is not valid for " . get_class($this), E_USER_ERROR);
                        break;
                    }

                    if (is_a($object->{$prop}, "Sub")) {
                        $object = $object->{$prop}->get_object();
                    } else {
                        $object = $object->{$prop};
                    }
                }

                $text = preg_replace("/\\$$tag/", "$object", $text);
            }

            $text = stripslashes($text);
        } else { // use object's  __toString() method
            $text = "$this";
        }

        $class = get_class($this);

        if (strlen($text) < 1) {
            // Content unavailable. Object not found?
            $text = "<a class='disable-links'>???</a>";
        }

        return "<a class='profile $class' href=\"" . $this->get_common_url(array("id" => $this->id())) . "\" target='_BLANK'>$text</a>";
    }

    public static function get_common_url($get_array = false, $xml_compliant = false)
    {
        $url = "/common/" . get_called_class() . "/";
        return url($url, $get_array, $xml_compliant);
    }

    /**
     * Generate two hidden html fields. One will contain all of the Field properties that were registered via
     * Field::toElement method calls, concatenated by ';', and the other will contain an unreversible hash of the
     * same string. When returned to the server these strings will be compared ( to detect tampering ) and only
     * Field names included will be writable.
     *
     * @return string
     */
    public function get_security_fields()
    {
        $__fields__ = implode(";", $this->registered_properties);
        $__enc__ = salty_md5($__fields__);

        $id_format = ($this->group_name !== null) ? "$this->group_name[%s]" : "%s";

        $out = "";
        foreach (array("__enc__", "__fields__") as $x) {
            $out .= el::hide(sprintf($id_format, $x), ${$x});
        }

        return $out;
    }

    /**
     * function to get all records relating to a specific object
     *
     * @param bool $field_name
     *
     * @return array
     */
    public static function all($field_name = false)
    {
        $object_array = [];
        $classname = get_called_class();
        if ($classname) {
            $s = new Search(new $classname);
            while ($a = $s->next(MYSQLI_ASSOC)) {
                (!$field_name) ?
                    $object_array[] = $a :
                    $object_array[$a->id()] = strval($a->$field_name);
            }
        }
        return $object_array;
    }

    public function get_profile_url($id = false)
    {

        if ($id === false) {
            $id = $this->id();
        }

        return url($this->get_public_dir(), array("id" => $id));
    }

    //// Validation methods
    ////*************************************

    public static function get_public_dir()
    {
        return __APP_CONTENT_DIR__ . strtolower(get_called_class()) . "/";
    }

    public function delete()
    {

        //// remove Object from database

        if (!$this->CAN_DELETE) {
            throw new Exception(get_class($this) . " is locked against deletion");
        }

        $query = "delete from " . $this->get_table() . " where " . $this->primary_key->get_field_name() . " = " . $this->id();

        $db = ($this->db_link) ? $this->db_link : new mydb($this->get_database());

        try {
            $db->query($query);

            // Log action
            $log = new LocalLog;
            $log->object_name(get_class($this));
            $log->object_id($this->id());
            $log->type("DELETE");

            $log->save();

            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }

    //****************************************

    public function use_db_link(&$link)
    {
        $this->db_link = $link;
    }

    public function save($return_query = false)
    {

        /*
         * # Syncs changes to database, updates preexisting entries and creates
         * # new entries where necessary.
         */

        $args = func_get_args();

        // determine query mode (update/create) and form query appropriately
        if ($this->loaded) {
            // entry has been edited, simply update changes
            $mode = DatabaseObject::SAVE_UPDATE_MODE;
            $q = "UPDATE ";
            $condition = ' WHERE ' . $this->primary_key->get_field_name() . ' = ' . $this->primary_key();
            $action = array('type' => "edit", 'id' => $this->id);
        } else {
            // a new entry must be created
            $mode = DatabaseObject::SAVE_INSERT_MODE;
            $q = "INSERT INTO ";
            $condition = false;
            $action = array('type' => "add");
        }

        //// Legacy auditing fields
        // * Despite these fields being declared+defined in the class def
        // * load() will have rendered the edited property as false if invoked
        // * through get()
        if (property_exists($this, "updatedwhen") && !$this->updatedwhen->edited) {
            $this->updatedwhen->set(time(), true, Date::UNIX);
        }
        if (property_exists($this, "updatedby") && !$this->updatedby->edited) {
            $this->updatedby->set(User::get_default_instance('id'));
        }

        try {
            // Validate entry before saving
            $this->is_valid($mode);

            // Prepare SQL expression string
            $fields = $this->get_SQL_fields($mode);
            if (!strlen(trim($fields))) {
                throw new Exception("There is no data to save");
            }
        } catch (Exception $e) {
            throw $e;
        }

        $q .= $this->get_table() . " SET $fields $condition";

        if ($return_query) {
            return $q;
        }

        $db = ($this->db_link) ? $this->db_link : new mydb($this->get_database());

        if (!$db->query($q) && $db->errno) {
            $status = false;

            switch ($db->errno) {
                case 1062:
                    // Duplicate entry '%' for key '%'
                    if (preg_match('/key \'(.*?)\'$/', $db->error, $m)) {
                        $field = $this->get_field_by_dbname($m[1]);
                        $error = $field->friendly_name() . " must be unique";
                    }
                    break;

                default:
                    $error = "Unknown database error ($db->errno) $q";
                    break;
            }

            throw new Exception($error);
        }

        $status = true;

        if ($mode == DatabaseObject::SAVE_INSERT_MODE) {
            $this->id($db->insert_id);
        }

        if ($this->ACTION_LOG) {
            // Log action
            $log = new LocalLog;
            $log->object_name(get_class($this));
            $log->object_id($this->id());
            $log->type($mode);

            if ($mode == DatabaseObject::SAVE_UPDATE_MODE && $this->SNAPSHOT_LOG) {
                // need to get the original instance (before editing)
                $class = get_class($this);
                $x = new $class($this->id());
                $log->snapshot(json_encode($x->flat_array()));
                //$log->snapshot(serialize($x->flat_array()));
            }

            $log->save();
        }

        $this->loaded = true;
        return $status;
    }

    public function is_valid()
    {

        /*
        // This is the standard validation validation function
        // which loops through every Field/Group instance in the
        // DatabaseObject and invokes their own validation methods.
        */

        try {
            // iterate Fields + Groups
            foreach ($this->field_name_map as $x => $db) {
                $f = $this->$x;
                $fref = get_class($this) . "->$x";

                $f->is_valid();
            }
        } catch (Exception $e) {
            // log exception message & return for handling
            trigger_error(get_class($this) . "->$x" . $e->getMessage());
            throw new Exception($f->friendly_name() . $e->getMessage());
        }

        try {
            // perform custom validation per Object
            $this->__is_valid();
        } catch (Exception $e) {
            // log exception message & return for handling
            trigger_error(get_class($this) . ": " . $e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    protected function __is_valid()
    {
        // overloadable method for Object-specific validation
    }

    public function get_SQL_fields($save_mode = false, $object = false)
    {

        /*
          Generate a string of column expressions for each adequately
          declared + defined (sub-)Field within this object
        */

        $fields = [];

        ($object !== false) or $object = $this;

        foreach ($object->field_name_map as $name => $prop) {
            $p = $object->{$name};

            if (is_a($p, "Group")) {
                // invoke Group's to_sql method to append sub-Field expressions
                //array_merge( $fields, $p->to_sql() );
                $sqlStr = $this->get_SQL_fields($save_mode, $p);
                if (strlen(trim($sqlStr)) > 0) {
                    $fields[] = $sqlStr;
                }
            } elseif (is_a($p, 'Field')) {
                if ($p->get_var(Field::PRIMARY_KEY)) {
                    // primary keys are typically auto_increment
                    continue;
                }

                if ($save_mode == DatabaseObject::SAVE_INSERT_MODE) {
                    // Skip Fields which may only be set on existing rows
                    if ($p->get_var(Field::BLOCK_INSERT)) {
                        continue;
                    }
                } elseif ($save_mode == DatabaseObject::SAVE_UPDATE_MODE) {
                    // Skip "one time only" Fields such as "added by" cols
                    if ($p->get_var(Field::BLOCK_UPDATE)) {
                        continue;
                    }
                }

                if (!$p->edited) {
                    continue;
                }

                if ($p->is_set) {
                    $fields[] = $p->to_sql(null, false, $save_mode);
                }

                $p->edited = false;
            }
        }

        return implode(", ", $fields);
    }

    public function get_database()
    {

        if (!empty($this->database)) {
            return $this->database;
        } elseif (defined("static::DB_NAME")) {
            return static::DB_NAME;
        } else {
            trigger_error(get_called_class() . " has no \$database or ::DB_NAM properties", E_USER_ERROR);
        }
    }

    public function on_update($POST_data = false)
    {
        /* Perform any DatabaseObject-specific tasks upon record update */
    }

    public function on_create($POST_data = false)
    {
        /* Perform any DatabaseObject-specific tasks upon creation of a new record */
    }

    public function on_delete()
    {
        // Perform actions (cleanup etc.) when this object's db entry
        // has been deleted. * Hint * you're going to want to overload
        // this.
        return true;
    }

    public function toElement($id = false, $attr = false, $value = false, $url = false, $el_type = DatabaseObject::ELEMENT_SUGGEST)
    {

        /*

          DatabaseObject::ELEMENT_SUGGEST = generate search field with suggestions
          DatabaseObject::ELEMENT_DROPDOWN = generate a dropdown field containing every instance of object >> ** potentially massive overhead **
        */


        if ($id === false) {
            $id = get_class($this);
        }

        if ($el_type == DatabaseObject::ELEMENT_SUGGEST) {
            if ($url === false) {
                $url = $this->get_public_dir();
            }

            return el::suggest($id, $url, "search", $value);
        } else {
            // get all options for a dropdown
            $options = array("<option value=''> -- Please select -- </option>");

            $iter = new Search($this);
            while ($x = $iter->next()) {
                $options[] = "\t<option value=\"" . $x->id() . "\">$x</option>";
            }

            return "<select id=\"$id\" name=\"$id\">\n" . implode("\n", $options) . "</select>";
        }
    }

    public function __set($key, $value)
    {

        //// Invoked when an unknown property is created

        $this->{$key} = $value;

        if (is_a($value, "Field") || is_a($value, "Group")) {
            if (is_a($value, "Group")) {
                $this->field_name_map[$key] = $value->field_name_map;
                $value->group_name = $key;
            } else {
                $this->field_name_map[$key] = $value->get_field_name(false);
            }

            $this->set_table_on_kids($this->{$key});
            $this->{$key}->name = $key;

            if (is_a($this->{$key}, "Field") && $this->{$key}->get_var(Field::PRIMARY_KEY)) {
                // if field is a primary key take a reference
                $this->primary_key = &$this->{$key};
                $this->primary_key->set_var(Field::HTML_FIELD_TYPE, Field::HTML_HIDDEN);
            }
        }
    }

    private function set_table_on_kids()
    {

        foreach ($this as $name => $value) {
            if (is_a($value, "Field")) {
                $this->{$name}->db_object = &$this;
            } elseif (is_a($value, "Group")) {
                foreach ($value as $sn => $sv) {
                    if (is_a($sv, "Field")) {
                        $this->{$name}->{$sn}->db_object = &$this;
                    }
                }
            }
        }
    }

    public function outstanding_workflow($object_code)
    {

        //// Get outstanding Workflow for this object, or return false

        $s = new Search(new Workflow);
        $s
            ->eq("_object", $object_code)
            ->eq("index", $this->id())
            ->limit(1);

        if (($v = $s->next()) !== false) {
            return $v;
        }

        return false;
    }

    private function get_cache_hash()
    {
        return md5(get_called_class() . "#" . $this->primary_key());
    }

    public function endWorkflow($message, $steps = null, $skipInProgress = false)
    {
        // end any workflows with the steps provided
        $s = new Search(new Workflow());
        $s->eq("_object", get_called_class());
        $s->eq("index", $this->id());

        if(!empty($steps)){
            $s->eq("step", $steps);
        }

        while($wf = $s->next(MYSQLI_ASSOC)){
            if($skipInProgress){
                $db = new mydb();

                $db->query("select * from " . WFPROGRESS_TBL . " where workflowID = " . $wf->id);

                if($db->next(MYSQLI_ASSOC)){
                    continue;
                }
            }

            $wf->end_ticket($message);
        }
    }

    public function endActionRequired($message, $subjects = null)
    {
        $s = new Search(new ActionRequired());
        $s->eq("object", get_called_class());
        $s->eq("object_id", $this->id());
        $s->eq("completed_when", null);

        if(!empty($subjects)){
            $s->eq("subject_id", $subjects);
        }

        while($ar = $s->next(MYSQLI_ASSOC)){
            $arm = new ActionRequiredMessage();

            $arm->action_required_id($ar->id());
            $arm->message($message);

            $arm->save();

            $ar->completed_by(User::get_default_instance("myps_user_id"));
            $ar->completed_when(time());

            $ar->save();
        }
    }

    public function has_action_required()
    {
        $s = new Search(new ActionRequired());
        $s->eq("object", get_called_class());
        $s->eq("object_id", $this->id());
        $s->eq("completed_when", null);
        $s->eq("archived", 0);

        $alerts = "";

        while($ar = $s->next(MYSQLI_ASSOC)){
            $alerts .= $ar->link("<div class='alert alert-warning mb-2'><i class='fas fa-info-circle'></i> This client currently has a \"" . $ar->subject_id . "\" action required ticket open. Please see " . $ar->get_allocated() . " for more information.</div>");
        }

        return $alerts;
    }
}

class Field
{
    /*
     *  # A field holds a single item of information within an object.
     *
     */

    const PRIMARY_KEY = "PRIMARY_KEY";
    const NO_TO_SQL = "NO_TO_SQL";
    const TYPE = "TYPE";
    const MAX_LENGTH = "MAX_LENGTH";
    const PATTERN = "PATTERN";
    const REQUIRED = "REQUIRED";
    const PARTNER_REF = "PARTNER_REF";
    const DISPLAY_NAME = "DISPLAY_NAME";

    // core settings
    const BLOCK_INSERT = "BLOCK_INSERT";
    const BLOCK_UPDATE = "BLOCK_UPDATE";
    const FORCE_ZERO = "FORCE_ZERO";
    const STRING = "string";
    const FLOAT = "float";
    const INTEGER = "integer";
    const CURRENCY = "currency";
    const DATE = "date";

    // SQL statement mode permissions
    const PASSWD = "passwd";
    const PERCENT = "percent";

    // @TODO essentially a hack
    //we have db values that can have zero as a primary key
    //we need this to pass validations checks and the sql to be created properly
    const HTML_ID_PLAIN = "plain";

    // Data types
    const HTML_ID_JQUERY = "jquery";
    const HTML_FIELD_TYPE = "HTML_FIELD_TYPE";
    const HTML_SINGLELINE = "singleline";
    const HTML_MULTILINE = "multiline";
    const HTML_HIDDEN = "hidden";
    const TRUE_VALUE = "TRUE_VALUE";
    const question = "question_mark.png";
    var $value = false;
    var $db_object;
    var $is_set;
    var $name;
    var $edited = false;
    var $group;
    private $databaseName;
    private $settings = [];

    public function __construct($db_name = false, $type = false)
    {
        $this->databaseName = $db_name;

        if ($type == Field::PRIMARY_KEY) {
            $this->set_var(Field::PRIMARY_KEY, true);
            $this->set_var(Field::TYPE, Field::INTEGER);
            $this->set_var(Field::BLOCK_UPDATE, true);
        } elseif ($type !== false) {
            $this->set_var(Field::TYPE, $type);
        } else {
            $this->set_var(Field::TYPE, Field::STRING);
        }
    }

    public function set_var($var_name, $var_value)
    {
        if (!defined('static::' . $var_name)) {
            trigger_error("$var_name setting does not exist in " . get_called_class(), E_USER_ERROR);
        }

        if ($var_name == Field::TYPE) { // backwards compatible
            switch ($var_value) {
                case Field::PERCENT:
                    $this->set_var(Field::PATTERN, "/^(100|[1-9]?[0-9])(\.\d+)?$/");
                    $this->set_var(Field::MAX_LENGTH, 5); // need to allow 99.55%
                    break;

                case Field::CURRENCY:
                    $this->set_var(Field::PATTERN, "/^\-?\d+(\.\d{1,2})?$/");
                    break;

                case Field::PARTNER_REF:
                    $this->db_object->partner = &$this;
                    break;
            }
        }

        $this->settings["Field::$var_name"] = $var_value;
        return $this;
    }

    public static function factory()
    {
        $r = new ReflectionClass(get_called_class());
        return $r->newInstanceArgs(func_get_args());
    }

    function __invoke($value = null)
    {
        // Calling Field object as a function

        if ($value === null) {
            // if value not supplied, read value
            return $this->value;
        }
        // otherwise set Field
        $this->set($value);
        return $this;
    }

    public function set($value, $mark_as_edited = true)
    {
        // Set the value of this Field
        $this->value = $value;
        $this->edited = $mark_as_edited;
        $this->is_set = true;
        return $this;
    }

    function __set($name, $value)
    {
        $this->{$name} = $value;

        if (is_a($value, "Field")) {
            $this->{$name}->name = $name;
        }
    }

    public function display_name()
    {
        // Returns a user-friendly version of the property name, attempts to
        // generate one if not specified using "->set_var( Field::DISPLAY_NAME, name )"

        if (($name = $this->get_var(Field::DISPLAY_NAME)) == null) {
            $name = ucwords(str_replace('_', ' ', $this->name));
        }

        return $name;
    }

    public function get_var($var_name)
    {
        if (!defined('static::' . $var_name)) {
            trigger_error("$var_name getting does not exist in " . get_called_class(), E_USER_ERROR);
        }

        return (array_key_exists("Field::$var_name", $this->settings)) ? $this->settings["Field::$var_name"] : null;
    }

    public function is_valid()
    {
        $val = $this();

        if ($this->get_var(Field::REQUIRED) && empty($val)) {
            if ($this->get_var(Field::FORCE_ZERO) && $val === '0') {
                //do nothing, weird control flow, but meh
            } else {
                // ensure required fields are populated
                throw new Exception(" is required");
            }
        } elseif (!empty($val) && ($pattern = $this->get_var(Field::PATTERN)) !== null) {
            // if regex pattern defined, check value against it
            if (!preg_match($pattern, trim($val))) {
                throw new Exception(" value '$val' is invalid");
            }
        }
    }

    public function to_sql($value = null, $wildcard = false, $mode = false)
    {
        $value = ($value !== null) ? $value : $this();

        // If an array has been passed as $value in the function parameters, then prepare the
        // SQL as "field_name IN (1,2,3...)"
        // ** Wildcard is always ignored in this style. **

        $db = new mydb($this->db_object->get_database());

        if (is_array($value)) {
            for ($i = 0; $i < count($value); $i++) {
                $value[$i] = (is_numeric($value[$i])) ? $value[$i] : "'" . $db->real_escape_string($value[$i]) . "'";
            }
            return
                $this->get_field_name(true, true) . " IN ( " . implode(", ", $value) . " )";
        }

        $value = $db->real_escape_string(trim($value));
        $out = $this->get_field_name(true, true);

        if ($this->get_var(Field::TYPE) == Field::PASSWD) {
            $value = password_hash($value, PASSWORD_DEFAULT);
            $out .= " = '$value'";
        } elseif (in_array($this->get_var(Field::TYPE), array(Field::INTEGER, Field::FLOAT)) || $this->get_field_name() === "feebase.tblpolicy.rate") {
            // we need to know the mode to handle null properly. If updating/inserting we need equals, selecting needs is
            $mode = ($mode == DatabaseObject::SAVE_INSERT_MODE || $mode == DatabaseObject::SAVE_UPDATE_MODE) ? "=" : "is";

            if (!empty($value)) {
                $out .= " = $value";
            } else {
                if ($this->get_var(Field::FORCE_ZERO) && $value === '0') {
                    $out .= " = 0";
                } else {
                    $out .= $mode . " NULL";
                }
            }
        } else {
            if ($wildcard) {
                $out .= " LIKE ";
                $value .= "%";
            } else {
                $out .= " = ";
            }

            if(is_null($value) || $value === ""){
                $out .= "NULL";
            } else {
                $out .= "\"$value\"";
            }
        }

        return $out;
    }

    public function get_field_name($absolute = true, $with_ticks = false)
    {
        // return Field's database field name
        $field = ($with_ticks) ? "`$this->databaseName`" : $this->databaseName;
        return ($absolute) ? $this->db_object->get_table() . ".$field" : "$field";
    }

    function __toString()
    {
        if ($this->get_var(Field::TRUE_VALUE) && empty($this->value)){
            return "<img src=\"" . el::image(Field::question) . "\"/>";
        }

        // format string rep depending on the field type
        switch ($this->get_var(Field::TYPE)) {
            case Field::CURRENCY:
                return "&pound; " . number_format((float)$this->value, 2);
                break;

            case Field::PERCENT:
                $value = (float)$this->value;
                return (($value % 1) ? number_format($value, 2) : $value) . "%";
                break;

            default:
                return "$this->value";
                break;
        }
    }

    public function get()
    {
        return "$this";
    }

    public function friendly_name()
    {
        // convert example_property to "Example Property"
        return ucwords(str_replace("_", " ", $this->name));
    }

    public function toElement($id = false, $attr = false, $value = false)
    {
        /*** Outputs the Field in an appropriate form element ***/
        $id = $this->get_html_id($id);
        $value = ($value !== false) ? $value : $this->value;

        // apply any class styles / component links to element
        if (property_exists($this, "className")) {
            $attr .= " class=\"" . $this->className . "\" ";
        }

        // as well as any value restrictions
        if ($this->get_var(Field::MAX_LENGTH) !== null) {
            $length = $this->get_var(Field::MAX_LENGTH);
            $size = $length;
            if ($length > 25) {
                $size = 35;
            }
            $attr .= "maxlength='$length' size='$size' ";
        }

        if (($pattern = $this->get_var(Field::PATTERN)) !== null) {
            /* convert regex from PCRE to J RegExp-friendly */

            $delim = substr($pattern, 0, 1);

            $field = get_class($this->db_object) . "->" . $this->name;

            if (($last_delim_pos = strrpos($pattern, $delim)) == 0) {
                trigger_error("No closing pattern delimiter found in $field", E_USER_ERROR);
            }

            // extract + validate modifiers
            $mods = substr($pattern, $last_delim_pos + 1);
            if (strlen($mods) > 0) {
                foreach (str_split($mods) as $sym) {
                    if (strpos("imsx", $sym) == -1) {
                        trigger_error("Invalid RegExp modifier '$sym' found in $field modifiers", E_USER_ERROR);
                    }
                }
            }

            // remove delimiters
            $pattern = substr($pattern, 1, $last_delim_pos - 1);

            // include regex + mods as HTML5-friendly attributes
            $attr .= "data-pattern=\"$pattern\" data-pattern_mods=\"$mods\" ";
        }

        // wrapper classes
        $wrap_classes = array($this->get_var(Field::TYPE));

        // register Field as writable
        $this->db_object->register_field($this);


        if ($this->get_var(Field::HTML_FIELD_TYPE) == Field::HTML_HIDDEN) {
            return el::hide($id, $value, $attr);
        }

        // output element based on object type
        $content = ($this->get_var(Field::HTML_FIELD_TYPE) == Field::HTML_MULTILINE)
            ? "<textarea id='$id' name='$id' $attr>$value</textarea>"
            : el::txt($id, $value, $attr);

        $out = "<span class='input " . join(" ", $wrap_classes) . "'>$content</span>";
        if ($this->get_var(Field::REQUIRED)) {
            $out = "<span class='required'>$out</span>";
        }

        return $out;
    }

    public function get_html_id($custom_id = false, $format = Field::HTML_ID_PLAIN)
    {
        // Get an HTML element ID for this Field.
        // In prinicipal this is simply the Field->name property,
        // however the new group_name method format may be encountered
        // where object security is used.


        $id = ($custom_id == false) ? $this->name : $custom_id;

        if ($this->group !== null) {
            return $this->db_object->group_name . "[" . $this->group->name . "][$id]";
        }

        if ($this->db_object !== null) {
            return $this->db_object->get_html_id($id, $format);
        }

        return $id;
    }
}

class Group
{
    var $group_name;
    var $field_name_map = [];

    //// Handle a collection of Fields

    public function __construct()
    {
    }

    public static function factory()
    {
        $r = new ReflectionClass(get_called_class());
        return $r->newInstanceArgs(func_get_args());
    }

    public static function type_is_numeric($type)
    {
        $numeric_types = array(
            Field::FLOAT,
            Field::INTEGER,
            Field::CURRENCY,
            Field::PERCENT
        );

        return in_array($type, $numeric_types);
    }

    public function get_field_name_map()
    {
        return $this->field_name_map;
    }

    public function to_sql()
    {
        $fields = [];

        foreach ($this->field_name_map as $name => $prop) {
            $f = $this->{$name};

            if (!$f->edited) {
                continue;
            }

            $fields[] = $f->to_sql();
        }

        return $fields;
    }

    public function load($array)
    {
        if (!is_array($array)) {
            return;
        }

        foreach ($array as $name => $value) {
            if (property_exists($this, $name)) {
                $this->{$name}->set($value);
            }
        }
    }

    public function is_valid()
    {
        try {
            foreach ($this->field_name_map as $x => $db) {
                $f = $this->{$x};
                $f->is_valid();
            }
        } catch (Exception $e) {
            throw new Exception($f->friendly_name() . $e->getMessage());
        }
    }

    public function __set($name, $prop)
    {
        if (is_a($prop, "Field")) {
            $prop->name = $name;
            $prop->group = &$this;
            $this->field_name_map[$name] = $prop->get_field_name(false);
        }

        $this->{$name} = $prop;
    }

    public function friendly_name()
    {
        // convert example_property to "Example Property"
        return ucwords(str_replace("_", " ", $this->group_name));
    }

    function __call($name, $args)
    {
        if (property_exists($this, $name)) {
            return call_user_func_array(array($this->{$name}, "__invoke"), $args);
        }
    }
}

interface FileEntry
{
    public function get_file_path();

    public function get_file_stream();
}