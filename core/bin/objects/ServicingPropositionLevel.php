<?php

class ServicingPropositionLevel extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "servicing_proposition_levels";
    const PERCENT = [
        1 => 1,
        2 => '0.75',
        3 => '0.5',
        'T' => 0,
        'P' => 0,
    ];

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client_id = Sub::factory("Client", "client_id")
            ->set_var(Field::REQUIRED, true);

        $this->partner_id = Sub::factory("Partner", "partner_id")
            ->set_var(Field::REQUIRED, true);

        $this->level = Choice::factory("level")
            ->push("1", "Level 1")
            ->push("2", "Level 2")
            ->push("3", "Level 3")
            ->push("T", "Transactional")
            ->push("P", "Pure Protection")
            ->push("B", "Bespoke")
            ->set_var(Field::DISPLAY_NAME, "level");
//            ->set_var(Field::REQUIRED, true);

        $this->percent = Field::factory("percent")
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->method = new Field("method");

        $this->level_when = Date::factory("level_when")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(date('Y-m-d'));

        $this->level_by = Sub::factory("User", "level_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set(date('Y-m-d'));

        $this->updated_by = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->frequency = Choice::factory("frequency")
            ->push("1", "1")
            ->push("2", "2")
            ->push("3", "3")
            ->push("4", "4")
            ->push("5", "5")
            ->push("6", "6")
            ->push("7", "7")
            ->push("8", "8")
            ->push("9", "9")
            ->push("10", "10")
            ->push("11", "11")
            ->push("12", "12")
            ->set_var(Field::DISPLAY_NAME, "level");

        $this->amount = Field::factory("amount")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->SNAPSHOT_LOG = true;

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }


   public function __toString()
   {
        return "$this->level";
   }


public function on_create($post = array())
    {
        //
    }
}
