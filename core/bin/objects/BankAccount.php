<?php
/**
 * Maps a bank entry
 *
 * This is how cash goes from being an entry in our bank account
 * to being processed through our database
 *
 *
 * @package prophet.objects.finance
 * @author kevin.dorrian
 *
 */

class BankAccount extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "bank_account";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->reference = Field::factory('descn');

        $this->amount = Field::factory('amount')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->date = Date::factory('date')
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->assigned_to = Choice::factory("assigned_to")
            ->setSource('select ' . USR_TBL . '.id,concat(first_name," ",last_name) as name from ' . TBL_USERS_GROUPS
                . ' inner join ' . USR_TBL
                . ' on ' . USR_TBL . '.id = ' . TBL_USERS_GROUPS . '.user_id
                          where group_id = 1 and active = 1');

        $this->statement_received = Boolean::factory("statement_received")
            ->set_values(1, 0)
            ->set(0);

        parent:: __construct($id, $autoget);
    }

    public function __toString()
    {
        // Reference (£xxx)
        return "$this->reference ( $this->amount )";
    }
}
