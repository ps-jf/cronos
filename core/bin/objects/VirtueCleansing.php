<?php

class VirtueCleansing extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "virtue_cleansing";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "clientID")
            ->set_var(Field::REQUIRED, true);

        $this->experian_date = Date::factory("experian_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->sent_provider = Date::factory("letter_sent_to_provider")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->tel_pref = Boolean::factory("telephone_preference")
            ->set_values(1, 0);

        parent::__construct($id, $auto_get);
    }
}
