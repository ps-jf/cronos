<?php

class FundMap extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "fund_nb_map";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->nbid = Sub::factory("NewBusiness", "nbid")
            ->set_var(Field::REQUIRED, true);

        $this->fund_id = Sub::factory("Fund", "fund_id")
            ->set_var(Field::REQUIRED, true);

        $this->direction = Choice::factory("direction")
            ->push("1", "In")
            ->push("2", "Out")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Fund Direction");

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
