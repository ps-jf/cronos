<?php

class Group
{
    var $group_name;
    var $field_name_map = [];

    //// Handle a collection of Fields

    public function __construct()
    {
    }

    public static function factory()
    {
        $r = new ReflectionClass(get_called_class());
        return $r->newInstanceArgs(func_get_args());
    }

    public static function type_is_numeric($type)
    {
        $numeric_types = array(
            Field::FLOAT,
            Field::INTEGER,
            Field::CURRENCY,
            Field::PERCENT
        );

        return in_array($type, $numeric_types);
    }

    public function get_field_name_map()
    {
        return $this->field_name_map;
    }

    public function to_sql()
    {
        $fields = [];

        foreach ($this->field_name_map as $name => $prop) {
            $f = $this->{$name};

            if (!$f->edited) {
                continue;
            }

            $fields[] = $f->to_sql();
        }

        return $fields;
    }

    public function load($array)
    {
        if (!is_array($array)) {
            return;
        }

        foreach ($array as $name => $value) {
            if (property_exists($this, $name)) {
                $this->{$name}->set($value);
            }
        }
    }

    public function is_valid()
    {
        try {
            foreach ($this->field_name_map as $x => $db) {
                $f = $this->{$x};
                $f->is_valid();
            }
        } catch (Exception $e) {
            throw new Exception($f->friendly_name() . $e->getMessage());
        }
    }

    public function __set($name, $prop)
    {
        if (is_a($prop, "Field")) {
            $prop->name = $name;
            $prop->group = &$this;
            $this->field_name_map[$name] = $prop->get_field_name(false);
        }

        $this->{$name} = $prop;
    }

    public function friendly_name()
    {
        // convert example_property to "Example Property"
        return ucwords(str_replace("_", " ", $this->group_name));
    }

    function __call($name, $args)
    {
        if (property_exists($this, $name)) {
            return call_user_func_array(array($this->{$name}, "__invoke"), $args);
        }
    }
}
