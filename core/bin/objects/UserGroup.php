<?php

class UserGroup extends DatabaseObject
{

    const DB_NAME = SYS_DB;
    const TABLE = "groups";

    public function __construct($id = false)
    {
        $this->table = GRP_TBL;

        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->name = new Field("name");

        $this->description = new Field("description");

        parent::__construct($id);
    }

    public function __toString()
    {
        return "$this->name";
    }

}
