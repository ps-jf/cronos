<?php

class WorkflowAssigned extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "workflow_assigned";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->group_id = new Sub("UserGroup", "group_id");

        $this->user_id = new Sub("User", "user_id");

        $this->workflow_id = new Sub("Workflow", "workflow_id");

        $this->is_global = Field::factory("is_global");

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }
}


