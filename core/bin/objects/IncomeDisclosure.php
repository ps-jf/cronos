<?php

class IncomeDisclosure extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "income_disclosure_mailing";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "clientid")
            ->set_var(Field::REQUIRED, true);

        $this->week = Field::factory('week');

        $this->pass = Field::factory('pass');

        $this->pass_plain = Field::factory('pass_plain');

        parent:: __construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->pass_plain";
    }

    public static function getPwd($clientid) {

        $s = new Search(new IncomeDisclosure());
        $s->eq("clientid", $clientid);
        $s->add_order('id', 'DESC');
        if ($c = $s->next(MYSQLI_ASSOC)) {
            return $c;
        }
        return false;
    }
}
