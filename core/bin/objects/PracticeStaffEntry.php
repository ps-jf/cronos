<?php

class PracticeStaffEntry extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "practice_staff";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->practice = Sub::factory("Practice", "practice_id")
            ->set_var(Field::REQUIRED, true);

        $this->staff = Sub::factory("Staff", "staff_id");

        $this->partner = Sub::factory("Partner", "partner_id");

        $this->active = Boolean::factory("active");

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function on_create($post = false)
    {
        // Check that either staff or partner set and not both
        if ($this->staff() && $this->partner()) {
            throw new Exception("Assign either Partner or Staff member, not both");
        } elseif (!$this->staff() && !$this->partner()) {
            throw new Exception("Set either Partner or Staff member");
        }

        // Check that there is no existing association between Practice + Staff
        $s = new Search($this);
        $s->eq("practice", $this->practice());

        if ($this->staff()) {
            $entity = "Staff member";
            $s->eq("staff", $this->staff());
        } elseif ($this->partner()) {
            $entity = "Partner";
            $s->eq("partner", $this->partner());
        }

        if ($s->count() > 0) {
            throw new Exception("$entity is already assigned to Practice");
        }
    }

    public function __toString()
    {
        return "$this->practice $this->staff";
    }
}
