<?php
/**
 * Created by PhpStorm.
 * User: Sean
 * Date: 2020-07-24
 * Time: 11:40
 */

class ClientReviewExtension extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "client_review_extensions";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "client_id");

        $this->extra_months = Choice::factory("extra_months")
            ->set_var(Field::REQUIRED, true)
            ->push(1, 1)
            ->push(2, 2)
            ->push(3, 3)
            ->push(4, 4)
            ->push(5, 5)
            ->push(6, 6)
            ->push(7, 7)
            ->push(8, 8)
            ->push(9, 9)
            ->push(10, 10)
            ->push(11, 11)
            ->push(12, 12);

        $this->reason_extended = Field::factory("reason_extended")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->added_by = Sub::factory("WebsiteUser", "added_by")
            ->set(User::get_default_instance("myps_user_id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE)
            ->set(date("Y-m-d H:i:s"));

        $this->original_date = Date::factory("original_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::REQUIRED, true);

        $this->archived_when = Date::factory("archived_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        $this->archived_by = Sub::factory("WebsiteUser", "archived_by");

        $this->reason_archived = Field::factory("reason_archived")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "Extended " . $this->extra_months . " months from " . $this->original_date;
    }

    public function archived()
    {
        return !empty($this->archived_when());
    }

    public function save($return_query = false)
    {
        if(empty($this->id())){
            // new extension
            // check for any open extensions before allowing save
            $s = new Search(new ClientReviewExtension());
            $s->eq("client", $this->client());
            $s->eq("archived_when", null);

            if($ext = $s->next(MYSQLI_ASSOC)){
                throw new Exception("This client already has an active extension. You must archive the active extension before creating a new one");
            }
        } else {
            // updating
            // $cre is the old object before updating
            $cre = new ClientReviewExtension($this->id(), true);

            if($cre->archived_when() != $this->archived_when()){
                // object is being archived
                // make sure we have a reason
                $this->reason_archived->set_var(Field::REQUIRED, true);
            }
        }

        return parent::save($return_query);
    }

    public function checkArchived()
    {
        if($this->archived()){
            return "<span class='fas fa-check text-success'>";
        } elseif(in_array(User::get_default_instance("department"), [4,7,15]) || in_array(User::get_default_instance("id"), [98])){
            return "<button class='btn btn-danger archive-extension' data-id='" . $this->id() . "'><span class='fas fa-archive'></span></button>";
        }

        return "<span class='fas fa-times text-danger'>";
    }
}