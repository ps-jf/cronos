<?php


class ValuationRequest extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "valuation_requests";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "clientID");

        $this->requested_for = Date::factory("requested_for")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->requested_when = Date::factory("requested_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->requested_by = Sub::factory("WebsiteUser", "requested_by")
            ->set(User::get_default_instance("myps_user_id"));

        $this->workflow = Sub::factory("Workflow", "workflow_id");

        $this->notes = Field::factory("notes");

        $this->delivery_method = Choice::factory("delivery_method")
            ->push(1, "Upload Only")
            ->push(2, "Deliver to Partner")
            ->push(3, "Deliver to Client");

        $this->delivery_option = Choice::factory("delivery_option")
            ->push(1, "Upload Only")
            ->push(2, "Standard Delivery")
            ->push(3, "Express Delivery")
            ->push(4, "Email");

        $this->estimate_given = Field::factory("estimate_given");

        $this->servicing_report = Boolean::factory("servicing_report")
            ->set(0);

        $this->auto_ordered = Boolean::factory("auto_ordered")
            ->set(0);

        $this->analytics = Field::factory("analytics");

        $this->myps_calculated_wf_date = Date::factory("myps_calculated_wf_date")
            ->set_var(Date::FORMAT, Date::ISO_8601);

        $this->estimated_delivery_date = Date::factory("estimated_delivery_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->late_delivery_notice_shown = Boolean::factory("late_delivery_notice_shown")
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->request_values = Boolean::factory("request_values")
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->database = REMOTE;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        if (! $this->workflow()) {
            return "No report workflow ticket";
        }
        $s = new Search(new ProreportInvoice());
        $s->eq('workflow_id', $this->workflow());

        $corr = '';
        while (($pri = $s->next()) && $pri->corr()) {

           if (strpos($pri->corr->link(), 'disable-links') !== false) {
               // can't locate corr item
               $corr .= "Corr ID: ".$pri->corr().'<br>';
           } else {
               $corr .= $pri->corr->link().'<br>';
           }
        }

        if (!empty($corr)) {
            return $corr;
        }

        $s1 = new Search(new WorkflowAudit());
        $s1->eq('workid', $this->workflow());

        if ($wfa = $s1->next()) {
            return "Workflow Closed". $wfa->link();
        }

        if ($this->workflow()) {
            return 'Pending Workflow: '.$this->workflow->link();
        }

        return 'Pending..';
    }

    public function save($return_query = false, $update_delivery_date = true)
    {
        $save = parent::save($return_query);

        if($update_delivery_date){
            $this->update_delivery_date();
        }

        return $save;
    }

    public function workflowAuditCheck()
    {
        $s1 = new Search(new WorkflowAudit());
        $s1->eq('workid', $this->workflow());

        if ($wfa = $s1->next()) {
            return $wfa;
        }

        return false;
    }

    public function workflowPendingCheck()
    {
        $s = new Search(new Workflow());
        $s->eq('id', $this->workflow());

        if ($wf = $s->next()) {
            return $wf;
        }

        return false;
    }

    public function update_delivery_date($wf_due = null)
    {
        if($wf_due){
            $newDate = \Carbon\Carbon::createFromTimestamp($wf_due);
        } else {
            $newDate = \Carbon\Carbon::createFromTimestamp($this->workflow->due());
        }

        if(!empty($this->estimated_delivery_date())){
            // get the old date for the note
            $oldDate = \Carbon\Carbon::parse($this->estimated_delivery_date());
        } else {
            $oldDate = addBusinessDays(\Carbon\Carbon::createFromTimestamp($this->workflow->due()), 5);
        }

        if(in_array($this->delivery_option(), [2,3])){
            // report is to be posted, add 5 business days, 2 for mailing house to prepare report, 3 for 1st class delivery
            $this->estimated_delivery_date(addBusinessDays($newDate, 5));
        } else {
            // report will get uploaded/emailed on the day it is due
            $this->estimated_delivery_date($newDate);
        }

        if($newDate->notEqualTo($oldDate)){
            // save the new date
            $this->save(false, false);

            // add a note to the workflow ticket
            $note = new Notes();
            $note->object_type("Workflow");
            $note->object_id($this->workflow());
            $note->note_type("Note");
            $note->subject("Updated Estimated Delivery Date");
            $note->text("Estimated delivery date updated from " . $oldDate->format("d/m/Y") . " to " . $newDate->format("d/m/Y"));

            $note->save();
        }
    }
}