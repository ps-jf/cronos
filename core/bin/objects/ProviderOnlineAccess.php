<?php

class ProviderOnlineAccess extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "provider_online_access";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->practice_id = Sub::factory("Practice", "practice_id");

        $this->staff_id = Field::factory("staff_id");

        $this->partner_id = Field::factory("partner_id");

        $this->issuer_id = Sub::factory("Issuer", "issuer_id");

        $this->active = Boolean::factory("active");

        $this->unipass = Boolean::factory("unipass");

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }

    public function getStaff($practice_id)
    {
        $db = new mydb();

        $db->query("select staff.id, concat(firstname, ' ', surname) as `staffname` " .
            " from " . PRACTICE_STAFF_TBL .
            " inner join " . STAFF_TBL .
            " on " . PRACTICE_STAFF_TBL . ".staff_id = " . STAFF_TBL . ".id " .
            " where practice_id = $practice_id " .
            " and " . PRACTICE_STAFF_TBL . ".active = 1");

        return el::dropdown("provideronlineaccess[staff_id]", $db);
    }

    public function getPartners($practice_id)
    {
        $db = new mydb();

        $db->query("select " . PARTNER_TBL . ".partnerid, concat(partnerfname, ' ', partnersname) as `partnername` " .
            " from " . PRACTICE_STAFF_TBL .
            " inner join " . PARTNER_TBL .
            " on " . PRACTICE_STAFF_TBL . ".partner_id = " . PARTNER_TBL . ".partnerid " .
            " where practice_id = $practice_id " .
            " and " . PRACTICE_STAFF_TBL . ".active = 1");

        return el::dropdown("provideronlineaccess[partner_id]", $db);
    }
}
