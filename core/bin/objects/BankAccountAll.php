<?php

class BankAccountAll extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblbankaccount";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->date = Date::factory('date')
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->reference = Field::factory('descn');

        $this->amount = Field::factory('amount')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->amountdr = Field::factory('amountdr')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->balance = Field::factory('balance')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->category = Choice::factory('categoryid')
            ->setSource('select id, concat(category, " (", sage_nc, ")") from feebase.accounts_categories order by category');

        parent:: __construct($id, $autoget);
    }

    public function __toString()
    {
        // Reference (£xxx)
        return "$this->reference ( $this->amount )";
    }

    public function getSageNC($cat)
    {
        $db = new mydb();
        $db->query("SELECT sage_nc FROM feebase.accounts_categories WHERE category = '" . TRIM($cat) . "'");

        while ($c = $db->next(MYSQLI_ASSOC)) {
            $category = $c['sage_nc'];
        }

        return $category;
    }
}
