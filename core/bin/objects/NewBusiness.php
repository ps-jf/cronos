<?php

class NewBusiness extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblnewbusiness";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("nbID", Field::PRIMARY_KEY);
        $this->policy = Sub::factory("Policy", "policyID")
            ->set_var(Field::REQUIRED, true);

        $this->status = Choice::factory("Status")
            ->set_var(Field::REQUIRED, true)
            ->setSource(NBSTATUS_TBL, "ID", "Description");

        //here begineth the dates
        $this->appReceived = Date::factory("appReceived")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Application Received");

        $this->initialOnHold = Date::factory("initialOnHold")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Initial On Hold Date");

        $this->appDated = Date::factory("appDated")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Application Dated");

        $this->kfIssuedAgreementSigned = Date::factory("kfIssuedAgreementSigned")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "KF Issued/Agreement Signed");

        $this->factfindCompleted = Date::factory("factfindCompleted")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Fact Find Completed");

        $this->illustrationKFD = Date::factory("illustrationKFD")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Illustration/KFD");

        $this->initialIntroLetter = Date::factory("initialIntroLetter")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Initial Introduction Letter to Client");

        $this->clientIdentification = Date::factory("clientIdentification")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Client ID");

        $this->appToProvider = Date::factory("appToProvider")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Application to Provider");

        $this->eoIssued = Date::factory("eoIssued")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "EO Issued");

        $this->eoReturned = Date::factory("eoReturned")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "EO Returned");

        $this->suitabilityIssued = Date::factory("suitabilityIssued")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Suitability Issued");

        $this->doSent = Date::factory("doSent")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "DO Sent");

        $this->providerAcceptanceTerm = Date::factory("providerAcceptanceTerm")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Acceptance from Provider");

        $this->policyDocsReceived = Date::factory("policyDocsReceived")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Policy Docs Received");

        $this->nextReviewDate = Date::factory("nextReviewDate")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Next Review Date");

        $this->breachIssued = Date::factory("breachIssued")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Breach Issued");

        $this->research = Date::factory("research")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Research");

        $this->suitabilityReturned = Date::factory("suitabilityReturned")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Suitability Returned");

        $this->kfIssuedAppSigned = Date::factory("kfIssuedAppSigned")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "KF Issued/App Signed");

        $this->eoinstruction = Date::factory("eoinstruction")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "EO Instruction");

        $this->directflag = Date::factory("directcorecd")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Comm Received");

        $this->notes = Field::factory("notes")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->type = Sub::factory("NewBusinessType", "newBusinessType")
            ->set_var(Field::REQUIRED, true);

        $this->complianceChecked = Boolean::factory("complianceChecked");

        $this->target_market_check = Boolean::factory("target_market_check");

        $this->complianceComments = Field::factory("complianceComments")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->remunerationType = Choice::factory("remunerationType")
            ->push("1", "Invoice to Client")
            ->push("2", "Direct from Product")
            ->push("3", "Partner Account")
            ->push("4", "Invoice SJP");

        $this->remunerationInformation = Field::factory("remunerationInformation")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->registeredUser = Choice::factory("registeredUser")
            ->set_var(Field::REQUIRED, true)
            ->setSource("select prophet.users.id, concat(first_name, \" \", last_name) as staff_mamber " .
                "from prophet.users inner join prophet.users_groups on prophet.users.id = prophet.users_groups.user_id " .
                "where active = 1 and department != 7 and (department = 3 OR group_id = 3) group by users.id order by staff_mamber");

        // This was changed from a boolean to a choice
        // The "No" selection has been assigned as "3" to help deal with all existing tickets at time of writing
        $this->replacement = Choice::factory("replacement")
            ->set_var(Field::REQUIRED, true)
            ->push("1", "Switch")
            ->push("2", "Conversion")
            ->push("3", "No");

        $this->premiumInvestment = Field::factory("premiumInvestment")
            ->set_var(Field::DISPLAY_NAME, "Investment - Withdrawal Amount");

        $this->annuityIncomePayment = Field::factory("annuityIncomePayment")
            ->set_var(Field::DISPLAY_NAME, "Income");

        $this->annuityGuaranteed = Field::factory("annuityGuaranteed")
            ->set_var(Field::DISPLAY_NAME, "Guaranteed Period");

        $this->increasing = Boolean::factory("increasing")
            ->set_var(Field::DISPLAY_NAME, "Increasing");

        $this->annuityWidow = Field::factory("annuityWidow")
            ->set_var(Field::DISPLAY_NAME, "Widows");

        $this->protectBasicSum = Field::factory("protectBasicSum")
            ->set_var(Field::DISPLAY_NAME, "Basic Sum Assured");

        $this->protectMinDeath = Field::factory("protectMinDeath")
            ->set_var(Field::DISPLAY_NAME, "Minimum Death");

        $this->protectCritIllSum = Field::factory("protectCritIllSum")
            ->set_var(Field::DISPLAY_NAME, "CI Sum Assured");

        $this->protectCritIllCover = Field::factory("protectCritIllCover")
            ->set_var(Field::DISPLAY_NAME, "Income");

        $this->protectWaiver = Boolean::factory("protectWaiver")
            ->set_var(Field::DISPLAY_NAME, "Waiver");

        $this->protectTermIll = Boolean::factory("protectTermIll")
            ->set_var(Field::DISPLAY_NAME, "Terminal Illness Included");

        $this->incomeLevel = Field::factory("incomeLevel")
            ->set_var(Field::DISPLAY_NAME, "Income Being Taken");

        $this->incomeDeferred = Field::factory("incomeDeferred")
            ->set_var(Field::DISPLAY_NAME, "Max GAD Income Available");

        $this->incomeDefDisability = Field::factory("incomeDefDisability")
            ->set_var(Field::DISPLAY_NAME, "Income Def Disability");

        $this->investFunds = Field::factory("investFunds")
            ->set_var(Field::DISPLAY_NAME, "Invest Funds");

        $this->investStructuredProduct = Boolean::factory("investStructuredProduct")
            ->set_var(Field::DISPLAY_NAME, "Structured Product");

        $this->investGrowth = Boolean::factory("investGrowth")
            ->set_var(Field::DISPLAY_NAME, "Income");

        $this->premium = Field::factory("premium")
            ->set_var(Field::DISPLAY_NAME, "Premium");

        $this->sumAssured = Field::factory("sumAssured")
            ->set_var(Field::DISPLAY_NAME, "Sum Assured");

        $this->term = Field::factory("term")
            ->set_var(Field::DISPLAY_NAME, "Term");

        $this->funds = Field::factory("funds")
            ->set_var(Field::DISPLAY_NAME, "Funds");

        $this->frequency = Choice::factory("frequency")
            ->set_var(Field::DISPLAY_NAME, "Frequency")
            ->push_array(
                array(
                    "single" => "Single",
                    "monthly" => "Monthly",
                    "quarterly" => "Quarterly",
                    "half-yearly" => "Half-Yearly",
                    "yearly" => "Yearly"
                )
            );

        $this->withProfit = Boolean::factory("withProfit")
            ->set_var(Field::DISPLAY_NAME, "With Profit");

        $this->guaranteed = Boolean::factory("guaranteed")
            ->set_var(Field::DISPLAY_NAME, "Guaranteed");

        $this->retirementAge = Field::factory("retirementAge")
            ->set_var(Field::TYPE, Field::INTEGER)
            ->set_var(Field::DISPLAY_NAME, "Retirement Age");

        $this->productrisk = Choice::factory("productrisk")
            ->push("1", "Low")
            ->push("2", "Medium")
            ->push("3", "High");

        $this->productrisk_reason = Choice::factory("productrisk_reason");

        $this->clientrisk = Choice::factory("clientrisk")
            ->push("1", "Low")
            ->push("2", "Medium")
            ->push("3", "High");

        $this->clientrisk_reason = Choice::factory("clientrisk_reason");

        $this->existing_client = Boolean::factory("existing_client")
            ->set_var(Field::DISPLAY_NAME, "Existing Client");

        $this->review_date = Date::factory("review_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Review Date");

        $this->on_risk_date = Date::factory("on_risk_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "On Risk Date");

        $this->comm_expected = Field::factory("comm_expected")
            ->set_var(Field::DISPLAY_NAME, "Income Expected");

        $this->fee_expected = Field::factory("fee_expected")
            ->set_var(Field::DISPLAY_NAME, "Fee Expected");

        $this->completed = Date::factory("completed")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Completed");

        $this->actual_comm = Field::factory("actual_comm")
            ->set_var(Field::DISPLAY_NAME, "Actual Income/Fee");

        $this->prior_policy = Field::factory("prior_policy")
            ->set_var(Field::DISPLAY_NAME, "Prior Policy Number");

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedWhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "addedBy")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->satisfactionCall = Date::factory("satisfactionCall")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Satisfaction Call");

        $this->recorded_delivery = Field::factory("recorded_delivery");

        $this->typeOfNB = Field::factory("typeOfNB")
            ->set_var(Field::DISPLAY_NAME, "Type of NB");

        $this->funds_in_name = Field::factory("funds_in_name")
            ->set_var(Field::DISPLAY_NAME, "Funds In");

        $this->funds_in_date = Date::factory("funds_in_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Funds In Date");

        $this->funds_out_name = Field::factory("funds_out_name")
            ->set_var(Field::DISPLAY_NAME, "Funds Out");

        $this->funds_out_date = Date::factory("funds_out_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Funds Out Date");

        $this->withdrawlAmount = Field::factory("withdrawlAmount")
            ->set_var(Field::DISPLAY_NAME, "Withdrawal Amount");

        $this->withdrawlMethod = Field::factory("withdrawlMethod")
            ->set_var(Field::DISPLAY_NAME, "Withdrawal Method");

        $this->nb_brand = Choice::factory("nb_brand")
            ->push("SJP", "SJP")
            ->push("Virtue", "Virtue")
            ->push("Own", "Own");

        $this->escalation = Boolean::factory("escalation")
            ->set_var(Field::DISPLAY_NAME, "Escalation")
            ->set_values("1", "0");

        $this->escalation_notes = Field::factory("escalation_notes")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE, "Escalation_Notes");

        $this->statusReason = Field::factory("statusReason")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE, "statusReason");

        $this->declined_npw_when = Field::factory("declined_npw_when");

        $this->declined_npw_by = Field::factory("declined_npw_by");

        $this->case_type = Choice::factory("case_type")
            ->push("1", "Paper")
            ->push("2", "Online")
            ->set_var(Field::REQUIRED, true);

        $this->case_checker = Field::factory("case_checker")
            ->set_var(Field::REQUIRED, true);

        $this->eo_admin_fee = Boolean::factory("eo_admin_fee")
            ->set_var(Field::DISPLAY_NAME, "Eo Admin Fee");

        $this->case_received = Date::factory("case_received")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Case Received");

        $this->client_agreement_signed = Date::factory("client_agreement_signed")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Client Agreement Signed");

        $this->risk_assessment_report = Date::factory("risk_assessment_report")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Risk Assessment Report");

        $this->telephone_call = Date::factory("telephone_call")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Telephone Call");

        $this->CAN_DELETE = true;

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    public function save($return_query = false)
    {
        if (in_array(User::get_default_instance("department"), [3])){
            // New Business Admin Team
            if (!empty($this->id())){
                // Allow new tickets to be saved without checks
                if (in_array($this->type(), [1, 12, 13, 17, 18])){
                    // Ticket type is in [EO, Full Withdrawal, Partial Withdrawal, Advice Liability, Switch to Cash]
                    if (in_array($this->policy->type(), [17])){
                        // Policy type is unknown
                        throw new Exception("Policy type is currently set to unknown, please update the policy type before saving this ticket");
                    }

                    if(empty($this->clientrisk())){
                        throw new Exception("Client Risk is a required field");
                    }

                    if(empty($this->clientrisk_reason())){
                        throw new Exception("Client Risk Reason is a required field");
                    }

                    if(empty($this->productrisk())){
                        throw new Exception("Product Risk is a required field");
                    }

                    if(empty($this->productrisk_reason())){
                        throw new Exception("Product Risk Reason is a required field");
                    }
                }
            }
        }

        return parent::save($return_query);
    }

    public function get_policy_fields()
    {
        //// @return array of policy Fields relevant to this NewBusiness Policy's type

        $policy_type = $this->policy->get_object()->type;
        $array = [];

        if (strlen($policy_type->new_business_form_fields) > 0) {
            $fields = unserialize($policy_type->new_business_form_fields);
            $array = [];
            foreach ($fields as $name) {
                $array[$name] = $this->{$name};
            }
        }

        return $array;
    }

    public function get_date_fields()
    {
        //// @return array of date Fields relevant to this NewBusinessType

//        $array = [];
//
//        if (strlen($this->type->date_fields)) {
//            $fields = unserialize($this->type->date_fields);
//
//            foreach ($fields as $name) {
//                $array[$name] = $this->{$name};
//            }
//        }
//        return $array;

        return json_decode($this->type->date_fields);
    }

    public function get_latest_action()
    {
        /*
           Return a name of last action taken and the date it occurred 
           e.g. "Application to Provider - 08/09/2011" 
        */

        $array = [];

        if (strlen($this->type->date_fields)) {
            $fieldsArray = json_decode($this->type->date_fields);

            $fields = [];

            foreach($fieldsArray as $a){
                $fields = array_merge($fields, $a->fields);
            }

            foreach ($fields as $name) {
                $array[$name] = $this->$name();
            }
        }

        $new_array = array_reverse($array);

        $key = "";
        $value = "";

        foreach ($new_array as $k => $v) {
            if ($v != null) {
                $key = $k;
                $value = $v;
                break;
            }
        }

        return $key . "<br />" . $value;
    }

    public function current_and_archived_users($id = false)
    {
        //// Return a dropdown list of Users for searching
        //// Newbusiness cases.

        /* #TODO -- find a cleaner way of implementing methods like this.
           This will probably involve developing a new Field subclass 
           which represents a many-many relationship. */

        $db = new mydb($this->database);

        $db->query(
            "select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) " .
            "from " . USR_TBL . " " .
            "inner join " . TBL_USERS_GROUPS . " " .
            "on " . TBL_USERS_GROUPS . ".user_id = " . USR_TBL . ".id " .
            "where group_id = '3'"
        );

        ($id !== false) or $id = $this->get_field_name();

        return el::dropdown($id, $db);
    }

    public function current_and_registered_users($id = false, $current_reg = false)
    {
        //// Return a dropdown list of current users and the current registered user even if they are in active

        $db = new mydb($this->database);

        $db->query(
            "select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) " .
            "from " . USR_TBL . " " .
            "inner join " . TBL_USERS_GROUPS . " " .
            "on " . TBL_USERS_GROUPS . ".user_id = " . USR_TBL . ".id " .
            "where group_id = '3' and (users.active = 1 OR users.id = " . $current_reg . ")"
        );

        ($id !== false) or $id = $this->get_field_name();

        return el::dropdown($id, $db);
    }

    public function on_update($post = false)
    {
        //this case has now been completed
        if (isset($post["status"])) {
            if ($post["status"] == 2 && $this->completed == "") {
                try {
                    $db = new mydb($this->database);
                    $db->query("update `" . DATA_DB . "`.tblnewbusiness set completed = '" . date('Y-m-d') . "' where nbid = " . $this->id());
                } catch (Exception $e) {
                    throw $e;
                }
            }
        }
    }

    public function product_risk_reason($riskval)
    {
        $dd = "";
        //this case has now been completed
        if (isset($riskval)) {
            $dd .= "<option value='0'>-- Product List --</option>";

            // load from DB
            $db = new mydb();

            $query = "SELECT id, reason FROM " . DATA_DB . ".nb_product_risk_reason WHERE risk = " . $riskval;

            $db->query($query);

            while($r = $db->next(MYSQLI_ASSOC)){
                $dd .= "<option value='" . $r['id'] . "'>" . $r['reason'] . "</option>";
            }

//            if ($riskval == 1) {
//                $dd .= "<option value='1'>Term Life Assurance</option>";
//                $dd .= "<option value='2'>Income Protection</option>";
//                $dd .= "<option value='3'>Critical Illness</option>";
//                $dd .= "<option value='4'>Occupational Pension</option>";
//                $dd .= "<option value='5'>CIMPs</option>";
//                $dd .= "<option value='6'>COMPs</option>";
//                $dd .= "<option value='7'>Final Salary</option>";
//                $dd .= "<option value='8'>Group AVCs</option>";
//                $dd .= "<option value='9'>Rebate Only Personal Pensions</option>";
//            } else if ($riskval == 2) {
//                $dd .= "<option value='10'>Whole of Life</option>";
//                $dd .= "<option value='11'>Life Assurance Savings Plan</option>";
//                $dd .= "<option value='12'>Endowments</option>";
//                $dd .= "<option value='13'>Group Stakeholder</option>";
//                $dd .= "<option value='14'>Income Drawdown</option>";
//                $dd .= "<option value='15'>Flexible Pension Plan</option>";
//                $dd .= "<option value='16'>Phased Retirement Plan</option>";
//                $dd .= "<option value='17'>FSAVC</option>";
//                $dd .= "<option value='18'>Stakeholder Plan</option>";
//                $dd .= "<option value='19'>Personal Pension</option>";
//                $dd .= "<option value='20'>Immediate Vesting PP</option>";
//                $dd .= "<option value='21'>Group Personal Pension</option>";
//                $dd .= "<option value='22'>CPA</option>";
//                $dd .= "<option value='23'>ISA</option>";
//                $dd .= "<option value='24'>OMO</option>";
//                $dd .= "<option value='25'>With Profit Pension Annuity</option>";
//                $dd .= "<option value='26'>PLA</option>";
//                $dd .= "<option value='27'>Hancock Annuity</option>";
//                $dd .= "<option value='28'>Pension Transfers</option>";
//                $dd .= "<option value='43'>DFM</option>";
//            } else if ($riskval == 3) {
//                $dd .= "<option value='29'>Single Premiun Investment Bond</option>";
//                $dd .= "<option value='30'>With Profit Bonds</option>";
//                $dd .= "<option value='31'>Guaranteed Bonds</option>";
//                $dd .= "<option value='32'>Investment Bonds</option>";
//                $dd .= "<option value='33'>Offshore International Bonds</option>";
//                $dd .= "<option value='34'>EPPS (excl. CIMPS/COMPS)</option>";
//                $dd .= "<option value='35'>SSAAS</option>";
//                $dd .= "<option value='36'>SIPP</option>";
//                $dd .= "<option value='37'>TIPP</option>";
//                $dd .= "<option value='38'>Unit Trust</option>";
//                $dd .= "<option value='39'>OEIC</option>";
//                $dd .= "<option value='40'>Venture Capital Trust</option>";
//                $dd .= "<option value='41'>Enterprise Investment Scheme</option>";
//                $dd .= "<option value='42'>Guaranteed Minimum Pension</option>";
//            }
        }

        return $dd;
    }

    public function client_risk_reason($riskval)
    {
        $dd = "";
        //this case has now been completed
        if (isset($riskval)) {
            $dd .= "<option value='0'>-- Typical Client Attributes --</option>";

            //load from DB
            $db = new mydb();

            $query = "SELECT id, reason FROM " . DATA_DB . ".nb_client_risk_reason WHERE risk = " . $riskval;

            $db->query($query);

            while($r = $db->next(MYSQLI_ASSOC)){
                $dd .= "<option value='" . $r['id'] . "'>" . $r['reason'] . "</option>";
            }

//            if ($riskval == 1) {
//                $dd .= "<option value='1'>Existing Client Greater than 1 yr relationship </option>";
//                $dd .= "<option value='2'>Client is Employed</option>";
//                $dd .= "<option value='3'>Earnings can be Verified</option>";
//                $dd .= "<option value='4'>Face to Face Client</option>";
//                $dd .= "<option value='5'>Original source of funds easily identified</option>";
//            } else if ($riskval == 2) {
//                $dd .= "<option value='6'>One off small/medium transaction</option>";
//                $dd .= "<option value='7'>Source of funds not easily identified</option>";
//                $dd .= "<option value='8'>Introduced business</option>";
//            } else if ($riskval == 3) {
//                $dd .= "<option value='9'>Politically exposed person </option>";
//                $dd .= "<option value='10'>Customers behavior</option>";
//                $dd .= "<option value='11'>One off client</option>";
//                $dd .= "<option value='12'>Unknown Client</option>";
//                $dd .= "<option value='13'>No Ongoing Relationship</option>";
//                $dd .= "<option value='14'>Complex business structure</option>";
//                $dd .= "<option value='15'>High Risk Jurisdiction</option>";
//                $dd .= "<option value='16'>Source of funds unable to be traced</option>";
//                $dd .= "<option value='17'>Suspicious behavior</option>";
//                $dd .= "<option value='18'>Undue level of secrecy</option>";
//                $dd .= "<option value='19'>Non face-to-face acceptance</option>";
//            }
        }

        return $dd;
    }

    public function case_checker_drop($staff = false)
    {
        $db = new mydb;

        $db->query("select prophet.users.id, concat(first_name, \" \", last_name) as staff_member " .
            "from prophet.users inner join prophet.users_groups on prophet.users.id = prophet.users_groups.user_id " .
            "where active = 1 and department != 7 and (department = 3 OR group_id = 3) group by users.id order by staff_member");

        $elem = "<select id='newbusiness[case_checker]' name='newbusiness[case_checker]'>";

        $elem .= "<option>--</option>";
        $elem .= "<option " . ($staff == 999 ? 'selected' : '') . " value='999'>Keyed at Partners Office</option>";

        while ($r = $db->next(MYSQLI_ASSOC)){
            $elem .= "<option " . ($staff == $r['id'] ? 'selected' : '') . " value='" . $r['id'] . "'>" . $r['staff_member'] . "</option>";
        }

        $elem .= "</select>";

        return $elem;
    }

    public function __toString()
    {
        // "policy number ( nb ID )"
        return "$this->policy ( $this->id )";
    }

    public function adviceDropdown()
    {
        $types = ["Drawdown", "DFM Review", "Pension Income Review", "Investment Income Review",
            "Increase to Contributions (ISA)", "Increase to Contributions (Pension)", "BED & ISA", "Lifetime Annuity",
            "Pension Fund Review", "Investment Fund Review", "Fund Switch", "Top Up - Pension", "Top Up - Investment",
            "Pension Transfer", "Death Transfer", "UFPLS"];

        $dropdown = "<select name='newbusiness[typeOfNB]' id='newbusiness[typeOfNB]' style='width: 95%'>";
        $dropdown .= "<option value=''>--</option>";

        foreach ($types as $type){
            $dropdown .= "<option " . (($this->typeOfNB() == $type) ? 'selected' : '') . ">" . $type . "</option>";
        }

        $dropdown .= "</select>";

        return $dropdown;
    }

    public function lastNote()
    {
        $s = new Search(new Notes());

        $s->eq("object_type", "NewBusiness");
        $s->eq("object_id", $this->id());
        $s->eq("is_visible", 1);
        $s->limit(1);

        return $s->next(MYSQLI_ASSOC);
    }

    public function checkforNote($type)
    {
        $s = new Search(new Notes());

        $s->eq("object_type", "NewBusiness");
        $s->eq("object_id", $this->id());
        $s->eq("note_type", $type);
        $s->eq("is_visible", 1);

        if($s->next(MYSQLI_ASSOC)){
            return true;
        } else {
            return false;
        }
    }
}
