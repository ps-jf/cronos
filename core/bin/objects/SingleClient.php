<?php
/**
 * Maps a client from the database.
 *
 *
 * We use python on the call machine to enter phone calls into the database
 * From here we can map those in and apply them to profiles, analyse them etc
 *
 * Any changes here need to be reflected in the following places:
 * bouncer -> auto address updater: Iwe build a custom serialized object
 *
 * @package prophet.objects.client
 * @author daniel.ness
 */

class SingleClient extends Group
{
    var $_exists = false;

    public function __construct($index)
    {
        $this->title = Title::factory("clientTitle$index")
            ->set_var(Field::DISPLAY_NAME, "Title $index");

        $this->forename = Field::factory("clientFname$index")
            ->set_var(Field::DISPLAY_NAME, "Forename $index");

        $this->surname = Field::factory("clientSname$index")
            ->set_var(Field::DISPLAY_NAME, "Surname $index");

        $this->dob = Date::factory("clientDOB$index")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::DISPLAY_NAME, "DOB $index")
            ->set_var(Field::PATTERN, "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/");

        $this->nino = Field::factory("clientNINO$index")
            ->set_var(Field::PATTERN, "/^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFMP]{0,1}$/")
            ->set_var(Field::MAX_LENGTH, 9)
            ->set_var(Field::DISPLAY_NAME, "NINo $index");

        $this->lei = Field::factory("clientLEI$index")
            ->set_var(Field::DISPLAY_NAME, "LEI $index");

        $this->email = Field::factory("email$index")
            ->set_var(Field::DISPLAY_NAME, "Email $index")
            ->set_var(Field::MAX_LENGTH, 320)
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->card = Boolean::factory("card$index");

        $this->client_deceased = Boolean::factory("client" . $index . "_deceased");

        $this->deceased_date = Date::factory("c" . $index . "_deceased_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Client $index Deceased Date");

        $this->mob_phone = Field::factory("client" . $index . "_mobno")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->nationality = Choice::factory("nationality$index")
            ->setSource("select nationality, nationality from prophet.countries group by nationality");

        $this->residency = Choice::factory("residency$index")
            ->setSource("select alpha_3_code, en_short_name from prophet.countries group by en_short_name");

        $this->professional_reason = Field::factory("clientpro_reason_$index")
            ->set_var(Field::DISPLAY_NAME, "Professional Reason $index")
            ->set_var(Field::MAX_LENGTH, 320);
    }

    public function exists()
    {
        // for a client to "exist", they must have either forename & surname, or title & surname
        return ($this->name("t|f s"));
    }

    public function name($format = "t f s")
    {
        $str = (string)$format;
        $out = "";

        // pre-parse or ( | ) switches

        if (preg_match('/([tfs])(\|[tfs])+/', $format, $m)) {
            $full_match = array_shift($m);
            foreach ($m as $match) {
                if ($n = $this->name(str_replace("|", "", $match))) {
                    $n = preg_replace('/([tfs])/', '\\\$1', $n);
                    $str = str_replace($full_match, $n, $str);
                    break;
                }
            }
            $str = preg_replace("/\|/", "", $str);
        }

        for ($i = 0; $i < strlen($str); $i++) {
            switch ($str[$i]) {
                case "\\":
                    // escape character, append next character
                    if (strlen($str) > ++$i) {
                        $out .= $str[$i];
                    }

                    break;

                default:
                    if (isset($str[$i])) {
                        if (in_array($str[$i], ["t", "f", "s"])) {
                            foreach (["title", "forename", "surname"] as $x) {
                                if ($x[0] == $str[$i]) {
                                    $y = trim((string)$this->$x);
                                    if (strlen($y)) {
                                        $out .= $y;
                                        break;
                                    }
                                }
                            }
                        } else {
                            $out .= $str[$i];
                        }
                    }

                    break;
            }
        }

        $out = trim($out);
        return (strlen($out)) ? $out : false;
    }

    public function __toString()
    {
        // John Smith
        if ($this->forename && $this->surname) {
            return $this->forename . " " . $this->surname;
        }

        // just surname
        return "$this->surname";
    }

    public function age()
    {
        if ($this->dob()){
            return \Carbon\Carbon::parse($this->dob())->age;
        }
    }

    public function vulnerableAge()
    {
        if ($this->age() < 18 || $this->age() >= 75){
            return true;
        } else {
            return false;
        }
    }

    public function geo_area()
    {
        $db = new mydb();

        if(!empty($this->residency())){
            $db->query("SELECT geo_area FROM prophet.countries WHERE alpha_3_code = '" . $this->residency() . "'");

            if($c = $db->next(MYSQLI_ASSOC)){
                return $c['geo_area'];
            }
        }

        return false;
    }
}
