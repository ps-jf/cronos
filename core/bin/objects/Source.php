<?php

class Source extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tbl_source";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("ID", Field::PRIMARY_KEY);

        $this->issuer = Sub::factory("Issuer", "issuer_ID");

        $this->ref = Field::factory("ref");

        $this->comm = Field::factory("comm");

        $this->policyNum = Field::factory("policyNum");

        $this->clientFname1 = Field::factory("clientFname1");

        $this->clientSname1 = Field::factory("clientSname1");

        $this->comm_total = Field::factory("comm_total");

        $this->paiddate = Field::factory("paiddate");

        $this->agency_no = Field::factory("agency_no");

        $this->batchid = Field::factory("batch_id");

        $this->bankid = Sub::factory("BankAccount", "id");

        $this->commtype = Field::factory("commtype_ps");

        $this->issuer_ID = Field::factory("issuer_ID");

        $this->addedby = Field::factory("Added_by");

        $this->partner = Sub::factory("Partner", "partnerID");

        $this->client = Sub::factory("Client", "clientID");

        $this->polnum_matched = Field::factory("polnum_matched");

        $this->policy = Sub::factory("Policy", "policyID");

        $this->filename = Field::factory("filename");

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->currency_type = Choice::factory("currency_type")
            ->push('GBP', "GBP")
            ->push('HKD', "HKD")
            ->push('USD', "USD")
            ->push('EUR', "EUR")
            ->set('GBP');

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }

    public function link($text = false)
    {
        return parent::link($text);
    }

    public function source_to_commission()
    {
        /*
        After file has been imported to tbl_source, try and match any entries to policies that may already exist.
        If policy already exists, update entries in tbl_source, add entries to tblcommission and remove
        matched/processed entries from tbl_source.
        */

        $json2 = (object)[
            "id" => null,
            "status" => false,
            "error" => null
        ];

        try {
            $db = new mydb();

            // Update entries in tbl_source to show policy id of matched policy.  Update matched flag to true ( -1 ).
            $update_source = $db->query("UPDATE tbl_source " .
                "INNER JOIN tblpolicy " .
                "ON (tbl_source.policyNum = tblpolicy.policyNum ) AND (tbl_source.issuer_ID = tblpolicy.issuerID) " .
                "SET tbl_source.policyID = tblpolicy.policyID, tbl_source.policynum_matched = '-1' where tblpolicy.archived is null");

            if ($update_source === false) {
                $json2->status = false;
                $json2->error = "failed to update matched policies in tbl_source, contact IT";
                return $json2;
            }

            // Insert new entries into tblcommission for newly created clients / policies
            $insert_comm = $db->query("INSERT INTO tblcommission ( editedWhen, batchID, amount, description, commissiontype, policyID, editedBy, currency_type) " .
                "SELECT Now() AS date, tbl_source.batch_id, tbl_source.comm, tbl_source.Added_by, tbl_source.commtype_ps, tbl_source.policyID, tbl_source.Added_by, tbl_source.currency_type " .
                "FROM tbl_source " .
                "INNER JOIN tblpolicy " .
                "ON tbl_source.policyid = tblpolicy.policyid " .
                "WHERE tbl_source.comm >= 0 AND tbl_source.commtype_ps = 0 AND tbl_source.policynum_matched = '-1' AND tblpolicy.archived is null " .
                "ORDER BY tbl_source.id");

            if ($insert_comm === false) {
                $json2->status = false;
                $json2->error = "failed to transfer matched entries to tblcommission, contact IT";
                return $json2;
            }

            // Remove entries from tbl_source after entries have been added to tblcommission
            $remove_source = $db->query("DELETE tbl_source.* " .
                "FROM tbl_source " .
                "INNER JOIN tblpolicy " .
                "ON tbl_source.policyID = tblpolicy.policyID " .
                "WHERE (((tbl_source.comm)>=0) AND ((tbl_source.commtype_ps)=0) AND ((tbl_source.policynum_matched)='-1') AND (tblpolicy.archived is null))");

            if ($remove_source === false) {
                $json2->status = false;
                $json2->error = "failed to remove matched entries from tbl_source, contact IT";
                return $json2;
            }

            $json2->status = true;
        } catch (Exception $e) {
            $json2->status = false;
            $json2->error = $e->getMessage();
        }

        return $json2->status;
    }

    public function get_unmatched($batchid)
    {
        $db = new mydb();

        // Obtain unmatched entries and display back to user
        $unmatched = [];

        $db->query("SELECT ref, policyNum, clientfname1, clientsname1, agency_no, commtype, comm FROM feebase.tbl_source WHERE batch_id = " . $batchid . " AND policynum_matched = 0");

        while ($d = $db->next()) {
            $unmatched[] = $d;
        }

        return $unmatched;
    }
}
