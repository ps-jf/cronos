<?php

class AccountReviewAudit extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "account_review_audit";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partner")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->recipient = Field::factory("recipient");

        $this->email = Field::factory("email_body");

        $this->sent_when = Date::factory("sent_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->sent_by = Sub::factory("User", "sent_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));


        parent::__construct($id, $auto_get);

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;
    }
}
