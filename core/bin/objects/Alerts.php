<?php

class Alerts extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "alerts";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object = Field::factory("object")
            ->set_var(Field::MAX_LENGTH, 45);

        $this->object_id = Field::factory("object_id")
            ->set_var(Field::MAX_LENGTH, 45);

        $this->is_added = Field::factory("is_added")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->object_added = Field::factory("object_added")
            ->set_var(Field::MAX_LENGTH, 45);

        $this->added_active = Field::factory("added_active")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->is_updated = Field::factory("is_updated")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->update_active = Field::factory("update_active")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->addedwhen = Date::factory("addedWhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "addedBy")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->is_active = Field::factory("is_active")
            ->set_var(Field::MAX_LENGTH, 1);


        //adviser charging alert fields
        $this->pack_produced = Field::factory("pack_produced")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->form_to_provider = Field::factory("form_to_provider")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->ac_applied = Field::factory("ac_applied")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->ac_exempt = Field::factory("ac_exempt")
            ->set_var(Field::MAX_LENGTH, 1);

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    function __toString()
    {
        return "$this->id";
    }
}
