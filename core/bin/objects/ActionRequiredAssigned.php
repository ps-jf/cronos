<?php

class ActionRequiredAssigned extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "action_required_assigned";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->action_required = Sub::factory("ActionRequired", "ar_id");

        $this->assigned = Sub::factory("WebsiteUser", "user_id");

        $this->addedby = Sub::factory("WebsiteUser", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->SNAPSHOT_LOG = true;
        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }
}