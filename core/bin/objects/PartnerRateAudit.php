<?php

class PartnerRateAudit extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "partnerrate_audit";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "PartnerID")
            ->set_var(Field::REQUIRED, true);

        $this->monthsum_12 = Field::factory("12mnthsum")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->oldrate = Field::factory("oldrate")
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->newrate = Field::factory("newrate")
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->date = Date::factory("Date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        parent::__construct($id, $auto_get);
    }
}
