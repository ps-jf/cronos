<?php

class LifeEventType extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "life_events_type";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);
        $this->desc = Field::factory("desc");
        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->desc";
    }
}
