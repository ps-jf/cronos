<?php

class AdditionalEmail extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "additional_email";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("ID", Field::PRIMARY_KEY);

        $this->partnerID = Field::factory("partner_id")
            ->set_var(Field::MAX_LENGTH, 10);

        $this->staffID = Field::factory("staff_id")
            ->set_var(Field::MAX_LENGTH, 10);

        $this->email = Field::factory("email")
            ->set_var(Field::MAX_LENGTH, 255)
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->tag = Choice::factory("tag")
            ->push("1", "Work")
            ->push("2", "Personal")
            ->set_var(Field::DISPLAY_NAME, "Tag");

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);

    }

    public function __toString()
    {
        return "$this->email";
    }
}
