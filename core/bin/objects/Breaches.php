<?php

class Breaches extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "breaches";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->reported_by = Choice::factory("reported_by")
            ->setSource("select prophet.users.id, concat(first_name, \" \", last_name) as staff_mamber " .
                "from prophet.users " .
                "order by staff_mamber")
            ->set_var(Field::REQUIRED, true);

        $this->root_cause = Choice::factory("root_cause")
            ->setSource("select prophet.users.id, concat(first_name, \" \", last_name) as staff_mamber " .
                "from prophet.users " .
                "order by staff_mamber")
            ->set_var(Field::REQUIRED, true);

        $this->cause_type = Choice::factory("cause_type")
            ->push("People", "People")
            ->push("Process", "Process")
            ->push("System", "System")
            ->set_var(Field::REQUIRED, true);

        $this->breach_type = Choice::factory("breach_type")
            ->push("Data Protection", "Data Protection")
            ->push("Complaints", "Complaints")
            ->push("Code of Conduct", "Code of Conduct")
            ->push("Other", "Other");

        $this->caused_by = Field::factory('caused_by');

        $this->description = Field::factory('description')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE)
            ->set_var(Field::REQUIRED, true);

        $this->breach_date = Date::factory("breach_date")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->breach_identified = Date::factory("breach_identified")
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->breach_effect = Field::factory('breach_effect')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE)
            ->set_var(Field::REQUIRED, true);

        $this->action_taken = Field::factory('action_taken')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE)
            ->set_var(Field::REQUIRED, true);

        $this->action_taken_date = Date::factory("action_taken_date")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->action_taken_due = Date::factory("action_taken_due")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(\Carbon\Carbon::now()->addMonth()->format("U"));

        $this->previous_incidences = Choice::factory("previous_incidences")
            ->push(1, "Yes")
            ->push(0, "No");

        $this->subjects_effected = Field::factory('subjects_effected')
            ->set_var(Field::REQUIRED, true);

        $this->hdt = Field::factory('hdt');

        $this->ico_ref = Field::factory('ico_ref');

        $this->reported_to_ico = Boolean::factory("reported_to_ico");

        $this->reported_reason = Field::factory('reported_reason')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->ico_response = Field::factory('ico_response')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->ps_action_required = Field::factory('ps_action_required')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->added_when = Date::factory("added_when")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(time());

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(time());

        $this->updated_by = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->archived_when = Date::factory("archived_when")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->archived_by = Sub::factory("User", "archived_by")
            ->set_var(Field::BLOCK_INSERT, true);

        $this->manager_response = Field::factory('manager_response')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->correspondence = Field::factory("corrID");

        $this->incorrect_partner = Field::factory("incorrect_partner");

        $this->correct_partner = Field::factory("correct_partner");

        $this->incorrect_client = Field::factory("incorrect_client");

        $this->correct_client = Field::factory("correct_client");

        $this->incorrect_policy = Field::factory("incorrect_policy");

        $this->correct_policy = Field::factory("correct_policy");

        $this->send_out = Boolean::factory("send_out")
            ->set(0);

        $this->myps_scan = Boolean::factory("myps_scan")
            ->set(0);

        $this->processes = Field::factory("processes");

        $this->assigned = Choice::factory("assigned")
            ->setSource("select prophet.users.id, concat(first_name, \" \", last_name) as staff_mamber " .
                        "from prophet.users " .
                        "where active = 1 " .
                        "order by staff_mamber");

        $this->other_comments = Field::factory("other_comments")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    /**
     * @param bool $selected
     * @return string
     * @throws Exception
     */
    public static function allUsers($selected = false)
    {
        $select = "<select id='breaches[caused_by_multiple]' name='breaches[caused_by_multiple]' multiple>";

        $s = new Search(new User());
        $s->add_order('username', 'ASC');
        if ($selected) {

            $user_array = explode(',', $selected);

            while ($user = $s->next('MYSQLI_ASSOC')) {
                if (in_array($user->id(), $user_array)) {
                    $select .= "<option value='" . $user->id() . "' selected>" . $user . "</option>";
                } else {
                    $select .= "<option value='" . $user->id() . "'>" . $user . "</option>";
                }
            }

        } else {
            while ($user = $s->next('MYSQLI_ASSOC')) {
                $select .= "<option value='" . $user->id() . "'>" . $user . "</option>";
            }
        }

        $select .= "</select>";

        return $select;
    }

    /**
     * @param $caused_by
     * @return string
     */
    public static function causedBy($caused_by, $withIDs = false)
    {
        $user_array = explode(',', $caused_by);
        $len = count($user_array);
        $str = "";
        $i = 0;

        foreach ($user_array as $key => $user) {
            $u = new User($user, true);
            $i++;

            if ($withIDs){
                $listItem = "<span data-id='" . $user . "'>" . $u->staff_name() . "</span>";
            } else {
                $listItem = $u->staff_name();
            }

            ($i < $len) ? $str .= $listItem . ", " : $str .= $listItem;
        }

        if ($str != "") {
            $str = truncate($str, 100, true);
        }

        return $str;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "$this->id";
    }

    /**
     * On create of a new breach we want to ensure any uploaded file are linked to the new breach object.
     * We also want to email the 'causer' and copy in compliance help desk.
     *
     * @param bool $post
     * @throws Exception
     */
    public function on_create($post = false)
    {
        try {
            if ($_POST['file-ids']) {
                // if any files were uploaded by user we need to map them to the newly added breach
                $files_array = explode(",", $_POST['file-ids']);
                foreach ($files_array as $key => $file_id) {
                    $file = new FileMapping($file_id, true);
                    $file->object_id($this->id());
                    $file->save();
                }
            }

            if ($this->breach_date()){
                $breach_date = \Carbon\Carbon::createFromTimestamp($this->breach_date())->toFormattedDateString();
            } else {
                $breach_date = "Not Given";
            }

            $root_cause = new User($post['root_cause'], true);

            // Send email to user with their username and password
            $local_template['html'] = new Template("sparkpost/emailTemplates/internalBreach/newBreach.html", [
                "breach" => $this,
                "caused_by" => $root_cause,
                "breach_date" => $breach_date
            ]);
            $local_template['text'] = new Template("sparkpost/emailTemplates/internalBreach/newBreach.txt", [
                "breach" => $this,
                "caused_by" => $root_cause,
            ]);

                // team leader/heads of mapping
                $map = [
                    29 => "kevin.dorrian@policyservices.co.uk",             // avril
                    44 => "kayleigh.dorrian@policyservices.co.uk",          // blair
                    45 => "kevin.dorrian@policyservices.co.uk",             // diane
                    87 => "kevin.dorrian@policyservices.co.uk",             // lewis
                    98 => "kayleigh.dorrian@policyservices.co.uk",          // louise gibson
                    99 => "avril.braes@policyservices.co.uk",               // lisa
                    116 => "louise.gibson@policyservices.co.uk",            // michelle
                    141 => "michelle.fraser@policyservices.co.uk",          // catherine
                    185 => "louise.gibson@policyservices.co.uk",            // louise mcgillivray
                    192 => "louise.gibson@policyservices.co.uk",            // sarah ireland
                    203 => "avril.braes@policyservices.co.uk",              // charmaine
                ];

            $from = [
                'name' => 'Policy Services - Compliance',
                'email' => 'compliance@policyservices.co.uk',
            ];

            $s = new Search(new FileMapping());
            $s->eq('object', 'Breaches');
            $s->eq('object_id', $this->id());
            $file_array = [];

            $i = 0;
            while ($files = $s->next(MYSQLI_OBJECT)) {
                $file_array[$i]['name'] = $files->display_name();
                $file_array[$i]['type'] = $files->mime_type();
                $file_array[$i]['data'] = base64_encode(file_get_contents($files->get_file_path()));
                $i++;
            }

            $recipient = [];

            $recipient[] = [
                'address' => [
                    'name' => "Compliance",
                    'email' => "compliance@policyservices.co.uk"
                ]
            ];

                if (array_key_exists($post['root_cause'], $map)) {
                    // caused by Head Of/Team Leader, use mapping above
                    $recipient[] = [
                        'address' => [
                            'email' => $map[$post['root_cause']],
                                'name' => $map[$post['root_cause']],
                            ]
                    ];
                } else {
                    $caused_by = new User($post['root_cause'], true);

                    // use department mapping
                    if ($caused_by->department() == 14 || $caused_by->department() == 15) {
                        // IO or Client Relations- send to Michelle
                        $recipient[] = [
                            'address' => [
                                'email' => "michelle.fraser@policyservices.co.uk",
                                'name' => "michelle.fraser@policyservices.co.uk",
                            ]
                        ];
                    } else if ($caused_by->department() == 6 || $caused_by->weekend_staff() == 1) {
                        // Client Services - send to Catherine
                        $recipient[] = [
                            'address' => [
                                'email' => "catherine.McClelland@policyservices.co.uk",
                                'name' => "catherine.McClelland@policyservices.co.uk",
                            ]
                        ];
                    } elseif ($caused_by->department() == 1){
                        $recipient[] = [
                            'address' => [
                                'email' => "charmaine.quinn@policyservices.co.uk",
                                'name' => "charmaine.quinn@policyservices.co.uk",
                            ]
                        ];
                    } else {
                        // search DB to try to predict team leader
                        $s = new Search(new User());
                        $s->eq('department', $caused_by->department());
                        $s->eq('team_leader', 1);
                        $s->eq('active', 1);

                        if ($team_leader = $s->next('MYSQLI_ASSOC')) {
                            $recipient[] = [
                                'address' => [
                                    'email' => $team_leader->email(),
                                    'name' => $team_leader->staff_name(),
                                ]
                            ];
                        }
                    }
                }

//                Dont send email to person who caused beach anymore
//                $recipient[] = [
//                    'address' => [
//                        'email' => $caused_by->email(),
//                        'name' => $caused_by->staff_name(),
//                    ]
//                ];

            //send to all heads of
            $recipient[]['address']['email'] = "diane.kennedy@policyservices.co.uk";
            $recipient[]['address']['email'] = "blair.mckenna@policyservices.co.uk";
            $recipient[]['address']['email'] = "louise.gibson@policyservices.co.uk";
            $recipient[]['address']['email'] = "lewis.barbour@policyservices.co.uk";
            $recipient[]['address']['email'] = "kevin.dorrian@policyservices.co.uk";
            $recipient[]['address']['email'] = "kayleigh.dorrian@policyservices.co.uk";
            $recipient[]['address']['email'] = "avril.braes@policyservices.co.uk";

            // send to assigned user
            if(!empty($post['assigned'])){
                $assigned = new User($post['assigned'], true);
                $recipient[]['address']['email'] = $assigned->email();
            }

            // send email, recipient array should always contain compliance regardless of TL mapping
            if (!sendSparkEmail($recipient, "New Breach Alert", '', false, $from, $file_array, $local_template)) {
                throw new Exception("Failed to send New Breach Alert email through sparkpost");
            }

        } catch (Exception $e) {
            global $bugsnag;

            $bugsnag->notifyException($e);

            throw $e;
        }
    }

}
