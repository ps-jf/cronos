<?php

class Report extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "reports";
    const FORM_DIR = "param_forms/";

    public function __construct()
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->title = Field::factory("title")
            ->set_var(Field::MAX_LENGTH, 50);

        $this->description = Field::factory("description")
            ->set_var(Field::MAX_LENGTH, 250);

        $this->form_file = Field::factory("form_file")
            ->set_var(Field::MAX_LENGTH, 50)
            ->set_var(Field::PATTERN, '/^\w+\.html$/');

        $this->report_file = Field::factory("report_file")
            ->set_var(Field::MAX_LENGTH, 50)
            ->set_var(Field::PATTERN, '/^\w+\.jrxml$/');

        $this->created_time = Date::factory("created_time")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->created_by = Sub::factory("User", "created_by")
            ->set(User::get_default_instance("id"));

        $this->is_published = Boolean::factory("published");
        //->set( true );

        $this->allow_all = Boolean::factory("allow_all");
        //->set( false );

        $this->department = Field::factory("department")
            ->set_var(Field::MAX_LENGTH, 250);

        call_user_func_array(array("parent", "__construct"), func_get_args());
    }

    public function get_form_path()
    {
        // Generate the absolute path of this Report's form template
        return $this->get_template_path("param_forms/" . $this->form_file());
    }

    public function __toString()
    {
        return "$this->title";
    }
}
