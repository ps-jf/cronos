<?php


class Permission extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "permissions";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->category = Sub::factory("PermissionCategory", "category_id");

        $this->name = Field::factory("name");

        $this->related_id = Field::factory("related_id");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return strval($this->name);
    }

    public static function by_category($cat)
    {
        $permissions = [];

        $s = new Search(new Permission());
        $s->eq("category_id", $cat);
        $s->add_order("name");

        while($p = $s->next(MYSQLI_ASSOC)){
            $permissions[] = $p;
        }

        return $permissions;
    }
}