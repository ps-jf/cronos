<?php

class NewBusSatisfactionSurvey extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "client_satisfaction_survey";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user_id = Choice::factory("user_id")
            ->setSource("select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "where active = '1' and id not in (62,63,80) " .
                "order by first_name, last_name")
            ->set_var(Field::REQUIRED, true);

        $this->g1 = Choice::factory("g1")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How satisfied with our communications and response to your queries were you?  ");

        $this->ta1 = Choice::factory("ta1")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How satisfied were you with the factfinding process that our adviser went through to gain an understanding of your financial objectives?")
            ->set_var(Field::REQUIRED, true);

        $this->ta2 = Choice::factory("ta2")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How satisfied were you with the establishment, explanation and consideration of your attitude to risk and capacity for loss?")
            ->set_var(Field::REQUIRED, true);

        $this->ta3 = Choice::factory("ta3")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How satisfied were you that everything was clearly explained? (including our services and charges)")
            ->set_var(Field::REQUIRED, true);

        $this->ta4 = Choice::factory("ta4")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->push("6", "N/A")
            ->set_var(Field::DISPLAY_NAME, "How satisfied were you with the interaction of our adviser with other professionals (e.g. accountant, solicitor or any other advisers where relevant)");

        $this->ta5 = Choice::factory("ta5")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How satisfied were you that the advice given was objective and suitable for your needs?")
            ->set_var(Field::REQUIRED, true);

        $this->ta6 = Choice::factory("ta6")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How satisfied were you with our recommendations and the explanation the adviser gave you on how the product/advice was to work?")
            ->set_var(Field::REQUIRED, true);

        $this->ta7 = Choice::factory("ta7")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "not Answered")
            ->set_var(Field::DISPLAY_NAME, "How would you rate the ability of the adviser to put you at ease and not make you feel you were under any pressure to commit?")
            ->set_var(Field::REQUIRED, true);

        $this->ta8 = Choice::factory("ta8")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How would you rate the adviser overall?")
            ->set_var(Field::REQUIRED, true);

        $this->ta9 = Field::factory("ta9")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE)
            ->set_var(Field::DISPLAY_NAME, "Is there anything our adviser did particularly well or that they could have done better?");

        $this->o1 = Choice::factory("o1")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How would you rate any other Policy Services individual you have dealt with (e.g. Paraplanner)?")
            ->set_var(Field::REQUIRED, true);

        $this->o2 = Choice::factory("o2")
            ->push("1", "Very Dissatisfied")
            ->push("2", "Dissatisfied")
            ->push("3", "Neither")
            ->push("4", "Satisfied")
            ->push("5", "Very Satisfied")
            ->push("6", "Not Answered")
            ->set_var(Field::DISPLAY_NAME, "How likely is it that you would recommend us to a friend or colleague?")
            ->set_var(Field::REQUIRED, true);


        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedby = Sub::factory("updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->nbid = Field::factory("nbid");

        $this->comments = Field::factory("comments")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->signed_off = Boolean::factory("signed_off")
            ->set_values(1, 0)
            ->set(0);

        $this->signed_off_when = Date::factory("signed_off_when")
            ->set_var(Date::FORMAT, DATE::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->feedback = Field::factory("feedback")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
