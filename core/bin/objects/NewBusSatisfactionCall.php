<?php

class NewBusSatisfactionCall extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "newbusiness_satisfaction_letter";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user_id = Choice::factory("user_id")
            ->setSource("select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "where active = '1' and id not in (62,63,80) " .
                "order by first_name, last_name")
            ->set_var(Field::REQUIRED, true);

        $this->sd1 = Choice::factory("sd1")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The clarification and explanation of the service provided by the firm.");
        //->set_var(Field::REQUIRED, true);

        $this->sd2 = Choice::factory("sd2")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The timely delivery of service by the firm.")
            ->set_var(Field::REQUIRED, true);

        $this->sd3 = Choice::factory("sd3")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The helpfulness of any representative of the firm who you dealt with.")
            ->set_var(Field::REQUIRED, true);

        $this->sd4 = Choice::factory("sd4")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The way you were dealt with if you had to contact the firms offices for any reason.")
            ->set_var(Field::REQUIRED, true);

        $this->sd5 = Choice::factory("sd5")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The accuracy of the information provided to you by the firm")
            ->set_var(Field::REQUIRED, true);

        $this->sd6 = Choice::factory("sd6")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The clarification and explanantion of how the firm were to be paid.")
            ->set_var(Field::REQUIRED, true);

        $this->sd7 = Choice::factory("sd7")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The way you were treated by the firm when raising any queries relating to the service.")
            ->set_var(Field::REQUIRED, true);

        $this->ad1 = Choice::factory("ad1")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The process the adviser went through to gain an understanding of your financial objectives.")
            ->set_var(Field::REQUIRED, true);

        $this->ad2 = Choice::factory("ad2")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The establishment, explanation and consideration of your attitude at risk.")
            ->set_var(Field::REQUIRED, true);

        $this->ad3 = Choice::factory("ad3")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The explanation the adviser gave you on how the product/advice was to work.")
            ->set_var(Field::REQUIRED, true);

        $this->ad4 = Choice::factory("ad4")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The interaction of your adviser with other professionals (e.g accountant/solicitor")
            ->set_var(Field::REQUIRED, true);

        $this->ad5 = Choice::factory("ad5")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The provision of objective and suitable advice for your needs.")
            ->set_var(Field::REQUIRED, true);

        $this->ad6 = Choice::factory("ad6")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The ability of the adviser to put you at ease and not make you feel you were under any pressure to commit.")
            ->set_var(Field::REQUIRED, true);

        $this->ad7 = Choice::factory("ad7")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The timing and delivery of the review by the adviser.")
            ->set_var(Field::REQUIRED, true);

        $this->com1 = Choice::factory("com1")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The clarity of any letters, brochures and any other documentation provided to you.")
            ->set_var(Field::REQUIRED, true);

        $this->com2 = Choice::factory("com2")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The timing and arrangements made to conduct a review with you.")
            ->set_var(Field::REQUIRED, true);

        $this->com3 = Choice::factory("com3")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The frequency of communication you receive from the firm.")
            ->set_var(Field::REQUIRED, true);

        $this->com4 = Choice::factory("com4")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The relevance of any communications sent to you by that firm.")
            ->set_var(Field::REQUIRED, true);

        $this->com5 = Choice::factory("com5")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The way you were treated by the firm when raising any queries on communications.")
            ->set_var(Field::REQUIRED, true);

        $this->com6 = Choice::factory("com6")
            ->push("1", "Better than expected")
            ->push("2", "As expected")
            ->push("3", "Below Expected")
            ->push("4", "N/A")
            ->set_var(Field::DISPLAY_NAME, "The overall standard of communications received from the firm.")
            ->set_var(Field::REQUIRED, true);

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedby = Sub::factory("updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->nbid = Field::factory("nbid");

        $this->comments = Field::factory("comments")
            ->set_var(Field::DISPLAY_NAME, "Comments");

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
