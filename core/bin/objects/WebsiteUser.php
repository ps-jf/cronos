<?php

class WebsiteUser extends DatabaseObject
{
    const DB_NAME = 'myps';
    const TABLE = "user_info";
    const COOKIE_USER_ID = "user";
    const COOKIE_USER_HASH = "uid_enc";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user_name = Field::factory("user_name");

        $this->first_name = Field::factory("fname")
            ->set_var(Field::REQUIRED, true);

        $this->last_name = Field::factory("sname")
            ->set_var(Field::REQUIRED, true);

        $this->password = Field::factory("user_passwd")
            ->set_var(Field::TYPE, Field::PASSWD);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::PARTNER_REF, true);

        $this->email_address = Field::factory("email")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->allowed_access = Boolean::factory("allowed_access");

        $this->beta_tester = Boolean::factory("beta_tester");

        $this->created = Date::factory("created")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->defunct_email = Boolean::factory("defunct_email");

        $this->last_login = Date::factory("last_login")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_FRIENDLY)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->notifications = Boolean::factory("notifications");

        $this->google2fa_secret = Field::factory("google2fa_secret");

        $this->request_portfolio_reports = Boolean::factory("request_portfolio_reports");

        /*
        $this->viewing = Sub::factory("Partner",false);
        #TODO cleaner
        $this->viewing( $_SESSION["partner_id"] );
        */

        $this->mandate_notification = Boolean::factory("mandate_notification");

        $this->proreport_notification = Boolean::factory("proreport_notification");

        $this->added_by = Sub::factory("WebsiteUser", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->added_how = Field::factory("added_how")
            ->set('prophet', true);

        $this->database = REMOTE;

        parent::__construct($id, $autoget);

    }

    static function get_default_instance()
    {
        if (isset($_SESSION["user_id"])) {
            return new WebsiteUser($_SESSION["user_id"]);
        }
    }

    public function __toString()
    {
        return "$this->email_address";
    }

    public function __is_valid()
    {
        try {
            if ($this->partner()) {
                // Check that partner record has synced to site, staff have
                // a habit of adding user accounts immediately after opening
                // an account.
                $partner = new Partner;
                $partner->use_db_link(new mydb(REMOTE));

                if (!$partner->get($this->partner())) {
                    throw new Exception("Partner not found on remote server, please try again later: " . $this->partner());
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function save($return_query = false)
    {
        if (empty($this->id())){
            // new user

            // check email address is not in use
            $s = new Search(new WebsiteUser());

            $s->eq("email_address", $this->email_address());
            $s->eq("allowed_access", 1);

            if ($wu = $s->next(MYSQLI_ASSOC)){
                throw new Exception("A user with this email address already exists and may only be set up as a sub user - " . $wu->link());
            }
        }

        return parent::save($return_query);
    }


    public function on_create($post = false)
    {
        try {
            // generate a new password reset token for user
            $pwd_reset = new DatabaseTokenRepository();
            $token = $pwd_reset->create($this);

            $body = json_encode(["PASSWORD" => $token]);

            $receiver = array('name' => $this->first_name() . " " . $this->last_name(), 'email' => $this->email_address());
            $sender = array('name' => "it@policyservices.co.uk", 'email' => "it@policyservices.co.uk");
            if (sendSparkEmail($receiver, "Your mypsaccount is ready...", 'new-myps-user', $body, $sender)) {
                $json['status'] = " A preview email has been sent to " . $this->email_address() . " with regards to account: ";
                $json['error'] = false;
            } else {
                throw new Exception("Could not send preview email");
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function changePassword($passwd)
    {
        if ($rdb = new mydb(REMOTE)) {
            // # TODO build a Password Field-subclass to handle this ugly query
            return $rdb->query("update " . $this->get_table() . " set `forgotten_token` = '" . $passwd . "' where " . $this->id->to_sql());
        }

        return false;
    }

    public static function available_accounts($partnerID = false)
    {
        if ($partnerID) {
            $db = new mydb();
            $q = "select partnerID from feebase.tblpartner
                inner join feebase.practice_staff on tblpartner.partnerID = practice_staff.partner_id
                inner join feebase.practice on practice_staff.practice_id = practice.id
                where practice.id = (select practice.id from feebase.tblpartner
                inner join feebase.practice_staff on tblpartner.partnerID = practice_staff.partner_id
                inner join feebase.practice on practice_staff.practice_id = practice.id
                where tblpartner.partnerID = " . $partnerID . " and active = 1) ";

            $db->query($q);

            $tr = "<thead>";
            while ($x = $db->next()) {
                $partner = new Partner($x['partnerID']);
                $tr .= "<tr id='$partner->id'>
                        <form>
                            <th>" . $partner->link() . "</th>
                            <th>User<input id='$partner->id' name='$partner->id' class='user' type='radio' value='User' /></th>
                            <th>Admin<input id='$partner->id' name='$partner->id' class='admin' type='radio' value='Admin' /></th>
                            <th colspan='2'>Custom<input id='$partner->id' name='$partner->id' class='custom' type='radio' value='Custom' /></th>
                        </form>
                    </tr>";
            }
            $tr .= "</thead>";

            return $tr;
        }
    }

    public function staff_member()
    {
        $s = new Search(new User());

        $s->eq("myps_user_id", $this->id());

        if($u = $s->next(MYSQLI_ASSOC)){
            return $u;
        }

        return false;
    }
}
