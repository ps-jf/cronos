<?php

class FileMapping extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "file_mapping";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->display_name = Field::factory('display_name');

        $this->filename = Field::factory("filename")
            ->set_var(Field::MAX_LENGTH, 255); // max NTFS filename length

        $this->path = Field::factory("fpath")
            ->set_var(Field::MAX_LENGTH, 270); // ( 5 * 2 char dir chunks split by '/' + 255 NTFS filename );

        $this->mime_type = Field::factory("mime_type");

        $this->size = Field::factory("size_b")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->view_count = Field::factory("view_count")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->object = Field::factory('object');

        $this->object_id = Field::factory('object_id');

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        parent::__construct($id, $autoget);

        $this->CAN_DELETE = true;
    }

    public function __toString()
    {
        return "$this->display_name ({$this->get_human_size()})";
    }

    public function get_human_size()
    {
        /* Get a human-friendly size for this file */
        $u = array("bytes", "kb", "MB", "GB");
        $e = floor(log($this->size()) / log(1024));
        return sprintf('%.1f ' . $u[$e], ($this->size() / pow(1024, floor($e))));
    }

    public function read_file()
    {
        $file = $this->get_file_path();

        if ($this->filename() == $this->display_name()) {
            $filename = $this->filename();
        } else {
            // get file extension
            $ext = pathinfo($this->filename(), PATHINFO_EXTENSION);
            $filename = $this->display_name() . "." . $ext;
        }

        if (file_exists($file)) {
            $handle = @fopen($file, "rb");

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Type: application/force-download");
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));

            ob_end_clean();//required here or large files will not work
            return @fpassthru($handle);//works fine now

            exit;
        }
    }

    public function get_file_path($fname = null)
    {
        return PROPHET_FILE_UPLOAD_DIR . "/$this->path";
    }
}
