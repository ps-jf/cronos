<?php

class Notes extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "notes";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object_type = Field::factory('object')
            ->set_var(Field::REQUIRED, true);

        $this->subject = Field::factory('subject');

        $this->object_id = Field::factory('object_id')
            ->set_var(Field::REQUIRED, true);

        $this->is_visible = Boolean::factory("is_visible")
            ->set_values(0, 1)
            ->set(1);

        $this->is_locked = Boolean::factory("is_locked")
            ->set_values(0, 1)
            ->set(1);

        $this->text = Field::factory('text')
            ->set_var(Field::REQUIRED, true);

        $this->parent_id = Field::factory('parent_id')
            ->set_var(Field::REQUIRED, false);

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            //->set_var( Field::BLOCK_INSERT, true )
            ->set(time());

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->note_type = Choice::factory("note_type")
            ->push("1", "Note")
            ->push("2", "Phonecall")
            ->push("3", "Email")
            ->push("4", "Meeting")
            ->push("5", "NB Additional Note")
            ->push("6", "NB Phonecall")
            ->push("7", "NB Email")
            ->push("8", "NB Meeting")
            ->push("9", "Compliance")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "NoteType");

        $this->sticky = Boolean::factory("sticky")
            ->set_values(0, 1)
            ->set(0);

        $this->tagged = Field::factory("tagged");

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }


    public static function get_types()
    {
        $object_types = false;
        $db = new mydb();
        $db->query("SELECT DISTINCT object FROM feebase.notes");

        while ($object_type = $db->next(MYSQLI_ASSOC)) {
            $object_types[] = $object_type['object'];
        }
        return $object_types;
    }

    public function get_parent($id = null)
    {
        $db = new mydb();
        $db->query("SELECT parent_id FROM feebase.notes WHERE id = " . $id);

        if ($x = $db->next()) {
            if ($x['parent_id']) {
                return true;
            } else {
                return false;
            }
        }
    }
}
