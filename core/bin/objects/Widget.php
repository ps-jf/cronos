<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 14/01/2019
 * Time: 15:36
 */

class Widget extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "widgets";

    public function __construct()
    {
        $this->widgetID = Field::factory("widgetID", Field::PRIMARY_KEY);

        $this->template = Field::factory("template");

        $this->title = Field::factory("title");

        $this->width = Field::factory("width");

        $this->height = Field::factory("height");

        $this->group = Choice::factory("group")
            ->push("1", "Personal")
            ->push("2", "Team")
            ->push("99", "Other");
    }
}