<?php

class ReportUser extends DatabaseObject
{
    const TABLE = "report_users";
    const DB_NAME = SYS_DB;

    public function __construct()
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user = Sub::factory("User", "user_id")
            ->set_var(Field::REQUIRED, true);

        $this->report = Sub::factory("Report", "report_id")
            ->set_var(Field::REQUIRED, true);

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->added_time = Date::factory("added_time")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        call_user_func_array(array("parent", "__construct"), func_get_args());
    }

    public function __toString()
    {
        return "$this->report $this->user";
    }
}
