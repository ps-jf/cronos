<?php

class SchemeMember extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "scheme_member";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->scheme_number = Sub::factory("Policy", "scheme_id");

        $this->member_number = Field::factory("member_number");

        $this->member_name = Field::factory("member_name");

        $this->dob = Date::factory("dob")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->nino = Field::factory("nino")
            ->set_var(Field::PATTERN, "/^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}$/");

        $this->address = Address::factory(
            "address_line1",
            "address_line2",
            "address_line3",
            "postcode"
        );

        $this->active = Boolean::factory("active")
            ->set_values(1, 0)
            ->set(1);

        $this->email = Field::factory("email");

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->member_name";
    }

    public function on_create($POST_data = false)
    {
//        # todo leave commented out until futher notice
//        $scheme_pol = new Policy($POST_data['scheme_number'], true);
//        // add new policy with main scheme policy and member name appended to it
//        $policy = new Policy();
//        $policy->number($scheme_pol->number()." - ".$POST_data['member_name']);
//        $policy->client($scheme_pol->client->id());
//        $policy->issuer($scheme_pol->issuer->id());
//        $policy->type($scheme_pol->type->id());
//        $policy->status($scheme_pol->status());
//
//        # todo - we would want this to be the scheme members name somehow...
//        $policy->owner(5);
//
//        $policy->save();
    }
}
