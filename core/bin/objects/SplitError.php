<?php

class SplitError extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "data_change_error";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->data_change_id = Field::factory("data_change_id");

        $this->addedwhen = Field::factory("addedwhen");

        $this->addedby = Field::factory("addedby");

        $this->comments = Field::factory("comments");

        $this->contacted = Boolean::factory("contacted");

        $this->object = Field::factory("object");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
