<?php

class Virtue extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "virtue";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "client_id")
            ->set_var(Field::REQUIRED, true);

        $this->known_as = Field::factory("known_as");

        $this->acorn_class = Field::factory("acorn_class");

        $this->active = Boolean::factory("active")
            ->set_values(1, 0);

        $this->company = Field::factory("company");

        $this->left_sjp = Boolean::factory("left_sjp")
            ->set_values(1, 0);

        $this->date_left_sjp = Date::factory("date_left_sjp")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->stage = Choice::factory("stage")
            ->push("1", "No Contact Made")
            ->push("2", "Provide Report")
            ->push("3", "Has SJP Adviser - Contact Them")
            ->push("4", "Has Other Adviser - Provide Report")
            ->push("5", "Wants Advice")
            ->push("6", "Initial/Follow Up Discussion")
            ->push("7", "Has Other Adviser - No Report Required")
            ->push("8", "No Future Contact - Add to 3918")
            ->push("9", "Call Back Required")
            ->push("10", "Report Sent - Follow Up Call")
            ->push("11", "Transactional Client")
            ->push("12", "Add to Moneyinfo")
            ->push("13", "Ongoing Service");

        $this->further_action = Field::factory("further_action");

        $this->soft_facts = Field::factory("soft_facts");

        $this->six_monthly = Boolean::factory("six_monthly")
            ->set_values(1, 0);

        $this->annually = Boolean::factory("annually")
            ->set_values(1, 0);

        $this->send_report_on = Date::factory("send_report_on")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->call_notes = Field::factory("call_notes");

        $this->money_info = Boolean::factory("money_info")
            ->set_values(1, 0);

        $this->intelligent_office = Boolean::factory("intelligent_office")
            ->set_values(1, 0);

        $this->ps_adviser = Choice::factory("ps_adviser")
            ->setSource("select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "where active = '1' and id in (22,26,49,56,105,108,109,111, 168) " .
                "order by first_name, last_name");

        $this->separated = Boolean::factory("client_separated")
            ->set_values(1, 0);

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->service_package = Choice::factory("service_package")
            ->push("1", "Lite")
            ->push("2", "On Demand")
            ->push("3", "Deluxe");

        $this->letter_sent = Boolean::factory("letter_sent")
            ->set_values(1, 0);

        $this->satisfaction_letter = Boolean::factory("satisfaction_letter")
            ->set_values(1, 0);

        $this->report_sent = Boolean::factory("report_sent")
            ->set_values(1, 0);

        $this->call_back = Boolean::factory("call_back")
            ->set_values(1, 0);

        $this->no_deal = Boolean::factory("no_deal")
            ->set_values(1, 0);

        $this->mail_pref = Boolean::factory("mail_preference")
            ->set_values(1, 0);

        $this->letter_type = Field::factory("letter_type");

        $this->date_letter = Date::factory("date_letter")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->follow_up_date = Date::factory("follow_up_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->outcome = Choice::factory("outcome")
            ->push("1", "Send Report")
            ->push("2", "Call Back")
            ->push("3", "Has SJP Partner")
            ->push("4", "Has Ext Adviser")
            ->push("5", "No Action / Remove")
            ->push("6", "Contact Outstanding")
            ->push("7", "Follow up letter");

        $this->report_sent_date = Date::factory("report_sent_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->report_follow_up = Date::factory("report_follow_up")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->follow_up_letter_sent = Date::factory("follow_up_letter_sent")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->report_due = Date::factory("report_due")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->report_results = Choice::factory("report_results")
            ->push("1", "Wishes for Advice")
            ->push("2", "To be removed")
            ->push("3", "Happy as is - Lite");

        $this->contact = Choice::factory("contact")
            ->push("1", "Virtue Money Called Client")
            ->push("2", "Client Called Virtue Money")
            ->push("3", "Email Sent")
            ->push("4", "Email Received")
            ->push("5", "Follow Up Letter Sent")
            ->push("6", "Letter Received")
            ->push("7", "Online");

        $this->frequency = Choice::factory("frequency")
            ->push("1", "6 Months")
            ->push("2", "Annually");

        $this->review_date = Date::factory("review_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        parent::__construct($id, $auto_get);
    }


    public function __toString()
    {
        return "$this->id";
    }

    public function link($text = false)
    {
        return parent::link($text);
    }

    public function get_previous_partner($client)
    {
        $s = new Search(new LocalLog);
        $s->eq("object", "Client");
        $s->eq("object_id", $client);
        $s->eq("type", "UPDATE");
        $s->add_order("id", "DESC");

        while ($res = $s->next(MYSQLI_ASSOC)) {
            //$bits = explode("\"", stristr($res->snapshot, 's:7:"partner";s:'));
            //$partner = $bits[3];

            //use new log_actions json
            $snapshot = json_decode($res->snapshot);
            $partner = $snapshot->partner;

            // Return partner ID when previous owning partner is not a Virtue account ( 3514, 3918, 3942 )
            if ($partner != 3514 && $partner != 3918 && $partner != 3942) {
                return $partner;
            }
        }
    }

    public function attractive_rating($id, $gri = false, $c1_age = false, $c2_age = false, $has_corr = false, $has_p_and_i = false)
    {
        $points = 0;
        $virtue = new Virtue($id, true);

        if (str_replace("&pound;", "", $gri) > 0) {
            $points += 1;
        }
        if ((str_replace(" years old", "", $c1_age) < 70) && (str_replace(" years old", "", $c2_age) < 70)) {
            $points += 1;
        }
        if ($has_corr != 0) {
            $points += 1;
        }
        if ($virtue->acorn_class() != null) {
            $points += 1;
        }
        // if client has at least one pension plan and one investment plan
        if ($has_p_and_i == 1) {
            $points += 1;
        }

        return $points;
    }

    public function has_pension_plan($client)
    {
        $s = new Search(new Policy);
        $s->eq("client", $client);
        $s->add_or(
            array(
                $s->eq("type", 5, true, 0),
                $s->eq("type", 22, true, 0),
                $s->eq("type", 23, true, 0),
                $s->eq("type", 25, true, 0)
            )
        );

        if ($s->count() >= 1) {
            $pension = "<img src='/lib/images/tick.png'>";
        } else {
            $pension = "<img src='/lib/images/cross.png'>";
        }
        return $pension;
    }

    public function has_investment_plan($client)
    {
        $s = new Search(new Policy);
        $s->eq("client", $client);
        $s->add_or(
            array(
                $s->eq("type", 1, true, 0),
                $s->eq("type", 4, true, 0),
                $s->eq("type", 10, true, 0),
                $s->eq("type", 14, true, 0),
                $s->eq("type", 15, true, 0),
                $s->eq("type", 16, true, 0)
            )
        );

        if ($s->count() >= 1) {
            $investment = "<img src='/lib/images/tick.png'>";
        } else {
            $investment = "<img src='/lib/images/cross.png'>";
        }
        return $investment;
    }

    public function has_term_plan($client)
    {
        $s = new Search(new Policy);
        $s->eq("client", $client);
        $s->add_or(
            array(
                $s->eq("type", 6, true, 0),
                $s->eq("type", 7, true, 0),
                $s->eq("type", 8, true, 0),
                $s->eq("type", 9, true, 0),
                $s->eq("type", 28, true, 0),
                $s->eq("type", 30, true, 0)
            )
        );

        if ($s->count() >= 1) {
            $term = "<img src='/lib/images/tick.png'>";
        } else {
            $term = "<img src='/lib/images/cross.png'>";
        }
        return $term;
    }


    public function has_p_and_i($client)
    {
        //check for pension plans
        $s = new Search(new Policy);
        $s->eq("client", $client);
        $s->add_or(
            array(
                $s->eq("type", 5, true, 0),
                $s->eq("type", 22, true, 0),
                $s->eq("type", 23, true, 0),
                $s->eq("type", 25, true, 0)
            )
        );

        if ($s->count() >= 1) {
            $pension = true;
        } else {
            $pension = false;
        }

        //check for investment plans
        $s1 = new Search(new Policy);
        $s1->eq("client", $client);
        $s1->add_or(
            array(
                $s1->eq("type", 1, true, 0),
                $s1->eq("type", 4, true, 0),
                $s1->eq("type", 10, true, 0),
                $s1->eq("type", 14, true, 0),
                $s1->eq("type", 15, true, 0),
                $s1->eq("type", 16, true, 0)
            )
        );

        if ($s1->count() >= 1) {
            $investment = true;
        } else {
            $investment = false;
        }

        //if both true, score + 1 for attractive rating
        if (($pension == true) && ($investment == true)) {
            $score = 1;
        } else {
            $score = 0;
        }

        return $score;
    }

    public function has_unknown_policies($client)
    {
        $s = new Search(new Policy);
        $s->eq("client", $client);
        $s->eq("type", 17);
        $s->nteq("issuer", 1045);
        $s->nteq("issuer", 1197);
        $s->nteq("issuer", 1254);
        $s->nteq("issuer", 11391);
        $s->nteq("issuer", 11764);

        if ($s->count() >= 1) {
            $unknown = "<img src='/lib/images/tick.png'>";
        } else {
            $unknown = "<img src='/lib/images/cross.png'>";
        }
        return $unknown;
    }
}
