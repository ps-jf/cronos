<?php

class ViewerEntry extends DatabaseObject
{
    /*
      A ViewerEntry is a remote database entry which dictates which
      users are allowed access to which Partner accounts via the 
      website.
     */

    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "viewer_tbl";
    protected $CAN_DELETE = true;

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user = Sub::factory("WebsiteUser", "user_id")
            ->set_var(Field::REQUIRED, true);

        $this->partner = Sub::factory("Partner", "partner_id")
            ->set_var(Field::REQUIRED, true);

        $this->stealth = Boolean::factory("stealth");

        $this->expires = Date::factory("until")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->added_by_staff = Field::factory("added_by_staff")
            //->set_var( Field::REQUIRED, true )
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        // The level of access assigned to this WebsiteUser
        $this->access_level = Choice::factory("access_level")
            ->push_array(["USER", "ADMIN", "CUSTOM"])
            ->set_var(Field::REQUIRED, true)
            ->set("USER");

        /*
        $this->permissions = Field::factory("permissions_object");

        $this->default_account = Boolean::factory("default_account");
        */

        $this->permissions = JSON::factory("permissions_array");

        $this->email_notification = Boolean::factory("email_notification");

        $this->access_revoked = Boolean::factory("access_revoked");

        $this->mandate_notification = Boolean::factory("mandate_notification");

        $this->proreport_notification = Boolean::factory("proreport_notification");

        $this->database = REMOTE;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->user($this->access_level)";
    }
}
