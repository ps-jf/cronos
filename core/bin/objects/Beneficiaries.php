<?php

class Beneficiaries extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblbeneficiaries";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->policy = Field::factory("policy_id");

        $this->firstname = Field::factory("firstname");

        $this->surname = Field::factory("surname");

        $this->dob = Date::factory("dob")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

}