<?php

class Practice extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "practice";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->name = Field::factory('name')
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::MAX_LENGTH, 100);

        $this->address = Address::factory(
            "address1",
            "address2",
            "address3",
            "postcode"
        );

        $this->phone = Field::factory('phone')
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->fax = Field::factory('fax')
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->email = Field::factory('email')
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS)
            ->set_var(Field::MAX_LENGTH, 320);

        $this->website = new Field('website');

        $this->updatedwhen = Date::factory("update_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "update_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("created_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "created_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->status = new Boolean("status");

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        if (!$this->loaded) {
            $this->get();
        }

        // IFA Practice Limited
        return "$this->name";
    }


    // Returns GRI for a pracice given a list of partnerids that appear in the practice.
    // Optional $life flag if lifetime GRI should be returned.  By default 12 months is returned.
    /* GRI calculated as (renewal(0) + clawback(2) + adviser_charging_ongoing(8) + client_direct_ongoing(10) + dfm_fees(12) +
    OAS Income(14) + Trail(15) + PPB Income(16)) - VAT */
    public function practice_gri($partnerids, $life = false)
    {
        $db = new mydb();
        $partners = (is_array($partnerids)) ? "'".implode("','", $partnerids)."'" : $partnerids;

        $gri_fig = "";

        if (!$life) {
            $q = "SELECT IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)) as GRI_12 " .
                " FROM " . COMM_AUDIT_TBL .
                " WHERE partnerid in (" . $partners . ") " .
                " AND commntype in " . CommissionType::griTypes() .
                " AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)";

            $db->query($q);

            if ($gri = $db->next()) {
                $gri_fig = "&pound; " . number_format($gri['GRI_12'], 2);
            } else {
                $gri_fig = "&pound;0.00";
            }
        } else {
            $db->query("SELECT IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)) as GRI_LIFE " .
                " FROM " . COMM_AUDIT_TBL .
                " WHERE partnerid in (" . $partners . ") " .
                " AND commntype in " . CommissionType::griTypes());

            if ($gri = $db->next()) {
                $gri_fig = "&pound; " . number_format($gri['GRI_LIFE'], 2);
            } else {
                $gri_fig = "&pound;0.00";
            }
        }

        return $gri_fig;
    }

    public static function suggestedReviewRate($tariff, $gri)
    {
        // Reviewable contract - 2009 Tariff
        $tariff_2009 = [
            ['min' => -99999, 'max' => 1000, 'percentage' => 0, 'band' => 0],
            ['min' => 1001, 'max' => 5000, 'percentage' => 60, 'band' => 1],
            ['min' => 5001, 'max' => 10000, 'percentage' => 65, 'band' => 2],
            ['min' => 10001, 'max' => 15000, 'percentage' => 70, 'band' => 3],
            ['min' => 15001, 'max' => 25000, 'percentage' => 75, 'band' => 4],
            ['min' => 25001, 'max' => 50000, 'percentage' => 80, 'band' => 5],
            ['min' => 50001, 'max' => 75000, 'percentage' => 82, 'band' => 6],
            ['min' => 75001, 'max' => 100000, 'percentage' => 84, 'band' => 7],
            ['min' => 100001, 'max' => 150000, 'percentage' => 86, 'band' => 8],
            ['min' => 150001, 'max' => 200000, 'percentage' => 88, 'band' => 9],
            ['min' => 200001, 'max' => 250000, 'percentage' => 90, 'band' => 10],
            ['min' => 250001, 'max' => 300000, 'percentage' => 92, 'band' => 11],
            ['min' => 300001, 'max' => 999999999, 'percentage' => 94, 'band' => 12]
        ];

        // Reviewable contract - 2013 Tariff
        $tariff_2013 = [
            ['min' => -99999, 'max' => 1000, 'percentage' => 30, 'band' => 0],
            ['min' => 1001, 'max' => 10000, 'percentage' => 60, 'band' => 1],
            ['min' => 10001, 'max' => 20000, 'percentage' => 65, 'band' => 2],
            ['min' => 20001, 'max' => 30000, 'percentage' => 70, 'band' => 3],
            ['min' => 30001, 'max' => 50000, 'percentage' => 75, 'band' => 4],
            ['min' => 50001, 'max' => 75000, 'percentage' => 80, 'band' => 5],
            ['min' => 75001, 'max' => 100000, 'percentage' => 82, 'band' => 6],
            ['min' => 100001, 'max' => 150000, 'percentage' => 84, 'band' => 7],
            ['min' => 150001, 'max' => 200000, 'percentage' => 86, 'band' => 8],
            ['min' => 200001, 'max' => 250000, 'percentage' => 88, 'band' => 9],
            ['min' => 250001, 'max' => 300000, 'percentage' => 90, 'band' => 10],
            ['min' => 300001, 'max' => 400000, 'percentage' => 92, 'band' => 11],
            ['min' => 400001, 'max' => 999999999, 'percentage' => 94, 'band' => 12]
        ];

        if ($tariff == 1) {
            $tariff = $tariff_2009;
        } else if ($tariff == 2) {
            $tariff = $tariff_2013;
        }

        if ($tariff) {
            $partner_gri = str_replace('&pound;', '', $gri);

            // Get a suggested rate dependant upon which tariff and percentage the partner's gri falls within
            $precision = 0.01;
            foreach ($tariff as $current) {
                if ((str_replace(',', '', $partner_gri) - $current['min']) >= $precision &&
                    (str_replace(',', '', $partner_gri) - $current['max']) <= $precision) {
                    $suggested_rate = $current['percentage'];
                }
            }
            $return = $suggested_rate."%";

        } else {
            $return = "ERROR";
        }

        return $return;
    }
}
