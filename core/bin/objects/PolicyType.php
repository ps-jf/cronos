<?php

class PolicyType extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblpolicytype";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("policyTypeID", Field::PRIMARY_KEY);

        $this->name = Field::factory("policyTypeDesc");

        $this->new_business_form_fields = Field::factory("nb_form_policy_fields");

        $this->new_business_form_title = Field::factory("nb_form_policy_title");

        $this->type = Field::factory("type");

        $this->category = Field::factory("category");

        $this->alias = Field::factory("alias");

        $this->regulated = Boolean::factory("regulated");

        $this->order = array("name");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->name";
    }
}
