<?php

class WebCorrespondenceAudit extends DatabaseObject
{
    var $table = WEB_CORRESPONDENCE_AUDIT_TBL;

    public function WebCorrespondenceAudit()
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->item = new Sub("Correspondence", "item_id");

        $this->user = new Sub("WebsiteUser", "user");

        $this->date = new Date("datime");

        call_user_func_array(array("parent", "__construct"), func_get_args());
    }
}
