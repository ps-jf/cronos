<?php

class TransferChecklist extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "transfer_checklist";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object_id = Field::factory('object_id');

        $this->object_type = Field::factory('object_type');

        $this->partner_and_notes_added = Boolean::factory("partner_and_notes_added")
            ->set_var(Field::DISPLAY_NAME, "Partner and all notes added to recruit system")
            ->set_values(1, 0)
            ->set(0);

        $this->intro_call = Boolean::factory("intro_call")
            ->set_var(Field::DISPLAY_NAME, "Called Partner to introduce yourself")
            ->set_values(1, 0)
            ->set(0);

        ## start of outgoing letter checks ##
        $this->relevant_ps_contract_out = Boolean::factory("relevant_ps_contract_out")
            ->set_var(Field::DISPLAY_NAME, "Relevant PS contract")
            ->set_values(1, 0)
            ->set(0);

        $this->ps_partner_tariff_out = Boolean::factory("ps_partner_tariff_out")
            ->set_var(Field::DISPLAY_NAME, "PS Partner Tariff")
            ->set_values(1, 0)
            ->set(0);

        $this->sample_ifa_letter_out = Boolean::factory("sample_ifa_letter_out")
            ->set_var(Field::DISPLAY_NAME, "Sample IFA Letter")
            ->set_values(1, 0)
            ->set(0);

        $this->sample_client_letters_out = Boolean::factory("sample_client_letters_out")
            ->set_var(Field::DISPLAY_NAME, "Sample Client Letters")
            ->set_values(1, 0)
            ->set(0);

        $this->ps_client_spreadsheet_out = Boolean::factory("ps_client_spreadsheet_out")
            ->set_var(Field::DISPLAY_NAME, "Policy Services Client Spreadsheet")
            ->set_values(1, 0)
            ->set(0);

        $this->pipeline_letters_out = Boolean::factory("pipeline_letters_out")
            ->set_var(Field::DISPLAY_NAME, "Pipeline Letters")
            ->set_values(1, 0)
            ->set(0);

        $this->sjp_application_out = Boolean::factory("sjp_application_out")
            ->set_var(Field::DISPLAY_NAME, "SJP Application");

        $this->last_year_pi_out = Boolean::factory("last_year_pi_out")
            ->set_var(Field::DISPLAY_NAME, "Last Years PI Application");

        ## start of received back letter checks ##
        $this->relevant_ps_contract_in = Boolean::factory("relevant_ps_contract_in")
            ->set_var(Field::DISPLAY_NAME, "Relevant PS contract")
            ->set_values(1, 0)
            ->set(0);

        $this->ps_partner_tariff_in = Boolean::factory("ps_partner_tariff_in")
            ->set_var(Field::DISPLAY_NAME, "PS Partner Tariff")
            ->set_values(1, 0)
            ->set(0);

        $this->sample_ifa_letter_in = Boolean::factory("sample_ifa_letter_in")
            ->set_var(Field::DISPLAY_NAME, "Sample IFA Letter")
            ->set_values(1, 0)
            ->set(0);

        $this->sample_client_letters_in = Boolean::factory("sample_client_letters_in")
            ->set_var(Field::DISPLAY_NAME, "Sample Client Letters")
            ->set_values(1, 0)
            ->set(0);

        $this->ps_client_spreadsheet_in = Boolean::factory("ps_client_spreadsheet_in")
            ->set_var(Field::DISPLAY_NAME, "Policy Services Client Spreadsheet")
            ->set_values(1, 0)
            ->set(0);
        $this->pipeline_letters_in = Boolean::factory("pipeline_letters_in")
            ->set_var(Field::DISPLAY_NAME, "Pipeline Letters")
            ->set_values(1, 0)
            ->set(0);
        ## end checks for documents sent and received back ##

        $this->document_walkthrough_call = Boolean::factory("document_walkthrough_call")
            ->set_var(Field::DISPLAY_NAME, "Called Partner to walk though documents")
            ->set_values(1, 0)
            ->set(0);

        $this->novation_analysis_completed = Boolean::factory("novation_analysis_completed")
            ->set_var(Field::DISPLAY_NAME, "Novation Analysis Completed")
            ->set_values(1, 0)
            ->set(0);

        $this->online_access_discussed = Boolean::factory("online_access_discussed")
            ->set_var(Field::DISPLAY_NAME, "Online Access Discussed")
            ->set_values(1, 0)
            ->set(0);

        $this->ac_discussed = Boolean::factory("ac_discussed")
            ->set_var(Field::DISPLAY_NAME, "Adviser Charging Discussed")
            ->set_values(1, 0)
            ->set(0);

        $this->model_portfolios_discussed = Boolean::factory("model_portfolios_discussed")
            ->set_var(Field::DISPLAY_NAME, "Model Portfolios Discussed")
            ->set_values(1, 0)
            ->set(0);

        ## start of post appointment checklists ##
        $this->novation_manager_signoff = Boolean::factory("novation_manager_signoff")
            ->set_var(Field::DISPLAY_NAME, "Managers signed off on Novation starting")
            ->set_values(1, 0)
            ->set(0);

        $this->open_prophet_account = Boolean::factory("open_prophet_account")
            ->set_var(Field::DISPLAY_NAME, "Open Account on Prophet")
            ->set_values(1, 0)
            ->set(0);

        $this->add_agency_codes = Boolean::factory("add_agency_codes")
            ->set_var(Field::DISPLAY_NAME, "Add Agency Codes")
            ->set_values(1, 0)
            ->set(0);

        $this->agency_codes_checked = Boolean::factory("agency_codes_checked")
            ->set_var(Field::DISPLAY_NAME, "Agency codes checked by colleague")
            ->set_values(1, 0)
            ->set(0);

        $this->start_novation = Boolean::factory("start_novation")
            ->set_var(Field::DISPLAY_NAME, "Start Novation")
            ->set_values(1, 0)
            ->set(0);

        $this->import_client_list = Boolean::factory("import_client_list")
            ->set_var(Field::DISPLAY_NAME, "Import Client List")
            ->set_values(1, 0)
            ->set(0);

        $this->prepop_arranged = Boolean::factory("prepop_arranged")
            ->set_var(Field::DISPLAY_NAME, "Arrange for IT to pre populate mandates for providers that will not novate")
            ->set_values(1, 0)
            ->set(0);

        $this->provider_crosscheck = Boolean::factory("provider_crosscheck")
            ->set_var(Field::DISPLAY_NAME, "Providers on client list cross check with provider on agency list")
            ->set_values(1, 0)
            ->set(0);

        $this->myps_walkthrough = Boolean::factory("myps_walkthrough")
            ->set_var(Field::DISPLAY_NAME, "MyPSaccount walk through completed")
            ->set_values(1, 0)
            ->set(0);

        $this->pipeline_letter_scanned = Boolean::factory("pipeline_letter_scanned")
            ->set_var(Field::DISPLAY_NAME, "Pipeline letter scanned and passed to New Business")
            ->set_values(1, 0)
            ->set(0);

        $this->sjp_application_in = Boolean::factory("sjp_application_in")
            ->set_var(Field::DISPLAY_NAME, "SJP Application");

        $this->last_year_pi_in = Boolean::factory("last_year_pi_in")
            ->set_var(Field::DISPLAY_NAME, "Last Years PI Application");

        ## start online access checks ##
        $this->check_online_access = Boolean::factory("check_online_access")
            ->set_var(Field::DISPLAY_NAME, "When letter of confirmation are processed, check to see if online access can be set up")
            ->set_values(1, 0)
            ->set(0);

        $this->setup_online_access = Boolean::factory("setup_online_access")
            ->set_var(Field::DISPLAY_NAME, "Where possible, set up access")
            ->set_values(1, 0)
            ->set(0);

        $this->record_online_access = Boolean::factory("record_online_access")
            ->set_var(Field::DISPLAY_NAME, "Record on 'Online Access' section of Prophet")
            ->set_values(1, 0)
            ->set(0);

        $this->email_partner_login = Boolean::factory("email_partner_login")
            ->set_var(Field::DISPLAY_NAME, "Email partner login details")
            ->set_values(1, 0)
            ->set(0);

        $this->registration_walkthrough = Boolean::factory("registration_walkthrough")
            ->set_var(Field::DISPLAY_NAME, "If online access cannot be set up by PS, call partner and walk them through the registration process")
            ->set_values(1, 0)
            ->set(0);

        ## start commission checks ##
        $this->commission_1month_report = Boolean::factory("commission_1month_report")
            ->set_var(Field::DISPLAY_NAME, "Month 1 progress report")
            ->set_values(1, 0)
            ->set(0);

        $this->commission_2month_report = Boolean::factory("commission_2month_report")
            ->set_var(Field::DISPLAY_NAME, "Month 2 progress report")
            ->set_values(1, 0)
            ->set(0);

        $this->commission_2month_check = Boolean::factory("commission_2month_check")
            ->set_var(Field::DISPLAY_NAME, "2 monthly Income check ")
            ->set_values(1, 0)
            ->set(0);

        $this->commission_3month_report = Boolean::factory("commission_3month_report")
            ->set_var(Field::DISPLAY_NAME, "Month 3 progress report")
            ->set_values(1, 0)
            ->set(0);

        $this->commission_4month_report = Boolean::factory("commission_4month_report")
            ->set_var(Field::DISPLAY_NAME, "Month 4 progress report")
            ->set_values(1, 0)
            ->set(0);

        $this->commission_4month_check = Boolean::factory("commission_4month_check")
            ->set_var(Field::DISPLAY_NAME, "4 Monthly Income check")
            ->set_values(1, 0)
            ->set(0);

        $this->final_check = Boolean::factory("final_check")
            ->set_var(Field::DISPLAY_NAME, "Final novation and Income check")
            ->set_values(1, 0)
            ->set(0);

        $this->completion_email_call = Boolean::factory("completion_email_call")
            ->set_var(Field::DISPLAY_NAME, "Completion email and phone call to partner")
            ->set_values(1, 0)
            ->set(0);

        $this->discovery_form_returned = Boolean::factory("discovery_form_returned")
            ->set_var(Field::DISPLAY_NAME, "Discovery Form Returned")
            ->set_values(1, 0)
            ->set(0);

        $this->discovery_form_output_completed = Boolean::factory("discovery_form_output_completed")
            ->set_var(Field::DISPLAY_NAME, "Discovery Form Output Completed")
            ->set_values(1, 0)
            ->set(0);

        $this->vat = Boolean::factory("vat")
            ->set_var(Field::DISPLAY_NAME, "VAT")
            ->set_values(1, 0)
            ->set(0);

        $this->servicing_proposition = Boolean::factory("servicing_proposition")
            ->set_var(Field::DISPLAY_NAME, "Servicing Proposition")
            ->set_values(1, 0)
            ->set(0);

        $this->ps_valuation_reports_costs = Boolean::factory("ps_valuation_reports_costs")
            ->set_var(Field::DISPLAY_NAME, "PS Valuation Reports Costs")
            ->set_values(1, 0)
            ->set(0);

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
