<?php

class HandbackRemoval extends DatabaseObject
{
    const DB_NAME = 'myps';
    const TABLE = "handback_removals";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "clientID")
            ->set_var(Field::REQUIRED, true);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->submitted_by = Sub::factory("WebsiteUser", "submitted_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->submitted_when = Date::factory("submitted_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->removal_reason = Field::factory("removal_reason")
            ->set_var(Field::REQUIRED, true);

        $this->removal_prompt = Choice::factory("removal_prompt")
            ->set_var(Field::REQUIRED, true)
            ->push("1", "Handback Client")
            ->push("2", "Unable to Obtain Client Agreement")
            ->set(1);

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }
}
