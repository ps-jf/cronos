<?php

class DocumentStorage extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "document_storage";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partnerID = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->object = Field::factory('object')
            ->set_var(Field::REQUIRED, false);

        $this->object_id = Field::factory('object_id')
            ->set_var(Field::REQUIRED, false);

        $this->filepath = Field::factory("filepath")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::MAX_LENGTH, 270); // ( 5 * 2 char dir chunks split by '/' + 255 NTFS filename );

        $this->original_name = Field::factory("original_name")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::MAX_LENGTH, 255); // max NTFS filename length

        $this->friendly_name = Field::factory('friendly_name');

        $this->hash = Field::factory('hash')
            ->set_var(Field::REQUIRED, true);

        $this->mime_type = Field::factory("mime_type")
            ->set_var(Field::REQUIRED, true);

        $this->file_size = Field::factory("file_size")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->added_by = Sub::factory("WebsiteUser", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->view_count = Field::factory("view_count")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->servicing_document = Boolean::factory("servicing_document")
            ->set(0);

        $this->declaration = Boolean::factory("declaration")
            ->set(0);

        $this->review_offer_evidence = Boolean::factory("review_offer_evidence")
            ->set(0);

        $this->category = Choice::factory("category_id")
            ->setSource("select id, name from myps.document_storage_categories order by name");

        $this->CAN_DELETE = true;

        $this->database = REMOTE;

        parent::__construct($id, $autoget);
    }


    public function __toString()
    {
        return "$this->friendly_name ({$this->getHumanSize()})";
    }

    public function getHumanSize()
    {
        /* Get a human-friendly size for this file */
        $u = array("bytes", "kb", "MB", "GB");
        $e = floor(log($this->file_size()) / log(1024));
        return sprintf('%.1f ' . $u[$e], ($this->file_size() / pow(1024, floor($e))));
    }

    public function readFile()
    {
        $file = $this->getFilePath();

        if ($this->original_name() == $this->friendly_name()) {
            $filename = $this->original_name();
        } else {
            // get file extension
            $ext = pathinfo($this->original_name(), PATHINFO_EXTENSION);
            $filename = $this->friendly_name() . "." . $ext;
        }

        if (file_exists($file)) {
            $handle = @fopen($file, "rb");

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header("Content-Type: application/force-download");
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));

            ob_end_clean();//required here or large files will not work

            return @fpassthru($handle);//works fine now

            exit;
        }
    }

    public function getFilePath($fname = null)
    {
        return PROPHET_FILE_UPLOAD_DIR . "/$this->path";
    }

    public static function formatBytes($size, $precision = 0)
    {
        $unit = ['Byte', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];

        for ($i = 0; $size >= 1024 && $i < count($unit) - 1; $i++) {
            $size /= 1024;
        }

        return round($size, $precision) . ' ' . $unit[$i];
    }

    public function getObject()
    {
        $objectName = $this->object();
        $object = new $objectName($this->object_id(), true);

        if($objectName === "ActionRequiredMessage"){
            $object = $object->action_required_id;
        }

        return $object;
    }

    public function getServicing()
    {
        if ($this->object() === 'Client') {
            $s = new Search(new OngoingService());
            $s->eq('client_id', $this->object_id());

            while ($serv = $s->next()) {
                if (!empty($serv->document_upload_ids)) {
                    $list = explode(',', $serv->document_upload_ids());
                    foreach ($list as $os) {
                        if ($os == $this->id()) {
                            return $serv;
                        }
                    }
                }
            }
        }
    }
}
