<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 08/08/2019
 * Time: 13:17
 */

class Script extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "scripts";
    const COMMON_PARAMS = ["file"];

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->category = Sub::factory("ScriptCategory", "category");

        $this->function = Field::factory("function");

        $this->command = Field::factory("command");

        $this->filename = Field::factory("filename");

        $this->name = Field::factory("name");

        $this->parameters = Field::factory("parameters");

        $this->archived = Boolean::factory("archived");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        $return = "";

        if (isset($this->name) && strlen($this->name)){
            $return = strval($this->name);
        } else {
            $return = strval($this->filename);
        }

        return $return;
    }

    public function get_parameters($key = false)
    {
        $parameters = json_decode($this->parameters());

        if($key){
            if(array_key_exists($key, $parameters)){
                return $parameters->$key;
            } else {
                return false;
            }
        }

        return $parameters;
    }

    // script functions
    public function mass_unsegment()
    {
        $response = [
            'success' => true,
            'feedback' => [],
            'files' => [],
        ];

        $file = fopen($_FILES['client_list']['tmp_name'], 'r');

        $titles = true;

        while($line = fgetcsv($file)){
            if($titles && !is_numeric(trim($line[0]))) { // skip any title rows
                continue;
            }

            $titles = false;

            $client = new Client(trim($line[0]), true);

            if(!empty($client->id())){
                $message = "This client has now been unsegmented (en masse) from a servicing level automatically.";
                $client->unsegment($message, boolval($_POST['welcome_letters']));

                $message .= " Therefore this ticket has now been been closed.";

                $client->endWorkflow($message,[41,188,189,193], true);
                $client->endActionRequired($message, [26,31,32,33,34]);

                $response['feedback'][] = "<span class='text-success'>" . $client->link() . " successfully unsegmented.";
            } else {
                $response['feedback'][] = "<span class='text-danger'>Client " . $line[0] . " couldnt not be found.</span>";
            }
        }

        fclose($file);

        return $response;
    }
}