<?php

class PasswordReset extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "password_resets";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->email = Field::factory("email")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->token = Boolean::factory("token");

        $this->created_at = Date::factory("created_at")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->database = REMOTE;

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);

    }

    public function __toString()
    {
        return "$this->email";
    }
}
