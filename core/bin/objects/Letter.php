<?php

class Letter extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "letters";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->name = new Field("name");

        $this->xml = new Field("xml_file");

        $this->form = new Field("form_file");

        $this->type = new Field("type"); // ENUM

        $this->jasper_file = new Field("jasper_file");

        $this->rosie_file = new Field("rosie_file");

        $this->archived = Boolean::factory("archived");

        parent::__construct($id, $auto_get);
    }

    public static function byName($name)
    {
        // just a quick way to get a Letter
        $s = new Search(new Letter);

        $s->eq("name", $name);
        if ($l = $s->next()) {
            return $l;
        }

        return false;
    }

    public static function url_path_corrections($report_url)
    {
        // bunch of string replace to fix differences between path names in db and on rosie
        if ($report_url == "/reports/Standard_Letters/Drawdown  > 100") {
            $report_url = "/reports/Standard_Letters/Drawdown_100_Plus";
        } elseif ($report_url == "/reports/Standard_Letters/Drawdown  < 100") {
            $report_url = "/reports/Standard_Letters/Drawdown_100_Less";
        } elseif ($report_url == "/reports/Standard_Letters/DFM - Client Letter") {
            $report_url = "/reports/Standard_Letters/DFM___Client_Letter";
        } elseif ($report_url == "/reports/Standard_Letters/DFM - Partner Letter") {
            $report_url = "/reports/Standard_Letters/DFM___Partner_Letter";
        }

        $report_url = str_replace(" ", "_", $report_url);
        $report_url = str_replace(">", "Plus", $report_url);
        $report_url = str_replace("<", "Less", $report_url);

        return $report_url;
    }

    public function __toString()
    {
        return "$this->name";
    }
}
