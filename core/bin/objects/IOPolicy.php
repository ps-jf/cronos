<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 17/10/2019
 * Time: 15:18
 */

class IOPolicy extends DatabaseObject
{
    const DB_NAME = IO_DB;
    const TABLE = "policies";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("io_id", Field::PRIMARY_KEY);

        $this->prophet_id = Field::factory("prophet_id");

        $this->database = IO;

        parent::__construct($id, $auto_get);
    }

    public function valuations($limit = 0)
    {
        $s = new Search(new IOValuation());
        $s->eq("io_policy", $this->id());
        $s->add_order("added_when", "DESC");

        if ($limit){
            $s->limit(1);

            if ($v = $s->next(MYSQLI_ASSOC)){
                return $v;
            } else {
                return false;
            }
        } else {
            $iovals = [];

            while ($v = $s->next(MYSQLI_ASSOC)){
                $iovals[] = $v;
            }

            return $iovals;
        }
    }
}