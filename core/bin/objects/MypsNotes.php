<?php

class MypsNotes extends DatabaseObject
{
    const DB_NAME = 'myps';
    const TABLE = "myps_notes";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object = Field::factory("object");

        $this->object_id = Field::factory("object_id");

        $this->is_visible = Field::factory("is_visible")
            ->set(1);

        $this->text = Field::factory("text");

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set(date('Y-m-d H:i:s'));

        $this->added_by = Sub::factory("WebsiteUser", "added_by")
            ->set(User::get_default_instance("myps_user_id"));

        $this->subject = Field::factory("subject");

        $this->note_type = Field::factory("note_type");

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->text";
    }

    public function save($return_query = false)
    {
        $className = $this->object();

        $obj = new $className($this->object_id(), true);

        $partner = null;
        $link = "/";

        switch(strtolower($className)){
            case "client":
                $partner = $obj->partner->id();
                $link .= "clients/";
                break;
            case "policy":
                $partner = $obj->client->partner->id();
                $link .= "policy/";
                break;
        }

        $link .= $obj->id();

        if (!empty($partner)){
            $notification = new MyPsNotification();

            $notification->partner($partner);
            $notification->title("Unread Note");
            $notification->text("New note added to " . class_basename($obj) . ": " . strval($obj));
            $notification->myps_link($link);

            $notification->save();
        }

        return parent::save($return_query);
    }
}
