<?php
/**
 * Maps branch entry from tblbranch
 *
 * A partner can be based out of one of many SJP offices
 * This is how we tell where they are from
 *
 *
 * @package prophet.objects.partner
 * @author daniel.ness
 */

class Branch extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblbranch";

    public function __construct($id = null, $auto_get = null)
    {
        $this->id = Field::factory("branchID", Field::PRIMARY_KEY);
        $this->name = Field::factory("branch");
        $this->address = Address::factory();

        parent::__construct($id, $auto_get);
    }

    function __toString()
    {
        return "$this->name";
    }
}
