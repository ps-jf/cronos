<?php

class Invoices extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "invoices";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true);

        $this->policy = Sub::factory("Policy", "policyID")
            ->set_var(Field::REQUIRED, true);

        $this->issuer = Sub::factory("Issuer", "issuerID")
            ->set_var(Field::REQUIRED, true);

        $this->reason = Field::factory('reason');

        $this->total = Field::factory('total')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->vat = Field::factory('vat')
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->date_invoiced = Date::factory('date_invoiced')
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::REQUIRED, true);

        $this->date_received = Date::factory('date_received')
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->notes = Field::factory('notes');

        $this->frequency = Choice::factory("frequency")
            ->push("0", "One Time Only")
            ->push("1", "Every Month")
            ->push("3", "Every 3 Months")
            ->push("6", "Every 6 Months")
            ->push("12", "Every 12 Months");

        $this->object_type = Choice::factory("object_type")
            ->push("client", "Client")
            ->push("issuer", "Issuer")
            ->push("partner", "Partner");

        $this->reminder_only = Field::factory('reminder_only');

        $this->CAN_DELETE = true;

        parent:: __construct($id, $autoget);
    }

    public function __toString()
    {
        // Reference (£xxx)
        return "$this->reference ( $this->total )";
    }
}
