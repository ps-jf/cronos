<?php

class PolicyGRI extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "policy_gri";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->policy = Sub::factory("Policy", "policy_id");

        $this->client = Sub::factory("Client", "client_id");

        $this->partner = Sub::factory("Partner", "partner_id");

        $this->renewal_lifetime = Field::factory("renewal_lifetime", Field::CURRENCY);

        $this->renewal_12 = Field::factory("renewal_12", Field::CURRENCY);

        $this->current_gri = Field::factory("current_gri", Field::CURRENCY);

        $this->current_minus_one_gri = Field::factory("current_minus_one_gri", Field::CURRENCY);

        $this->current_minus_two_gri = Field::factory("current_minus_two_gri", Field::CURRENCY);

        parent::__construct($id, $auto_get);
    }
}
