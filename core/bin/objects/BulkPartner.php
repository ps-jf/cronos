<?php
/**
 * Maps a partner from the database into an object.
 *
 * Like agencies extending policies, bulk partners are an extension
 * over partners, they simply have more complicated operations
 * on their accounts.
 *
 *
 * @package prophet.objects.partner
 * @author kevin.dorrian
 */

class BulkPartner extends Partner
{
    public function __construct($id = false, $autoget = false)
    {
        $this->btrans = Choice::factory("bulkTransferComplete")
            ->setSource(DATA_DB . ".tbltransferstatus", "id", "status");

        $this->network = Choice::factory("prevNetwork")
            ->setSource(DATA_DB . ".tblnetworks", "id", "networkName");

        $this->bulkstartdate = Date::factory("bulkStartDate")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::ISO_8601_SHORT);

        $this->bulkcompletedate = Date::factory("bulkCompleteDate")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::ISO_8601_SHORT);

        $this->bam = Choice::factory("bam")
            ->setSource(DATA_DB . ".tblbusacqman", "id", "name");

        $this->btm = Choice::factory("btm")
            ->setSource(DATA_DB . ".tblbustransman", "id", "name");

        $this->online_access_discussed = Boolean::factory("online_access_discussed");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        if (isset($this->code)) {
            return "$this->code";
        } else {
            return "";
        }
    }

    //CAREFUL here the date object returns a null for zero dates and a date time string with dates with no DISPLAY_FORMAT property set this makes date null comparisons tricky
    public function on_update($post = false)
    {
        // Update Bulk start and complete dates as appropriate
        if ($post["btrans"] == 1 && ($this->bulkstartdate == "" || is_null($this->bulkstartdate) || $this->bulkstartdate == '0000-00-00')) {
            try {
                $db = new mydb();
                $db->query("update " . PARTNER_TBL . " set bulkStartDate = '" . date("Y-m-d") . "' WHERE partnerid = " . $this->id());
            } catch (Exception $e) {
                throw $e;
            }
        } elseif ($post["btrans"] == 1 && ($this->bulkcompletedate != "" || is_null($this->bulkcompletedate))) {
            try {
                $db = new mydb();
                $db->query("update " . PARTNER_TBL . " set bulkCompleteDate = '0000-00-00' WHERE partnerid = " . $this->id());
            } catch (Exception $e) {
                throw $e;
            }
        } elseif ($post["btrans"] == 2 && ($this->bulkcompletedate == "" || is_null($this->bulkcompletedate) || $this->bulkcompletedate == '0000-00-00')) {
            try {
                $db = new mydb();
                $db->query("update " . PARTNER_TBL . " set bulkCompleteDate = '" . date("Y-m-d") . "' WHERE partnerid = " . $this->id());
            } catch (Exception $e) {
                throw $e;
            }
        } elseif ($post["btrans"] == 3 && ($this->bulkcompletedate != "" || is_null($this->bulkcompletedate))) {
            try {
                $db = new mydb();
                $db->query("update " . PARTNER_TBL . " set bulkStartDate = '0000-00-00' WHERE partnerid = " . $this->id());
                $db->query("update " . PARTNER_TBL . " set bulkCompleteDate = '0000-00-00' WHERE partnerid = " . $this->id());
            } catch (Exception $e) {
                throw $e;
            }
        } elseif ($post["btrans"] == 4 && ($this->bulkcompletedate != "" || is_null($this->bulkcompletedate))) {
            try {
                $db = new mydb();
                $db->query("update " . PARTNER_TBL . " set bulkStartDate = '0000-00-00' WHERE partnerid = " . $this->id());
                $db->query("update " . PARTNER_TBL . " set bulkCompleteDate = '0000-00-00' WHERE partnerid = " . $this->id());
            } catch (Exception $e) {
                throw $e;
            }
        }
    }
}
