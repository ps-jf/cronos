<?php

class FlaggedCorrespondence extends DatabaseObject
{
    const TABLE = "corr_fes_flagged";
    const DB_NAME = DATA_DB;

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user = Sub::factory("User", "user_id")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->file_name = Field::factory('file_name');

        $this->dept = Field::factory('dept');

        $this->flagged_when = Date::factory("flagged_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->reason1 = Field::factory("q1");

        $this->reason2 = Field::factory("q2");

        $this->reason3 = Field::factory("q3");

        $this->reason4 = Field::factory("q4");

        $this->note = Field::factory("note");

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }
}
