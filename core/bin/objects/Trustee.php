<?php

class Trustee extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "client_trustee";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->client = Sub::factory("Client", "clientid")
            ->set_var(Field::REQUIRED, true);

        $this->company = Field::factory("company");

        $this->title = Title::factory("title");

        $this->firstname = Field::factory("fname");

        $this->surname = Field::factory("sname");

        $this->dob = Date::factory("dob")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->nino = Field::factory("nino")
            ->set_var(Field::PATTERN, "/^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}$/");

        $this->address = Address::factory(
            "address1",
            "address2",
            "address3",
            "postcode"
        );

        $this->servicing_level = Choice::factory("servlevel")
            ->push_array(array("1", "3", "6", "12", "18", "24", "36", "48", "L", "T"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->nationality = Choice::factory("nationality")
            ->setSource("select nationality, nationality from prophet.countries group by nationality");

        $this->residency = Choice::factory("residency")
            ->setSource("select alpha_3_code, en_short_name from prophet.countries group by en_short_name");

        $this->CAN_DELETE = true;
        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    public static function mandateCheck($policy)
    {
        $mandates = [];
        $s = new Search(new MandateTrustee());
        $s->eq('policy', $policy);

        while ($tm = $s->next(MYSQLI_OBJECT)) {
            $mandates[] = $tm;
        }

        return $mandates;
    }

    public function __toString()
    {
        $name = "";

        if (!$this->loaded) {
            $this->get();
        }

        if (isset($this->surname) && $this->surname != ""){
            // Mr. John Smith
            $name = "$this->title $this->firstname $this->surname";
        } elseif (isset($this->company) && $this->company != "") {
            $name = "$this->company";
        }

        return $name;
    }

}
