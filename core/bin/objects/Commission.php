<?php
/**
 * Maps a payment already made to the partner
 *
 *
 * We keep a record of every financial transaction that gets paid to partners
 * This is the object that allows us to access them.
 *
 *
 * @package prophet.objects.finance
 * @author daniel.ness
 *
 */

class Commission extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblcommissionauditsql";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("Auditrefno", Field::PRIMARY_KEY);

        $this->date = Date::factory("paidDate")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->batch = Sub::factory("Batch", "batchid");

        $this->policyNum = Field::factory("policyNum");

        $this->issuer = Field::factory("issuerName");

        $this->type = Sub::factory("CommissionType", "Commntype");

        $this->amount = Field::factory("amount", Field::CURRENCY);

        $this->rate = Field::factory("Partnershare", Field::PERCENT);

        $this->paid = Field::factory("CommtoPartner", Field::CURRENCY);

        $this->sjp_rate = Field::factory("SJP%", Field::PERCENT);

        $this->client = Sub::factory("Client", "clientID");

        $this->policy = Sub::factory("Policy", "policyID");

        $this->partner = Sub::factory("Partner", "partnerID");

        $this->description = Field::factory("Description");

        $this->vat = Field::factory("vat", Field::CURRENCY);

        $this->processed_by = Sub::factory("User", "editedBy");

        $this->currency_type = Choice::factory("currency_type")
            ->push('GBP', "GBP")
            ->push('HKD', "HKD")
            ->push('USD', "USD")
            ->push('EUR', "EUR")
            ->set('GBP');

        $this->order = array("id");

        parent::__construct($id, $auto_get);
    }

    function __toString()
    {
        // policynumber (£xxxx)
        return "$this->policyNum ( £ $this->amount )";
    }
}
