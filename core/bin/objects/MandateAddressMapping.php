<?php

class MandateAddressMapping extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "mandate_address_mapping";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->issuer = Sub::factory("Issuer", "issuer");

        $this->issuer_contact = Sub::factory("IssuerContact", "issuer_contact_id");

        $this->partner = Sub::factory("Partner", "partnerid");

        $this->policy = Sub::factory("Policy", "policy_id");

        $this->owner = Field::factory("owner");

        $this->mandate = Sub::factory("Mandate", "mandate_id");

        $this->client = Sub::factory("Client", "client_id");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
