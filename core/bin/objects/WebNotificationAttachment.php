<?php

class WebNotificationAttachment extends DatabaseObject
{
    var $table = WEB_NOTIFICATION_ATTACHMENT_TBL;

    public function __construct()
    {
        call_user_func_array(array("parent", "__construct"), func_get_args());
    }
}
