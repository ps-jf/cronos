<?php

class CallScoring2018 extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "call_scoring2018";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user = Choice::factory("user_id", "user_id")
            ->setSource("select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "where active = '1' and id not in (62,63,80) " .
                "order by first_name, last_name")
            ->set_var(Field::REQUIRED, true);

        $this->person2 = Field::factory('other_end');

        $this->date = Date::factory("date")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE)
            ->set_var(Field::REQUIRED, true);

        $this->q1 = Choice::factory("q1")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent begin with an appropriate greeting including company name, agent name and offer of assistance?")
            ->set_var(Field::REQUIRED, true);

        $this->q2 = Choice::factory("q2")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent obtain the name of the caller and where they are calling from?")
            ->set_var(Field::REQUIRED, true);

        $this->q3 = Choice::factory("q3")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent ask for/confirm the partner's name and SJP code??")
            ->set_var(Field::REQUIRED, true);

        $this->q4 = Choice::factory("q4")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent ask for/confirm the client's name, postcode, account number and date of birth?")
            ->set_var(Field::REQUIRED, true);

        $this->q5 = Choice::factory("q5")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent correctly establish the reason for the call?")
            ->set_var(Field::REQUIRED, true);

        $this->q6 = Choice::factory("q6")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent use effective questioning skills and demonstrate active listening?")
            ->set_var(Field::REQUIRED, true);

        $this->q7 = Choice::factory("q7")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent answer customer questions correctly?")
            ->set_var(Field::REQUIRED, true);

        $this->q8 = Choice::factory("q8")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent notify the customer of relevant documentation or section of MyPSaccount?")
            ->set_var(Field::REQUIRED, true);

        $this->q9 = Choice::factory("q9")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent offer the most appropriate solution to meet the caller's needs?")
            ->set_var(Field::REQUIRED, true);

        $this->q10 = Choice::factory("q10")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent have sufficient knowledge to handle the call effectively?")
            ->set_var(Field::REQUIRED, true);

        $this->q11 = Choice::factory("q11")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent pro-actively add value throughout the call?")
            ->set_var(Field::REQUIRED, true);

        $this->q12 = Choice::factory("q12")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent offer further assistance at the end of the call?")
            ->set_var(Field::REQUIRED, true);

        $this->q13 = Choice::factory("q13")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent close the call in appropriate manner?")
            ->set_var(Field::REQUIRED, true);

        $this->q14 = Choice::factory("q14")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent make the call sound polite, friendly and welcoming by using a positive tone of voice?")
            ->set_var(Field::REQUIRED, true);

        $this->q15 = Choice::factory("q15")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent adapt to the customer and build rapport?")
            ->set_var(Field::REQUIRED, true);

        $this->q16 = Choice::factory("q16")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent sound clear and confident throughout the call?")
            ->set_var(Field::REQUIRED, true);

        $this->q17 = Choice::factory("q17")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent control their speed and pace effectively throughout the call, avoiding long silences and interruptions?")
            ->set_var(Field::REQUIRED, true);

        $this->q18 = Choice::factory("q18")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent follow the correct procedures for placing a customer on hold or transferring a call?")
            ->set_var(Field::REQUIRED, true);

        $this->q19 = Choice::factory("q19")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent refrain from passing the call to another department, unless for a good reason?")
            ->set_var(Field::REQUIRED, true);

        $this->q20 = Choice::factory("q20")
            ->push("1", "No")
            ->push("2", "Yes")
            ->push("3", "N/A")
            ->set_var(Field::DISPLAY_NAME, "Did the agent leave notes on the client's account?")
            ->set_var(Field::REQUIRED, true);

        $this->score = Field::factory("score");

        $this->keep_on = Field::factory('keep_on');

        $this->work_on = Field::factory('work_on');

        $this->training_needs = Field::factory('training_needs');

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
