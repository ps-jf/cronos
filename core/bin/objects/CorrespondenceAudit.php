<?php

class CorrespondenceAudit extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "correspondence_audit";

    public function __construct($arg = false, $auto_get = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->corr_id = Field::factory("corrID");

        $this->checked_by = Field::factory("checkedBy")
            ->set(User::get_default_instance()->id());

        $this->date = Date::factory("dateStamp");

        $this->spot_checked = Boolean::factory("spot_checked");

        $this->accepted = Field::factory("accepted");

        $this->predicted = Boolean::factory("policyNum_predicted");

    }
}
