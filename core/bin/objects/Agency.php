<?php
/**
 * Maps a specific policy from tblpolicy to an Agency object.
 *
 * This object extends a standard policy object, but is treated differently due
 * to the different treatment of agencies in business logic
 *
 * @package prophet.objects.agency
 * @subpackage policy
 * @author kevin.dorrian
 *
 */

class Agency extends Policy
{

    /**
     * Standard constructor - see long description.
     *
     *
     * Calls parent, but first includes a few extra fields pertinent to agencies
     *
     * @package prophet.objects.constructors
     * @author daniel.ness
     *
     */
    public function __construct($id = false, $auto_get = false)
    {

        $this->short_bulk = Field::factory("short_bulk_policy");

        $this->old_bulk = Field::factory("old_bulk_policy");

        parent::__construct($id, $auto_get);

        $this->status = Choice::factory("Status")
            ->setSource("select ID, Field1 from " . POLICY_STATUS_TBL . " where ID in (5,12,15,16,13,24,17,14,23,10,22,28,29,34) order by Field1")
            ->set(2);

        $this->__set("status", $this->status);
    }

    /**
     * Standard toString function - see long description
     *
     *
     * An agency is defined by its code, so we should return that when printing
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     */

    public function __toString()
    {
        return "$this->code";
    }


    /**
     * Standard on_update function - see long description
     *
     *
     * When an agency code is transferred, the underlying policies should all be transferred as well
     * This means we need to perform an update on specific policies on update of all agencies
     * We also add an item of correspondence as a recording mechanism
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     */

    public function on_update($post = false)
    {
        // Update policy status as appropriate
        if ($post["status"] == 13 || $post["status"] == 17 || $post["status"] == 22 || $post["status"] == 24 || $post["status"] == 23) {
            try {
                if ($post["status"] == 13) {
                    $update_to = '2';
                } elseif ($post["status"] == 17) {
                    $update_to = '4';
                } elseif ($post["status"] == 22) {
                    $update_to = '22';
                } elseif ($post["status"] == 24) {
                    $update_to = '10';
                } elseif ($post["status"] == 23) {
                    $update_to = '22';
                }

                $db = new mydb();
                $db->query("update " . POLICY_TBL .
                    " inner join " . CLIENT_TBL .
                    " on " . POLICY_TBL . ".clientid = " . CLIENT_TBL . ".clientid" .
                    " inner join " . ISSUER_TBL .
                    " on " . POLICY_TBL . ".issuerid = " . ISSUER_TBL . ".issuerid" .
                    " set " . POLICY_TBL . ".status = " . $update_to .
                    " WHERE " . CLIENT_TBL . ".partnerid = " . $this->client->partner->id() . " and " . POLICY_TBL . ".issuerid = " . $this->issuer->id .
                    " and (" . CLIENT_TBL . ".bulkclient = '' or " . CLIENT_TBL . ".bulkclient is null) and " . POLICY_TBL . ".status = 1 and " . POLICY_TBL . ".added_how = 4 and ".POLICY_TBL.".mandate_id is null");


                // Add correspondence regarding status update
                $corr = new Correspondence();
                $corr->policy($this->id);
                $corr->client($this->client->id());
                $corr->partner($this->client->partner->id());
                $corr->message('Agency Status Updated');
                $corr->subject('Bulk Transfer System');
                $corr->sender($_COOKIE["uid"]);
                $corr->corrnotsend(1);
                $corr->save();
            } catch (Exception $e) {
                throw $e;
            }
        } # Agency transferred away from PS - Leavers process - update policies to transferred away from PS
        elseif ($post["status"] == 10) {
            $db = new mydb();
            $db->query("update " . POLICY_TBL .
                " inner join " . CLIENT_TBL .
                " on " . POLICY_TBL . ".clientid = " . CLIENT_TBL . ".clientid" .
                " inner join " . ISSUER_TBL .
                " on " . POLICY_TBL . ".issuerid = " . ISSUER_TBL . ".issuerid" .
                " set " . POLICY_TBL . ".status = 10 " .
                " WHERE " . CLIENT_TBL . ".partnerid = " . $this->client->partner->id() . " and " . POLICY_TBL . ".issuerid = " . $this->issuer->id .
                " and (" . CLIENT_TBL . ".bulkclient = '' or " . CLIENT_TBL . ".bulkclient is null)");
        }
    }
}
