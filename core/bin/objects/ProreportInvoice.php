<?php

class ProreportInvoice extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "proreport_invoicing";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->corr = Sub::factory("Correspondence", "corrID")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set_var(Field::REQUIRED, true);

        $this->date = Date::factory("date")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->amount = Field::factory("amount", Field::CURRENCY)
            ->set_var(Field::REQUIRED, true);

        $this->client_option = Field::factory("client");

        $this->workflow_id = Field::factory("workflow_id");

        $this->invoice_id = Field::factory("invoice_id");

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }


    public function __toString()
    {
        return "$this->id";
    }
}
