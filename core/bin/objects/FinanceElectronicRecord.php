<?php

class FinanceElectronicRecord extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "finance_electronic_records";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->issuer = Sub::factory("Issuer", "issuer_id");

        $this->statement_date = Date::factory("statement_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->reference = Field::factory("reference");

        $this->entries = Field::factory("entries");

        $this->total_value = Field::factory("total_value", Field::CURRENCY);

        $this->bank_entry_date = Date::factory("bank_entry_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->process_date = Date::factory("process_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->batch = Sub::factory("Batch", "batch_id");

        $this->entry_type = Field::factory("entry_type");

        $this->balanced = Boolean::factory("balanced");

        $this->processed_by = Sub::factory("User", "processed_by")
            ->set(User::get_default_instance("id"));

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    function __toString()
    {
        return "$this->id";
    }
}
