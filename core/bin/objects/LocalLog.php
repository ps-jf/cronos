<?php

class LocalLog extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "log_actions_new";
    //const TABLE = "log_actions";

    public function __construct($id = false, $autoload = false)
    {
        /** DON'T REMOVE - would write infinite entries to db **/
        $this->ACTION_LOG = false;
        /** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! **/
        $this->order = [["id", Search::ORDER_DESCEND]];

        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->date = Date::factory("datetime")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->user = Sub::factory("User", "user_id")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->object_name = Field::factory("object")
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->object_id = Field::factory("object_id")
            ->set_var(Field::TYPE, Field::INTEGER)
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->snapshot = Field::factory("snapshot")
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->type = Field::factory("type")
            ->set_var(Field::BLOCK_UPDATE, true);

        $this->old_object_id = Field::factory("old_object_id")
            ->set_var(Field::BLOCK_UPDATE, true);

        parent::__construct($id, $autoload);
    }

    public function on_get()
    {
        $oname = $this->object_name();
        $this->object = new $oname($this->object_id);
        if ($this->object->SNAPSHOT_LOG) {
            //$this->object->load(unserialize($this->snapshot()));
            $this->object->load(json_decode($this->snapshot(), true));
        }
    }

    function __toString()
    {
        return "$this->type $this->object_name($this->object_id)";
    }

    public static function mergeHistory ($object, $id) {
        $dedupe_array = [];

        $s = new Search(new DedupePolicy);
        $s->eq('newPolicyID', $id);
        while ($dedupe = $s->next(MYSQLI_OBJECT)) {
            $dedupe_array[] = $dedupe;
        }

        return $dedupe_array;
    }


}
