<?php

class AccountSegmentation extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "account_segmentation";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partner_id")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->soft_rating = Field::factory("soft_rating");

        $this->overall_rating = Field::factory("overall_rating");

        $this->alert_cost = Field::factory("alert_cost");

        $this->alert_unread = Field::factory("alert_unread");

        $this->alert_csrd = Field::factory("alert_csrd");

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    function __toString()
    {
        return $this->policy . "(" . $this->ac_requested . ")";
    }

    public function star_rating($rating = false)
    {
        $stars = "";

        if ($rating == 0) {
            $stars = "<img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating > 0 && $rating <= 10) {
            $stars = "<img class='stars' src='/lib/images/star_half.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 11 && $rating <= 20) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 21 && $rating <= 30) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_half.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 31 && $rating <= 40) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 41 && $rating <= 50) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_half.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 51 && $rating <= 60) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 61 && $rating <= 70) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_half.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 71 && $rating <= 80) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_empty.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 81 && $rating <= 90) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_half.png' title='Total Rating: $rating/100'>";
        }

        if ($rating >= 91) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>
                        <img class='stars' src='/lib/images/star_full.png' title='Total Rating: $rating/100'>";
        }

        return $stars;
    }

    public function alert_cost($alert_cost = false)
    {
        if ($alert_cost == 1) {
            return "<img class='alert' src='/lib/images/alert_money.png' title='This alert is being displayed as we earn no money from this Partner yet process correspondence for them or because we do more work for them than money earned'>";
        }
    }

    public function alert_csrd($alert_csrd = false)
    {
        if ($alert_csrd == 1) {
            return "<img class='alert' src='/lib/images/alert_csrd.png' title='It seems that this Partner is not participating in CSRD'>";
        }
    }

    public function alert_unread($alert_unread = false)
    {
        if ($alert_unread == 1) {
            return "<img class='alert' src='/lib/images/alert_mail.png' title='Partner has read less than 3% of their mail in the last 6 months'>";
        }
    }
}
