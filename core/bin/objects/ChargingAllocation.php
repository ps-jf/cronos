<?php

class ChargingAllocation extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "charging_allocation";

    public function __construct($id = false, $auto_get = false)
    {
        $this->staff = Sub::factory("Staff", "staff_id")
            ->set_var(Field::REQUIRED, true);

        $this->partner = Sub::factory("Partner", "partner_id")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $auto_get);
    }

    //get partners key contact for adviser charging
    public static function key_contact_span($partner_id)
    {
        $s = new Search(new ChargingAllocation());
        $s->eq('partner', $partner_id);

        while ($c = $s->next(MYSQLI_ASSOC)) {
            $key_contact = new User($c->staff(), true);
            $contact = $key_contact->forename . " " . $key_contact->surname;
            $span = "<div class='notification static' style='display:block;'>Adviser Charging Key Contact: " . $contact . "</div>";
        }

        if (isset($span)) {
            return $span;
        }
    }

    public static function packs_produced($partner_id)
    {
        $count = 0;

        $db = new mydb();
        $q = "SELECT count(*) AS counter, MAX(datestamp) AS datestamp FROM tblcorrespondencesql
                INNER JOIN tblpolicy
                ON tblcorrespondencesql.Policy = tblpolicy.policyID
                WHERE subject = 'Adviser Pack Sent to Partner'
                AND issuerID IN(1152, 86, 1216)
                AND Partner = " . $partner_id;

        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            if ($row['counter'] > 1) {
                $count = $row['counter'] . " (Latest: " . date("jS M Y", strtotime($row['datestamp'])) . ")";
            } else {
                $count = $row['counter'];
            }
        }

        return $count;
    }

    public static function packs_to_provider($partner_id)
    {
        $count = 0;

        $db = new mydb();
        $q = "SELECT count(*) AS counter, MAX(datestamp) AS datestamp FROM tblcorrespondencesql
                INNER JOIN tblpolicy
                ON tblcorrespondencesql.Policy = tblpolicy.policyID
                WHERE subject = 'Adviser Charging Form sent to Provider'
                AND issuerID IN(1152, 86, 1216)
                AND Partner = " . $partner_id;

        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            if ($row['counter'] > 1) {
                $count = $row['counter'] . " (Latest: " . date("jS M Y", strtotime($row['datestamp'])) . ")";
            } else {
                $count = $row['counter'];
            }
        }

        return $count;
    }

    public static function key_contact($partner_id)
    {
        $s = new Search(new ChargingAllocation());
        $s->eq('partner', $partner_id);

        while ($c = $s->next(MYSQLI_ASSOC)) {
            $key_contact = new User($c->staff(), true);
            $contact = $key_contact->forename . " " . $key_contact->surname;
        }

        if (isset($contact)) {
            return $contact;
        } else {
            return "<i>Unassigned</i>";
        }
    }

    public function ac_percentage($partner_id)
    {
        $json = array(
            "total_at_risk" => null,
            "percent" => null
        );

        $plats = array(
            1152, # CoFunds
            86,   # Fidelity
            1216, # Old Mutual Wealth Platform, formally know as @SiS
        );
        $plat_str = implode(', ', $plats);

        $db = new mydb();
        $db->query("select client_id, sum(renewal_12) as renewal_12, sum(renewal_lifetime)
                    from feebase.policy_gri
                    inner join feebase.tblclient
                    on tblclient.clientid = policy_gri.client_id
                    inner join feebase.tblpolicy
                    on tblpolicy.policyid = policy_gri.policy_id
                    where tblclient.partnerid = " . $partner_id . "
                    and tblpolicy.issuerid in (" . $plat_str . ")
                    and tblpolicy.status = 2
                    and tblpolicy.ac_exempt = 0
                    and renewal_12 IS NOT NULL
                    group by tblclient.clientid
                    order by renewal_12 desc");

        $clients = [];
        $gri_12_total = 0;
        $gri_12_submitted = 0;

        //make an array of clients that are brought back from the above query
        while ($x = $db->next(MYSQLI_ASSOC)) {
            $gri_12_total += $x['renewal_12'];
            if (!in_array($x['client_id'], $clients)) {
                $clients[] = $x['client_id'];
            }
        }


        /* for each client we want to check that every policy under the platforms has a correspondence item with
        subject 'Adviser Charging Form sent to Provider', if not we will define this as at risk */
        $client_to_remove = [];

        foreach ($clients as $c) {
            $db = new mydb();
            $db->query("SELECT DISTINCT policyID FROM feebase.tblpolicy
                        WHERE clientID = " . $c . "
                        AND issuerID IN(" . $plat_str . ")
                        AND status = 2 AND tblpolicy.ac_exempt = 0");

            $policies_returned = [];

            while ($p = $db->next(MYSQLI_NUM)) {
                //add unique policyIDs to an array for checking, $p[0] == policyID
                $policies_returned[] = $p[0];
            }

            $safe_policies = [];

            foreach ($policies_returned as $pol) {
                //check if policy has correspondence against it
                $db->query("SELECT Policy FROM feebase.tblcorrespondencesql
                          WHERE Policy = " . $pol . " AND subject = 'Adviser Charging Form sent to Provider'");

                while ($corr = $db->next(MYSQLI_NUM)) {
                    //if row returned, add to safe_policies array,  $corr[0] == policy id
                    if ($corr[0]) {
                        $safe_policies[] = $corr[0];
                    }
                }

                //check if policy has ac applied
                $db->query("SELECT policyID FROM feebase.tblpolicy
                          WHERE policyID = " . $pol . " AND ac_applied = 1");

                while ($ac_applied = $db->next(MYSQLI_NUM)) {
                    //if row returned, add to safe_policies array,  $ac_applied[0] == policy id
                    if ($ac_applied[0]) {
                        if (!in_array($ac_applied[0], $safe_policies)) {
                            $safe_policies[] = $ac_applied[0];
                        }
                    }
                }
            }

            //create a new array with the safe policies removed
            $policies_array = array_diff($policies_returned, $safe_policies);

            //array is empty there for all policies under this client are safe to be removed.
            if (empty($policies_array)) {
                $client_to_remove[] = $c;
            }
        }

        /* if a partner has no clients to show, add value of 0 to the array so to not break the
            IN clause on the search below  */
        if (empty($client_to_remove)) {
            $client_to_remove[] = 0;
        }
        $client_str = implode(', ', $client_to_remove);

        $db = new mydb();
        $db->query("select sum(renewal_12) as renewal_12
                    from feebase.policy_gri
                    inner join feebase.tblclient
                    on tblclient.clientid = policy_gri.client_id
                    inner join feebase.tblpolicy
                    on tblpolicy.policyid = policy_gri.policy_id
                    where tblclient.clientID in(" . $client_str . ")
                    and tblpolicy.issuerid in (" . $plat_str . ")
                    and tblpolicy.status = 2
                    and tblpolicy.ac_exempt = 0
                    and renewal_12 IS NOT NULL
                    group by tblclient.clientid
                    order by renewal_12 desc");


        while ($d = $db->next(MYSQLI_NUM)) {
            $gri_12_submitted += $d[0];
        }

        if ($gri_12_submitted != 0) {
            $percent = $gri_12_submitted / $gri_12_total * 100;
        } else {
            $percent = 0;
        }

        $json['percent'] = number_format((float)$percent, 2, '.', '');

        //$json['total_at_risk'] = number_format((float)$gri_12_total, 2, '.', '');

        return $json['percent'];
    }
}
