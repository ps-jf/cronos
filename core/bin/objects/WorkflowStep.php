<?php

class WorkflowStep extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "workflow_step";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("stepID", Field::PRIMARY_KEY);

        $this->desc = Field::factory("stepDesc");

        $this->process = Field::factory("processID");

        $this->next_step = Field::factory("nextstepID");

        $this->order = Field::factory("order");

        $this->group = Field::factory("group");

        $this->loop = Boolean::factory("loop");

        $this->timescale = Field::factory("est_timescale");

        $this->active = Boolean::factory("active");

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        /* Simply return the description */
        return "$this->desc";
    }
}
