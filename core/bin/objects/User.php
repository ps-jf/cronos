<?php

class User extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "users";
    const LOGIN_TBL = "prophet.log_logins";
    const LOGIN_SALT = "startmeup!";
    const COOKIE_USER_ID = "uid";
    const COOKIE_USER_HASH = "u_enc";
    const COOKIE_USER_EXPIRY = "u_expiry";
    const DIRECTORS = [
        23, // Kevin Dorrian
        29, // Avril Braes (Director for purposes of use, may need changed in future)
        60, // Kayleigh Dorrian
        98, // Louise Gibosn (Director for purposes of use, may need changed in future)
        253, // Peter Craddock
        254, // Brian Galvin
        255, // Gail Glen
    ];
    var $_profilePath = "/pages/user/";
    var $timeOut = 10800; // 3 hours

    public function __construct($id = false, $autoget = false)
    {
        $this->ACTION_LOG = false;

        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->username = Field::factory("user_name")
            ->set_var(Field::REQUIRED, true);

        $this->forename = Field::factory("first_name");

        $this->surname = Field::factory("last_name");

        $this->password = Field::factory("passwd", Field::PASSWD)
            ->set_var(Field::REQUIRED, true);

        $this->email = Field::factory("email_address")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->status = Field::factory("status");

        $this->myps_user_id = Field::factory('myps_user_id');

        $this->department = Sub::factory("UserGroup", "department");

        $this->internal_extension = Field::factory("internal_ext")
            ->set_var(Field::PATTERN, "/^\d{3}$/")
            ->set_var(Field::MAX_LENGTH, 3);

        $this->external_extension = Field::factory("external_ext")
            ->set_var(Field::PATTERN, "/^\d{3}$/")
            ->set_var(Field::MAX_LENGTH, 3);

        $this->active = Boolean::factory("active");

        $this->weekend_staff = Boolean::factory("weekend_staff");

        $this->password_expired = Field::factory("password_expired");

        $this->mobile = Field::factory("mobile_number");

        $this->team_leader = Boolean::factory("team_leader");

        $this->ac_team = Field::factory("ac_team");

        $this->staff_start = Date::factory("staff_start")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->staff_leave = Date::factory("staff_leave")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->beta_tester = Boolean::factory("beta_tester")
            ->set(1);

        $this->spot_check_percent = Field::factory("spot_check_percent");

        $this->first_aider = Boolean::factory("first_aider");

        $this->fire_warden = Boolean::factory("fire_warden");

        $this->smt = Boolean::factory("smt");

        parent::__construct($id, $autoget);
    }

    static function get_default_instance($property = false)
    {
        // Return an instance of the current User
        if (isset($_COOKIE[User::COOKIE_USER_ID])) {
            $user = new User($_COOKIE[User::COOKIE_USER_ID]);

            if ($user->get()) {
                // if $property arg defined, just get value
                return ($property) ? $user->{$property}() : $user;
            }
        }
        return false;
    }

    public static function correspondence_best($user_id)
    {
        $db = new mydb();
        $db->query("select count(*) as total, sender, date_format(datestamp, '%d-%m-%Y') " .
            "from tblcorrespondencesql " .
            "where sender = $user_id " .
            "group by day(datestamp), month(datestamp), year(datestamp) " .
            "order by count(*) desc limit 1");

        if ($max = $db->next(MYSQLI_ASSOC)) {
            return $max['total'];
        } else {
            return "0";
        }
    }

    public function auth()
    {
        //// Sets required security cookies for user
        $timeout = null;
        setcookie(User::COOKIE_USER_ID, $this->id(), $timeout, "/");
        setcookie(User::COOKIE_USER_HASH, salty_md5(User::LOGIN_SALT . $this->id()), $timeout, "/");
        setcookie(User::COOKIE_USER_EXPIRY, time() + $this->timeOut, 0, "/");
        return $timeout;
    }

    public static function loginUsingId($id)
    {
        $user = new User($id, true);

        $user->auth();
    }

    public function login($username = false, $password = false)
    {
        // Set status, cookies, and audit visit
        $status = $msg = false;

        $auth = new Search($this);
        $auth->eq("password", $password)
            ->add_or(
                $auth->eq("email", $username, false, false),
                $auth->eq("username", $username, false, false)
            );

        // get a count of matching users
        $matches_n = $auth->execute();

        if ($matches_n == 0) { // credentials don't match old system
            $authBcrypt = new Search($this);
            $authBcrypt->add_or(
                $auth->eq("email", $username, false, false),
                $auth->eq("username", $username, false, false)
            );

            $matched_accounts = [];

            while($u = $authBcrypt->next(MYSQLI_ASSOC)){
                // check for new style password
                if(password_verify($password, $u->password())){
                    $matched_accounts[] = $u;
                }
            }

            if(count($matched_accounts) > 1) {
                $msg = "System error, IT has been notified";
            } elseif(count($matched_accounts) === 1) {
                $user = $matched_accounts[0];

                if($user->active()){
                    $user->auth();           // set session cookies
                    $user->status("active"); // mark user's status as logged in #HACK to work around enums for local database
                    $user->save();

                    $log = $user->logLogin();
                    $status = $log['status'];
                    $msg = $log['msg'];
                } else {
                    $msg = "Access has been revoked for this account";
                }
            } else {
                $msg = "User details do not match";
            }
        } elseif ($matches_n > 1) { // duplication, keep user out until rectified
            $msg = "System error, IT has been notified";
        } else { // success!
            $u = $auth->next();

            // check that user hasn't been deactivated
            if ($u->active()) {
                $u->auth();           // set session cookies
                $u->status("active"); // mark user's status as logged in #HACK to work around enums for local database
                $u->save();

                $u->bcryptPassword($password);

                $log = $u->logLogin();
                $status = $log['status'];
                $msg = $log['msg'];
            } else {
                $msg = "Access has been revoked for this account";
            }
        }

        return ["status" => $status, "msg" => $msg];
    }

    public function logout($logout_type = "logout")
    {
        /* Logout this user */
        $this->deauth($logout_type);
    }

    private function deauth($logout_type = "logout")
    {
        $cookie_array = [];

        // Reset all security cookies
        foreach ($_COOKIE as $k => $v) {
            $cookie_array[$k] = $v;
            setcookie($k, -1, 2000, "/");
        }

        if ($this->status() != false) {
            $c = $cookie_array['uid'];

            //Perform a search to see if user id exists
            $db = new mydb();
            $q = "SELECT * FROM prophet.users where id = '$c'";
            $db->query($q);

            while ($d = $db->next(MYSQLI_ASSOC)) {
                $userID = $d['id'];
            }

            //if user id exists, log out user and update entry in db to status offline
            if ($userID == $cookie_array['uid']) {
                try {
                    // audit logout time for user - mysql version
                    $db = new mydb();
                    if ($logout_type == "logout") {
                        $db->query("INSERT into " . LOG_TBL . " (user_id, datime, direction) VALUES (" . $userID . ", " . time() . ", 'LOGOUT')");
                        //sets user status
                        $this->status("offline");
                    } else if ($logout_type == "lunch") {
                        $db->query("INSERT into " . LOG_TBL . " (user_id, datime, direction) VALUES (" . $userID . ", " . time() . ", 'LUNCH')");
                        //sets user status
                        $this->status("lunch");
                    } else if ($logout_type == "break") {
                        $db->query("INSERT into " . LOG_TBL . " (user_id, datime, direction) VALUES (" . $userID . ", " . time() . ", 'BREAK')");
                        //sets user status
                        $this->status("break");
                    }
                    $this->save();
                } catch (Exception $e) {
                    die($e->getMessage());
                }
            }
        }
        // Return user to login page
        exit("<script type='text/javascript'>$(function(){ window.location.reload( true ); });</script>");
    }

    public function logLogin()
    {
        $status = $msg = false;

        try {
            // audit login time for user - mysql version
            $db = new mydb();
            if ($db->query("INSERT INTO " . LOG_TBL . " (user_id, datime, direction) VALUES (" . $this->id() . ", " . time() . ", 'LOGIN')")) {
                // response data

                sleep(1);
                /* do sanity check */
                $db = new mydb();
                if ($db->query("SELECT user_id, datime, direction FROM 
                                    " . LOG_TBL . " where user_id = " . $this->id() . " 
                                    and direction = 'LOGIN'
                                   order by id desc limit 1")) {
                    while ($d = $db->next(MYSQLI_ASSOC)) {
                        $time = $d['datime'];
                    }

                    $rowcount = $db->affected_rows;
                    // response data
                    if ($rowcount == 0) {
                        $status = false;
                        $msg = "Login failed  - No database entry";
                    } else {
                        //Get the current timestamp
                        $curtime = time();

                        if (($curtime - $time) > 6) {     //6 seconds - any longer and it fails
                            $status = false;
                            $msg = "Login failed  - Database entry not consistent";
                        } else {
                            $status = true;
                            $msg = "Login successful";
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $msg = 'Caught exception: ' . $e->getMessage() . "\n";
        }

        return [
            "status" => $status,
            "msg" => $msg,
        ];
    }

    public function bcryptPassword($password)
    {
        $db = new mydb();

        $db->query("UPDATE prophet.users SET passwd = '" . password_hash($password, PASSWORD_DEFAULT) . "' WHERE id = " . $this->id());

        $db->execute();
    }

    public function valid_cookies()
    {
        return (salty_md5(User::LOGIN_SALT . $_COOKIE[User::COOKIE_USER_ID]) == $_COOKIE[User::COOKIE_USER_HASH]);
    }

    public function get_recently_viewed_rs()
    {
        #todo get this in Redis!
        /*
          Gets the last x profiles viewed by the user.

           Return a mydb instance from which ->next( ) can be called to
           get results without having to loop twice... pedantic, I know.
        */

        $db = new mydb();
        if ($db->query("SELECT * FROM `" . SYS_DB . "`.recently_viewed WHERE user_id=" . $this->id())) {
            return $db;
        } else {
            return false;
        }
    }

    function register_recently_viewed(&$object)
    {
        #todo get this in Redis!
        $db = new mydb();

        $pieces = [
            "user_id=$this->id",
            "object_type=\"" . get_class($object) . "\"",
            "object_id=$object->id",
            "object_link=\"" . addslashes($object->link()) . "\""
        ];

        // don't allow duplicates
        $db->query("delete from " . VIEWED_TBL . " where " . implode(" AND ", $pieces));

        // append entry
        $db->query("insert into " . VIEWED_TBL . " set " . implode(", ", $pieces));

        // and only keep track of the last 10 viewed profile per user
        if (($n = $db->query("SELECT true FROM " . VIEWED_TBL . " WHERE user_id=" . $this->id)) > 9) {
            $db->query("DELETE FROM " . VIEWED_TBL . " WHERE user_id=" . $this->id . " ORDER BY id LIMIT " . ($n - 9));
        }
    }

    public static function getUserTime($id)
    {
        /* This gets the start time.*/

        #Gets the time of the first login, converted in to Hours, minutes and seconds
        $db = new mydb();
        $db->query(" SELECT FROM_UNIXTIME(MIN( datime ), '%H:%i:%s') as totalTime1 FROM prophet.log_logins     " .
            " WHERE user_id = " . $id .
            " AND DATE(from_unixtime(datime)) = CURDATE() ");

        while ($d = $db->next(MYSQLI_ASSOC)) {
            $total = $d["totalTime1"];
        }
        if ($total == null || $total == 0) {
            $resultsTotal = "No time logged for today";
        } else {
            $resultsTotal = $total;
        }

        return $resultsTotal;
    }

    function __toString()
    {
        /* Output username */
        return (string)$this->username;
    }

    public static function check_user_status($user_id)
    {
        $db = new mydb();
        $db->query("SELECT status FROM prophet.users WHERE id = " . $user_id);

        $status = "";

        if ($check = $db->next(MYSQLI_ASSOC)) {
            $status = $check['status'];
        }

        if ($status == "active") {
            $style = "style='color:rgb(0, 128, 0);'";
        } elseif ($status == "lunch") {
            $style = "style='color:rgb(255, 165, 0);'";
        } elseif ($status == "offline") {
            $style = "style='color:rgb(255, 0, 0);'";
        } elseif ($status == "meeting") {
            $style = "style='color:rgb(40, 170, 200);'";
        } elseif ($status == "busy") {
            $style = "style='color:rgb(195, 88, 255);'";
        } else {
            $style = "style='color:rgb(255, 0, 0);'";
        }

        return $style;
    }

    public function statusBadge()
    {
        switch($this->status()){
            case "active":
                $badge = "<span class='status-badge badge badge-pill badge-success'>Active</span>";
                break;
            case "lunch":
                $badge = "<span class='status-badge badge badge-pill badge-warning'>Lunch</span>";
                break;
            case "meeting":
                $badge = "<span class='status-badge badge badge-pill badge-primary'>Meeting</span>";
                break;
            case "busy":
                $badge = "<span class='status-badge badge badge-pill badge-info'>Busy</span>";
                break;
            default:
                $badge = "<span class='status-badge badge badge-pill badge-danger'>Offline</span>";
                break;
        }

        return $badge;
    }

    public function correspondence_target($user_id)
    {
        // Count the number of correspondence items added by this user today
        $s = new Search(new Correspondence);
        $s->eq("sender", $user_id);
        $s->eq("date", date("Y-m-d") . "%", true);
        $corr_count = $s->count();


        if ($corr_count == 0) {
            $stars = "<img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>
                          <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>
                          <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>";
        }

        if ($corr_count > 0 && $corr_count <= 56) {
            $stars = "<img class='stars' src='/lib/images/star_half.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>";
        }

        if ($corr_count >= 57 && $corr_count <= 112) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>";
        }

        if ($corr_count >= 113 && $corr_count <= 169) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_half.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>";
        }

        if ($corr_count >= 170 && $corr_count <= 226) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_empty.png' title='Total Processed: $corr_count'>>";
        }

        if ($corr_count >= 227 && $corr_count <= 279) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_half.png' title='Total Processed: $corr_count'>";
        }

        // Met
        if ($corr_count >= 280 && $corr_count <= 350) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>";
        }

        // Exceed
        if ($corr_count >= 350) {
            $stars = "<img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>
                      <img class='stars' src='/lib/images/star_full.png' title='Total Processed: $corr_count'>";
        }

        return $stars;
    }

    public function active_users($dept = false, $myps_ids = false)
    {
        /*
         * return a dropdown containing current members of staff
         */

        // Count the number of correspondence items added by this user today
        $s = new Search(new User);
        $s->eq("active", 1);
        $s->nt("id", 62);
        $s->nt("id", 63);
        $s->nt("id", 80);
        $s->add_order("username");

        if ($dept){
            $s->eq("department", $dept);
        }

        $dd = '<select id="staff_filter" title="Staff Filter">';
        $dd .= "<option value='-'>-- Staff --</option>";
        while ($user = $s->next(MYSQLI_ASSOC)) {
            $userID = $user->id();

            if ($myps_ids){
                $userID = $user->myps_user_id();
            }

            $dd .= "<option value='" . $userID . "'>" . $user->staff_name() . "</option>";
        }

        $dd .= '</select>';

        return $dd;
    }

    public static function active_users_array()
    {
        /*
         * return an array containing current members of staff
         */


        $user_array = [];

        // Count the number of correspondence items added by this user today
        $s = new Search(new User);
        $s->eq("active", 1);
        $s->nt("id", 62);
        $s->nt("id", 63);
        $s->nt("id", 80);
        $s->add_order("username");

        while ($user = $s->next(MYSQLI_ASSOC)) {
            $user_array[$user->id()] = $user->staff_name();
        }

        return $user_array;
    }

    public function staff_name()
    {
        return ucwords(str_replace(".", " ", $this->username));
    }

    public static function list_users($activeOnly = false, $attr = "", $value = null, $teamHeadsOnly = false, $required = true, $forceDirectors = false){
        //function to create a dropdown list of all users
        $s = new Search(new User);

        if ($activeOnly){
            $s->eq("active", 1);
        }

        $s->add_order("forename");

        $return = "<select " . $attr . "><option " . ($required ? 'disabled' : '') . " selected>--</option>";

        while($user = $s->next(MYSQLI_ASSOC)){
            if(!$teamHeadsOnly || $user->team_head($user) || ($forceDirectors && in_array($user->id(), User::DIRECTORS))){
                $return .= "<option value='" . $user->id . "' " . ($value == $user->id() ? 'selected' : '') . ">" . $user->forename . " " . $user->surname . "</option>";
            }
        }

        $return .= "</select>";

        return $return;
    }

    public function allDepartments($excludes = false, $selects = true, $selected = false, $attr = false, $required = true)
    {
        //function to create a dropdown list of all users
        $s = new Search(new UserGroup);

        if ($excludes){
            $s->nt("id", [8, 9, 12, 13]);
        }

        if ($selects){
            $return = "<select " . ($attr ? $attr : 'id=\'dept_filter\'') . "><option " . ($required ? 'disabled' : '') . " selected>--</option>";
        } else {
            $return = "";
        }

        while($group = $s->next(MYSQLI_ASSOC)){
            if ($group->id() == 1){
                $group->name = "Finance";
            } elseif ($group->id() == 4){
                $group->name = "Partner Accounts";
            }

            $return .= "<option value='" . $group->id() . "' " . ($selected == $group->id() ? 'selected' : '') . ">" . $group->name . "</option>";
        }

        if ($selects){
            $return .= "</select>";
        }

        return $return;
    }

    public static function hadLunch()
    {
        $db = new mydb();
        $db->query("select * from prophet.log_logins
            where date(from_unixtime(datime)) like date(CURDATE())
            and direction = 'LUNCH'
            and user_id = ".User::get_default_instance('id'));

        if ($lunch = $db->next(MYSQLI_ASSOC)) {
            return true;
        } else {
            return false;
        }
    }

    public static function getBreaks()
    {
        $total_break = 0;
        $db = new mydb();
        $db->query("select * from prophet.log_logins
            where date(from_unixtime(datime)) like date(CURDATE())
            and direction = 'BREAK'
            and user_id = " . User::get_default_instance('id'));

        while ($break_logout = $db->next(MYSQLI_ASSOC)) {
            $db_2 = new mydb();
            $db_2->query("select * from prophet.log_logins
            where date(from_unixtime(datime)) like date(CURDATE())
            and direction = 'LOGIN'
            and user_id = " . User::get_default_instance('id') . " 
            and id > " . $break_logout['id'] . " limit 1");

            if ($break_login = $db_2->next(MYSQLI_ASSOC)) {
                $total_break += ($break_login['datime'] - $break_logout['datime']);
            }
        }
        return $total_break;
    }

    public function groups($userid = false)
    {
        if ($userid){
            $user = new User($userid, true);
        } else {
            $user = $this;
        }

        $groups = [];

        $groups[] = $user->department();

        $s = new Search(new UsersGroups);
        $s->eq("user", $user->id());

        while($g = $s->next(MYSQLI_ASSOC)){
            $groups[] = $g->user_group();
        }

        $groups = array_unique($groups);

        return $groups;
    }

    public function validatePassword($password, $confirm = false)
    {
        $new_passwd = trim($password);

        $search = $new_passwd;
        $lines = file('forbidden_passwords.txt');

        foreach ($lines as $line) {
            $line = preg_replace("[\r\n]", '', $line);
            if ($line == $search) {
                throw new Exception("Passwords must not be on the forbidden password list");
                break;
            }
        }

        if ($confirm){
            if ($new_passwd != trim($_POST["confirm_passwd"])) {
                throw new Exception("New password fields do not match");
            }
        }

        if (strlen($new_passwd) < 7) {
            throw new Exception("Passwords must be at least 7 characters in length");
        } else {
            if (ctype_alpha($new_passwd) || ctype_digit($new_passwd)) {
                throw new Exception("Passwords must be a combination of alphanumeric characters ( symbols are allowed )");
            } else {
                if (stristr($new_passwd, $this->email()) > -1 || stristr($new_passwd, $this->username()) > -1) {
                    throw new Exception("Passwords may not contain your username or email address");
                } else {
                    if (stristr($new_passwd, $this->forename()) || stristr($new_passwd, $this->surname())) {
                        throw new Exception("Password must not contain your firstname or surname");
                    }
                }
            }
        }

        return $new_passwd;
    }

    public function checkUsername($username = false){
        if (!$username){
            $username = $this->username();
        }

        //// no duplicate user names
        $s = new Search(new User);
        $s->eq("username", $username);
        if ($match = $s->next()) {
            if ($match->id() != $this->id()) {
                $this->errors[] = "Username has already been taken";
            }
        }

        return (!sizeof($this->errors)); // true / false
    }

    public static function team_head($user = null)
    {
        if(empty($user)){
            $user = new User($_COOKIE["uid"], true);
        }

        if ($user->department() == 1){
            // Finance all have team leader permissions, trust me, its just easier that way
            if ($user->id() == 203){
                // Charmaine
                return true;
            } else {
                return false;
            }
        } else {
            if ($user->team_leader()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function has_permission($permission_id)
    {
        $s = new Search(new ObjectPermission());

        $s->eq("object_type", "User");
        $s->eq("object_id", $this->id());
        $s->eq("permission", $permission_id);

        if($s->next(MYSQLI_ASSOC)){
            return true;
        }

        // check if they have permission via department
        return $this->has_permission_via_group($permission_id);
    }

    public function has_permission_via_group($permission_id)
    {
        // check if they have permission via department
        $s = new Search(new ObjectPermission());

        $s->eq("object_type", "Group");
        $s->eq("object_id", $this->department());
        $s->eq("permission", $permission_id);

        return $s->next(MYSQLI_ASSOC);
    }

    public function has_permission_category($category)
    {
        $permissions = [];

        $s = new Search(new Permission());
        $s->eq("category", $category);

        while($p = $s->next(MYSQLI_ASSOC)){
            $permissions[] = $p->id();
        }

        if(count($permissions)){
            return $this->has_permission($permissions);
        }

        return false;
    }

    public function active()
    {
        $users = [];

        $s = new Search(new User());
        $s->eq("active", 1);
        $s->nt("id", [62, 63, 80]);
        $s->add_order("username");

        while($u = $s->next(MYSQLI_ASSOC)){
            $users[] = $u;
        }

        return $users;
    }
}
