<?php

class InvoiceReminder extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "invoice_reminder";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory('id', Field::PRIMARY_KEY);

        $this->invoiceID = Sub::factory("Invoices", "invoiceID")
            ->set_var(Field::REQUIRED, true);

        $this->date_due = Date::factory('date_due')
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->date_resend = Date::factory('date_resend')
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->archived = Field::factory('archived');

        parent:: __construct($id, $autoget);
    }

    public function __toString()
    {
        // Reference (£xxx)
        return "$this->reference ( $this->total )";
    }
}
