<?php


class ObjectPermission extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "object_permissions";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object_type = Field::factory("object_type");

        $this->object_id = Field::factory("object_id");

        $this->permission = Sub::factory("Permission", "permission_id");

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return strval($this->permission);
    }

    public function morph()
    {
        if($this->object_type() === "Group"){
            return new Groups($this->object_id(), true);
        }

        $type = $this->object_type();

        return new $type($this->object_id(), true);
    }
}