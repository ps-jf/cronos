<?php

class Fund extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "fund";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->sedol = Field::factory('sedol');

        $this->name = Field::factory('name')
            ->set_var(Field::REQUIRED, true);

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->name";
    }
}
