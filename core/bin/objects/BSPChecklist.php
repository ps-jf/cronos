<?php
/**
 * Created by PhpStorm.
 * User: Sean
 * Date: 2020-09-07
 * Time: 15:30
 */

class BSPChecklist extends DatabaseObject
{
    const TABLE = "bsp_checklist";
    const DB_NAME = DATA_DB;
    const ASPECTS = [
        "transferring_letter" => "Transferring Letter",
        "accepting_letter" => "Accepting Letter",
        "client_letter" => "Client Letter",
        "client_list" => "Client List",
        "compliance_sign_off" => "Compliance Sign Off"
    ];

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->bsp = Sub::factory("BSP", "bsp_id");

        $this->aspect = Choice::factory("aspect")
            ->set_var(Field::REQUIRED, true);

        foreach(BSPChecklist::ASPECTS as $key => $value){
            $this->aspect->push($key, $value);
        }

        $this->value = Boolean::factory("value");

        $this->updatedby = Sub::factory("User", "added_by")
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return strval($this->value);
    }
}