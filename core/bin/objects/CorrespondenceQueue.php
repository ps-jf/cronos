<?php

class CorrespondenceQueue extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "correspondence_queue";

    public function __construct($arg = false, $auto_get = false)
    {
        $this->id = Field::factory('CorrID', Field::PRIMARY_KEY);

        $this->queue = Field::factory("corr_queue");

        $this->staff = Sub::factory("User", 'staff')
            ->set_var(Field::REQUIRED, true)
            ->set(User::get_default_instance()->id());

        parent::__construct(false, $auto_get);
    }
}
