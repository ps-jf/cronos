<?php

class IncomeHoldReason extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "income_hold_reason";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->reason = Field::factory("reason");

        parent::__construct($id, $autoget);
    }

    function __toString()
    {
        return "$this->reason";
    }
}
