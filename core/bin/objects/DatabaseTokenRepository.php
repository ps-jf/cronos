<?php

class DatabaseTokenRepository
{
    /**
     * The database connection instance.
     *
     * @var \Illuminate\Database\ConnectionInterface
     */
    protected $connection;

    /**
     * The token database table.
     *
     * @var string
     */
    protected $table;

    /**
     * The number of seconds a token should last.
     *
     * @var int
     */
    protected $expires;

    /**
     * Create a new token repository instance.
     *
     * @param  int $expires
     * @return void
     */
    public function __construct($expires = 60)
    {
        $this->table = "password_reset";
        $this->expires = $expires * 60;
        $this->connection = 'myps';
    }
    

    /**
     * Create a new token record.
     *
     * @param $user
     * @return bool|string
     * @throws Exception
     */
    public function create($user)
    {
        $email = $user->email_address();

        $deleteExisting = $this->deleteExisting($user);

        if ($deleteExisting) {
            // We will create a new, random token for the user so that we can e-mail them
            // a safe link to the password reset form. Then we will insert a record in
            // the database so that we can verify the token within the actual reset.
            $token = $this->createNewToken();

            if ($token) {
                $options = [
                    'cost' => 10
                ];
                $hash = password_hash($token, PASSWORD_BCRYPT, $options);

                ob_start();
                $pwd = new PasswordReset();
                $pwd->email($email);
                $pwd->token($hash);
                $pwd->created_at(date('Y-m-d H:i:s', time()));

                if ($pwd->save()) {
                    ob_end_clean();
                    return $token;
                }
            }
        }

        return false;
    }

    /**
     * Delete all existing reset tokens from the database.
     *
     * @param  $user
     * @return int
     */
    protected function deleteExisting($user)
    {
        if ($rdb = new mydb(REMOTE)) {
            $q = "select * from myps.password_resets where email = '" . $user->email_address() . "'";

            $rdb->query($q);

            if ($x = $rdb->next()) {
                if ($rdb->query("delete from myps.password_resets where email ='" . $user->email_address() . "'")) {
                    return true;
                } else {
                    dd("Could not delete existing reset token(s)");
                }
            } else {
                return true;
            }
        } else {
            dd("could not connect to remote DB");
        }
    }

    /**
     * Create a new token for the user.
     *
     * @return string
     */
    public function createNewToken()
    {
        return hash_hmac('sha256', random_string(40), MYPS_APP_KEY);
    }

    /**
     * Delete a token record by user.
     *
     * @param $user
     * @return void
     */
    public function delete($user)
    {
        $this->deleteExisting($user);
    }
}
