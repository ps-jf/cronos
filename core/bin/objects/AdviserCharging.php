<?php

class AdviserCharging extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "adviser_charging";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->policy = Field::factory("policyID");

        $this->ac_requested = Field::factory("AC_requested");

        $this->fee_requested = Field::factory("fee_requested");

        $this->vat = Boolean::factory("vat_included");

        $this->sent = Date::factory("sent_to_provider")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->comments = Field::factory("comments");

        $this->pack_produced = Boolean::factory("pack_produced");

        $this->illustration_provided = Boolean::factory("illustration_provided");

        $this->addedwhen = Date::factory("addedWhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "addedBy")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->expected_first_pay = Date::factory("expected_first_pay")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->payment_frequency = Field::factory("payment_frequency");

        $this->SNAPSHOT_LOG = true;
        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    function __toString()
    {
        return $this->policy . "(" . $this->ac_requested . ")";
    }
}
