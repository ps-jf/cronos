<?php

/**
 * Groups together a variety  of database fields to form an address block.
 *
 *
 * This class allows the various fields throughout the database tables to be
 * grouped together to form one entity. Allows us to implement things
 * like postcode lookup etc
 *
 *
 * @package prophet.objects.types
 * @author daniel.ness
 *
 * @todo We need to make this more efficient. We pay by the request for this service and
 * currently we do two requests per lookup
 */

class Address extends Group
{
    /* credentials for Postcode Anywhere lookup service
     *
     * www.postcodeanywhere.co.uk
     *
     */

    const DEFAULT_DELIMITER = ",";
    const UK_POSTCODE_REGEX = '/^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$/';
    var $name;
    var $delimiter = Address::DEFAULT_DELIMITER;
    private $license = "CB82-JC35-BJ14-DY84";
    private $account = "POLIC11116";

    /**
     * Standard constructor - see long description.
     *
     *
     * Builds various input boxes if needed and outputs a standard
     * lookup button as well
     *
     * @package prophet.objects.constructors
     * @author daniel.ness
     *
     */
    function __construct($arg_list = false)
    {
        $args = func_get_args();
        $link = random_string(10);

        for ($i = 0; $i < sizeof($args); $i++) {
            $el = $args[$i];

            $name = ($i == sizeof($args) - 1) ? "postcode" : "line" . ($i + 1);

            if (!is_a($el, "Field")) {
                $el = new Field((string)$el);
            }

            $el->name = $name;
            $el->group_name = $this->name;
            if (!isset($el->className)) {
                $el->className = "";
            }
            $el->className .= "address $name $link ";
            $this->{$name} = $el;
        }

        // button
        //$this->button = el::bt( "Search", false, "value=\"$link\" class=\"address_search\"" );
        $this->button = '<div class="autocomplete-suggestions" style="display:inline-grid; max-height: 300px; max-width: 100%;"></div>';
    }


    /**
     * Allows us to specify the delimiter for an address object.
     *
     *
     * If we need an address as a string in any other format than
     * comma delimited, then this comes in handy(eg space delimited)
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param string $delimiter
     *
     * @return object address
     */
    function delimiter($delimiter = Address::DEFAULT_DELIMITER)
    {
        $this->delimiter = $delimiter;
        return $this;
    }

    /**
     * Standard toString function - see long description
     *
     *
     * If we want to print an address, as its a group(read array) we need to
     * return a string with delimter
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @return String address
     *
     */
    function __toString()
    {
        return join(", ", $this->as_array());
    }

    /**
     * Returns an array for use throughout the application.
     *
     *
     * If we want to manipulate an address in any way or perform
     * ops on it, then it's preferable to work with an array
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     * @return array address
     *
     */
    function as_array()
    {
        $lines = [];

        // 3/4, 33 Oswald Street, Glasgow, G1 4PG
        for ($i = 1; $i < 4; $i++) {
            $l = (string)$this->{"line$i"};
            if (!empty($l)) {
                $lines[] = $l;
            }
        }

        $lines[] = (string)$this->postcode;
        return $lines;
    }
}
