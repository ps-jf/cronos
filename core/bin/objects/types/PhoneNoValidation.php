<?php

class PhoneNoValidation
{
    //Credit: Thanks to Stuart Sillitoe (http://stu.so/me) for the original PHP that these samples are based on.
    //The key to use to authenticate to the service.
    private $Key;
    //The mobile/cell phone number to verify. This must be in international format (+447528471411 or 447528471411)
    // if no country code is provided or national format with a Country parameter provided
    // (07528471411 and GB as the Country parameter).
    private $Phone;
    //The ISO2 country code of the number you are trying to validate (if provided in national format).
    private $Country;
    //Holds the results of the query
    private $Data;


    public function __construct($Key, $Phone, $Country)
    {
        $this->Key = $Key;
        $this->Phone = $Phone;
        $this->Country = $Country;
    }

    function MakeRequest()
    {
        $url = "https://api.addressy.com/PhoneNumberValidation/Interactive/Validate/v2.20/json3.ws";

        $curl = curl_init();

        $data = [
            "Key" => $this->Key,
            "Phone" => $this->Phone,
            "Country" => $this->Country,
        ];

        $url = sprintf("%s?%s", $url, http_build_query($data));

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl));

        if(!empty($result->Items) && empty($result->Items[0]->Error)){
            $this->Data = $result->Items;
        } else {
            throw new Exception("An error occurred");
        }
    }

    function HasData()
    {
        if (!empty($this->Data)) {
            return $this->Data;
        }
        return false;
    }
}
