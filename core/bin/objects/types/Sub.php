<?php

/*
 * # The lack of this feature has been troubling me, and attempting to use 
 * # Choices as an alternative didn't stop the nagging. I present * fanfare * nested 
 * # DatabaseObject references. 
 * # 
 * # Example: each item of Correspondence has a corresponding Policy, Client and Partner.
*/

class Sub extends Field
{
    const HTML_DROPDOWN = "dropdown";
    const HTML_SUGGEST = "suggest";
    var $id;
    var $obj;
    private $loaded = false;

    public function __construct($obj = false, $databaseName = false, $settings = false)
    {
        $this->databaseName = $databaseName;
        $this->obj = $obj;

        parent::__construct($databaseName, $settings);
        $this->set_var(Field::TYPE, Field::INTEGER);
    }

    public function __call($name, $arguments)
    {
        //// forward unknown function calls to the wrapped object
        $this->get_object();
        return call_user_func_array(array($this->obj, $name), $arguments);
    }

    public function get_object($load = true)
    {
        if (is_string($this->obj)) {
            // Subobject is currently a string of the DatabaseObject class which it represents.

            if ($this()) {
                if (isset(DatabaseObject::$cache[$this->obj])) {
                    // If sub's ID is known, check the local session cache for an existing instance
                    if (isset(DatabaseObject::$cache[$this->obj][$this()])) {
                        // unserialize cached instance + apply as subobject
                        $this->obj = unserialize(DatabaseObject::$cache[$this->obj][$this()]);
                    } else {
                        // init a new instance
                        $this->obj = new $this->obj;
                    }
                } else {
                    // init a new instance
                    $this->obj = new $this->obj;
                }
            } else {
                // init a new instance
                $this->obj = new $this->obj;
            }
        }

        $this->obj->primary_key($this->value);

        if ($load) {
            $this->obj->get();
        }

        return $this->obj;
    }

    public function __get($p)
    {
        // assume to be subobject property access attempt if not immediately available
        if (!isset($this->{$p})) {
            return $this->get_object()->{$p};
        } else {
            return $this->{$p};
        }
    }

    public function set_object($object)
    {
        $this->get_object(false);
        $this->id($object->id());
        $this->obj = $object;
        $this->loaded = true;
    }

    public function toElement($id = false, $attr = false, $value = false, $url = false, $el_type = false, $classes=false)
    {
        if ($id === false) {
            $id = $this->get_html_id();     // get absolute DOM ID i.e. group name if nec.
        }

        $this->get_object();
        $this->db_object->register_field($this); // make property writeable

        if (!$el_type) {
            if (($type = $this->get_var(Field::HTML_FIELD_TYPE)) !== null) {
                $el_type = $type;
            } else {
                $el_type = DatabaseObject::ELEMENT_SUGGEST;
            }
        }

        /// Output specified/default element.
        switch ($el_type) {
            case DatabaseObject::ELEMENT_HIDDEN:
                // hidden field
                return el::hide($id, $this(), $attr);
                break;

            case DatabaseObject::ELEMENT_DROPDOWN:
                // standard <select/> populated by iterating Search instance's resultset
                return el::dropdown($id, new Search($this->obj), $this(), $attr, false, false);
                break;

            case DatabaseObject::ELEMENT_SUGGEST:
                // AJAX-based suggestions search

                if ($url === false) {
                    $url = $this->obj->index_url();
                }

                return el::suggest($id, $url, "search", false, $this(), "$this", $classes);
                break;
        }
    }

    public function set($value, $mark_as_edited = true)
    {
        parent::set($value, $mark_as_edited);

        $this->id = $value;

        // remember to set nested object it's available
        if (is_a("DatabaseObject", $this->obj)) {
            $this->obj->id->set($this->id);
        }

        return $this;
    }

    public function to_sql($value = null, $wildcard = false, $mode = false)
    {
        // @ Overrides Field::to_sql
        // nb: does not use $wildcard, included for compatibility

        return parent::to_sql($value, false, $mode);
    }

    public function __toString()
    {
        $this->get_object();
        return "$this->obj";
    }
}
