<?php

class AddressFinder
{
    //Credit: Thanks to Stuart Sillitoe (http://stu.so/me) for the original PHP that these samples are based on.
    //The key to use to authenticate to the service.
    private $Key;
    //The search term to find. If the LastId is provided, the SearchTerm searches within the results from the LastId.
    private $SearchTerm;
    //The name or ISO 2 or 3 character code for the country to search in.
    // Most country names will be recognised but the use of the ISO country code is recommended for clarity.
    private $Country;
    //The 2 or 4 character language preference identifier e.g. (en, en-gb, en-us etc).
    private $LanguagePreference;
    //The container breaks down the address by the last searched term
    private $Container;
    //Holds the results of the query
    private $Data;


    public function __construct($Key, $SearchTerm, $Country, $LanguagePreference, $container="")
    {
        $this->Key = $Key;
        $this->SearchTerm = $SearchTerm;
        $this->Country = $Country;
        $this->LanguagePreference = $LanguagePreference;
        $this->Container = $container;
    }


    function MakeRequest()
    {
        $url = "http://api.addressy.com/Capture/Interactive/Find/v1.1/json3.ws";

        $curl = curl_init();

        $data = [
            "Key" => $this->Key,
            "Text" => $this->SearchTerm,
            "Countries" => $this->Country,
            "Language" => $this->LanguagePreference,
            "Container" => $this->Container,
        ];

        $url = sprintf("%s?%s", $url, http_build_query($data));

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl));

        if(empty($result->Error) && !empty($result->Items)){
            $this->Data = $result->Items;
        }
    }

    function HasData()
    {
        if (!empty($this->Data)) {
            return $this->Data;
        }
        return false;
    }
}
