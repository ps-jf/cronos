<?php

class JSON extends Field
{
    public function __invoke($value = null)
    {
        if ($value == null) {
            return json_encode($this->value);
        }
        $this->set($value);
    }

    public function set($value, $mark_as_edited = true)
    {
        parent::set($value, $mark_as_edited);

        if (is_string($this->value)) {
            $this->value = json_decode($this->value);
        }
    }

    //does this need to be overloaded?
    public function get()
    {
        return $this->value;
    }
}
