<?php

class AddressRetrieval
{
    //Credit: Thanks to Stuart Sillitoe (http://stu.so/me) for the original PHP that these samples are based on.

    private $Key; //The key to use to authenticate to the service.
    private $Id; //The Id from a Find method to retrieve the details for.
    private $Data; //Holds the results of the query

    public function __construct($Key, $Id)
    {
        $this->Key = $Key;
        $this->Id = $Id;
    }

    function MakeRequest()
    {
        $url = "https://api.addressy.com/Capture/Interactive/Retrieve/v1/json3.ws";

        $curl = curl_init();

        $data = [
            "Key" => $this->Key,
            "Id" => $this->Id,
        ];

        $url = sprintf("%s?%s", $url, http_build_query($data));

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl));

        if(empty($result->Error) && !empty($result->Items)){
            $this->Data = $result->Items;
        }
    }

    function HasData()
    {
        if (!empty($this->Data)) {
            return $this->Data;
        }
        return false;
    }
}
