<?php

/**
 * A means to represent a boolean type in our database.
 *
 *
 * This class allows for us to represent booleans in our application layer
 * There are many fields where we simply need a yes/no answer,
 * so simplify their logic and allow them special rights in here.
 *
 *
 * @package prophet.objects.types
 * @author daniel.ness
 *
 */

class Boolean extends Field
{

    const cross = "cross.png";
    const tick = "tick.png";
    var $value = false;
    private $yes = true;
    private $no = false;
    private $custom_values = false;

    /**
     * Overridden toElement method to output data on the page.
     *
     *
     * As a special type with boolean values, we can make the input
     * slightly fancier than most. Output a toggle button with images
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @param bool/string id
     * @param bool/string attributes
     * @param bool/string defaultvalue
     * @return String html
     */
    public function toElement($id = false, $attr = false, $value = false)
    {
        $id = $this->get_html_id($id);
        $this->db_object->register_field($this);
        return "<button type='button' id='$id' name='$id' class='bool'
                 value='" . intval($this()) . "' data-obj-id='" . $this->db_object->id() . "'>
                 &nbsp;
                 </button>";
    }


    /**
     * Allows us to set which values map to true/false.
     *
     * We have many cases where a value is true/false but we do not store a native
     * php bool. (Mainly legacy MS access stuff of -1/0) but some other freaky stuff like
     * o(yes thats correct, lowercase O)/ and ""
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param string/boolean $yes
     * @param string/boolean $no
     * @return object $this
     */
    public function set_values($yes, $no)
    {
        $this->yes = $yes;
        $this->no = $no;
        $this->custom_values = true;
        return $this;
    }

    /**
     * Overridden set method: we may need to use custom values.
     *
     *
     * Again boils down any value to a proper true/false boolean dependent on the custom values
     *
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param $value
     * @param bool $mark_as_edited
     * @return $this
     */
    public function set($value, $mark_as_edited = true)
    {

        parent::set($value, $mark_as_edited);

        if (is_bool($value) || $this->get_var(Boolean::TRUE_VALUE)) {
            $this->value = $value;
        } elseif ($this->custom_values && in_array($value, array($this->yes, $this->no))) {
            $this->value = ($value == $this->yes);
        }

        return $this;
    }


    /**
     * Overridden toString function - see long description
     *
     *
     * Instead of producing true/false strings, which arent very UI friendly,
     * we instead hand off to the toImage method
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @return toImage
     *
     */
    function __toString()
    {
        return $this->toImage();
    }


    /**
     * Produces an image to represent a boolean state
     *
     *
     * Instead of producing true/false strings, we output little ticks or crosses
     * to represent our boolean state
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @return String html
     *
     */
    public function toImage()
    {
        $img = ($this->value == $this->yes) ? Boolean::tick : (($this->get_var(Field::TRUE_VALUE) && (is_null($this->value) || $this->value === "")) ? Field::question : Boolean::cross);

        return "<img src=\"" . el::image($img) . "\" />";
    }


    /**
     * Overridden to sql method: correctly forms sql string for database manipulation.
     *
     *
     * When we boil down a boolean to true/false regardless of how the database is represented,
     * that creates a need to convert back to what the database understands when we send some sql
     * This will take the yes/no values and build a sql compatible string
     *
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     *
     * @param string value
     * @param bool wildcard
     * @param bool/string mode
     *
     * @return String sql
     *
     */
    public function to_sql($value = null, $wildcard = false, $mode = false)
    {

        ($value !== null) or $value = $this();

        if ($this->custom_values) {
            $value = ($value) ? $this->yes : $this->no;
        }

        return $this->get_field_name() . " = '$value'";
    }

    public static function represent($value)
    {
        $b = new Boolean();
        $b->value = boolval(strval($value));

        return $b;
    }
}
