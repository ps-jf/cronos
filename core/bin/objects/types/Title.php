<?php

class Title extends Choice
{
    var $name;

    public function __construct($db_name)
    {
        $x = array("Mr.", "Mrs.", "Ms.", "Miss.", "Dr.", "Sir.", "Lord.", "Lady.", "Prof.", "Rev.", "Captain.", "Rev. Dr.", "Major", "Canon", "Canon Dr.", "Mx.", "Father");
        foreach ($x as $title) {
            $this->push($title);
        }

        parent::__construct($db_name);
    }
}
