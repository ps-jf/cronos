<?php

class EmailValidation
{
    //Credit: Thanks to Stuart Sillitoe (http://stu.so/me) for the original PHP that these samples are based on.

    private $Key; //The key to use to authenticate to the service.
    private $Email; //The email address to verify.
    private $Timeout; //The time (in milliseconds) you want to give for the valiation attempt to be executed within. Value must be between 1 and 15000 (values outside of these ranges will fallback to the default of 15000).
    private $Data; //Holds the results of the query

    public function __construct($Key, $Email, $Timeout)
    {
        $this->Key = $Key;
        $this->Email = $Email;
        $this->Timeout = $Timeout;
    }

    function MakeRequest()
    {
        $url = "https://api.addressy.com/EmailValidation/Interactive/Validate/v2.00/json3.ws";

        $curl = curl_init();

        $data = [
            "Key" => $this->Key,
            "Email" => $this->Email,
            "Timeout" => $this->Timeout,
        ];

        $url = sprintf("%s?%s", $url, http_build_query($data));

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl));

        if(!empty($result->Items) && empty($result->Items[0]->Error)){
            $this->Data = $result->Items;
        } else {
            throw new Exception("An error occurred");
        }
    }

    function HasData()
    {
        if (!empty($this->Data)) {
            return $this->Data;
        }
        return false;
    }
}
