<?php

/**
 * Allows us to work with dates throughout the application in a uniform way.
 *
 *
 * We use unix behind the scenes for all dates, regardless of their output or database format
 * Utilising phps excellent built in date operations, we can treat all dates in a uniform way
 *
 *
 * @package prophet.objects.types
 * @author daniel.ness
 *
 */
class Date extends Field
{
    const FORMAT = "FORMAT";
    const DISPLAY_FORMAT = "DISPLAY_FORMAT";
    const UNIX = "UNIX";
    const UK_STYLE = "d/m/Y H:i:s";
    const UK_STYLE_SHORT = "d/m/Y";
    const UK_STYLE_FRIENDLY = "d/m/Y g:i a";
    const ISO_8601 = "Y-m-d H:i:s";
    const ISO_8601_SHORT = "Y-m-d";
    const HTML_DAY_ID = "D";
    const HTML_MON_ID = "M";
    const HTML_YEAR_ID = "Y";
    const HTML_HOUR_ID = "h";
    const HTML_MINS_ID = "m";
    const UK_DATE_STYLE_PATTERN = '/^([0-2]?[0-9]|3[0-1])\/(1[0-2]|0?[0-9])\/([1-2]\d{3})(([0-1]?[0-9]|2[0-3]):([0-5]?[0-9]?)(:([0-5]?[0-9])?)?)?$/'; // time
    const DAY_PATTERN = "(0?[1-9])|([1,2][0-9])|(3[0,1])";
    const MONTH_PATTERN = "(0?[1-9])|(1[0-2])";
    const YEAR_PATTERN = '([1,2]\d{3})';
    private $unix;

    /**
     * Standard constructor - see long description.
     *
     *
     * Calls parent, but we know that the type is of DATE and we (should) always want UK style
     * output on the page
     *
     * @package prophet.objects.constructors
     * @author daniel.ness
     *
     * @param bool $databaseName
     * @param bool $format
     * @param bool $date
     * @param bool $settings
     */
    public function __construct($databaseName = false, $format = false, $date = false, $settings = false)
    {
        parent::__construct($databaseName, $settings);

        $this->format = $format;

        $this->set_var(Field::TYPE, FIELD::DATE);

        $this->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        if ($date !== false) {
            // Convert initial value if supplied
            $this->toUnix($date);
        }
    }

    /**
     * Allows us to manipulate any date string into a unix timestamp for processing.
     *
     *
     * Tries to catch as many different datetime values as possible and convert them to a unix timestamp
     * If we know that something is in a specific format, we can obviously have less overhead throughout
     * the application, due to conversions at that point. No jit here.
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param bool $date
     * @param bool $format
     * @return bool|int
     */
    public function toUnix($date = false, $format = false)
    {
        /*
         * # Stores date as a unix timestamp
         * @param - $date - string/int - any format date
         */

        if ($this && $date === false) {
            $date = $this->value;
        }

        ($format !== false) or $this->get_var(Date::FORMAT);

        if (empty($date)) {
            $unix = false;
        } elseif ($format == Date::UNIX && is_numeric($date)) {
            // should already be a timestamp, simply cast to integer
            $unix = intval($date);
        } elseif (in_array($format, array(Date::UK_STYLE, Date::UK_STYLE_SHORT))) {
            // PHP's core date functions don't detect our superior date format,so manually convert it into timestamp

            $date_array = explode(" ", $date);
            $date_string = $date_array[0];
            $time_string = $date_array[1];

            $date_str_array = explode("/", $date_string);
            $time_str_array = explode(":", $time_string);

            list($day, $month, $year) = $date_str_array;
            list($hour, $mins, $secs) = (sizeof($time_str_array) > 1) ? $time_str_array : array(0, 0, 0);

            $unix = mktime($hour, $mins, $secs, $month, $day, $year);
        } elseif ($this->get_var(Date::FORMAT) !== null) {
            // Catch all other formats e.g. ISO8601
            $unix = strtotime($date);
        } else {
            trigger_error("Date instance " . get_class($this->db_object) . "->$this->name requires a Date::FORMAT variable", E_USER_ERROR);
        }

        if ($this) {
            $this->unix = $unix;
            $this->value = $unix;
        }

        return $unix;
    }

    /**
     * PHP magic invoke, if this class is called in a peculiar way, try and give the caller what it wants
     *
     *
     * Calls parent,if we know the value. Otherwise try and return the value
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @param null $value
     * @return bool|null|string
     */
    public function __invoke($value = null)
    {
        if ($value !== null) {
            parent::__invoke($value);
            return;
        } else {
            // Return Date's value in the declared I/O format
            return $this->get();
        }
    }

    /**
     * Outputs a date in its I/O format
     *
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @return bool|null|string
     */
    public function get()
    {
        $format = $this->get_var(Date::FORMAT);
        $unix = $this->value;

        if ($unix === false) {
            return null;
        }

        return ($format == Date::UNIX) ? $unix : date($format, $unix);
    }

    /**
     * Allows us to set the date from outside the class
     *
     *
     * If we want to set the date, we can do so here.
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param $value
     * @param bool $mark_as_edited
     * @param bool $format
     * @return $this
     */
    public function set($value, $mark_as_edited = true, $format = false)
    {
        ($format !== false) or $format = $this->get_var(Date::FORMAT);

        if (is_array($value) && is_assoc($value)) {
            // Arrange input array into ISO 8601 format
            $date = join("-", array($value[Date::HTML_YEAR_ID],
                $value[Date::HTML_MON_ID],
                $value[Date::HTML_DAY_ID]));
            $format = Date::ISO_8601_SHORT;

            // If available, append time fields
            if (array_key_exists(Date::HTML_HOUR_ID, $value) && array_key_exists(Date::HTML_MINS_ID, $value)) {
                $date .= " " . join(":", array($value[Date::HTML_HOUR_ID], $value[Date::HTML_MINS_ID]));
                $format = Date::ISO_8601;
            }

            $value = $date;
        }

        parent::set($value, $mark_as_edited);

        // disregard input format and convert value to unix timestamp
        $this->value = $this->toUnix($value, $format);

        return $this;
    }

    /**
     * Overridden toString function - see long description
     *
     *
     * As this is a date, we know living in the UK, that the user will expect a specific format
     * output on the page. Obviously if we want the date in another format, we can specify and
     * tostring will listen to that instead. Basically this makes a unix timestamp human-readable
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->get_var(Field::TRUE_VALUE) && empty($this->value)){
            return "<img src=\"" . el::image(Field::question) . "\"/>";
        }

        //// Output date string in it's specified Date::DISPLAY_FORMAT
        $v = ($this->unix) ? $this->unix : $this->value;
        return ($v) ? date($this->get_var(Date::DISPLAY_FORMAT), $v) : "";
    }

    /**
     * Overridden to sql method: correctly forms sql string for database manipulation.
     *
     *
     * Our sql here is complicated by the fact that we could be dealing with database datetime fields.
     * We need to format our sql in the best way that the database engine understands.
     *
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @param null $value
     * @param bool $wildcard
     * @return string
     */
    function to_sql($value = null, $wildcard = false, $mode = false)
    {
        if ($value == null) {
            $value = $this->get();
        }

        $normal_to_sql = parent::to_sql($value, $wildcard);

        // empty value (faster than regex)
        if (strpos($normal_to_sql, "\"\"") > -1 || $normal_to_sql == "0000-00-00") {
            return $this->get_field_name() . " = NULL";
        }

        return $normal_to_sql;
    }

    /**
     * Produces a series of input fields,related to date.
     *
     *
     * To aid UI, date input consists of 3 input boxes, one for day,month,year. We can
     * also include time fields if we wish.
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @param bool $id - key to use when retrieving from POST/GET
     * @param bool $implode_mode - return (true) ? string : array
     * @param bool $include_time - output time fields
     * @return array|string
     */
    public function toElement($id = false, $implode_mode = true, $include_time = false, $classes = false)
    {
        if ($id === false) {
            $id = $this->get_html_id();
        }

        $self = (get_class($this) == "Date") ? $this : new Date;

        // -62169984000 = unix for 0000-00-00
        if ($this->unix !== false && $this->unix !== null && $this->unix != -62169984000) {
            list($d, $m, $y, $hours, $mins) = explode("-", date("d-m-Y-G-i", $this->unix));
        } else {
            $d = $m = $y = $hours = $mins = null;
        }

        $elements = array(
            "day" => el::txt($id . "[" . Date::HTML_DAY_ID . "]", $d, "data-pattern='" . Date::DAY_PATTERN . "' class='" . $classes . " date day' maxlength='2' size='2'"),
            "month" => el::txt($id . "[" . Date::HTML_MON_ID . "]", $m, "data-pattern='" . Date::MONTH_PATTERN . "' class='" . $classes . " date month' maxlength='2' size='2'"),
            "year" => el::txt($id . "[" . Date::HTML_YEAR_ID . "]", $y, "data-pattern='" . Date::YEAR_PATTERN . "' class='" . $classes . " date year' maxlength='4' size='4'"),
            "hours" => ($include_time) ? el::dropdown($id . "[" . Date::HTML_HOUR_ID . "]", range(0, 23), $hours) : false,
            "mins" => ($include_time) ? el::dropdown($id . "[" . Date::HTML_MINS_ID . "]", range(0, 59), intval($mins)) : false
        );

        return ($implode_mode) ? implode(" ", $elements) : $elements;
    }

    /**
     * Returns years elapsed since date (hint: age)
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @return bool/float
     */
    public function years()
    {
        if (!$date = $this->value) {
            return false;
        }

        $date = date('Y-m-d', $date);
        list($y, $m, $d) = explode("-", $date);

        $now = date('Y-m-d');
        list($nowY, $nowM, $nowD) = explode("-", $now);

        $yDiff = $nowY - $y;

        $age = (($nowM < $m) || ($nowM == $m && $nowD < $d))
            ? --$yDiff
            : $yDiff;

        return $age;
    }

    /**
     * Format as specified and return date
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param bool/string $format
     * @return bool|string
     */
    public function out($format = false)
    {
        // return raw timestamp
        if (!$format) {
            return $this->unix;
        }

        // ensure unix timestamp is valid
        if ($this->unix) {
            return date($format, $this->unix);
        } // return nothing
        else {
            return false;
        }
    }

    /**
     * Returns a friendly comparison of two times
     *
     *
     * i.e.
     *     - 3 minutes ago
     *     - 4 hours ago
     *     - 3 days ago
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param int $time
     * @param string $format
     * @return string
     */
    public function comparison($time = 0, $format = "d/m/Y g:ia")
    {
        $time_diff = time() - $time;

        if ($time_diff < (60 * 60)) { // less than an hour ago
            $mins_ago = $time_diff / 60;

            // minutes or seconds?
            $date = ($mins_ago > 1)
                ? floor($mins_ago) . " minutes ago"
                : $time_diff . " seconds ago";
        } elseif ($time_diff < (24 * 60 * 60)) { // less than a day
            $hours_ago = floor($time_diff / (60 * 60));
            $date = $hours_ago . " hours ago";
        } else {
            $date = date($format, $time);
        }

        return "<span title=\"" . date('d/m/Y h:ia') . "\">$date</span>";
    }

    public static function valid_date($d = 1, $m = 1, $y = 1999){
        //check for valid date
        $dateArray = array($d, $m, $y);

        $emptyFields = 0;

        //checks that the date is either blank or fully filled
        foreach ($dateArray as $dateItem){
            if (empty($dateItem)){
                $emptyFields++;
            }
        }

        if ($emptyFields == 0){
            //fully filled
            if ($d >= 1 && $d <= 31 && $m >= 1 && $m <= 12 && $y >= 1900 && $y <= date("Y")){
                return true;
            }
        } elseif ($emptyFields == 3){
            //blank date (allowed as not all dates are required fields
            return true;
        } else {
            return false;
        }
    }
}
