<?php

/**
 * This class is all about giving the users a choice over various different values
 * (Think <select>).
 *
 *
 * If we want to restrict the users choice of values, then this is the place to be.
 *
 *
 * @package prophet.objects.types
 * @author daniel.ness
 *
 */
class Choice extends Field
{
    private $choices = [];
    private $source = false;
    private $text = false;

    /**
     * Prepare the sql needed to get the choices from the databases.
     *
     *
     * Normally choices will be kept in a database table
     * Here we can attempt to define the SQL that we need to extract the correct choices
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param string $source - table name/query
     * @param string/int $key - field name/index for value
     * @param string/int $value - field name/index for textual rep
     * @return $this
     */
    public function setSource($source, $key = 0, $value = 1)
    {
        //passed an database object
        if (is_a($source, 'DatabaseObject')) {
            die("Choice::setSource(DbObject); deprecated for Sub");
            // pass object's table name to elseif below
            $source = $source->get_table();
        }

        if ($source && $key && $value) {
            $this->source = "select $key, $value from $source order by $value";
        } //Deriving choices from query
        elseif (preg_match('/^select `?(.*?)`?, ?`?(.*?)`? from `?(\w+\.\w+)`?/i', $source, $m)) {
            $this->source = $source;
        } //Passed a database table name
        elseif (preg_match('/^(\w+\.\w+)$/', $source, $m)) {

            $db = new mydb();

            $db->query("DESCRIBE $m[1]");

            if (is_int($key) || is_int($value)) {
                $i = 0;
                while ($field = $db->next(MYSQLI_ASSOC)) {
                    // index match
                    if (is_int($key) && $i == $key) {
                        $key = $field['Field'];
                    }

                    if (is_int($value) && $i == $value) {
                        $value = $field['Field'];
                    }

                    ++$i;
                }
            }

            $this->source = "select $key,$value from $m[1] order by $value";
        }
        return $this;
    }

    /**
     * Manually push a choice into the list of available choices.
     *
     *
     * Allows us to add choices by hand, useful for things like spacers and
     * where SQL would be an unviable choice(values aren't in a database table)
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param $id
     * @param bool $value
     * @return $this
     */
    public function push($id, $value = false)
    {
        // add a choice
        $this->choices[$id] = ($value !== false) ? $value : $id;
        return $this;
    }

    /**
     * Manually push an array of choice into the list of available choices.
     *
     *
     * Allows us to add choices by hand, all in the one operation, if we already have an array
     * of choices. Similar to push, but for an array(funnily enough!)
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param $array
     * @return $this
     */
    public function push_array($array)
    {
        // add multiple choices
        $this->choices = array_merge($this->choices, $array);
        return $this;
    }

    /**
     * Overridden set method: see long description.
     *
     *
     * The use of internal variables means that we need to map them to the standard FIELD
     * vars. Also the type can switch depending on the type of data, so set that here
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param $value
     * @param bool $mark_as_edited
     * @return $this
     */
    public function set($value, $mark_as_edited = true)
    {
        $this->type = (is_int($value)) ? FIELD::INTEGER : FIELD::STRING;
        $this->text = false;
        $this->value = $value;
        $this->is_set = true;
        $this->edited = $mark_as_edited;
        return $this;
    }

    /**
     * Overridden toElement:Return a drop down box with the available options. Preselect the current value.
     *
     *
     * With a choice we will always return a <select> html element because of its behaviour
     * Here, we will actually output the html source built from this objects choices.
     *
     *
     * @param bool $id
     * @param bool $attr
     * @param bool $value
     * @param bool $excludes
     * @param bool $connection
     * @param bool $exclude_select
     * @param bool $exclude_spacer
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     */
    public function toElement($id = false, $attr = false, $value = false, $excludes = false, $connection = false, $exclude_select = false, $exclude_spacer = false)
    {
        if ($id === false) {
            $id = $this->name;
        }

        $id = $this->get_html_id($id);
        $this->db_object->register($this);

        if ($value === false) {
            $value = $this();
        }

        return el::dropdown($id, $this->get_choices($excludes, $connection), $value, $attr, $exclude_select, $exclude_spacer);
    }

    /**
     * get_choices returns the actual choices that are available for a particular choice.
     *
     *
     * At this stage we will actually hit the database in order to get the choices keys and values
     * We can also use this to exclude any values that we do not want to appear
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param bool/array $excludes
     * @return array
     */
    public function get_choices($excludes = false, $connection = false)
    {

        //if we are not excluding anything turn it into an empty array
        if ($excludes == false) {
            $excludes = [];
        }

        if ($this->source) {
            $tmp = $this->choices;
            $this->choices = [];

            if ($connection) {
                $db = new mydb(REMOTE);
            } else {
                $db = new mydb();
            }

            $db->query($this->source);

            while ($d = $db->next(MYSQLI_NUM)) {
                if (!array_search($d[0], $excludes)) {
                    $this->choices[$d[0]] = $d[1];
                }
            }

            foreach ($tmp as $k => $v) {
                $this->choices[$k] = $v;
            }
        }

        if (!empty($excludes)) {
            $tmp2 = array_diff($this->choices, $excludes);
            return $tmp2;
        }

        return $this->choices;
    }

    /**
     * Overridden toString: see long description
     *
     *
     * UI-wise output will be changed to the text rather than the value of the choice.
     *
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->text === false) {
            $this->get_text();
        }

        if ($this->get_var(Field::TRUE_VALUE) && empty($this->value)){
            return "<img src=\"" . el::image(Field::question) . "\"/>";
        }

        return "$this->text";
    }

    /**
     * Gets the textual representation of a choice from the correct source.
     *
     *
     * In a choice we will normally have a key -> value relationship
     * Obviously in the application it makes sense to use the key as this will normally be an integer
     * but from a UI perspective, we should be using the value. This function will get that for us.
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @return string text
     */
    public function get_text()
    {
        if (is_array($this->value)) {
            return false;
        }

        // check in stored choices
        if (!empty($this->value) && isset($this->choices[$this->value])) {
            $this->text = $this->choices[$this->value];
        } else {
            // take from database
            if ($this->value !== false && preg_match('/^(select (.*?),.*? from \w+\.\w+)/i', $this->source, $m)) {
                $query = $m[1] . " WHERE $m[2] = '" . $this->value . "'";

                $db = new mydb($this->db_object->database);
                $db->query($query);

                if ($d = $db->next(MYSQLI_NUM)) {
                    $this->text = $d[1];
                    $this->choices[$d[0]] = $d[1];
                }
            }
        }

        return $this->text;
    }

    /**
     * Overridden get: see long description
     *
     *
     * If we have the value of the choice, then we should return it
     * Otherwise we can fallback to the toString magic method
     *
     *
     * @package prophet.objects.magics
     * @author daniel.ness
     *
     * @return bool|string
     */
    function get()
    {
        return ($this->value !== false) ? $this->value : "$this";
    }


    /**
     * Overridden to_sql: see long description
     *
     *
     * Here we simply make a call to the parents method, but first we must check
     * that we have access to the value, otherwise the sql generated would be garbage.
     *
     *
     * @package prophet.objects.helpers
     * @author daniel.ness
     *
     * @param bool/mixed $value
     * @param bool $wildcard
     * @return mixed
     */
    function to_sql($value = false, $wildcard = false, $mode = false)
    {
        $value = ($value === false) ? $this->value : $value;
        return call_user_func_array(
            array("parent", "to_sql"),
            array((($value === false) ? $this->value : $value), $wildcard)
        );
    }
}
