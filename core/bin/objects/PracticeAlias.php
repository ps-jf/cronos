<?php

class PracticeAlias extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "practice_alias";
    var $_profilePath = "/pages/practice/";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->alias = Field::factory("alias");

        $this->practice_id = Field::factory("practice_id");

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->name";
    }
}
