<?php

class NewBusinessChecklist extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "newbus_checklist";

    // This is just the beginning for the newbusiness checklist

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->nbid = Sub::factory("NewBusiness", "nbID");

        $this->s1q1 = Boolean::factory("s1q1")
            ->set_var(Field::DISPLAY_NAME, "Ensure CFR, ID and Client Agreement have been recieved, signed and dated")
            ->set_values(1, 0);

        $this->s1q1_addedby = Sub::factory("User", "s1q1_addedby");

        $this->s1q1_addedwhen = Date::factory("s1q1_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q2 = Boolean::factory("s1q2")
            ->set_var(Field::DISPLAY_NAME, "Check client and relevant policy is on the system and if applicable request mandates via the partner")
            ->set_values(1, 0);

        $this->s1q2_addedby = Sub::factory("User", "s1q2_addedby");

        $this->s1q2_addedwhen = Date::factory("s1q2_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s1q3 = Boolean::factory("s1q3")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Refer the case to sarah to check validity");

        $this->s1q3_addedby = Sub::factory("User", "s1q3_addedby");


        $this->s1q3_addedwhen = Date::factory("s1q3_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q4 = Boolean::factory("s1q4")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Add TBC policy(if relevant)");

        $this->s1q4_addedby = Sub::factory("User", "s1q4_addedby");

        $this->s1q4_addedwhen = Date::factory("s1q4_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q5 = Boolean::factory("s1q5")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Add 'Advice - Limited' ticket to TBC or relevant policy (checker should be paraplanner)"
            );

        $this->s1q5_addedby = Sub::factory("User", "s1q5_addedby");

        $this->s1q5_addedwhen = Date::factory("s1q5_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q6 = Boolean::factory("s1q6")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Generate initial letter and collate with ATR and T&Cs - scan as Original Sent to Client");

        $this->s1q6_addedby = Sub::factory("User", "s1q6_addedby");

        $this->s1q6_addedwhen = Date::factory("s1q6_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q7 = Boolean::factory("s1q7")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Add case to spreadsheet(ensuring all relevant fields completed)");

        $this->s1q7_addedby = Sub::factory("User", "s1q7_addedby");

        $this->s1q7_addedwhen = Date::factory("s1q7_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q8 = Boolean::factory("s1q8")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Generate PTQ if required and forward to provider - Scan as Original Sent to Company"
            );

        $this->s1q8_addedby = Sub::factory("User", "s1q8_addedby");

        $this->s1q8_addedwhen = Date::factory("s1q8_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q9 = Boolean::factory("s1q9")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Print out ceding scheme info if applicable");

        $this->s1q9_addedby = Sub::factory("User", "s1q9_addedby");

        $this->s1q9_addedwhen = Date::factory("s1q9_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q10 = Boolean::factory("s1q10")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Add client folder to Ongoing Cases in Advice Admin");

        $this->s1q10_addedby = Sub::factory("User", "s1q10_addedby");

        $this->s1q10_addedwhen = Date::factory("s1q10_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q11 = Boolean::factory("s1q11")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Obtain state pension age confirmation and add to folder");

        $this->s1q11_addedby = Sub::factory("User", "s1q11_addedby");

        $this->s1q11_addedwhen = Date::factory("s1q11_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q12 = Boolean::factory("s1q12")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "ID Client and complete Risk Assessment - ask adviser to sign");


        $this->s1q12_addedby = Sub::factory("User", "s1q12_addedby");

        $this->s1q12_addedwhen = Date::factory("s1q12_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s1q13 = Boolean::factory("s1q13")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Scan any recieved documents into the folder i.e. CFR, Client Agreement, ID"
            );

        $this->s1q13_addedby = Sub::factory("User", "s1q13_addedby");

        $this->s1q13_addedwhen = Date::factory("s1q13_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q1 = Boolean::factory("s2q1")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Prepare for call - read through CFR and prepare initial addendum"
            );

        $this->s2q1_addedby = Sub::factory("User", "s2q1_addedby");

        $this->s2q1_addedwhen = Date::factory("s2q1_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q2 = Boolean::factory("s2q2")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Risk profile existing funds - save report to advice file "
            );

        $this->s2q2_addedby = Sub::factory("User", "s2q2_addedby");

        $this->s2q2_addedwhen = Date::factory("s2q2_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q3 = Boolean::factory("s2q3")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Check current ongoing charge on policy(if applicable) to discuss with the client during the call"
            );

        $this->s2q3_addedby = Sub::factory("User", "s2q3_addedby");

        $this->s2q3_addedwhen = Date::factory("s2q3_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q4 = Boolean::factory("s2q4")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Print Call Record sheet to be completed during the call"
            );

        $this->s2q4_addedby = Sub::factory("User", "s2q4_addedby");

        $this->s2q4_addedwhen = Date::factory("s2q4_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q5 = Boolean::factory("s2q5")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Updated addendum (including notes on initial/ongoing charges and details of recommendation)"
            );

        $this->s2q5_addedby = Sub::factory("User", "s2q5_addedby");

        $this->s2q5_addedwhen = Date::factory("s2q5_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q6 = Boolean::factory("s2q6")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Download call file from system and save into case folder in advice admin"
            );

        $this->s2q6_addedby = Sub::factory("User", "s2q6_addedby");

        $this->s2q6_addedwhen = Date::factory("s2q6_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q7 = Boolean::factory("s2q7")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Complete Risk Profile report and confirm results with client (save copy to advice folder and print)"
            );

        $this->s2q7_addedby = Sub::factory("User", "s2q7_addedby");

        $this->s2q7_addedwhen = Date::factory("s2q7_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q8 = Boolean::factory("s2q8")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Update the partner as well as prophet and the spreadsheet as required"
            );

        $this->s2q8_addedby = Sub::factory("User", "s2q8_addedby");

        $this->s2q8_addedwhen = Date::factory("s2q8_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s2q9 = Boolean::factory("s2q9")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Send letter to client confirming the charges"
            );

        $this->s2q9_addedby = Sub::factory("User", "s2q9_addedby");

        $this->s2q9_addedwhen = Date::factory("s2q9_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s3q1 = Boolean::factory("s3q1")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Check information has been recieved from ceding provider"
            );

        $this->s3q1_addedby = Sub::factory("User", "s3q1_addedby");

        $this->s3q1_addedwhen = Date::factory("s3q1_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q2 = Boolean::factory("s3q2")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Print Retirement Options Booklet(if relevant) and save a copy into the Advice folder"
            );

        $this->s3q2_addedby = Sub::factory("User", "s3q2_addedby");

        $this->s3q2_addedwhen = Date::factory("s3q2_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q3 = Boolean::factory("s3q3")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Complete Assureweb quotes(save Comparison table, illustration and Key Features to advice folder)"
            );

        $this->s3q3_addedby = Sub::factory("User", "s3q3_addedby");

        $this->s3q3_addedwhen = Date::factory("s3q3_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q4 = Boolean::factory("s3q4")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Complete synaptics - Save comparison report and specific provider report to advice folder"
            );

        $this->s3q4_addedby = Sub::factory("User", "s3q4_addedby");

        $this->s3q4_addedwhen = Date::factory("s3q4_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q5 = Boolean::factory("s3q5")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Confirm clients chosen risk profile with adviser"
            );

        $this->s3q5_addedby = Sub::factory("User", "s3q5_addedby");

        $this->s3q5_addedwhen = Date::factory("s3q5_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q6 = Boolean::factory("s3q6")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Discuss potential funds with adviser
                 before collating relevant information and completing research on FE Analytics "
            );

        $this->s3q6_addedby = Sub::factory("User", "s3q6_addedby");

        $this->s3q6_addedwhen = Date::factory("s3q6_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q7 = Boolean::factory("s3q7")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Check funds are in line with chosen risk profile"
            );

        $this->s3q7_addedby = Sub::factory("User", "s3q7_addedby");

        $this->s3q7_addedwhen = Date::factory("s3q7_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q8 = Boolean::factory("s3q8")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Generate Performance Table, Ratio Table and Short/Medium/Long term scatter chart"
            );

        $this->s3q8_addedby = Sub::factory("User", "s3q8_addedby");

        $this->s3q8_addedwhen = Date::factory("s3q8_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q9 = Boolean::factory("s3q9")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Confirm with partner and client that we are working on case & expected turnaround"
            );

        $this->s3q9_addedby = Sub::factory("User", "s3q9_addedby");

        $this->s3q9_addedwhen = Date::factory("s3q9_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q10 = Boolean::factory("s3q10")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Collate Key Features & Fund  Factsheets/KIIDs ( save to Advice folder and print"
            );

        $this->s3q10_addedby = Sub::factory("User", "s3q10_addedby");

        $this->s3q10_addedwhen = Date::factory("s3q10_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q11 = Boolean::factory("s3q11")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Complete Fund Charges Spreadsheet"
            );

        $this->s3q11_addedby = Sub::factory("User", "s3q11_addedby");

        $this->s3q11_addedwhen = Date::factory("s3q11_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q12 = Boolean::factory("s3q12")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Request relevant application process/forms from the provider"
            );

        $this->s3q12_addedby = Sub::factory("User", "s3q12_addedby");

        $this->s3q12_addedwhen = Date::factory("s3q12_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q13 = Boolean::factory("s3q13")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Download relevant report template from Compliance First docs (save copy to Advice Folder)"
            );

        $this->s3q13_addedby = Sub::factory("User", "s3q13_addedby");

        $this->s3q13_addedwhen = Date::factory("s3q13_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q14 = Boolean::factory("s3q14")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Generate report (save copy to Advice Folder)"
            );

        $this->s3q14_addedby = Sub::factory("User", "s3q14_addedby");

        $this->s3q14_addedwhen = Date::factory("s3q14_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s3q15 = Boolean::factory("s3q15")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Update Addendum to match report and adding relevant sections re:objectives"
            );

        $this->s3q15_addedby = Sub::factory("User", "s3q15_addedby");

        $this->s3q15_addedwhen = Date::factory("s3q15_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s3q16 = Boolean::factory("s3q16")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Sort all documents in Advice Folder into Sections 1-5(as per Client contact sheet"
            );

        $this->s3q16_addedby = Sub::factory("User", "s3q16_addedby");

        $this->s3q16_addedwhen = Date::factory("s3q16_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s3q17 = Boolean::factory("s3q17")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Complete any relevant file notes as required"
            );

        $this->s3q17_addedby = Sub::factory("User", "s3q17_addedby");

        $this->s3q17_addedwhen = Date::factory("s3q17_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s3q18 = Boolean::factory("s3q18")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "If applicable, print sign off sheet and have this signed by Diane"
            );

        $this->s3q18_addedby = Sub::factory("User", "s3q18_addedby");

        $this->s3q18_addedwhen = Date::factory("s3q18_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s3q19 = Boolean::factory("s3q19")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Obtain an illustration from the provider"
            );

        $this->s3q19_addedby = Sub::factory("User", "s3q19_addedby");

        $this->s3q19_addedwhen = Date::factory("s3q19_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s4q1 = Boolean::factory("s4q1")
            ->set_values(1, 0)
            ->set_var(Field::DISPLAY_NAME, "Print out checklist");

        $this->s4q1_addedby = Sub::factory("User", "s4q1_addedby");

        $this->s4q1_addedwhen = Date::factory("s4q1_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s4q2 = Boolean::factory("s4q2")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Read report and addendum then check against each other"
            );

        $this->s4q2_addedby = Sub::factory("User", "s4q2_addedby");

        $this->s4q2_addedwhen = Date::factory("s4q2_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s4q3 = Boolean::factory("s4q3")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Check each section folder against the checklist"
            );

        $this->s4q3_addedby = Sub::factory("User", "s4q3_addedby");

        $this->s4q3_addedwhen = Date::factory("s4q3_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s4q4 = Boolean::factory("s4q4")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Ensure file is compliant"
            );

        $this->s4q4_addedby = Sub::factory("User", "s4q4_addedby");

        $this->s4q4_addedwhen = Date::factory("s4q4_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s4q5 = Boolean::factory("s4q5")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Print out SL and final addendum"
            );

        $this->s4q5_addedby = Sub::factory("User", "s4q5_addedby");

        $this->s4q5_addedwhen = Date::factory("s4q5_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s4q6 = Boolean::factory("s4q6")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Call the client to confirm recommendation and that report is being sent"
            );

        $this->s4q6_addedby = Sub::factory("User", "s4q6_addedby");

        $this->s4q6_addedwhen = Date::factory("s4q6_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s5q1 = Boolean::factory("s5q1")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Collate relevant documents"
            );

        $this->s5q1_addedby = Sub::factory("User", "s5q1_addedby");

        $this->s5q1_addedwhen = Date::factory("s5q1_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s5q2 = Boolean::factory("s5q2")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Have report signed"
            );

        $this->s5q2_addedby = Sub::factory("User", "s5q2_addedby");

        $this->s5q2_addedwhen = Date::factory("s5q2_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s5q3 = Boolean::factory("s5q3")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Send via Recorded Delivery to client"
            );

        $this->s5q3_addedby = Sub::factory("User", "s5q3_addedby");

        $this->s5q3_addedwhen = Date::factory("s5q3_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s5q4 = Boolean::factory("s5q4")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Update fields on NB ticket"
            );

        $this->s5q4_addedby = Sub::factory("User", "s5q4_addedby");

        $this->s5q4_addedwhen = Date::factory("s5q4_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s5q5 = Boolean::factory("s5q5")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Update partner to confirm the report is being issued"
            );

        $this->s5q5_addedby = Sub::factory("User", "s5q5_addedby");

        $this->s5q5_addedwhen = Date::factory("s5q5_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s6q1 = Boolean::factory("s6q1")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Ensure signed SL letter and relevant application forms are recieved back"
            );

        $this->s6q1_addedby = Sub::factory("User", "s6q1_addedby");

        $this->s6q1_addedwhen = Date::factory("s6q1_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s6q2 = Boolean::factory("s6q2")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Confirm with client and partner upon receipt"
            );

        $this->s6q2_addedby = Sub::factory("User", "s6q2_addedby");

        $this->s6q2_addedwhen = Date::factory("s6q2_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s6q3 = Boolean::factory("s6q3")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Forward any relevant paperwork to provider/key online and fill in relevant fields in ticket"
            );

        $this->s6q3_addedby = Sub::factory("User", "s6q3_addedby");

        $this->s6q3_addedwhen = Date::factory("s6q3_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s6q4 = Boolean::factory("s6q4")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Add diary entry to chase 5 working days after submission"
            );

        $this->s6q4_addedby = Sub::factory("User", "s6q4_addedby");

        $this->s6q4_addedwhen = Date::factory("s6q4_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);


        $this->s6q5 = Boolean::factory("s6q5")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Chase and confirm update with client and partner - repeat if necessary"
            );

        $this->s6q5_addedby = Sub::factory("User", "s6q5_addedby");

        $this->s6q5_addedwhen = Date::factory("s6q5_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s6q6 = Boolean::factory("s6q6")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Ensure application is completed correctly and confirm with client
                 - upon completion fill in relevant fields on NB ticket"
            );

        $this->s6q6_addedby = Sub::factory("User", "s6q6_addedby");

        $this->s6q6_addedwhen = Date::factory("s6q6_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s6q7 = Boolean::factory("s6q7")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Where applicable, ask finance to issue an invoice to the client"
            );

        $this->s6q7_addedby = Sub::factory("User", "s6q7_addedby");

        $this->s6q7_addedwhen = Date::factory("s6q7_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->s6q8 = Boolean::factory("s6q8")
            ->set_values(1, 0)
            ->set_var(
                Field::DISPLAY_NAME,
                "Ensure remuneration is received on the back of the advice"
            );

        $this->s6q8_addedby = Sub::factory("User", "s6q8_addedby");

        $this->s6q8_addedwhen = Date::factory("s6q8_addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->CAN_DELETE = true;

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->category";
    }
}
