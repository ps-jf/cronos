<?php

use Carbon\Carbon;

class Client extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblclient";

    var $NAME_ORDER = ["one->surname", "one->forename", "two->surname", "two->forename"];

    public function __construct()
    {
        $this->SNAPSHOT_LOG = true;

        $this->id = Field::factory("clientID", Field::PRIMARY_KEY);

        $this->one = SingleClient::factory(1);

        $this->one->surname->set_var(Field::REQUIRED, true);

        $this->two = SingleClient::factory(2);

        $this->group_scheme = Boolean::factory("group_scheme")
            ->set_values(1, 0)
            ->set(0);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->address = Address::factory(
            "clientaddress1",
            "clientaddress2",
            "clientaddress3",
            "clientaddresspostcode"
        );

        $this->phone = Field::factory("clienttelno")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->mob_phone = Field::factory("clientmobno")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->default_num = Choice::factory("default_num")
            ->push("1", "Home")
            ->push("2", "Mobile")
            ->set_var(Field::DISPLAY_NAME, "default_num")
            ->set(1);

        $this->bulk = Boolean::factory("BulkClient")
            ->set_values("x", "");

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedWhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "addedBy")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        // 1:Prophet 2:Mandate 3:Commission 4:Import 5: Bulk system 6:Client Split
        $this->addedhow = Field::factory('added_how')
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(1);

        $this->servicing_level = Choice::factory("servicing_level")
            ->push_array(array("1", "3", "6", "12", "18", "24", "36", "48", "L", "T"));
        //->push_array( array(1=>"Monthly",2=>"Bimonthly",3=>"Quarterly",4=>"Biannually",5=>"Annually",6=>"Biennially") );

        $this->last_review_date = Date::factory("last_review_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->next_review_date = Date::factory("next_review_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->csrd = Boolean::factory("csrd");

        $this->salutation = Field::factory("salutation");

        $this->csrd_excluded = Choice::factory("csrd_excluded")
            ->push("1", "Admin Client")
            ->push("2", "Family/Self")
            ->push("3", "Written to 2012")
            ->push("4", "Written to")
            ->push("5", "Marked for removal")
            ->push("6", "Policy Status")
            ->push("7", "Policy Type")
            ->push("8", "Zero Policies");

        $this->archived = Date::factory("archived")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->intelligent_office = Choice::factory("intelligent_office")
            ->push("0", "Not on IO")
            ->push("1", "Added to IO")
            ->push("-1", "Pending");

        $this->pv_mandatory_start = Date::factory("pv_mandatory_start")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->pv_comp = Boolean::factory("pv_comp");

        $this->pre_rdr = Boolean::factory("pre_rdr");

        $this->pure_protection = Boolean::factory("pure_protection");

        $this->welcome_pack = Date::factory("welcome_pack")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->complaint_ongoing = Boolean::factory("complaint_ongoing");

        $this->import_id = Field::factory("tempclientidforimport");

        $this->scheme_name = Field::factory("scheme_name");

        $this->joint_consent = Boolean::factory("joint_consent")
            ->set(0);


        /**
         *
         * IMPORTANT: UPDATE DEDUPECLIENT TABLE & OBJECT WHEN ADDING NEW FIELDS TO TBLPOLICY SO WE HAVE A FULL AUDIT
         *
         */

        $this->CAN_DELETE = true;

        call_user_func_array(array("parent", "__construct"), func_get_args());
    }

    public function save($return_query = false)
    {
        if ($this->group_scheme()){
            $this->one->surname->set_var(Field::REQUIRED, false);
            $this->scheme_name->set_var(Field::REQUIRED, true);
        }

        if(!empty($this->address->postcode())){
            foreach(["one", "two"] as $i){
                if(empty($this->$i->residency())){
                    // client does not have a residency set
                    if(preg_match(Address::UK_POSTCODE_REGEX, $this->address->postcode())){
                        // this looks like a UK postcode, update the clients residency
                        $this->$i->residency("GBR");
                    }
                }
            }
        }

        return parent::save($return_query);
    }

    public static function merge_requested($client)
    {
        $merge_request = false;

        // outstanding merges on this client?
        $s = new Search(new DataChange());
        $s->eq("object_type", "Client");
        $s->eq("comments", DataChange::COMMENT_CLIENT_MERGE);
        $s->eq("object_id", $client);
        $s->eq_null("committed_by");
        $s->order_by(array("id", "DESC"));

        while ($c = $s->next(MYSQLI_ASSOC)) {
            $db = new mydb(REMOTE, E_USER_WARNING);

            if (mysqli_connect_errno()) {
                throw new Exception("Can't connect to the mypsaccount server: " . mysqli_connect_errno());
            }
            $q = "SELECT * FROM " . REMOTE_SYS_DB . ".user_info where id = " . $c->proposed_by();
            $db->query($q);

            $user_name = "";
            while ($row = $db->next(MYSQLI_ASSOC)) {
                $user_name = $row['user_name'];
            }

            if ($user_name != "") {
                $by = "by " . ucwords(str_replace(".", " ", $user_name)) . ".";
            } else {
                $by = ".";
            }

            if ($c->committed_by() == "" && $c->committed_time() == "") {
                $merge_request = "<div class='notification static' style='display:block;'>
            This client was marked for merging on " . date("d/m/Y H:i:s", $c->proposed_time()) . "
            " . $by . " These changes are pending and we try to approve and apply them within 5 days.</div>";
            }
        }

        return $merge_request;
    }

    public static function mandatory_portfolio_report($client){
        if ($client->pv_mandatory_start() != null){
            $carbon = Carbon::createFromTimestamp($client->pv_mandatory_start());
            $reportDue = $carbon->addYears(1);
            $days = $reportDue->diffInDays();

            $report_span = "<div class='notification static' style='display: block;'>This client requires a mandatory portfolio report within the next <strong>".$days." days</strong>.</div>";

            return $report_span;
        }
    }

    public function servicing_due_by()
    {
        $client_review_next = $nextReviewString = $ca_diff = false;

        $s = new Search(new ServicingPropositionLevel());
        $s->eq("client_id", $this->id());

        if($spl = $s->next(MYSQLI_ASSOC)){
            if(!in_array($spl->level(), ['T', 'P'], true) && $this->reviewCountdownString()){
                $nextReviewString = 'Client Review - '.$this->reviewCountdownString();
            }
        } elseif (!empty($this->welcome_pack())){
            $now = Carbon::now('Europe/London');

            $wp_sent = Carbon::createFromTimestamp($this->welcome_pack());

            if ($this->partner->client_income_hold_days()) {
                $ca_due = $wp_sent->copy()->addDays($this->partner->client_income_hold_days());
            } else {
                $ca_due = $wp_sent->copy()->addDays(90);
            }

            if ($now->diffInDays($ca_due) > 1) {
                $days = 'days';
            } else {
                $days = 'day';
            }

            if ($ca_due > $now) {
                $ca_diff = 'is due in the next <strong>'.$now->diffInDays($ca_due).' '.$days.'</strong>';
            } else {
                $ca_diff = 'was due <strong>'.$now->diffInDays($ca_due).' '.$days.' ago</strong>';
            }
        }

        if(!empty($ca_diff) && $this->get_fees()){
            $string = "This client has not yet signed their client agreement. The client agreement " . $ca_diff;
        } elseif (!empty($nextReviewString) && !$this->pure_protection()) {
            $string = $nextReviewString;
        }

        if(!empty($string)){
            return "<div class='alert alert-info mb-1'>
            <i class='fas fa-info-circle'></i> " . $string . "
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                <span aria-hidden=\"true\">&times;</span>
            </button>
            </div>";
        }

        return false;
    }

    public function reviewCountdownString()
    {
        $nextReviewString = '';
        $review = $this->reviewCountdown();

        if ($review) {
            $arranged = $review['arranged'];
            $arranged_date = $review['arranged_date'];
            $nextReview = $review['next'];
            $initial_date_needed = $review['initial'];
            $declaration_needed = $review['declaration_needed'];

            (!empty($review['covid_extension'])) ?
                $icon = "<i title='This review has been given a 3 month extension due to conditions put in place by COVID-19' class='fa fa-info-circle fa-margin-right'></i>" :
                $icon = '';

            (!empty($review['custom_extension'])) ?
                $icon = "<i title='".$review['custom_extension']."' class='fa fa-info-circle fa-margin-right'></i>" :
                $icon = '';

            if ($initial_date_needed) {
                $title_text = $colour_class = '';
                if ($nextReview <= Carbon::now()) {
                    $colour_class = 'text-danger';
                    $title_text = '- Overdue';
                } elseif ($nextReview <= Carbon::now()->addMonths(2)) {
                    $colour_class = 'text-warning';
                    $title_text = 'within next 2 months';
                }

                // level 1 client, date needed
                $nextReviewString = "<span class='duedate ".$colour_class."' title='Review Date Needed ".$title_text."' data-dueby='".$nextReview->toFormattedDateString()."'>".$icon." Review date needed</span>";
            } elseif ($arranged) {
                if ($declaration_needed) {
                    if (isset($review['active_os']) && is_null($review['active_os']->review_done())) {
                        $nextReviewString = "<span class='duedate text-danger' title='Action Needed' data-arranged='".$arranged_date->toFormattedDateString()."' data-dueby='".$nextReview->toFormattedDateString()."'>Action Needed</span>";
                    } else if (isset($review['active_os']) && $review['active_os']->review_done() === 1) {
                        if ($review['active_os']->val_req_id()) {
                            //date was set by partner
                            $nextReviewString = "<span class='duedate text-danger' title='Confirmation Needed' data-arranged='".$arranged_date->toFormattedDateString()."' data-dueby='".$nextReview->toFormattedDateString()."'>Confirmation Needed</span>";
                        } else {
                            //date was set by partner
                            $nextReviewString = "<span class='duedate text-danger' title='Declaration Needed' data-arranged='".$arranged_date->toFormattedDateString()."' data-dueby='".$nextReview->toFormattedDateString()."'>Declaration Needed</span>";
                        }
                    } else if (isset($review['active_os']) && $review['active_os']->review_done === 0) {
                        $nextReviewString = "<span class='duedate text-danger' title='Action Needed' data-arranged='".$arranged_date->toFormattedDateString()."' data-dueby='".$nextReview->toFormattedDateString()."'>Action Needed</span>";
                    } else {
                        //date was set by partner
                        $nextReviewString = "<span class='duedate text-success' title='Review arranged - Not Done' data-arranged='".$arranged_date->toFormattedDateString()."' data-dueby='".$nextReview->toFormattedDateString()."'>Arranged: ".$arranged_date->toFormattedDateString()." at ".$arranged_date->format('H:iA').'</span>.';
                    }
                } else {
                    //date was set by partner
                    $nextReviewString = "<span class='duedate text-success' title='Review arranged' data-arranged='".$arranged_date->toFormattedDateString()."' data-dueby='".$nextReview->toFormattedDateString()."'>Arranged: ".$arranged_date->toFormattedDateString()." at ".$arranged_date->format('H:iA').'</span>.';
                }
            } elseif ($declaration_needed) {
                if ($review['active_os']->val_req_id()) {
                    $nextReviewString = "<span class='duedate text-danger' title='Confirmation Needed' data-dueby='".$nextReview->toFormattedDateString()."'>Confirmation Needed</span>";
                } else {
                    // no review - declaration needed
                    $nextReviewString = "<span class='duedate text-danger' title='Declaration Needed' data-dueby='".$nextReview->toFormattedDateString()."'>Declaration Needed</span>";
                }
            } elseif ($nextReview) {
                //date is guessed
                    $colour_class = ($nextReview <= Carbon::now()->addMonths(2)) ? 'text-warning' : '';
                    $title_text = ($nextReview <= Carbon::now()->addMonths(2)) ? 'within next 2 months' : '';
                    $nextReviewString = "<span class='duedate ".$colour_class."' title='Review due ".$title_text."' data-dueby='".$nextReview->toFormattedDateString()."'>".$icon." Due by: ".$nextReview->toFormattedDateString().'</span>';
            }

            return $nextReviewString;
        }

        return false;
    }

    public function reviewCountdown()
    {
        $return = [
            'last' => false,
            'next' => false,
            'next_original' => false,
            'arranged' => false,
            'arranged_date' => false,
            'initial' => false,
            'declaration_needed' => false,
            'level' => false,
            'custom_extension' => false,
            'custom_extension_months' => false,
        ];

        $last_review_date = false;
        $nextReview = null;
        $level_date = false;

        $s = new Search(new ServicingPropositionLevel());
        $s->eq("client_id", $this->id());

        if($spl = $s->next(MYSQLI_ASSOC)){
            $return['level'] = $spl->level();

            // get date of most recent servicing level
            if(!empty($spl->updated_when())){
                $level_date = $spl->updated_when();
            } else {
                $level_date = $spl->level_when();
            }

            if (in_array($spl->level(), [1,2,3,'B'])){
                // we need to loop through previous OS objects
                $s2 = new Search(new OngoingService());
                $s2->eq("client_id", $this->id());
                $s2->add_order("id", "DESC");

                while($os = $s2->next(MYSQLI_ASSOC)){
                    $return['initial'] = false;

                    if(empty($os->archived_when()) && empty($os_active)){
                        $return['active_os'] = $os_active = $os;
                    } else if ($last_review_date = Client::checkOsConditions($os)) {
                        $return['last_os'] = $os;
                        // we have found a record to use
                        break;
                    }
                }

                $level_date = Carbon::parse($level_date);

                if (! $last_review_date) {
                    // never had a review, base on servicing level date
                    $new_base_date = $level_date;
                    if ($spl->level() == 1) {
                        $return['initial'] = true;
                    }
                } else {
                    $last_review_date = Carbon::parse($last_review_date);
                    $new_base_date = ($level_date > $last_review_date) ? $level_date : $last_review_date;
                }

                $lastReview = $new_base_date;
                $return['last'] = $lastReview;
                $nextReview = $lastReview->copy();

                if(!empty($spl->frequency())){
                    // use custom frequency column for bespoke servicing levels
                    $reviewPeriod = $spl->frequency();
                } else {
                    $reviewPeriod = 12;
                    // if client is on level 1 and are not awaiting their initial review, 6 month cycle
                    if ($spl->level() == 1 && !$return['initial']) {
                        $reviewPeriod = 6;
                    }
                }

                $nextReview = $nextReview->addMonths($reviewPeriod);

                if ($nextReview >= '2020-03-24 00:00:00' && $nextReview <= '2020-07-12 23:59:59') {
                    // while we are in lockdown, allow an extra 3 months per review period
                    $return['covid_extension'] = true;
                    $return['next_original'] = $nextReview->copy();
                    $nextReview = $nextReview->addMonths(3);
                }

                $s3 = new Search(new ClientReviewExtension());
                $s3->eq("client", $this->id());
                $s3->eq("archived_when", null);
                $s3->limit(1);

                if ($extension = $s3->next(MYSQLI_ASSOC)) {
                    $return['custom_extension_months'] = $extension->extra_months();
                    $return['custom_extension'] = $extension->extra_months().' '.pluralize($extension->extra_months(), 'month').' custom exception granted ('. $extension->reason_extended() .')';
                    $return['next_original'] = $nextReview->copy();
                    $nextReview = $nextReview->addMonths($extension->extra_months());
                }

                $return['next'] = $nextReview;
            }

            /*
             * all date logic is done to determine next review date,
             * we need to check if we have an active ongoing service object with a proposed review date
            */

            if (! empty($os_active)) {
                if (!empty($os_active->proposed_review_date())) {
                    $return['initial'] = false;
                    $return['arranged'] = true;
                    $return['arranged_date'] = Carbon::parse($os_active->proposed_review_date());

                    /**
                     * Don't move date on if report wasn't issued
                     */
                    if (! empty($os_active->val_req_id->workflowAuditCheck()) && ! $os_active->val_req_id->workflowAuditCheck()->early_end()) {
                        // report done
                        $nextReview = $return['arranged_date']->copy()->addMonths($reviewPeriod);
                    } elseif (empty($os_active->val_req_id->workflowAuditCheck())) {
                        // report still pending
                        $nextReview = $return['arranged_date']->copy()->addMonths($reviewPeriod);
                    }

                    if ($return['arranged_date']->isPast()) {
                        $return['declaration_needed'] = true;
                    }
                } else {
                    $return['arranged_date'] = false;
                    if ($os_active->no_review_dropdown() === "Client Satisfied - no meeting required" && empty($os_active->document_upload_ids())) {
                        $return['declaration_needed'] = true;
                    }
                }
            }

            $return['next'] = $nextReview;
            return $return;
        }

        return false;
    }

    public function checkOsConditions($os)
    {
        $last_review_date = false;

        if ($os->review_type() === 'PS Forced') {
            // PS forced review, we are moving the review date on
            $last_review_date = $os->archived_when();
        } elseif ($os->review_done()) {
            // client review was completed, move date on
            $last_review_date = $os->proposed_review_date();
        } elseif (! $os->review_accepted() && $os->no_review_dropdown() === 'Client Satisfied - no meeting required') {
            // PS will send a report (if applicable), we are moving the review date on
            $last_review_date = $os->archived_when();
        } elseif (! $os->review_done() && ! is_null($os->val_req_id())) {
            // Review not done but report sent so PSL obligation complete, move date on
            if (! empty($os->val_req_id->workflowAuditCheck()) && ! $os->val_req_id->workflowAuditCheck()->early_end()) {
                // we have checked report wasn't early ended and actually produced
                $last_review_date = $os->proposed_review_date();
            } elseif (! empty($os->val_req_id->workflowPendingCheck())) {
                // workflow is still pending and report will likely be produced, move date on. If workflow is cancelled, date may revert.
                $last_review_date = $os->proposed_review_date();
            }
        } elseif ($os->archived_when() <= "2020-07-13" || (!empty($os->proposed_review_date()) && $os->proposed_review_date() <= "2020-07-13")) {
            // this has been done to take into account changes we made, move on for old behaviour
            if($os->review_offered() && !empty($os->proposed_review_date()) && empty($os->review_done())){
                $last_review_date = $os->proposed_review_date();
            } elseif($os->review_offered() && empty($os->proposed_review_date() && empty($os->review_done()))) {
                $last_review_date = $os->archived_when();
            } elseif ($os->review_offered() && empty($os->review_accepted())){
                $last_review_date = $os->archived_when();
            }
        }

        // extra checks for Non PS report partners
        if(isset($os, $this->partner) && !$this->partner->request_pro_report() && $os->archived_when() >= "2020-09-14"){

            $documents = $os->document_upload_ids();

            // check for any documents relating to the service object
            if(empty($documents)){
                $s = new Search(new DocumentStorage());
                $s->eq("object", "OngoingService");
                $s->eq("object_id", $os->id());
                $s->add_order("id", "DESC");

                if($doc = $s->next(MYSQLI_ASSOC)){
                    $documents = $doc;
                }
            }

            // if still empty, check for any documents relating to review offers
            if(empty($documents)){
                $s = new Search(new ReviewAttempt());
                $s->eq("ongoingService", $os->id());
                $s->add_order("id", "DESC");

                if($attempt = $s->next(MYSQLI_ASSOC)){
                    if(!empty($attempt->contact_number())){
                        // telephone/text evidence supplied, fake document flag to allow date to move on
                        $documents = true;
                    } else {
                        $s = new Search(new DocumentStorage());
                        $s->eq("object", "ReviewAttempt");
                        $s->eq("object_id", $attempt->id());

                        if($doc = $s->next(MYSQLI_ASSOC)){
                            $documents = $doc;
                        }
                    }
                }
            }

            if(!empty($documents)){
                if(!$os->review_accepted() && !empty($os->declined_review_date())){
                    // review was declined, move date on
                    $last_review_date = $os->declined_review_date();
                }

                if(!$os->review_accepted() && !empty($attempt)){
                    // review was declined, contact evidence provided, move date on
                    $last_review_date = $attempt->contact_when();
                }

                if($os->review_accepted() && !empty($os->proposed_review_date()) && !$os->review_done()){
                    // review was accepted but did not take place, move date on
                    $last_review_date = $os->proposed_review_date();
                }
            }
        }

        return $last_review_date;
    }

    public static function merge_history($client)
    {
        $old_ids = [];
        $previous_id = false;

        $db = new mydb();
        $q = "SELECT * FROM tbldedupe where movedtoClientID = " . $client;
        $db->query($q);

        while ($row = $db->next(MYSQLI_ASSOC)) {
            $old_ids[] = $row['oldClientID'];
            $previous_id = true;
        }

        $ids = implode(", ", $old_ids);
        $text = "Previous Client ID's that have been merged into viewing Client: " . $ids;
        $notification = "<div class='notification info static' style='display:block;'>" . $text . " <a class='float-right view-merge-history' data-id='" . $client . "' href='#'>View History</a></div>";

        if ($previous_id == true) {
            return $notification;
        }
    }

    // Returns GRI at risk for a client given a client id.
    public static function client_gri_atrisk($client_id, $life = false)
    {
        // Search for clients who have policies with platforms
        $plats = array(
            1152, # CoFunds
            86,   # Fidelity
            1216, # Old Mutual Wealth Platform, formally know as @SiS
        );

        $gri_fig = "";
        $plat_str = implode(', ', $plats);

        $db = new mydb();
        $db->query("select client_id, sum(renewal_12) as renewal_12, sum(renewal_lifetime)
            from feebase.policy_gri
            inner join feebase.tblclient
            on tblclient.clientid = policy_gri.client_id
            inner join feebase.tblpolicy
            on tblpolicy.policyid = policy_gri.policy_id
            where tblclient.clientID = " . $client_id . "
            and tblpolicy.issuerid in (" . $plat_str . ")
            and tblpolicy.status = 2
            and tblpolicy.ac_applied = 0
            and tblpolicy.ac_exempt = 0
            and renewal_12 IS NOT NULL
            group by tblclient.clientid
            order by renewal_12 desc");

        //make an array of clients that are brought back from the PolicyGRI search itr.
        $clients = [];
        while ($x = $db->next(MYSQLI_ASSOC)) {
            if (!in_array($x['client_id'], $clients)) {
                $clients[] = $x['client_id'];
            }
        }

        /* for each client we want to check that every policy under the platforms has a correspondence item with
       subject 'Adviser Charging Form sent to Provider', if not we will define this as at risk */

        $client_to_remove = [];

        foreach ($clients as $c) {
            $db = new mydb();
            $db->query("SELECT DISTINCT policyID FROM feebase.tblpolicy
                        WHERE clientID = " . $c . "
                        AND issuerID IN(" . $plat_str . ")
                        AND status = 2
                        AND tblpolicy.ac_applied = 0 AND tblpolicy.ac_exempt = 0");

            $policies_returned = [];

            while ($p = $db->next(MYSQLI_NUM)) {
                //add unique policyIDs to an array for checking, $p[0] == policyID
                $policies_returned[] = $p[0];
            }

            $safe_policies = [];

            foreach ($policies_returned as $pol) {
                $db->query("SELECT Policy FROM feebase.tblcorrespondencesql
                          WHERE Policy = " . $pol . " AND subject = 'Adviser Charging Form sent to Provider'");

                while ($corr = $db->next(MYSQLI_NUM)) {
                    //if row returned, add to safe_policies array,  $corr[0] == policy id
                    if ($corr[0]) {
                        $safe_policies[] = $corr[0];
                    }
                }
            }

            //create a new array with the safe policies removed
            $policies_array = array_diff($policies_returned, $safe_policies);

            //array is empty there for all policies under this client are safe to be removed.
            if (empty($policies_array)) {
                $client_to_remove[] = $c;
            }
        }

        if (isset($policies_array)) {
            //only bring back rows for clients that are at risk
            $client_to_show = array_diff($clients, $client_to_remove);

            /* if a partner has no clients to show, add value of 0 to the array so to not break the
                IN clause on the search below  */
            if (empty($client_to_show)) {
                $client_to_show[] = 0;
            }

            $client_str = implode(', ', $client_to_show);

            $db = new mydb();
            $db->query("select client_id, sum(renewal_12) as renewal_12, sum(renewal_lifetime) as renewal_lifetime
                from feebase.policy_gri
                inner join feebase.tblclient
                on tblclient.clientid = policy_gri.client_id
                inner join feebase.tblpolicy
                on tblpolicy.policyid = policy_gri.policy_id
                where tblclient.clientID = " . $client_id . "
                and tblpolicy.issuerid in (" . $plat_str . ")
                and tblpolicy.status = 2
                and tblpolicy.ac_applied = 0
                and tblpolicy.ac_exempt = 0
                and renewal_12 IS NOT NULL
                and client_id in(" . $client_str . ")
                group by tblclient.clientid
                order by renewal_12 desc");


            if (!$life) {
                while ($x = $db->next(MYSQLI_ASSOC)) {
                    $gri_fig += $x['renewal_12'];
                }
            } else {
                while ($x = $db->next(MYSQLI_ASSOC)) {
                    $gri_fig += $x['renewal_lifetime'];
                }
            }

            return "&pound;" . number_format(floatval($gri_fig), 2);
        } else {
            return "&pound;" . number_format(0, 2);
        }
    }

    public function on_create($post = false)
    {
        if (isset($post["reason"])) {
            try {
                //create new database instance and then add reasoning to tbl_action_reason
                $db = new mydb();
                $q = "INSERT INTO
                         `feebase`.`tbl_action_reason` (`object`, `object_id`, `reason`, `created_by`, `created_when`,`action` )
                         VALUES ('Client', '" . $this->id() . "', '" . $post['reason'] . "', '" . $_COOKIE['uid'] . "', '" . time() . "', 'Add')";
                $db->query($q);
            } catch (Exception $e) {
                throw $e;
            }
        }

        if (!isset($this->salutation) || $this->salutation() == ""){
            $this->salutation($this->generate_salutation($this));
            $this->save();
        }
    }

    public function on_update($post = false)
    {
        $response = [
            "trigger" => null,
            "data" => null,
        ];

        $c = new Client($_GET['id'], true);

        if (!$c->group_scheme() && $post['group_scheme']) {
            // client has been changed to 'Group Scheme', update all policies to scheme owner
            $s = new Search(new Policy);
            $s->eq("client", $c->id());
            while ($pol = $s->next()) {
                $pol->owner(5);
                $pol->save();
            }
        }

        if ($post['name_changed'] && $sal = $this->generate_salutation($this)){
            $response['trigger'] = "salutation";
            $response['data'] = $sal;
        } /*else {
//            // loads the current instance of the client, before its saved
//            $c = new Client($this->id(), true);
//
//            // check if the client is on IO
//            if ($c->intelligent_office()){
//                // check if any of the details we store on IO have changed
//                if(
//                    strval($c->one->title) != strval($this->one->title) ||
//                    strval($c->one->forename) != strval($this->one->forename) ||
//                    strval($c->one->surname) != strval($this->one->surname) ||
//                    strval($c->one->nino) != strval($this->one->nino) ||
//                    strval($c->one->dob) != strval($this->one->dob) ||
//                    $c->one->client_deceased() != $this->one->client_deceased() ||
//                    strval($c->two->nationality) != strval($this->two->nationality) ||
//                    strval($c->two->title) != strval($this->two->title) ||
//                    strval($c->two->forename) != strval($this->two->forename) ||
//                    strval($c->two->surname) != strval($this->two->surname) ||
//                    strval($c->two->nino) != strval($this->two->nino) ||
//                    strval($c->two->dob) != strval($this->two->dob) ||
//                    $c->two->client_deceased() != $this->two->client_deceased() ||
//                    strval($c->two->nationality) != strval($this->two->nationality) ||
//                    strval($c->salutation) != strval($this->salutation)
//                ){
//                    try{
//                        // create curl resource
//                        $ch = curl_init();
//
//                        // set url
//                        curl_setopt($ch, CURLOPT_URL, "http://192.168.16.194:8000/api/client/" . $c->id());
//                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
//
//                        // get stringified data/output. See CURLOPT_RETURNTRANSFER
//                        $data = curl_exec($ch);
//                        $error = curl_error($ch);
//
//                        if ($error) {
//                            throw new Exception("Client Update - " . $error);
//                        } else if ($data) {
//                            // We've created a job on HRZN to update the client
//                            $data = json_decode($data);
//
//                            if(!$data->success){
//                                $response['trigger'] = "alert";
//
//                                $response['data'] = "Failed to update client in IO. " . $data->data . ". Contact IT";
//                            } else {
//                                $response['trigger'] = "alert";
//
//                                $response['data'] = "Failed to update client in IO. " . $data->data . ". Contact IT";
//                            }
//                        } else {
//                            throw new Exception("Unknown error updating client");
//                        }
//                    } catch (Exception $e) {
//                        $response['data'] = "Error: " . $e;
//                    }
//                }
//            }
        }*/

        if(($post['one']['client_deceased'] && !$c->one->client_deceased()) || ($post['two']['client_deceased'] && !$c->two->client_deceased())){
            // one of the clients was marked as deceased
            if ($post['one']['client_deceased'] && (empty($post['two']['surname']) || $post['two']['client_deceased'])){
                // both clients marked as deceased
                $s = new Search(new ActionRequired());

                // get any open action required tickets for "Urgent: New Client Agreement Required"
                $s->eq("subject_id", 29);
                $s->eq("object", "Client");
                $s->eq("object_id", $this->id());
                $s->eq("completed_by", null);

                while($ar = $s->next(MYSQLI_ASSOC)){
                    $ar->add_message("Action required automatically closed as the client has sadly been marked as deceased. No further action required.");
                    $ar->close();
                }

                // check to see if there are portfolio report workflow tickets open
                $s = new Search(new Workflow());
                $s->eq("_object", "Client");
                $s->eq("index", $this->id());
                $s->eq("step", Workflow::VALUATION_STEPS);

                if($wf = $s->next(MYSQLI_ASSOC)){
                    // there is workflow, email IO and let them know
                    $subject = "Open Workflow for Deceased Client";
                    $sender = [
                        'name' => "IT",
                        'email' => 'it@policyservices.co.uk',
                    ];

                    $recipient = [
                        [
                            'address' => [
                                'name' => 'IO',
                                'email' => 'portfolioreports@policyservices.co.uk'
                            ]
                        ]
                    ];

                    $content_string = "<p>Client " . $this->id() . " has now been marked as deceased. There are open portfolio reports workflow tickets against this client that should be checked.</p>";

                    $body = json_encode([
                        "SUBJECT" => $subject,
                        "FROM_NAME" => "IT",
                        "SENDER" => "it@policyservices.co.uk",
                        "REPLY_TO" => "it@policyservices.co.uk",
                        "ADDRESSEE" => "IO",
                        "CONTENT_STRING" => $content_string
                    ]);

                    sendSparkEmail($recipient, $subject, 'generic-staff', $body, $sender);
                }
            }
        }

        return (object)$response;
    }

    public function addressee()
    {
        $pieces = [];

        foreach (array("one", "two") as $person) {
            foreach (array("title", "surname") as $namebit) {
                if (strlen($this->{$person}->{$namebit}->value)) {
                    $pieces[$person][$namebit] = $this->{$person}->{$namebit}->value . "";
                }
            }
        }

        if (sizeof($pieces) == 2) {
            return ($pieces["two"]["surname"] == $pieces["one"]["surname"] || !strlen($pieces["two"]["surname"]))
                ? $pieces["one"]["title"] . " & " . $pieces["two"]["title"] . " " . $pieces["one"]["surname"]                  // Mr. & Mrs. Smith
                : $pieces["one"]["title"] . " " . $pieces["one"]["surname"] . " & " . $pieces["two"]["title"] . " " . $pieces["two"]["surname"]; // Mr. Smith & Mrs. White
        } elseif (sizeof($pieces) == 0) {
            return "";
        } else {
            return $pieces["one"]["title"] . " " . $pieces["one"]["surname"]; // Mr. Smith
        }
    }

    public function __toString()
    {
        if (!$this->loaded) {
            $this->get();
        }

        if ($this->group_scheme() && $this->scheme_name()) {
            return $this->scheme_name();
        } else {
            foreach (array("one", "two") as $x) {
                foreach (array("forename", "surname") as $y) {
                    ${$y . '_' . $x} = "" . $this->{$x}->{$y};
                }
            }

            if (!$this->primary_key()) {
                return "";
            } elseif (empty($surname_one) && empty($surname_two)) {
                return "Unnamed";
            }


            // Same surnames
            if ($surname_one == $surname_two) {
                // Example: Smith, John & Mary
                $n = "$forename_one & $forename_two $surname_one";
                if (preg_match('/[\w\-\']+ & [\w\-\']+ [\w\-\']+/', $n)) {
                    return $n;
                }
            }

            // both available, different surnames Example: Smith, John & White*, Mary
            $n = "$forename_one $surname_one & $forename_two $surname_two";
            if (preg_match('/[\w\-\']+ [\w\-\']+ & [\w\-\']+ [\w\-\']+/', $n)) {
                return $n;
            }

            // just client one Example: Smith, John
            $n = "$forename_one $surname_one";
            if (preg_match('/[\w\-\']+ [\w\-\']+/', $n)) {
                return $n;
            }

            // bah! I give up
            return $surname_one;
        }
    }

    function addressee_options()
    {
        $a = [];

        $f1 = $this->one->name("t|f");
        $s1 = $this->one->name("s");

        $a[] = trim("$f1 $s1");

        if ($this->two->exists()) {
            $f2 = $this->two->name("t|f");
            $s2 = $this->two->name("s");

            $a[] = trim("$f2 $s2");

            if (!empty($s1)) {
                array_unshift(
                    $a,
                    (($s1 == $s2 || empty($s2)) ? "$f1 & $f2 $s1" : "$f1 $s1 & $f1 $s2")
                );
            }
        }

        $a[] = "Sir or Madam";

        return $a;
    }

    public function link($text = false)
    {
        $link = parent::link($text);

        //highlight clients without a postcode
        $postcode = (string)$this->address->postcode;
        if (empty($postcode)) {
            $link = preg_replace("/(class='.*?)(')/", "$1 warning$2", $link);
        }

        //highlight clients under partner UNTRACED
        $partner_id = $this->partner();
        if ($partner_id == '394') {
            $link = preg_replace("/(class='.*?)(')/", "$1 untraced$2", $link);
        }

        //highlight clients that are Archived
        if ($this->archived() != null) {
            $link = preg_replace("/(class='.*?)(')/", "$1 Archived$2", $link);
        }


        return $link;
    }

    public function has_workflow()
    {
        $s = new Search(new Workflow);
        $s->eq("_object", "client");
        $s->eq("index", $this->id());
        $s->add_or(
            array(
                $s->eq("step", 16, true, 0),   // watch

                $s->eq("step", 90, true, 0),   // client address change - client services
                $s->eq("step", 91, true, 0),   // client address change - client services
                $s->eq("step", 92, true, 0),   // client address change - client services

                $s->eq("step", 93, true, 0),   // client address change - acq support
                $s->eq("step", 94, true, 0),   // client address change - acq support
                $s->eq("step", 95, true, 0),   // client address change - acq support

                $s->eq("step", 96, true, 0),   // client address change - new business
                $s->eq("step", 97, true, 0),   // client address change - new business
                $s->eq("step", 98, true, 0),   // client address change - new business

                $s->eq("step", 106, true, 0),  // client address change - IO
                $s->eq("step", 107, true, 0),  // client address change - IO
                $s->eq("step", 108, true, 0),  // client address change - IO

                $s->eq("step", 109, true, 0),  // client address change - Client Relations
                $s->eq("step", 110, true, 0),  // client address change - Client Relations
                $s->eq("step", 111, true, 0),  // client address change - Client Relations

                $s->eq("step", 40, true, 0),   // helpdesk chase - client services
                $s->eq("step", 42, true, 0),   // helpdesk chase - new business
                $s->eq("step", 43, true, 0)    // helpdesk chase - acq support
            )
        );

        $alerts = "";

        while($wf = $s->next(MYSQLI_ASSOC)){
            $alerts .= $wf->link("<div class='alert alert-warning mb-2 block-submit'><i class='fas fa-exclamation-circle'></i> This client currently has a \"" . $wf->step . "\" ticket open. Please see " . $wf->addedby . " for more information. (Click here to open & acknowledge)</div>");
        }

        echo $alerts;
    }

    //Returns true whether a client has trustees or not
    public function has_trustee()
    {
        $s = new Search(new Trustee);
        $s->eq("client", $this->id());

        return (($t = $s->next()) !== false) ? $t : false;
    }

    public function clientWorth($life = false)
    {
        $db = new mydb();

        $redisClient = RedisConnection::connect();

        $worth_fig = "";

        if($life){
            if($redisClient->exists("income.client.".$this->id().".worth.lifetime")){
                $worth_fig = $redisClient->get("income.client.".$this->id().".worth.lifetime");
            } else {
                $db->query("select round(sum(amount),2) as total_amount from tblcommissionauditsql c
                        inner join tblpolicy p on c.policyID = p.policyID
                        where p.clientID = " . $this->id() . "
                        and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                        and Commntype in (8,10)
                        group by p.clientID
                        limit 1");

                if($client_worth = $db->next(MYSQLI_ASSOC)){
                    $db->query("select round(sum(vat),2) as total_vat from tblcommissionauditsql c
                            inner join tblpolicy p on c.policyID = p.policyID
                            where p.clientID = " . $this->id() . "
                            and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                            and Commntype in (8,10)
                            group by p.clientID
                            limit 1");

                    if($vat = $db->next(MYSQLI_ASSOC) ){
                        ($vat['total_vat'] < 0) ? $client_worth_1 = $client_worth['total_amount'] + $vat['total_vat'] : $client_worth_1 = $client_worth['total_amount'] - $vat['total_vat'];
                    } else {
                        $client_worth_1 = 0;
                    }
                } else {
                    $client_worth_1 = 0;
                }

                ($client_worth_1) ? $worth_fig = format_current($client_worth_1) : $worth_fig = "£ 0.00";

                $redisClient->set("income.client.".$this->id().".worth.lifetime", $worth_fig);
            }
        } else {
            if($redisClient->exists("income.client.".$this->id().".worth")){
                $worth_fig = $redisClient->get("income.client.".$this->id().".worth");
            } else {
                $minus_1_year = Carbon::now()->subYear();

                $db->query("select round(sum(amount),2) as total_amount from tblcommissionauditsql c
                        inner join tblpolicy p on c.policyID = p.policyID
                        where p.clientID = " . $this->id() . "
                        and date(paidDate) >= '" . $minus_1_year->format("Y-m-d") . "'
                        and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                        and Commntype in (8,10)
                        group by p.clientID
                        limit 1");

                if($client_worth = $db->next(MYSQLI_ASSOC)){
                    $db->query("select round(sum(vat),2) as total_vat from tblcommissionauditsql c
                            inner join tblpolicy p on c.policyID = p.policyID
                            where p.clientID = " . $this->id() . "
                            and date(paidDate) >= '" . $minus_1_year->format("Y-m-d") . "'
                            and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                            and Commntype in (8,10)
                            group by p.clientID
                            limit 1");

                    if($vat = $db->next(MYSQLI_ASSOC) ){
                        ($vat['total_vat'] < 0) ? $client_worth_1 = $client_worth['total_amount'] + $vat['total_vat'] : $client_worth_1 = $client_worth['total_amount'] - $vat['total_vat'];
                    } else {
                        $client_worth_1 = 0;
                    }
                } else {
                    $client_worth_1 = 0;
                }

                ($client_worth_1) ? $worth_fig = format_current($client_worth_1) : $worth_fig = "£ 0.00";

                $redisClient->set("income.client.".$this->id().".worth", $worth_fig);
            }
        }

        return $worth_fig;
    }

    public static function griQuery($where, $life = false)
    {
        $query = "SELECT IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)) as GRI " .
            " FROM " . COMM_AUDIT_TBL .
            " " . $where . " " .
            " AND commntype in " . CommissionType::griTypes();

        if(!$life){
            $query .= " AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)";
        }

        return $query;
    }

    // Returns GRI for a client given a client id.
    // Optional $life flag if lifetime GRI should be returned.  By default 12 months is returned.
    /* GRI calculated as (renewal(0) + clawback(2) + adviser_charging_ongoing(8) + client_direct_ongoing(10) + dfm_fees(12) +
    OAS Income(14) + Trail(15) + PPB Income(16)) - VAT */
    public function client_gri($client, $life = false)
    {
        $db = new mydb();
        $gri_fig = "";

        $redisClient = RedisConnection::connect();

        if (!$life) {
            if ($redisClient->exists("income.client.".$client.".client_gri")){
                $gri_fig = $redisClient->get("income.client.".$client.".client_gri");
            } else {
                $db->query(Client::griQuery("WHERE clientid = " . $client));

                if ($gri = $db->next()) {
                    $gri_fig = "&pound; " . number_format($gri['GRI'], 2);
                } else {
                    $gri_fig = "&pound;0.00";
                }

                $redisClient->set("income.client.".$client.".client_gri", $gri_fig);
            }
        } else {
            if($redisClient->exists("income.client.".$client.".client_gri.lifetime")){
                $gri_fig = $redisClient->get("income.client.".$client.".client_gri.lifetime");
            } else {
                $db->query(Client::griQuery("WHERE clientid = " . $client, true));

                if ($gri = $db->next()) {
                    $gri_fig = "&pound; " . number_format($gri['GRI'], 2);
                } else {
                    $gri_fig = "&pound;0.00";
                }

                $redisClient->set("income.client.".$client.".client_gri.lifetime", $gri_fig);
            }
        }

        return $gri_fig;
    }

    /* Check if client1 and/or client2 is deceased and inform user via a notification on prophet */
    public function is_deceased($client)
    {
        if ($client->one->client_deceased() == 1 && $client->two->client_deceased() != 1) {
            if ($client->two->forename != "") {
                $return = '<div class="alert alert-info mb-0" role="alert">
                <i class="fa fa-info-circle"></i>
                  Sadly ' . $client->one . ' has passed away, but is survived by ' . $client->two . '
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            } else {
                $return = '<div class="alert alert-info mb-0" role="alert">
                 <i class="fa fa-info-circle"></i>
                  Sadly ' . $client->one . ' has passed away
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            }
        } elseif ($client->one->client_deceased() != 1 && $client->two->client_deceased() == 1) {
            if ($client->one->forename != "") {
                $return = '<div class="alert alert-info mb-0" role="alert">
                <i class="fa fa-info-circle"></i>
                  Sadly ' . $client->two . ' has passed away, but is survived by ' . $client->one . '
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            } else {
                $return = '<div class="alert alert-info mb-0" role="alert">
                    <i class="fa fa-info-circle"></i>
                  Sadly ' . $client->two . ' has passed away
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            }
        } elseif ($client->one->client_deceased() == 1 && $client->two->client_deceased() == 1) {
            $return = '<div class="alert alert-info mb-0" role="alert">
                <i class="fa fa-info-circle"></i>
                  Sadly both of these clients have passed away.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
        }

        if (isset($return)) {
            echo $return;
        }
    }

    public function total_policies($client)
    {
        $s = new Search(new Policy);
        $s->eq("clientid", $client);
        return $s->count();
    }

    public function policies()
    {
        $s = new Search(new Policy());

        $s->eq("client", $this->id());

        $policies = [];

        while($p = $s->next(MYSQLI_ASSOC)){
            $policies[] = $p;
        }

        if (count($policies)){
            return $policies;
        } else {
            return false;
        }
    }

    public function pipelineEntries()
    {
        $pipelineEntries = [];

        if (!empty($this->policies())){
            foreach($this->policies() as $policy){
                array_merge($pipelineEntries, $policy->pipelineEntries());
            }
        }

        return $pipelineEntries;
    }

    public function commission()
    {
        $commission = [];

        if (!empty($this->policies())){
            foreach($this->policies() as $policy){
                $s = new Search(new Commission());

                $s->add_or([
                    $s->eq("policy", $policy->id(), false, 0),
                    $s->eq("client", $this->id(), false, 0),
                ]);

                while($p = $s->next(MYSQLI_ASSOC)){
                    $commission[] = $p;
                }
            }
        }

        return $commission;
    }

    public function sourceIncome()
    {
        $sources = [];

        if (!empty($this->policies())){
            foreach($this->policies() as $policy){
                array_merge($sources, $policy->sourceIncome());
            }
        }

        return $sources;
    }

    public function has_salutation($client)
    {
        if ($client->salutation()) {
            $salutation = "<img src='/lib/images/tick.png'>";
        } else {
            $salutation = "<img src='/lib/images/cross.png'>";
        }
        return $salutation;
    }

    public function client_age($client, $client_number = false)
    {
        $dob = $client->{$client_number}->dob();
        $from = new DateTime($dob);
        $to = new DateTime('today');
        $age = $from->diff($to)->y;

        return $age . " years old";
    }

    public function has_corr_last_12months($client)
    {
        $s = new Search(new Correspondence);
        $s->eq("client", $client->id());
        $s->gt("date", date("Y-m-d H:i:s", strtotime("-1 year", time())));

        return $s->count();
    }

    public function has_corr_image($client)
    {
        $s = new Search(new Correspondence);
        $s->eq("client", $client->id());
        $s->gt("date", date("Y-m-d H:i:s", strtotime("-1 year", time())));


        if ($s->count() >= 1) {
            $corr = "<img src='/lib/images/tick.png'>";
        } else {
            $corr = "<img src='/lib/images/cross.png'>";
        }
        return $corr;
    }

    public function has_client_agreement($client)
    {
        $s = new Search(new Policy);
        $s->eq("client", $client);
        $s->eq("issuer", 11764);

        if ($s->count() >= 1) {
            $corr = "<img src='/lib/images/tick.png'>";
        } else {
            $corr = "<img src='/lib/images/cross.png'>";
        }
        return $corr;
    }

    public static function address_change_check($client_id)
    {
        $db = new mydb();
        $q = "SELECT * FROM workflow where step in (44,45,46,47) and object_index = " . $client_id;

        $db->query($q);
        if ($row = $db->next(MYSQLI_ASSOC)) {
            return true;
        } else {
            return false;
        }
    }

    public function servicingPropositionLevel($asString = false, $asObject = false)
    {
        $s = new Search(new ServicingPropositionLevel);
        $s->eq("client_id", $this->id());

        if ($spl = $s->next(MYSQLI_ASSOC)){
            if($asObject){
                return $spl;
            } else {
                if ($asString){
                    return strval($spl);
                } else {
                    return $spl->level();
                }
            }
        } else {
            return false;
        }
    }

    public function splPercent()
    {
        $s = new Search(new ServicingPropositionLevel);
        $s->eq("client_id", $this->id());

        if ($spl = $s->next(MYSQLI_ASSOC)){
            return $spl->percent();
        }
    }

    public function ongoing_complaint($client)
    {
        if ($client->complaint_ongoing()){
            return "<div class='alert alert-danger'>
                <i class='fas fa-exclamation-circle'></i> We are currently dealing with a complaint regarding this client. Please contact compliance for more details.
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
                </div>";
        }
    }

    public function generate_salutation($client)
    {
        if ($client->group_scheme()) {
            $salutation = $client->scheme_name();
        } else {
            if (!$client->one->client_deceased() && !$client->one->deceased_date()
                && !$client->two->client_deceased() && !$client->two->deceased_date()){
                $salutation = $client->new_salutation($client->one->title(), $client->one->forename(), $client->one->surname(), $client->two->title(), $client->two->forename(), $client->two->surname());
            } elseif(!$client->one->client_deceased() && !$client->one->deceased_date()) {
                $salutation = $client->new_salutation($client->one->title(), $client->one->forename(), $client->one->surname());
            } elseif(!$client->two->client_deceased() && !$client->two->deceased_date()) {
                $salutation = $client->new_salutation($client->two->title(), $client->two->forename(), $client->two->surname());
            }
        }


        if (isset($salutation) && $salutation != strval($client->salutation)){
            return $salutation;
        }
    }

    public function new_salutation($clienttitle1, $clientfname1, $clientsname1, $clienttitle2 = null, $clientfname2 = null, $clientsname2 = null)
    {
        $salutation = "";
        if (isset($clienttitle1) && $clienttitle1 != ""
            && isset($clientfname1) && $clientfname1 != ""
            && isset($clientsname1) && $clientsname1 != ""
            && isset($clienttitle2) && $clienttitle2 != ""
            && isset($clientfname2) && $clientfname2 != ""
            && isset($clientsname2) && $clientsname2 != ""){

            if ($clientsname1 == $clientsname2){
                // Mr. & Mrs. Smith
                $salutation = $clienttitle1 . " & " . $clienttitle2 . " " . $clientsname1;
            } else {
                //Mr. Smith & Mrs. Duncan
                $salutation = $clienttitle1 . " " . $clientsname1 . " & " . $clienttitle2 . " " . $clientsname2;
            }
        } elseif (isset($clientfname1) && $clientfname1 != ""
            && isset($clientsname1) && $clientsname1 != ""
            && isset($clientfname2) && $clientfname2 != ""
            && isset($clientsname2) && $clientsname2 != ""){

            if ($clientsname1 == $clientsname2){
                // John & Sarah Smith
                $salutation = $clientfname1 . " & " . $clientfname2 . " " . $clientsname1;
            } else {
                // John Smith & Sarah Duncan
                $salutation = $clientfname1. " " . $clientsname1 . " & " . $clientfname2 . " " . $clientsname2;
            }
        } elseif (isset($clientfname1) && $clientfname1 != ""
            && isset($clientsname1) && $clientsname1 != ""
            && (!isset($clientfname2) || $clientfname2 == "")
            && (!isset($clientsname2) || $clientsname2 == "")){

            if (isset($clienttitle1) && $clienttitle1 != ""){
                //Mr. Smith
                $salutation = $clienttitle1 . " " . $clientsname1;
            } else {
                // John Smith
                $salutation = $clientfname1 . " " . $clientsname1;
            }
        } else {
            $salutation = null;
        }

        return str_replace(".", "", $salutation);
    }

    public function income_on_hold($client)
    {
        $s = new Search(new IncomeHold());
        $s->eq("object_type", "client");
        $s->eq("object_index", $client->id());
        $s->eq("release_when", null);

        if ($ih = $s->next(MYSQLI_ASSOC)){
            return $ih;
        } else {
            return false;
        }
    }

    public function income_on_hold_span($client){
        if ($ih = $this->income_on_hold($client)){
            if(IncomeHold::totalIncomeHeld($client->id()) > 0){
                $held_when = Carbon::parse($ih->held_when())->toFormattedDateString();
                return "<div id='income-hold-alert' class='alert alert-danger mb-1'>
                    <i class='fas fa-exclamation-circle'></i> This clients adviser charging income has been placed on hold by " . $ih->held_by . " on " . $held_when . ". Reason: " . $ih->holdReason() .  "
                    Total on Hold: &pound;". number_format(IncomeHold::totalIncomeHeld($client->id()), 2)."
                    </div>";
            }
        }
        return false;
    }

    public function vulnerable($index){
        $s = new Search(new VulnerableClient());

        switch ($index){
            case "one":
                $index = 1;
                break;
            case "two":
                $index = 2;
                break;
        }

        $s->eq("client", $this->id());
        $s->eq("client_index", $index);
        $s->eq("archived", 0);

        if ($vc = $s->next(MYSQLI_ASSOC)){
            return $vc;
        } else {
            return false;
        }
    }

    public function ar_count(){
        $redisClient = RedisConnection::connect();
        return $redisClient->exists("action_required.Client." . $this->id());
    }

    public function get_fees(){
        // check for pipeline income first, this will be quicker than checking all income
        $db = new mydb();
        $db->query("SELECT * FROM " . DATA_DB . ".tblcommission c INNER JOIN " . DATA_DB . ".tblpolicy p on c.policyID = p.policyID WHERE clientID = " . $this->id() . " AND commissiontype in (8, 10)");

        if ($db->next(MYSQLI_ASSOC)){
            return true;
        } else {
            // no pipeline income, lets check if they paid fees in the last 12 months
            $lastYear = Carbon::now()->subYear()->format("Y-m-d");

            $s = new Search(new Commission());
            $s->eq("client", $this->id());
            $s->eq("type", [8, 10]);
            $s->gt("date", $lastYear);
            $s->limit(1);

            if ($fees = $s->next(MYSQLI_ASSOC)){
                return true;
            } else {
                return false;
            }
        }
    }

    public function ar_links(){
        $s = new Search(new ActionRequired());

        $s->eq("object", "Client");
        $s->eq("object_id", $this->id());
        $s->eq("completed_when", null);
        $s->eq("archived", 0);

        $links = [];

        while($ar = $s->next(MYSQLI_ASSOC)){
            $links[] = $ar->link();
        }

        if (count($links)){
            return implode(", ", $links);
        } else {
            return false;
        }
    }

    public function wf_links($steps = []){
        $s = new Search(new Workflow());
        $links = [];

        $s->eq("_object", "Client");
        $s->eq("index", $this->id());

        if (count($steps)){
            $s->eq("step", $steps);
        }

        while($wf = $s->next(MYSQLI_ASSOC)){
            $links[] = $wf->link();
        }

        if ($policies = $this->policies()){
            foreach($policies as $p){
                if ($wfl = $p->wf_links($steps)){
                    $l = explode(", ", $wfl);

                    foreach($l as $link){
                        $links[] = $link;
                    }
                }
            }
        }

        if (count($links)){
            return implode(", ", $links);
        } else {
            return false;
        }
    }

    public function historic_wf_links($steps = []){
        $s = new Search(new WorkflowAudit());
        $links = [];

        $s->eq("_object", "Client");
        $s->eq("index", $this->id());

        if (count($steps)){
            $s->eq("step", $steps);
        }

        while($wf = $s->next(MYSQLI_ASSOC)){
            $links[] = $wf->link();
        }

        if ($policies = $this->policies()){
            foreach($policies as $p){
                if ($wfl = $p->historic_wf_links($steps)){
                    $l = explode(", ", $wfl);

                    foreach($l as $link){
                        $links[] = $link;
                    }
                }
            }
        }

        if (count($links)){
            return implode(", ", $links);
        } else {
            return false;
        }
    }

    public function deleteRecords(){
        $canDelete = true;

        if ($canDelete && !empty($this->sourceIncome())){
            $canDelete = false;

            throw new Exception("Client has income currently being processed");
        }

        if ($canDelete && !empty($this->pipelineEntries())){
            $canDelete = false;

            throw new Exception("Client has pending income");
        }

        if ($canDelete && !empty($this->commission())){
            $canDelete = false;

            throw new Exception("Client has historic commission entries");
        }

        if ($canDelete){
            // delete the client and everything under it

            // delete all notes
            // Delete notes under client first
            $sClientNotes = new Search(new Notes());
            $sClientNotes->eq("object_type", "Client");
            $sClientNotes->eq("object_id", $this->id());

            while($note = $sClientNotes->next(MYSQLI_ASSOC)){
                $note->delete();
            }

            // delete all trustees
            $sTrustees = new Search(new Trustee());
            $sTrustees->eq("client", $this->id());

            while($trustee = $sTrustees->next(MYSQLI_ASSOC)){
                $trustee->delete();
            }

            // delete all life events
            $sLifeEvents = new Search(new LifeEvent());
            $sLifeEvents->eq("client", $this->id());

            while($le = $sLifeEvents->next(MYSQLI_ASSOC)){
                $le->delete();
            }

            // delete all mandates
            $sMandates = new Search(new Mandate());
            $sMandates->eq("client", $this->id());

            while($m = $sMandates->next(MYSQLI_ASSOC)){
                $m->delete();
            }

            // delete all correspondence
            $sCorrespondence = new Search(new Correspondence());
            $sCorrespondence->eq("client", $this->id());

            while($c = $sCorrespondence->next(MYSQLI_ASSOC)){
                $c->delete();
            }

            // Delete action required
            $sAR = new Search(new ActionRequired());
            $sAR->eq("object", "Client");
            $sAR->eq("object_id", $this->id());

            while($ar = $sAR->next(MYSQLI_ASSOC)){
                $ar->delete();
            }

            // policy level loop for anything stored under the client at policy level
            if ($this->policies()){
                foreach($this->policies() as $policy){
                    $policy->deleteRecords(false);
                }
            }

            // delete client
            $this->delete();
        }

        return $canDelete;
    }

    public function io_client($index = 1){
        $s = new Search(new IOClient());
        $s->eq("prophet_id", $this->id());
        $s->eq("index", $index);

        if ($ioc = $s->next(MYSQLI_ASSOC)){
            return $ioc;
        } else {
            return new IOClient();
        }
    }

    public function changeOwningPartner($partnerTo, $reason, $action="Move Owning Partner")
    {
        $oldPartner = $this->partner();

        $this->partner($partnerTo);

        if ($this->save()){
            // create an entry in action reason
            $action = new ActionReason();

            $action->object("Client");
            $action->object_id($this->id());
            $action->reason(addslashes($reason));
            $action->created_by(User::get_default_instance("id"));
            $action->created_when(time());
            $action->action(addslashes($action));

            $action->save();

            // move correspondence to new partner
            $sCorr = new Search(new Correspondence());
            $sCorr->eq("client", $this->id());
            $sCorr->eq("partner", $oldPartner);

            while($c = $sCorr->next(MYSQLI_ASSOC)){
                $c->partner($partnerTo);

                $c->save();
            }

            // move action required to new partner
            $sAR = new Search(new ActionRequired());
            $sAR->eq("object", "Client");
            $sAR->eq("object_id", $this->id());

            while($ar = $sAR->next(MYSQLI_ASSOC)){
                $ar->partner($partnerTo);

                $ar->save();
            }

            // keep servicing level with client
            $sSPL = new Search(new ServicingPropositionLevel());
            $sSPL->eq("client_id", $this->id());

            while($spl = $sSPL->next(MYSQLI_ASSOC)){
                $spl->partner_id($partnerTo);

                $spl->save();
            }
        } else {
            return false;
        }

        return true;
    }

    public function canMerge($showReason = false)
    {
        $canMerge = true;

        if($this->pure_protection()){
            // client is marked as pure protection
            if (!empty($this->servicingPropositionLevel()) && $this->servicingPropositionLevel() !== "P"){
                $canMerge = false;

                if($showReason){
                    return "Client is marked as pure protection but is a " . $this->servicingPropositionLevel(true) . " client.";
                }
            }
        }

        return $canMerge;
    }

    public function markedForRemoval()
    {
        $s = new Search(new HandbackRemoval());
        $s->eq("client", $this->id());

        if($s->next(MYSQLI_ASSOC)){
            return true;
        }

        if($this->servicing_level() === "R"){
            return true;
        }

        return false;
    }

    public function review_extensions()
    {
        $s = new Search(new ClientReviewExtension());
        $s->eq("client", $this->id());
        $s->eq("archived_when", null);

        if($ext = $s->next(MYSQLI_ASSOC)){
            return "<div class='alert alert-warning mb-1'><i class='fas fa-info-circle'></i> " . $this . " currently " . (!empty($this->two->surname()) ? 'have' : 'has') . " a " . $ext->extra_months() . " month extension in place.</div>";
        }

        return false;
    }

    public function dsar($owner)
    {
        $python = '/usr/bin/python2.7';
        $py_script = '/usr/local/bin/dsar.py';

        if(!file_exists($python)){
            throw new Exception("Python executable does not exist in " . $python);
        }

        if(!is_executable($python)){
            throw new Exception("Python executable is not executable in " . $python);
        }

        if(!file_exists($py_script)){
            throw new Exception("Python script does not exist in " . $py_script);
        }

        if(!is_executable($py_script)){
            throw new Exception("Python script is not executable in " . $py_script);
        }

        $cmd = "$python $py_script " . $this->id() . " '$owner'";

        exec($cmd . " 2>&1", $output, $return_code);

        $message = "";

        // Loop through the return array and create a string of errors.
        foreach ($output as $r) {
            $message .= $r . "<br>";
        }

        //	If return code returns 0, no problems have arisen
        if ($return_code != 0) {
            // If array returned with data in it, then json return the data
            throw new Exception($message);
        }

        return $message;
    }

    public function unsegment($reason, $remove_welcome_pack = true)
    {
        $s = new Search(new ServicingPropositionLevel());

        $s->eq("client_id", $this->id());

        while($spl = $s->next(MYSQLI_ASSOC)){
            $spl->delete();
        }

        $ar = new ActionReason();

        $ar->object("Client");
        $ar->object_id($this->id());
        $ar->reason(addslashes($reason));
        $ar->action("Unsegment Client");

        $ar->save();

        if($remove_welcome_pack){
            // null out the client welcome letter field, treat it as if they never had one
            $this->welcome_pack("");
        }

        $this->save();

        $this->endOpenReviews("This client's review has been archived as they have been unsegmented.");
    }

    public function endOpenReviews($reason)
    {
        // end any open review cycles
        $s = new Search(new OngoingService());
        $s->eq("client_id", $this->id());
        $s->nt("review_done", 1);
        $s->eq("archived_when", null);

        while($os = $s->next(MYSQLI_ASSOC)){
            $os->archived_when(date("Y-m-d"));
            $os->archived_by(User::get_default_instance("myps_user_id"));

            $n = new MypsNotes();

            $n->object("Client");
            $n->object_id($this->id());
            $n->subject("Servicing Review Archive - PS");
            $n->text($reason);

            $n->save();

            $os->reason_declined($n->id());

            $os->save();
        }
    }

    public function geo_area()
    {
        $db = new mydb();
        $area = [];

        if(!empty($this->two->exists())){
            // joint client
            if(!empty($this->one->geo_area())){
                $area[] = $this->one->geo_area();
            }

            if(!empty($this->two->geo_area()) && $this->two->geo_area() !== $this->one->geo_area()){
                $area[] = $this->two->geo_area();
            }

            return implode(" & ", $area);
        }

        if(!empty($this->one->geo_area())){
            return $this->one->geo_area();
        }

        return "<i>No Residency</i>";
    }
}
