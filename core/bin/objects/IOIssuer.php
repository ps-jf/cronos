<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 18/10/2019
 * Time: 13:13
 */

class IOIssuer extends DatabaseObject
{
    const DB_NAME = IO_DB;
    const TABLE = "issuers";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("io_id", Field::PRIMARY_KEY);

        $this->prophet_id = Field::factory("prophet_id");

        $this->database = IO;

        parent::__construct($id, $auto_get);
    }
}