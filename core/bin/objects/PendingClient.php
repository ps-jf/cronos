<?php

class PendingClient extends Client
{
    public function __construct($id = false, $auto_get = false)
    {
        $this->data_hash = new Field("data_hash");

        $this->table = "`" . DATA_DB . "`.mandate_client_tbl";

        parent::__construct($id, $auto_get);
    }

    public function link($text = false)
    {

        // there is no link available for PendingClients as of yet. Override with toString method
        return "<abbr title='This is Client is pending and as such cannot be found via Prophet'><i>$this</i></abbr>";
    }
}
