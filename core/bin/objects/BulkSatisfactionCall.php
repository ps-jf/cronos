<?php

class BulkSatisfactionCall extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "bulk_satisfaction_call";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true);

        $this->q1 = Field::factory("q1")
            ->set_var(Field::DISPLAY_NAME, "Are you happy with the service that X provided for you during your transition period?");

        $this->q2 = Field::factory("q2")
            ->set_var(Field::DISPLAY_NAME, "Was X approachable and happy to help with any queries or concerns you may have had?");

        $this->q3 = Field::factory("q3")
            ->set_var(Field::DISPLAY_NAME, "Did you find the timeline document helpful (if applicable)?");

        $this->q4 = Field::factory("q4")
            ->set_var(Field::DISPLAY_NAME, "Did you and staff receive regular updates throughout your transfer?");

        $this->q5 = Field::factory("q5")
            ->set_var(Field::DISPLAY_NAME, "Did you receive a mypsaccount walkthrough session to explain how PS works?");

        $this->q6 = Field::factory("q6")
            ->set_var(Field::DISPLAY_NAME, "Was there any area of the service where you feel an improvement could have been made?");

        $this->q7 = Field::factory("q7")
            ->set_var(Field::DISPLAY_NAME, "On a scale of 1-10, with 10 being the best, how would you rate your KAC?");

        $this->q8 = Field::factory("q8")
            ->set_var(Field::DISPLAY_NAME, "Are you aware we have a NB team which are available to you for any NB cases ( non –SJP products)");

        $this->q9 = Field::factory("q9")
            ->set_var(Field::DISPLAY_NAME, "Do you understand the NB Tariff?");

        $this->q10 = Field::factory("q10")
            ->set_var(Field::DISPLAY_NAME, "If in contact with our NB team how did you find the experience?");

        $this->q11 = Field::factory("q11")
            ->set_var(Field::DISPLAY_NAME, "Were all the application forms which you required sourced by our NB team?");

        $this->q12 = Field::factory("q12")
            ->set_var(Field::DISPLAY_NAME, "Are you aware we operate an Online Switching system for bulks? ");

        $this->q13 = Field::factory("q13")
            ->set_var(Field::DISPLAY_NAME, "Have you used the NB section on your mypsaccount – Did you find the information useful?");

        $this->q14 = Field::factory("q14")
            ->set_var(Field::DISPLAY_NAME, "Was there any area of the service where you feel an improvement could have been made?");

        $this->q15 = Field::factory("q15")
            ->set_var(Field::DISPLAY_NAME, "How do you find the mypsaccount?");

        $this->q16 = Field::factory("q16")
            ->set_var(Field::DISPLAY_NAME, "Is there anything we could implement to improve the service we provide you?");

        $this->q17 = Field::factory("q17")
            ->set_var(Field::DISPLAY_NAME, "When in contact with PS – do you find our staff friendly and helpful?");

        $this->q18 = Field::factory("q18")
            ->set_var(Field::DISPLAY_NAME, "Have you got any concerns , 6 months on which I can address for you?");

        $this->comments = Field::factory("comments")
            ->set_var(Field::DISPLAY_NAME, "Any other comments?");

        $this->addedby = Sub::factory("User", "addedby")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("addedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updatedby")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updatedwhen")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
