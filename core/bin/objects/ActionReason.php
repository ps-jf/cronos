<?php

class ActionReason extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tbl_action_reason";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->object = Field::factory("object");

        $this->object_id = Field::factory("object_id");

        $this->reason = Field::factory("reason");

        $this->created_by = Sub::factory("User", "created_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->created_when = Date::factory("created_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->action = Field::factory("action");

        parent::__construct($id, $auto_get);
    }

    public static function getReason($object = false, $object_id = false, $action = false)
    {
        $span = "";

        if ($object != false && $object_id != false && $action != false) {

            $s = new Search(new ActionReason());
            $s->eq("object", $object);
            $s->eq("object_id", $object_id);
            $s->eq("action", $action);

            while ($a = $s->next(MYSQLI_ASSOC)) {
                $span .= '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> &nbsp; ' . $action . ' Reason:  ' . $a->reason() . '</div>';
            }
        }

        return $span;
    }

    function __toString()
    {
        return "$this->reason";
    }
}
