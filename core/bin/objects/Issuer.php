<?php
/**
 * Maps a specific issuer from tblissuer
 *
 *
 * @package prophet.objects.issuer
 * @author daniel.ness
 *
 */

class Issuer extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblissuer";
    var $_profilePath = "/pages/issuer/";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("issuerID", Field::PRIMARY_KEY);

        $this->name = Field::factory("issuerName")
            ->set_var(Field::REQUIRED, true);

        $this->code = Field::factory("issuerCode");

        $this->accepts_fax_mandates = Boolean::factory("acceptsmanbyfax");

        $this->visible = Boolean::factory("mypsview");

        $this->payscomm = Boolean::factory("payscomm");

        $this->website = Field::factory("website");

        $this->address = Address::factory(
            "issuerAdd1",
            "issuerAdd2",
            "issuerAdd3",
            "issuerAddPostcode"
        );

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->bulkaddress1 = Field::factory("bulk_transferaddress1");

        $this->bulkaddress2 = Field::factory("bulk_transferaddress2");

        $this->bulkaddress3 = Field::factory("bulk_transferaddress3");

        $this->bulkpostcode = Field::factory("bulk_transferpostcode");

        $this->bulkcontact = Field::factory("bulk_transfercontact");

        $this->bulkphone = Field::factory("bulk_transferphone");

        $this->bulkfax = Field::factory("bulk_transferfax");

        $this->bulkemail = Field::factory("bulk_transferemail");

        $this->bulksoftfacts = Field::factory("bulk_softfacts");

        parent::__construct($id, $auto_get);

        $this->SNAPSHOT_LOG = true;
    }

    public function __toString()
    {
        return "$this->name";
    }

    //CAREFUL:: always returns an address as a fallback
    public function get_contact($iss, $for = false, $forid = false)
    {
        if (!$iss) {
            $iss = $this->id();
        }

        $s = new Search(new IssuerContact);
        $s->eq("issuer", $iss);
        $s->add_order("main", "ASC");

        if ($for && $forid) {
            $s->add_or(array("$for = " . intval($forid), "is_main = 1"));
        } else {
            $s->eq("main", 1);
        }

        return $s->next();
    }
}
