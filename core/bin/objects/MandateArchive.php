<?php

class MandateArchive extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "mandate_archive";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->mandate_id = Sub::factory("Mandate", "mandate_id");

        $this->policy = Sub::factory("Policy", "policy_id");

        $this->client = Sub::factory("Client", "client_id");

        $this->issuer = Sub::factory("Issuer", "issuer_id");

        $this->archived_by = Sub::factory("User", "archived_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->archived_when = Date::factory("archived_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->CAN_DELETE = true;

        parent::__construct($id, $auto_get);
    }

    public static function get_mandate_history($id)
    {
        $p = new Policy($id, true);
        $current_mandate = $p->mandate();

        $history = "";
        $address_line = "";
        $address_line_archive = "";

        if ($current_mandate) {
            $mandate = new Mandate($current_mandate, true);

            if ($mandate->id()) {

                $s1 = new Search(new MandateAddressMapping());
                $s1->eq("mandate", $mandate->id());
                $s1->eq("policy", $p->id());
                while ($m = $s1->next()) {
                        $address_line .= "<tr>
                        <td>".$mandate->file."</td>
                        <td colspan='5'>Contact Details for recipient: ".$m->issuer_contact->link($m->issuer_contact->issuer)."</td>
                    </tr>";
                }

                $history = "<tr>
                        <td><a href='pages/mandate/?do=view&id=".$mandate->id()."' target='_blank'>" . $mandate->file . "</a></td>
                        <td>" . $mandate->sent_when . "</td>
                        <td>" . $mandate->sent_by->staff_name() . "</td>
                        <td>--</td><td>--</td>
                        <td>" . $mandate->original_copy . "</td>
                    </tr>";

            }

            if ($address_line != "") {
                $history = $history.$address_line;
            }
        }

        $s = new Search(new MandateArchive());
        $s->eq("policy", $id);
        $s->add_order("archived_when", "DESC");

        while ($m = $s->next()) {

            if ($m) {
                $s2 = new Search(new MandateAddressMapping());
                $s2->eq("mandate", $m->mandate_id());
                $s2->eq("policy", $p->id());

                while ($m1 = $s2->next()) {
                    $address_line_archive .= "<tr>
                        <td>".$m->mandate_id->file."</td>
                        <td colspan='4'>Contact Details for recipient: ".$m1->issuer_contact->link($m1->issuer_contact->issuer)."</td>
                    </tr>";
                }

                $history .= "<tr id='" . $m->mandate_id() . "'>
                    <td><a href='pages/mandate/?do=view&id=".$m->mandate_id()."' target='_blank'>" . $m->mandate_id->file . "</a></td>
                    <td>" . $m->mandate_id->sent_when . "</td>
                    <td>" . $m->mandate_id->sent_by->staff_name() . "</td>
                    <td>" . $m->archived_when . "</td>
                    <td>" . $m->archived_by->staff_name() . "</td>
                    <td>" . $mandate->original_copy . "</td>
                </tr>";
            }

            if ($address_line_archive != "") {
                $history = $history.$address_line_archive;
            }
        }

        return $history;
    }

    public function __toString()
    {
        return "$this->id";
    }
}
