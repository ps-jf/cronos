<?php

class TransferChecklistAudit extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "transfer_checklist_audit";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->checklist_id = Sub::factory("TransferChecklist", "transfer_checklist_id");

        $this->checklist_item = Field::factory('checklist_item');

        $this->field_value = Field::factory('field_value');

        $this->timestamp = Date::factory("timestamp")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set(time());

        $this->user_id = Sub::factory("User", "user_id")
            ->set(User::get_default_instance("id"));

        parent::__construct($id, $auto_get);
    }

    public static function get_submitted($checklist_id, $field)
    {
        $search = new Search(new TransferChecklistAudit);
        $search->eq("checklist_id", $checklist_id);
        $search->eq("checklist_item", $field);
        $search->add_order("id", "DESC");

        $audit = $search->next();

        if ($audit) {
            return "&nbsp;" . str_replace(".", " ", $audit->user_id) . " &nbsp;(" . $audit->timestamp . ") &nbsp; ";
        }
    }

    public function __toString()
    {
        return "$this->id";
    }
}
