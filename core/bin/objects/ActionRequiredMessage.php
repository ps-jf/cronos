<?php

class ActionRequiredMessage extends DatabaseObject
{
    const DB_NAME = REMOTE_SYS_DB;
    const TABLE = "action_required_message";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->action_required_id = Sub::factory("ActionRequired", "action_required_id");

        $this->message = Field::factory("message");

        $this->addedby = Sub::factory("WebsiteUser", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("myps_user_id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());


        $this->updated_by = Sub::factory("User", "updated_by");

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->open_by = Field::factory("open_by");

        $this->open_when = Date::factory("open_when")
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->internal = Boolean::factory("internal")
            ->set_values(1, 0)
            ->set(1);

        $this->alert = Boolean::factory("alert")
            ->set_values(1, 0)
            ->set(0);

        $this->CAN_DELETE = true;
        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->message";
    }
}
