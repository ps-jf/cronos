<?php

class IssuerContact extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "issuer_contact";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->issuer = Sub::factory("Issuer", "issuer_id")
            ->set_var(Field::REQUIRED, true);

        $this->policy_type = Choice::factory("policy_type")
            ->setSource(POLICY_TYPE_TBL, "policyTypeID", "policyTypeDesc");

        $this->group = Sub::factory("UserGroup", "group_id");

        $this->address = Address::factory(
            "address1",
            "address2",
            "address3",
            "postcode"
        );

        //$this->address1 = Field::factory("address1");
        //$this->address2 = Field::factory("address2");
        //$this->address3 = Field::factory("address3");
        //$this->postcode = Field::factory("postcode");

        $this->contact = Field::factory("contact");

        $this->phone = Field::factory("phone")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->fax = Field::factory("fax")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->email = Field::factory("email")
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->email_preferred = Boolean::factory("email_pref");

        $this->originals_required = Boolean::factory("orig_req");

        $this->main = Boolean::factory("is_main");

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->updated_by = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }

    public function default_email($id)
    {
        $s = new Search(new IssuerContact());
        $s->eq('issuer', $id);
        $s->eq('group', 4);

        if ($i = $s->next(MYSQLI_ASSOC)) {
            $email = $i->email();
        } else {
            $s = new Search(new IssuerContact());
            $s->eq('issuer', $id);
            $s->eq('main', 1);

            if ($i = $s->next(MYSQLI_ASSOC)) {
                $email = $i->email();
            }
        }

        return $email;
    }


    public function default_fax($id)
    {
        $s = new Search(new IssuerContact());
        $s->eq('issuer', $id);
        $s->eq('group', 4);

        if ($i = $s->next(MYSQLI_ASSOC)) {
            $fax = $i->fax();
        } else {
            $s = new Search(new IssuerContact());
            $s->eq('issuer', $id);
            $s->eq('main', 1);

            if ($i = $s->next(MYSQLI_ASSOC)) {
                $fax = $i->fax();
            }
        }

        return $fax;
    }

    static function format_fax($fax, $efax = true)
    {
        if ($fax == "") {
            return $fax;
        }

        $fax = trim($fax);
        $fax = str_replace(" ", "", $fax);

        if (strpos($fax, '+', 0) == 0) {
            //this is an international number
            $returnfax = substr($fax, 1);
        }

        if (strpos($fax, '0', 0) == 0) {
            //replace 0 with UK dialing code
            $returnfax = "44" . substr($fax, 1);
        }

        if ($efax == true) {
            //append the eFax domain to allow fax/email to send
            return $returnfax . "@efaxsend.com";
        } else {
            return $returnfax;
        }
    }
}
