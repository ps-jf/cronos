<?php

class DocMail extends DatabaseObject
{
    private $wsdl;
    private $sUsr;
    private $sPwd;
    private $sCallingApplicationID;
    private $nusoap_client;
    private $bColour;
    private $bDuplex;
    private $bIsMono;
    private $eDeliveryType;
    private $eAddressNameFormat;
    private $ProductType;
    private $DocumentType;
    private $sDespatchASAP;
    private $sDespatchDate;
    private $EmailOnProcessMailingError;
    private $EmailOnProcessMailingSuccess;

    /**
     * DocMail constructor.
     *
     * @param int $timeout
     */
    public function __construct()
    {
        // increase system timeout
        $timeout = 240;

        $this->wsdl = 'https://www.cfhdocmail.com/testAPI2/DMWS.asmx?WSDL';
        $this->sUsr = CFH_API_USR;
        $this->sPwd = CFH_API_PWD;
        $this->sCallingApplicationID = APP_NAME;
        $this->nusoap_client = new nusoap_client($this->wsdl, true);
        $this->nusoap_client->timeout = $timeout;
        set_time_limit($timeout);

        // Print in colour?
        $this->bColour = true;
        $this->bIsMono = !$this->bColour;
        // Print on both sides of paper?
        $this->bDuplex = false;

        // Undefined/FirstClass/StandardClass
        $this->eDeliveryType = 'Standard';
        //How the name appears in the envelope address  “Full Name”, “Firstname Surname”, “Title Initial Surname”,“Title Surname”, or “Title Firstname Surname”
        $this->eAddressNameFormat = 'Full Name';
        //ProductType (on Mailing): “A4Letter”, “BusinessCard”, “GreetingCard”, or “Postcard”
        $this->ProductType = 'A4Letter';
        //DocumentType (on Templates - selects the sub-type for a given template): “A4Letter”, “BusinessCard”,“GreetingCardA5”, “PostcardA5”, “PostcardA6”, “PostcardA5Right” or “PostcardA6Right”
        $this->DocumentType = 'A4Letter';

        $this->sDespatchASAP = true;
        $this->sDespatchDate = '';

        // email on success/failure
        $this->EmailOnProcessMailingError = DEV_EMAIL;
        $this->EmailOnProcessMailingSuccess = DEV_EMAIL;
    }

    /**
     * CreateMailing  - Setup array to pass into webservice call
     * https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=CreateMailing
     *
     * @param $sMailingName
     * @param $sMailingDescription
     *
     * @return mixed
     */
    public function CreateMailing($sMailingName, $sMailingDescription)
    {
        // Setup array to pass into webservice call
        $params = [
            'Username' => $this->sUsr,
            'Password' => $this->sPwd,
            'CustomerApplication' => $this->sCallingApplicationID,
            'ProductType' => $this->ProductType,
            'MailingName' => $sMailingName,
            'MailingDescription' => $sMailingDescription,
            'IsMono' => $this->bIsMono,
            'IsDuplex' => $this->bDuplex,
            'DeliveryType' => $this->eDeliveryType,
            'DespatchASAP' => $this->sDespatchASAP,
            //'DespatchDate' => $sDespatchDate,   //only include if delayed despatch is required
            'AddressNameFormat' => $this->eAddressNameFormat,
            'ReturnFormat' => 'JavaScript'
        ];

        return $this->makeCall(__FUNCTION__, $params);
    }

    /**
     * Add Single Address
     *
     * @param $address
     * @param $MailingGUID
     *
     * @return mixed
     */
    public function AddAddress($MailingGUID, $address)
    {
        $params = [
            "Username" => $this->sUsr,
            "Password" => $this->sPwd,
            "MailingGUID" => $MailingGUID,
            "Fullname" => $address['fullname'] ?? null,
            "Title" => $address['title'],
            "FirstName" => $address['fname'],
            "Surname" => $address['sname'],
            "Address1" => $address['line1'],
            "Address2" => $address['line2'],
            "Address3" => $address['line3'],
            "Address4" => $address['line4'],
            "Custom1" => $address['custom1'] ?? null,
            "Custom2" => $address['custom2'] ?? null,
            "Custom3" => $address['custom3'] ?? null,
            "Custom4" => $address['custom4'] ?? null,
            "Custom5" => $address['custom5'] ?? null,
            "Custom6" => $address['custom6'] ?? null,
            "Custom7" => $address['custom7'] ?? null,
            "Custom8" => $address['custom8'] ?? null,
            "Custom9" => $address['custom9'] ?? null,
            "Custom10" => $address['custom10'] ?? null,
            "ReturnFormat" => "JavaScript"
        ];

        return $this->makeCall(__FUNCTION__, $params);
    }

    /**
     * AddTemplateFile  - Read in $TemplateFile file data and setup array to pass into webservice call
     * https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=AddTemplateFile
     *
     * @param $TemplateFile
     * @param $TemplateName
     * @param $sTemplateFileName
     * @param $MailingGUID
     * @param int $copies
     * @param bool $AddressedDocument
     *
     * @return false|mixed
     */
    public function AddTemplateFile($TemplateFile, $TemplateName, $sTemplateFileName, $MailingGUID, $copies = 1, $AddressedDocument= true)
    {
        if ($TemplateFile) {
            // Load contents of word file into base-64 array to pass across SOAP
            $TemplateHandle = fopen($TemplateFile, 'rb');
            $TempeContents_doc = base64_encode(fread($TemplateHandle, filesize($TemplateFile)));
            fclose($TemplateHandle);

            $params = [
                "Username" => $this->sUsr,
                "Password" => $this->sPwd,
                "MailingGUID" => $MailingGUID,
                "DocumentType" => $this->DocumentType,
                "TemplateName" => $TemplateName,
                "FileName" => $sTemplateFileName,
                "FileData" => $TempeContents_doc,
                "AddressedDocument" => $AddressedDocument,
                "Copies" => $copies,
                "ReturnFormat" => "JavaScript"
            ];

            return $this->makeCall(__FUNCTION__, $params);
        }
        return false;
    }

    /**
     * @param $MailingGUID
     * @param string $templateName
     * @param int $copies
     *
     * https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=AddTemplateFile
     *
     * @return false|mixed
     */
    public function AddTemplateFromLibrary($MailingGUID, $templateName = 'Sample1', $copies = 1)
    {
        $params = [
            "Username" => $this->sUsr,
            "Password" => $this->sPwd,
            "MailingGUID" => $MailingGUID,
            "TemplateName" => $templateName,
            "Copies" => $copies,
            "ReturnFormat" => "JavaScript"
        ];

        return $this->makeCall(__FUNCTION__, $params);
    }

    /**
     * there are useful parameters that you may wish to include on this call which enable asynchronous notifications of successes and fails of automated orders to be sent to you via email or HTTP Post:
     * EmailSuccessList,EmailErrorList
     * HttpPostOnSuccess,HttpPostOnError
     * https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=ProcessMailing
     *
     * @param $MailingGUID
     * @param $ProofApprovalRequired
     *
     * @throws Exception
     */
    public function ProcessMailing($MailingGUID, $ProofApprovalRequired = false)
    {
        $params = [
            "Username" => $this->sUsr,
            "Password" => $this->sPwd,
            "MailingGUID" => $MailingGUID,
            "CustomerApplication" => $this->sCallingApplicationID,
            "SkipPreviewImageGeneration" => false,
            "Submit" => !$ProofApprovalRequired, //auto submit when approval is not requried
            "PartialProcess" => $ProofApprovalRequired, //fully process when approval is not requried
            "Copies" => 1,
            "ReturnFormat" => "JavaScript",
            "EmailSuccessList" => $this->EmailOnProcessMailingSuccess,
            "EmailErrorList" => $this->EmailOnProcessMailingError,
        ];

        $result = $this->makeCall(__FUNCTION__, $params);

        //optional wait to confirm the partial processing from ProcessMailing has completed
        $this->WaitForProcessMailingStatus($MailingGUID, "Partial processing complete", true);

        return $result;
    }

    /**
     * returns the file data of the PDF proof if it has been generated.
     * NOTE:  Status must that the show last "ProcessMailing"	call is complete before a proof can be returned.
     * https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetProofFile
     */
    public function GetProofFile($MailingGUID)
    {
        $params = [
            "Username" => $this->sUsr,
            "Password" => $this->sPwd,
            "MailingGUID" => $MailingGUID,
            "ReturnFormat" => "JavaScript"
        ];

        $result = $this->makeCall(__FUNCTION__, $params);

        $file_name = 'testing'.date('Y-m-d').'-'.time().'.pdf';

        // download a copy of proof
        file_put_contents($file_name, base64_decode($result));

        return $file_name;
    }

    /**
     * https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetStatus)
     * returns the status of a mailing from the mailing guid
     */
    public function GetStatus($MailingGUID)
    {
        $params = [
            "Username" => $this->sUsr,
            "Password" => $this->sPwd,
            "MailingGUID" => $MailingGUID,
            "ReturnFormat" => "JavaScript"
        ];

        return $this->makeCall(__FUNCTION__, $params);
    }

    /**
     * @param $callName
     * @param $params
     *
     * @return mixed
     */
    public function makeCall($callName, $params)
    {
        try {
            $callResult = $this->nusoap_client->call($callName, $params);
            // check for errors
            $this->checkError($callResult[$callName . 'Result']);

        } catch (Exception $e) {
            trigger_error($e);
        }

        //var_dump($callResult);

        return $callResult[$callName . 'Result'] ?? false;
    }

    /**
     * @param $res
     *
     * @return bool
     */
    public function checkError($res): bool
    {
        $error = false;

        $res = json_decode($res);

        if (empty($res)) {
            return $error;
        }

        if (is_object($res)) {
            foreach ($res as $key => $r) {
                if (in_array($key, ['Error code', 'Error message', 'Error code string'])) {
                    $error = true;
                    //throw new Exception("<h2>There was an error: </h2> " . $r);
                    var_dump("<h2>There was an error: </h2> " . $r);
                }
            }
        } else {
            var_dump("Response not an object: ", $res);
        }

        return $error;
    }

    /**
     * poll GetStatus in a loop until the processing has completed
     * loop a maximum of 10 times, with a 10 second delay between iterations.
     * alternatively; handle callbacks from the HttpPostOnSuccess & HttpPostOnError parameters on ProcessMailing
     * to identify when the processing has completed
     *
     * https://www.cfhdocmail.com/TestAPI2/DMWS.asmx?op=GetStatus
     * @param $MailingGUID
     * @param $expectedStatus
     */
    public function WaitForProcessMailingStatus($MailingGUID, $expectedStatus)
    {
        $i = 0;
        do {
            $result = json_decode($this->GetStatus($MailingGUID));
            $error = $result->ErrorCode ?? false;
            $status = $result->Status;

            if ($error || $status === "Error in processing") {
                //var_dump($error);
                //var_dump($status);
                // error, bomb out
                break;
            }

            if ($status === $expectedStatus) {
                //var_dump("expected status met");
                //end loop once processing is complete
                break;
            }

            sleep(5); //wait 10 second before repeating
            ++$i;
        } while ($i < 1); // 10

        # TODO - find out what we need to trigger this
        if ($status === "something") {
            $this->GetProcessingError($status, $MailingGUID);
        }

        // display error(s)
        if ($status !== $expectedStatus) {
            # todo, check other statuses, ie "Processing mailing - generating proof"
            //var_dump("WARNING: expected status '" . $expectedStatus . "' not reached.  Current status: '" . $status . "'");
        }
    }

    /**
     * @param $status
     * @param $MailingGUID
     */
    public function GetProcessingError($status, $MailingGUID)
    {
        if ($status === "Error in processing") {
            //get description of error in processing
            $params = [
                "Username" => $this->sUsr,
                "Password" => $this->sPwd,
                "MethodName" => __FUNCTION__,
                "ReturnFormat" => "JavaScript",
                "Properties" => [
                    "PropertyName" => __FUNCTION__,
                    "PropertyValue" => $MailingGUID
                ]
            ];

            $result = $this->makeCall("ExtendedCall", $params);
            echo $result;
        }
    }

}
