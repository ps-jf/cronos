<?php
/**
 * Created by PhpStorm.
 * User: sean.ross
 * Date: 14/01/2019
 * Time: 16:23
 */

class UserWidgets extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "user_widgets";

    public function __construct()
    {
        $this->CAN_DELETE = true;

        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->user_id = Field::factory("user_id")
            ->set(User::get_default_instance("id"));

        $this->widget_id = Field::factory("widget_id");

        $this->row = Field::factory("row");

        $this->col = Field::factory("col");
    }
}