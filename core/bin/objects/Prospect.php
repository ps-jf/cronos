<?php

class Prospect extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "pm_prospects";
    var $_profilePath = "/pages/prospect/";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->title = Title::factory("title");

        $this->firstname = Field::factory("firstname");

        $this->surname = Field::factory("surname")
            ->set_var(Field::REQUIRED, true);

        $this->practice = Field::factory("practice");

        $this->address = Address::factory(
            "address1",
            "address2",
            "address3",
            "postcode"
        );

        $this->sjp_manager1 = Choice::factory("sjp_manager1")
            ->setSource(DATA_DB . ".tblbusacqman", "id", "name");

        $this->sjp_manager2 = Choice::factory("sjp_manager2")
            ->setSource(DATA_DB . ".tblbusacqman", "id", "name");

        $this->sjp_btm = Choice::factory("sjp_btm")
            ->setSource(DATA_DB . ".tblbustransman", "id", "name");

        $this->ps_manager = Choice::factory("ps_manager")
            ->setSource(
                "select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "inner join " . TBL_USERS_GROUPS . " " .
                "on " . TBL_USERS_GROUPS . ".user_id = " . USR_TBL . ".id " .
                "where group_id = '4' AND department != '7'" .
                "order by last_name, first_name"
            );

        $this->phone = Field::factory("phone")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->mobile = Field::factory("mobile")
            ->set_var(Field::PATTERN, Patterns::MOBILE_PHONE);

        $this->email = Field::factory("email")
            ->set_var(Field::MAX_LENGTH, 50)
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->staff = Field::factory("staff")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->network = Choice::factory("network")
            ->setSource(DATA_DB . ".tblnetworks", "id", "networkName");

        $this->renewals = Field::factory("renewals")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->rate = Field::factory("rate")
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->transtype = Choice::factory("trans_type")
            ->push("1", "Mandate")
            ->push("2", "Agency- Single")
            ->push("3", "Agency- Agency")
            ->push("4", "Agency- Mandate")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Transfer Type");

        $this->agencies = Field::factory("agencies")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->clients = Field::factory("clients")
            ->set_var(Field::TYPE, Field::INTEGER);

        $this->top_issuers = Field::factory("top_issuers")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->back_office = Field::factory("back_office")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->unipass = Boolean::factory("unipass");

        $this->online_access = Field::factory("online_access")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->appointment = Date::factory("appointment")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->next_chase = Date::factory("next_chase_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->status = Choice::factory("status")
            ->push("1", "Recruit")
            ->push("2", "Appointed *Urgent*")
            ->push("3", "Not Joining SJP")
            ->push("4", "Not Joining PS")
            ->push("5", "Left SJP")
            ->push("6", "Active Recruit")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Status");

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->partner = Sub::factory("Partner", "partner_id");

        $this->background = Field::factory("background")
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->rbo = Field::factory("rbo")
            ->set_var(Field::TYPE, Field::CURRENCY)
            ->set(0);

        $this->hidden = Field::factory("hidden");

        $this->agencyCode = Field::factory("SJPAgencyCode")
            ->set_var(Field::PATTERN, '/\d{6}[A-Z]/')
            ->set_var(Field::MAX_LENGTH, 7);

        $this->previous_status = Field::factory("previous_status");

        # documents needed #
        $this->appointment_email = Boolean::factory("appointment_email")->set_var(Field::DISPLAY_NAME, "Appointment Email");
        $this->ps_contract = Boolean::factory("ps_contract")->set_var(Field::DISPLAY_NAME, "PS Contract");
        $this->client_list = Boolean::factory("client_list")->set_var(Field::DISPLAY_NAME, "Client List");
        $this->agency_letter = Boolean::factory("agency_letter")->set_var(Field::DISPLAY_NAME, "Agency Letter");
        $this->network_letter = Boolean::factory("network_letter")->set_var(Field::DISPLAY_NAME, "Network Letter");
        $this->client_letter = Boolean::factory("client_letter")->set_var(Field::DISPLAY_NAME, "Client Letter");
        $this->agency_codes = Boolean::factory("agency_codes")->set_var(Field::DISPLAY_NAME, "Agency Codes");
        $this->attended_insight = Boolean::factory("attended_insight")->set_var(Field::DISPLAY_NAME, "Attended Cirencester Insight Day ");
        $this->attended_wm = Boolean::factory("attended_wm")->set_var(Field::DISPLAY_NAME, "Attended WM@SJP");

        $this->SNAPSHOT_LOG = true;

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public static function new_ps_manager($id = "prospect[ps_manager]")
    {

        // Return a dropdown with relevant ps_managers for NEW recruits
        // Criteria: Group 4, Active staff, Department 4

        $db = new mydb();

        $db->query(
            "select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
            "inner join " . TBL_USERS_GROUPS . " " .
            "on " . TBL_USERS_GROUPS . ".user_id = " . USR_TBL . ".id " .
            "where group_id = '4' and active = '1' AND department = '4' " .
            "order by last_name, first_name"
        );

        ($id !== false) or $id = $this->get_field_name();

        return el::dropdown($id, $db);
    }

    public static function mandate_six($id)
    {
        $prospect = new Prospect($id, true);
        if ($prospect->transtype() == 1) {
            $six_months_ago = strtotime("-6 month");
            if ($six_months_ago > $prospect->addedwhen()) {
                return '<div class="notification static" style="display:block;">Recruit was added more than 6 months ago</div>';
            }
        }
    }

    public function __toString()
    {
        if (!$this->loaded) {
            $this->get();
        }

        if (!$this->id()) {
            return "";
        }

        // Mr. John Smith
        return "$this->title $this->firstname $this->surname";
    }

    public function get_document_fields()
    {
        //// @return array of document fields pertinent to the transfer type

        $array = [];

        switch ($this->transtype()) {
            //mandate
            case 1:
                $array['Appointment Email'] = $this->{'appointment_email'};
                $array['PS Contract'] = $this->{'ps_contract'};

                break;


            //agency single, agency bulk and the default
            default:
                $array['Attended Cirencester Insight Day'] = $this->{'attended_insight'};
                $array['Attended WM@SJP '] = $this->{'attended_wm'};
                $array['Appointment Email'] = $this->{'appointment_email'};
                $array['PS Contract'] = $this->{'ps_contract'};
                $array['Agency Letter'] = $this->{'agency_letter'};
                $array['Network Letter'] = $this->{'network_letter'};
                $array['Client Letter'] = $this->{'client_letter'};
                $array['Client List'] = $this->{'client_list'};
                $array['Agency Codes'] = $this->{'agency_codes'};

                break;
        }

        return $array;
    }

    public function on_delete()
    {

        // delete notes associated with prospect to be deleted
        try {
            $this->db_link->query("DELETE FROM feebase.notes WHERE object='prospect' AND object_id=" . ($this->id()));
        } catch (Exception $e) {
            throw $e;
        }
    }
}
