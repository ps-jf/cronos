<?php

class BSP extends DatabaseObject
{
    const TABLE = "tblpartnerbsp";
    const DB_NAME = DATA_DB;

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->sold_from = Sub::factory("Partner", "sold_from")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->sold_to = Sub::factory("Partner", "sold_to")
            ->set_var(Field::PARTNER_REF, true);

        $this->type = Choice::factory("type")
            ->push("1", "Full")
            ->push("2", "Partial")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "BSP Type");

        $this->bsp_date = Date::factory("bsp_date")
            ->set_var(Date::FORMAT, Date:: UNIX)
            ->set(time());

        $this->added_by = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->updated_by = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->updated_when = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->notes = Field::factory("notes");

        $this->last_chase = Date::factory("last_chase")
            ->set_var(Date::FORMAT, DATE::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, DATE::UK_STYLE_SHORT);

        $this->CAN_DELETE = true;
        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        return $this->sold_from . " to " . $this->sold_to;
    }

    public function save($return_query = false)
    {
        if(!empty($this->bsp_date())){
            $this->sold_to->set_var(Field::REQUIRED, true);
        }

        return parent::save($return_query);
    }

    public function checklistValue($aspect, $value = null)
    {
        $s = new Search(new BSPChecklist());

        $s->eq("bsp", $this->id());
        $s->eq("aspect", $aspect);

        if($c = $s->next(MYSQLI_ASSOC)){
            if(is_null($value)){
                // were getting the value
                return $c->value;
            } elseif($value != $c->value()) {
                // were setting the value
                $c->value($value);

                return $c->save();
            }
        } else {
            // nothing set, create new
            $c = new BSPChecklist();

            if(is_null($value)){
                // were getting the value
                return $c->value;
            } elseif($value != $c->value()) {
                $c->bsp($this->id());
                $c->aspect($aspect);
                $c->value($value);

                return $c->save();
            }
        }
    }

    public function checklistBy($aspect)
    {
        $s = new Search(new BSPChecklist());

        $s->eq("bsp", $this->id());
        $s->eq("aspect", $aspect);

        if($c = $s->next(MYSQLI_ASSOC)){
            return [
                "by" => $c->updatedby,
                "when" => $c->updatedwhen,
            ];
        }

        return false;
    }

    public function get_note($key)
    {
        if(!empty($this->notes())){
            $notes = json_decode($this->notes());

            if(!empty($notes->$key)){
                return $notes->$key;
            }
        }

        return "";
    }
}
