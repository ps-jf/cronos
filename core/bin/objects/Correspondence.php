<?php

class Correspondence extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblcorrespondencesql";
    const DEFAULT_MESSAGE = "We are pleased to enclose correspondence that we have received for the above client for your attention.";
    const PREMIUM_REMINDER_MESSAGE = "Please see below correspondence that we have received for your clients plan. Can you please contact your client and advise them of the premium that is due.";
    const LOA_MESSAGE = "Letter of Authority has been sent to provider";
    const COMP_REF = "Please find attached documentation regarding a complaint on this policy.";
    const COMP_REF_AR = "We have received a complaint regarding this policy transferred from yourselves. Can you please review and respond directly to the complainant.";
    const OMW_SERV_MESSAGE = "Please complete and return a scanned copy of the attached form to omwservicing@policyservices.co.uk in line with the instruction provided from OMW.";
    const AJBELL_AR_MESSAGE = "Please see attached negative account summary for your clients AJ Bell policy. We have checked on the AJ Bell website and this client will not have enough cash for ongoing charges, fee or income. For this to be rectified we will require the following: Execution only documents & data protection form which can be found on your myPSaccount as well as a copy of passport or driving licence for ID. If you have any further queries, please contact our email address below.";

    const LIST_LIMIT = 25;

    var $automatically_update_status = true; // set to "Servicing Transferred to PS" if below that status
    var $order = array(array("id", Search::ORDER_DESCEND));

    public function __construct($arg = false, $auto_get = false)
    {
        $this->id = Field::factory('CorrID', Field::PRIMARY_KEY);

        $this->date = Date::factory("dateStamp")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_FRIENDLY)
            ->set(time(), true, Date::UNIX);

        $this->client = Sub::factory("Client", 'Client')
            ->set_var(Field::REQUIRED, true);

        $this->partner = Sub::factory("Partner", 'Partner')
            ->set_var(Field::REQUIRED, true);

        $this->policy = Sub::factory("Policy", 'Policy')
            ->set_var(Field::REQUIRED, true);

        $this->subject = Field::factory('Subject')
            ->set_var(Field::REQUIRED, true);

        $this->message = Field::factory('Message')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE)
            ->set(Correspondence::DEFAULT_MESSAGE);

        $this->sender = Sub::factory("User", 'Sender')
            ->set_var(Field::REQUIRED, true)
            ->set(User::get_default_instance()->id());

        $this->letter = Sub::factory("Letter", "letter_type");

        $this->traditional_scan = Boolean::factory("traditional_scan");

        $this->processed_fes = Boolean::factory("processed_fes");

        $this->corrnotsend = Boolean::factory("CorrNotSend");

        $this->dragdropflag = Boolean::factory("dragdropflag");

        $this->ppp_predicted = Field::factory("ppp_predicted");

        // Passing a Policy object initiates a new item of correspondence
        if (is_a($arg, "Policy")) {
            if (!$arg->loaded) {
                $arg->get();
            }

            $this->policy($arg->id());
            $this->client($arg->client());
            $this->partner($arg->client->get_object()->partner());

            parent::__construct(false, $auto_get);

            if ($auto_get) {
                $this->get();
            }
        } else {
            // ... whereas an ID will reference an existing item
            $this->CAN_DELETE = true;

            parent::__construct($arg, $auto_get);
        }
    }

    public static function get_frontend_queues()
    {
        // get list of all queues
        $main_dir = scandir(CORRESPONDENCE_PENDING_SCAN_DIR);
        $main_dir = array_slice($main_dir, 2);
        $dir_array = [];
        foreach ($main_dir as $key => $dir) {
            if (strlen($dir) < 4) {
                $dir_array[] = $dir;
            }

        }
        sort($dir_array);

        $queue_select = '<select id="corr_queue" name="corr_queue"><option value="0">--</option>';

        foreach ($dir_array as $key => $value) {
            // Append 'flagged' queues
            if ($value == 99) {
                $text = "Flagged - Client Services";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 98) {
                $text = "Flagged - Acquisition Support";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 96) {
                $text = "Flagged - New Business";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 20) {
                $text = "Drawdown";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 21) {
                $text = "OMO";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 22) {
                $text = "NB Send outs";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 23) {
                $text = "NB online mail";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 13) {
                $text = "NB mail";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } else {
                // Get name of staff member queue is assigned to and append to dropdown
                $s = new Search(new CorrespondenceQueue);
                $s->eq('queue', $value);
                if ($user = $s->next(MYSQLI_ASSOC)) {
                    $assigned = $user->staff;
                    $queue_select .= '<option value="' . $value . '">' . $value . ' - ' . $assigned . '</option>';
                }
            }
        }

        $queue_select .= '</select>';

        return $queue_select;
    }

    public static function get_spotcheck_queues(){

        //get all spot check queues
        $sc_dir = scandir(CORRESPONDENCE_HOLDING_DIR);
        $sc_dir = array_slice($sc_dir, 2);
        $sc_dir_array = [];
        foreach ($sc_dir as $key => $dir){
            if (strlen($dir) < 4){
                $sc_dir_array[] = $dir;
            }
        }
        sort($sc_dir_array);

        $queue_select = '<select id="spot_check_corr_queue" name="spot_check_corr_queue"><option value="0">--</option>';

        foreach ($sc_dir_array as $key => $value) {
            // Append 'flagged' queues
            if ($value == 99) {
                $text = "Flagged - Client Services";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 98) {
                $text = "Flagged - Acquisition Support";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 96) {
                $text = "Flagged - New Business";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 20) {
                $text = "Drawdown";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 21) {
                $text = "OMO";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 22) {
                $text = "NB Send outs";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 23) {
                $text = "NB online mail";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 13) {
                $text = "NB mail";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } else {
                // Get name of staff member queue is assigned to and append to dropdown
                $s = new Search(new CorrespondenceQueue);
                $s->eq('queue', $value);
                if ($user = $s->next(MYSQLI_ASSOC)) {
                    $assigned = $user->staff;
                    $queue_select .= '<option value="' . $value . '">' . $value . ' - ' . $assigned . '</option>';
                }
            }
        }

        $queue_select .= '</select>';

        return $queue_select;
    }

    public static function corrSubjects($front_end, $action_required = true, $default = true, $searchOnly = false,
                                        $processOnly = false, $partnerID = false, $practiceID = false)
    {
        $s = new Search(new CorrespondenceSubject());

        $s->eq("archived", 0);

        if ($practiceID !== 2894) {
            // only show subject for PS Virtue Practice
            $s->nt("id", 147);
        }

        if ($searchOnly) {
            $s->eq("search", 1);
        }

        if ($processOnly) {
            $s->eq("process", 1);
        }

        $subjects = "";

        if ($default) {
            $subjects .= "<option value='default'>-- Please select --</option>";
        }

        $s->add_order("subject", "ASC");

        while ($sub = $s->next(MYSQLI_ASSOC)) {
            if ($action_required || !in_array($sub->id(), [3, 4])) {
                $subjects .= "<option id='".$sub->id()."'  " . (($sub->id() == 90 && $front_end) ? 'selected' : '') . ">" . $sub->subject . "</option>";
            }
        }

        return $subjects;
    }

    public function scan_exists($path = false, $upload_only = true)
    {
        if($upload_only){
            return file_exists(
                ($path)
                    ? $path
                    : $this->get_path($this->id, $upload_only)
            );
        } else {
            if (file_exists(($path) ? $path : $this->get_path($this->id))){
                return true;
            } else if (file_exists(($path) ? $path : $this->get_path($this->id, false))) {
                return "<i>pending</i>";
            } else {
                return false;
            }
        }
    }

    public function get_path($id = false, $upload_only = true)
    {
        /*
          Items are held in directory of 10,000 item
          ranges corresponding to the db ID.
          Determine which dir this item would sit in
          based on it's ID.
        */

        $base = 10000;
        $units;

        if (isset($this->id)) {
            $id = (int)"$this->id";
        }

        if (!$id) {
            return false;
        }

        // first directory
        if ($id < $base) {
            $p = "0-" . $base;
        } else {
            $units = (int)($id / $base);

            $p = (($id % $base) == 0)
                ? (($units * $base) - 9999) . "-" . ($units) . "0000"
                : $units . "0001-" . (($units * $base) + $base);
        }

        if($upload_only || $this->scan_exists(CORR_SCAN_DIR . "/$p/$id.pdf")){
            return CORR_SCAN_DIR . "/$p/$id.pdf";
        } elseif ($this->scan_exists(CORRESPONDENCE_PROCESSED_SCAN_DIR . "/$p/$id.pdf")){
            return CORRESPONDENCE_PROCESSED_SCAN_DIR . "/$p/$id.pdf";
        } elseif ($this->scan_exists(CORRESPONDENCE_PROCESSED_SCAN_DIR . "/$id.pdf")){
            return CORRESPONDENCE_PROCESSED_SCAN_DIR . "/$id.pdf";
        }

        return false;

        #uncomment line for when developing in windows
        //return CORR_SCAN_DIR."$id.pdf";
    }

    public function __toString()
    {
        $proreports = [
            "Portfolio Valuation",
            "Portfolio Valuation - Elevate Only",
            "Portfolio Valuation - Non PS",
            "Portfolio Valuation Amendment",
            "Portfolio Valuation Amendment - PS",
            "Portfolio Valuation Amendment - Partner",
            "Portfolio Valuation Amendment - Provider"
        ];

        $string = strval($this->subject);

        if (in_array($this->subject(), $proreports)){
            $s = new Search(new ProreportInvoice());
            $s->eq("corr", $this->id());

            if ($pi = $s->next(MYSQLI_ASSOC)){
                $options = [
                    "1" => $this->policy->client->one->name(),
                    "2" => $this->policy->client->two->name(),
                    "3" => "Joint",
                    "4" => "Trustee",
                    "5" => "Scheme"
                ];

                $clients = [];

                foreach(explode(",", $pi->client_option) as $option){
                    $clients[] = $options[$option];
                }

                $string .= " (" . implode(", ", $clients) . ")";
            }
        }

        return $string;
    }

    public function on_create($post = false)
    {
        /**The policy_status element is only in the $post variable when a
         * correspondence and policy object are both ran through 'common.php:do=create'.
         * Otherwise just grab the existing policy with the policy id that is passed through on other
         * correspondence adds **/

        if (!isset($post['policy_status'])) {
            $policy_status = $this->policy->status();
        } else {
            $policy_status = $post['policy_status'];
        }

        try {
            $omission_subjects = unserialize(CORR_STATUS_OMISSION);
            if (
                ($this->automatically_update_status && $policy_status < 2) &&
                (!in_array($post['subject'], $omission_subjects))
            ) {
                // If Policy is awaiting confirmation etc. update to "Servicing Transferred to PS"
                $policy = $this->policy->get_object();
                $policy->transferred(date("Y-m-d"));
                $policy->status(2);
                $policy->save();
            } else {
                if (isset($post['policy_status'])) {
                    $policy = $this->policy->get_object();
                    $policy->status($post['policy_status']);
                    $policy->save();
                }
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function get_frontend_queues_reassign()
    {
        // get list of all queues
        $main_dir = scandir(CORRESPONDENCE_PENDING_SCAN_DIR);
        $main_dir = array_slice($main_dir, 2);

        $dir_array = [];
        foreach ($main_dir as $key => $dir) {
            if (strlen($dir) < 4) {
                $dir_array[] = $dir;
            }

        }
        sort($dir_array);

        $queue_select = '<select id="reassign_queue" name="reassign_queue"><option value="0">--</option>';
        foreach ($dir_array as $key => $value) {
            // Append 'flagged' queues
            if ($value == 99) {
                $text = "Flagged - Client Services";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 98) {
                $text = "Flagged - Acquisition Support";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 96) {
                $text = "Flagged - New Business";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 20) {
                $text = "Drawdown";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 21) {
                $text = "OMO";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 22) {
                $text = "NB Send outs";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 23) {
                $text = "NB online mail";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } elseif ($value == 13) {
                $text = "NB mail";
                $queue_select .= '<option value="' . $value . '">' . $text . '</option>';
            } else {
                // Get name of staff member queue is assigned to and append to dropdown
                $s = new Search(new CorrespondenceQueue);
                $s->eq('queue', $value);
                if ($user = $s->next(MYSQLI_ASSOC)) {
                    $assigned = $user->staff;
                    $queue_select .= '<option value="' . $value . '">' . $value . ' - ' . $assigned . '</option>';
                }
            }

        }
        $queue_select .= '</select>';

        return $queue_select;
    }

    public static function correspondence_count($user_id)
    {
        // Count the number of correspondence items added by this user today
        $s = new Search(new Correspondence);
        $s->eq("sender", $user_id);
        $s->eq("date", date("Y-m-d") . "%", true);
        $corr_count = $s->count();

        return $corr_count;
    }

    public static function motivational_image()
    {
        // get directory which includes motivational images
        $dir = str_replace("//", "/", $_SERVER['DOCUMENT_ROOT'] . CORR_MOTIVATION);

        // check directory exists
        if (file_exists($dir)) {
            // only get files of set extensions
            $images = glob($dir . '*.{jpg, jpeg, png, gif}', GLOB_BRACE);

            // make sure we have at least one image
            if (!empty($images)) {
                // give images a little shuffle
                shuffle($images);

                // select a single image
                $randomImage = array_pop($images);

                // ensure we still have image
                if ($randomImage) {
                    // get the part of the source url that we need
                    $arr = explode('public', $randomImage);
                    $randomImage = $arr[1];

                    // give user a motivational image!
                    $return = "<img style='height:400px; max-width: 550px;' src='$randomImage' />";
                } else {
                    $return = "Contact IT, random image not selected";
                }
            } else {
                $return = "Contact IT, Motivational image array is empty";
            }
        } else {
            $return = "Contact IT, Motivational directory does not exist";
        }

        return $return;
    }
}
