<?php

class MyPsNotification extends DatabaseObject
{
    const DB_NAME = 'myps';
    const TABLE = "notifications";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->partner = Sub::factory("Partner", "partnerID")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::PARTNER_REF, true);

        $this->title = Field::factory("title");

        $this->text = Field::factory("text");

        $this->myps_link = Field::factory("link");

        $this->added_by = Sub::factory("User", "added_by")
            ->set(User::get_default_instance("myps_user_id"));

        $this->added_when = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set(time());

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }
}