<?php

/**
 * List of regexps that can be used to determine issuer policy types.
 *
 *
 * This is me just noting down the regexes for different providers
 * Im going to try to be as minimalist as possible,
 * but this still might produce 3 or 4 regexes per provider(maybe more)
 * Syntax will probably be a multidimensional array for each provider,
 * numerical key with regex, description and examples within the next array
 * this is going to hurt performance. ouch
 * NB not used as of yet
 *
 *
 * @package prophet.variables
 * @author kevin.dorrian
 *
 */

// Jupiter
$rexep_127 = array(
                    array("^\d{6,7}$", "6,7 numbers","123456"),
                    array("^\d{10}$", "10 numbers","1234567890")
                  );

// Lincoln
$rexep_133 = array(
                    array("^\d{3}-\d{6}-\d{2}$", "3 numbers-dash-6 numbers-dash-2 numbers","123-123456-12")
                  );


// Aviva
$rexep_177 = array(
                    array("^\d{6,8}[a-zA-Z]{1,2}$", "6,7,8 numbers followed by 1,2 letters","123456A"),
                    []
                  );

// Pegasus
$rexep_186 = array(
                    array("^\d{6}$", "6 numbers","123456"),
                    array("^\d{10}$", "10 numbers","1234567890")
                  );
                  
// Scottish Life Investments
$rexep_219 = array(
                    array("^[a-zA-Z]{1}\d{7}$", "1 letters-7 numbers","A1234567")
                  );
                  
// Skandia Life Assurance Co ltd
$rexep_225 = array(
                    array("^[a-zA-Z]{3}\d{9}$", "3 letters-9 numbers","ABC123456789")
                  );
                  
// Sterling Assurance Plc
$rexep_234 = array(
                    array("^[a-zA-Z]-\d{6}$", "1 letter-dash-6 numbers","A-123456")
                  );
                  
// Tunbridge Wells Equitable
$rexep_250 = array(
                    array("^\d{7,10}$", "7,8,9 or 10 numbers","1234567")
                  );
                  
// Zurich Intermediary Group
$rexep_1030 = array(
                    array("^\d{8}$", "8 numbers","12345678")
                  );
                  
// Liverpool Victoria
$rexep_1031 = array(
                    array("^\d{7}-01-1A$", "7 numbers-dash-01-dash-1A","1234567-01-1A")
                  );

// Payment shield
$rexep_1124 = array(
                    array("^[a-zA-Z]{2,3}\d{7,8}$", "2,3 letters-7,8 numbers","AB1234567")
                  );
                  
// Co funds
$rexep_1152 = array(
                    array("^\d{5,6}$", "5,6 numbers","12345")
                  );
