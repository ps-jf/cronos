<?php

/**
 * Outside vendor class - used to create line charts easily.
 *
 *
 * @package prophet.helpers
 * @author anon
 *
 */

class line_chart
{
//--------------------------------------------
// variables 
//--------------------------------------------
    //Chart labels default
    var $chart_title    = "Chart";
    var $x_label        = "X";
    var $y_label        = "Y";
    
    //Chart dimensions default
    var $chart_width     = 300;
    var $chart_height    = 200;
    
    //Chart perimeter default;
    var $chart_perimeter = 50;

    //Plot data array
    var $data_array      = [];
    
    //Color data array
    var $color          = [];
    
    // band information
    var $band           = array(0 => 500,1 => 3000,2 => 7500,3 => 12500,4 => 20000,5 => 37500,
    6 => 62500,7 => 87500,8 => 125000,9 => 175000,10 => 225000,11 => 275000,12 => 300000 );
    
    //These variables will be set as the program runs
    var $chart;

    var $font_width;
    var $font_height;
    
    var $y_axis_values = [];
    var $mod;
//--------------------------------------------
// set chart dimensions
// Sets dimension of chart. Defaulted previously.
//--------------------------------------------
    public function set_chart_dimensions($width, $height)
    {
        $this->chart_width  = $width;
        $this->chart_height = $height;
    }
//--------------------------------------------
// set chart perimeter
// Sets perimeter width. Defaulted previously
//--------------------------------------------
    public function set_chart_perimeter($perimeter)
    {
        $this->perimeter = $perimeter;
    }
//--------------------------------------------
// set labels
// Sets chart labels.
//--------------------------------------------
    public function set_labels($title, $xLabel, $yLabel)
    {
        $this->chart_title = $title;
        $this->x_label     = $xLabel;
        $this->y_label     = $yLabel;
    }
//--------------------------------------------
// add line
// Passes an array of information to data_array. 
//--------------------------------------------	
    public function add_line($new_data)
    {
        $this->data_array[] = $new_data;
    }

//--------------------------------------------
// prepare canvas
//--------------------------------------------	
    private function prepare_canvas()
    {
        //this->chart is image variable
        $this->chart = imagecreatetruecolor($this->chart_width, $this->chart_height);
    
        
        /*
		Manual colour import
		*/
        $this->color[1]     = imagecolorallocate($this->chart, 255, 255, 255);  //white
        $this->color[2]     = imagecolorallocate($this->chart, 0, 0, 0);          //black
        $this->color[3]    = imagecolorallocate($this->chart, 248, 255, 190);   //yellow
        $this->color[4]      = imagecolorallocate($this->chart, 3, 12, 94);           //blue
        $this->color[5]      = imagecolorallocate($this->chart, 102, 102, 102);     //grey
        $this->color[6] = imagecolorallocate($this->chart, 216, 216, 216);  //lightGrey
        $this->color[7]   = imagecolorallocate($this->chart, 255, 0, 255);      //magenta
        $this->color[8]   = imagecolorallocate($this->chart, 0, 255, 255);      //cyan
        
    
        
        //For font position adjusting
        $this->font_width  = imagefontwidth(2);
        $this->font_height = imagefontheight(2);
        
        //Fills canvas with colour
        imagefill($this->chart, 0, 0, $this->color[6]);
    }
//--------------------------------------------
// get min and max y values 
//--------------------------------------------	
    private function calculate_min_and_max_y_values()
    {
        for ($i=0; $i<count($this->data_array); $i++) {
            $current_line = $this->data_array[$i];
            if (min($current_line)>$y_min) {
                $y_min = min($current_line);
            }
            if (max($current_line)>$y_max) {
                $y_max = max($current_line);
            }
        }
        //min
        $places = strlen($y_min);
        $mod    = pow(10, $places-1);
        $y_min    = $mod - $y_min;
        if ($y_min<0) {
            $y_min = 0;
        }
        
        //max
        $places = strlen($y_max);
        $mod    = pow(10, $places-1);
        $y_max  = $mod + $y_max;
        
        $this->mod = $mod;
        
        
        $this->y_axis_values['min'] = $y_min;
        $this->y_axis_values['max'] = $y_max;
    }

//--------------------------------------------
// prepare grid
//--------------------------------------------	
    private function prepare_grid()
    {
        
        //The grid width. We don't want the grid to be the same size as the chart otherwise the entire
        //document will be just one grid with no place for labels.
        // Therefore, subtract the perimeter width from all sides.
        $gridWidth  = $this->chart_width  - ($this->chart_perimeter*2);
        $gridHeight = $this->chart_height - ($this->chart_perimeter*2);
    
        //Coordinates for chart perimeter
        $this->c1 = array("x"=>$this->chart_perimeter,            "y"=>$this->chart_perimeter);
        $this->c2 = array("x"=>$this->chart_perimeter+$gridWidth, "y"=>$this->chart_perimeter);
        $this->c3 = array("x"=>$this->chart_perimeter+$gridWidth, "y"=>$this->chart_perimeter+$gridHeight);
        $this->c4 = array("x"=>$this->chart_perimeter,            "y"=>$this->chart_perimeter+$gridHeight);
    
    
        //now that I've made the grid coordinates its time to connect the dots and create a grid plane.
        //In this function I'm creating a solid white rectangle
        //show image to demonstrate
        imagefilledrectangle(
            $this->chart,
            $this->c1['x'],
            $this->c1['y'],
            $this->c3['x'],
            $this->c3['y'],
            $this->color[1]
        );
    
        
        //finding the size of the grid squares
        
        //getting the biggest array
        //now I need to find the highest x-value but unlike the y-value that I found earlier, I'm not trying to find
        //the highest data value but rather the highest count value.
        //To put it simply, which array has the most entries?
        $biggest_array = [];
        for ($i=0; $i<count($this->data_array); $i++) {
            if (count($biggest_array) < count($this->data_array[$i])) {
                $biggest_array = $this->data_array[$i];
            }
        }
        
        
        //Finding grid unit width and height.
        //I'm going to increment by grid units so I need to find what the width and
        // height of that grid unit is (in pixels)
        $this->square['w'] = $gridWidth/count($biggest_array);
        $this->square['h'] = $gridHeight/$this->y_axis_values['max'];
        
        
        //create image to demonstrate.
        $this->vertical_padding = $this->square['w']/2;
        
        
        //------------------------------------------------
        //drawing the vertical lines and axis labels
        //------------------------------------------------
        foreach ($biggest_array as $assoc => $value) {
            //drawing the line
            imageline(
                $this->chart,
                $this->vertical_padding+$this->c4['x']+$increment,
                $this->c4['y'],
                $this->vertical_padding+$this->c1['x']+$increment,
                $this->c1['y'],
                $this->color[2]
            );
            
            //axis values
            //finding the width of the word so that we can accuratly position it
            $wordWidth = strlen($assoc)*$this->font_width;
            
            //the x-position of the line, this will be incremented on every loop
            $xPos = $this->c4['x']+$increment+$this->vertical_padding-($wordWidth/2);
            
            //creating the axis value label
            ImageString($this->chart, 2, $xPos, $this->c4['y'], $assoc, $this->color[2]);
            
            
            $increment += $this->square['w'];
        }
        
        
        //------------------------------------------------
        //drawing the horizontel lines and axis labels
        //------------------------------------------------
        //resetting the increment back to 0
        $increment = 0;
        for ($i=$this->y_axis_values['min']; $i<$this->y_axis_values['max']; $i++) {
            //main lines
            //often the y-values can be in the thousands, if this is the case then we don't want to draw every single
            //line so we need to make sure that a line is only drawn every 50 or 100 units.
            if ($i%$this->mod==0) {
                //drawing the line
                imageline(
                    $this->chart,
                    $this->c4['x'],
                    $this->c4['y']+$increment,
                    $this->c3['x'],
                    $this->c3['y']+$increment,
                    $this->color[5]
                );
                
                //axis values
                $xPos = $this->c1['x']-($this->font_width*strlen($i))-5;
                ImageString(
                    $this->chart,
                    2,
                    $xPos,
                    $this->c4['y']+$increment-($this->font_height/2),
                    $i,
                    $this->color[2]
                );
            } //tics
            //these are the smaller lines between the longer, main lines.
            elseif (($this->mod/5)>1 && $i%($this->mod/5)==0) {
                imageline(
                    $this->chart,
                    $this->c4['x'],
                    $this->c4['y']+$increment,
                    $this->c4['x']+10,
                    $this->c4['y']+$increment,
                    $this->color[5]
                );
            }
            $increment-=$this->square['h'];
        }

        
        //------------------------------------------------
        //creating the left line
        //------------------------------------------------
        imageline($this->chart, $this->c1['x'], $this->c1['y'], $this->c4['x'], $this->c4['y'], $this->color[2]);
    }
//--------------------------------------------
// draw chart labels 
//--------------------------------------------	
    private function draw_chart_labels()
    {
        //------------------------------------------------
        // Making the graph labels
        //------------------------------------------------
        //Graph Title
        ImageString(
            $this->chart,
            2,
            ($this->chart_width/2)-(strlen($this->chart_title)*$this->font_width)/2,
            $this->c1['y']-($this->chart_perimeter/2),
            $this->chart_title,
            $this->color['green']
        );

        //X-Axis
        ImageString(
            $this->chart,
            2,
            ($this->chart_width/2)-(strlen($this->x_label)*$this->font_width)/2,
            $this->c4['y']+($this->chart_perimeter/2),
            $this->x_label,
            $this->color['green']
        );

        //Y-Axis
        ImageStringUp(
            $this->chart,
            2,
            $this->c1['x']-$this->font_height*3,
            $this->chart_height/2+(strlen($this->y_label)*$this->font_width)/2,
            $this->y_label,
            $this->color['green']
        );
    }
//--------------------------------------------
// plot lines
//--------------------------------------------	
    private function plot_lines()
    {
        //------------------------------------------------
        // Making chart lines
        //------------------------------------------------
        $increment = 0;         //resetting the increment value
        $j = 8;
        //looping through each array of data (in this case there is only one set of data
        //but later you'll be able to add more lines)
        for ($i=0; $i<count($this->data_array); $i++) {
            $line = $this->data_array[$i];
            //getting the first value
            $firstVal = end(array_reverse($line));
        
            $increment = 0;
            //getting the first point for your line
            $point1 = array($this->c4['x']+$increment+$this->vertical_padding,
                $this->c4['y']-($firstVal*$this->square['h']));
            
            //looping through your current array of data
            foreach ($line as $assoc => $value) {
                //getting the second point for your line
                $yPos   = $this->c4['y']-($value*$this->square['h']);
                $xPos   = $this->c4['x']+$increment+$this->vertical_padding;
                $point2 = array($xPos, $yPos);
                
                //drawing your line
                imageline($this->chart, $point1[0], $point1[1], $point2[0], $point2[1], $this->color[$j]);
                imagefilledellipse($this->chart, $point1[0], $point1[1], 5, 5, $this->color[$j]);
                
                //point1 becomes point2
                $point1 = $point2;
                
                //increment to the next x position
                $increment += $this->square['w'];
            }
            $j--;
        }
    }
//--------------------------------------------
// output 
//--------------------------------------------	
    private function output()
    {
        //sets the type of output (in this case a png image)
        header("Content-type: image/png");
        imagepng($this->chart);
        
        //after output it removes the image from the buffer to free up memory
        imagedestroy($this->chart);
    }
//--------------------------------------------
// plot
//--------------------------------------------	
    public function plot()
    {
        $this->prepare_canvas();
        $this->calculate_min_and_max_y_values();
        $this->prepare_grid();
        $this->draw_chart_labels();
        $this->plot_lines();
        
        $this->output();
    }
}//end of class
