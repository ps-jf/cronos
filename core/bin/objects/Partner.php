<?php

use Carbon\Carbon;

class Partner extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblpartner";
    var $_profilePath = "/pages/partner/";

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("partnerID", Field::PRIMARY_KEY);

        $this->title = Title::factory("partnerTitle");
        $this->forename = Field::factory("partnerFname");
        $this->surname = Field::factory("partnerSname")
            ->set_var(Field::REQUIRED, true);

        $this->dob = Date::factory("PartnerDOB")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->agencyCode = Field::factory("SJPAgencyCode")
            ->set_var(Field::PATTERN, '/\d{6}[A-Z]/')
            ->set_var(Field::MAX_LENGTH, 7);

        $this->entered = Date::factory("partnerEntered")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Date::BLOCK_UPDATE, true)
            ->set(time());

        $this->signed = Date::factory("partnerSigned")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        $this->review = Date::factory("accountReviewDate")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->agreed = Date::factory("partnerContractAgreed")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT);

        //-------------  Bank Details  ---------------//

        $this->sortcode = Field::factory("partnerSortcode")
            //->set_var( Field::TYPE, Field::INTEGER )
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::MAX_LENGTH, 6)
            ->set_var(Field::PATTERN, "/^\d{6}$/");

        $this->account_num = Field::factory("partnerAccountNo")
            //->set_var( Field::TYPE, Field::INTEGER )
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::MAX_LENGTH, 8)
            ->set_var(Field::PATTERN, "/^\d{8}$/");

        $this->account_name = Field::factory("partnerAccountName")
            ->set_var(Field::MAX_LENGTH, 50);

        //-----------------------------------------------//

        $this->branch = Choice::factory("branchID")
            ->setSource(BRANCH_TBL, "branchID", "branch")
            ->set_var(Field::REQUIRED, true);

        $this->acctype = Choice::factory("type")
            ->push_array(["Sole Trader" => "Sole Trader", "Partnership" => "Partnership", "Limited Company" => "Limited Company",
                    "Limited Partnership" => "Limited Partnership",
                    "Limited Liability Partnership" => "Limited Liability Partnership",
                    "Sales Adviser" => "Sales Adviser", "Main" => "Main", "Sub" => "Sub"]);

        $this->current = Choice::factory("currentPartner")
            ->setSource(DATA_DB . ".tblcurrentpartner", "currentPartnerID", "description");

        $this->transtype = Choice::factory("trans_type")
            ->push("1", "Mandate")
            ->push("2", "Agency- Single")
            ->push("3", "Agency- Agency")
            ->push("4", "Agency- Mandate")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Transfer Type");
        //$this->transtype(1);


        $this->bulk = Boolean::factory("bulkPartner")
            ->set_values(-1, 0);

        $this->exempt = Boolean::factory("exemptSJPRate");

        $this->address = Address::factory(
            "partnerAdd1",
            "partnerAdd2",
            "partnerAdd3",
            "partnerPostcode"
        );

        $this->bulk_rate = Field::factory("partnerBulkRate")
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->single_rate = Field::factory("partnerCurrentRate")
            ->set_var(Field::TYPE, Field::PERCENT)
            ->set_var(Field::REQUIRED, true)
            ->set(60);

        $this->sjpsinglerate = Field::factory("sjpCurrent")
            ->set_var(Field::TYPE, Field::FLOAT);

        $this->sjpbulkrate = Field::factory("sjpBulk")
            ->set_var(Field::TYPE, Field::FLOAT);

        $this->phone = Field::factory("partnerPhone")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->phonecsrd = Field::factory("phone_csrd")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->fax = Field::factory("partnerFax")
            ->set_var(Field::PATTERN, Patterns::TELEPHONE_NUM);

        $this->email = Field::factory("partnerEmail")
            ->set_var(Field::MAX_LENGTH, 255)
            ->set_var(Field::PATTERN, Patterns::EMAIL_ADDRESS);

        $this->mobile = Field::factory("partnerMobile")
            ->set_var(Field::PATTERN, Patterns::MOBILE_PHONE);

        $this->manager = Choice::factory("ps_manager")
            ->setSource(
                "select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "inner join " . TBL_USERS_GROUPS . " " .
                "on " . TBL_USERS_GROUPS . ".user_id = " . USR_TBL . ".id " .
                "where group_id = '4' " .
                "order by last_name, first_name"
            );

        $this->is_closed = Boolean::factory("is_closed");

        $this->rbo = Field::factory("rboPartner")
            ->set_var(Field::TYPE, Field::CURRENCY)
            ->set(0);

        $this->holdcomm = Boolean::factory("hold_comm")
            ->set_values(-1, 0);

        $this->renewals = Field::factory("expectedRenewals")
            ->set_var(Field::TYPE, Field::CURRENCY);

        $this->addressee = Field::factory("addressee")
            ->set_var(Field::MAX_LENGTH, 100);

        $this->addresseecsrd = Field::factory("addressee_csrd")
            ->set_var(Field::MAX_LENGTH, 100);

        $this->band = Choice::factory("band")
            ->push_array(range(0, 12));
        $this->band(1);

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->unipass = Boolean::factory("unipass");

        //$this->reviewable = Boolean::factory( "reviewable_contract" );

        $this->reviewable = Choice::factory("reviewable_contract")
            ->push("1", "Reviewable - 2009 Tariff")
            ->push("2", "Reviewable - 2013 Tariff")
            ->push("3", "Non-Reviewable")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::DISPLAY_NAME, "Reviewable Contract");

        $this->lastchase = Date::factory("lastchasedate")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->previous_status = Field::factory("previous_status");

        // Flag set to true/false - used in Acquisition System to determine whether or not to show agency codes for
        // single account or for all accounts within practice
        $this->practice_bubble = Boolean::factory("practice_bubble");

        $this->top20percent = Boolean::factory("top20percent");

        $this->ac_status = Choice::factory("ac_status")
            ->push("1", "No Contact Yet")
            ->push("2", "Future Intention")
            ->push("3", "No Intention")
            ->push("4", "Partner Submitting Packs")
            ->push("5", "Partner 100% Completed Adviser Charging Exercise")
            ->set_var(Field::DISPLAY_NAME, "Adviser Charging Status");

        $this->csrd_free = Boolean::factory("csrd_free");

        $this->admin_manager = Choice::factory("ps_manager_admin")
            ->setSource(
                "select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name) from " . USR_TBL . " " .
                "inner join " . TBL_USERS_GROUPS . " " .
                "on " . TBL_USERS_GROUPS . ".user_id = " . USR_TBL . ".id " .
                "where group_id = '4' " .
                "order by last_name, first_name"
            );

        $this->sjp_btm = Choice::factory("btm")
            ->setSource(DATA_DB . ".tblbustransman", "id", "name");

        $this->sjp_manager1 = Choice::factory("bam")
            ->setSource(DATA_DB . ".tblbusacqman", "id", "name");

        $this->practice_director = Boolean::factory("practice_director");

        $this->deceased = Boolean::factory("deceased");

        $this->deceased_date = Date::factory("deceased_date")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Partner Deceased Date");

        $this->request_pro_report = Boolean::factory("request_portfolio_report");
        // 1 if we produce report, 0 if they do their own

        $this->client_income_hold_days = Field::factory("client_income_hold_days");

        $this->mailing_exempt = Boolean::factory("mailing_exempt")
            ->set(0);

        $this->servicing_chase_exemption = Date::factory("servicing_chase_exemption")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT)
            ->set_var(Field::DISPLAY_NAME, "Servicing Chase Exemption");

        $this->SNAPSHOT_LOG = true;

        parent::__construct($id, $autoget);
    }

    public function new_ps_manager($id = "partner[manager]")
    {
        // Return a dropdown with relevant ps_managers for NEW partners
        // Criteria: Group 4, Active staff, Not IT

        $db = new mydb();

        $db->query(
            "select " . USR_TBL . ".id, CONCAT(first_name, ' ', last_name)
			  from " . USR_TBL . " " .
            "inner join " . TBL_USERS_GROUPS . " " .
            "on " . TBL_USERS_GROUPS . ".user_id = " . USR_TBL . ".id " .
            "where group_id = '4' and active = '1' and department != '7'" .
            "order by last_name, first_name"
        );

        ($id !== false) or $id = $this->get_field_name();

        return el::dropdown($id, $db);
    }

    public static function practiceDirector($partner_id)
    {
        //$partner = new Partner($partner_id, true);
        $partner_array = [];
        $partner_link_array = [];

        $search = new Search(new PracticeStaffEntry());
        $search->eq('partner', $partner_id);
        if ($x = $search->next(MYSQLI_OBJECT)) {

            $search = new Search(new PracticeStaffEntry());
            $search->eq('practice', $x->practice());
            while ($y = $search->next(MYSQLI_OBJECT)) {
                $partner_array[] = $y->partner();
            }
        }

        if (!empty($partner_array)) {
            foreach ($partner_array as $p) {
                $partner = new Partner($p, true);
                if ($partner->practice_director()) {
                    $partner_link_array[] = $partner->link();
                }
            }
        }

        if (!empty($partner_link_array)) {
            $span = "<div class='alert alert-info'>
                        <i class='fas fa-info-circle'></i>
                        Practice directors: ".implode(', ', $partner_link_array)."
                    </div>";
        }

        if (isset($span)) {
            return $span;
        }
    }

    public static function pv_contact_span($partner_id)
    {
        $db = new mydb();

        $db->query("select user_id FROM feebase.pv_allocation WHERE partner_id = " . $partner_id);

        if ($c = $db->next(MYSQLI_ASSOC)) {
            $staff = new User($c['user_id'], true);
            if ($staff) {
                $key_contact = $staff->forename . " " . $staff->surname;
                $span = "<div class='alert alert-info'> <i class=\"fa fa-book\" aria-hidden=\"true\"></i>  Portfolio Valuation Key Contact: " . $staff->link($key_contact) . "</div>";
            }
        }

        if (isset($span)) {
            return $span;
        }
    }

    public function __toString()
    {
        if (!$this->loaded) {
            $this->get();
        }

        if (!$this->id()) {
            return "";
        }

        // Mr. John Smith (2272)
        return "$this->title $this->forename $this->surname ( $this->id )";
    }

    public function link($text = false)
    {
        $link = parent::link($text);

        if ($this->is_closed()) {
            $link = "<img src='/lib/images/cross.png' />" . preg_replace("/(class='.*?)(')/", "$1 closed$2", $link);
        }

        return $link;
    }

    public function clients($attribute = false)
    {
        // return all partners clients
        $clients = [];

        $s = new Search(new Client());
        $s->eq("partner", $this->id());

        while($c = $s->next(MYSQLI_ASSOC)){
            if($attribute){
                $clients[] = strval($c->{$attribute});
            } else {
                $clients[] = $c;
            }
        }

        return $clients;
    }

    public function save($return_query = false)
    {
        if ($this->id()){
            // object is being updated
            $redisClient = RedisConnection::connect();

            if ($redisClient->get("incomerun")){
                // income run is currently being processed
                $p = new Partner($this->id(), true);

                // p is the unedited partner
                if ((isset($_REQUEST['partner']['sortcode']) && $_REQUEST['partner']['sortcode'] != $p->sortcode()) ||
                    (isset($_REQUEST['partner']['account_num']) && $_REQUEST['partner']['account_num'] != $p->account_num()) ||
                    (isset($_REQUEST['partner']['account_name']) && $_REQUEST['partner']['account_name'] != $p->account_name())){
                    throw new Exception("Partner bank details cannot be updated whilst the income run is being processed.");

                    return false;
                }
            }
        } else {
            // object is being created
            if(empty($_REQUEST['partner']['practice'])){
                throw new Exception("Partner Practice required");

                return false;
            }
        }

        return parent::save($return_query);
    }

    public function on_update($post = false)
    {
        /*As of April 2015 we have started using feebase.partnerrate_audit to audit changes in the partner rates*/

        //get partner object to check if bulk rate has been updated
        $p = new Partner($_GET['id'], true);
        $old_rate = $p->bulk_rate();
        $trans_type_check = $p->transtype();
        $current_status_check = $p->current();

        //if bulk rate has been updated we want to audit this
        if ($old_rate != $post['bulk_rate']) {
            //gets the partner gri (same way account review system does this)
            $db1 = new mydb();
            $q = "SELECT SUM(amount)-SUM(vat) AS gri
                        FROM tblpartner
                        INNER JOIN practice_staff
                          ON tblpartner.partnerID = practice_staff.partner_id
                        INNER JOIN practice
                          ON practice_staff.practice_id = practice.id
                        INNER JOIN tblcommissionauditsql
                          ON tblpartner.partnerID = tblcommissionauditsql.partnerid
                        WHERE tblpartner.partnerID = " . $_GET['id'] . "
                          AND commntype IN " . CommissionType::griTypes() . "
                          AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)
                          AND practice.id != 2894
                          AND tblpartner.is_closed != 1";

            $db1->query($q);

            if ($row = $db1->next(MYSQLI_ASSOC)) {
                $gri = number_format((float)$row['gri'], 2, '.', '');

                //audit rate changed
                $pra = new PartnerRateAudit();
                $pra->partner($_GET['id']);
                $pra->monthsum_12($gri);
                $pra->oldrate($old_rate);
                $pra->newrate($post['bulk_rate']);
                $pra->date(date("Y-m-d", time()));
                $pra->save();
            }
        }

        //if trans type has been already added we want to audit this
        if ($trans_type_check != $post['transtype']) {
            //if trans type is changed from mandate to any other status
            if ($trans_type_check == 1 && $post['transtype'] != 1) {
                //gets the bulk details for that partner
                $db1 = new mydb();
                $q = " SELECT * FROM feebase.bulk_details
                       where partnerID = " . $_GET['id'] . " ";

                $db1->query($q);
                //check if data entry exists in the bulk_details table
                //If yes then return null
                //If no add a new entry into the table
                if ($row = $db1->next(MYSQLI_ASSOC)) {
                    return;
                } else {
                    // Add details into table bulk_details table
                    $ifa_auth = $db1->real_escape_string("$this->forename $this->surname\IFA Letter.pdf");
                    $net_auth = $db1->real_escape_string("$this->forename $this->surname\Network Letter.pdf");
                    $sub_agency = $db1->real_escape_string("$this->forename $this->surname");

                    $db1->query("INSERT INTO " . BULK_DETAILS_TBL . " (`ifa_auth`, `network_auth`, `agency_called`,`PartnerID`)
							 VALUES('" . $ifa_auth . "', '" . $net_auth . "', '" . $sub_agency . "'," . $_GET['id'] . ")");
                }
            }
        }

        if (isset($post["practice"]) && ctype_digit($post["practice"])) {
            //// Assign Partner to practice
            try {
                $db = new mydb();
                // remove existing practice link
                $db->query("delete from " . PRACTICE_STAFF_TBL . " where partner_id = " . $this->id());
                // insert new link
                $db->query("insert into " . PRACTICE_STAFF_TBL . " set partner_id = " . $this->id() . ", practice_id = $post[practice]");
            } catch (Exception $e) {
                throw $e;
            }
        }

        if (isset($post["current"])) {
            if ($post["current"] != $current_status_check) {

                // Statuse - "Left SJP", "Left SJP - Letter Sent"
                $statuses = ["2", "16"];
                if (in_array($post["current"], $statuses, true)) {

                    try {
                        $db = new mydb();

                        $db->query("select user_id FROM feebase.pv_allocation WHERE partner_id = " . $this->id());

                        if ($c = $db->next(MYSQLI_ASSOC)) {

                            $staff = new User($c['user_id'], true);
                            if ($staff) {

                                //build up data for the json being passed through to the email
                                $staff_email = $staff->email();
                                $staff_name = $staff->forename();
                                $partner_name = $this->title() . " " . $this->forename() . " " . $this->surname();

                                $receiver = ['name' => $staff_name, 'email' => $staff_email];
                                $sender = ['name' => "IT", 'email' => "it@policyservices.co.uk"];

                                $body = json_encode(["PARTNERNAME" => $partner_name . ' (' . $this->id() . ')',
                                    "FIRSTNAME" => $staff_name,
                                    "YEAR" => date("Y")
                                ]);


                                if (sendSparkEmail($receiver, "Portfolio Review - Partner Left", 'portfolio-partner-left', $body, $sender)) {
                                    $json['status'] = " An email has been sent to " . $staff_email . " with regards to account: " . $partner_name . " as they are the registered Portfolio Expert for this partner.";
                                    $json['error'] = false;
                                }
                            }
                        }


                    } catch (Exception $e) {
                        throw $e;
                    }
                }
            }
        }

        if (isset($post["is_closed"]) && $post["is_closed"] && $post["is_closed"] != $p->is_closed()){
            $s = new Search(new WebsiteUser());

            $s->eq("partner", $p->id());

            while($wu = $s->next(MYSQLI_ASSOC)){
                $s2 = new Search(new ViewerEntry());

                $s2->eq("user", $wu->id());

                if($ve = $s2->next(MYSQLI_ASSOC)){
                    // user still has access to other partners, just unset their partner ID
                    $wu->partner("");
                } else {
                    // user doesnt have access to any other account, revoke access entirely
                    $wu->allowed_access(0);
                }

                $wu->save();
            }

            $s = new Search(new ViewerEntry());

            $s->eq("partner", $p->id());

            while($ve = $s->next(MYSQLI_ASSOC)){
                $ve->access_revoked(1);

                $ve->save();
            }
        }
    }

    public function on_create($post = false)
    {
        try {
            if (isset($post["practice"]) && ctype_digit($post["practice"])) {
                // Assign Partner to practice
                $db = new mydb();
                $db->query("insert into " . PRACTICE_STAFF_TBL . " set partner_id = " . $this->id() . ", practice_id = $post[practice]");
            }

            // Check if partner being added is a bulk and add the appropriate bulk client and workflow ticket
            if ($post["transtype"] != 1) {
                // Create new bulk Client
                $c = new Client;
                $c->partner($this->id());
                $c->one->surname("*Agency: ");
                $c->bulk(true);
                $c->addedhow(1);
                $c->save();


                // Add usual details into table bulk_details
                $ifa_auth = $db->real_escape_string("$this->forename $this->surname\IFA Letter.pdf");
                $net_auth = $db->real_escape_string("$this->forename $this->surname\Network Letter.pdf");
                $sub_agency = $db->real_escape_string("$this->forename $this->surname");


                $db->query("INSERT INTO " . BULK_DETAILS_TBL . " (`ifa_auth`, `network_auth`, `agency_called`,`PartnerID`)
							 VALUES('" . $ifa_auth . "', '" . $net_auth . "', '" . $sub_agency . "'," . $this->id() . ")");

                // Workflow ticket reminder 2 months and 6 months after partner has been added
                $wf = new Workflow;

                $array_2 = [
                    'process' => '14',
                    'step' => '22',
                    'priority' => '2',
                    'due' => time() + 4838000,
                    'desc' => '',
                    '_object' => 'Partner',
                    'index' => $this->id()
                ];

                $array_6 = [
                    'process' => '7',
                    'step' => '10',
                    'priority' => '2',
                    'due' => time() + 15308663,
                    'desc' => '6 month satisfaction call. Partner ID: ' . $this->id(),
                    '_object' => 'Partner',
                    'index' => $this->id()
                ];

                // Messy!! Output workflow details into buffer and then clear buffer -> stops confusion when redirect to partner profile on addition
                ob_start();

                if ($post["transtype"] != 1) {
                    //Bug 461 - Only assign workflow tickets to Louise when Bulk Partners are added
                    $wf->workflow_add($array_6, [98]);
                } else {
                    $wf->workflow_add($array_6, [98]);
                    $wf->workflow_add($array_2, [$this->manager()], 4);
                }

                ob_end_clean();
            }

            if (isset($_GET['prospect_id'])) {
                $db = new mydb();

                // adds partnerID to pm_prospects
                $db->query(
                    "UPDATE " . PROSPECT_TBL . "
					SET partner_id = " . $this->id() . ", updated_when = " . mktime() . ", updated_by = " . $_COOKIE["uid"] . "
					WHERE id = " . $_GET['prospect_id'] . ""
                );

                // move recruit image to new directory with new partner ID
                azureCopyFile(CDN_DIR . "recruit_images", $_GET['prospect_id'] . ".jpg", CDN_DIR . "partner_images", $this->id() . ".jpg");
                azureRemoveFile(CDN_DIR . "recruit_images", $_GET['prospect_id'] . ".jpg");
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function get_practice()
    {
        $db = new mydb();

        $db->query(
            "SELECT " . PRACTICE_TBL . ".id, " . PRACTICE_TBL . ".name" .
            " FROM " . PRACTICE_TBL .
            " JOIN " . PRACTICE_STAFF_TBL . " on " . PRACTICE_STAFF_TBL . ".practice_id = " . PRACTICE_TBL . ".id" .
            " WHERE " . PRACTICE_STAFF_TBL . ".partner_id = " . $this->primary_key() . " AND active=1" .
            " LIMIT 1"
        );

        $practice = $db->next(new Practice);
        return ($practice === false) ? new Practice : $practice;
    }


    // Returns GRI for a partner given a partner id.
    // Optional $life flag if lifetime GRI should be returned.  By default 12 months is returned.
    /* GRI calculated as (renewal(0) + clawback(2) + adviser_charging_ongoing(8) + client_direct_ongoing(10) + dfm_fees(12) +
    OAS Income(14) + Trail(15) + PPB Income(16)) - VAT */

    public function get_website_account()
    {
        //// Get an array/instance of Partner's mypsaccount WebsiteUser object.

        $s = new Search(new WebsiteUser);
        $s->eq("partner", $this->id());

        $accounts = [];
        while ($a = $s->next()) {
            $accounts[] = $a;
        }

        if (!sizeof($accounts)) {
            // No account found for this Partner
            return false;
        } elseif (sizeof($accounts) == 1) {
            // Partner has the recommended solo account
            return $accounts[0];
        } else {
            // Partner has multiple associated accounts
            trigger_error("Partner $this->id has multiple mypsaccounts", E_USER_WARNING);
            return array_pop($accounts); // return most recent account
        }
    }

    //get partners key contact for adviser charging
    public function clientsWorth($life = false)
    {
        $db = new mydb();

        $worth_fig = "";

        if($life){
            $db->query("select round(sum(amount),2) as total_amount from tblcommissionauditsql c
                    inner join tblpolicy p on c.policyID = p.policyID
                    where partnerid = " . $this->id() . "
                    and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                    and Commntype in (8,10)
                    group by partnerid
                    limit 1");

            if($client_worth = $db->next(MYSQLI_ASSOC)){
                $db->query("select round(sum(vat),2) as total_vat from tblcommissionauditsql c
                        inner join tblpolicy p on c.policyID = p.policyID
                        where partnerid = " . $this->id() . "
                        and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                        and Commntype in (8,10)
                        group by partnerid
                        limit 1");

                if($vat = $db->next(MYSQLI_ASSOC) ){
                    ($vat['total_vat'] < 0) ? $client_worth_1 = $client_worth['total_amount'] + $vat['total_vat'] : $client_worth_1 = $client_worth['total_amount'] - $vat['total_vat'];
                } else {
                    $client_worth_1 = 0;
                }
            } else {
                $client_worth_1 = 0;
            }

            ($client_worth_1) ? $worth_fig = format_current($client_worth_1) : $worth_fig = "£ 0.00";
        } else {
            $minus_1_year = Carbon::now()->subYear();

            $db->query("select round(sum(amount),2) as total_amount from tblcommissionauditsql c
                    inner join tblpolicy p on c.policyID = p.policyID
                    where partnerid = " . $this->id() . "
                    and date(paidDate) >= '" . $minus_1_year->format("Y-m-d") . "'
                    and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                    and Commntype in (8,10)
                    group by partnerid
                    limit 1");

            if($client_worth = $db->next(MYSQLI_ASSOC)){
                $db->query("select round(sum(vat),2) as total_vat from tblcommissionauditsql c
                        inner join tblpolicy p on c.policyID = p.policyID
                        where partnerid = " . $this->id() . "
                        and date(paidDate) >= '" . $minus_1_year->format("Y-m-d") . "'
                        and Status in (" . implode(',', LIVE_POLICY_STATUS) . ")
                        and Commntype in (8,10)
                        group by partnerid
                        limit 1");

                if($vat = $db->next(MYSQLI_ASSOC) ){
                    ($vat['total_vat'] < 0) ? $client_worth_1 = $client_worth['total_amount'] + $vat['total_vat'] : $client_worth_1 = $client_worth['total_amount'] - $vat['total_vat'];
                } else {
                    $client_worth_1 = 0;
                }
            } else {
                $client_worth_1 = 0;
            }

            ($client_worth_1) ? $worth_fig = format_current($client_worth_1) : $worth_fig = "£ 0.00";
        }

        return $worth_fig;
    }

    public static function partner_gri($partner, $life = false)
    {
        $db = new mydb();
        $gri_fig = "";

        if (!$life) {
            $db->query("SELECT IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)) as GRI_12 " .
                " FROM " . COMM_AUDIT_TBL .
                " WHERE partnerid = " . $partner .
                " AND commntype in " . CommissionType::griTypes() .
                " AND paiddate >= DATE_SUB(NOW(), INTERVAL 12 MONTH)");

            if ($gri = $db->next()) {
                $gri_fig = "&pound; " . number_format($gri['GRI_12'], 2);
            } else {
                $gri_fig = "&pound;0.00";
            }
        } else {
            $db->query("SELECT IF((SUM(vat) < 0), SUM(amount)+SUM(vat), SUM(amount)-SUM(vat)) as GRI_LIFE " .
                " FROM " . COMM_AUDIT_TBL .
                " WHERE partnerid = " . $partner .
                " AND commntype in " . CommissionType::griTypes());

            if ($gri = $db->next()) {
                $gri_fig = "&pound; " . number_format($gri['GRI_LIFE'], 2);
            } else {
                $gri_fig = "&pound;0.00";
            }
        }

        return $gri_fig;
    }

    //function to output the total income at risk for partner

    public static function partner_gri_atrisk($partner, $life = false)
    {
        // Search for clients who have policies with platforms
        $plats = [
            1152, # CoFunds
            86,   # Fidelity
            1216, # Old Mutual Wealth Platform, formally know as @SiS
        ];

        $plat_str = implode(', ', $plats);

        $db = new mydb();
        $db->query("select client_id, sum(renewal_12) as renewal_12, sum(renewal_lifetime)
            from feebase.policy_gri
            inner join feebase.tblclient
            on tblclient.clientid = policy_gri.client_id
            inner join feebase.tblpolicy
            on tblpolicy.policyid = policy_gri.policy_id
            where tblclient.partnerid = " . $partner . "
            and tblpolicy.issuerid in (" . $plat_str . ")
            and tblpolicy.status = 2
            and tblpolicy.ac_applied = 0
            and tblpolicy.ac_exempt = 0
            and renewal_12 IS NOT NULL
            group by tblclient.clientid
            order by renewal_12 desc");

        //make an array of clients that are brought back from the PolicyGRI search itr.
        $clients = [];
        while ($x = $db->next(MYSQLI_ASSOC)) {
            if (!in_array($x['client_id'], $clients)) {
                $clients[] = $x['client_id'];
            }
        }

        /* for each client we want to check that every policy under the platforms has a correspondence item with
        subject 'Adviser Charging Form sent to Provider', if not we will define this as at risk */
        $client_to_remove = [];

        foreach ($clients as $c) {
            $db = new mydb();
            $db->query("SELECT DISTINCT policyID FROM feebase.tblpolicy
                        WHERE clientID = " . $c . "
                        AND issuerID IN(" . $plat_str . ")
                        AND status = 2
                        AND tblpolicy.ac_applied = 0 AND tblpolicy.ac_exempt = 0");

            $policies_returned = [];

            while ($p = $db->next(MYSQLI_NUM)) {
                //add unique policyIDs to an array for checking, $p[0] == policyID
                $policies_returned[] = $p[0];
            }

            $safe_policies = [];

            foreach ($policies_returned as $pol) {
                $db->query("SELECT Policy FROM feebase.tblcorrespondencesql
                          WHERE Policy = " . $pol . " AND subject = 'Adviser Charging Form sent to Provider'");

                while ($corr = $db->next(MYSQLI_NUM)) {
                    //if row returned, add to safe_policies array,  $corr[0] == policy id
                    if ($corr[0]) {
                        $safe_policies[] = $corr[0];
                    }
                }
            }

            //create a new array with the safe policies removed
            $policies_array = array_diff($policies_returned, $safe_policies);


            //array is empty there for all policies under this client are safe to be removed.
            if (empty($policies_array)) {
                $client_to_remove[] = $c;
            }
        }

        //only bring back rows for clients that are at risk
        $client_to_show = array_diff($clients, $client_to_remove);


        /* if a partner has no clients to show, add value of 0 to the array so to not break the
            IN clause on the search below  */
        if (empty($client_to_show)) {
            $client_to_show[] = 0;
        }

        $client_str = implode(', ', $client_to_show);

        $db = new mydb();
        $db->query("select client_id, sum(renewal_12) as renewal_12, sum(renewal_lifetime) as renewal_lifetime
        from feebase.policy_gri
        inner join feebase.tblclient
        on tblclient.clientid = policy_gri.client_id
        inner join feebase.tblpolicy
        on tblpolicy.policyid = policy_gri.policy_id
        where tblclient.partnerid = " . $partner . "
        and tblpolicy.issuerid in (" . $plat_str . ")
        and tblpolicy.status = 2
        and tblpolicy.ac_applied = 0
        and tblpolicy.ac_exempt = 0
        and renewal_12 IS NOT NULL
        and client_id in(" . $client_str . ")
        group by tblclient.clientid
        order by renewal_12 desc");

        $gri_fig = 0;

        if (!$life) {
            while ($x = $db->next(MYSQLI_ASSOC)) {
                $gri_fig += $x['renewal_12'];
            }
        } else {
            while ($x = $db->next(MYSQLI_ASSOC)) {
                $gri_fig += $x['renewal_lifetime'];
            }
        }

        return "&pound;" . number_format($gri_fig, 2);
    }

    public static function suggestedReviewRate($partner, $gri)
    {
        $tariff = $suggested_rate = false;

        // Reviewable contract - 2009 Tariff
        $tariff_2009 = [
            ['min' => -99999, 'max' => 1000, 'percentage' => 0, 'band' => 0],
            ['min' => 1001, 'max' => 5000, 'percentage' => 60, 'band' => 1],
            ['min' => 5001, 'max' => 10000, 'percentage' => 65, 'band' => 2],
            ['min' => 10001, 'max' => 15000, 'percentage' => 70, 'band' => 3],
            ['min' => 15001, 'max' => 25000, 'percentage' => 75, 'band' => 4],
            ['min' => 25001, 'max' => 50000, 'percentage' => 80, 'band' => 5],
            ['min' => 50001, 'max' => 75000, 'percentage' => 82, 'band' => 6],
            ['min' => 75001, 'max' => 100000, 'percentage' => 84, 'band' => 7],
            ['min' => 100001, 'max' => 150000, 'percentage' => 86, 'band' => 8],
            ['min' => 150001, 'max' => 200000, 'percentage' => 88, 'band' => 9],
            ['min' => 200001, 'max' => 250000, 'percentage' => 90, 'band' => 10],
            ['min' => 250001, 'max' => 300000, 'percentage' => 92, 'band' => 11],
            ['min' => 300001, 'max' => 999999999, 'percentage' => 94, 'band' => 12]
        ];

        // Reviewable contract - 2013 Tariff
        $tariff_2013 = [
            ['min' => -99999, 'max' => 1000, 'percentage' => 30, 'band' => 0],
            ['min' => 1001, 'max' => 10000, 'percentage' => 60, 'band' => 1],
            ['min' => 10001, 'max' => 20000, 'percentage' => 65, 'band' => 2],
            ['min' => 20001, 'max' => 30000, 'percentage' => 70, 'band' => 3],
            ['min' => 30001, 'max' => 50000, 'percentage' => 75, 'band' => 4],
            ['min' => 50001, 'max' => 75000, 'percentage' => 80, 'band' => 5],
            ['min' => 75001, 'max' => 100000, 'percentage' => 82, 'band' => 6],
            ['min' => 100001, 'max' => 150000, 'percentage' => 84, 'band' => 7],
            ['min' => 150001, 'max' => 200000, 'percentage' => 86, 'band' => 8],
            ['min' => 200001, 'max' => 250000, 'percentage' => 88, 'band' => 9],
            ['min' => 250001, 'max' => 300000, 'percentage' => 90, 'band' => 10],
            ['min' => 300001, 'max' => 400000, 'percentage' => 92, 'band' => 11],
            ['min' => 400001, 'max' => 999999999, 'percentage' => 94, 'band' => 12]
        ];

        if ($partner->reviewable() == 1) {
            $tariff = $tariff_2009;
        } else if ($partner->reviewable() == 2) {
            $tariff = $tariff_2013;
        }

        if ($tariff) {
            $partner_gri = str_replace('&pound;', '', $gri);

            // Get a suggested rate dependant upon which tariff and percentage the partner's gri falls within
            $precision = 0.01;
            foreach ($tariff as $current) {
                if ((str_replace(',', '', $partner_gri) - $current['min']) >= $precision &&
                    (str_replace(',', '', $partner_gri) - $current['max']) <= $precision) {
                    $suggested_rate = $current['percentage'];
                }
            }
            $return = $suggested_rate."%";

        } else {
            $return = "ERROR";
        }

        return $return;
    }

    public function hasClientIncomeHold()
    {
        $db = new mydb();

        $db->query("SELECT * FROM income_on_hold inner join tblclient on object_index = clientID where object_type = \"client\" and release_when is null and partnerID = " . $this->id());

        $clients = 0;
        $total = 0;

        while($ih = $db->next(MYSQLI_ASSOC)){
            $held = IncomeHold::totalIncomeHeld($ih['clientID']);
            if($held > 0){
                $clients++;
                $total += $held;
            }
        }

        if ($clients){
            echo "<div class='alert alert-danger mr-2 mb-2'><i class='fas fa-exclamation-circle'></i> This partner has " .
                $clients . " " . pluralize($clients, "client") .  " with adviser charging income on hold totalling £" .
                number_format($total, 2) . "</div>";
        }
    }

    public function ar_count(){
        $redisClient = RedisConnection::connect();
        return $redisClient->exists("action_required.Partner." . $this->id());
    }

    public function can_add_clients() {
        if(array_key_exists($this->id(), PARTNER_CLIENT_ADD_BLOCK)){
            $allowed = PARTNER_CLIENT_ADD_BLOCK[$this->id()];

            if(!in_array(User::get_default_instance("id"), $allowed['users'])){
                if(!in_array(User::get_default_instance("department"), $allowed['depts'])){
                    if(!User::get_default_instance('team_leader') || !$allowed['team_leaders']){
                        return false;
                    }
                }
            }
        }

        // check if partner was transferred as "Agency- Single", clients cannot be added to this account unless IT or Partner Accounts
        if($this->transtype() == 2){
            if(!User::get_default_instance()->has_permission(47)){
                return false;
            }
        }

        return true;
    }

    public function has_info()
    {
        $db = new mydb();

        $query = "SELECT * FROM partner_info WHERE partner_id = " . $this->id() . "
                AND (
                    (portfolio_reports IS NOT NULL AND portfolio_reports != '')
                    OR
                    (welcome_letters IS NOT NULL AND welcome_letters != '')
                    OR
                    (new_business_cases IS NOT NULL AND new_business_cases != '')
                    OR
                    (client_proposition IS NOT NULL AND client_proposition != '')
                    OR
                    (other IS NOT NULL AND other != '')
                )";

        $db->query($query);

        return $db->next(MYSQLI_ASSOC);
    }
}
