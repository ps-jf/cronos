<?php
/**
 * Allows us to access any phone calls logged to the database.
 *
 * We use python on the call machine to enter phone calls into the database
 * From here we can map those in and apply them to profiles, analyse them etc
 *
 *
 * @package prophet.admin
 * @author lewis.barbour
 */

class Call extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "calls";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->start = Date::factory("start_time")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        $this->end = Date::factory("end_time")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        $this->person1 = Field::factory("person1");

        $this->person2 = Field::factory("person2");

        $this->number1 = Field::factory("number1");

        $this->number2 = Field::factory("number2");

        $this->direction = Field::factory("direction");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return "$this->id";
    }
}
