<?php


class ReviewAttempt extends DatabaseObject
{
    const DB_NAME = "myps";
    const TABLE = "review_contact_attempts";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->ongoingService = Sub::factory("OngoingService", "os_id");

        $this->contact_type = Field::factory("contact_type");

        $this->contact_when = Date::factory("contact_when")
            ->set_var(Date::FORMAT, Date::ISO_8601_SHORT)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE_SHORT);

        $this->contact_number = Field::factory("contact_number");

        $this->addedwhen = Date::factory("created_at")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        $this->updatedwhen = Date::factory("updated_at")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        $this->deletedwhen = Date::factory("deleted_at")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE);

        $this->addedby = Sub::factory("WebsiteUser", "created_by");

        $this->updatedby = Sub::factory("WebsiteUser", "updated_by");

        $this->deletedby = Sub::factory("WebsiteUser", "deleted_by");

        $this->database = REMOTE;

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        return strval($this->contact_type);
    }

    public function evidence()
    {
        switch($this->contact_type()){
            case "Written/Letter":
                $s = new Search(new DocumentStorage());
                $s->eq("object", "ReviewAttempt");
                $s->eq("object_id", $this->id());

                if($doc = $s->next(MYSQLI_ASSOC)){
                    $evidence = $doc->link();
                }

                break;

            default:
                $evidence = $this->contact_number();

                break;
        }

        if(!empty($evidence)){
            return $evidence;
        }

        return "No evidence found";
    }

    public function link($text = false)
    {
        return $this->ongoingService->link(strval($this));
    }
}