<?php

class PipelineEntry extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "tblcommission";
    const VAT_RATE = 0.2;
    const VAT_REDUCED_RATE = 0.05;

    public function __construct($id = false, $autoget = false)
    {
        $this->id = Field::factory("commissionID", Field::PRIMARY_KEY);

        $this->policy = Sub::factory("Policy", "policyID")
            ->set_var(Field::REQUIRED, true);

        // issuer is not a column in tblcommission.. WHY IS IT HERE?!
        $this->issuer = Field::factory("issuerID");

        $this->amount = Field::factory("amount")
            ->set_var(Field::TYPE, Field::CURRENCY)
            ->set_var(Field::REQUIRED, true);

        $this->batch = Sub::factory("Batch", "batchID")
            ->set_var(Field::REQUIRED, true);

        $this->type = Sub::factory("CommissionType", "commissiontype")
            ->set_var(Field::REQUIRED, true)
            ->set_var(Field::FORCE_ZERO, true);

        $this->partner_percent = Field::factory('%agetopartner')
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->ps_percent = Field::factory('%agetops')
            ->set_var(Field::TYPE, Field::PERCENT);

        $this->description = Field::factory('description')
            ->set_var(Field::HTML_FIELD_TYPE, Field::HTML_MULTILINE);

        $this->added_by = Sub::factory("User", "editedBy")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance('id'));

        $this->added_when = Date::factory('editedWhen')
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->vat_rate = Choice::factory("vat")
            ->push('1', "Exempt")
            ->push('2', "Zero")
            ->push('3', "Reduced")
            ->push('4', "Current")
            ->set(1);

        $this->currency_type = Choice::factory("currency_type")
            ->push('GBP', "GBP")
            ->push('HKD', "HKD")
            ->push('USD', "USD")
            ->push('EUR', "EUR")
            ->set('GBP');

        $this->on_hold = Boolean::factory("on_hold")
            ->set_values(1, 0)
            ->set(0);

        $this->CAN_DELETE = true;
        $this->SNAPSHOT_LOG = true;


        parent::__construct($id, $autoget);
    }

    public function _is_valid()
    {
        try {
            // there must be a new business entry for this policy if initial commission
            if ("$this->type" == "New Business") {
                $newb = new Search(new NewBusiness);
                $newb
                    ->eq("policy", $this->policy())
                    ->set_limit(1);

                if (!($case = $newb->next())) {
                    throw new Exception("No New Business entry found for Policy");
                }
            }

            // policy must belong to the Batch's Provider
            $batch = $this->batch->get_object();
            $policy = $this->policy->get_object();

            if ($batch->issuer() != $policy->issuer()) {
                throw new Exception("Policy doesn't belong to $batch->issuer");
            }

            // amount must be greater/lesser than 0
            if ($this->amount() == 0) {
                throw new Exception("Amount must be greater/lesser than &pound;0.00");
            }

            // if % shares provided, must total 100%
            $partner = $this->partner_percent();
            $ps = $this->ps_percent();

            if (($partner !== false && $ps !== false) && intval($partner + $ps) != 100) {
                throw new Exception("Ensure that both share percentages supplied and that they total 100%");
            }
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return false;
        }

        return true;
    }

    function __toString()
    {
        // policyID (£xxx)
        return "$this->policy ( £ $this->amount )";
    }
}
