<?php

class Workflow extends DatabaseObject
{
    const DB_NAME = DATA_DB;
    const TABLE = "workflow";
    const VALUATION_STEPS = [41, 188, 193];

    public function __construct($id = false, $autoget = false)
    {
        $this->id = new Field("id", Field::PRIMARY_KEY);

        $this->due = Date::factory("due", false)
            ->set_var(Field::REQUIRED, true)
            ->set_var(Date::FORMAT, Date::UNIX);

        $this->step = Sub::factory("WorkflowStep", "step")
            ->set_var(Field::REQUIRED, true);

        $this->desc = Field::factory("descText")
            ->set_var(FIELD::HTML_FIELD_TYPE, FIELD::HTML_MULTILINE);

        $this->priority = Choice::factory("priority")
            ->push(1, "Low")
            ->push(2, "Medium")
            ->push(3, "High")
            ->set_var(Field::REQUIRED, true);

        $this->_object = Field::factory("object")
            ->set_var(Field::REQUIRED, true);

        $this->index = Field::factory("object_index");

        $this->addedwhen = Date::factory("added")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(time());

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::UNIX)
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(time());

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true)
            ->set(User::get_default_instance("id"));

        $this->issuer = Sub::factory("Issuer", "object_index");

        $this->partner = Sub::factory("Partner", "object_index");

        $this->SNAPSHOT_LOG = true;

        $this->CAN_DELETE = true;

        parent::__construct($id, $autoget);
    }

    public function __toString()
    {
        /* Output...something */
        return "Workflow #" . $this->id;
    }

    public function save($return_query = false)
    {
        // get an instance of the old object before updating
        $wf = new Workflow($this->id(), true);

        $save = parent::save($return_query);

        if(in_array($this->step(), Workflow::VALUATION_STEPS)){
            // this is valuation workflow
            if ($this->due() != $wf->due()){
                // workflow due date has changed

                // find the valuation request
                $s = new Search(new ValuationRequest());
                $s->eq("workflow", $this->id());

                if($valReq = $s->next(MYSQLI_ASSOC)){
                    // we have a valuation request, now find the next available delivery date
                    $valReq->update_delivery_date($this->due());
                }
            }
        }

        return $save;
    }

    /**
     * Finds a workflow ticket using search parameters
     *
     * @param $s - current step
     * @param $o - the object as defined in objects table
     * @param $i - the table index of the above object
     * @return int  - id of the workflow
     */
    public function workflow_find($s, $o, $i)
    {
        $m = new Search($this);
        $m->eq('_object', $o);
        $m->eq('index', $i);
        $m->eq('step', $s);
        $m->set_limit(1);

        return ((int)$m->next()->id);
    }

    /**
     * Adds an entry to the workflow table
     *
     * @param $arr_args - array of workflow properties
     * @param $assigned - array of users to assign to
     * @param bool $assigned_group - group ticket should be assigned to
     * @param bool $is_global - true if added false otherwise
     */
    public function workflow_add($arr_args, $assigned = false, $assigned_group = false, $is_global = false)
    {
        $this->load($arr_args);
        
        $json = (object)[
            "id" => false,
            "status" => false,
            "error" => false
        ];

        try {
            if ($this->save()) {
                if (is_array($assigned) || is_object($assigned)) {
                    foreach ((array)$assigned as $u) {
                        $wa = new WorkflowAssigned;
                        $wa->user_id->set($u);
                        $wa->workflow_id->set($this->id);
                        $wa->save();
                    }
                } elseif(!empty($assigned)) {
                    $wa = new WorkflowAssigned;
                    $wa->user_id->set($assigned);
                    $wa->workflow_id->set($this->id);
                    $wa->save();
                }

                if ($assigned_group) {
                    $wa = new WorkflowAssigned;
                    $wa->group_id->set($assigned_group);
                    $wa->workflow_id->set($this->id);
                    $wa->save();
                }

                if ($is_global) {
                    $wa = new WorkflowAssigned;
                    $wa->is_global->set($is_global);
                    $wa->workflow_id->set($this->id);
                    $wa->save();
                }

                $json->status = true;
                $json->id = $this->id();
            }
        } catch (Exception $e) {
            $json->error = $e->getMessage();
        }

        echo json_encode($json);
    }

    /**
     * Audits a workflow ticket and moves it along from step to step or closes it if need be
     *
     * @param bool $time
     * @param bool $early_end
     * @param bool $end_reason
     * @return bool - true if "nexted" false otherwise
     * @throws Exception
     */
    public function workflow_next($time = false, $early_end = false, $end_reason = false)
    {
        $db = new mydb();

        $feesOff = false;

        $query = "INSERT INTO " . WFA_TBL .
            "(`workid`,`due`,`early_end`, `end_reason`, `step`,`descText`,`priority`,`object`,`object_index`,`completed`,`added`,`added_by`,`completed_by`) " .
            "VALUES " .
            "('" . $this->id . "','" . $this->due->value .
            "','" . $early_end . "','" . $end_reason .
            "','" . $this->step->value . "','" . addslashes($this->desc->value) . "','" . $this->priority->value .
            "','" . $this->_object->value . "','" . $this->index->value . "','" . time() . "','" . $this->addedwhen->value . "','" . $this->addedby->value . "','" . User::get_default_instance("id") . "')";

        // audits the workflow item
        if ($db->query($query)) {
            if ($this->step->next_step() == 0) {
                if ($db->query("DELETE FROM " . $this::DB_NAME . "." . $this::TABLE . " WHERE id = " . $this->id)
                    && ($db->query("DELETE FROM " . WFPROGRESS_TBL . " WHERE workflowID = " . $this->id))) {
                }
                return true;
            }

            if ($this->step() == 180){
                // if we are moving on from "Turn Off Fee" for Income Disclosure
                $feesOff = true;
            }

            //if the ticket that is being moved along is a complaint
            //The update the complaints object as requir
            $complaint_steps = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58];
            if (in_array($this->step->next_step(), $complaint_steps)) {
                $cobj = new Search(new Complaints);
                $cobj->eq('id', $this->index());

                while ($c = $cobj->next()) {
                    $c->status($this->step->next_step());
                    $c->save();
                }
            }

            if ($time) {
                $this->due->set($time);
            } else {
                $this->due->set($this->due->value + (int)$this->step->timescale());
            }

            if ($this->step->loop() == 0) {
                $this->step->set($this->step->next_step());
            }

            return $this->save();
        }
    }

    /**
     * Roll workflow ticket back to previous step
     *
     * @param $workf - id of workflow ticket
     * @return object
     */
    public function workflow_rollback($workf)
    {
        $json = (object)[
            "id" => null,
            "status" => false,
            "error" => null
        ];

        try {
            $db = new mydb();
            $wf = new Workflow($workf, true);

            # Obtain step to rollback to
            $db->query("SELECT stepID from " . WFS_TBL . " WHERE nextstepID = " . $wf->step());

            while ($step = $db->next(MYSQLI_ASSOC)) {
                $previous_step = $step['stepID'];
                $wfs = new WorkflowStep($previous_step, true);

                // Rollback workflow entry to previous step
                $wf->step->set($previous_step);
                $wf->due->set((int)$wf->due() - $wfs->timescale());
                $wf->save();

                //if the ticket that is being moved along is a complaint
                //The update the complaints object as required
                $complaint_steps = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58];
                if (in_array($previous_step, $complaint_steps)) {
                    $cobj = new Search(new Complaints);
                    $cobj->eq('id', $wf->index());

                    while ($c = $cobj->next()) {
                        $c->status($previous_step);
                        $c->save();
                    }
                }

                // On successful update, add workflow audit entry to log the rollback
                if ($wf->save()) {
                    $wfa = new WorkflowAudit();

                    $wfa->workid($wf->id);
                    $wfa->due((int)$wf->due() + $wfs->timescale());
                    $wfa->step($wfs->next_step());
                    $wfa->desc(addslashes($wf->desc));
                    $wfa->priority($wf->priority());
                    $wfa->index($wf->index());
                    $wfa->_object($wf->_object());
                    $wfa->completed();
                    $wfa->addedby();
                    $wfa->addedwhen($wf->addedwhen());

                    // If wofrkflow audit entry added successfully, all is good and process rolled back
                    if ($wfa->save()) {
                        $json->status = true;
                        $json->error = false;
                    } else {
                        $json->status = false;
                        $json->error = "Failed to audit rollback, contact IT";
                    }
                } else {
                    $json->status = false;
                    $json->error = "Failed to rollback workflow to previous step, contact IT";
                }
                return $json;
            }
            $json->status = false;
            $json->error = "No previous step to rollback to";
        } catch (Exception $e) {
            $json->status = false;
            $json->error = $e->getMessage();
        }

        return $json;
    }

    public static function backdatedCheck($wf_id)
    {
        $db = new mydb(REMOTE, E_USER_WARNING);
        if (mysqli_connect_errno()) {
            echo "Can't connect to the mypsaccount server: Error#" . mysqli_connect_errno();
            exit();
        }
        $db->query("select workflow.id
            from myps.valuation_requests
            inner join feebase.workflow on workflow.id = workflow_id
            inner join myps.ongoing_service os on os.val_req_id = valuation_requests.id
            inner join user_info on user_info.id = valuation_requests.requested_by
            inner join prophet.users on users.myps_user_id = user_info.id
            where date_format(proposed_review_date, '%Y-%m-%d') < date_format(from_unixtime(requested_for), '%Y-%m-%d')
            and workflow.id = " . $wf_id);

        if ($ticket = $db->next(MYSQLI_ASSOC)) {
            return "Back-dated by PS";
        }
    }

    public function end_ticket($reason)
    {
        $complete = false;

        $wfa = new WorkflowAudit();

        $wfa->workid($this->id());
        $wfa->due($this->due());
        $wfa->step($this->step());
        $wfa->desc(addslashes($this->desc()));
        $wfa->priority($this->priority());
        $wfa->index($this->index());
        $wfa->_object($this->_object());
        $wfa->completed(time());
        $wfa->addedby(User::get_default_instance('id'));
        $wfa->addedwhen($this->addedwhen());
        $wfa->early_end(1);
        $wfa->end_reason(addslashes($reason));

        if ($wfa->save()){
            $db = new mydb();
            $remoteDB = new mydb(REMOTE);

            $query = "Update feebase.notes set object ='WorkflowAudit' , object_id =" . $wfa->id() . " where object = 'Workflow' and object_id =" . $this->id();

            $db->query($query);

            if ($this->delete()){
                $ac_steps = [18, 19, 20];
                if (in_array($this->step(), $ac_steps)) {
                    $remoteDB->query("UPDATE " . REMOTE_SYS_DB . ".charging_pack_requests
                                        SET archived= 1 WHERE clientID =" . $this->index());
                }

                $complaint_steps = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58];
                if (in_array($this->step(), $complaint_steps)) {
                    //Because the complaint has been set to record closed, please close this complaint
                    $db->query("UPDATE feebase.complaints_register
                                        SET Status = 59 , date_resolved = UNIX_TIMESTAMP() ,
                                         record_closed = 1 
                                         WHERE id =" . $this->index());

                }

                $complete = true;
            } else {
                throw new Exception("WF Audit entry added, Failed to remove Workflow Ticket " . $this->id() . ", contact IT");
            }
        } else {
            throw new Exception("Failed to end ticket, contact IT");
        }

        return $complete;
    }

    public function date_change_reason($newDate, $reason)
    {
        $ar = new ActionReason();
        $ar->object("Workflow");
        $ar->object_id($this->id());
        $ar->reason($reason);
        $ar->action("Workflow due update");

        if ($ar->save()) {
            if (in_array($this->step(), [41,193])) {
                $client = new Client($this->index(), true);
                if ($client->partner->email()) {
                    $partner_addressee = addslashes(str_replace("&", "and", $client->partner->addressee()));

                    // check time and give custom greeting in email
                    (date('G') <= 11) ? $greeting = "Good Morning" : $greeting = "Good Afternoon";
                    ($partner_addressee && $partner_addressee != "") ?
                        $greeting = $greeting . " " . $partner_addressee . ", " :
                        $greeting = $greeting . ", ";

                    // we want to email partner to inform them of change
                    $sender_email = "portfolioservicing@policyservices.co.uk";
                    $recipient[]['address']['email'] = $client->partner->email();
                    $recipient[]['address']['name'] = $partner_addressee;

                    $recipient[]['address'] = ['name' => User::get_default_instance('forename'), 'email' => User::get_default_instance('email')];
                    $subject = "Portfolio Report - Date Change";
                    $from_name = "Policy Services Ltd - Portfolio Servicing";
                    $sender = ['name' => $from_name, 'email' => $sender_email];

                    // build up main content of email body
                    $content_string = "<p>There has been an update to our workflow which may affect the delivery date of a Portfolio Report ordered for <strong>" . $client . "</strong>, reference (" . $client->id() . ").</p>
                                <p>Please see below for details of the change:</p>
                                <p>Workflow reference: " . $this->id() . "</p>
                                <p>Original due date (internal): " . substr($this->due, 0, 10) . "</p>
                                <p>Updated due date (internal): " . $newDate . "</p>
                                <p>Reason: " . $reason . "</p>
                                <p>Please note the dates above reflect when we aim to have the report completed internally, this is then passed to an external mailing house therefore delivery dates will differ from those shown above.</p>";

                    $body = json_encode([
                        "SUBJECT" => $subject,
                        "FROM_NAME" => $from_name,
                        "SENDER" => $sender_email,
                        "REPLY_TO" => $sender_email,
                        "ADDRESSEE" => $greeting,
                        "CONTENT_STRING" => $content_string
                    ]);

                    // attempt email with ol' sparky
                    if (!sendSparkEmail($recipient, $subject, 'generic-partner', $body, $sender)) {
                        throw new Exception("Reason successfully logged, partner email failed");
                    }
                } else {
                    throw new Exception("Reason successfully logged, no partner email address on record");
                }
            }
        } else {
            throw new Exception("Failed to save reason, try again or contact IT");
        }

        return true;
    }
}
