<?php


class Query extends DatabaseObject
{
    const DB_NAME = SYS_DB;
    const TABLE = "queries";

    public function __construct($id = false, $auto_get = false)
    {
        $this->id = Field::factory("id", Field::PRIMARY_KEY);

        $this->name = Field::factory("name")
            ->set_var(Field::REQUIRED, true);

        $this->select = Field::factory("select")
            ->set_var(Field::REQUIRED, true);

        $this->filters = Field::factory("filters")
            ->set_var(Field::REQUIRED, true);

        $this->grouping = Field::factory("grouping")
            ->set_var(Field::REQUIRED, true);

        $this->sql_file = Field::factory("sql_file");

        $this->group_access = Field::factory("group_access");

        $this->user_access = Field::factory("user_access");

        $this->addedwhen = Date::factory("added_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE)
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(date("Y-m-d H:i:s"));

        $this->addedby = Sub::factory("User", "added_by")
            ->set_var(Field::BLOCK_UPDATE, true)
            ->set(User::get_default_instance("id"));

        $this->updatedwhen = Date::factory("updated_when")
            ->set_var(Date::FORMAT, Date::ISO_8601)
            ->set_var(Date::DISPLAY_FORMAT, Date::UK_STYLE)
            ->set_var(Field::BLOCK_INSERT, true);

        $this->updatedby = Sub::factory("User", "updated_by")
            ->set_var(Field::BLOCK_INSERT, true);

        $this->connection = Field::factory("connection")
            ->set("REPORTING");

        parent::__construct($id, $auto_get);
    }

    public function __toString()
    {
        $output = strval($this->name);

        if(!empty($this->sql_file())){
            $output .= " (raw)";
        }

        return $output;
    }

    public function getSelects()
    {
        return json_decode($this->select());
    }

    public function getFilters()
    {
        return json_decode($this->filters());
    }

    public function getGroupAccess()
    {
        if(in_array("all", json_decode($this->group_access()))){
            return "all";
        }

        return json_decode($this->group_access());
    }

    public function getUserAccess()
    {
        if(in_array("all", json_decode($this->group_access()))){
            return "all";
        }

        return json_decode($this->user_access());
    }

    public static function loadQueries($query = null, $groupID = null)
    {
        $results = [];

        $db = new mydb();

        switch($query){
            case "all":
                if(User::get_default_instance("department") == 7){
                    $q = "SELECT id FROM prophet.queries ORDER BY id DESC";
                } else {
                    $q = "SELECT id FROM prophet.queries WHERE group_access is not null AND group_access != '' AND JSON_SEARCH(group_access, 'one', 'all') is not null ORDER BY id DESC";
                }

                break;

            case "group":
                $q = "SELECT id FROM prophet.queries WHERE group_access is not null AND group_access != '' AND JSON_SEARCH(group_access, 'one', '" . $groupID . "') is not null ORDER BY id DESC";

                break;

            case "user":
                $q = "SELECT id FROM prophet.queries WHERE user_access is not null AND user_access != '' AND JSON_SEARCH(user_access, 'one', '" . User::get_default_instance("id") . "') is not null ORDER BY id DESC";

                break;
        }

        $db->query($q);

        while ($qu = $db->next(MYSQLI_ASSOC)){
            $results[] = new Query($qu['id'], true);
        }

        return $results;
    }
}