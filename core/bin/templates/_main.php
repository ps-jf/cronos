<?php

function Template($fname, $tags = false)
{
    // __invoke magic-method available in 5.3.0
    return new Template($fname, $tags);
}

class Template
{

    var $html;
    var $javascripts = [];
    var $stylesheets = [];
    var $tags = array(  );

    function __construct($filename = false, $tags = false)
    {

        if (!$filename) {
            return;
        }

        if (!in_array($filename[ 0 ], array( '/', '\\','C' ))) { #@todo FIX ME. Absoluate pths on F drive
            $filename = TEMPLATE_DIR . $filename;
        }

        if (file_exists($filename)) {
            $this->template_path = $filename;
        } else {
            throw new Exception("Template '$filename' not found");
        }


        $this->tags($tags);

        return $this;
    }

    function __invoke()
    {

        $args = func_get_args();

        foreach ($args as $arg) {
            // filename
            if (is_string($arg)) {
                $this->template_path = $arg;
            } // variables
            else if (is_array($arg)) {
                $this->tags($arg);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->output();
    }

    function tags($array)
    {

        if (!is_array($array)) {
            $array = array($array);
        }

        foreach ($array as $name => $value) {
            $this->tag($value, $name);
        }
    }

    /**
     * Append a tag to the tags array
     *
     * @param $tag - var to pass
     * @param $key - tag name to use
     * @param bool $reference - whether to copy or merely reference the var
     * @return $this
     */
    function tag($tag, $key, $reference = true)
    {
        ($reference) ? $this->tags[$key] = &$tag : $this->tags[$key] = $tag;
        return $this;
    }

    function output()
    {

        if (file_exists($this->template_path)) {
            ob_start();

            // Useful constants
            foreach (array("_SELF"=>$_SERVER["PHP_SELF"], "_SELF_URL"=>$_SERVER["REQUEST_URI"], "_LINK"=>random_string(12)) as $constant => $value) {
                if (!defined($constant)) {
                    define($constant, $value);
                }
            }

            /*
            # Make template tags available at variable level.
            # However, first check that doing so would not overwrite
            # any existing variable. If so, leave in tags array to be
            # picked up manually by the template designer.
            */
            $tags = [];

            foreach ($this->tags as $key => $value) {
                // from $this -> tags[key] to $key
                if( !isset( ${$key} ) )
                    ${$key} = $value;
            }

            if (isset($this->template_path)) {
                if (file_exists($this->template_path) && trim(file_get_contents($this->template_path)) != false) {
                    include($this->template_path);
                }
            }

            $buffer = ob_get_contents();

            ob_end_clean();
        }

        return $buffer;
    }
}
