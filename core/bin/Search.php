<?php

function is_string_r($arg)
{

    // quick method for determining that a variable is a string, or
    // that if it's an array that it is composed entirely of strings
    
    if (is_string($arg)) {
        return true;
    } elseif (is_array($arg)) {
        foreach ($arg as $var) {
            if (!is_string($var)) {
                return false;
            }
        }
        return true;
    }
    return false;
}

function Search($object)
{
    return new Search($object);
}

class Search
{

    const CUSTOM_FUNC    = 10; // allow use of custom parsing method
    const DEFAULT_STRING = 20;
    const DEFAULT_INT    = 30;
    const PATTERN        = 40;
    const FLAG_USAGE     = 50;

    const PROP_STR_OBJECT_DELIMITER = "|";
    const PROP_STR_DELIMITER = "->";
    
    // Example flags
    const IDEG  = '12345';
    const NAMEEG = 'John Smith';
    const POSTCODEEG = 'EH49 7YR';
    const NINOEG = 'AB123456C';
    const DATEEG = '01/01/1970';
    const DOBEG = '01/01/1970';
    const SEG = 'schemename';
    const SJPEG = '123456A';
    const EMAILEG = 'mr.man@localhost.com';
    const NUMBEREG = '123456A';
    const PARTNEREG = '10';
    const ISSUEREG = '100';
    const HAS_MANDATEEG = 'true or false';
    const MANDATEEG = "ID e.g. 1234, 4321, or 1";
    const TYPEEG = 'numeric ie 1 , 27';
    const FNAMEEG = 'David *';
    const MANAGEREG = ' firstname.secondname';
    const PREVSTATUSEG = ' IFA, ex , etc..  ';

    const ORDER_ASCEND = "ASC";
    const ORDER_DESCEND = "DESC";
    
    private $columns;
    private $flags;
    private $conditions;
    private $joins;
    private $patterns;
    private $object;
    private $db;

    private $map = [];
    private $primary_names;

    private $result;
    private $limit = false;//1000; // default limit
    private $offset = false;
    private $remaining = false;
    private $i = false;
    
    var $calc_rows = false;

    private $order = [];
    private $extra_cols;

    private $default_int_property = false;
    private $default_str_property = false;


    public function __construct($object)
    {
        $this->db = new mydb($object->database);

        $objects = func_get_args();

        $this->object = ( sizeof($objects) > 1 ) ? $objects  : $objects[0];

        $this->flags  = [];
        $this->conditions = [];
        $this->patterns = [];
        $this->extra_cols = [];
        $this->joins = [];
        return $this;
    }

    public function count_total_rows()
    {
        //// return the total number of rows disregarding effect of offset + limit
        return $this->remaining + $this->i + $this->offset;
    }

    public function sum($col)
    {
        //// Execute a SUM() query
        // * $col = column to SUM
        $ocol = $col;
        $col = $this->locate_property($col);

        if (!is_a($col, "Field")) {
            trigger_error("'$ocol' is not a valid property of ".get_class($this->object), E_USER_ERROR);
        }

        $q = preg_replace(
            '/^SELECT\s+([\w_\.]+)\*/',
            "SELECT SUM(".$col->get_field_name().")",
            $this->execute(false, true)
        );
        $this->db->query($q);
        list( $sum ) = $this->db->next(MYSQLI_NUM);
        return $sum;
    }

    public function distinct($col)
    {
        //// Execute a DISTINCT() query
        // @arg    - property to find distinct occurrences of
        // @return - an array of property's distinct values

        $col = $this->locate_property($col);

        $q = preg_replace(
            '/^SELECT\s+([\w_\.]+)\*/',
            "SELECT DISTINCT(".$col->get_field_name().")",
            $this->execute(false, true)
        );
        $this->db->query($q);

        
        $fields = [];
        while ($row = $this->db->next(MYSQLI_NUM)) {
            $fields[] = $row[0];
        }

        return $fields;
    }

    public function cols($col)
    {
        foreach (func_get_args() as $arg) {
            if (is_array($arg)) {
                list( $property, $alias ) = $arg;
            } else {
                $property = $arg;
            }

            $property = $this->locate_property($property);
            if (is_a($property, "Field")) {
                $col = $property->get_field_name();
                if (isset($alias)) {
                    $col = "$col as `$alias`";
                }

                $this->extra_cols[] = $col;
            }
        }
    }

    public function count($col = false)
    {
        
        //// execute a COUNT() query
        // * $col = column to count DISTINCT
        if ($col !== false) {
            $col = $this->locate_property($col);

            if (is_a($col, "Field")) {
                $col = "DISTINCT(".$col->get_field_name().")";
            }
        } else {
            $col = "*";
        }

        $q = preg_replace('/^SELECT\s+([\w_\.]+)\*/', "SELECT COUNT($col)", $this->execute(false, true));
        $this->db->query($q);
        list( $n ) = $this->db->next(MYSQLI_NUM);
        return $n;
    }

    public function map()
    {

        //// Create associations between object properties.

        // example: map( array("item_id", "id"), ... )

        // nb. property names uses the same order as the constructor

        foreach (func_get_args() as $map_arg) {
            if (is_array($map_arg)) {
                $this->map[] = $map_arg;
            }
        }
    }

    public function __get($name)
    {
        //// Private properties should be read-only
        return $this->{$name};
    }

    public function set_limit($int)
    {
        $this->limit = (int)$int;
    }
    public function limit($int)
    {
        $this->limit = (int)$int;
        return $this;
    }

    public function set_offset($int)
    {
        $this->offset = (int)$int;
    }
    public function get_offset()
    {
        return $this->offset;
    }

    public function set_flags($flags)
    {
        foreach ($flags as $flag) {
            call_user_func_array(array($this,"flag"), $flag);
        }
    }

    public function locate_property($str, $object = false, $return_obj = false)
    {

        //// Examine the $str arg to determine which of the object(s) properties it refers to.

        // Formats:
        // "propertyName"            => every object
        // "subName->propertyName"   => Sub property
        // "ObjectName|propertyName" => specific object

        if ($object === false) {
            $object = &$this->object;
        }

        if (is_array($object)) {
            if (strpos($str, Search::PROP_STR_OBJECT_DELIMITER) > -1) {
                /* Mapped objects available, but one has been specified */
                
                list( $obj_str, $str ) = explode(Search::PROP_STR_OBJECT_DELIMITER, $str);
            
                // inspect each object type until an $obj_str is encountered
                foreach ($object as $obj) {
                    if (is_a($obj, $obj_str)) {
                        $y = &$obj;
                        break;
                    }
                }
            } else {
                /* Mapped objects available, compile a return array via recursion */

                $props = [];

                foreach ($object as $o) {
                    $props[] = $this->locate_property($str, $o);
                }

                return $props;
            }
        } else {
            $y = &$object;
        }

        if (is_a($str, "Field")) {
            return $str;
        }
        
        $steps = explode(Search::PROP_STR_DELIMITER, $str);
        for ($j=0; $j<sizeof($steps); $j++) {
            $step = $steps[$j];

            if (property_exists($y, $step)) {
                // Allow access to a Sub's properties as well, this requires stepping $y onto the nested
                // object. However, only do so if there are property names still to follow. Otherwise only
                // refer to the Sub's inital property.
                if (is_a($y->{$step}, "Sub") && $j!=(sizeof($steps)-1)) {
                    // use Sub's classname to index the joins array, thereby ensuring we only join a table once.
                    if (is_string($y->{$step}->obj)) {
                        $y->{$step}->get_object();
                    }
                    
                    $object = &$y->{$step}->obj;
                    
                    $this->joins[get_class($object)] = "INNER JOIN ".$object->get_table()." ".
                                                       "ON ".$y->{$step}->get_field_name()." = ".
                                                        $object->id->get_field_name();

                    $last_y = $object;
                    $y = &$y->{$step}->obj;
                } else {
                    $last_y = $y;
                    $y = &$y->{$step};
                }
            } else {
                return false; // stall, property not available
            }
        }
        
        return ( $return_obj === true ) ? array( $y, $last_y ) : $y;
    }

    public function flag($flag_name)
    {

        $args = array_slice(func_get_args(), 1);

        $this->flags[$flag_name] = (object) [];
        $flag = &$this->flags[$flag_name];

        for ($i=0; $i<count($args); $i++) {
            $arg = $args[$i];

            if (is_string_r($arg)) {
                // A property selector
                // e.g. partner->client->postcode
                
                if (is_string($arg)) {
                    //$flag->property = &$this->locate_property($arg);
                    $arg = explode("|", $arg);
                }
                
                for ($j=0; $j<sizeof($arg); $j++) {
                    $arg[$j]=$this->locate_property($arg[$j]);
                }
                $flag->property = array_pop($arg);
            } elseif ($arg == SEARCH::CUSTOM_FUNC) {

                // Flag uses a custom search method, rather than type-based
                $func = $args[++$i];

                if (!function_exists($func)) {
                    // Run undefined function for native error
                    //call_user_func($func); #todo throw useful error? never needed this before but might be worthwhile
                } else {
                    $flag->func = $func;
                }


            } elseif ($arg == SEARCH::FLAG_USAGE) {
                $flag->usage = (string)$args[++$i];
            } elseif ($arg == SEARCH::PATTERN) {
                $this->patterns[ $args[++$i] ] = &$flag;
            } elseif ($arg == SEARCH::DEFAULT_STRING) {
                $this->default_str_property = &$flag;
            } elseif ($arg == SEARCH::DEFAULT_INT) {
                $this->default_int_property = &$flag;
            }
        }

        if (!property_exists($flag, "property")) {
            if (isset($this->object->{$flag_name})) {
                $flag->property = $this->object->{$flag_name};
            }
        }

        return $this;
    }

    private function input_str_to_flag_array($input)
    {

        /*
          Parse input string into an associative array 
          where the key is any flag name used and the
          value is the argument itself.
         */

        $args  = [];

        $quote_char    = null;
        $last_flag     = null;
        $flag          = null;
        $esc_next_char = null;

        $buffer = "";


        $wildcard = false;
        $wildcard_indices = [];


    // Allow search string to contain '-' character
        if (strpos($input, '-') != 0) {
            $input = str_replace('-', '*', $input);
        }


        $input = stripslashes($input);

        for ($i = 0; $i < strlen($input); $i++) {
            $char = $input[ $i ];

            if ($flag !== null) {
                // currently reading flag name
                if ($char == " ") {
                    $flag = substr($flag, 1);
                    $args[$flag] = "";
                    $buffer = &$args[$flag];
                    $last_flag = $flag;

                    $flag = null;
                    $wildcard = false;
                    $wildcard_indices = [];
                } elseif ($i == strlen($input)-1) {
                    $args[substr("$flag$char", 1)] = false;
                } else {
                    $flag .= $char;
                }
            } elseif ($char == "_" || $char == "%") {
                if ($wildcard !== false) {
                    $buffer .= "!$char";
                } else {
                    $wildcard_indices[ ] = strlen($buffer);
                    $buffer .= $char;
                }
            } elseif ($esc_next_char) {
                // add to buffer no matter what character it is
                $buffer .= $char;
                $esc_next_char = false;
            } elseif ($char == "\\") {
                // escape next char
                $esc_next_char = true;
            } elseif ($char == "'" || $char == '"') {
                // check for end of quotation
                if ($char == $qc) {
                    $quote_char = false;
                } // reading within quotes, another quote character
                //else if ( $quote_char ) {
                else {
                        $buffer .= $char;
                }
            } elseif (!$quote_char && $char == "*") {
                // encountered a wildcard character.
                // NOTE: i've switched from % to * as this is more familiar to users (Windows/Mac/Unix all feature)

                $buffer .= "%";
                $wildcard = true;

                // now need to retrospectively escape all SQL wildcards ( %, _ )
                for ($j = 0; $j < sizeof($wildcard_indices); $j++) {
                    $index = $wildcard_indices[ $j ] + $j;
                    $buffer = substr($buffer, 0, $index) . "!" . substr($buffer, $index);
                }
            } elseif (!$quote_char && $char == "-") {
                if (!sizeof($args) && strlen($buffer)) {
                    $args[""] = array( "text"=>trim($buffer), "wildcard"=>$wildcard );
                } elseif (sizeof($args)) {
                    $args[$last_flag] = array( "text"=>$args[$last_flag], "wildcard"=>$wildcard );
                }
                
                // declaring a new flag
                $flag = $char;
            } else {
                // simple append to buffer
                $buffer .= $char;
            }
        }

        // flush buffer
        if (!sizeof($args) && strlen($buffer)) {
            $args[""] = array( "text"=>$buffer,"wildcard"=>$wildcard );
        } elseif ($last_flag !== null && is_array($args) && array_key_exists($last_flag, $args)) {
            $args[$last_flag] = array( "text"=>$args[$last_flag], "wildcard"=>$wildcard );
        }

        return $args;
    }

    private function operator($operator, $property, $value, $wildcard = false, $auto_push = true)
    {

        /** Compose a sql condition based on nested DatabaseObject **/

        // resolve property
        $p = $property;

        if (!$property = $this->locate_property($property)) {
            // if doesn't resolve to a property, return original value
            $property = $p;
        }

        if (is_string($property)) {
            // assume that $p is valid, perhaps a subquery.
            $statement = "( $p ) ";
        } else {
            // Start preparing the condition ( field_name {operator} [value..n] )
            $statement = $property->get_field_name()." ";
        }

        if ($value === null) {
            if ($operator === "!="){
                $operator = "is not";
            } else {
                $operator = "is";
            }

            $value = " NULL";
        } elseif (is_a($property, "Field") &&
            $property->get_var(Field::TYPE) == Field::PASSWD &&
            is_string($value) ) {
            $value = "PASSWORD('".$this->db->real_escape_string($value)."')";
        } elseif (is_array($value) && in_array($operator, array( "=", "!=" ))) {
            /// Multiple values provided, array into "field_name (not) in [ value1, value2... valueN ]"
            
            $operator = ( "!=" == $operator ) ? "NOT IN" : "IN"; // operator change necessary
            
            if (count($value) == 0) {
                // Spot of sense-checking (at least one value necessary in array)
                trigger_error("$property's value array must contain at least one value");
                return false;
            }
            
            $varray = [];
            foreach ($value as $v) {
                // clean inputs
                $varray[] = ($this->is_numeric_type($property->get_var(Field::TYPE)))
                    ? intval($v)
                    : "'".$this->db->real_escape_string($v)."'";
            }
            
            $value = "( ".implode(', ', $varray)." )";
        } elseif ($wildcard && in_array($operator, array( "=", "!=" ))) {
        // Wildcard search on a single value

            $operator = ( "!=" == $operator ) ? "not like" : "like";
            $value = "'".$this->db->real_escape_string($value)."'";
        } else {
            $value = ( ( is_a($property, "Field") && $property->get_var(Field::TYPE) == Field::INTEGER )
                        || ( !is_a($property, "Field") && is_numeric($value) ) )
                ? intval($value)
                : "'".$this->db->real_escape_string($value)."'";
        }
        
        $statement .= "$operator $value";

        
        if ($auto_push) {
            $this->add_and($statement);
            return $this; // allows daisy-chaining
        } else {
            return "$statement";
        }
    }
    
    public function btw($property, $value1, $value2 = false, $auto_push = true)
    {

        // allow use of property selectors
        if (is_string($property) && !($property=&$this->locate_property($property) )) {
            return false;
        }

        if ($value2 === false) {
            // legacy support ( both values in $value1 )
            $eq = $property->get_field_name() ." BETWEEN ".$value1;
        } else {
            // Format values appropriately by passing them to $property's
            // Field::set method and retrieving them again.
            $origVal = $property();
            $property($value1);
            $value1 = $this->db->real_escape_string($property());
            $property($value2);
            $value2 = $this->db->real_escape_string($property());
            $property($origVal);

            $eq = $property->get_field_name()." BETWEEN '$value1' AND '$value2'";
        }
        
        if ($auto_push===true) {
            $this->add_and($eq);
            return $this;
        }
    }

    public function __call($name, $args)
    {
        /*
           Catch calls to condition constructors like eq(), nt() etc. where
           only one function is necessary but for aesthetic reasons I'm dealing with
           them here.
        */

        $cfuncs = array( "eq"=>"=", "gt"=>">", "lt"=>"<", "gteq"=>">=", "lteq"=>"<=", "nt"=>"!=" );

        if (array_key_exists($name, $cfuncs)) {
            array_unshift($args, $cfuncs[$name]); // prepend sql operator to arguments

            if (sizeof($args) < 2) {
                trigger_error("Only ".count($args).
                    " of necessary 2 ( String/Field $property, Any $value ) arguments provided", E_ERROR);
            }

            return call_user_func_array(array($this,"operator"), $args);
        } elseif ($name === "add_and" || $name === "add_or") {
            $conditions = [];

            // Return mode will prevent the prepared conditions from being
            // automatically appended to the collection.
            $return_mode = array_pop($args);
            if ($return_mode !== false && $return_mode !== true) {
                $args[ ] = $return_mode;
                $return_mode = false;
            }

            foreach ($args as $arg) {
                if (is_array($arg)) {
                    /*
                      // Spot of recursion, use return mode for this nested-array

                      Example output: 
                          where ( ( id = 10 OR age = 21 ) AND name = "daniel" )
                     */

                    $arg[ ]       = true;
                    $conditions[] = call_user_func_array(array( &$this, $name ), $arg);
                } else {
                    $conditions[ ] = $arg;
                }
            }

            $result = "( ".implode(( ( $name=="add_and" ) ? " AND " : " OR " ), $conditions)." )";

            if ($return_mode) {
                return $result;
            } else {
                $this->conditions[ ] = $result;
            }
        }
    }

    private function parse_arg_by_type($property, $search)
    {
        if (isset($property->type)) {
            $type = ( $property->type===false)
                ? Field::STRING
                : $property->get_var(Field::TYPE);
        } else {
            $type = Field::STRING;
        }


        $text=$search["text"];
        $wildcard=$search["wildcard"];

        if (strpos(substr($text, 0, 1), ' ') !== FALSE) {
            $trimmings = ltrim($text, ' ');
            $words = explode(" ", $trimmings);
        }else{
            $words = explode(" ", $text);
        }

        // #TODO $property->is(SQL_CAN_CMP_BETWEEN); rather than $type
        if ($type != Field::STRING && ( strtolower($words[0]) == "between" )) {
            // 'between 10, 20' 'between 10 and 20' 'between 10 & 20' etc.

            // prepare range
            $min = intval($words[1]);
            $max = intval((empty($words[3])) ? $words[2] : $words[3]);

            // add to conditions
            $this->add_and($property->get_field_name() ." between $min and $max");

            return;
        } elseif (isset($words[1]) && strlen($words[1]) >= 1) {
            if (intval($words[1]) && ( 2 % strlen($words[0]) == 0 ) && strpos(">= <=", $words[0]) !== false) {
                $this->add_and($property->get_field_name() . " $text");
                return;
            }
        }

        if (is_array($property)) {
            if (!sizeof($property)) {
                return false;
            }

            foreach ($property as $p) {
                $ors[ ] = $p->to_sql($text, $wildcard);
            }
            $this->add_or($ors);
        } else {
            $this->add_and($property->to_sql($text, $wildcard));
        }
    }

    public function get_object()
    {
        return $this->object;
    }

    private function search_on_flag(&$flag, $search)
    {
        if (isset($flag->func)) {
            call_user_func_array($flag->func, array( &$this, $search ));
        } else {
            $this->parse_arg_by_type($flag->property, $search);
        }
    }

    public function apply_filters($array, $or_mode = false)
    {

        //// Take an array [ 'property_name'=>"value" ], and generate
        //// conditions based on these

        // @arg $array
        // @arg $or_mode - seperate filters with OR ops rather than AND
        // @return null

        $sql = [];

        foreach ($array as $name => $value) {
            // if( empty($value) ){
                // continue;
            // }

            $step = $this->object;
            foreach (explode("->", $name) as $n) {
                if (!property_exists($step, $n) && ( is_a($step->{$n}, "Field") || is_a($step->{$n}, "Group") )) {
                    $step = $this->object;
                    break;
                }

                $step = $step->{$n};
            }

            if ($step !== $this->object) {
                $sql[] = $step->to_sql($value);
            }
        }

        if ($or_mode) {
            $this->add_or($sql);
        } else {
            $this->add_and($sql);
        }
    }

    public function add_order($property, $direction = Search::ORDER_ASCEND)
    {

        list( $prop, $parent ) = $this->locate_property($property, false, true);

        if (is_array($prop)) {
            foreach ($prop as $el) {
                if ($el = $this->locate_property($el, $parent)) {
                    $this->order[] = $el->get_field_name(false)." $direction";
                }
            }
        } else {
            if (!$prop) {
                $this->order[ ] = $property." ".$direction;
            } else {
                $this->order[ ] = $prop->get_field_name()." $direction";
            }
        }

        return $this;
    }

    public function order($colArray)
    {
        if (is_array($colArray) || is_object($colArray))
        {
            foreach ($colArray as $col) {
                if (!is_array($col)) {
                    $col = array( $col, Search::ORDER_ASCEND );
                }

                call_user_func_array(array($this,"add_order"), $col);
            }
        }
    }

    public function execute($input = false, $debug_mode = false)
    {
        // Separate the input string into flags and their corresponding
        // argument values.

        foreach ($this->input_str_to_flag_array($input) as $f => $a) {
            if (is_array($this->flags) && !array_key_exists($f, $this->flags)) {
                // check whether it corresponds to any regex pattern specified in
                // the Search model construction
                foreach ($this->patterns as $pattern => $property) {
                    if (preg_match($pattern, $a["text"])) {
                        $flag = &$property;
                    }
                }

                if (!isset($flag)) {
                    if (is_numeric(trim($a["text"])) && $this->default_int_property!==false) {
                        $flag = &$this->default_int_property;
                    } elseif ($this->default_str_property!==false) {
                        $flag = &$this->default_str_property;
                    }
                }
            } else {
                $flag = $this->flags[$f];
            }
        
            // when flag == postcode: strip whitespace, split string into array and implode with wildcards
            // this allow postcodes to be searched on with different formats
            // eg: 'fk136ru' 'fk13 6ru' 'fk1 36ru' will now be treated the same regardless of spacing/formating
            if ($f == 'postcode') {
                $a['text'] = str_replace(' ', '', $a['text']);
                $arr = str_split($a['text']);
                $a['text'] = implode("%", $arr);
                $a['wildcard'] = true;
            }

            if (isset($flag)) {
                $this->search_on_flag($flag, $a);
            }
        }

        $query = "SELECT ".( ( $this->calc_rows ) ? "SQL_CALC_FOUND_ROWS" : "" )." ";

        if (sizeof($this->extra_cols)) {
            $query .= implode(", ", $this->extra_cols) . ", ";
        }
        
        $query .= $this->object->get_table().".* FROM ".$this->object->get_table()." ".implode(" ", $this->joins)." ";

        if (sizeof($this->conditions) > 0) {
            $query .= "WHERE ".implode(" AND ", $this->conditions);
        }

        if (sizeof($this->order)) {
            $query .= " ORDER BY ". implode(", ", $this->order);
        } elseif (is_countable($this->object->order) && sizeof($this->object->order) > 0) {
            // Use DatabaseObject's default order specification
            if (is_array($this->object->order)){
                $this->order($this->object->order);
                $query .= " ORDER BY ".implode(", ", $this->order);
            }
        }

        if ($this->limit !== false) {
            $query .= " LIMIT $this->limit";
        }
        if ($this->offset !== false) {
            $query .= " OFFSET $this->offset";
        }

        $this->query = $query;

        if ($debug_mode) {
            return $query;
        }

        $this->db = new mydb(( ( is_array($this->object) ) ? $this->object[0]->database : $this->object->database ));

        if ($this->db->query($query)) {
            $this->result = $this->db->get_result(); // store result set

            if ($this->calc_rows) {
                // If desired, calculate number of rows ignoring offsets + limits.
                $this->db->query("SELECT FOUND_ROWS()");
                list($this->remaining) = $this->db->next(MYSQLI_NUM);
                $this->remaining -= (int) $this->offset;

                $this->i = 0;
                return $this->remaining;
            } else {
                $this->remaining = $this->result->num_rows;
                return $this->remaining;
            }
        }
        return false;
    }

    public function next($search_str = false)
    {

        if (!$this->result) {
            // if query hasn't executed yet, do so
            $this->execute($search_str);
        }

        if (is_object ($this)) {
            if (isset($this->result->num_rows) && isset($this->i)) {
                if ($this->result->num_rows == $this->i) {
                    $this->result = false;
                    return false;
                }
            }

        }


        if ($this->remaining > 0) {
            if ($d = $this->result->fetch_assoc()) {
                --$this->remaining;
                ++$this->i;

                if (is_array($this->object)) {
                    // Classname is stored in the final field
                    $classname = array_pop($d);
                    if (!class_exists($classname)) {
                        trigger_error("$classname is unknown", E_USER_ERROR);
                    }

                    // if this isn't an instance of the "primary" class the
                    // fields are going to have to be renamed in order to load()
                    if ($classname != get_class($this->object[0])) {
                        // determine object's position in the object array
                        $object_idx = -1;
                        for ($i = 0; $i < sizeof($this->object); $i++) {
                            if (get_class($this->object[$i]) == $classname) {
                                $object_idx = $i;
                                break;
                            }
                        }

                        if ($object_idx == -1) { // not found
                            trigger_error("Object type $classname unsupported", E_USER_ERROR);
                        }

                        $d2 = [];
                        for ($i = 0; $i < sizeof($this->map); $i++) {
                            $d2[ $this->map[$i][$object_idx] ] = array_shift($d);
                        }
                        
                        $d = $d2;
                    }
                } else {
                    $classname = get_class($this->object);
                }

                $next = new $classname;
                $next->load($d, false);
                $next->loaded = true;
                return ( sizeof($this->extra_cols) ) ? array( $next, (object) $d ) : $next;
            }
        }

        $this->db = false;
        return false;
    }

    public function help_html()
    {
        return (string) new Template(
            "generic/search-help.html",
            array( "class"=>ucwords(get_class($this->object)), "flags"=>$this->flags)
        );
    }

    public function get_response_array($search_str = false, $textgen = false, $valuegen = false)
    {
        
        /*
         * Automatically generate an array containing the default 
         * search response structure. 
         *
         * @Params
         * - search_str - specify the string to be searched upon
         * - textgen    - lambda function for generating the result text
         * - value_prop - lambda function for generating the return value
         */
        $matches = array( "results"=>[] );

        while ($m = $this->next($search_str)) {
            $match = [];

            $match["text"] = ( is_a($textgen, "Closure") )
                ? $textgen($m)
                : "$m";

            $match["value"] = ( is_a($valuegen, "Closure") )
                ? $valuegen($m)
                : $m->id();

            $matches["results"][] = $match;
        }

        $matches["remaining"] = $this->remaining;
        $matches["skipped"] = $this->offset;
        $matches["query"] = $this->query;

        return $matches;
    }
}
