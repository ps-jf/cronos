<?php
/**
 * Essentially our master library, needs to be included for the entire application to work
 *
 *
 * @package prophet.core
 * @author daniel.ness
 */

// Developer may store their own system settings (e.g. db creds)
// which will prevent core-system default settings from activating.

if (file_exists(__APP_BIN__ . "my_settings.php")) {
    include(__APP_BIN__ . "my_settings.php");
} else {
    // go looking for a my_settings.php file, just in case its somewhere else
    $dir = ".";
    while ($dir != '/') {
        if (file_exists($dir.'/shared/my_settings.php')) {
            include($dir."/shared/my_settings.php");
            break;
        } else {
            chdir('..');
            $dir = getcwd();
        }
    }
}

// suppressing 'Constant XY already defined' notice
@include __APP_BIN__ . "_settings.php";

require_once(__CORE_BIN__ . "objects/_main.php");
require_once(__CORE_BIN__ . "templates/_main.php");
require_once(__CORE_BIN__ . "Search.php");


use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use SparkPost\SparkPost;
use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;

function autoloadPropehtClasses($class_name)
{

    // Notice the lack of 'require_once's? This little monkey of a function
    // will automatically load undefined classes and interfaces. We'll check
    // the types subdirectory first as there will be more calls ( and subsequent
    // file access attempts ) for Type objects per request than there will be
    // DatabaseObjects.

    foreach ([__CORE_BIN__, __APP_BIN__] as $base) {
        foreach (["objects/types/", "objects/", "objects/tools/"] as $subdir) {
            $file = "{$base}{$subdir}{$class_name}.php";

            if (is_readable($file)) {
                require_once $file;
                break;
            }
        }
    }
}

spl_autoload_register("autoloadPropehtClasses");


function my_error_handler($number, $message, $file, $line)
{

    switch ($number) {
        case E_NOTICE:
            $type = "Notice";
            break;

        case E_WARNING:
            $type = "Warning";
            break;

        case E_USER_WARNING:
            $type = "Warning";
            break;

        case E_USER_NOTICE:
            $type = "Notice";
            break;

        case E_RECOVERABLE_ERROR:
            $type = "Non-Fatal Error";
            break;

        default:
            $type = "Error";
            break;
    }

    $row = array(
        $type,                    // type of error
        "$file:$line",            // reference
        $_SERVER['REQUEST_URI'],  // current url
        $message
    );

    $log = new Log(Log::ERROR);
    $log->write($row);

    if ($number == E_USER_ERROR) {
        header("HTTP/1.0 500 Internal Server Error");
    }
}

//set_error_handler( "my_error_handler" );


/**** Common Regular Expressions ***/
class Patterns
{
    const OUTSIDE_QUOTES = '(([^"]*"[^"]*")*[^"]*"[^"]*)?$';
    const UK_POSTCODE = '[a-z]{1,2}[0-9][0-9a-z]?\s*([0-9][a-z]{2})?';
    const NINO = '[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}';
    const EMAIL_ADDRESS = '/^[-A-Z0-9._%+&]+@[A-Z0-9.&-]+\.[A-Z]{2,4}$/i'; // http://www.regular-expressions.info/email.html
    const MOBILE_PHONE = '/^[\d\s]+$/';
    const TELEPHONE_NUM = '/^[\d\s\-]+$/';
}

function url($url, $get, $xml_compliant = false)
{
    // form a url
    // -- used in Templates to modify _SELF and existing get

    $get_in = [];
    if (strpos($url, "?") > -1) {

        // parse original GET query string
        parse_str(substr($url, strrpos($url, "?") + 1), $get_in);
        $url = substr($url, 0, strrpos($url, "?"));
    }

    $get_out = []; //, noone wants you here, [];

    // Iterate through input array, adjusting and including elements
    // as necessary.
    if (is_array($get)) {
        foreach ($get as $key => $value) {

            if (gettype($key) != "string") { // treat array value as key

                if (isset(${$value}))
                    $get_out[$value] = ${$value};
                else if (array_key_exists($value, $_GET))
                    $get_out[$value] = $_GET[$value];
            } else {

                // NULL nulifies inclusion of $key derived from any source
                if ($value == NULL) {
                    unset($get_in[$key]);
                    continue;
                }

                $get_out[$key] = $value;
            }
        }
    }

    // Any duplicates will be overwritten ( array1 <- array2 )
    foreach (array_merge($get_in, $get_out) as $key => $value)
        $get_out[$key] = "$key=" . urlencode($value);

    $url .= "?" . implode("&", $get_out);

    return ($xml_compliant) ? htmlentities($url) : $url;
}

function salty_md5($str_to_encrypt = false)
{
    // Returns MD5-encryption based on input string and a dynamic salt.

    // salt
    $host = $_SERVER['SERVER_ADDR'];

    if (in_array($host, ["localhost:8080", "prophet.test:8088", "prophet.test", "prophet.test:8080"])){
        $host = "127.0.0.1";
    }

    $k = (int)(str_replace(".", "", $host) / (date('Y') - 2002));
    $k .= substr("aeiouhklmnpw", date('n') - 1, 1);
    return (!$str_to_encrypt) ? false : md5($k . $str_to_encrypt);
}

// random_string - LETTERS only for ID's
function random_string($length)
{
    $str = "rdm";
    $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";

    $size = strlen($chars);
    for ($i = 0; $i < $length; $i++) {
        $str .= $chars[rand(0, $size - 1)];
    }

    return $str;
}

function passwd()
{
    // generate a password
    $pwd = strtolower(random_string(8)) . ";";
    $swap = ["a", "s", "x", "d"];
    $for = ["@", "$", "*", "?"];
    return str_replace($swap, $for, $pwd);
}


function sendEmail($recipient, $subject, $template, $blind_copy = false, $sender = PS_EMAIL_MAIN)
{
    if (in_array($_SERVER['REMOTE_ADDR'], EMAIL_WHITELIST)) {
        $recipient = User::get_default_instance("email");
    }

    if (isset($blind_copy)) {
        $headers = "From: <$sender>\r\n" .
            "Reply-To: <$sender>\r\n" .
            "Recipient: <$recipient>\r\n" .
            "Return-Path: <$sender>\r\n" .
            "BCC: <$blind_copy>\r\n";

    } else {
        $headers = "From: <$sender>\r\n" .
            "Reply-To: <$sender>\r\n" .
            "Recipient: <$recipient>\r\n" .
            "Return-Path: <$sender>\r\n";
    }

    return mail($recipient, $subject, "$template", $headers, "-r partnersupport@policyservices.co.uk");
}

/**
 * SparkPost mailer function, two methods, one using a locally hosted template (need to use this if attachments are to
 * be sent) and second method using template hosted on SparkPost
 *
 * @param $recipient
 * @param $subject
 * @param $hosted_template
 * @param $substitutionData
 * @param $from
 * @param array $file_array
 * @param array $local_template
 * @return bool
 */
function sendSparkEmail($recipient, $subject, $hosted_template, $substitutionData, $from, $file_array = [], $local_template = [])
{
    $statusMessage = "";
    $campaign_name = $subject;
    $templateName = $hosted_template;
    $returnAddress = ($_GET['return_email']) ?? 'it@policyservices.co.uk';

    if (APP_ENV != "production") {
        $recipient = [
            [
                "address" => [
                    "email" => User::get_default_instance("email"),
                    "name" => User::get_default_instance("username"),
                ]
            ]
        ];
    } else {
        if (isset($recipient['email'])) {
            //we dont want to use this format anymore, switch to the new format
            $recipient = [
                [
                    "address" => [
                        "email" => $recipient['email'],
                        "name" => $recipient['name'] ?? "Policy Services",
                    ]
                ]
            ];
        }

        if (!is_array($recipient)) {
            // if this still isnt an array, use the default values
            $recipient = [
                [
                    "address" => [
                        "email" => 'it@policyservices.co.uk',
                        "name" => 'Policy Services',
                    ]
                ]
            ];
        }
    }

    $httpClient = new GuzzleAdapter(new GuzzleClient());
    $sparky = new Sparkpost($httpClient, ['key' => SPARKPOST_API_KEY]);

    // pass the rest off to ol' sparky!
    if ($hosted_template === "" && !empty($local_template)) {
        // we need to use local template here as we cannot attach files and use hosted file in same call
        $promise = $sparky->transmissions->post([
            "options" => [
                "ip_pool" => DEDICATED_MAILER_IP
            ],
            "campaign_id" => $campaign_name,
            "content" => [
                'from' => $from,
                'subject' => $subject,
                'html' => (string)$local_template['html'],
                'text' => (string)$local_template['text'],
                "attachments" => $file_array
            ],
            "recipients" => $recipient
        ]);
    } elseif ($hosted_template !== "") {
        // use a template that we have created and hosted on sparkpost
        $promise = $sparky->transmissions->post([
            "options" => [
                "ip_pool" => DEDICATED_MAILER_IP
            ],
            "campaign_id" => $campaign_name,
            "content" => [
                'from' => $from,
                'subject' => $subject,
                "template_id" => $hosted_template
            ],
            "substitution_data" => json_decode($substitutionData),
            "recipients" => $recipient
        ]);
    } else {
        dd("Arguments missing");
    }

    try {
        $response = $promise->wait();
        $statusMessage .= $response->getStatusCode() . "\n";
        ($statusMessage == 200) ? $status = true : $status = false;

    } catch (Exception $e) {

        $statusMessage .= $e->getCode() . "\n";
        $statusMessage .= $e->getMessage() . "\n";
        $status = false;
    }

    return $status;
}

/**
 * Save files to azure
 * adds file to the storage. Usage: storageAddFile("myContainer", "C:\path\to\file.png", "filename-on-storage.png")
 *
 * @param $containerName
 * @param $file
 * @param $fileName
 *
 * @return bool
 */
function azureAddFile($containerName, $file, $fileName)
{
    # setup and instance of an azure client
    $connectionString = "DefaultEndpointsProtocol=https;AccountName=".AZURE_ACCOUNT_NAME.";AccountKey=".AZURE_ACCOUNT_KEY.";EndpointSuffix=core.windows.net";

    // Create blob client
    $blobClient = BlobRestProxy::createBlobService($connectionString);

    $handle = @fopen($file, "r");
    if ($handle) {
        $options = new CreateBlockBlobOptions();
        $mime = null;

        try{
            // identify mime type
            $mimes = new \Mimey\MimeTypes;
            $mime = $mimes->getMimeType(pathinfo($fileName, PATHINFO_EXTENSION));
            // set content type
            $options->setContentType($mime);
        } catch (Exception $e) {
            error_log("Failed to read mime from '".$file."': " . $e);
        }

        try{
            if ($mime) {
                $cacheTime = getCacheTimeByMimeType($mime);
                if ($cacheTime){
                    $options->setCacheControl("public, max-age=".$cacheTime);
                }
            }

            $blobClient->createBlockBlob($containerName, $fileName, $handle, $options);
        } catch (Exception $e) {
            error_log("Failed to upload file '" . $file . "' to storage: " . $e);
        }

        @fclose($handle);
        return true;
    } else {
        error_log("Failed to open file '" . $file . "' to upload to storage.");
        return false;
    }
}

## get cache time by mime type
function getCacheTimeByMimeType($mime){
    $mime = strtolower($mime);

    $types = array(
        "application/json" => 604800,// 7 days
        "application/javascript" => 604800,// 7 days
        "application/xml" => 604800,// 7 days
        "application/xhtml+xml" => 604800,// 7 days
        "image/bmp" => 604800,// 7 days
        "image/gif" => 604800,// 7 days
        "image/jpeg" => 604800,// 7 days
        "image/png" => 604800,// 7 days
        "image/tiff" => 604800,// 7 days
        "image/svg+xml" => 604800,// 7 days
        "image/x-icon" => 604800,// 7 days
        "text/plain" => 604800, // 7 days
        "text/html" => 604800,// 7 days
        "text/css" => 604800,// 7 days
        "text/richtext" => 604800,// 7 days
        "text/xml" => 604800,// 7 days
    );

    // return value
    if (array_key_exists($mime, $types)){
        return $types[$mime];
    }

    return FALSE;
}

# check if a number is is e.164 format and return a tel link
function callLink($number){
    if(!empty($number)){
        return "<a href='tel:$number'><i class='fas fa-phone'></i> Call</a>";
    } else {
        return "";
    }
}

## check if a file exists on azure
function azureCheckExists($container, $file){
    # setup and instance of an azure client
    $connectionString = "DefaultEndpointsProtocol=https;AccountName=".AZURE_ACCOUNT_NAME.";AccountKey=".AZURE_ACCOUNT_KEY.";EndpointSuffix=core.windows.net";

    // Create blob client
    $blobClient = BlobRestProxy::createBlobService($connectionString);

    try {
        $blob = $blobClient->getBlob($container, $file);

        return true;
    } catch (Exception $e) {
        return false;
    }
}

## move a blob to another container
function azureCopyFile($sourceContainer, $sourceFile, $newContainer, $newFile){
    # setup and instance of an azure client
    $connectionString = "DefaultEndpointsProtocol=https;AccountName=".AZURE_ACCOUNT_NAME.";AccountKey=".AZURE_ACCOUNT_KEY.";EndpointSuffix=core.windows.net";

    // Create blob client
    $blobClient = BlobRestProxy::createBlobService($connectionString);

    try {
        $blobClient->copyBlob($newContainer, $newFile, $sourceContainer, $sourceFile);
    } catch (Exception $e) {
        error_log("Failed to copy file '" . $sourceFile . "': " . $e);
    }
}

## removes file from the storage. Usage: storageAddFile("myContainer", "filename-on-storage.png")
function azureRemoveFile($containerName, $fileName)
{
    # Setup a specific instance of an Azure::Storage::Client
    $connectionString = "DefaultEndpointsProtocol=https;AccountName=".AZURE_ACCOUNT_NAME.";AccountKey=".AZURE_ACCOUNT_KEY.";EndpointSuffix=core.windows.net";
    // Create blob client.
    $blobClient = BlobRestProxy::createBlobService($connectionString);

    try
    {
        $blobClient->deleteBlob($containerName, $fileName);
    } catch ( Exception $e ) {
        error_log("Failed to delete file '".$fileName."' from storage: " . $e);
    }

    return true;
}

class mydb
{

    const NO_CACHE = "no-cache";

    private static $db_links = [];
    private $mysqli;
    private $result;

    public function __construct($server = false, $connection_err_level = E_USER_ERROR)
    {

        ($server) or $server = LOCAL;

        /*
          To cut connections down to one per User I've implemented a class array
          which will store mysqli instances indexed by server.
          Therefore, this constructor will check the array for an existing mysqli
          instance and, if found, assign instance variable $mysqli a reference to it.
          If a preexisting instance is not found, one will be initiated and stored.
        */

        if (!array_key_exists($server, self::$db_links)) {

            $link = mysqli_init();
            list($host, $user, $password, $db_name) = json_decode($server);

            if($server === REPORTING){
                $link->options(MYSQLI_OPT_CONNECT_TIMEOUT, 30); // 30 seconds to connect
                $link->options(MYSQLI_OPT_READ_TIMEOUT, 300); // 5 minutes to return
            }

            // open persistent connection
            if (!$link->real_connect($host, $user, $password, $db_name)) {
                trigger_error($link->error, $connection_err_level);
            }

            $link->set_charset("utf8mb4");

            self::$db_links[$server] = $link;
        }

        $this->mysqli = &self::$db_links[$server];
    }

    public function __get($name)
    {
        // Allow access to mysqli class properties
        return $this->mysqli->{$name};
    }

    public function __call($name, $args)
    {
        if (sizeof($args)) {
            // Allow access to mysqli class methods
            return call_user_func_array(array($this->mysqli, $name), $args);
        }
    }

    public function query($stmt)
    {

        $r = $this->mysqli->query($stmt);

        if (!$this->errno) {

            $this->result = $r;

            return (strpos(strtolower($stmt), "insert") === 0)
                ? $this->insert_id
                : $this->affected_rows;

        } else {
            trigger_error("$this->error ($stmt)");
            return false;
        }
    }

    public function get_result()
    {
        return $this->result;
    }

    public function set_result($result)
    {
        $this->result = $result;
    }

    public function next($type = MYSQLI_ASSOC, $result = false)
    {

        ($result) or $result = $this->result;

        if (is_a($type, "DatabaseObject")) {

            $d = mysqli_fetch_array($result, MYSQLI_ASSOC);

            if (!$d) {
                return false;
            }

            $type->load($d);
            return $type;

        } else {
            if ($result instanceof mysqli_result) {
                if ($type == MYSQLI_OBJECT) {
                    $d = mysqli_fetch_object($result);
                } else {
                    $d = mysqli_fetch_array($result, $type);
                }
            } else {
                $d = false;
            }
        }

        return $d;
    }
}


function el($args)
{
    $args = func_get_args();

    if (is_a($args[0], "Field") || is_a($args[0], "DatabaseObject")) {

        if (method_exists($args[0], "toElement")) {
            return call_user_func_array(array($args[0], "toElement"), array_slice($args, 1));
        }
    }
}

class el
{

    /*
       Experimental tag generator. Really only deals with form elements at the moment, with
       the aim to allow easy insertion of custom objects such as date input, advanced drop
       down boxes etc.
    */

    const TEXT_FIELD = "txt";
    const HIDDEN_FIELD = "hide";

    static function css($files, $root = __APP_STATIC_ROOT__)
    {

        /*** list of files ***/

        if (is_array($files)) {
            $out = [];
            foreach ($files as $f) {
                $out[] = el::css($f, $root);
            }
            return $out;
        }

        /*** single file ***/

        if (substr($files, -4) != ".css") {
            $files .= ".css";
        }

        if ($files[0] != '/') {
            // path not specified, use $root
            $files = $root . "css/$files";
        }

        return "<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"$files\" />";
    }

    static function js($files, $root = __APP_STATIC_ROOT__)
    {

        /*** list of files ***/

        if (is_array($files)) {
            $out = [];
            foreach ($files as $f) {
                $out[] = el::js($f, $root);
            }
            return $out;
        }

        /*** single file ***/

        if (substr($files, -4) != ".js") {
            $files .= ".js";
        }

        if ($files[0] != '/') {
            // path not specified, use $root
            $files = $root . "js/$files";
        }

        return "<script type=\"text/javascript\" src=\"$files\"></script>";

    }

    static function txt($id, $value = false, $etc = false)
    {
        return "<input type='text' id='$id' name='$id' value=\"$value\"$etc />";
    }

    static function pwd($id)
    {
        return "<input type='password' id='$id' name='$id' autofill='no'/>";
    }

    static function date($id)
    {
        $style = "style=\"text-align:center;\"";
        return "<input type='text' class='date day' id='" . $id . "_d' name='" . $id . "_d' $style size=2 maxlength=2 pattern='^(0[1-9]|[12][0-9]|3[01])$' /> " .
            "<input type='text' class='date month' id='" . $id . "_m' name='" . $id . "_m' $style size=2 maxlength=2 pattern='^(0[1-9]|1[012])$' /> " .
            "<input type='text' class='date year' id='" . $id . "_y' name='" . $id . "_y' $style size=4 maxlength=4 pattern='^(19|20)\d{2}$' />";
    }

    static function datetime($id)
    {
        $style = "style=\"text-align:center;\"";
        return "<input type='text' class='datetime day' id='" . $id . "_d' name='" . $id . "_d' $style size=2 maxlength=2 pattern='^(0[1-9]|[12][0-9]|3[01])$' /> " .
            "<input type='text' class='datetime month' id='" . $id . "_m' name='" . $id . "_m' $style size=2 maxlength=2 pattern='^(0[1-9]|1[012])$' /> " .
            "<input type='text' class='datetime year' id='" . $id . "_y' name='" . $id . "_y' $style size=4 maxlength=4 pattern='^(19|20)\d{2}$' />&nbsp;:&nbsp;" .
            "<input type='text' class='datetime hour' id='" . $id . "_h' name='" . $id . "_h' $style size=2 maxlength=2 pattern='^([01][0-9]|2[0-3])$' /> " .
            "<input type='text' class='datetime minute' id='" . $id . "_i' name='" . $id . "_i' $style size=2 max=59 maxlength=2 pattern='^([05][0-9])$' /> ";
    }

    static function hide($id, $value = false, $attr = false)
    {
        return "<input type='hidden' id='$id' name='$id' value='$value' $attr />";
    }

    static function bt($content = false, $id = false, $attr = false)
    {

        if ($id)
            $id = "id=\"$id\" name=\"$id\"";

        return "<button $id $attr>$content</button>";

    }

    static function image($src, $root = __APP_STATIC_ROOT__)
    {

        if ($src[0] != "/")
            $src = $root . "images/" . $src;

        return $src;
    }

    static function chk($id, $etc = false)
    {
        return "<input type=\"checkbox\" name=\"$id\" id=\"$id\"$etc />";
    }

    static function radio($id, $value, $etc = false)
    {
        return "<input type=\"radio\" name=\"$id\" id=\"$id\" value=\"$value\"$etc />";
    }

    static function dropdown($id, $options, $value = false, $attr = false, $exclude_select_tag = false, $exclude_spacer_option = false)
    {
        $out = ($exclude_select_tag) ? "" : "<select id='$id' name='$id' $attr>\n";

        if (!$exclude_spacer_option) {
            $out .= "<option value=''> -- </option>\n";
        }

        if (is_a($options, "Search")) {

            while ($object = $options->next()) {
                $selected = ($object->id() == $value) ? "selected='selected'" : "";
                $out .= "<option value='" . $object->id() . "'$selected>$object</option>\n";
            }

        } else if (is_a($options, "mydb")) {

            while ($row = $options->next(MYSQLI_NUM)) {

                if (sizeof($row) < 2) {
                    $id = $text = $row[0];
                } else
                    list($id, $text) = $row;

                $selected = ($id == $value) ? "selected='selected'" : false;

                $out .= "<option value='$id' $selected>$text</option>\n";
            }
        } else if (is_array($options)) {

            $is_assoc = is_assoc($options);

            foreach ($options as $v => $l) {
                ($is_assoc) or $v = $l;


                $selected = ($value !== false && $v == $value) ? "selected='selected'" : false;

                $out .= "<option value='$v' $selected>$l</option>\n";
            }
        }

        if (!$exclude_select_tag) {
            $out .= "</select>";
        }

        return $out;
    }

    static function suggest( $id, $url, $var, $focus=false, $value=false, $text=false, $classes=false, $elClasses=false ) {

        $t = new Template( "misc/suggest.html" );
        $t -> tag( $id, "id" );
        $t -> tag( random_string( 15 ), "link" );
        $t -> tag( $url, "url" );
        $t -> tag( $var, "var" );
        $t -> tag( $value, "val" );
        $t->tag( $text, "text" );
        $t -> tag( $focus, "focus" );
        $t -> tag( $classes, "classes" );
        $t -> tag( $elClasses, "elClasses" );

        echo $t;
    }

    static function boolean($id, $name=false, $value="0")
    {
        if(!$name){
            $name = $id;
        }

        echo "<button type='button' class='bool' id='$id' name='$name' value='$value'>&nbsp;</button>";
    }
}

function sanitize($input)
{
    $search = array('@<script[^>]*?>.*?</script>@si',   // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
    );

    $output = preg_replace($search, '', $input);
    return trim($output);

}

function is_assoc($array)
{

    return array_keys($array) !== range(0, count($array) - 1);

}

// --------------------------------
//
//      Context Menu Objects
//
// --------------------------------

class ContextMenu
{

    private $filename;
    private $js_functions;
    private $root_menu;
    private $object;

    public function __construct($object, $xml_filename)
    {
        $this->object = $object;
        $this->filename = $xml_filename;
        $this->js_functions = [];
        $this->parse_xml();
    }

    private function parse_xml()
    {

        $object = $this->object;

        try {
            // buffer xml file content
            ob_start();
            include($this->filename);
            $xml_content = (string)ob_get_contents();
            ob_end_clean();

            if (strlen($xml_content) < 1) {
                return false;
            }

            $xml = new SimpleXMLElement($xml_content, LIBXML_NOCDATA);
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        // if context menu has a functions block containing
        // function definitions append within the root js_functions
        // array.
        if (isset($xml->functions)) {

            foreach ($xml->functions->children() as $func) {

                $name = trim((string)$func->attributes()->name);

                if (!strlen($name)) { // function must be named
                    continue;
                }

                // pick up variables declared but undefined in the source.
                // These can be set as an attribute in the item/menu tag.
                preg_match_all("/\bvar ([\w][\w_0-9]*);/", $func, $vars);

                $this->js_functions[$name] = array("vars" => $vars[1], "src" => $func);


            }
        }

        $this->root_menu = $this->get_menu($xml->menu);
    }

    private function get_menu($menu_xml)
    {

        $items = [];

        // check for an onclick action
        $menu_action = (isset($menu_xml->attributes()->action))
            ? (string)$menu_xml->attributes()->action
            : false;

        foreach ($menu_xml->item as $node) {


            $item = [];

            foreach ($node->attributes() as $k => $v) {
                $item[$k] = (string)$v;
            }

            if (isset($node->children()->menu)) {

                $item["menu"]["items"] = $this->get_menu($node->children()->menu);
            }


            // ensure that either an attribute has set the item label,
            // otherwise take the element's inner text for one.
            if (!array_key_exists("label", $item)) {
                $item["label"] = trim("$node");
            }

            // if no onclick action has been defined for this item
            // inherit parent's, which incidentally may also not
            // exist.
            if (!array_key_exists("action", $item)) {
                $item["action"] = $menu_action;
            }

            if ($item["action"] && array_key_exists($item["action"], $this->js_functions)) {

                $func = $item["action"];
                $args = array("this");

                foreach ($this->js_functions[$func]["vars"] as $var) {
                    $args[] = (array_key_exists($var, $item)) ? "\"" . htmlentities($item[$var]) . "\"" : null;
                }

                $item["action"] = "$item[action](" . implode(',', $args) . ")";
            }

            $items[] = $item;
        }

        return $items;
    }

    public function __toString()
    {

        $output = array("items" => [], "actions" => []);

        foreach ($this->js_functions as $name => $func) {
            array_unshift($func["vars"], "__li");
            $output["actions"][$name] = "function(" . implode(",", $func["vars"]) . "){" . $func['src'] . "}";
        }

        $output["items"] = $this->root_menu;

        return (string)json_encode($output);
    }
}

function truncate($string, $length, $show_title = true)
{
    /*
     * # If $string's length exceeds $length, return truncated version with a title
     * # attribute so that mouseover text reveals the full text. Make sure and not
     * # invalidate any nested tags.
     */

    $string = '' . $string; // cast to string

    if (strlen($string) > $length) {
        $in_tag = false;
        $o = "";

        for ($i = 0; $i < $length; $i++) {

            if (!isset($string[$i]))
                return $string;

            $char = $string[$i];

            if ($in_tag) {
                if ($char == '>')
                    $in_tag = false;

                ++$length;
            } elseif ($char == '<') {
                $in_tag = true;
                ++$length;
            }

            $o .= $char;
        }

        $title = preg_replace(array('/<.*?>/', '/\s{2,}/'), '', $string);
        $title = preg_replace('/"/', '\\"', $title);

        return ($show_title)
            ? "<span class='truncated' title=\"$title\">$o...</span>"
            : "$o...";
    }
    return $string;
}

function parse_name($in)
{

    /*
     * # Parses string arg into a standard array of name components.
     *
     * @param - string - raw input name
     * @return - array - parsed pieces OR false
     *
     * - note: If $wildcard, also provide a hint as to where sql queries should use a LIKE.
     */

    $out = [];
    $in = trim($in);

    if (($comma_pos = strpos($in, ",")) !== false) {
        // Smith, John%
        $out["forename"] = trim(substr($in, $comma_pos + 1));
        $out["surname"] = trim(substr($in, 0, $comma_pos));
        $out["wildcard"] = "forename";
    } elseif (($last_space_pos = strrpos($in, " ")) !== false) {
        // John Smith%
        $out['forename'] = trim(substr($in, 0, $last_space_pos));
        $out['surname'] = trim(substr($in, $last_space_pos + 1));
        $out['wildcard'] = 'surname';
    } else {
        // Smith%
        $out['surname'] = $in;
        $out['wildcard'] = 'surname';
    }

    return $out;
}


/**
 * function to check multidimensional array and whether or not all values are empty
 *
 * @param array $array
 * @return bool
 */
function isArrayEmpty(array $array): bool
{
    $empty = true;

    array_walk_recursive($array, function ($leaf) use (&$empty) {
        if ($leaf === [] || $leaf === '' || $leaf === '--') {
            return;
        }

        $empty = false;
    });

    return $empty;
}

/**
 * Pluralizes a word if quantity is not one.
 *
 * @param int $quantity Number of items
 * @param string $singular Singular form of word
 * @param string $plural Optional plural form of word; function will attempt to deduce plural form from singular if not provided
 * @return null|string Pluralized word if quantity is not one, otherwise singular
 */
function pluralize($quantity, $singular, $plural = null)
{
    if ($quantity == 1 || !strlen($singular)) return $singular;
    if ($plural !== null) return $plural;

    $last_letter = strtolower($singular[strlen($singular) - 1]);
    switch ($last_letter) {
        case 'y':
            return substr($singular, 0, -1) . 'ies';
        case 's':
            return $singular . 'es';
        default:
            return $singular . 's';
    }
}


/**
 * Pings an IP or FQDN to see if it lives
 *
 * @param $host
 * @param $port
 * @param $timeout
 * @return bool|mixed
 */
function ping($host, $port, $timeout) {
    $a = gethostbyname('idontexist.tld');
    $b = gethostbyname($host);

    if ($a == $b)
        return false;

    $time = microtime(true);
    $fp = @fsockopen($host, $port, $errCode, $errStr, $timeout);
    $time = microtime(true) - $time;
    if ($fp) {
        fclose($fp);
        return $time;
    } else {
        return false;
    }
}


/**
 * @param $startDate
 * @param $businessDays
 * @return mixed
 */
function addBusinessDays($carbonDate, $businessDays)
{
    while ($businessDays > 0) {
        if ($carbonDate->isWeekend()) {
            // adds 1 day (not a business day so do not decrement counter)
            $carbonDate = $carbonDate->addDay();
        } else {
            // add 1 business day
            $carbonDate = $carbonDate->addDay();
            $businessDays--;
        }
    }
    return $carbonDate;
}

function format_current($number, $dp = 2)
{
    $number = (float) $number;

    return ($number < 0) ? '- £ '.number_format(($number * -1), $dp) : '£ '.number_format($number, $dp);
}

class RedisConnection {
    private static $connection;

    public static function connect(): \Predis\Client
    {
        if(empty(self::$connection)){
            self::$connection = new Predis\Client();
        }

        return self::$connection;
    }
}
