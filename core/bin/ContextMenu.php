<?php
function sanitize($input)
{
    $search = [
        '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
    ];

    $output = preg_replace($search, '', $input);
    return trim($output);
}

function is_assoc($array)
{
    return array_keys($array) !== range(0, count($array) - 1);
}

// --------------------------------
//
//      Context Menu Objects
//
// --------------------------------

class ContextMenu
{
    private $filename;
    private $js_functions;
    private $root_menu;
    private $object;

    public function __construct($object, $xml_filename)
    {
        $this->object = $object;
        $this->filename = $xml_filename;
        $this->js_functions = [];
        $this->parse_xml();
    }

    private function parse_xml()
    {
        $object = $this->object;

        try {
            // buffer xml file content
            ob_start();
            include($this->filename);
            $xml_content = (string)ob_get_contents();
            ob_end_clean();

            if (strlen($xml_content) < 1) {
                return false;
            }

            $xml = new SimpleXMLElement($xml_content, LIBXML_NOCDATA);
        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        // if context menu has a functions block containing
        // function definitions append within the root js_functions
        // array.
        if (isset($xml->functions)) {
            foreach ($xml->functions->children() as $func) {
                $name = trim((string)$func->attributes()->name);

                if (!strlen($name)) { // function must be named
                    continue;
                }

                // pick up variables declared but undefined in the source.
                // These can be set as an attribute in the item/menu tag.
                preg_match_all("/\bvar ([\w][\w_0-9]*);/", $func, $vars);

                $this->js_functions[$name] = array("vars" => $vars[1], "src" => $func);
            }
        }

        $this->root_menu = $this->get_menu($xml->menu);
    }

    private function get_menu($menu_xml)
    {
        $items = [];

        // check for an onclick action
        $menu_action = (isset($menu_xml->attributes()->action))
            ? (string)$menu_xml->attributes()->action
            : false;

        foreach ($menu_xml->item as $node) {
            $item = [];

            foreach ($node->attributes() as $k => $v) {
                $item[$k] = (string)$v;
            }

            if (isset($node->children()->menu)) {
                $item["menu"]["items"] = $this->get_menu($node->children()->menu);
            }


            // ensure that either an attribute has set the item label,
            // otherwise take the element's inner text for one.
            if (!array_key_exists("label", $item)) {
                $item["label"] = trim("$node");
            }

            // if no onclick action has been defined for this item
            // inherit parent's, which incidentally may also not
            // exist.
            if (!array_key_exists("action", $item)) {
                $item["action"] = $menu_action;
            }

            if ($item["action"] && array_key_exists($item["action"], $this->js_functions)) {
                $func = $item["action"];
                $args = array("this");

                foreach ($this->js_functions[$func]["vars"] as $var) {
                    $args[] = (array_key_exists($var, $item)) ? "\"" . htmlentities($item[$var]) . "\"" : null;
                }

                $item["action"] = "$item[action](" . implode(',', $args) . ")";
            }

            $items[] = $item;
        }

        return $items;
    }

    public function __toString()
    {
        $output = array("items" => [], "actions" => []);

        foreach ($this->js_functions as $name => $func) {
            array_unshift($func["vars"], "__li");
            $output["actions"][$name] = "function(" . implode(",", $func["vars"]) . "){" . $func['src'] . "}";
        }

        $output["items"] = $this->root_menu;

        return (string)json_encode($output);
    }
}
