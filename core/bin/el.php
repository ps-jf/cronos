<?php
function el($args)
{

    $args = func_get_args();

    if (is_a($args[0], "Field") || is_a($args[0], "DatabaseObject")) {
        if (method_exists($args[0], "toElement")) {
            return call_user_func_array(array($args[0],"toElement"), array_slice($args, 1));
        }
    }
}

class el
{

    /*
       Experimental tag generator. Really only deals with form elements at the moment, with
       the aim to allow easy insertion of custom objects such as date input, advanced drop
       down boxes etc.
    */

    const TEXT_FIELD = "txt";
    const HIDDEN_FIELD = "hide";

    static function css($files, $root = __APP_STATIC_ROOT__)
    {

        /*** list of files ***/

        if (is_array($files)) {
            $out = [];
            foreach ($files as $f) {
                $out[] = el::css($f, $root);
            }
            return $out;
        }

        /*** single file ***/

        if (substr($files, -4) != ".css") {
            $files .= ".css";
        }

        if ($files[0] != '/') {
            // path not specified, use $root
            $files = $root."css/$files";
        }

        return "<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"$files\" />";
    }

    static function js($files, $root = __APP_STATIC_ROOT__)
    {

        /*** list of files ***/

        if (is_array($files)) {
            $out = [];
            foreach ($files as $f) {
                $out[] = el::js($f, $root);
            }
            return $out;
        }

        /*** single file ***/

        if (substr($files, -4) != ".js") {
            $files .= ".js";
        }

        if ($files[0] != '/') {
            // path not specified, use $root
            $files = $root."js/$files";
        }

        return "<script type=\"text/javascript\" src=\"$files\"></script>";
    }

    static function txt($id, $value = false, $etc = false)
    {
        return "<input type='text' id='$id' name='$id' value=\"$value\"$etc />";
    }

    static function pwd($id)
    {
        return "<input type='password' id='$id' name='$id' autofill='no'/>";
    }

    static function date($id)
    {
        $style = "style=\"text-align:center;\"";
        return  "<input type='text' class='date day' id='".$id."_d' name='".$id."_d' $style size=2 maxlength=2 pattern='^(0[1-9]|[12][0-9]|3[01])$' /> ".
            "<input type='text' class='date month' id='".$id."_m' name='".$id."_m' $style size=2 maxlength=2 pattern='^(0[1-9]|1[012])$' /> ".
            "<input type='text' class='date year' id='".$id."_y' name='".$id."_y' $style size=4 maxlength=4 pattern='^(19|20)\d{2}$' />";
    }

    static function hide($id, $value = false, $attr = false)
    {
        return "<input type='hidden' id='$id' name='$id' value='$value' $attr />";
    }

    static function bt($content = false, $id = false, $attr = false)
    {

        if ($id) {
            $id = "id=\"$id\" name=\"$id\"";
        }

        return "<button $id $attr>$content</button>";
    }

    static function image($src, $root = __APP_STATIC_ROOT__)
    {

        if ($src[0] != "/") {
            $src = $root."images/".$src;
        }

        return $src;
    }

    static function chk($id, $etc = false)
    {
        return "<input type=\"checkbox\" name=\"$id\" id=\"$id\"$etc />";
    }

    static function radio($id, $value, $etc = false)
    {
        return "<input type=\"radio\" name=\"$id\" id=\"$id\" value=\"$value\"$etc />";
    }

    static function dropdown($id, $options, $value = false, $attr = false, $exclude_select_tag = false, $exclude_spacer_option = false)
    {

        $out = ( $exclude_select_tag ) ? "" : "<select id='$id' name='$id'>\n";

        if (!$exclude_spacer_option) {
            $out.="<option value=''> -- </option>\n";
        }

        if (is_a($options, "Search")) {
            while ($object = $options->next()) {
                $selected = ($object->id()==$value) ? "selected='selected'" : "";
                $out .= "<option value='".$object->id()."'$selected>$object</option>\n";
            }
        } else if (is_a($options, "mydb")) {
            while ($row = $options->next(MYSQLI_NUM)) {
                if (sizeof($row) < 2) {
                    $id = $text = $row[0];
                } else {
                    list( $id, $text ) = $row;
                }

                $selected = ( $id == $value ) ? "selected='selected'" : false;

                $out .= "<option value='$id' $selected>$text</option>\n";
            }
        } else if (is_array($options)) {
            $is_assoc = is_assoc($options);

            foreach ($options as $v => $l) {
                ( $is_assoc ) or $v = $l;


                $selected = ( $value!==false && $v == $value ) ? "selected='selected'" : false;

                $out .= "<option value='$v' $selected>$l</option>\n";
            }
        }

        if (!$exclude_select_tag) {
            $out .= "</select>";
        }

        return $out;
    }

    static function suggest($id, $url, $var, $focus = false, $value = false, $text = false)
    {

        $t = new Template("misc/suggest.html");
        $t -> tag($id, "id");
        $t -> tag(random_string(15), "link");
        $t -> tag($url, "url");
        $t -> tag($var, "var");
        $t -> tag($value, "val");
        $t->tag($text, "text");
        $t -> tag($focus, "focus");
        echo $t;
    }
}
