#!/usr/bin/python

from sparkpost import SparkPost
from sparkpost.exceptions import SparkPostAPIException
import csv
import sys
import MySQLdb as mysql

MYSQL_HOST = "192.168.16.22"
MYSQL_USER = "root"
MYSQL_PWD = "splat"
MYSQL_DB = "prophet"

# Sparkpost API key for office IP address
sparky = SparkPost('06b2eb616803cd51d9b718ede66045fb9333e1d4')


def email(template, staff):
    try:
        recipient = [staff['address']['email1']]

        response = sparky.transmissions.send(
            from_email='it@policyservices.co.uk',
            recipients=recipient,
            subject='Prophet - System Updates',
            html=template,
            ip_pool='ps_mailer',
            track_opens=True,
            track_clicks=True,
            use_draft_template=True
        )

        print response

    except SparkPostAPIException as err:
        # http response status code
        print(err.status)
        # python requests library response object
        # http://docs.python-requests.org/en/master/api/#requests.Response
        print(err.response.json())
        # list of formatted errors
        print(err.errors)


def html_template(staff):

    with open('changelog_email.html', 'r') as f:
        template = f.read()

    return email(template, staff)


recipient_list = []

try:
    # Prophet database connection
    conn = mysql.connect(host=MYSQL_HOST, user=MYSQL_USER, passwd=MYSQL_PWD, db=MYSQL_DB)
    cursor = conn.cursor()

    # Select staff bonus entries for month
    cursor.execute("select email_address from prophet.users where active = 1 and id not in (62, 63, 26)")

    staff_list = cursor.fetchall()

    for staff in staff_list:
        staff_dict = {
            'address': {
            'email1': staff[0],
            }
        }
        html_template(staff_dict)
        recipient_list.append(staff_dict)
except Exception, e:
    sys.exit(e)
