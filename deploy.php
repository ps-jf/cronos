<?php

namespace Deployer {

    require 'recipe/common.php';
    require 'vendor/deployer/recipes/recipe/bugsnag.php';
    require 'vendor/deployer/recipes/recipe/cachetool.php';

    // to deploy a specific branch, specify at end
    // dep5 deploy $stage --branch="branch_name"

    // Configuration
    set('repository', 'git@bitbucket.org:policyservices/cronos.git');
    set('git_tty', false); // [Optional] Allocate tty for git on first deployment
    set('shared_files', []);
    set('shared_dirs', []);

    set('writable_dirs', [
        'current/',
        'current/vendor',
        'release/vendor'
    ]);

    set('allow_anonymous_stats', false);

    // Custom security/permissions
    set('writable_use_sudo', true);        // Using sudo in writable commands?
    set('writable_mode', 'chown');         // Default to chown instead of acl
    set('writable_chmod_mode', '0755');    // For chmod mode
    set('writable_chmod_recursive', true); // For chmod mode
    set('cleanup_use_sudo', true);         // Run cleanup using sudo
    set('http_user', 'www-data');          // Set ownership to www-data

    // Customise Composer defaults
    set('composer_action', 'update');
    set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

    // BugSnag config
    set('bugsnag_api_key', "917eb844e1fd9a875bcd199a1601a3dc");
    set('bugsnag_app_version', "deploy-{{stage}}-{{release_name}}");

    // Hosts
    host('production')
        ->hostname('192.168.16.22')
        ->stage('production')
        ->set('deploy_path', '/var/www/sites/prophet');

    /* http://bigmummy2.internal.policyservices.com:81/ */
    host('staging')
        ->hostname('192.168.16.22')
        ->stage('staging')
        ->set('deploy_path', '/var/www/beta/prophet');

    host('ps-prophet-staging')
        ->hostname('192.168.16.186')
        ->stage('ps-prophet-staging')
        ->set('deploy_path', '/var/www/sites/prophet');

    // Send notification to staff with latest changes
    task('staffEmail', function () {
        run("python changelog.py");
    })->local();


    task('printTaskTime', function () {
        writeln(date('Y-m-d H:i:s'));
    });

    // tag deploy
    task('tag_deploy', function () {
        run('cd {{release_path}} && git tag -a "deploy-{{stage}}-{{release_name}}" -m "Tag Generated during deploy" && git push origin --tags');
        writeln("Tagged: deploy-{{stage}}-{{release_name}}");
    });

    task('clearCache', function () {
        run('curl "https://bigmummy2.internal.policyservices.com/clear_cache.php"');
    });

    task('repo_check', function () {
        $branch = get('branch');
        writeln("Checking if we have the latest changes, branch: ".$branch);
        set('local_hash', exec('git rev-parse --short HEAD'));
        writeln('Local hash: {{local_hash}}');
        set('remote_hash', exec('git rev-parse --short origin/'.$branch));
        writeln('Remote hash: {{remote_hash}}');

        if (get('local_hash') !== get('remote_hash')) {
            writeln('Aborting deploy.. please pull latest code');
            invoke('deploy:unlock');
            exit();
        }
        writeln("Local copy is up to date");
    });

    // Tasks
    desc('Deploy Prophet');
    task('deploy', [
        'printTaskTime',
        'repo_check',
        'deploy:prepare',
        'deploy:lock',
        'deploy:release',
        'deploy:update_code',
        'deploy:shared',
        'deploy:writable',
        'deploy:vendors',
        'deploy:clear_paths',
        'deploy:symlink',
        'deploy:unlock',
        'cleanup',
        'success',
        //'staffEmail',
        'bugsnag:notify',       // notify Bugsnag of deploy
        'tag_deploy',
        'clearCache',
        'printTaskTime'
    ]);

    // [Optional] if deploy fails automatically unlock.
    after('deploy:failed', 'deploy:unlock');
}
